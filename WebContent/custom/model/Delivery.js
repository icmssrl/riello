jQuery.sap.declare("model.Delivery");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.Delivery = (function () {

  Delivery = function (serializedData) {
    this.orderId = "";
    this.deliveryId = "";
    this.bollaNum = "";
    this.state = "";
    this.goodsMov= "";
    this.deliveryDate = "";
    this.preTel = "";
    this.contactPer = "";
    this.contactTel = "";
    this.sender ="";
    this.senderName ="";
    this.senderStreet = "";
    this.senderAddrNum ="";
    this.senderPostCode = "";
    this.senderCity = "";
    this.senderRegion="";
    this.senderCountry= "";
    this.destination= "";
    this.destinationName="";
    this.destinationStreet="";
    this.destinationPostCode= "";
    this.destinationCity="";
    this.destinationRegion="";
    this.destinationCountry="";
    this.filiale ="";
    this.trakingInfos = [];
    this.practices = [];

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };


    this.loadTrackingInfos = function(data)
    {
      var defer = Q.defer();
      var req =
      {
        "VbelnSo": this.orderId,
        "Vbeln":this.deliveryId
      };

      var fSuccess = function(result)
      {
        this.trakingInfos=[];
        if(result && result.results.length>0)
        {
          for(var i = 0; i < result.results.length; i++)
          {
            this.trakingInfos.push(model.persistence.Serializer.delivery_item.fromSAP(result.results[i]));
          }
        }
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);
      var fError = function (err)
      {
        this.trakingInfos = [];
        defer.reject(err);
      }
      fError=_.bind(fError, this);

      model.odata.chiamateOdata.loadTrackingInfos(req, fSuccess, fError);

      return defer.promise;
    };

    
    this.getShippmentPrintPdf= function()
    {
      var defer = Q.defer();
      var fSuccess = function(result)
      {
        this.shippmentPrintUrl = result.EUrl;
        var url  = this.shippmentPrintUrl;
        sap.m.URLHelper.redirect(url, true);
        defer.resolve(this.shippmentPrintUrl);
      }
      var fError = function(err)
      {
        this.shippmentPrintUrl = "";
        defer.reject(err);
      }
      fSuccess = _.bind(fSuccess, this);
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadShippmentPrintPdfUrl(this.deliveryId, fSuccess, fError)

      return defer.promise;
    };
    this.getOrderId = function () {
      return this.orderId;
    };

    this.getId = function () {
      return this.deliveryId;
    };


    this.getInvoice= function()
    {
      var defer=Q.defer();
      var req = {
        "VbtypV":"J",
        "Vbelv":this.deliveryId
      }
      var fSuccess = function(result)
      {
        if(result)
        {
          if(result.results)
          {
            this.invoices = [];
            if(result.results.length > 0)
              this.invoices = result.results;
          }
          else {
            this.invoices = result;
          }
        }
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);
      var fError = function(err)
      {
        this.invoices = [];
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadFollowingFunction(req, fSuccess, fError);
      return defer.promise;
    };

    this.getInvoicePrintPdf= function(invoiceId)
    {

      var defer = Q.defer();
      var fSuccess = function(result)
      {
        this.invoicePrintUrl = result.EUrl;
        var url  = this.invoicePrintUrl;
        sap.m.URLHelper.redirect(url, true);
        defer.resolve(this.invoicePrintUrl);
      }
      var fError = function(err)
      {
        this.invoicePrintUrl = "";
        sap.m.MessageToast.show("Error loading invoice pdf");
        defer.reject(err);
      }
      fSuccess = _.bind(fSuccess, this);
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadInvoicePrintPdfUrl(invoiceId, fSuccess, fError)

      return defer.promise;
    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

    
     this.getAbnormalPracticesList = function(req)
  {
    var defer = Q.defer();
    
    var fSuccess = function(result){
      
      var res = model.persistence.Serializer.abnormalPracticesButtons.fromSAPItems(result);
      
      defer.resolve(res);
    };
    var fError = function(err)
    {
      sap.m.MessageToast.show(utils.Message.getSAPErrorMsg(err));
      defer.reject(err);
    };
    
    model.odata.chiamateOdata.getAbnormalPracticesList(req, fSuccess, fError);
    
    return defer.promise;
  };
  this.getOpenPracticesList = function(org)
  {
	  var defer = Q.defer();
	  
	  var req = {};
	  req.Bukrs = org.Bukrs;
	  req.Vkorg =org.Vkorg;
	  req.Vtweg = org.Vtweg;
	  req.Spart = org.Spart;
	  req.Vbelv=this.deliveryId;
	  
	  var fSuccess = function(result)
	  {
		  this.practices = [];
		  this.practices = model.persistence.Serializer.practice.fromSAPItems(result);
		  defer.resolve(this);
	  }
	  fSuccess = _.bind(fSuccess, this);
	  
	  var fError = function(err)
	  {
		this.practices= [];
		defer.reject(err);  
	  }
	  fError = _.bind(fError, this);
	  
	  model.odata.chiamateOdata.loadDeliveryPractices(req,fSuccess, fError);
	  
    
	  return defer.promise;
  };
    
    
    if (serializedData) {
      this.update(serializedData);
    }

    
    
    
    
    
    
    return this;
  };
  
  
 
  
  
  

  return Delivery;


})();
