jQuery.sap.declare("model.Product");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("utils.ParseDate");

model.Product = (function () {

  Product = function (serializedData) {

    //proprietà dell'oggetto prodotto
    this.productId = undefined;
    //this.id = undefined;
    this.scale = undefined;
    this.description = undefined;
    //this.name = undefined;
    this.unitListPrice = 0;
    this.unitNetPrice = 0;
    this.totalPrice = 0;
    this.currency="EUR";

    this.taxPrice = 0;
    this.productPicUrl = undefined;
    this.availableInfo = "";
    this.quantity=1;
    this.isSanctioned = false;

    this.getId = function () {
      return this.productId;
    };

    this.init = function(id, description, scale, unitListPrice, currency, kit)
    {
      this.productId = id;
      this.description = description;
      this.scale = scale;
      this.unitListPrice = unitListPrice;
      this.currency = currency;
      this.isKit = _.isEmpty(kit)? false : true;
      
    };

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };

    this.getUnitListPrice = function()
    {
      return this.unitListPrice;
    };

    this.getUnitNetPrice = function()
    {
      return this.unitNetPrice;
    };

    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
        if(prop == "availableDate")
        {
        	this[prop]= new Date(this[prop]);
        }
      }
    };
    this.getTypeDescription= function()
    {
    	this.typeDescr = this.typeDescr ? this.typeDescr : "";
    	switch(this.type){
	    	case "N": 
	    		this.typeDescr = model.i18n.getText("new");
	    		break;
	    	case "C":
	    		this.typeDescr = model.i18n.getText("JOB");
	    		break;
	    	default:
	    		this.typeDescr = "";
	    		break;
    	}
    	return this.typeDescr;
    };
    this.getAvailability = function(userInfo, reqQty, reqDate)
    {
      var defer = Q.defer();

      var req =
  		{
  			"salesOrg" :userInfo.Vkorg,
  	    "distrCh": userInfo.Vtweg,
  	    "division" : userInfo.Spart,
  	    "productId" : this.productId,
  	    "quantity" : reqQty,
  	    "reqDate" : utils.ParseDate().formatDate(reqDate, "yyyy-MM-ddT00:00:00")
  		};

  		var fSuccess = function(result)
  		{
        //this.availableDate = !!result.EDtmin ? new Date(result.EDtmin) : ((!!result.EDatum) ? new Date(result.EDatum) : undefined);
        this.availableDate = !!result.EDtmin ? result.EDtmin : ((!!result.EDatum) ? (result.EDatum) : undefined);
        this.availableQty = parseInt(result.EMenge);
        this.stockQty = parseInt(result.EStock);
        this.type = result.EAtptype;
        this.typeDescr = this.getTypeDescription();
        this.status = (result.EOk === "OK")? true : false;
        this.reqQty = parseInt(result.CMenge);
        //this.reqDate = !!result.CEdatu ? new Date(result.CEdatu) : undefined;
        this.reqDate = !!result.CEdatu ? (result.CEdatu) : undefined;
        
        /*		Added Check on committed product	*/
        if(this.availableDate && this.availableDate > this.reqDate && this.type == "C")
        {
        	sap.m.MessageToast.show(model.i18n.getText("requestedDateBeforeAvailableDate"));
        	this.reqDate = new Date(this.availableDate);
        } 
        /* ------------------------------------------*/
        
        this.isKit = _.isEmpty(result.EIskit)? false : true;
        this.isSanctioned = _.isEmpty(result.ESanzionato)? false : true;
        if(this.isKit)
        {
          this.components = [];
          for(var i = 0; i< result.Matnr_ATP_CompSet.results.length; i++)
          {
            this.components.push({
              "componentId": result.Matnr_ATP_CompSet.results[i].Matnr,
              "description": result.Matnr_ATP_CompSet.results[i].Maktx,
              "availability" : (result.Matnr_ATP_CompSet.results[i].Xmiss === "X") ? false : true,
              "stockQty": parseInt(result.Matnr_ATP_CompSet.results[i].Stock),
              "reqQty" : parseInt(result.Matnr_ATP_CompSet.results[i].Kmpmg),
              "qtyXkit" : parseInt(result.Matnr_ATP_CompSet.results[i].Mnghd),
              "availableQty": parseInt(result.Matnr_ATP_CompSet.results[i].Menge),
              //"availableDate": !!result.Matnr_ATP_CompSet.results[i].Datum ? new Date(result.Matnr_ATP_CompSet.results[i].Datum) : undefined
              "availableDate": !!result.Matnr_ATP_CompSet.results[i].Datum ? (result.Matnr_ATP_CompSet.results[i].Datum) : undefined
            })
          }
        }
        this.availableInfo = result.EOut;
        //Trasform availableInfo string to delete comma in float
        // var words = this.availableInfo.split(" ");
        // this.availableInfo = words[0];
        // for(var i =1; i< words.length; i++)
        // {
        //   if(!isNaN(parseFloat(words[i])))
        //     words[i]= parseInt(words[i]);
        //
        //   this.availableInfo += " "+words[i];
        // }
        //--------------------------------
  			//console.log(result);
        defer.resolve(this);
  		}
  		fSuccess = _.bind(fSuccess, this);

  		var fError = function(err)
  		{

  			//console.log(JSON.parse(err.response.body));
//        sap.m.MessageToast.show(model.i18n.getText("CHECKING_AVAILABILITY_ERROR")+": "+this.productId);
  			
//        defer.reject(JSON.parse(err.response.body));
  			var jsonError =JSON.parse(err.response.body)
  			var text = model.i18n.getText("CHECKING_AVAILABILITY_ERROR")+": "+this.productId;
  			jsonError.error.message.value = text;
  			err.response.body = JSON.stringify(jsonError);
  			defer.reject(err);

  		}
  		fError = _.bind(fError, this);

  		model.odata.chiamateOdata.getProductAvailability(req, fSuccess, fError);

      return defer.promise;
    };

    this.isProduct = function()
    {
      return true;
    };
    this.setQuantity = function(quantity)
    {
      if(!isNaN(quantity))
      {
        this.quantity = parseInt(quantity);

        //-----------Temp-----------------------
        // this.refreshPrice_temp();
      }

    };
    this.getPriceDetail = function(customer, iva)
    {
      var defer = Q.defer();
      var data = {
        "IVkorg":customer.registry.salesOrg,
        "IVtweg":customer.registry.distrCh,
        "ISpart":customer.registry.division,
        "IWerks":"",
        "IVkbur":customer.registry.areaManager,
        "IVkgrp":customer.registry.territoryManager,
        "ICdage":customer.registry.agentCode,
        "IKunag":customer.registry.id,
        "IKunwe":"",
        "IMatnr":this.productId,
        "IMenge":this.quantity,
        "IVrkme":"",
        "ITaxk1":iva? iva: ""
      };
     

      var fSuccess = function(result)
      {
        //console.log(result);
        this.setPrice(result);

        defer.resolve(result);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function(error)
      {
        //console.log(error);
        utils.Message.getSAPErrorMsg(error, "Error loading Price");
        defer.reject(error);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.getProductPriceDetail(data, fSuccess, fError);

      return defer.promise;
    };
    this.setPrice  =function(priceInfo)
    {
      this.totalPrice = priceInfo.OPrice;
      this.currency=priceInfo.OWaers;
      this.unitNetPrice = priceInfo.OPriceUnit;
      this.taxPrice = priceInfo.OIva;
    };
//---------------------------Temp-----------------------
    // this.refreshPrice_temp = function()
    // {
    //   this.totalPrice = this.unitListPrice * this.quantity;
    //
    // };
//---------------------------------------------------

    if (serializedData) {
      this.update(serializedData);
    }
    return this;
  };
  return Product;


})();
