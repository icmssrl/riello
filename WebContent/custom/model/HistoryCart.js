
jQuery.sap.declare("model.HistoryCart");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("utils.Message");
jQuery.sap.require("model.Position");

model.HistoryCart = ( function ()
{


      /******************************************************************/
      //Temp Code


    HistoryCart = function(data)
      {
        this.orderId  = "";
        this.customerId = "";
        this.customerName = "";
        this.username="";
        this.favorite = false;


        this.cartCreationDate = "";
        this.cartCreationTime = "";

        this.flag = "";
        this.positions = [];


        this.update = function(data)
        {
          for(var prop in data)
          {
            this[prop]= data[prop];
          }
        };

        this.create = function(orderId)
        {
          this.orderId= orderId;

        };

//        this.addPosition = function(position)
//        {
//          if(!this.positions || this.positions.length === 0)
//            this.positions = [];
//
//          this.positions.push(position);
//        };
        this.setPositions = function(positionsArr)
        {
          if(positionsArr && positionsArr.length > 0)
          {
            this.positions = positionsArr;
          }
        };
        this.getPositions = function()
        {
          if(!this.positions || this.positions.length === 0)
            this.positions = [];

          return this.positions;
        };

        this.loadPositions = function()
        {

          var defer = Q.defer();
          var fSuccess = function (result) {
            if (result && result.results && result.results.length > 0) {
              for (var i = 0; i < result.results.length; i++) {
                this.positions.push(new model.Position(model.persistence.Serializer.order_item.fromSAP(result.results[i])));
              }
              // _.forEach(result.results, _.bind(function(item){
              //   this.loadProduct(item.Matnr, user)
              //   .then(_.bind(function(res){
              //
              //     var position = new model.Position(model.persistence.Serializer.order_item.fromSAP(item));
              //     position.setProduct(new model.Product(model.persistence.Serializer.product.fromSAP(res)));
              //     this.positions.push(position);
              //
              //   }, this))
              // }, this))


            }
            defer.resolve(this);
          }
          fSuccess = _.bind(fSuccess, this);

          var fError = function (err) {

            utils.Message.getSAPErrorMsg(err, "Error Loading Cart Items of " + this.orderId)

            defer.reject(err);
          }
          fError = _.bind(fError, this);

          model.odata.chiamateOdata.loadOrderItems(this.orderId, fSuccess, fError)

          // .then(fSuccess, fError);

          return defer.promise;

        };

        // this.loadProduct= function(orgData, position, defer)
        // {
        //   var data = orgData;
        //   data.Matnr = position.productId;
        //
        //   var fSuccess = function(res)
        //   {
        //     if(res && res.results && res.results.length > 0)
        //     {
        //       var qta = position.quantity; //OMG
        //       position.setProduct(new model.Product(model.persistence.Serializer.product.fromSAP(res.results[0])));
        //       position.setQuantity(qta);
        //     }
        //     defer.resolve(position);
        //   }
        //   fSuccess = _.bind(fSuccess, this);
        //   var fError = function(err)
        //   {
        //     utils.Message.getSAPErrorMsg(err, "Error Loading product");
        //     defer.reject(err);
        //   }
        //   fError= _.bind(fError, this);
        //
        //   model.odata.chiamateOdata.getProductDetail(data, fSuccess, fError)
        //
        // };
        this.loadProducts = function(orgData)
        {
          var defer = Q.defer();
          var callback = function(result)
          {
            defer.resolve(this);
          };
          callback = _.bind(callback, this);

          var loadProduct = function(orgData, positions, loop, index)
          {
            var item = positions[index];
            var data = orgData;
            data.Matnr = item.productId;
            var fSuccess = function(res)
            {
              if(res && res.results && res.results.length > 0)
              {
                var qta = item.quantity; //OMG
                item.setProduct(new model.Product(model.persistence.Serializer.product.fromSAP(res.results[0])));
                item.setQuantity(qta);
              }
              loop();
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function(err)
            {
              utils.Message.getSAPErrorMsg(err, "Error Loading product");
              defer.reject(err);
              return;
            }
            fError= _.bind(fError, this);

            model.odata.chiamateOdata.getProductDetail(data, fSuccess, fError)
          };
          loadProduct = _.bind(loadProduct, this);

          var asyncLoop = function(o){

            var i=-1;

            var loop = function(){
              i++;
              if(i==o.length){o.callback(); return;}
              o.functionToLoop(o.org, o.items, loop, i);
            }
            loop();//init
          }

          asyncLoop({
            items: this.positions,
            org: orgData,
            length : this.positions.length,
            functionToLoop : loadProduct,
            callback : callback

          });

          return defer.promise;
          //-----------------------------------------------------------------------------
          // _.forEach(this.positions, _.bind(function(item){
          //   var data = orgData;
          //   data.Matnr = item.productId;
          //   var fSuccess = function(res)
          //   {
          //     if(res && res.results && res.results.length > 0)
          //     {
          //       var qta = item.quantity; //OMG
          //       item.setProduct(new model.Product(model.persistence.Serializer.product.fromSAP(res.results[0])));
          //       item.setQuantity(qta);
          //     }
          //
          //   }
          //   fSuccess = _.bind(fSuccess, this);
          //   var fError = function(err)
          //   {
          //     utils.Message.getSAPErrorMsg(err, "Error Loading product");
          //
          //   }
          //   fError= _.bind(fError, this);
          //
          //   model.odata.chiamateOdata.getProductDetail(data, fSuccess, fError)
          // }, this))

          //----------------------------------------------------------------------------------------------------
          // var defer = Q.defer();
          // var fSuccess = function (result) {
          //   if (result && result.results && result.results.length > 0) {
          //     var product = new model.Product(model.persistence.Serializer.order_item.fromSAP(result.results[0])));
          //     }
          //
          //   defer.resolve(product);
          // }
          // fSuccess = _.bind(fSuccess, this);
          //
          // var fError = function (err) {
          //
          //   utils.Message.getSAPErrorMsg(err, "Error Loading Product " + productId)
          //
          //   defer.reject(err);
          // }
          // fError = _.bind(fError, this);
          //
          // model.odata.chiamateOdata.getProductDetail(data, fSuccess, fError)
          //
          // // .then(fSuccess, fError);
          //
          // return defer.promise;

        };

        this.loadCartHeader = function(user)
        {
          var defer = Q.defer();
          var data = {};

          data.Uname = user.userGeneralData.username;
          data.Vbeln = this.orderId;
          data.Xpref="";

          var fSuccess = function(result)
          {
            this.update(model.persistence.Serializer.historyCart.fromSAP(result));
            // var loadItems = _.bind(this.loadPositions, this);
            // loadItems()
            // .then(_.bind(function(result)
            // {
            //   defer.resolve(this);
            // }, this), _.bind(function(err)
            // {
            //   defer.reject(err);
            // }, this))
            defer.resolve(this);
          }
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            utils.Message.getSAPErrorMsg(err, "Error loading Cart");
            defer.reject(err);
          }
          fError = _.bind(fError, this);

          model.odata.chiamateOdata.getCartByOrder(data, fSuccess, fError)


          return defer.promise;
        };

        this.loadCart = function(user, orgData)
        {

          var defer = Q.defer();

          var fSuccess = function(result)
          {
            defer.resolve(this);
          }
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            utils.Message.getSAPErrorMsg(err, "Error Loading Cart");
            defer.reject(err)
          }
          fError = _.bind(fError, this);




          this.loadCartHeader(user)
          .then(_.bind(this.loadPositions, this))
          .then(_.bind(function(res)
          {
            var def = Q.defer();

            this.totListCart = 0;
            this.totNetCart = 0;
            this.totTaxCart = 0;
            for(var j=0; j < this.positions.length; j++)
            {
              this.totListCart += parseFloat(this.positions[j].totalListPrice);
              this.totNetCart += parseFloat(this.positions[j].totalNetPrice);
              this.totTaxCart += parseFloat(this.positions[j].totalTaxPrice);
            }

            this.loadProducts(orgData)
              .then(fSuccess, fError);

            return def.promise;
          }, this))


          // var defer = Q.defer();
          // var data = {};
          //
          // data.Uname = user.userGeneralData.username;
          // data.Vbeln = this.orderId;
          // data.Xpref="";
          //
          // var fSuccess = function(result)
          // {
          //   this.update(model.persistence.Serializer.historyCart.fromSAP(result));
          //   var loadItems = _.bind(this.loadPositions, this);
          //   loadItems()
          //   .then(_.bind(function(result)
          //   {
          //     defer.resolve(this);
          //   }, this), _.bind(function(err)
          //   {
          //     defer.reject(err);
          //   }, this))
          //
          // }
          // fSuccess = _.bind(fSuccess, this);
          //
          // var fError = function(err)
          // {
          //   utils.Message.getSAPErrorMsg(err, "Error loading Cart");
          //   defer.reject(err);
          // }
          // fError = _.bind(fError, this);
          //
          // model.odata.chiamateOdata.getCartByOrder(data, fSuccess, fError)



          return defer.promise;
        };
        this.getModel = function()
        {
          var model = new sap.ui.model.json.JSONModel(this);
          return model;
        };


      if(data)
        this.update(data);

      return this;
    }

    return HistoryCart;






      /*******************************************************************/


})();
