jQuery.sap.declare("model.praticheAnomale.ProductAP");

model.praticheAnomale.ProductAP = (function () {

  ProductAP = function (serializedData) {
    
 
    this.productId = undefined;
    this.orderId = undefined;
    this.shippmentId = undefined;
    this.position = undefined;
    this.upperPosition = undefined;
    this.description = undefined;
    this.managedViaCommessa = undefined;
    this.managedViaMatricola = undefined;
    this.nrMatricola = undefined;
    this.originalQuantity = undefined;
      
    this.missingQuantity = undefined;
    this.defectQuantity = undefined;
    this.toRenderBackQuantity = undefined;
      
    this.renderedQuantity = undefined;
    this.creditNoteValue = undefined;
    this.creditNoteValueRequest = undefined;
    this.billNetValue = undefined;

    this.getId = function () {
      return this.productId;
    };

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };

    this.getPosition = function()
    {
      return this.position;
    };
    
    this.getShippmentId = function()
    {
      return this.shippmentId;
    };

    this.getKitPosition = function()
    {
      return this.upperPosition;
    };
      
    this.getNrMatricola = function()
    {
      return this.nrMatricola;
    };
      
    this.setPosition = function(newPosition)
    {
      this.position = newPosition;
    };
      
    this.setNrMatricola = function(newMatricola)
    {
      this.nrMatricola = newMatricola;
    };
      
    this.getOriginalQuantity = function()
    {
      return this.originalQuantity;
    };
      
    this.getMissingQuantity = function()
    {
      return this.missingQuantity;
    };
      
    this.getDefectQuantity = function()
    {
      return this.defectQuantity;
    };
      
    this.getToRenderBackQuantity = function()
    {
      return this.toRenderBackQuantity;
    };
      
    this.setOriginalQuantity = function(newQuantity)
    {
      this.originalQuantity = newQuantity;
    };
      
    this.setMissingQuantity = function(newQuantity)
    {
      this.missingQuantity = newQuantity;
    };
      
    this.setDefectQuantity = function(newQuantity)
    {
      this.defectQuantity = newQuantity;
    };
      
    this.setToRenderBackQuantity = function(newQuantity)
    {
      this.toRenderBackQuantity = newQuantity;
    };
      

    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

    if (serializedData) {
      this.update(serializedData);
    }
    return this;
  };
  return ProductAP;


})();
