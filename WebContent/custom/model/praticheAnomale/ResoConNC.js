jQuery.sap.declare("model.praticheAnomale.ResoConNC");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.praticheAnomale.ProductAP");
jQuery.sap.require("model.collections.ProductsAP");

model.praticheAnomale.ResoConNC = (function () {




  ResoConNC = function (serializedData) {

    this.renderType="Reso Con Nota Credito";
    this.orderId = undefined;
    this.shippmentId = undefined;
    this.committerId = undefined;
    this.committerName = undefined;
    this.payerName = undefined;
    this.receiverName = undefined;
    this.areaManager = undefined;
    this.territoryManager = undefined;
    this.billNumber = undefined;
    this.billDate = undefined;
    this.commercialInvoice = undefined;
    this.accountingInvoice = undefined;
    this.invoiceDate = undefined;
    this.renderReason = undefined;
    this.appointment = undefined;
    this.notes = undefined;
    this.notesText = undefined;
    this.confirmIntegrityFlag = undefined;
    this.productsAP = [];


    // this.initialize = function(newForm)
    // {
    //   this.renderType="Reso Con Nota Credito";
    //   this.orderId = newForm.orderId ? newForm.orderId : "";
    //   this.shippmentId = newForm.shippmentId ? newForm.shippmentId : "";
    //   this.committerName = newForm.committerName ? newForm.committerName : "";
    //   this.payerName = newForm.payerName ? newForm.payerName : "";
    //   this.receiverName = newForm.receiverName ? newForm.receiverName : "";
    //   this.areaManager = newForm.areaManager ? newForm.areaManager : "";
    //   this.territoryManager = newForm.territoryManager ? newForm.territoryManager : "";
    //   this.billNumber = newForm.billNumber ? newForm.billNumber : "";
    //   this.billDate = newForm.billDate ? newForm.billDate : "";
    //   this.commercialInvoice = newForm.commercialInvoice ? newForm.commercialInvoice : "";
    //   this.accountingInvoice = newForm.accountingInvoice ? newForm.accountingInvoice : "";
    //   this.invoiceDate = newForm.invoiceDate ? newForm.invoiceDate : "";
    //
    // };
    this.initialize = function(delivery, order)
    {

      this.orderId = delivery.orderId ? delivery.orderId : "";
      this.shippmentId = delivery.deliveryId ? delivery.deliveryId : "";
      this.committerName = delivery.destinationName ? delivery.destinationName : "";
      this.payerName = delivery.destinationName ? delivery.destinationName : "";
      this.receiverName = delivery.destinationName ? delivery.destinationName : "";
            // areaManager = user.organizationData.results[0].areaManager ? user.organizationData.results[0].areaManager : "";
      			// areaManagerText = user.organizationData.results[0].areaManagerText ? user.organizationData.results[0].areaManagerText : "";
            // territoryManager = user.organizationData.results[0].territoryManager ? user.organizationData.results[0].territoryManager : "";
      			// territoryManagerText = user.organizationData.results[0].territoryManagerText ? user.organizationData.results[0].territoryManagerText : "";
      this.areaManager = order.areaManager ? order.areaManager : "";

      this.territoryManager = order.territoryManager ? order.territoryManager : "";

      this.billNumber = delivery.bollaNum ? delivery.bollaNum : "";
      this.billDate = delivery.billDate ? delivery.billDate : "";
      this.commercialInvoice = delivery.commercialInvoice ? delivery.commercialInvoice : "";
      this.accountingInvoice = delivery.accountingInvoice ? delivery.accountingInvoice : "";
      this.invoiceDate = delivery.invoiceDate ? delivery.invoiceDate : ""


    };



    this.getRenderType = function () {
      return this.renderType;
    };

    this.getShippmentId = function () {
      return this.shippmentId;
    };

    this.getIntegrityFlagConfirmation = function () {
      return this.confirmIntegrityFlag;
    };

    this.setIntegrityFlagConfirmation = function (value) {
      this.confirmIntegrityFlag = value;
    };


    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);

      return model;

    };


    this.getProductsAP = function(shippmentId)
    {
        var thisDefer = Q.defer();
        model.collections.ProductsAP.getByShippmentId(shippmentId)
          .then(
            _.bind(function(result){
                this.productsAP = result;
                thisDefer.resolve(this.productsAP);
//                return this.productsAP;
            },this));
        return thisDefer.promise;
    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }


    };


    if (serializedData) {

      this.update(serializedData);


    }


    return this;
  };
  return ResoConNC;
  //return model.persistence.Serializer.resi.fromSAP(ResoConNC);


})();
