jQuery.sap.declare("model.praticheAnomale.Resi");
jQuery.sap.require("model.persistence.Serializer");



model.praticheAnomale.Resi = ( function ()
{
  var resi = [];
  
  
  return {
    
    getRenderReasons : function(req)
    {
      var defer = Q.defer();
      var fSuccess = function(result)
      {
        var res = model.persistence.Serializer.renderReasons.fromSAPItems(result);
        if(res && res.items){
        resi = res.items;
        }
        defer.resolve(resi);
      };
      var fError = function(err)
      {
        resi = [];
        defer.reject(err);
      };
      
      return defer.promise;
    },
    
    getAccollamentoCosti : function(req)
    { 
      var res = {};
      if(resi && resi.items)
      {
        res = _.find(res.items,{codice : req});
      }
      return res;
      
      
      
    },
    
      
    

    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"resi":[]};
      for(var i = 0; i< resi.length; i++)
      {
        data.resi.push(resi[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
