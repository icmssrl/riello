jQuery.sap.declare("model.praticheAnomale.Reso");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.praticheAnomale.ProductAP");
jQuery.sap.require("model.collections.ProductsAP");

model.praticheAnomale.Reso = (function () {




  Reso = function (serializedData) {

    this.renderType="";
    this.resoId="";
    this.society="";
    this.salesOrg="";
    this.distrCh="";
    this.division="";
    this.areaManager="";
    this.areaManagerText="";
    this.territoryManager="";
    this.territoryManagerText="";
    this.agentCode="";
    this.agentName="";
    
    this.customerId="";
    this.customerName="";
    this.customerAddress="";
    this.customerCountry="";
    this.customerPostCode="";
    this.customerCity="";
    this.customerTel="";
    
    this.buyerId="";
    this.buyerName="";
    this.buyerAddress="";
    this.buyerCountry="";
    this.buyerPostCode="";
    this.buyerCity="";
    this.buyerTel="";
    
    this.receiverId="";
    this.receiverName="";
    this.receiverAddress="";
    this.receiverCountry="";
    this.receiverPostCode="";
    this.receiverCity="";
    this.receiverTel="";
    
    
    
    this.orderId = "";//VbelVa?
    this.deliveryId = "";//VbelnVl
    
    

    this.billNumber = "";//Xabln
    this.billDate = "";//Fkdat?
    this.deliveryDate="";
    this.wareMoveDate="";
    this.billDoc ="";
    this.nrDoc="";
    this.year="";
    this.regDate="";
    this.docDate="";
    
    this.renderReason="";
    this.renderReasonText="";
    this.appointment="";//Sdabw;
    this.appointmentText="";
    this.textId="";
    this.textContent="";
    this.nrReg="";
    this.costs="";
    this.deliveryBlock="";
    this.invoiceBlock="";
    this.accountingInvoice="";
    this.commercialInvoice="";
    this.confirmIntegrityFlag=false;
    this.items=[];
    this.attachments=[];
    this.links=[];
    
//    this.commercialInvoice = undefined;
//    this.accountingInvoice = undefined;
//    this.invoiceDate = undefined;
//    this.renderReason = undefined;
//    this.appointment = undefined;
//    this.notes = undefined;
//    this.notesText = undefined;
//    this.newShippingAddress = {};
//    this.productsAP = [];


   
    
//    this.initialize = function(delivery, order)
//    {
//      this.renderType="Mancanza merce";
//      this.orderId = delivery.orderId ? delivery.orderId : "";
//      this.shippmentId = delivery.deliveryId ? delivery.deliveryId : "";
//      this.committerName = delivery.destinationName ? delivery.destinationName : "";
//      this.payerName = delivery.destinationName ? delivery.destinationName : "";
//      this.receiverName = delivery.destinationName ? delivery.destinationName : "";
//            // areaManager =user.organizationData.results[0].areaManager ? user.organizationData.results[0].areaManager : "";
//            // areaManagerText = user.organizationData.results[0].areaManagerText ? user.organizationData.results[0].areaManagerText : "";
//            // territoryManager = user.organizationData.results[0].territoryManager ? user.organizationData.results[0].territoryManager : "";
//            // territoryManagerText = user.organizationData.results[0].territoryManagerText ? user.organizationData.results[0].territoryManagerText : "";
//      this.areaManager = order.areaManager ? order.areaManager : "";
//
//      this.territoryManager = order.territoryManager ? order.territoryManager : "";
//
//      this.billNumber = delivery.bollaNum ? delivery.bollaNum : "";
//      this.billDate = delivery.billDate ? delivery.billDate : "";
//      this.commercialInvoice = delivery.commercialInvoice ? delivery.commercialInvoice : "";
//      this.accountingInvoice = delivery.accountingInvoice ? delivery.accountingInvoice : "";
//      this.invoiceDate = delivery.invoiceDate ? delivery.invoiceDate : ""
//
//
//    };



    this.getRenderType = function () {
      return this.renderType;
    };

//    this.setNewShippingAddress = function(newAddress) {
//
//          this.newShippingAddress.name1 = newAddress.name1 ? newAddress.name1 : "";
//          this.newShippingAddress.name2 = newAddress.name2 ? newAddress.name2 : "";
//          this.newShippingAddress.street = newAddress.street ? newAddress.street : "";
//          this.newShippingAddress.streetNumber = newAddress.streetNumber ? newAddress.streetNumber : "";
//          this.newShippingAddress.zipCode = newAddress.zipCode ? newAddress.zipCode : "";
//          this.newShippingAddress.city = newAddress.city ? newAddress.city : "";
//          this.newShippingAddress.country = newAddress.country ? newAddress.country : "";
//          this.newShippingAddress.region = newAddress.region ? newAddress.region : "";
//          this.newShippingAddress.mail = newAddress.mail ? newAddress.mail : "";
//          this.newShippingAddress.phone = newAddress.phone ? newAddress.phone : "";
//          this.newShippingAddress.fax = newAddress.fax ? newAddress.fax : "";
//          this.newShippingAddress.contactPerson = newAddress.contactPerson ? newAddress.contactPerson : "";
//
//    };


    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);

      return model;

    };
    
    this.load=function(order, delivery)
    {
    	var defer = Q.defer();
    	
//    	var req = {
//    			"VbelnCr":this.resoId,
//    			"Bukrs":this.society ? this.society : order.society,
//    			"Vkorg":this.salesOrg ? this.salesOrg : order.salesOrg,
//    			"Vtweg":this.distrCh ? this.distrCh : order.distrCh,
//    			"Spart":this.division ? this.division : order.division,
//    			"Cdage":order.agentCode,
//    			"Vkbur":this.areaManager? this.areaManager : order.areaManager,
//    			"Vkgrp":this.territoryManager ? this.territoryManager : order.territoryManager,
//    			"Auart":this.renderType,
//    			"Xabln":delivery.bollaNum,
//    			"VbelnVl":delivery.deliveryId,
//    			"Nmatr":"",
//    	}
    	var req = {
    			"VbelnCr":this.resoId,
    			"Bukrs":this.society ? this.society : order.society,
    			"Vkorg":this.salesOrg ? this.salesOrg : order.salesOrg,
    			"Vtweg":this.distrCh ? this.distrCh : order.distrCh,
    			"Spart":this.division ? this.division : order.division,
    			"Cdage":(order && order.agentCode)? order.agentCode : this.agentCode,
    			"Vkbur":this.areaManager? this.areaManager : order.areaManager,
    			"Vkgrp":this.territoryManager ? this.territoryManager : order.territoryManager,
    			"Auart":this.renderType,
    			"Xabln":"",
    			"VbelnVl":"",
    			"Nmatr":"",
    	}
    	var fSuccess = function(result)
    	{
    		var reso = model.persistence.Serializer.reso.fromSAP(result);
    		this.update(reso);
    		defer.resolve(this);
    	}
    	fSuccess = _.bind(fSuccess,this);
    	
    	var fError = function(err)
    	{
    		var error = utils.Message.getError(err);
    		defer.reject(error);
    	}
    	fError = _.bind(fError, this);
    	
    	
    	model.odata.chiamateOdata.getPracticeByParameters(req, fSuccess, fError, true);
    	
    	return defer.promise;
    };
    this.initializeByDelivery=function(delivery, order, type)
    {
    	var defer = Q.defer();
    	
    	var req = {
    			"VbelnCr":"",
    			"Bukrs":order.society,
    			"Vkorg":order.salesOrg,
    			"Vtweg":order.distrCh,
    			"Spart":order.division,
    			"Cdage":order.agentCode,
    			"Vkbur":order.areaManager,
    			"Vkgrp":order.territoryManager,
    			"Auart":type,
    			"Xabln":"",
    			"VbelnVl":delivery.deliveryId,
    			"Nmatr":"",
    	}
    	var fSuccess = function(result)
    	{
    		var reso = model.persistence.Serializer.reso.fromSAP(result);
    		this.update(reso);
    		defer.resolve(this);
    	}
    	fSuccess = _.bind(fSuccess,this);
    	
    	var fError = function(err)
    	{
    		var error = utils.Message.getError(err);
    		defer.reject(error);
    	}
    	fError = _.bind(fError, this);
    	
    	
    	model.odata.chiamateOdata.getPracticeByParameters(req, fSuccess, fError, true);
    	
    	return defer.promise;
    };
//    this.initialize = function(order, delivery, renderType, id)
//    {
//    	 this.renderType= renderType;
//    	    this.resoId=id;
//    	    this.society=order.society;
//    	    this.salesOrg=order.salesOrg;
//    	    this.distrCh=order.distrCh;
//    	    this.division=order.division;
//    	    this.areaManager=order.areaManager;
//    	    this.areaManagerText="";
//    	    this.territoryManager=order.territoryManager;
//    	    this.territoryManagerText="";
//    	    this.agentCode=order.agentCode;
//    	    this.agentName=order.agentName;
//    	    
//    	    this.customerId=order.customerId;
//    	    this.customerName=order.customerName;
//    	    this.customerAddress=delivery.destinationStreet;;
//    	    this.customerCountry=delivery.destinationCountry;;
//    	    this.customerPostCode=delivery.destinationPostCode;
//    	    this.customerCity=delivery.destinationCity;
//    	    this.customerTel="";
//    	    
//    	    this.buyerId=order.customerId;
//    	    this.buyerName=order.customerName;
//    	    this.buyerAddress=delivery.destinationStreet;
//    	    this.buyerCountry=delivery.destinationCountry;
//    	    this.buyerPostCode=delivery.destinationPostCode;
//    	    this.buyerCity=delivery.destinationCity;
//    	    this.buyerTel="";
//    	    
//    	    this.receiverId=delivery.destination;
//    	    this.receiverName=delivery.destinationName;
//    	    this.receiverAddress=delivery.destinationStreet;
//    	    this.receiverCountry=delivery.destinationCountry;
//    	    this.receiverPostCode=delivery.destinationPostCode;
//    	    this.receiverCity=delivery.destinationCity;
//    	    this.receiverTel="";
//    	    
//    	    
//    	    
//    	    this.orderId = order.orderId;//VbelVa?
//    	    this.deliveryId = delivery.deliveryId;//VbelnVl
//    	    
//    	    
//
//    	    this.billNumber = delivery.bollaNum;//Xabln
//    	    this.billDate = order.orderCreationDate;//Fkdat?
//    	    this.deliveryDate=delivery.deliveryDate;
//    	    this.wareMoveDate=delivery.goodsMov;
//    	    this.billDoc ="";
//    	    this.nrDoc="";
//    	    this.year=(new Date()).getFullYear();
//    	    this.regDate="";
//    	    this.docDate="";
//    	    
//    	    this.renderReason="";
//    	    this.renderReasonText="";
//    	    this.appointment="";//Sdabw;
//    	    this.appointmentText="";
//    	    this.textId="";
//    	    this.textContent="";
//    	    this.nrReg="";
//    	    this.costs="";
//    	    this.deliveryBlock="";
//    	    this.invoiceBlock="";
//    	    
//    	    this.accountingInvoice="";
//    	    this.commercialInvoice="";
//    	    
//    	    this.items=[];
//    	
//    };
    this.initialize = function(practice)
    {
    	this.resoId=practice.resoId;
    	this.society = practice.society;
    	this.salesOrg = practice.salesOrg;
    	this.distrCh = practice.distrCh;
    	this.division=practice.division;
    	this.areaManager = practice.areaManager;
    	this.territoryManager = practice.territoryManager;
    	this.resoDate = practice.resoDate;
    	this.renderType=practice.renderType;
    	this.renderTypeText = practice.renderTypeText;
    	this.renderReason = practice.renderReason;
    	this.renderReasonText= practice.renderReasonText;
    	this.agentCode = practice.agentCode? practice.agentCode : "";
    };
    this.sendToSAP=function()
    {
    	var defer= Q.defer();
    	
    	var req = model.persistence.Serializer.reso.toSAP(this);
    	//console.log("-----RESO REQUEST-----");
    	//console.log(req);
    	var fSuccess=function(result)
    	{
    		//console.log("----RESO RESULT-----");
    		//console.log(result);
    		var serializedRes = model.persistence.Serializer.reso.fromSAP(result);
    		this.update(serializedRes);
    		defer.resolve(this);
    	}
    	fSuccess = _.bind(fSuccess, this);
    	
    	var fError = function(err)
    	{
    		defer.reject(err);
    	}
    	fError = _.bind(fError, this);
    	
    	model.odata.chiamateOdata.resoCreate(req, fSuccess, fError);
    	return defer.promise;
    };
    this.checkPractice = function(reqCost)
    {
    	var res = {"success": true, "errors":[]}
    	var propToCheck = ["renderReason", "appointment"];
    	
    	if(reqCost)
    		propToCheck.push("costs");
    	
    	for(var i = 0; i< propToCheck.length; i++)
    	{
    		if(!this[propToCheck[i]])
    		{
    			res.success = false;
    			res.errors.push(propToCheck[i]);
    		}
    	}
    	if(!(_.find(this.items, function(item){return item.quantityToRender > 0})))
    	{
    		res.success=false;
    		res.errors.push("noItemRendered");
    	}
    	
    	return res;
    	
    	
    };
    
    
    this.loadLinks = function () {
        var defer = Q.defer();
        var req = {"Vbeln":this.resoId};
        var fSuccess = function (result) {
          if (result && result.results && result.results.length > 0)
          {
            this.links=[];
             for(var i = 0; i< result.results.length ; i++)
             {
               var link = model.persistence.Serializer.link.fromSAP(result.results[i]);
              //  link.url= icms.Component.getMetadata().getConfig().settings.serverUrl+"SO_ContentAttachSet(IObjId='"+link.attachId+"')/$value";
              link.url= sessionStorage.getItem("serverUrl")+"SO_ContentAttachSet(IObjId='"+link.attachId+"')/$value";

              this.links.push(link);


             }
          }
          defer.resolve(this);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
          this.links=[];
          sap.m.MessageToast.show("Error Loading Links of " + this.orderId);
          defer.reject(err);
        }
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.loadAttachmentLinks(req, fSuccess, fError);

        // .then(fSuccess, fError);

        return defer.promise;
      };

//    this.getProductsAP = function(shippmentId)
//    {
//        var thisDefer = Q.defer();
//        model.collections.ProductsAP.getByShippmentId(shippmentId)
//          .then(
//            _.bind(function(result){
//                this.productsAP = result;
//                thisDefer.resolve(this.productsAP);
////                return this.productsAP;
//            },this));
//        return thisDefer.promise;
//    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }


    };


    if (serializedData) {

      this.update(serializedData);


    }


    return this;
  };
  return Reso


})();