jQuery.sap.declare("model.Current");
jQuery.sap.require("model.Customer");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.HistoryCart");
jQuery.sap.require("model.persistence.Storage");

//module to store and recover current entities

model.Current = (function () {

  var customer;
  var order;
  var historyCart;

  var setCurrentCustomer = function (c) {
    customer = c;
  };

  var getCurrentCustomer = function () {
    if (!customer) {
      var customerData = model.persistence.Storage.session.get("currentCustomer");
      if (customerData) {
        customer = new model.Customer(customerData);
      } else {
        customer = null;
      }
    }
    return customer;
  };
  var setCurrentOrder = function (o) {
    order = o;
    model.persistence.Storage.session.save("currentOrder", order);
  };
  var getCurrentOrder = function () {
    if (!order) {
      var orderData = model.persistence.Storage.session.get("currentOrder");
      if (orderData) {
        order = new model.Order(orderData);
      } else {
        order = null;
      }
    }
    return order;
  };

  var setCurrentHistoryCart = function (h) {
    historyCart = h;
    model.persistence.Storage.session.save("currentHistoryCart", h);
  };


  var getCurrentHistoryCart = function () {
    if (!historyCart) {
      var historyCartData = model.persistence.Storage.session.get("currentHistoryCart");
      if (historyCartData) {
        // historyCart = new model.Order(historyCartData);
        historyCart = new model.HistoryCart(historyCartData);
      } else {
        historyCart = null;
      }
    }
    return historyCart;
  };

  var removeOrder = function () {
    model.persistence.Storage.session.remove("currentOrder");
    order = undefined;
  };

  var removeCustomer = function () {
    model.persistence.Storage.session.remove("currentCustomer");
    customer = undefined;
  };

  var removeHistoryCart = function () {
    model.persistence.Storage.session.remove("currentHistoryCart");
    historyCart = undefined;
  };
  
  var sessionOrder = function(){
	  var obj = getCurrentOrder();
	  model.persistence.Storage.session.save("currentOrder", obj);
	  
  }

  return {
    setCustomer: setCurrentCustomer,
    setOrder: setCurrentOrder,
    getCustomer: getCurrentCustomer,
    getOrder: getCurrentOrder,
    removeOrder : removeOrder,
    keepOrder: sessionOrder,
    removeCustomer: removeCustomer,
    getHistoryCart: getCurrentHistoryCart,
    setHistoryCart: setCurrentHistoryCart,
    removeHistoryCart: removeHistoryCart

  };
})();
