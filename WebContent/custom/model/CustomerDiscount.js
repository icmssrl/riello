jQuery.sap.declare("model.CustomerDiscount");

jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.CustomerStatuses");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.NetworkStatus");

//Library to use to set the customer discount value and status;
model.CustomerDiscount = function (){
		
		this._discount = {"value":"", "status":"", "statusDescr":""};
		
		this.initialize= function(value, status)
		{
			this._discount.value = value;
			(status)? this._setStatus(status) : this._setStatus("INS");
			
			
		};
		
		this.setDiscount=function(user, value)
		{
			if(!this._discount.value)
			{
				console.log("discount must be initialize first");
				return;
			}
				
			
			if(user.type =="AG")
			{
				if(!value)
					return null;
				else
				{
					this._discount.value = value;
					this._setStatus("INS");
					
				}
			}
			else if(user.type == "TM")
			{
				if(!value || (this._discount.value == value))
				{
					this._setStatus("ATM");
				}
				else
				{
					this._discount.value = value;
					this._setStatus("MTM");
					
				}
			}
			else if(user.type == "AM")
			{
				if(!value || (this._discount.value == value))
				{
					this._setStatus("APP");
				}
				else
				{
					this._discount.value = value;
					this._setStatus("MPP");
					
				}
			}
		};
		
		this._setStatus = function(status)
		{
			this._discount.status=status;
			this._discount.statusDescr = this.getStatusDescription(status);
		};
		
		this.getStatusDescription = function(status)
		{
			if(!status)
			{
				console.log("Error: status undefined");
				return null;
			}
			
			return model.NetworkStatus.getStatusDescr(status, "A");
//			var result = "";
			
//			switch(status){
//			
//			case "AAM" : 
//				result = model.i18n.getText("Approvato AM");
//				
//				break;
//				
//			case "MAM":
//				result=model.i18n.getText("Approvato AM Con Modifiche");
//				break;
//			
//			case "ATM":
//				result=model.i18n.getText("Approvato TM");
//				break;
//			
//			case "MTM":
//				result=model.i18n.getText("Approvato TM Con Modifiche");
//				break;
//			
//			case "INS":
//				result=model.i18n.getText("Inserito");
//				break;
//				
//				
//			}
//			return result;
		};
		this.getDiscountValue=function()
		{
			return this._discount.value;
		};
		this.getDiscountStatus=function()
		{
			return this._discount.status;
		};
		this.getDiscountStatusDescr=function()
		{
			return this._discount.statusDescr;
		};
		this.isEditable= function(user)
		{
			if(!this._discount.status)
			{
				console.log("discount must be initialize first");
				return null;
			}
			if(user.type=="AG" || user.type == "TM")
			{
				if(this._discount.status !== "INS")
					return false;
				else
					return true;
			}
			else if(user.type=="AM")
			{
				if(this._discount.status == "INS")
					return false;
				else
					return true;
			}
		};
		
	
		
		
		
		
}


