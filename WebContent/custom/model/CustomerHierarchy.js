jQuery.sap.declare("model.CustomerHierarchy");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.CustomerHierarchyNode");
jQuery.sap.require("model.collections.CustomerHierarchies");

model.CustomerHierarchy = ( function () {

  CustomerHierarchy = function() {

   
    this.hierarchy = {"items":[]};


    this.getHierarchy = function()
    {

          var hDefer = Q.defer();
          var fSuccess = function(result)
          {
            this.hierarchy.items  = result;
            //To set hierarchyData as preferred
            hDefer.resolve(this.hierarchy);
          };
          fSuccess = _.bind(fSuccess, this);
          var fError = function(err)
          {
            this.hierarchy.items =[];
            hDefer.reject(err);
          };
          fError = _.bind(fError, this);

          model.collections.Hierarchies.loadHierarchies()
          .then(fSuccess, fError);


      return hDefer.promise;
    };


      
    this.getChildNodeItems = function(parentId)
    {
        var items = this.hierarchy.items;
        var nodeArr = [];
        for(var i = 0; i < items.length; i++){
            for(var j = 0; j < items[i].items.length; j++){
                nodeArr.push(items[i].items[j]);
            }            
        }
        var filterArr = [];
        var foundParentId;
        nodeArr.map(function(obj){     
                            if (obj.productId === parentId) 
                            //filterArr.push(obj);
                            foundParentId = obj.parentId;
                        });
        nodeArr.map(function(obj){     
                            if (obj.parentId === foundParentId) 
                            filterArr.push(obj);
                        });
        

        return {"items":filterArr};
    };

    this.getProducts = function(parentId)
    {

        var pDefer = Q.defer();
        var pSuccess = function(result)
        {
            

          pDefer.resolve({"items":result});
        };
        pSuccess = _.bind(pSuccess, this);

        var pError = function(err)
        {
          this.hierarchy = [];
          pDefer.reject(err);
        };
        pError = _.bind(pError, this);

        model.collections.Products.loadProducts(parentId)
        .then(pSuccess, pError);

      return pDefer.promise;


    };

    this.getModel = function()
    {
      var defer = Q.defer();
      var model = new sap.ui.model.json.JSONModel();

        model.setData({items : this.hierarchy.items});


      return model;
    };



    return this;
  };

 

  return CustomerHierarchy;
})();
