
jQuery.sap.declare("model.BlockingOrderReason");



model.BlockingOrderReason = ( function ()
{

     
          
    BlockingOrderReason = function(data)
      {
        this.orderId  = undefined;
        this.position = undefined;
        this.blockCode = undefined;
        this.blockType = undefined;
        this.blockDescription = undefined;
        this.blockerName = undefined;
        this.blockDate = undefined;
        this.releaserName = undefined;
        this.releaseDate = undefined;
        this.releaseTime = undefined;
        

        this.getId = function(){
            return this.orderId;
        };
        
        this.update = function(data)
        {
          for(var prop in data)
          {
            this[prop]= data[prop];
          }
        };


        this.getModel = function()
        {
          var model = new sap.ui.model.json.JSONModel(this);
          return model;
        };


      if(data)
        this.update(data);

      return this;
    }
    
    return BlockingOrderReason;
        


})();
