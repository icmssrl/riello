jQuery.sap.declare("model.Discount");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("sap.m.MessageToast");

model.Discount = (function () {

  Discount = function (serializedData) {

    //proprietà dell'oggetto discount
    //I suppose every element ( with exception of productId, orderId, positionId) an object composed by name, unitVal, scale, totValue, currency
    this.orderId = "";
    this.positionId = "";
    this.productId = "";
    this.price = {};
    this.currency = "EUR";
    this.scale = "%";

//    this.price.currency = this.currency;
//    this.price.scale = "EUR";
//
//    this.agentDiscount = {};
//    this.agentDiscount.currency = this.currency;
//    this.agentDiscount.scale = this.scale;
//
//    this.firstLocDiscount = {};
//    this.firstLocDiscount.currency=this.currency;
//    this.firstLocDiscount.scale = this.scale;
//
//
//    this.price.name = model.i18n.getText("LISTPRICE");
//
//    this.agentDiscount.name = model.i18n.getText("AGENTDISCOUNT");
//
//    this.firstLocDiscount.name = model.i18n.getText("FIRSTLOCDISCOUNT");
//
//    this.secondLocDiscount = {};
//    this.secondLocDiscount.currency=this.currency;
//    this.secondLocDiscount.scale = this.scale;
//
//    this.secondLocDiscount.name = model.i18n.getText("SECONDLOCDISCOUNT");
//
//    this.allegedCommRC = {};
//    this.allegedCommRC.name = model.i18n.getText("ALLEGEDCOMMRC");
//
//    this.addTraspRC = {};
//    this.addTraspRC.name = model.i18n.getText("ADDTRASPRC");
//
//    this.addTraspAutomRC = {};
//    this.addTraspAutomRC.name = model.i18n.getText("ADDTRASPAUTOMRC");
//
//    this.IVA = {};
//    this.IVA.name = model.i18n.getText("IVA");
//
//    this.thirdLocDiscount = {};
//    this.thirdLocDiscount.currency = this.currency;
//    this.thirdLocDiscount.scale = this.scale;
//
//    this.thirdLocDiscount.name = model.i18n.getText("THIRDLOCDISCOUNT");
    this.quantity = 0;

    //-------------NEW--------------------------------------------------------

    this.discountArray = [];








    //-----------------------------------------------------------------------
//    var calculateValues = function (quantity) {
//
//      //Maybe it's not needed
//      if (quantity)
//        this.quantity = parseInt(quantity);
//
//      if (!this.quantity)
//        this.quantity = 0;
//
//      this.price.totValue = parseFloat(this.price.unitVal) * parseInt(this.quantity);
//      this.agentDiscount.totValue = this.price.totValue * (parseFloat(this.agentDiscount.unitVal) / 100);
//      this.firstLocDiscount.totValue = (this.price.totValue - this.agentDiscount.totValue) * (parseFloat(this.firstLocDiscount.unitVal) / 100);
//      this.secondLocDiscount.totValue = (this.price.totValue - this.agentDiscount.totValue - this.firstLocDiscount.totValue) * (parseFloat(this.secondLocDiscount.unitVal) / 100);
//      this.thirdLocDiscount.totValue = (this.price.totValue - this.agentDiscount.totValue - this.firstLocDiscount.totValue - this.secondLocDiscount.totValue) * (parseFloat(this.thirdLocDiscount.unitVal) / 100);
//
//    };
//    this.setQuantity = function(quantity)
//    {
//      var updateValues = _.bind(calculateValues, this);
//      updateValues(quantity);
//    };

//    this.getAppliedDiscounts = function()
//    {
//        return {
//          "agentDiscount":this.agentDiscount,
//          "firstLocDiscount": this.firstLocDiscount,
//          "secondLocDiscount":this.secondLocDiscount,
//          "thirdLocDiscount":this.thirdLocDiscount
//        };
//    };
//    this.assignDiscountsValues = function(values)
//    {
//      this.agentDiscount = _.clone(values.agentDiscount);
//      this.firstLocDiscount = _.clone(values.firstLocDiscount);
//      this.secondLocDiscount = _.clone(values.secondLocDiscount);
//      this.thirdLocDiscount = _.clone(values.thirdLocDiscount);
//    };

    this.initialize = function (orderId, position) {

      this.orderId = orderId;
      this.positionId = position.getId();
      this.productId = position.getProductId();
      this.quantity = position.getQuantity();
      this.price.unitVal = position.product.unitListPrice;

      // var defer = Q.defer();
      //
      // //It will be placed in a corresponding odata File as loadDiscount
      // var fSuccess = function (result) {
      //   var data = {};
      //   if (result && result.length > 0) {
      //     for (var i = 0; i < result.length; i++) {
      //       data = model.persistence.Serializer.discount.fromSAP(result[i]);
      //       if (data.productId === this.productId)
      //         break;
      //     }
      //     if (data.productId !== this.productId)
      //       defer.reject();
      //     else {
      //       this.update(data);
      //       // //Just to comprehend the wrong behaviour---------------
      //       // this.orderId = orderId;
      //       // this.positionId = position.getId();
      //       // this.productId = position.getProductId();
      //       // this.quantity = position.getQuantity();
      //       // this.price.unitVal = position.product.unitListPrice;
      //       // //----------------------------------------------------------
      //       var calculate = _.bind(calculateValues, this);
      //       calculate();
      //       defer.resolve(this);
      //     }
      //   } else {
      //     defer.reject();
      //   }
      //
      //
      // };
      // fSuccess = _.bind(fSuccess, this);
      //
      // var fError = function (err) {
      //   sap.m.MessageToast.show("Error loading Discount Data");
      //   defer.reject(err);
      // }
      // fError = _.bind(fError, this);
      //
      // $.getJSON("custom/model/mock/data/discount.json")
      //   .success(fSuccess)
      //   .fail(fError);
      //
      // return defer.promise;
      //--------------------------------------------------------------------------


    };


    this.setPosition = function(position)
    {
      this.orderId = position.orderId;
      this.positionId = position.getId();
      this.productId = position.getProductId();
      this.quantity = position.getQuantity();
      //this.price.unitVal = position.product.unitListPrice;
    };
    this.setForcedPrice= function(value)
    {
    	var forcedPriceCond = _.find(this.discountArray, {typeId:"ZPMA"});
    	if(!forcedPriceCond)
    	{
    		sap.m.MessageToast.show("Error: impossible assign a forced price");
    		return;
    	}
    	forcedPriceCond.value = value;
    };
    




    //
    //    this.getId =  function()
    //    {
    //      return this.materialCode;
    //    };
    //

    //This model is adapted to conform Discount Dialog
//    this.getModel = function () {
//
//      var model = new sap.ui.model.json.JSONModel();
//
//      var values = [];
//      values.push(this.price);
//      values.push(this.agentDiscount);
//      values.push(this.firstLocDiscount);
//      values.push(this.secondLocDiscount);
//      values.push(this.thirdLocDiscount);
//      model.setData({
//        "values": values,
//        "ref": this
//      });
//
//
//      return model;
//
//    };
    this.getModel_new = function () {

      var model = new sap.ui.model.json.JSONModel();

      model.setData(this);


      return model;

    };

    this.getDiscountValues = function(product, customer)
    {
      var defer = Q.defer();

      var data = {
        "IVkorg":customer.registry.salesOrg,
        "IVtweg":customer.registry.distrCh,
        "ISpart":customer.registry.division,
        "IWerks":"",
        "IVkbur":customer.registry.areaManager,
        "IVkgrp":customer.registry.territoryManager,
        "ICdage":customer.registry.agentCode,
        "IKunag":customer.registry.id,
        "IKunwe":"",
        "IMatnr":product.productId,
        "IMenge":product.quantity,
        "IVrkme":"",
        "ITaxk1":""
      };

      var fSuccess = function(result)
      {
        //console.log(result);
        // this.discountArray = [];
        // this.discountArray = model.persistence.Serializer.discount.fromSAPItemsToSet(result);
        // _.forEach(this.discountArray, _.bind(function(item){
        //   item.positionId=this.positionId;
        // }, this));
        this.setDiscountItems(result);
        defer.resolve(this);
      }
      var fError = function(error)
      {
        //console.log(error);
        defer.reject(error);
      }
      fSuccess= _.bind(fSuccess, this);
      fError = _.bind(fError, this);


      model.odata.chiamateOdata.getProductPriceDetail(data, fSuccess, fError, true);

      return defer.promise;

    };

    this.setDiscountItems = function(data)
    {
//      this.discountArray = [];
//      this.discountArray = model.persistence.Serializer.discount.fromSAPItemsToSet(data);
//      _.forEach(this.discountArray, _.bind(function(item){
//        item.positionId=this.positionId;
//      }, this));
    	
    	var discountArray = [];
        discountArray = model.persistence.Serializer.discount.fromSAPItemsToSet(data);
        for(var i= 0; this.discountArray && this.discountArray.length> 0 && i < this.discountArray.length ; i++)
        {
        	var discountItem = _.find(discountArray, {typeId: this.discountArray[i].typeId });
        	if(discountItem)
        		discountItem.value = this.discountArray[i].value;
        }
        this.discountArray = discountArray;
        _.forEach(this.discountArray, _.bind(function(item){
          item.positionId=this.positionId;
        }, this));
    };

    this.refreshDiscountPrices = function(discounts)
    {
      if(!discounts || discounts.length <= 0)
        return;

      if(!this.discountArray || this.discountArray.length<=0)
      {
        this.discountArray = [];
      }
      for(var i = 0; i< discounts.length; i++)
      {
//        var item = _.find(this.discountArray, {typeId: discounts[i].Kschl});
//        if(!item)
//        {
//          this.discountArray.push(model.persistence.Serializer.discount.fromSAP(discounts[i]));
//        }
//        else
//        {
//            item = model.persistence.Serializer.discount.fromSAP(discounts[i]);
//        }
    	  var found = false;
    	  for(var j =0; j< this.discountArray.length; j++)
	    	{
	    		  if(this.discountArray[j].typeId == discounts[i].Kschl)
	    			  {
	    			  found = true;
	    			  var item = model.persistence.Serializer.discount.fromSAP(discounts[i]);
	    			  this.discountArray[j].value = item.value;
	    			  this.discountArray[j].total = item.total;
	    			  this.discountArray[j].price = item.price;
	    	         
	    			  break;
	    			  }
	    	}
    	  if(!found)
    		  {
    		  this.discountArray.push(model.persistence.Serializer.discount.fromSAP(discounts[i]));
    		  }
      }
    };

//    this.refreshModel = function (quantity) {
//      var calculate = _.bind(calculateValues, this);
//      calculate(quantity);
//      return this.getModel();
//    };

    this.update = function (data) {
      for (var prop in data) {
        if (_.isObject(this[prop])) {
          _.merge(this[prop], data[prop]);
        } else {
          this[prop] = data[prop];
        }
      }
    };
    
    
    //-------------------PART TO COPY DISCOUNT VALUES----------------------------
    
    this.copyDiscount = function(src)//src is a model.Discount
    {
    	if(!src.discountArray || src.discountArray.length == 0)
    	{
    		//console.log("No Discount Items on Src Discount");
    		return;
    	}
    	var srcEditableItems = src.getEditableItems();
    	if(srcEditableItems && srcEditableItems.length >0)
    	{
    		for(var i = 0; i< srcEditableItems.length; i++)
    		{
    			if(this.isEditableItem(srcEditableItems[i].typeId) && srcEditableItems[i].typeId !== "ZPMA")
    			{
    				var item = this.getItem(srcEditableItems[i].typeId);
    				item.value = srcEditableItems[i].value;
    				item.total="";
    			}
    		}
    	}
    	   	
    };
    
    this.getItem = function(type)
    {
    	var item = _.find(this.discountArray, {typeId:type});
    	return item;
    };
    
    this.isEditableItem = function(type)
    {
    	var item = this.getItem(type);
    	if(!item)
    	{
    		//console.log("Item type "+type+"not found");
    		return false;
    	}
    	return item.isEditable;
    };
    this.getEditableItems = function()
    {
    	var result = [];
    	if(!this.discountArray || this.discountArray.length == 0)
    	{
    		return result;
    	}
    	for(var i = 0; i<this.discountArray.length ; i++)
    	{
    		if(this.discountArray[i].isEditable)
    		{
    			result.push(this.discountArray[i]);
    		}
    	}
    	return result;
    	
    };
    //---------------------------------------------------------------------------------------
    
    
    if (serializedData) {
      this.update(serializedData);

    }

    return this;
  };
  return Discount;


})();
