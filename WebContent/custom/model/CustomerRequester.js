jQuery.sap.declare("model.CustomerRequester");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.CustomerStatuses");

model.CustomerRequester = (function(){

  CustomerRequester = function(userInfo) {

    this.parameters = {};

    if(userInfo)
    {
      this.parameters = {
        "Uname" : userInfo.username,
        "Ustyp" : userInfo.userType,
        "Bukrs" : userInfo.society,
        "Vkorg" : userInfo.salesOrg,
        "Vtweg" : userInfo.distrCh,
        "Spart" : userInfo.division,
        "Vkbur" : userInfo.areaManager,
        "Vkgrp" : userInfo.territoryManager,
        "Cdage" : userInfo.agentCode
      };
    }

    this.setParameters = function(params)
    {
      this.parameters.Uname = params.Uname ? params.Uname : "";
      this.parameters.Ustyp = params.Ustyp ? params.Ustyp: "";
      this.parameters.Bukrs = params.Bukrs ? params.Bukrs : "";
      this.parameters.Vkorg = params.Vkorg ? params.Vkorg : "";
      this.parameters.Vtweg = params.Vtweg ? params.Vtweg : "";
      this.parameters.Spart = params.Spart ? params.Spart : "";
      this.parameters.Vkbur = params.Vkbur ? params.Vkbur : "";
      this.parameters.Vkgrp = params.Vkgrp ? params.Vkgrp : "";
      this.parameters.Cdage = params.Cdage ? params.Cdage : "";

    };
    //
    //
    this.getParameters = function()
    {
      return this.parameters;
    };
    this.hasAgent = function()
    {
      return !_.isEmpty(this.parameters.Cdage);
    };
    //
    this.hasTerritoryManager = function()
    {
      return !_.isEmpty(this.parameters.Vkgrp);
    };
    //
    this.setTerritoryManager = function (value)
    {
      if(this.parameters.Ustyp === "AM")
        this.parameters.Vkgrp = value;
    };
    this.removeTerritoryManager = function()
    {
      if(this.parameters.Ustyp === "AM")
        this.parameters.Vkgrp = "";

    };

    this.setAgent = function(value)
    {
      if(this.parameters.Ustyp !== "AG")
      {
        this.parameters.Cdage = value;
      }
    };
    this.removeAgent = function()
    {
      if(this.parameters.Ustyp !== "AG")
      {
        this.parameters.Cdage = "";
      }
    };

    this.loadCustomersList = function ()
    {
    //
      var defer = Q.defer();
      utils.Busy.show();
      var fSuccess = function(result)
      {
        utils.Busy.hide();
        defer.resolve(result);
      };
      fSuccess = _.bind(fSuccess, this);
      var fError  = function(err)
      {
        utils.Busy.hide();
        defer.reject(err);
      };
      fError = _.bind(fError, this);
      model.collections.Customers.loadODataCustomers(this.parameters)
        .then(fSuccess, fError);

      return defer.promise;
    };

    this.loadCustomerDetail= function (clientID)
    {
    //
      var infos = {
        "Kunnr": clientID,
        "Bukrs":this.parameters.Bukrs,
        "Vkorg":this.parameters.Vkorg,
        "Vtweg":this.parameters.Vtweg,
        "Spart":this.parameters.Spart,
        "Cdage":this.parameters.Cdage
      };


      var defer = Q.defer();
      utils.Busy.show();
      //**
       var getStatus = function(id, result,defer){return model.collections.CustomerStatuses.getById(id, result,defer);};
        getStatus = _.bind(getStatus, this);
      //**
      var fSuccess = function(result)
      {
        //utils.Busy.hide();

        //**
        var id = result.getId();
        getStatus(id, result,defer);
        //defer.resolve(result);
        //**
      };
      fSuccess = _.bind(fSuccess, this);
      var fError  = function(err)
      {
        utils.Busy.hide();
        defer.reject(err);
      };
      fError = _.bind(fError, this);

      model.collections.Customers.getCustomerDetail(infos)
        .then(fSuccess, fError);

      return defer.promise;
    };

    return this;

  };

  return CustomerRequester;

})();
