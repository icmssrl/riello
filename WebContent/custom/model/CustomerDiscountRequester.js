jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.Customer");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.CustomerDiscount");
jQuery.sap.require("utils.Asynchronous");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.NetworkStatus");
jQuery.sap.declare("model.CustomerDiscountRequester");
//This function create an instance of a customer Discount Requester from the req Data of the customer needed for the odata request only (salesOrg, agent, ..)
model.CustomerDiscountRequester = function (customer) {
    this.maxCustomerDiscount = customer.maxCustomerDiscount ? customer.maxCustomerDiscount : 100;
    this.proposedValue = null;
    this.proposedNote = customer.proposedValue ? customer.proposedValue : "";
    this.status = customer.discountStatus ? customer.discountStatus : "";
    this.note = "";
    this.value = customer.getCustomerDiscountValue();
    this.customer = customer;
    this._discounts = [];
    this.arrayDiscount = {
        "results": []
    };
    //this.network = [];
    this.initialize = function (customerDiscountItem) {
        for (var prop in customerDiscountItem) {
            this[prop] = customerDiscountItem[prop];
        }
    };
    if (this.customer.getCustomerDiscount()) this.initialize(this.customer.getCustomerDiscount());
    this.loadMaxCustomerDiscountValue = function (user, boolApprove) {
        var defer = Q.defer();
        this.u = user;
        var fSuccess = function (res) {
            if (this.u.type !== "AG") {
                this.maxCustomerDiscount = (parseInt(res.Kbetr) > 0) ? parseInt(res.Kbetr) : 100;
            }
            else {
                this.maxCustomerDiscount = (parseInt(res.Kbetr) > 0) ? parseInt(res.Kbetr) : 100;;
            }
            //this.customer.setMaxCustomerDiscountValue(res.maxVal);
            //this.customer.maxCustomerDiscount = res.maxVal;
            defer.resolve(this);
        }
        fSuccess = _.bind(fSuccess, this);
        var fError = function (err) {
            this.maxCustomerDiscount = -1;
            defer.reject(err);
        }
        fError = _.bind(fError, this);
        
        
        var req = {
            "Bukrs": this.customer.registry.society
            , "Kappl": "V"
            , "Prsdt": utils.ParseDate().formatDate(new Date(), "yyyy-MM-ddT00:00:00")
            , "Kschl": "ZSM3"
            , "Vkorg": this.customer.registry.salesOrg
            , "Vtweg": this.customer.registry.distrCh
            , "Spart": this.customer.registry.division
            , "Kunnr": ""
            , "Zzagente1": this.customer.registry.agentCode
        }
        
        if(boolApprove || user.type == "TM" || user.type == "AM")
        {
        	req.Kunnr = this.customer.registry.id;
        	req.Kschl = "ZSX3";
        }
        	
        //Before Zzagente2 and Kunnr was interchanged
        model.odata.chiamateOdata.loadMaxCustomerDiscountValue(req, fSuccess, fError);
        return defer.promise;
    };
    this.updateCustomerDiscount = function (user) {
        var defer = Q.defer();
        var fSuccess = function (res) {
            this.customer.refreshCustomerDiscount(req);
            this.value = req.Sconto;
            this.status = req.Stat;
            this.statusDescr = model.NetworkStatus.getStatusDescr(req.Stat, "A");
            defer.resolve(req);
        }
        fSuccess = _.bind(fSuccess, this);
        var fError = function (err) {
            defer.reject(err);
        }
        fError = _.bind(fError, this);
        //From this.customer get customerDiscountItem;
        var discountItem = this.customer.getCustomerDiscount();
        //--------Using customerDiscount.js that is a utility library to manage discount status.
        var customerDiscount = new model.CustomerDiscount();
        customerDiscount.initialize(discountItem.value, discountItem.status);
        customerDiscount.setDiscount(user, this.value);
        //---------------------------------------------------------------------------------
        var req = {
            "Vkorg": this.customer.registry.salesOrg
            , "Vtweg": this.customer.registry.distrCh
            , "Spart": this.customer.registry.division
            , "Kunnr": this.customer.registry.id
            , "Cdage": this.customer.registry.agentCode
            , "Ustyp": user.type
            , "Bukrs": this.customer.registry.society
            , "Kschl": "ZSL3"
            , "Kappl": "V"
            , "Sconto": parseFloat(this.value).toString()
            , "Note": this.note
            , "Stat": customerDiscount.getDiscountStatus()
        };
        model.odata.chiamateOdata.updateCustomerDiscount(req, fSuccess, fError)
        return defer.promise;
    };
    this.getModel = function () {
        if (!this.value || this.value == 0) this.value = this.customer.getCustomerDiscountValue();
        this.value = parseFloat(this.value);
        return new sap.ui.model.json.JSONModel(this);
    };
    this.loadCustomerDiscountConditions = function () {
        var defer = Q.defer();
        var fSuccess = function (res) {
            var customerTemp = "";
            var id = this.customer.registry.id;
            if (id == "") {
                id = Math.floor(Math.random() * 10000 + 1);
            }
            if ((id % 5) == 0) customerTemp = "5"
            else if ((id % 4) == 0) customerTemp = "4";
            else if ((id % 3) == 0) customerTemp = "3";
            else if ((id % 2) == 0) customerTemp = "2"
            else {
                customerTemp = "1"
            }
            var tempRes = {
                "results": []
            };
            tempRes.results = _.filter(res.results, {
                Kunnr: customerTemp
            });
            var result = model.persistence.Serializer.customerDiscountCondition.fromSAPItems(tempRes);
            if (result && result.results) this._discounts = result.results;
            this.customer.setCustomerDiscountConditions(this._discounts);
            defer.resolve(this.customer);
        }
        fSuccess = _.bind(fSuccess, this);
        var fError = function (err) {
            this._discounts = [];
            defer.reject(err);
        }
        fError = _.bind(fError, this);
        $.getJSON("./custom/model/mock/data/customerDiscountConditions.json").success(fSuccess).error(fError)
        return defer.promise;
    };
    //--------Function to call in Customer Creation Phase----------------------
    // Actually Temporary with mock data
    //------------------------------------------------------------------------
    this.loadInitialCustomerDiscountConditions = function (req) {
        var defer = Q.defer();
        var fSuccess = function (res) {
            this.arrayDiscount.results.push(res);
            defer.resolve();
        };
        fSuccess = _.bind(fSuccess, this);
        var fError = function (err) {
            this._discounts = [];
            defer.reject(err);
        }
        fError = _.bind(fError, this);
        model.odata.chiamateOdata.loadCustomerDiscountConditions(req, fSuccess, fError);
        return defer.promise;
    };
    this.saveCustomerDiscountValue = function (user, previousStatus) //ci vuole lo stato futuro e il valore
        {
            var newDiscount = new model.CustomerDiscount();
            newDiscount.initialize(this.customer.getCustomerDiscountValue(), previousStatus);
            newDiscount.setDiscount(user, this.value);
            this.customer.setCustomerDiscountValue(this.value, newDiscount.getDiscountStatus());
            //-------------Version 2-------------------
            //Add here update call to customerDiscount Odata
            //------------------------------------------------
        };
    this.loadStatusNetwork = function () {
        var defer = Q.defer();
        var fSuccess = function (res) {
            this.network = [];
            for (var i = 0; res && res.results && i < res.results.length; i++) {
                this.network.push(res.results[i]);
            }
            defer.resolve(this.network);
        }
        var fError = function (err) {
            this.network = [];
            defer.reject(err);
        }
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.odata.chiamateOdata.loadNetworkStatus(fSuccess, fError);
        return defer.promise;
    };
    this.loadInitialCustomerDiscountConditionsNew = function (req) {
        var that = this;
        that.defer = Q.defer();
        that.f = _.bind(this.loadInitialCustomerDiscountConditions, this);
        that.itemtype = ["ZSL1", "ZSL2", "ZSM3"];
        
        that.callbackFunction = function () {
            var result = model.persistence.Serializer.customerDiscountCondition.fromSAPItems(that.arrayDiscount);
            for (var i = 0; result.results && result.results.length > 0 && i < result.results.length; i++) {
                //--------Modified from L.C. 09/09/16 09:44-----------------
                if (result.results[i].typeId == "ZSM3") {
                	
                    result.results[i].typeId = "ZSL3";
                    result.results[i].typeDescr = model.i18n.getText("ZSL3");
                    result.results[i].maxValue = (parseFloat(result.results[i].value)> 0) ? parseFloat(result.results[i].value) : 100 ;
                    that.maxCustomerDiscount = result.results[i].maxValue;
                    result.results[i].value = 0;
                }
                //----------Before------------------
                //          if (result.results[i].typeId == "ZSL3")
                //            result.results[i].value = 0;
                //----------------------------------
            }
            if (result && result.results) that._discounts = result.results;
            that.customer.setCustomerDiscountConditions(that._discounts);
            that.defer.resolve(that.customer);
        }
        
        utils.Asynchronous.asyncLoop({
            length: 3
            , functionToLoop: function (loop, i) {
                if (i === 0) {
                    that.miaReq = _.clone(req);
                    that.miaReq.Kunnr = "";
                    that.miaReq.Zzagente1 = "";
                }
                else if (i === 1) {
                    that.miaReq = _.clone(req);
                    that.miaReq.Zzagente1 = that.miaReq.Kunnr;
                    that.miaReq.Kunnr = "";
                   
                }
                else {
                    that.miaReq = _.clone(req);
//                    that.miaReq.Kunnr = that.miaReq.Kunnr;
//                    that.miaReq.Zzagente1 = "";
                    that.miaReq.Zzagente1 = that.miaReq.Kunnr;
                    that.miaReq.Kunnr = "";
                    
                }
                that.miaReq.Kschl = that.itemtype[i];
                that.f(that.miaReq).then(_.bind(loop, this));
            }
            , 
            callback:_.bind(that.callbackFunction, that)
        });
        return that.defer.promise;
    };
}