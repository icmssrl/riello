jQuery.sap.declare("model.Cart");

model.Cart = ( function () {

  Cart = function(data)
  {
    this.orderId  = "";
    this.customerId = "";
    this.username="";
    this.positions = [];

    this.update = function(data)
    {
      for(var prop in data)
      {
        this[prop]= data[prop];
      }
    };

    this.create = function(orderId, customerId)
    {
      this.orderId= orderId;
      this.customerId = customerId;
    };

    this.addPosition = function(position)
    {
      if(!this.positions || this.positions.length === 0)
        this.positions = [];

      this.positions.push(position);
    };
    this.setPositions = function(positionsArr)
    {
      if(positionsArr && positionsArr.length > 0)
      {
        this.positions = positionsArr;
      }
    };
    this.getPositions = function()
    {
      if(!this.positions || this.positions.length === 0)
        this.positions = [];

      return this.positions;
    };
    this.getModel = function()
    {
      var model = new sap.ui.model.json.JSONModel(this);
      return model;
    };

    this.sendToSAP= function(order, user, pref)
    {
      var defer = Q.defer();

      var info = {
        "Vbeln": order.orderId,
        "Uname": user.userGeneralData.username,
        "Xpref": ""
      }
      if(pref)
        info.Xpref= "X";

      utils.Busy.show();
      var fSuccess = function(result)
      {
        utils.Busy.hide();
        //console.log(result);
        defer.resolve(result);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {
        utils.Busy.hide();
        //console.log(err);
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.saveCart(info, fSuccess, fError)

      return defer.promise;

    }

  if(data)
    this.update(data);

  return this;
}

  return Cart;


})();
