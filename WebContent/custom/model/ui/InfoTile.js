jQuery.sap.declare("model.ui.InfoTile");
jQuery.sap.require("sap.m.library");
jQuery.sap.require("sap.m.StandardTile");

jQuery.sap.includeStyleSheet("custom/model/ui/css/InfoTile.css","InfoTileCSSid");

sap.m.StandardTile.extend
(
	"model.ui.InfoTile",
	{
		metadata :
		{
			properties : {           // setter and getter are created behind the scenes, incl. data binding and type validation
				 "title":"string",
				 "icon":"string",
				 "subtitle1":"string",// in simple cases, just define the type
				 "subtitle2":"string",
				 "subtitle3":"string",
				 "subtitle4":"string"
				
				 
				
				
                
				//"subText" : "string",
//				"bottomImg" : "string"
                
			}
		},

		renderer: {
			_renderContent: function(r, t)
			{
				r.write('<img class="InfoTileImg" src="'+ t.getIcon() +'" alt="">');
				r.write('<div class="InfoTileTitle"><span style="color:' + t.getColor() +'">' + t.getTitle() + ' </span></div>');
				r.write('<div class="InfoTileSubText"><p>' + t.getSubtitle1() + '</p></div>');
				r.write('<div class="InfoTileSubText"><p>' + t.getSubtitle2() + '</p></div>');
				r.write('<div class="InfoTileSubText"><p>' + t.getSubtitle3() + '</p></div>');
				r.write('<div class="InfoTileSubText"><p>' + t.getSubtitle4() + '</p></div>');
//				r.write('<img class="LogoTileBottomImg" src="'+ t.getBottomImg() +'" alt="">');
			},
		},
        
        onAfterRendering: function(oEvent) {  
       // !!! important: we have to call the original onAfterRendering method to get the tiles placed properly !!!  
       sap.m.StandardTile.prototype.onAfterRendering.apply(this, arguments);  
       var oTile = oEvent.srcControl;  
       // get dom element and add style commands to display an background-image  
       var oDOMEl = document.getElementById(oTile.getId());  
       if (oDOMEl) {  
            //oDOMEl.style.backgroundImage="url('" + this.getBackgroundImage() + "')";
            //oDOMEl.style.backgroundColor = this.getBackgroundColor();
            oDOMEl.childNodes[1].style.backgroundColor=this.getBackgroundColor();
            oDOMEl.style.backgroundRepeat="no-repeat";  
            oDOMEl.style.backgroundSize="contain"; // !!! CSS3 !!!  
       }  
      },  
      init: function() {  
           sap.m.StandardTile.prototype.init.apply(this, arguments);  
      } 
	}
);


 