
jQuery.sap.declare("model.CustomerStatus");

model.CustomerStatus = ( function () {

  CustomerStatus = function(data)
  {
    this.customerId = 0;
    this.totalOrder = 0;
    this.payedOrder =0;
    this.totalDebt = 0;
    this.usedDept = 0;
    this.orderRate = 0;
    this.debtRate = 0;
    var defer = Q.defer();
    var setOrderRate = function()
    {
      if(this.totalOrder === 0)
        this.orderRate = 0;
      else {
        this.orderRate = parseInt((this.payedOrder/this.totalOrder)*100);
      }
    };

    var setDebtRate = function()
    {
      if(this.totalDebt === 0)
        this.debtRate = 0;
      else {
        this.debtRate = parseInt((1-(this.usedDebt/this.totalDebt))*100);
      }
    };

    this.update = function (data)
    {
      for(var prop in data)
        this[prop] = data[prop];

      setOrderRate();
      setDebtRate();

    };


    this.getModel= function()
    {
      var model = new sap.ui.model.json.JSONModel(this);

      return model;
    };
    
//     this.getCreditLimit = function(params)
//    {
//      var defer = Q.defer();
//  
//      var fSuccess = function (result) {
//        var fido = model.persistence.Serializer.creditLimit.fromSAP(result);
//        //console.log(fido);
//        defer.resolve(fido);
//      };
//      fSuccess = _.bind(fSuccess, this);
//
//      var fError = function (err) {
//
//        //console.log(err);
//        defer.reject(err);
//      };
//      fError = _.bind(fError, this);
//
//      model.odata.chiamateOdata.getCustomerCreditLimit(params, fSuccess, fError);
//        
//      return defer.promise;
//    };
      
    
    
    
    
    
    //It's needed customerStatusID ?
    this.getId= function()
    {
      return this.customerId;
    };
    //---------------------------------


    if(data)
      this.update(data);

    return this;
  };

  return CustomerStatus;
})();
