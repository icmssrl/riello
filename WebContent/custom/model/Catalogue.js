jQuery.sap.declare("model.Catalogue");
jQuery.sap.require("model.collections.Products");
jQuery.sap.require("model.HierarchyNode");
jQuery.sap.require("model.collections.Hierarchies");
jQuery.sap.require("model.Product");
jQuery.sap.require("utils.Busy");

model.Catalogue = (function () {

  Catalogue = function () {



    // this.hierarchy = {"items":[]};
    this.hierarchy = {};
    this.products = {};
    //     this.getHierarchy = function()
    //     {
    // //      if(hDefer.promise.isFulfilled() && this.hierarchy.length > 0 && !forceReload )
    // //      {
    // //        hDefer.resolve(this.hierarchy);
    // //      }
    // //      else
    // //      {
    //           var hDefer = Q.defer();
    //           var fSuccess = function(result)
    //           {
    //             this.hierarchy.items  = result;
    //             //To set hierarchyData as preferred
    //             hDefer.resolve(this.hierarchy);
    //           };
    //           fSuccess = _.bind(fSuccess, this);
    //           var fError = function(err)
    //           {
    //             this.hierarchy.items =[];
    //             hDefer.reject(err);
    //           };
    //           fError = _.bind(fError, this);
    //
    //           model.collections.Hierarchies.loadHierarchies()
    //           .then(fSuccess, fError);
    //
    // //      }
    //       return hDefer.promise;
    //     };
    this.getHierarchy = function (userInfo, itemInfo) {
      //      if(hDefer.promise.isFulfilled() && this.hierarchy.length > 0 && !forceReload )
      //      {
      //        hDefer.resolve(this.hierarchy);
      //      }
      //      else
      //      {
      var hDefer = Q.defer();
      utils.Busy.show();
      var fSuccess = function (result) {
        utils.Busy.hide();
        this.hierarchy = result;
        //To set hierarchyData as preferred
        hDefer.resolve(this.hierarchy);
      };
      fSuccess = _.bind(fSuccess, this);
      var fError = function (err) {
        utils.Busy.hide();
        this.hierarchy = {
          "results": []
        };
        hDefer.reject(err);
      };
      fError = _.bind(fError, this);

      model.collections.Hierarchies.loadCatalogue(fSuccess, fError, userInfo, itemInfo)


      //      }
      return hDefer.promise;
    };

    this.getProductByInfo = function (userInfo, productInfo) {
      var defer = Q.defer();


      utils.Busy.show();
      var fSuccess = function (result) {
        utils.Busy.hide();
        defer.resolve(result);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        utils.Busy.hide();
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.collections.Products.getByInfo(userInfo, productInfo)
        .then(fSuccess, fError);

      return defer.promise;

    };

    this.getAccessoriesByProduct = function (userInfo, productId) {
      var defer = Q.defer();

      //        if(defer && defer.promise.isFulfilled())
      //        {
      //          defer.resolve(accessories);
      //        }
      //        else
      //        {
      var accessories = [];
      var data = _.clone(userInfo);
      data.Matnr = productId;
      var fSuccess = function (result) {
        if (result && result.results.length > 0) {
          accessories = [];

          for (var i = 0; i < result.results.length; i++) {
            var data = model.persistence.Serializer.product.fromSAP(result.results[i]);
            accessories.push(new model.Product(data));
          }

        }
        defer.resolve(accessories);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        accessories = [];
        //console.log("Error loading accessories");
        utils.Message.getSAPErrorMsg(err, "Error loading Accessories");
        defer.reject(err);
      };
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.getAccessories(data, fSuccess, fError);

      return defer.promise;
    };








    this.getById = function (id) {
      return _.find(this.hierarchy, {
        productId: id
      });
    };


    this.getChildNodeItems = function (parentId) {
      var items = this.hierarchy.items;
      var nodeArr = [];
      for (var i = 0; i < items.length; i++) {
        for (var j = 0; j < items[i].items.length; j++) {
          nodeArr.push(items[i].items[j]);
        }
      }
      var filterArr = [];
      var foundParentId;
      nodeArr.map(function (obj) {
        if (obj.productId === parentId)
        //filterArr.push(obj);
          foundParentId = obj.parentId;
      });
      nodeArr.map(function (obj) {
        if (obj.parentId === foundParentId)
          filterArr.push(obj);
      });


      return {
        "items": filterArr
      };
    };

    this.getProducts = function (userInfo, parentId) {

      var pDefer = Q.defer();
      var pSuccess = function (result) {

        //          var getNode = _.bind(getNodeByPath, this);
        //          var node  = getNode();
        //          if(!node.items)
        //            node.items = [];
        //
        //          node.items = result;
        //To set hierarchyData as preferred
        pDefer.resolve({
          "items": result
        });
      };
      pSuccess = _.bind(pSuccess, this);

      var pError = function (err) {
        this.hierarchy = [];
        pDefer.reject(err);
      };
      pError = _.bind(pError, this);

      model.collections.Products.loadProducts(parentId)
        .then(pSuccess, pError);

      return pDefer.promise;


    };

    this.getModel = function () {
      var defer = Q.defer();
      var model = new sap.ui.model.json.JSONModel();
      //var getNode = _.bind(getNodeByPath, this);
      //var node = getNode();

      //if(!node)
      model.setData(this);
      //      else
      //      {
      //        model.setProperty("/items", node.getModel().getData().items);
      //        model.setProperty("/path", this.path);
      //        return model;
      //      }

      return model;
    };



    return this;
  };



  return Catalogue;
})();