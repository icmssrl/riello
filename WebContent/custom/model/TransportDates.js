jQuery.sap.declare("model.TransportDates");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("utils.Busy");


model.TransportDates = (function () {




  TransportDates = function () {


    this.incoterms = "";
    this.codElabSpeciale = "";
    this.shippingType = "";
    //this.ILfdat = new Date();
    this.nation = "";
    this.prov = "";
    
    this.getTransportDate = function(req)
    {
      var defer = Q.defer();
      
      var request = {
        "ICcode" : "",
        "ICname" : req.provincia,
        "IInco1" : req.incoterms,
        "ILand1" : req.nation,
        "ILfdat" : utils.ParseDate().formatDate(new Date(), "yyyy-MM-ddT00:00:00"),
        "IRegio" : req.prov,
        "ISdabw" : req.codElabSpeciale,
        "ISpart" : req.Spart,
        "IVkorg" : req.Vkorg,
        "IVsart" : req.shippingType,
        "IVtweg" : req.Vtweg,
        "IWerks" : ""
//        "ELfdat" : ""
        
      };
      
      
      
      var success = function(result)
      {
        
        var deliveryDate = model.persistence.Serializer.deliveryDate.fromSAP(result);
        var deliveryDates = {results:[]};
        deliveryDates.results.push(deliveryDate);
        defer.resolve(deliveryDates);
        
      };
      var error = function(err)
      {
        defer.reject(err);
      };
      
      model.odata.chiamateOdata.getTransportDate(request, success, error);
      
      return defer.promise;
    };



    this.getModel = function () {
      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };





    return this;
  };
  return TransportDates;


})();