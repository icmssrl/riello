jQuery.sap.declare("model.Tiles");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.User");
jQuery.sap.require("model.Customer");
jQuery.sap.require("model.CustomerRequester");
jQuery.sap.require("model.collections.ApprovationsList");
jQuery.sap.require("model.collections.OfferApprovationsList");
jQuery.sap.require("model.collections.OffersList");
jQuery.sap.require("utils.Message");

model.Tiles = function () {



  TileCollection = [
    {
      "icon": "waiver",
      //"title": model.i18n.getText("CLOSE_SESSION"),
      "title": model.i18n._getLocaleText("discountApprovations"),
      "url": "customerDiscountApprovations",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": false,
      "userType": ["AM", "TM"],
      "all": false,
      "ri": true,
      "required": "discountEnable",
      "discountEnable" : true,
      "order": 1

},
    {
      "icon": "create-session",
      //"title": model.i18n.getText("CUSTOMER_MANAGEMENT"),
      "title": model.i18n._getLocaleText("CUSTOMER_MANAGEMENT"),
      "url": "customersList",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": false,
      "info": "",
      "userType": ["AG", "AM", "TM"],
      "all": false,
      "required": "discountEnable",
      "discountEnable" : false,
      
      "ri":false,
      "order": 2


    },
    {
        "icon": "create-session",
        //"title": model.i18n.getText("CUSTOMER_MANAGEMENT"),
        "title": model.i18n._getLocaleText("CUSTOMER_MANAGEMENT"),
        "url": "customersList",
        "type": "None",
        "isAgent": true,
        "showIfPhone": true,
        "inSession": false,
        "info": "",
        "userType": ["AG", "AM", "TM"],
        "all": false,
        "ri":true,
        "required": "discountEnable",
        "discountEnable" : true,
        
        "order": 2


      },
    {
      "icon": "customer",
      //"title": model.i18n.getText("CREATE_NEW_CUSTOMER"),
      "title": model.i18n._getLocaleText("CREATE_NEW_CUSTOMER"),
      "url": "newCustomer",
      "type": "None",
      "isAgent": true,
      "inSession": false,
      "showIfPhone": true,
      "wrCustReq": true,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 3



    },


    {
      "icon": "bbyd-active-sales",
      //"title": model.i18n.getText("CREATE_NEW_CUSTOMER"),
      "title": model.i18n._getLocaleText("ANORMAL_PRACTICES_LIST"),
      // "url": "anormalPractices",
      "url": "noDataSplitDetail",
      "type": "None",
      "isAgent": true,
      "showIfPhone": false,
      "inSession": false,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 4

    },
    {
      "icon": "customer-financial-fact-sheet",
      //"title": model.i18n.getText("CREATE_NEW_CUSTOMER"),
      "title": model.i18n._getLocaleText("BILLS_AND_COMMISSIONS"),
      "url": "billsCommissions",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": false,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 5

    },
    {
      "icon": "travel-itinerary",
      //"title": model.i18n.getText("CREATE_NEW_CUSTOMER"),
      "title": model.i18n._getLocaleText("HANDLE_TRANSPORT_DATES"),
      "url": "transportDates",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": false,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 6

    },


//    {
//      "icon": "customer-history",
//      "title": model.i18n.getText("CUSTOMER_SYNCRO"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},
    {
      "icon": "customer-order-entry",
      //"title": model.i18n.getText("ORDER_LIST"),
      "title": model.i18n._getLocaleText("ORDER_LIST"),
      "url": "orderList",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": false,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 7
		},
    {
      "icon": "response",
      //"title": model.i18n.getText("RETURN_LIST"),
      "title": model.i18n._getLocaleText("RETURN_LIST"),
      "url": "returnList",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": false,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 8
			},
//    {
//      "icon": "citizen-connect",
//      "title": model.i18n.getText("FAIR"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},
//    {
//      "icon": "table-chart",
//      "title": model.i18n.getText("EXCEL_LIST"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},
//    {
//      "icon": "feed",
//      "title": model.i18n.getText("FEED"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},
//    {
//      "icon": "locked",
//      "title": model.i18n.getText("CHANGE_PASSWORD"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},

    //tile di sessione  //Maybe to add if user is not in RI division
    {
      "icon": "add",
      //"title": model.i18n.getText("NEW_ORDER_CREATE"),
      "title": model.i18n._getLocaleText("NEW_ORDER_CREATE"),
      "url": "newOrder",
      "type": "None",
      "isAgent": true,
      "inSession": true,
      "showIfPhone": true,
      "wrOrdReq": true,
      "userType": ["AG", "AM", "TM"],
      "all": false,
      "ri": false,
      "required": "quotationEnable",
      "quotationEnable" : false,
      
      "order": 9
		},

    {
      "icon": "add",
      //"title": model.i18n.getText("NEW_ORDER_CREATE"),
      "title": model.i18n._getLocaleText("NEW_OFFER_ORDER_CREATE"),
      "url": "newOffer",
      "type": "None",
      "isAgent": true,
      "inSession": true,
      "showIfPhone": true,
      "wrOrdReq": true,
      "userType": ["AG", "AM", "TM"],
      "all": false,
      "required": "quotationEnable",
      "quotationEnable" : true,
      
      "ri": true,
      "order": 10
    },
    {
      "icon": "lead-outdated",
      //"title": model.i18n.getText("CATALOG"),
      "title": model.i18n._getLocaleText("CATALOG"),
      "url": "emptyROHierarchy",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": true,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 11

		},
    {
      "icon": "history",
      //"title": model.i18n.getText("HISTORY_CARTS"),
      "title": model.i18n._getLocaleText("HISTORY_CARTS"),
      "url": "emptyHistoryCarts",
      "type": "None",
      "isAgent": true,
      "showIfPhone": false,
      "inSession": true,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 12

		},
    {
      "icon": "customer-order-entry",
      //"title": model.i18n.getText("ORDER_LIST"),
      "title": model.i18n._getLocaleText("ORDER_LIST"),
      "url": "orderList",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": true,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 13
		},
    {

      "icon": "response",
      //"title": model.i18n.getText("ORDER_LIST"),
      "title": model.i18n._getLocaleText("RETURN_LIST"),
      "url": "returnList",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": true,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 14
		},
    {
      "icon": "customer-order-entry",
      //"title": model.i18n.getText("ORDER_LIST"),
      "title": model.i18n._getLocaleText("OFFER_LIST"),
      "url": "offersList",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": false,
      "info": "",
      "userType": ["AG", "AM", "TM"],
      "all": false,
      "ri": true,
      "required": "quotationEnable",
      "quotationEnable" : true,
      
      "order": 15
    },
    {
      "icon": "customer-order-entry",
      //"title": model.i18n.getText("ORDER_LIST"),
      "title": model.i18n._getLocaleText("OFFER_LIST"),
      "url": "offersList",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": true,
      "info": "",
      "userType": ["AG", "AM", "TM"],
      "all": false,
      "ri": true,
      "required": "quotationEnable",
      "quotationEnable" : true,
      
      "order": 16
    },
    {
      "icon": "customer-order-entry",
      //"title": model.i18n.getText("ORDER_LIST"),
      "title": model.i18n._getLocaleText("OFFER_APPROVATIONS_LIST"),
      "url": "offerApprovationsList",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": false,
      "userType": ["AM", "TM"],
      "all": false,
      "required": "quotationEnable",
      "quotationEnable" : true,
      "ri": true,
      "order": 17
    },
    {
      "icon": "customer-order-entry",
      //"title": model.i18n.getText("ORDER_LIST"),
      "title": model.i18n._getLocaleText("OFFER_APPROVATIONS_LIST"),
      "url": "offerApprovationsList",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": true,
      "userType": ["AM", "TM"],
      "all": false,
      "required": "quotationEnable",
      "quotationEnable" : true,
      "ri": true,
      "order": 18
    },


//    {
//      "icon": "sales-order-item",
//      "title": model.i18n.getText("ORDER_ACTIVE_VIEW"),
//      //this.getView().getModel("i18n").getResourceBundle().getText("ORDER_ACTIVE_VIEW"),
//      //this.getView().getModel("i18n").getText("ORDER_ACTIVE_VIEW")
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": true
//		},
//    {
//      "icon": "globe",
//      "title": model.i18n.getText("DESTINATIONS"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": true
//		},
    {
      "icon": "customer-briefing",
      //"title": model.i18n.getText("CUSTOMER_REPORT"),
      "title": model.i18n._getLocaleText("CUSTOMER_REPORT"),
      "url": "fullCustomerDetail",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": true,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 19


		},
    {
      "icon": "log",
      //"title": model.i18n.getText("CLOSE_SESSION"),
      "title": model.i18n._getLocaleText("CLOSE_SESSION"),
      "url": "launchpad",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": true,
      "userType": ["AG", "AM", "TM"],
      "all": true,
      "order": 20
		},


  ];
  //-----------------------------------Let's see in Version 2.0-------------------------------------------------------------
  // discountSessionTiles=[
  //   {
  // 	      "icon": "waiver",
  // 	      //"title": model.i18n.getText("CLOSE_SESSION"),
  // 	      "title": model.i18n._getLocaleText("discountRequest"),
  // 	      "url": "emptyDiscount",
  // 	      "type": "None",
  // 	      "isAgent": true,
  // 	      "showIfPhone": true,
  // 	      "inSession": true,
  //         "requestNum":model.persistence.Storage.session.get("currentCustomer").requestedDiscounts.length,
  //         "userType":["AG"]
  // 			},
  //     {
  //   	      "icon": "waiver",
  //   	      //"title": model.i18n.getText("CLOSE_SESSION"),
  //   	      "title": model.i18n._getLocaleText("discountApprovations"),
  //   	      "url": "emptyDiscount",
  //   	      "type": "None",
  //   	      "isAgent": true,
  //   	      "showIfPhone": true,
  //   	      "inSession": true,
  //           "requestNum":model.persistence.Storage.session.get("currentCustomer").requestedDiscounts.length,//To change
  //           "userType":["AM", "TM"]
  //   			},
  // ];
  //-------------------------------------------------------------------------------------------------------------------------------------
  /////***********Previous solution with second level tiles**********////////////

  //  societyTiles = [
  //    {
  //      "textColor":"#272868",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/fitservice.png",
  //      "title": "Fit Service",
  //      //"url": "divisionLaunchpad",
  //      "society": "SI41"
  //    },
  //    {
  //      "textColor":"#0066a4",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/riellogroup.png",
  //      "title": "Riello",
  //      //"url": "divisionLaunchpad",
  //      "society": "SI01"
  //    }
  //
  //  ];
  //
  //  divisionTiles = [
  //    {
  //      "textColor":"#ED1620",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/riello.png",
  //      "title": "Riello",
  //      "url": "launchpad",
  //      "society": "SI01",
  //      "division": "RI"
  //    },
  //    {
  //      "textColor":"#b7d30b",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/beretta.png",
  //      "title": "Beretta",
  //      "url": "launchpad",
  //      "society": "SI01",
  //      "division": "BR"
  //    },
  //    {
  //
  //      "textColor":"#01AD63",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/sylber.png",
  //      "title": "Sylber",
  //      "url": "launchpad",
  //      "society": "SI01",
  //      "division":"SY"
  //    },
  //    {
  //
  //      "textColor":"#e5322d",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/thermital.png",
  //      "title": "Thermital",
  //      "url": "launchpad",
  //      "society": "SI01",
  //      "division":"TH"
  //    },
  //    {
  //
  //      "textColor":"#1489ca",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/fontecal.png",
  //      "title": "Fontecal",
  //      "url": "launchpad",
  //      "society": "SI01",
  //      "division":"FO"
  //    },
  //    {
  //
  //      "textColor":"#272868",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/fitservice.png",
  //      "title": "Fit Service",
  //      "url": "launchpad",
  //      "society": "SI41",
  //      "division":"CF"
  //    },
  //    {
  //
  //      "textColor":"#ED1620",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/riello.png",
  //      "title": "Riello",
  //      "url": "launchpad",
  //      "society": "SI41",
  //      "division":"RI"
  //    },
  //    {
  //
  //      "textColor":"#b7d30b",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/beretta.png",
  //      "title": "Beretta",
  //      "url": "launchpad",
  //      "society": "SI41",
  //      "division":"BR"
  //    },
  //    {
  //
  //      "textColor":"#01AD63",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/sylber.png",
  //      "title": "Sylber",
  //      "url": "launchpad",
  //      "society": "SI41",
  //      "division":"SY"
  //    },
  //    {
  //
  //      "textColor":"#e5322d",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/thermital.png",
  //      "title": "Thermital",
  //      "url": "launchpad",
  //      "society": "SI41",
  //      "division":"TH"
  //    },
  //    {
  //
  //      "textColor":"#1489ca",
  //      "backgroundColor":"white",
  //      "icon": "./custom/img/loghi/fontecal.png",
  //      "title": "Fontecal",
  //      "url": "launchpad",
  //      "society": "SI41",
  //      "division":"FO"
  //    }
  //
  //  ];



  ////////////*********************///////////////////////

  divisionTiles = [
    {
      "textColor": "#ED1620",
      "backgroundColor": "white",
      "icon": "./custom/img/loghi/riello.png",
      "title": "Riello",
      "url": "launchpad",
      //      "society": "SI01",
      "division": "RI"
    },
    {
      "textColor": "#b7d30b",
      "backgroundColor": "white",
      "icon": "./custom/img/loghi/beretta.png",
      "title": "Beretta",
      "url": "launchpad",
      //      "society": "SI01",
      "division": "BR"
    },
    {

      "textColor": "#01AD63",
      "backgroundColor": "white",
      "icon": "./custom/img/loghi/sylber.png",
      "title": "Sylber",
      "url": "launchpad",
      //      "society": "SI01",
      "division": "SY"
    },
    {

      "textColor": "#e5322d",
      "backgroundColor": "white",
      "icon": "./custom/img/loghi/thermital.png",
      "title": "Thermital",
      "url": "launchpad",
      //      "society": "SI01",
      "division": "TH"
    },
    {

      "textColor": "#1489ca",
      "backgroundColor": "white",
      "icon": "./custom/img/loghi/fontecal.png",
      "title": "Fontecal",
      "url": "launchpad",
      //      "society": "SI01",
      "division": "FO"
    },
    {

      "textColor": "#272868",
      "backgroundColor": "white",
      "icon": "./custom/img/loghi/fitservice.png",
      "title": "Fit Service",
      "url": "launchpad",
      //      "society": "SI41",
      "division": "CF"
    }

  ];

  AMTMTiles = [
    {
      "icon": "waiver",
      //"title": model.i18n.getText("CLOSE_SESSION"),
      "title": model.i18n._getLocaleText("discountApprovations"),
      "url": "customerDiscountApprovations",
      "type": "None",
      "isAgent": true,
      "showIfPhone": true,
      "inSession": false,


      },

      // {
      //     "icon": "waiver",
      //     //"title": model.i18n.getText("CLOSE_SESSION"),
      //     "title": model.i18n._getLocaleText("discountApprovations"),
      //     "url": "customerDiscountApprovations",
      //     "type": "None",
      //     "isAgent": true,
      //     "showIfPhone": true,
      //     "inSession": true,
      //
      //   },
      // {
      //   "icon": "customer-order-entry",
      //   //"title": model.i18n.getText("ORDER_LIST"),
      //   "title": model.i18n._getLocaleText("OFFER_APPROVATIONS_LIST"),
      //   "url": "offerApprovationsList",
      //   "type": "None",
      //   "isAgent": true,
      //   "showIfPhone": true,
      //   "inSession": false
      // },
      // {
      //   "icon": "customer-order-entry",
      //   //"title": model.i18n.getText("ORDER_LIST"),
      //   "title": model.i18n._getLocaleText("OFFER_APPROVATIONS_LIST"),
      //   "url": "offerApprovationsList",
      //   "type": "None",
      //   "isAgent": true,
      //   "showIfPhone": true,
      //   "inSession": true
      // },
  ];

  //To integrate when userType entity is realized
  //  var map = [{"userType": "agent","tiles":[{"id":1}, {"id":2}, {"id":3}]}];

  //getById:function(idCode)
  //{
  //retun _.find(TileCollection, {id:idCode});
  //}
  // var userTiles = _.where(map, {userType : type});
  // var tiles = _.map(userTiles, this.getById(item.id));
  //  if (isCustomerSession) {
  // arrayTiles = _.where(tiles, {
  //   'inSession': true
  // });
  //-------------------------------------Version 1.0------------------------------------
  //   getTiles = function (user,isCustomerSession, isPhone, customer) {
  //
  //     var type = user.type;
  //     if (type.toUpperCase() === "AG".toUpperCase()
  //         || type.toUpperCase() === "TM".toUpperCase()
  //         || type.toUpperCase() === "AM".toUpperCase()) {
  //       if (isCustomerSession) {
  //           if(isPhone){
  //         arrayTiles = _.where(TileCollection, {
  //           'isAgent': true,
  //           'inSession': true,
  //           'showIfPhone': true
  //         });
  //           }else{
  //         arrayTiles = _.where(TileCollection, {
  //           'isAgent': true,
  //           'inSession': true
  //         });
  //
  //
  //       }
  //     }
  //     else {
  //
  //         if(isPhone){
  //         arrayTiles = _.where(TileCollection, {
  //           'isAgent': true,
  //           'inSession': false,
  //           'showIfPhone': true
  //         });
  //           }else{
  //         arrayTiles = _.where(TileCollection, {
  //           'isAgent': true,
  //           'inSession': false
  //         });
  //
  //           }
  //
  //
  //       }
  //     } else {
  //       //toDO
  //     }
  // //    if(!user.canEditOrders || (customer && customer.isBlocked))
  // //    {
  // //      _.remove(arrayTiles, function(item) { return item.wrOrdReq;});
  // //    }
  // //    if(!user.canEditCustomers)
  // //    {
  // //      _.remove(arrayTiles, function(item) { return item.wrCustReq;});
  // //    }
  //     if(!user.organizationData.results[0].canEditOrders || (customer && customer.isBlocked))
  //     {
  //       _.remove(arrayTiles, function(item) { return item.wrOrdReq;});
  //     }
  //     if(!user.organizationData.results[0].canEditCustomers)
  //     {
  //       _.remove(arrayTiles, function(item) { return item.wrCustReq;});
  //     }
  //
  //     //------------Part Version 2.0------------------------------
  //
  //     var defer = Q.defer();
  //     var userInfo = {
  //     	"username" : user.organizationData.results[0].username,
  //     	"userType" :  user.type,
  //     	"society" : user.organizationData.results[0].society,
  //     	"salesOrg" : user.organizationData.results[0].salesOrg,
  //     	"distrCh": user.organizationData.results[0].distributionChannel,
  //     	"division": user.organizationData.results[0].division,
  //     	"areaManager": user.organizationData.results[0].areaManager,
  //     	"territoryManager" : user.organizationData.results[0].territoryManager,
  //     	"agentCode" : user.userGeneralData.customer
  //     }
  //
  //
  //     if(user.type == "AG") // If agent add discountToApprove field to customer management tile, to load
  //     {
  //
  //       var customerRequester = new model.CustomerRequester(userInfo);
  //       customerRequester.loadCustomersList()
  //       .then(_.bind(function(res){
  //
  //         var discountToApprove = _.where(res, function(item){return item.discountStatus !== "S"});
  //         var numDiscountToApprove = discountToApprove && discountToApprove.length>0 ? discountToApprove.length : 0;
  //
  //         var customerMngmt=_.find(arrayTiles, {url:"customersList"});
  //         if(customerMngmt)
  //         {
  //           customerMngmt.info=model.i18n.getText("discountToApprove")+": "+numDiscountToApprove;
  //         }
  //         defer.resolve(arrayTiles);
  //       }, this))
  //     }
  //     else //if am/tm add approvationDiscount tile
  //     {
  //
  //       model.collections.ApprovationsList.getApprovationsList(userInfo)
  //       .then(_.bind(function(res){
  //
  //         var discountToApprove = _.where(res.results, {customerDiscountStatus:"M"});
  //         var numDiscountToApprove = discountToApprove && discountToApprove.length>0 ? discountToApprove.length : 0;
  //
  //         var approvationDiscountTile = _.find(AMTMTiles, {url:"customerDiscountApprovations"});
  //         if(approvationDiscountTile)
  //         {
  //           approvationDiscountTile.number = numDiscountToApprove;
  //           arrayTiles.push(approvationDiscountTile);
  //         }
  //         var customerMngmt=_.find(arrayTiles, {url:"customersList"});
  //
  //         if(customerMngmt)
  //         {
  //           customerMngmt.info= "";
  //         }
  //
  //         defer.resolve(arrayTiles);
  //       }, this))
  //     }
  //     return defer.promise;
  //     //-------------------------------------------------------------
  //
  //     //return arrayTiles;
  //   };
  //----------------------------Version 1.0 End---------------------------------------

  //--------------------------------Version 2.0------------------------------------------
  getTiles = function (user, isCustomerSession, isPhone, customer) {

    var inSession = isCustomerSession ? true : false;
    var type = user.type;
    var phone = isPhone;

    arrayTiles = _.filter(TileCollection, _.bind(function (item) {
      var checkType = _.includes(item.userType, type);
      var checkSession = (item.inSession == inSession);
      var checkPhone = phone ? (item.showIfPhone) : true;
      var checkAllDivision = item.all;
      return checkType && checkSession && checkPhone && checkAllDivision;

    }, this));

    var optTiles = _.filter(TileCollection, _.bind(function(item){
    	var checkType = _.includes(item.userType, type);
    	var checkSession = (item.inSession == inSession); 
    	var checkPhone = phone ? (item.showIfPhone) : true;
    	var checkRequired = !!item.required;
    	var checkRequiredProp = false;
    	switch(item.required){
    		case "discountEnable":
    			checkRequiredProp = item.discountEnable == user.organizationData.results[0].canEditDiscount;
    			break;
    		case "quotationEnable":
    			checkRequiredProp = item.quotationEnable == user.organizationData.results[0].canEditQuotation;
    			break;
    	};
    	return checkType && checkSession && checkPhone && checkRequired && checkRequiredProp;
    	
    	
    	
    }, this));
    
//    var riTiles = _.filter(TileCollection, _.bind(function (item) {
//      var checkType = _.includes(item.userType, type);
//      var checkSession = (item.inSession == inSession);
//      var checkPhone = phone ? (item.showIfPhone) : true;
//      var checkRiDivision = item.ri;
//      return checkType && checkSession && checkPhone && checkRiDivision;
//
//    }, this));
//
//    var notRiTiles = _.filter(TileCollection, _.bind(function (item) {
//      var checkType = _.includes(item.userType, type);
//      var checkSession = (item.inSession == inSession);
//      var checkPhone = phone ? (item.showIfPhone) : true;
//      var checkNotRiDivision = !item.all && !item.ri;
//      return checkType && checkSession && checkPhone && checkNotRiDivision;
//
//    }, this));
    
    var orgData = user.organizationData.results[0];
    
    arrayTiles = arrayTiles.concat(optTiles);
    //arrayTiles = (orgData.distributionChannel === "DO" && orgData.division === "RI" && orgData.salesOrg === "SA20" && orgData.society === "SI01") ? arrayTiles.concat(riTiles) : arrayTiles.concat(notRiTiles);


    arrayTiles = _.sortBy(arrayTiles, 'order');
//    if (!user.organizationData.results[0].canEditOrders 
//    	|| (customer && (customer.isBlocked || (customer.discountStatus !== "APP" && customer.discountStatus !== "MPP" && customer.registry.society == "SI01" && customer.registry.salesOrg == "SA20" && customer.registry.division=="RI" && customer.registry.distrCh == "DO" )))) {
//      _.remove(arrayTiles, function (item) {
//        return item.wrOrdReq;
//      });
//    }
    if (!user.organizationData.results[0].canEditOrders 
        	|| (customer && (customer.isBlocked || (customer.discountStatus !== "APP" && customer.discountStatus !== "MPP" && user.organizationData.results[0].canEditQuotation)))) {
          _.remove(arrayTiles, function (item) {
            return item.wrOrdReq;
          });}
    if (!user.organizationData.results[0].canEditCustomers) {
      _.remove(arrayTiles, function (item) {
        return item.wrCustReq;
      });
    }
    //------------------------------------------------------------------------
    
//    if (!(orgData.distributionChannel === "DO" && orgData.division === "RI" && orgData.salesOrg === "SA20" && orgData.society === "SI01")){
//   
//      var defer = Q.defer();
//
//      defer.resolve(arrayTiles);
//      return defer.promise;
//    }
    
    if(!user.organizationData.results[0].canEditDiscount && !user.organizationData.results[0].canEditQuotation)
    {
    	var defer = Q.defer();
    	defer.resolve(arrayTiles);
    	return defer.promise;
    }
 
    //-----------------------------------------------------------
    //------------Part Version 2.0------------------------------

    var defer = Q.defer();
    var userInfo = {
      "username": user.organizationData.results[0].username,
      "userType": user.type,
      "society": user.organizationData.results[0].society,
      "salesOrg": user.organizationData.results[0].salesOrg,
      "distrCh": user.organizationData.results[0].distributionChannel,
      "division": user.organizationData.results[0].division,
      "areaManager": user.organizationData.results[0].areaManager,
      "territoryManager": user.organizationData.results[0].territoryManager,
      "agentCode": user.userGeneralData.customer
    }


    var setOfferListTile = function(offersRes)
    {
    	var offersApprovedByOrder = _.filter(offersRes.results, function (item) {
            return (item.orderStatus === "APP" || item.orderStatus == "MPP" || !item.orderStatus) && (item.orderReason == "QFO")
          });
          var numOffersApprovedByOrder = offersApprovedByOrder ? offersApprovedByOrder.length : 0;

          var offersWaitingByOrder = _.filter(offersRes.results, function (item) {
            return (item.orderStatus !== "APP" && item.orderStatus !== "MPP" && item.orderStatus) && (item.orderReason == "QFO")
          });
          var numOffersWaitingByOrder = offersWaitingByOrder ? offersWaitingByOrder.length : 0;

          var offersApprovedByOffer = _.filter(offersRes.results, function (item) {
            return (item.orderStatus === "APP" || item.orderStatus == "MPP" || !item.orderStatus) && (item.orderReason == "QFQ")
          });
          var numOffersApprovedByOffer = offersApprovedByOffer ? offersApprovedByOffer.length : 0;

          var offersWaitingByOffer = _.filter(offersRes.results, function (item) {
            return (item.orderStatus !== "APP" && item.orderStatus !== "MPP" && item.orderStatus) && (item.orderReason == "QFQ")
          });
          var numOffersWaitingByOffer = offersWaitingByOffer ? offersWaitingByOffer.length : 0;


          var offerTile = _.find(arrayTiles, {
            url: "offersList"
          });
          if (offerTile) {
            offerTile.number = model.i18n.getText("byOrder") + ": " + numOffersApprovedByOrder;
            offerTile.number += " " + model.i18n.getText("byOffer") + ": " + numOffersApprovedByOffer;
            offerTile.numberUnit = model.i18n.getText("approved");
            offerTile.info = model.i18n.getText("waiting");
            offerTile.info += " " + model.i18n.getText("byOrder") + ": " + numOffersWaitingByOrder;
            offerTile.info += " " + model.i18n.getText("byOffer") + ": " + numOffersWaitingByOffer;


            //        offerTile.info = numOffersNotApproved ? model.i18n.getText("waitingOffers")+numOffersNotApproved : "";
            //arrayTiles.push(approvationDiscountTile);
          }
    };
    setOfferListTile = _.bind(setOfferListTile, this);
    
    
    var getDiscountList = function(info)
	{
		var d_defer = Q.defer();
		
		var fSuccess = function(res)
		{
			var discountToApprove = _.filter(res, function (item) {
	            return item.discountStatus !== "APP" && item.discountStatus !== "MPP";
	          });
	          var numDiscountToApprove = discountToApprove && discountToApprove.length > 0 ? discountToApprove.length : 0;


	          var customerMngmt = _.find(arrayTiles, {
	            url: "customersList", 
	            discountEnable:true
	          });
	          if (customerMngmt) {
	            customerMngmt.info = model.i18n.getText("waitingCustomer") + numDiscountToApprove;
	          }
	          d_defer.resolve(this);
		}
		fSuccess = _.bind(fSuccess, this);
		
		var fError = function(err)
		{
			sap.m.MessageToast.show(utils.Message.getError(err));
			d_defer.reject(err);
		}
		fError = _.bind(fError, this);
		
		var customerRequester = new model.CustomerRequester(info);
		
		customerRequester.loadCustomersList()
		.then(fSuccess, fError);
		
		return d_defer.promise;
	}
	getDiscountList = _.bind(getDiscountList, this);
	
	var getOffersList = function(info)
	{
		var o_defer  =Q.defer();
		
		var fSuccess = function(res)
		{
			setOfferListTile(res);
			o_defer.resolve(this);
		}
		fSuccess = _.bind(fSuccess, this);
		
		var fError = function(err)
		{
			sap.m.MessageToast.show(utils.Message.getError(err));
			o_defer.reject(err);
		}
		fError = _.bind(fError, this);
		
		model.collections.OffersList.getOffersList(info)
		.then(fSuccess, fError)
		
		return o_defer.promise;
		
	}
	getOffersList =  _.bind(getOffersList, this);
    
	var setOfferApprovationListTile = function(offerAppRes)
	{
		 var offerToApprove = _.filter(offerAppRes.results, _.bind(function (item) {
            	if(user.type == "AM")
	            	return item.orderStatus == "ATM" || item.orderStatus == "MTM";
	            else if(user.type == "TM")
	            	return item.orderStatus == "INS";
            }, this));

          var numOfferToApprove = offerToApprove && offerToApprove.length > 0 ? offerToApprove.length : 0;

          var approvationOfferTile = _.find(arrayTiles, {
            url: "offerApprovationsList"
          });
          if (approvationOfferTile) {
            approvationOfferTile.number = numOfferToApprove;
            //arrayTiles.push(approvationDiscountTile);
          }
	}
	setOfferApprovationListTile = _.bind(setOfferApprovationListTile, this);
	
	var getDiscountApprovationsList = function(info)
	{
		var da_defer = Q.defer();
		var fSuccess= function(res)
		{
			var discountToApprove = _.filter(res.results,_.bind(function (item) {
	            if(user.type == "AM")
	            	return item.customerDiscountStatus == "ATM" || item.customerDiscountStatus == "MTM";
	            else if(user.type == "TM")
	            	return item.customerDiscountStatus == "INS";
	          }, this));
	          
	          var numDiscountToApprove = discountToApprove && discountToApprove.length > 0 ? discountToApprove.length : 0;

	          var approvationDiscountTile = _.find(arrayTiles, {
	            url: "customerDiscountApprovations"
	          });
	          if (approvationDiscountTile) {
	            approvationDiscountTile.number = numDiscountToApprove;
	            //arrayTiles.push(approvationDiscountTile);
	          }
	          var customerMngmt = _.find(arrayTiles, {
	            url: "customersList"
	          });

	          if (customerMngmt) {
	            customerMngmt.info = "";
	          }
	          da_defer.resolve(this);
		}
		fSuccess = _.bind(fSuccess, this);
		
		var fError = function(err)
		{
			sap.m.MessageToast.show(utils.Message.getError(err));
			da_defer.reject(err);
		}
		fError=_.bind(fError, this);
		
		model.collections.ApprovationsList.getApprovationsList(info)
		.then(fSuccess, fError);
		
		return da_defer.promise;
	}
	getDiscountApprovationsList= _.bind(getDiscountApprovationsList, this);
	
	var getOfferApprovationsList = function(info)
	{
		var qa_defer=Q.defer();
		
		var fSuccess = function(res)
		{
			setOfferApprovationListTile(res);
			qa_defer.resolve(this);
		}
		fSuccess = _.bind(fSuccess, this);
		
		var fError = function(err)
		{
			sap.m.MessageToast.show(utils.Message.getError(err));
			qa_defer.reject(err);
		}
		fError = _.bind(fError, this);
		
		model.collections.OfferApprovationsList.getOfferApprovationsList(info)
		.then(fSuccess, fError);
		
		return qa_defer.promise;
	}
	getOfferApprovationsList=_.bind(getOfferApprovationsList,this);
    
    if (user.type == "AG") // If agent add discountToApprove field to customer management tile, to load
    {
    	
    	
    	
    	
//    	if(!isCustomerSession)
//    	{
    		/*
    		var customerRequester = new model.CustomerRequester(userInfo);
    		
    		
    	      Q.all([customerRequester.loadCustomersList(), model.collections.OffersList.getOffersList(userInfo)])
    	        .spread(_.bind(function (customerRes, offersRes) {

    	          var discountToApprove = _.filter(customerRes, function (item) {
    	            return item.discountStatus !== "APP" && item.discountStatus !== "MPP";
    	          });
    	          var numDiscountToApprove = discountToApprove && discountToApprove.length > 0 ? discountToApprove.length : 0;


    	          var customerMngmt = _.find(arrayTiles, {
    	            url: "customersList", 
    	            ri:true
    	          });
    	          if (customerMngmt) {
    	            customerMngmt.info = model.i18n.getText("waitingCustomer") + numDiscountToApprove;
    	          }
    	          
    	          setOfferListTile(offersRes);

    	          //-------------------------------------------
				
    		


    	          defer.resolve(arrayTiles);
    	        }, this))
    	        */
    		
    		var functionArr = [];
    		if(!isCustomerSession)
    		{
    			
    			
    			if(user.organizationData.results[0].canEditDiscount)
    				functionArr.push(getDiscountList(userInfo));
    			
    			if(user.organizationData.results[0].canEditQuotation)
    				functionArr.push(getOffersList(userInfo));
    				
    			if(functionArr.length == 0)
    				defer.resolve(arrayTiles);
    			else
    			{
    				Q.all(functionArr)
        			.then(_.bind(function(res){
        				defer.resolve(arrayTiles);
        			}, this))
    			}
    			
    		}
    		else
    		{
    			userInfo.customerId = customer.registry.id;
    			if(orgData.canEditQuotation)
    				getOffersList(userInfo)
    				.then(_.bind(function(res){
    					defer.resolve(arrayTiles);
    				}, this))
    			else
    				defer.resolve(arrayTiles);
    		}
    	//}
//    	else
//    	{
//    		userInfo.customerId=customer.registry.id;
//    		model.collections.OffersList.getOffersList(userInfo)
//    		.then(_.bind(function(res)
//    		{
//    			setOfferListTile(res);
//    			defer.resolve(arrayTiles);
//    		}, this), _.bind(function(err){
//    			sap.m.MessageToast.show("Error Loading Offer List");
//    			defer.reject(err);
//    		}, this))
//    	}
      
    } else //if am/tm add approvationDiscount tile
    {

    	
    	
    	var functionArr = [];
    	
    	if(!isCustomerSession)
    	{
    		
			
			
			
    		
    		if(user.organizationData.results[0].canEditDiscount)
    			functionArr.push(getDiscountApprovationsList(userInfo));
    		
    		if(user.organizationData.results[0].canEditQuotation)
				functionArr.push(getOfferApprovationsList(userInfo));
				
    		if(functionArr.length == 0)
				defer.resolve(arrayTiles);
			else
			{
				Q.all(functionArr)
    			.then(_.bind(function(res){
    				 var offerTile = _.find(arrayTiles, {
    	      	            url: "offersList"
    	      	          });
    	      	          if (offerTile) {
    	      	            offerTile.number = "";
    	      	            offerTile.numberUnit = "";
    	      	            offerTile.info = "";
    	      	           
    	      	          }
    				defer.resolve(arrayTiles);
    			}, this))
			}
   
    	}
    	else
    	{

    		
    		
    		if(orgData.canEditQuotation)
    		{
    			functionArr.push(getOfferApprovationsList(userInfo));
    			var customerInfo = _.clone(userInfo);
        		customerInfo.customerId=customer.registry.id;
        		customerInfo.agentCode = customer.registry.agentCode;
        		customerInfo.territoryManager = customer.registry.territoryManager;
        		functionArr.push(getOffersList(customerInfo));
    		}
				
				
    		if(functionArr.length == 0)
				defer.resolve(arrayTiles);
			else
			{
				Q.all(functionArr)
    			.then(_.bind(function(res){
//    				 var offerTile = _.find(arrayTiles, {
//    	      	            url: "offersList"
//    	      	          });
//    	      	          if (offerTile) {
//    	      	            offerTile.number = "";
//    	      	            offerTile.numberUnit = "";
//    	      	            offerTile.info = "";
//    	      	           
//    	      	          }
    				defer.resolve(arrayTiles);
    			}, this))
			}
    		
    		
    	}

    }
    return defer.promise;
    //-------------------------------------------------------------

    //return arrayTiles;
  };
  //---------------------------End Version 2.0--------------------------------------------------------------




  getDivisionTile = function (division) {
    return _.find(divisionTiles, {
      division: division
    });
  };

  //////////////*********Previous solution*********/////////////

  //  getSocietyTile = function(society)
  //  {
  //    return _.find(societyTiles, {society : society});
  //  };

  //  getDivisionTile = function(society, division)
  //  {
  //    return _.find(divisionTiles, {society : society, division : division});
  //  };

  //////////////***********************************/////////////

  return {
    getMenu: getTiles,
    getDivisionTile: getDivisionTile
      //    getSocietyTile: getSocietyTile

  };



}();