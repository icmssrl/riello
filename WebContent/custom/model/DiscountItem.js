jQuery.sap.declare("model.DiscountItem");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("sap.m.MessageToast");

model.DiscountItem = (function () {

  DiscountItem = function (serializedData) {

    //proprietà dell'oggetto discount
    //I suppose every element ( with exception of productId, orderId, positionId) an object composed by name, unitVal, scale, totValue, currency
    this.orderId = "";
    this.positionId = "";
    //this.productId = "";
    this.price = {};
    this.currency = "EUR";
    this.scale = "%";
    this.value= 0;

    this.typeId="";
    this.typeDescr ="";
    this.maxValue=null;
    this.isEditable=false;
    
//    this.initializeInstance = function(type)
//    {
//      switch(type)
//      {
//        case "ZSL4":
//          this.price = {};
//          this.currency = "EUR";
//          this.scale = "%";
//          this.value= 0;
//
//          this.typeId=type;
//          this.typeDescr =model.i18n.getText(this.typeId);
//          this.maxValue= 0;
//          this.isEditable=true;
//          this.string = "(max: "+this.maxValue+this.scale+")";
//          return this;
//          break;
//        default:
////          this.price = {};
////          this.currency = "EUR";
////          this.scale = "%";
////          this.value= 0;
////
////          this.typeId=type;
////          this.typeDescr ="Sconto Cliente";
////          this.maxValue= Math.floor(Math.random()*40+1);
//          this.price={};
//          this.typeId="ZSL4";
//          this.typeDescr=model.i18n.getText(this.typeId);
//          this.isEditable=true;
//          this.scale="%";
//          this.value = Math.floor(Math.random()*40 +1);
//          this.limiter= this.value;
//          this.maxValue = 30 + this.limiter;
//          this.string = "(limite: "+this.limiter+this.scale+", max: "+this.maxValue+this.scale+")";
//          
//          return this;
//        break;
//      }
//
//    };
    
    this.setType = function(typeId)
    {
    	if(!typeId)
    		return null;
    	this.typeId = typeId;
    	this.typeDescr = model.i18n.getText(typeId);
    	
    	
    	this.isEditable=(typeId == "ZSL3" || typeId == "ZSL4");
    	
    		
    	return ;
    };
    
    this.setMaxValue = function(value)
    {
    	if(!this.typeId)
    		return;
    	switch (this.typeId){
    	
    	case "ZSL3":
    		//this.maxValue = parseFloat(this.value) + parseFloat(value); Before
    		this.maxValue = parseFloat(value); //After
    		
    		//this.maxValue = Math.round(parseFloat(this.value)+ (parseFloat(value)*parseFloat(this.value)/100));
    		return;
    	case "ZSL4":
    		this.maxValue= value;
    		return;
    	default :
    		this.maxValue=null;
    		return;
    	}
    };
    //---------------------------------------------------------------------------------------
    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
        
      }
      if(this.value)
    	  this.value=parseFloat(this.value);

    };

    if (serializedData) {
      this.update(serializedData);

    }

    return this;
  };
  return DiscountItem;


})();
