jQuery.sap.declare("model.Order");
jQuery.sap.require("model.collections.Destinations");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Discount");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.Delivery");
jQuery.sap.require("utils.Date");
jQuery.sap.require("model.DiscountItem");
jQuery.sap.require("model.collections.DiscountItems");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.persistence.Storage");


model.Order = (function () {
    Order = function (serializedData) {
        this.orderId = "";
        this.customerId = "";
        this.customerName = "";
        this.rifOrder = "";
        this.basketType = "";
        this.destination = "";
        this.paymentMethod = "";
        this.paymentCondition = "";
        this.paymentConditionDescr = "";
        this.resa1 = "";
        this.resa2 = "";
        this.meansShipping = "";
        this.totalEvasion = "";
        this.appointmentToDelivery = "";
        this.deliveryType = "";
        this.transportArea = "";
        this.IVACode = "";
        this.promoCode = "";
        this.contactPerson = "";
        this.contactPersonTel = "";
        this.validDateList = new Date();
        this.requestedDate = new Date();
        this.orderReason = "";
        this.alternativeDestination = {
            constructor: ""
            , name: ""
            , street: ""
            , streetNumber: ""
            , city: ""
            , zipCode: ""
            , tel: ""
            , nation: ""
            , region: ""
        };
        this.positions = [];
        this.billsNotes = [];
        this.salesNotes = [];
        this.creditNotes = [];
        this.deliveries = [];
        this.attachments = [];
        this.links = [];
        // this.defaultAgentCommission = 20.00;
        this.agentCommission = 0;
        this.selectedType = null;
        this.setSelectedType = function (type) {
            this.selectedType = type;
        };
        this.getSelectedType = function () {
            return this.selectedType;
        };
        /*
         * this.refreshAgentCommission = function () { var maxSums = 0; var
         * totValue = 0; for (var i = 0; this.positions && this.positions.length >
         * 0 && i < this.positions.length; i++) { var positionDiscountArray =
         * this.positions[i].getOrderDiscounts(); for (var j = 0;
         * positionDiscountArray && positionDiscountArray.length && j <
         * positionDiscountArray.length; j++) { if
         * (positionDiscountArray[j].typeId == "ZSL3" ||
         * positionDiscountArray[j].typeId == "ZSL4") { maxSums +=
         * parseFloat(positionDiscountArray[j].maxValue); totValue +=
         * parseFloat(positionDiscountArray[j].value); } } } if (totValue >
         * maxSums) { this.agentCommission =
         * parseFloat((this.defaultAgentCommission * maxSums) /
         * totValue).toFixed(2); } else this.agentCommission =
         * this.defaultAgentCommission; };
         */
        this.getModel = function () {
            var model = new sap.ui.model.json.JSONModel(this);
            return model;
        };
        // this.create = function (customerId) {
        // var defer = Q.defer();
        // var fSuccess = function (res) {
        // if (res) {
        // this.customerId = res.getId();
        // this.customerName = res.getCustomerName();
        //
        // }
        // defer.resolve(res);
        // };
        // fSuccess = _.bind(fSuccess, this);
        //
        // var fError = function (err) {
        // //console.log("Error loading Customer Order Data");
        // defer.reject(err);
        // }
        // fError = _.bind(fError, this);
        //
        // model.collections.Customers.getById(customerId)
        // .then(fSuccess, fError);
        //
        // return defer.promise;
        //
        // };
        this.create = function (customer) {
        	this.customer = customer;
            this.customerId = customer.getId();
            this.customerName = customer.getCustomerName();
            this.paymentCondition = customer.sales.paymentCond;
            this.paymentConditionDescr = customer.sales.paymentCondDescr;
            this.paymentMethod = customer.sales.billType;
            this.resa1 = customer.sales.resa;
            this.resa1Descr = customer.sales.resaDescr;
            this.resa2 = customer.sales.incoterms2;
            this.transportArea = customer.sales.carrier;
            this.fullEvasionAvailableDate = null;
            this.transportAreaDescr = customer.sales.carrierDescr;
            // -----------------------Version
            // 2.0---------------------------------------
            // Condition: Only if division == RI Importing customer discount is
            // needed
            this.division = customer.registry.division;
            this.salesOrg = customer.registry.salesOrg;
            this.society = customer.registry.society;
            this.distrCh = customer.registry.distrCh;
            this.agentCode = customer.registry.agentCode;
            var cr = customer.registry;
//            if (cr.division !== "RI" || cr.distrCh !== "DO" || cr.salesOrg !== "SA20" || cr.society !== "SI01") return;
//            this.importCustomerDiscount(customer.customerDiscountConditions);
            // --------Before---------------------------------------------------------------------
            // this.commonDiscounts =
            // this.importCustomerDiscount(customer.customerDiscountConditions);
            // var orderDiscount = new model.DiscountItem();
            // // // orderDiscount.initializeInstance("ZSL4");
            // this.commonDiscounts.push(orderDiscount);
            // ------------------------------------------------------------------------------
        };
        this.getPosition = function (id) {
            if (!this.positions || this.positions.length == 0) return null;
            return _.find(this.positions, {
                positionId: id
            });
        };
        this.getPositions = function () {
            if (!this.positions || this.positions.length === 0) this.positions = [];
            return this.positions;
        };
        this.addPosition = function (position) {
            // var row = new model.Position(position);
            position.setOrder(this);
            var user = model.persistence.Storage.session.get("workingUser"); 
        	
        		
            //if (this.division == "RI" && this.distrCh === "DO" && this.salesOrg === "SA20" && this.society === "SI01") {
            if(user.organizationData.results[0].canEditQuotation){
                position.setOrderDiscounts(this.commonDiscounts);
            }
            var row = position;
            if (!this.positions || this.positions.length === 0) this.positions = [];
            this.positions.push(row);
            this.refreshFullEvasionAvailableDate();
        };
        this.load = function (id) {
            this.orderId = id;
            var defer = Q.defer();
            var fSuccess = function (result) {
                //if (this.division == "RI" && this.distrCh == "DO" && this.salesOrg == "SA20" && this.society == "SI01") this.initializeCommonDiscounts();
                var user = model.persistence.Storage.session.get("workingUser"); 
            	if(user.organizationData.results[0].canEditQuotation)
            		this.initializeCommonDiscounts();
                defer.resolve(this);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                sap.m.MessageToast.show("Error loading order" + this.orderId);
                defer.reject(err);
            }
            fError = _.bind(fError, this)
            
            
            this.loadHeader() // I don't know why I've done so, but until
                // we'll have more time I'll keep it
                .then(_.bind(this.loadItems, this)).then(_.bind(this.loadConditions, this)).then(_.bind(this.loadNotes, this)).then(_.bind(this.loadLinks, this)).then(fSuccess, fError);
            return defer.promise;
        };
        this.loadCart = function (id) {
            this.orderId = id;
            var defer = Q.defer();
            var fSuccess = function (result) {
            	var user = model.persistence.Storage.session.get("workingUser"); 
            	if(user.organizationData.results[0].canEditQuotation)
            		this.initializeCommonDiscounts();
            	//if (this.division == "RI" && this.distrCh == "DO" && this.salesOrg == "SA20" && this.society == "SI01") this.initializeCommonDiscounts();
                defer.resolve(this);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                sap.m.MessageToast.show("Error loading order" + this.orderId);
                defer.reject(err);
            }
            fError = _.bind(fError, this)
            this.loadHeader() // I don't know why I've done so, but until
                // we'll have more time I'll keep it
                .then(_.bind(this.loadItems, this)).then(_.bind(this.loadConditions, this)).then(fSuccess, fError);
            return defer.promise;
        };
        this.loadHeader = function () {
            var defer = Q.defer();
            var fSuccess = function (result) {
                var header = model.persistence.Serializer.order.fromSAP(result);
                var orderId = this.orderId;
                this.update(header);
                if (this.orderId === "") {
                    this.orderId = orderId;
                }
                //if (this.basketType == "ZOF" && this.division == "RI" && this.distrCh == "DO" && this.salesOrg == "SA20" && this.society == "SI01")
                var user = model.persistence.Storage.session.get("workingUser"); 
                if (this.basketType == "ZOF" && user.organizationData.results[0].canEditQuotation)
                {
                	this.loadZsl3Customer()
                	.then(_.bind(function(){defer.resolve(this)}, this))
                }
                else
                	defer.resolve(this);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                sap.m.MessageToast.show("Error Loading Order Header of " + this.orderId);
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.loadOrderHeader(this.orderId, fSuccess, fError);
            // .then(fSuccess, fError);
            return defer.promise;
        };
        this.loadItems = function () {
            var defer = Q.defer();
            var fSuccess = function (result) {
                if (result && result.results && result.results.length > 0) {
                    if (!this.positions) this.positions = [];
                    for (var i = 0; i < result.results.length; i++) {
                        this.positions.push(new model.Position(model.persistence.Serializer.order_item.fromSAP(result.results[i]))); // Not
                        // using
                        // add
                        // §Positions
                        // because
                        // i'm
                        // reading
                        // a
                        // position
                        // already
                        // set
                    }
                }
                defer.resolve(this);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                sap.m.MessageToast.show("Error Loading Order Items of " + this.orderId);
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.loadOrderItems(this.orderId, fSuccess, fError)
                // .then(fSuccess, fError);
            return defer.promise;
        };
        this.loadConditions = function () {
            var defer = Q.defer();
            var fSuccess = function (result) {
                if (result && result.results && result.results.length > 0) {
                    var discounts = model.persistence.Serializer.discount.fromSAPItems(result);
                    for (var i = 0; i < this.positions.length; i++) {
                        var d = _.find(discounts, {
                            positionId: this.positions[i].positionId
                        })
                        this.positions[i].setDiscount(new model.Discount(d));
                        var price = _.find(d.discountArray, {
                            typeId: "ZPBA"
                        });
                        if (price) this.positions[i].unitNetPrice = price.value;
                        //if (this.division == "RI" && this.salesOrg == "SA20" && this.distrCh == "DO" && this.society == "SI01") {
                        var user = model.persistence.Storage.session.get("workingUser"); 
                        if(user.organizationData.results[0].canEditQuotation){
                            this.positions[i].setOrderDiscounts(d.discountArray, true);
                        }
                    }
                    // var posObjs = _.groupBy(result.results, 'Posex');
                    // for (var prop in posObjs) {
                    // var position = this.getPosition(prop);
                    // // var serializedDiscount =
                    // model.persistence.Serializer.discount.fromSAPItems(posObjs[prop]);
                    //
                    //
                    // var serializedDiscount = new model.Discount();
                    // serializedDiscount.initialize(this.orderId, position);
                    // serializedDiscount.discountArray=[];
                    //
                    // var serializedDiscount =
                    // model.persistence.Serializer.discount.fromSAPItems(posObjs[prop]);
                    // position.setDiscount(new
                    // model.Discount(serializedDiscount));
                    // }
                    // for(var i = 0; i< result.results.length ; i++)
                    // {
                    // this.addPosition(new
                    // model.Position(model.persistence.Serializer.order_item.fromSAP(result.results[i])));
                    // }
                }
                defer.resolve(this);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                sap.m.MessageToast.show("Error Loading Order Conditions of " + this.orderId);
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.loadOrderConditions(this.orderId, fSuccess, fError);
            // .then(fSuccess, fError);
            return defer.promise;
        };
        this.loadNotes = function () {
            var defer = Q.defer();
            var fSuccess = function (result) {
                if (result && result.results && result.results.length > 0) {
                    for (var i = 0; i < result.results.length; i++) {
                        var note = model.persistence.Serializer.order_note.fromSAP(result.results[i]);
                        if (note.id === "Z001") {
                            this.salesNotes.push(note);
                        }
                        else if (note.id === "Z002") {
                            this.billsNotes.push(note)
                        }
                        else if (note.id === "Z013") {
                            this.creditNotes.push(note);
                        }
                    }
                }
                defer.resolve(this);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                sap.m.MessageToast.show("Error Loading Order Notes of " + this.orderId);
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.loadOrderNotes(this.orderId, fSuccess, fError);
            // .then(fSuccess, fError);
            return defer.promise;
        };
        this.loadLinks = function () {
            var defer = Q.defer();
            var req = {
                "Vbeln": this.orderId
            };
            var fSuccess = function (result) {
                if (result && result.results && result.results.length > 0) {
                    this.links = [];
                    for (var i = 0; i < result.results.length; i++) {
                        var link = model.persistence.Serializer.link.fromSAP(result.results[i]);
                        // link.url=
                        // icms.Component.getMetadata().getConfig().settings.serverUrl+"SO_ContentAttachSet(IObjId='"+link.attachId+"')/$value";
                        link.url = sessionStorage.getItem("serverUrl") + "SO_ContentAttachSet(IObjId='" + link.attachId + "')/$value";
                        this.links.push(link);
                    }
                }
                defer.resolve(this);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                this.links = [];
                sap.m.MessageToast.show("Error Loading Links of " + this.orderId);
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.loadAttachmentLinks(req, fSuccess, fError);
            // .then(fSuccess, fError);
            return defer.promise;
        };
        this.update = function (data) {
            for (var prop in data) {
                this[prop] = data[prop];
                if (prop == "validDateList" || prop == "requestedDate" || prop == "fullEvasionAvailableDate") {
                    this[prop] = new Date(this[prop]);
                }
            }
            for (var i = 0; this.positions && this.positions.length > 0 && i < this.positions.length; i++) {
                this.positions[i] = new model.Position(this.positions[i]);
            }
        };
        this.loadDeliveries = function () {
            var defer = Q.defer();
            var req = {
                "VbelnSo": this.orderId
            };
            var fSuccess = function (result) {
                this.deliveries = [];
                if (result && result.results.length > 0) {
                    for (var i = 0; i < result.results.length; i++) {
                        this.deliveries.push(new model.Delivery(model.persistence.Serializer.delivery.fromSAP(result.results[i])));
                    }
                }
                defer.resolve(this.deliveries);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                this.deliveries = [];
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.loadDeliveries(req, fSuccess, fError);
            return defer.promise;
        };
        // this.loadAttach:function(attachId)
        // {
        // var defer = Q.defer();
        // var fSuccess = function(result)
        // {
        // sap.m.URLHelper.redirect(this._getVal(evt), true);
        // };
        // fSuccess = _.bind(fSuccess, this);
        // var fError = function(err)
        // {
        //
        // }
        // fError = _.bind(fError, this);
        //
        // model.odata.chiamateOdata.showAttachment(attachId, fSuccess, fError);
        //
        // return defer.promise;
        // };
        if (serializedData) {
            this.update(serializedData);
        }
        this.getId = function () {
            return this.orderId;
        };
        this.removePosition = function (position) {
            if (this.positions.length > 0) {
            	var found = false;
            	var index = null;
            	
                for (var i = 0; i < this.positions.length; i++) {
                    if (position === this.positions[i].positionId) 
                    {
                        
                        
                      
                     /*--------------------*/
                        found=true;
                        index = i;


	                /*--------------------------*/
	                
		            }
		            if(found && i>index)
		            {
		                this.positions[i]._refreshId(this.positions[i].positionId-1);    
		            }
                        /*----------------------*/
                     
                }
                if(found && index >= 0)
                	this.positions.splice(index, 1);
            }
            this.refreshFullEvasionAvailableDate();
            return;
        };
        this.refreshFullEvasionAvailableDate = function () {
            this.fullEvasionAvailableDate = null;
            for (var i = 0; i < this.positions.length; i++) {
                if (!this.positions[i].wantedDate) {
                    this.fullEvasionAvailableDate = this.requestedDate ? new Date(this.requestedDate) : null;
                    return this.fullEvasionAvailableDate;
                }
                if ((this.fullEvasionAvailableDate === null) || ((this.positions[i].availableDate) && (new Date(this.fullEvasionAvailableDate) < new Date(this.positions[i].wantedDate)))) {
                    this.fullEvasionAvailableDate = new Date(this.positions[i].wantedDate);
                }
                // if(!this.fullEvasionAvailableDate || new
                // Date(this.fullEvasionAvailableDate) < new
                // Date(this.positions[i].availableDate) )
                // this.fullEvasionAvailableDate = new
                // Date(this.positions[i].wantedDate);
                // else
                // {
                // this.fullEvasionAvailableDate= new
                // Date(this.positions[i].wantedDate);
                // }
            }
            return this.fullEvasionAvailableDate;
        };
        // /New refresh------------------------------------------------
        // if((this.fullEvasionAvailableDate === null) ||
        // ((this.positions[i].availableDate) && (new
        // Date(this.fullEvasionAvailableDate) < new
        // Date(this.positions[i].wantedDate))))
        // {
        // this.fullEvasionAvailableDate = new
        // Date(this.positions[i].wantedDate);
        // }
        // --------------------------------------
        // this.setDestination = function (destinationCode)
        // {
        // if(destinationCode)
        // {
        // var d =
        // model.collections.Destinations.getById(destinationCode);//Maybe to
        // correct, it depends from customer's destinations
        // this.destination = d.destination;
        // }
        // else
        // {
        // //mi prendo l'id del primo elemento dell'array delle destinazioni di
        // questo customer
        // var currentCustomer =
        // model.collections.Customers.getById(this.customerCode);
        // var destinations = currentCustomer.getDestination();
        // if(destinations && destinations.length>0)
        // {
        // this.destination = destinations[0].destination;
        // }
        // }
        // };
        this.setDestination = function (destination) {
            this.destination = destination;
        };
        this.addNote = function (param, oEntry) {
            var note = {
                "text": oEntry.text
                , "author": oEntry.author
            };
            this[param][0] = note;
        };
        // ** Francesco invia l'ordine a SAP
        this.sendToSAP = function (order) {
            var defer = Q.defer();
            utils.Busy.show();
            // ------Added from L.C. 09/09/16 11:29----
            // -----Move position.OrderDiscount to position.discount to be
            // serialized as before
            
            
            //if (order.division === "RI" && order.distrCh === "DO" && order.salesOrg === "SA20" && order.society === "SI01") this.updateDiscountOriginalFromRI();
            var user = model.persistence.Storage.session.get("workingUser"); 
        	if(user.organizationData.results[0].canEditQuotation)
        		this.updateDiscountOriginalFromRI();
            // --------------------------------------
            var sapOrder = model.persistence.Serializer.order.toSAP(order);
            var fSuccess = function (result) {
                utils.Busy.hide();
                // console.log(result);
                defer.resolve(result);
            };
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                utils.Busy.hide();
                // console.log(err);
                defer.reject(err);
            };
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.createOrder(sapOrder, fSuccess, fError);
            return defer.promise;
        };
        this.refreshOrderBySAP = function (order) {
            var defer = Q.defer();
            utils.Busy.show();
            //if (order.division === "RI" && order.distrCh === "DO" && order.salesOrg === "SA20" && order.society === "SI01") this.updateDiscountOriginalFromRI();
            var user = model.persistence.Storage.session.get("workingUser"); 
        	if(user.organizationData.results[0].canEditQuotation)
        		this.updateDiscountOriginalFromRI();
        	
            var sapOrder = model.persistence.Serializer.order.toSAP(order);
            sapOrder.Simula = "X";
            var fSuccess = function (result) {
                utils.Busy.hide();
                this.refreshTotalPrices(result);
                this.refreshPositionPrices(result.SalesOrderItemSet); // -It's
                // needed
                // to
                // reload
                // the
                // positions
                // this.reloadPositions(result.SalesOrderItemSet);
                this.refreshDiscountsPrices(result.SalesOrderConditionSet);
                // console.log(result);
                defer.resolve(result);
            };
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                utils.Busy.hide();
                // console.log(err);
                defer.reject(err);
            };
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.createOrder(sapOrder, fSuccess, fError);
            return defer.promise;
        };
        this.refreshTotalPrices = function (result) {
            this.netPrice = parseFloat(result.Netwr);
            this.ivaPrice = parseFloat(result.Mwsbp);
            this.transportPrice = parseFloat(result.SoTranspTot);
            this.totalPrice = parseFloat(result.SoTot);
            this.agentCommission = parseFloat(result.PrvTot);
        };
        this.reloadPositions = function (positions) {
            if (positions && positions.results && positions.results.length > 0) {
                var arr = positions.results;
                this.positions = [];
                for (var i = 0; i < arr.length; i++) {
                    var pos = new model.Position(model.persistence.Serializer.order_item.fromSAP(arr[i]));
                    pos.initializeProduct();
                    this.positions.push(pos);
                }
            }
        };
        this.refreshPositionPrices = function (positions) {
            if (!positions || !positions.results || (positions.results.length <= 0)) {
                return;
            }
            var arr = positions.results;
            for (var i = 0; i < arr.length; i++) {
                var position = this.getPositionByMatnrRequestDate(arr[i].Matnr, arr[i].Edatu);
                if (position) {
                    position.positionId = parseInt(arr[i].Posex); // Set
                    // position
                    // to
                    // retrieve
                    // it when
                    // reading
                    // Discount
                    position.refreshPricesBySAP(arr[i]);
                }
                else {
                    // console.log("Position not found - Fuck");
                }
            }
        };
        this.refreshDiscountsPrices = function (discounts) {
            if (!discounts || !discounts.results || (discounts.results.length <= 0)) {
                return;
            }
            var positionsDiscountArr = _.groupBy(discounts.results, "Posex");
            if (positionsDiscountArr) {
                for (var prop in positionsDiscountArr) {
                    var position = this.getPositionByPosex(prop);
                    if (position) position.refreshDiscountsPricesBySAP(positionsDiscountArr[prop]);
                    else {
                        // console.log("Position not found - Fuck");
                    }
                }
            }
            // if(this.division == "RI")
            
           // if (this.division == "RI" && this.distrCh === "DO" && this.salesOrg === "SA20" && this.society === "SI01") {
            var user = model.persistence.Storage.session.get("workingUser"); 
        	if(user.organizationData.results[0].canEditQuotation){
                this.updateDiscountRIFromOriginal();
            }
        };
        this.getPositionByMatnr = function (matnr) {
            if (!this.positions || this.positions.length <= 0) return;
            var req = matnr
            var result = null;
            for (var i = 0; i < this.positions.length; i++) {
                if (this.positions[i].productId === matnr) {
                    result = this.positions[i];
                    break;
                }
            }
            return result;
        };
        this.getPositionByMatnrRequestDate = function (matnr, reqDate) {
            if (!this.positions || this.positions.length <= 0) return;
            // var req = matnr
            var result = null;
            for (var i = 0; i < this.positions.length; i++) {
                if (this.positions[i].productId === matnr && (utils.Date.compareDate(this.positions[i].wantedDate, reqDate) == 0)) {
                    result = this.positions[i];
                    break;
                }
            }
            return result;
        };
        this.getPositionByPosex = function (posex) {
            if (!this.positions || this.positions.length <= 0) return;
            var req = _.isString(posex) ? parseInt(posex) : posex;
            var result = null;
            for (var i = 0; i < this.positions.length; i++) {
                if (parseInt(this.positions[i].getId()) === parseInt(req)) {
                    result = this.positions[i];
                    break;
                }
            }
            return result;
        };
        this.assignPositionDiscountToAll = function (position) {
            if (!this.positions || this.positions.length <= 1) {
                sap.m.MessageToast.show(model.i18n.getText("NO_POSITION_IN_WHICH_COPY"));
                return;
            }
            for (var i = 0; i < this.positions.length; i++) {
                if (this.positions[i].positionId !== position.positionId) {
                    this.positions[i].copyDiscount(position.getDiscount());
                }
            }
        };
        this.assignOrderDiscountToPos = function (editableBool) {
            // if(!model.collections.DiscountItems.checkCommonDiscounts(this.commonDiscounts))
            // {
            // this.basketType = "ZOF";
            // }
            // else if(this.getSelectedType()=="ZRD")
            // {
            // this.basketType = "ZRD";
            // }
            // if(this.basketType == "ZOF")
            // {
            // if(this.getSelectedType() == this.basketType)
            // this.offerType = "QFQ";
            // else
            // this.offerType="QFO";
            // }
            for (var i = 0; i < this.positions.length; i++) {
                this.positions[i].setOrderDiscounts(this.commonDiscounts, {keepEditable: editableBool, keepValue: editableBool });
            }
        };
        this.checkPositionsDiscount = function () {
            var result = true;
            for (var i = 0; i < this.positions.length; i++) {
                if (!this.positions[i].discountAcceptable) result = false;
            }
            return result;
        };
        this.checkOrderType = function () {
            var bool = this.checkPositionsDiscount();
            // var basketType = bool ? this.getSelectedType() : "ZOF";
            // this.setBasketType(basketType);
        }
        this.transformToOffer = function (isDiscountOK) {
            this.basketType = isDiscountOK ? this.getSelectedType() : "ZOF";
        };
        this.setBasketType = function (type) {

//        	if (this.division == "RI" && this.distrCh === "DO" && this.salesOrg === "SA20" && this.society === "SI01")
//        	{
//        		
//        		this._setPositionsMaxValues();
//        	}
        	
        	
        };
        this.getPosition = function (posId) {
            return _.find(this.positions, {
                positionId: posId
            });
        };
        // ** destinazione alternativa
        this.setAlternativeDestination = function (dest) {
            this.alternativeDestination = dest;
            this.destination = dest.constructor;
        };
        this.removeAlternativeDestination = function () {
            delete this.alternativeDestination; // Francesco
            // this.alternativeDestination = {name:"", street:"",
            // streetNumber:"", city:"",zipCode:"",tel:"",nation:""}; //Lorenzo,
            // I don't know the logic about it, but a delete removes the
            // property and the ruins the binding in orderCreate
        };
        // **camion gru
        this.setCamionGruData = function (data) {
            this.gru = data;
        };
        this.removeCamionGruData = function () {
            delete this.gru;
        };
        this.setDefaultDiscounts = function (discountObj) {
            this.defaultDiscount = discountObj;
        };
        this.getDefaultDiscount = function () {
            return this.defaultDiscount ? this.defaultDiscount : null;
        };
        // this.assignDiscountDefaultValueToAll = function()
        // {
        // if(!this.defaultDiscount)
        // return;
        //
        // for(var i = 0; i< this.positions.length; i++)
        // {
        // this.positions[i].discount.assignDiscountsValues(this.defaultDiscount);
        // }
        // return;
        //
        // };
        this.setDeliveryType = function (deliveryType) {
            if (deliveryType !== "11") this.removeCamionGruData();
            this.deliveryType = deliveryType;
        };
        // ** Francesco return only data without function
        this.getData = function () {
            // **
            var order = _.cloneDeep(this.getModel().getData());
            for (var prop in order) {
                if (typeof (order[prop]) === "function") {
                    delete(order[prop]);
                }
                else if (prop === "positions") {
                    var positions = order[prop];
                    for (var i = 0; i < positions.length; i++) {
                        for (var positionsProp in positions[i]) {
                            if (typeof (positions[i][positionsProp]) === "function") {
                                delete(positions[i][positionsProp]);
                            }
                        }
                    }
                }
                else if (prop === "billsNotes") {
                    var billsNotes = order[prop];
                    for (var i = 0; i < billsNotes.length; i++) {
                        for (var billsNotesProp in billsNotes[i]) {
                            if (typeof (billsNotes[i][billsNotesProp]) === "function") {
                                delete(billsNotes[i][billsNotesProp]);
                            }
                        }
                    }
                }
                else if (prop === "salesNotes") {
                    var salesNotes = order[prop];
                    for (var i = 0; i < salesNotes.length; i++) {
                        for (var salesNotesProp in salesNotes[i]) {
                            if (typeof (salesNotes[i][salesNotesProp]) === "function") {
                                delete(salesNotes[i][salesNotesProp]);
                            }
                        }
                    }
                }
            }
            return order;
        };
        // **
        this.deleteAttach = function (attachId) {
            var defer = Q.defer();
            var attach = _.find(this.attachments, {
                posId: attachId
            });
            var req = {
                "IDrunr": attach.posId
                , "IGuid": this.guid
            }
            var fSuccess = function (result) {
                _.remove(this.attachments, {
                    posId: attachId
                });
                defer.resolve(result);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.deletePreAttach(req, fSuccess, fError);
            return defer.promise;
        };
        this.loadDefaultValues = function (req) {
            var deferDefault = Q.defer();
            var fSuccess = function (res) {
                var result = model.persistence.Serializer.loadDefault.fromSAPItems(res);
                // assegnare alla proprietà resa2 il valore dall' array result.
                var obj = _.find(result.results, {
                    attributeName: "INCO2"
                });
                this.resa2 = obj.attributeValue;
                deferDefault.resolve(this);
            };
            var fError = function (err) {
                deferDefault.reject(err);
            };
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.loadDefaultValues(req, fSuccess, fError);
            return deferDefault.promise;
        };
        // --------------------Version
        // 2.0-----------------------------------------
//        this.importCustomerDiscount = function (customerDiscounts) {
        this.importCustomerDiscount = function () {
            this.commonDiscounts = [];
            var customerDiscounts = this.customer.customerDiscountConditions;
            if (!customerDiscounts || customerDiscounts.length == 0) return;
            this._loadMaxConditionsValue().then(_.bind(function (res) {
                for (var i = 0; i < customerDiscounts.length; i++) {
                    switch (customerDiscounts[i].typeId) {
                    case "ZSL3":
                    	
                    	//Fixed Item 
                    	var fixedItem = _.cloneDeep(customerDiscounts[i]);
                    	fixedItem.isEditable=false;
                    	fixedItem.typeId="ZSL3_fixed";
                    	fixedItem = new model.DiscountItem(fixedItem);
                    	this.commonDiscounts.push(fixedItem);
                    	
                    	//Variable Item
                        var item = _.cloneDeep(customerDiscounts[i]);
                        item.limiter = parseFloat(item.value);
                        item.scale = res.zsx3.scale;
                        // item.maxValue = parseFloat(item.limiter) +
                        // parseFloat(res.zsx3.value); //Before
                        //item.maxValue = Math.round(parseFloat(item.value) + (parseFloat(item.value)*parseFloat(res.zsx3.value)/100)); // After
                        
                        item.value= (this.basketType == "ZRD") ? 0 : (item.value ? item.value : 0);//21/09/2016 see mail 
                        item.maxValue = parseFloat(res.zsx3.value);
                        item.orderString = "(Max: "+item.limiter+")";
                        item.offerString = "(" + model.i18n.getText("limit")+ ": " + item.limiter + item.scale + "\nMax: " + item.maxValue + item.scale + ")";
                        //-Maybe useless------------------------
                        if(this.basketType == "ZOF")
                        	item.string = "(" + model.i18n.getText("limit")+ ": " + item.limiter + item.scale + "\nMax: " + item.maxValue + item.scale + ")";
                        else
                        	item.string= "(Max: "+item.limiter+")";
                        //---------------------------------------------------
                        item = new model.DiscountItem(item);
                        this.commonDiscounts.push(item);
                        break;
                    default:
                        var item = _.cloneDeep(customerDiscounts[i]);
                        item = new model.DiscountItem(item);
                        this.commonDiscounts.push(item);
                        break;
                        // case "ZPSA":
                        // var item = _.cloneDeep(customerDiscounts[i]);
                        //    				
                        // item.maxValue = 20 //20 will be a constant from
                        // ERP
                        // item.string = "(max:
                        // "+this.maxValue+this.scale+")";
                        // result.push(item);
                        // break;
                    }
                }
                var orderItem = new model.DiscountItem();
                orderItem.scale = res.zsx4.scale
                orderItem.setType("ZSL4");
                var orderValue = res.zsx4.value > 0  ? res.zsx4.value : 20;
                orderItem.setMaxValue(orderValue);
                this.commonDiscounts.push(orderItem);
                //In case history Cart ---- if local? 
                for(var i = 0; this.positions && this.positions.length > 0 &&  i < this.positions.length ; i++)
                {
                	this.positions[i].setOrderDiscounts(this.commonDiscounts);
                    
                }
                //----------------------------------------
            }, this), _.bind(function (err) {
                this.commonDiscounts = [];
            }, this))
        };
        this._loadMaxConditionsValue = function () {
            var defer = Q.defer();
            var fSuccess = function (custMax, ordMax) {
                defer.resolve({
                    "zsx3": custMax
                    , "zsx4": ordMax
                });
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            Q.all([this._loadCondition("ZSX3"), this._loadCondition("ZSX4")]).spread(fSuccess, fError);
            return defer.promise;
        };
        this._loadCondition = function (condType) {
            var defer = Q.defer();
            var req = {};
            req.Bukrs = this.society;
            req.Vkorg = this.salesOrg;
            req.Spart = this.division;
            req.Vtweg = this.distrCh;
            //req.Kunnr = this.agentCode;
            //req.Zzagente1 = "";
            req.Kunnr = this.customerId;
            req.Zzagente1 = this.agentCode;
            req.Prsdt = utils.ParseDate().formatDate(new Date(), "yyyy-MM-ddT00:00:00");
            
            req.Kschl = condType;
            req.Kappl = "V";
            var fSuccess = function (res) {
                var item = model.persistence.Serializer.customerDiscountCondition.fromSAP(res);
                defer.resolve(item);
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.loadCustomerDiscountConditions(req, fSuccess, fError);
            return defer.promise;
        };
        // ------------------Version
        // 2.0-------------------------------------------
        this.setCommonDiscounts = function (discounts) {
            this.commonDiscounts = discounts;
            this.assignOrderDiscountToPos();
        };
        // --------------------------------------------------------------------------
        this.loadDiscount_v2 = function () {
            var defer = Q.defer();
            var fSuccess = function (res) {
                if (this.commonDiscounts) // Temp
                {
                    defer.resolve(this);
                }
                else {
                    this.commonDiscounts = [];
                    var result = model.persistence.Serializer.discount_v2.fromSAPItems(res);
                    if (result && result.results && result.results.length > 0) {
                        // -----------Temp Mock -------------------------
                        var nationalDiscount = _.find(result.results, {
                            typeId: "ZSL1"
                        });
                        nationalDiscount = new model.DiscountItem(nationalDiscount);
                        nationalDiscount.typeId = "ZSL1";
                        nationalDiscount.typeDescr = "Sconto Base";
                        nationalDiscount.value = 35;
                        this.commonDiscounts.push(nationalDiscount);
                        var regioDiscount = _.find(result.results, {
                            typeId: "ZSL2"
                        });
                        regioDiscount = new model.DiscountItem(regioDiscount);
                        regioDiscount.typeId = "ZSL2";
                        regioDiscount.typeDescr = "Sconto Addizionale";
                        regioDiscount.value = Math.floor(Math.random() * 40 + 1);
                        this.commonDiscounts.push(regioDiscount);
                        var customerDiscount = _.find(result.results, {
                            typeId: "ZSL3"
                        });
                        customerDiscount = new model.DiscountItem(customerDiscount);
                        customerDiscount.typeId = "ZSL3";
                        customerDiscount.typeDescr = "Sconto Cliente";
                        customerDiscount.isEditable = true;
                        customerDiscount.scale = "%";
                        customerDiscount.value = Math.floor(Math.random() * 40 + 1);
                        customerDiscount.limiter = customerDiscount.value;
                        // customerDiscount.maxValue = customerDiscount.maxValue
                        // + customerDiscount.value; Before
                        customerDiscount.maxValue = customerDiscount.maxValue // After
                        customerDiscount.string = "(" + model.i18n.getText("limit")+ ": " + customerDiscount.limiter + "\nMax:" + customerDiscount.maxValue + ")";
                        this.commonDiscounts.push(customerDiscount)
                        var orderDiscount = _.find(result.results, {
                            typeId: "ZSL4"
                        });
                        orderDiscount = new model.DiscountItem(orderDiscount);
                        orderDiscount.typeId = "ZSL4";
                        orderDiscount.typeDescr = "Sconto Discrezionale";
                        orderDiscount.isEditable = true;
                        orderDiscount.value = 0;
                        orderDiscount.maxValue = 20;
                        orderDiscount.string = "(max: " + orderDiscount.maxValue + ")";
                        this.commonDiscounts.push(orderDiscount);
                        // ----------------------------------------------
                        // this.commonDiscounts = result.results;
                    }
                    defer.resolve(this);
                }
            }
            fSuccess = _.bind(fSuccess, this);
            var fError = function (err) {
                this.commonDiscounts = [];
                defer.reject(err);
            }
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.loadDiscounts_v2(this.orderId, fSuccess, fError)
            return defer.promise;
        };
        this.updateDiscounts = function (discountsArray) {
            for (var i = 0; discountsArray && discountsArray.length > 0 && i < discountsArray.length; i++) {
                for (var j = 0; this.commonDiscounts && j < this.commonDiscounts.length; j++) {
                    if (this.commonDiscounts[j].typeId == discountsArray[i].typeId) {
                        switch (discountsArray[i].typeId) {
                        case "ZSL3":
                        case "ZSL4":
                            this.commonDiscounts[j].value = discountsArray[i].value;
                            this.commonDiscounts[j].price = discountsArray[i].price;
                            break;
                        default:
                            this.commonDiscounts[j] = discountsArray[j];
                        }
                    }
                }
            }
            // this.commonDiscounts = discountsArray;
        };
        this.updateDiscountOriginalFromRI = function () {
            for (var i = 0; this.positions && this.positions.length > 0 && i < this.positions.length; i++) {
                this.positions[i].updateDiscountOriginalFromRI();
            }
        };
        this.updateDiscountRIFromOriginal = function () {
            for (var i = 0; this.positions && this.positions.length > 0 && i < this.positions.length; i++) {
                this.positions[i].updateDiscountRIFromOriginal();
            }
        };
        // --------------------Discount
        // Management----------------------------------
        // this._discount = {"value":"", "status":"", "statusDescr":""};
        //	
        // this.initialize= function(value, status)
        // {
        // this._discount.value = value;
        // (status)? this._setStatus(status) : this._setStatus("INS");
        //  		
        //  		
        // };
        this.setOfferDiscountStatus = function (user, typeEnum) {
            // if(!this._discount.value)
            // {
            // console.log("discount must be initialize first");
            // return;
            // }
            if (user.type == "AG") {
                // this._discount.value = value;
                this._setStatus("INS");
            }
            else if (typeEnum == "R") {
                this._setStatus("REJ");
            }
            else if (user.type == "TM") {
                if (!typeEnum) // Yet To check if discount equals to the past
                // discount
                {
                    this._setStatus("ATM");
                }
                else {
                    // this._discount.value = value;
                    this._setStatus("MTM");
                }
            }
            else if (user.type == "AM") {
                if (!typeEnum) // || (this._discount.value == value))
                {
                    this._setStatus("APP");
                }
                else {
                    // this._discount.value = value;
                    this._setStatus("MPP");
                }
            }
        };
        this._setStatus = function (status) {
            this.orderStatus = status;
            this.orderStatusDescr = this.getStatusDescription(status);
        };
        this.getStatusDescription = function (status) {
            if (!status) {
                console.log("Error: status undefined");
                return null;
            }
            var result = "";
            switch (status) {
            case "APP":
                result = model.i18n.getText("Approvato AM");
                break;
            case "MPP":
                result = model.i18n.getText("Approvato AM Con Modifiche");
                break;
            case "ATM":
                result = model.i18n.getText("Approvato TM");
                break;
            case "MTM":
                result = model.i18n.getText("Approvato TM Con Modifiche");
                break;
            case "INS":
                result = model.i18n.getText("Inserito");
                break;
            case "REJ":
                result = model.i18n.getText("refused");
                break;
            }
            return result;
        };
        // this.getDiscountValue=function()
        // {
        // return this._discount.value;
        // };
        this.getOrderStatus = function () {
            return this.orderStatus;
        };
        this.getOrderStatusDescr = function () {
            return this.orderStatusDescr;
        };
        this.isEditable = function (user) {
            if (user.type == "AG") return (this.orderStatus == "REJ")
            else if (user.type == "TM") {
                return (this.orderStatus !== "INS")
            }
            else if (user.type == "AM") {
                if (this.orderStatus == "INS" || this.orderStatus == "REJ") return false;
                else return true;
            }
        };
        this.initializeCommonDiscounts = function () {
        	
        	
            this.commonDiscounts = [];
            if (this.positions && this.positions.length > 0) {
                if (this.positions[0].discount && this.positions[0].discount.discountArray && this.positions[0].discount.discountArray.length > 0) {
                    var discounts = this.positions[0].discount.discountArray;
                    for (var i = 0; i < discounts.length; i++) {
                        var item = _.cloneDeep(discounts[i]);
                        switch (item.typeId) {
                        case "ZSL3":
                        	var fixedItem = _.cloneDeep(item);
                        	fixedItem.isEditable=false;
                        	fixedItem.value=this.avgCustomer;
                        	fixedItem.typeId="ZSL3_fixed";
                        	fixedItem = new model.DiscountItem(fixedItem);
                        	this.commonDiscounts.push(fixedItem);
                        	
                            item.maxValue = this.maxCustomerDiscount ? this.maxCustomerDiscount : 100;
                            item.value = this.avgCustomer ? this.avgCustomer : 0;
                            item.limiter = this.avgCustomer ? this.avgCustomer : 0;
                            item.orderString = "(Max: "+item.limiter+")";
                            item.offerString = "(" + model.i18n.getText("limit")+ ": "  + item.limiter + item.scale + "\nMax: " + item.maxValue + item.scale + ")";
                            //-------Maybe Useless--------------------
                            if(this.basketType == "ZOF")
                            	item.string = "(" + model.i18n.getText("limit")+ ": "  + item.limiter + item.scale + "\nMax: " + item.maxValue + item.scale + ")";
                            else
                            	item.string = "(Max: "+item.limiter+")";
                            //-------------------------------------------
                            item.isEditable = true;
                            this.commonDiscounts.push(item);
                            break;
                        case "ZSL4":
                            item.maxValue = this.maxOrderDiscount ? this.maxOrderDiscount : 100;
                            item.value = 0;
                            item.isEditable = true;
                            this.commonDiscounts.push(item);
                            break;
                        case "ZSL1":
                        case "ZSL2":
                            this.commonDiscounts.push(item);
                        default:
                            break;
                        }
                    }
                }
                this._setPositionsMaxValues(true);
            }
            
            
        };
        this._setPositionsMaxValues = function (keepValue) {
            for (var i = 0; i < this.positions.length; i++) {
                var zsl3Item = _.find(this.positions[i].discount.discountArray, {
                    typeId: "ZSL3"
                });
                if (zsl3Item) {
                    zsl3Item.maxValue = this.maxCustomerDiscount ? this.maxCustomerDiscount : 100;
                    zsl3Item.limiter = this.avgCustomer ? this.avgCustomer : 0;
                    zsl3Item.orderString = "(Max: "+zsl3Item.limiter+")";
                    zsl3Item.offerString = "(" + model.i18n.getText("limit")+ ": "  +zsl3Item.limiter + zsl3Item.scale + "\nMax: " + zsl3Item.maxValue + zsl3Item.scale + ")";
                    //-----------Maybe Useless----------------------------
                    if(this.basketType == "ZOF")
                    	zsl3Item.string = "(" + model.i18n.getText("limit")+ ": "  +zsl3Item.limiter + zsl3Item.scale + "\nMax: " + zsl3Item.maxValue + zsl3Item.scale + ")";
                    else
                    {
                    	zsl3Item.string = "(Max: "+zsl3Item.limiter+")";
                    	//zsl3Item.value = zsl3Item.value > zsl3Item.maxValue ? zsl3Item.maxValue : zsl3Item.value;
                    }
                    //------------------------------------------------------	
                    
                    zsl3Item.isEditable = true;
                }
                var zsl4Item = _.find(this.positions[i].discount.discountArray, {
                    typeId: "ZSL4"
                });
                if (zsl4Item) {
                    zsl4Item.maxValue = this.maxOrderDiscount ? this.maxOrderDiscount : 100;
                    zsl4Item.isEditable = true;
                }
                this.positions[i].setOrderDiscounts(this.positions[i].discount.discountArray, {keepValue:true});
            }
        };
        this.offerApprovation = function (u, o) {
            var defer = Q.defer();
            var req = {};
            req.orderId = o.orderId;
            req.type = u.type;
            req.note = "";
            req.stat = o.orderStatus;
            req.positions = o.positions;
            var r = model.persistence.Serializer.offerApprovation.toSAP(req);
            var fSuccess = function (res) {
                console.log(res);
                defer.resolve(res);
            };
            var fError = function (err) {
                defer.reject(err);
            };
            fSucess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            model.odata.chiamateOdata.offerApprovations(r, fSuccess, fError);
            return defer.promise;
        };
        
        this.loadZsl3Customer = function()
        {
        	var defer = Q.defer();
        	var req = {
        		"Bukrs":this.society,
        		"Vkorg":this.salesOrg,
        		"Vtweg": this.distrCh,
        		"Spart":this.division,
        		"Cdage":this.agentCode,
        		"Kunnr":this.customerId
        	}
        	
        	var fSuccess = function(res)
        	{
        		this.avgCustomer = parseFloat(res.Zsl3Actual);
        		defer.resolve(this);
        	}
        	fSuccess = _.bind(fSuccess, this);
        	
        	var fError = function(err)
        	{
        		defer.reject(err);
        	}
        	model.odata.chiamateOdata.getCustomerDetail(req, fSuccess, fError);
        	return defer.promise;
        };
        this.initializeCustomerDiscounts = function(customer)
        {
        	if(!this.customer)
        		this.customer = customer;
        	
        	if(!this.commonDiscounts || this.commonDiscounts.length == 0)
        		this.initializeCommonDiscounts();
        }
        
        return this;
    };
    
    return Order;
})();