jQuery.sap.declare("model.Offer");
jQuery.sap.require("model.collections.Destinations");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Discount");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.Delivery");
jQuery.sap.require("utils.Date");
jQuery.sap.require("model.DiscountItem");

model.Offer = (function () {

  Offer = function (serializedData) {




    this.orderId = "";
    this.customerId = "";
    this.customerName = "";
    this.rifOrder = "";
    this.basketType = "";
    this.destination = "";
    this.paymentMethod = "";
    this.paymentCondition = "";
    this.paymentConditionDescr="";
    this.resa1 = "";
    this.resa2 = "";
    this.meansShipping = "";
    this.totalEvasion = "";
    this.appointmentToDelivery = "";
    this.deliveryType = "";
    this.transportArea = "";
    this.IVACode = "";
    this.promoCode = "";
    this.contactPerson = "";
    this.contactPersonTel = "";

    this.validDateList = new Date();
    this.requestedDate = new Date();
    this.orderReason = "";
    this.alternativeDestination = {constructor:"", name:"", street:"", streetNumber:"", city:"",zipCode:"",tel:"",nation:"", region:""};


    this.positions = [];
    this.billsNotes = [];
    this.salesNotes = [];
    this.creditNotes = [];

    this.deliveries=[];

    this.attachments = [];
    this.links=[];

    // this.defaultDiscount = {
    //   "agentDiscount":"",
    //   "firstLocDiscount": "",
    //   "secondLocDiscount":"",
    //   "thirdLocDiscount":""
    // };
    this.agentCommission=20.00;

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };

    // this.create = function (customerId) {
    //   var defer = Q.defer();
    //   var fSuccess = function (res) {
    //     if (res) {
    //       this.customerId = res.getId();
    //       this.customerName = res.getCustomerName();
    //
    //     }
    //     defer.resolve(res);
    //   };
    //   fSuccess = _.bind(fSuccess, this);
    //
    //   var fError = function (err) {
    //     //console.log("Error loading Customer Order Data");
    //     defer.reject(err);
    //   }
    //   fError = _.bind(fError, this);
    //
    //   model.collections.Customers.getById(customerId)
    //     .then(fSuccess, fError);
    //
    //   return defer.promise;
    //
    // };
    this.create = function (customer) {
      this.customerId = customer.getId();
      this.customerName = customer.getCustomerName();
      this.paymentCondition = customer.sales.paymentCond;
      this.paymentConditionDescr=customer.sales.paymentCondDescr;
      this.paymentMethod = customer.sales.billType;
      this.resa1 = customer.sales.resa;
      this.resa1Descr=customer.sales.resaDescr;
      this.resa2 = customer.sales.incoterms2;
      this.transportArea = customer.sales.carrier;
      this.fullEvasionAvailableDate=null;
      this.transportAreaDescr = customer.sales.carrierDescr;

    };

    this.getPosition = function (id) {
      if (!this.positions || this.positions.length == 0)
        return null;
      return _.find(this.positions, {
        positionId: id
      });
    };
    this.getPositions = function () {
      if (!this.positions || this.positions.length === 0)
        this.positions = [];
      return this.positions;
    };
    this.addPosition = function (position) {
      // var row = new model.Position(position);
      position.setOrder(this);
      var row = position;
      if (!this.positions || this.positions.length === 0)
        this.positions = [];
      this.positions.push(row);
      this.refreshFullEvasionAvailableDate();

    };



    this.load = function (id) {
      this.orderId = id;
      var defer = Q.defer();
      var fSuccess = function (result) {
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        sap.m.MessageToast.show("Error loading order" + this.orderId);
        defer.reject(err);
      }
      fError = _.bind(fError, this)

      this.loadHeader()                         //I don't know why I've done so, but until we'll have more time I'll keep it
        .then(_.bind(this.loadItems, this))
        .then(_.bind(this.loadConditions, this))
        .then(_.bind(this.loadNotes, this))
        .then(_.bind(this.loadLinks, this))
        .then(fSuccess, fError);

      return defer.promise;
    };

    this.loadCart = function(id)
    {
      this.orderId = id;
      var defer = Q.defer();
      var fSuccess = function (result) {
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        sap.m.MessageToast.show("Error loading order" + this.orderId);
        defer.reject(err);
      }
      fError = _.bind(fError, this)

      this.loadHeader()                         //I don't know why I've done so, but until we'll have more time I'll keep it
        .then(_.bind(this.loadItems, this))
        .then(_.bind(this.loadConditions, this))
        .then(fSuccess, fError);

      return defer.promise;
    };

    this.loadHeader = function () {
      var defer = Q.defer();
      var fSuccess = function (result) {
        var header = model.persistence.Serializer.order.fromSAP(result);
        this.update(header);
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        sap.m.MessageToast.show("Error Loading Offer Header of " + this.orderId);
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadOrderHeader(this.orderId, fSuccess, fError);
      // .then(fSuccess, fError);

      return defer.promise;
    };

    this.loadItems = function () {
      var defer = Q.defer();
      var fSuccess = function (result) {
        if (result && result.results && result.results.length > 0) {
          if(!this.positions)
            this.positions=[];
          for (var i = 0; i < result.results.length; i++) {
            this.positions.push(new model.Position(model.persistence.Serializer.order_item.fromSAP(result.results[i])));//Not using add §Positions because i'm reading a position already set
          }
        }
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        sap.m.MessageToast.show("Error Loading Order Items of " + this.orderId);
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadOrderItems(this.orderId, fSuccess, fError)

      // .then(fSuccess, fError);

      return defer.promise;
    };

    this.loadConditions = function () {
      var defer = Q.defer();
      var fSuccess = function (result) {
        if (result && result.results && result.results.length > 0) {

          var discounts  = model.persistence.Serializer.discount.fromSAPItems(result);
          for(var i = 0; i < this.positions.length ; i++)
          {
            var d = _.find(discounts, {positionId : this.positions[i].positionId})
            this.positions[i].setDiscount(new model.Discount(d));
            var price = _.find(d.discountArray, {typeId : "ZPBA"});
            if(price)
              this.positions[i].unitNetPrice = price.value;
          }


          // var posObjs = _.groupBy(result.results, 'Posex');
          // for (var prop in posObjs) {
          //   var position = this.getPosition(prop);
          //   // var serializedDiscount = model.persistence.Serializer.discount.fromSAPItems(posObjs[prop]);
          //
          //
          //   var serializedDiscount = new model.Discount();
          //   serializedDiscount.initialize(this.orderId, position);
          //   serializedDiscount.discountArray=[];
          //
          //   var serializedDiscount = model.persistence.Serializer.discount.fromSAPItems(posObjs[prop]);
          //   position.setDiscount(new model.Discount(serializedDiscount));
          // }
          //  for(var i = 0; i< result.results.length ; i++)
          //  {
          //    this.addPosition(new model.Position(model.persistence.Serializer.order_item.fromSAP(result.results[i])));
          //  }
        }
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        sap.m.MessageToast.show("Error Loading Order Conditions of " + this.orderId);
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadOrderConditions(this.orderId, fSuccess, fError);

      // .then(fSuccess, fError);

      return defer.promise;
    };

    this.loadNotes = function () {
      var defer = Q.defer();
      var fSuccess = function (result) {
        if (result && result.results && result.results.length > 0)
        {
           for(var i = 0; i< result.results.length ; i++)
           {
             var note = model.persistence.Serializer.order_note.fromSAP(result.results[i]);
             if(note.id === "Z001")
             {
               this.salesNotes.push(note);
             }
             else if(note.id === "Z002")
             {
               this.billsNotes.push(note)
             }
             else if(note.id==="Z013")
             {
            	 this.creditNotes.push(note);
             }

           }
        }
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        sap.m.MessageToast.show("Error Loading Order Notes of " + this.orderId);
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadOrderNotes(this.orderId, fSuccess, fError);

      // .then(fSuccess, fError);

      return defer.promise;
    };

    this.loadLinks = function () {
      var defer = Q.defer();
      var req = {"Vbeln":this.orderId};
      var fSuccess = function (result) {
        if (result && result.results && result.results.length > 0)
        {
          this.links=[];
           for(var i = 0; i< result.results.length ; i++)
           {
             var link = model.persistence.Serializer.link.fromSAP(result.results[i]);
            //  link.url= icms.Component.getMetadata().getConfig().settings.serverUrl+"SO_ContentAttachSet(IObjId='"+link.attachId+"')/$value";
            link.url= sessionStorage.getItem("serverUrl")+"SO_ContentAttachSet(IObjId='"+link.attachId+"')/$value";

            this.links.push(link);


           }
        }
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        this.links=[];
        sap.m.MessageToast.show("Error Loading Links of " + this.orderId);
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadAttachmentLinks(req, fSuccess, fError);

      // .then(fSuccess, fError);

      return defer.promise;
    };
    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
        if(prop == "validDateList" || prop == "requestedDate" || prop == "fullEvasionAvailableDate")
        {
        	this[prop] = new Date(this[prop]);
        }
      }
      for(var i=0; this.positions && this.positions.length > 0 && i < this.positions.length; i++)
      {
    	  this.positions[i]= new model.Position(this.positions[i]);
      }
    };




    this.loadDeliveries = function()
    {
      var defer = Q.defer();
      var req = {"VbelnSo": this.orderId};
      var fSuccess = function(result)
      {
        this.deliveries=[];
        if(result && result.results.length>0)
        {
          for(var i = 0; i < result.results.length; i++)
          {
            this.deliveries.push(new model.Delivery(model.persistence.Serializer.delivery.fromSAP(result.results[i])));
          }
        }
        defer.resolve(this.deliveries);
      }
      fSuccess = _.bind(fSuccess, this);
      var fError = function (err)
      {
        this.deliveries = [];
        defer.reject(err);
      }
      fError=_.bind(fError, this);

      model.odata.chiamateOdata.loadDeliveries(req, fSuccess, fError);

      return defer.promise;

    };


    // this.loadAttach:function(attachId)
    // {
    //   var defer = Q.defer();
    //   var fSuccess = function(result)
    //   {
    //     sap.m.URLHelper.redirect(this._getVal(evt), true);
    //   };
    //   fSuccess = _.bind(fSuccess, this);
    //   var fError = function(err)
    //   {
    //
    //   }
    //   fError = _.bind(fError, this);
    //
    //   model.odata.chiamateOdata.showAttachment(attachId, fSuccess, fError);
    //
    //   return defer.promise;
    // };

    if (serializedData) {
      this.update(serializedData);
    }

    this.getId = function () {
      return this.orderId;
    };


    this.removePosition = function (position) {
      if (this.positions.length > 0) {
        for (var i = 0; i < this.positions.length; i++) {
          if (position === this.positions[i].positionId) {
            this.positions.splice(i, 1);
            break;
          }
        }
      }
      this.refreshFullEvasionAvailableDate();
      return;

    };
    this.refreshFullEvasionAvailableDate = function()
    {
      this.fullEvasionAvailableDate = null;
      for(var i = 0; i< this.positions.length; i++)
      {
        if(!this.positions[i].wantedDate)
        {
          this.fullEvasionAvailableDate = this.requestedDate ? new Date(this.requestedDate) : null;
          return this.fullEvasionAvailableDate;
        }

        if((this.fullEvasionAvailableDate === null) || ((this.positions[i].availableDate) && (new Date(this.fullEvasionAvailableDate) < new Date(this.positions[i].wantedDate))))
        {
          this.fullEvasionAvailableDate = new Date(this.positions[i].wantedDate);
        }

 //        if(!this.fullEvasionAvailableDate || new Date(this.fullEvasionAvailableDate) < new Date(this.positions[i].availableDate) )
 //          this.fullEvasionAvailableDate = new Date(this.positions[i].wantedDate);
 // else
 //        {
 //          this.fullEvasionAvailableDate= new Date(this.positions[i].wantedDate);
 //        }
      }
      return this.fullEvasionAvailableDate;
    };




    ///New refresh------------------------------------------------
    // if((this.fullEvasionAvailableDate === null) || ((this.positions[i].availableDate) && (new Date(this.fullEvasionAvailableDate) < new Date(this.positions[i].wantedDate))))
    // {
    //   this.fullEvasionAvailableDate = new Date(this.positions[i].wantedDate);
    // }

    //--------------------------------------
    // this.setDestination = function (destinationCode)
    // {
    //   if(destinationCode)
    //   {
    //     var d = model.collections.Destinations.getById(destinationCode);//Maybe to correct, it depends from customer's destinations
    //     this.destination = d.destination;
    //   }
    //   else
    //   {
    //     //mi prendo l'id del primo elemento dell'array delle destinazioni di questo customer
    //     var currentCustomer = model.collections.Customers.getById(this.customerCode);
    //     var destinations = currentCustomer.getDestination();
    //     if(destinations && destinations.length>0)
    //     {
    //       this.destination = destinations[0].destination;
    //     }
    //   }
    // };
    this.setDestination = function (destination) {
      this.destination = destination;
    };

    this.addNote = function (param, oEntry) {
      var note = {
        "text": oEntry.text,
        "author": oEntry.author
      };
      this[param][0] = note;
    };


    //** Francesco invia l'ordine a SAP

    this.sendToSAP = function (order) {
      var defer = Q.defer();
      utils.Busy.show();
      var sapOrder = model.persistence.Serializer.order.toSAP(order);

      var fSuccess = function (result) {

        utils.Busy.hide();
        //console.log(result);
        defer.resolve(result);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        utils.Busy.hide();
        //console.log(err);
        defer.reject(err);
      };
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.createOrder(sapOrder, fSuccess, fError);

      return defer.promise;

    };

    this.refreshOrderBySAP = function(order)
    {
      var defer = Q.defer();
      utils.Busy.show();
      var sapOrder = model.persistence.Serializer.order.toSAP(order);
      sapOrder.Simula = "X";

      var fSuccess = function (result) {

        utils.Busy.hide();
        this.refreshTotalPrices(result);
        this.refreshPositionPrices(result.SalesOrderItemSet); //-It's needed to reload the positions
        //this.reloadPositions(result.SalesOrderItemSet);
        this.refreshDiscountsPrices(result.SalesOrderConditionSet);

        //console.log(result);
        defer.resolve(result);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        utils.Busy.hide();
        //console.log(err);
        defer.reject(err);
      };
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.createOrder(sapOrder, fSuccess, fError);

      return defer.promise;

    };
    this.refreshTotalPrices = function(result)
    {
      this.netPrice = parseFloat(result.Netwr);
      this.ivaPrice = parseFloat(result.Mwsbp);
      this.transportPrice = parseFloat(result.SoTranspTot);
      this.totalPrice= parseFloat(result.SoTot);
    };
    this.reloadPositions = function(positions)
    {
      if(positions && positions.results && positions.results.length>0)
      {
        var arr = positions.results;
        this.positions = [];
        for(var i = 0; i< arr.length ; i++)
        {
          var pos = new model.Position(model.persistence.Serializer.order_item.fromSAP(arr[i]));
          pos.initializeProduct();
          this.positions.push(pos);
        }
      }
    };
    this.refreshPositionPrices = function(positions)
    {
      if(!positions || !positions.results || (positions.results.length <= 0))
      {
        return;
      }
      var arr = positions.results;
      for(var i = 0; i< arr.length; i++)
      {
        var position = this.getPositionByMatnrRequestDate(arr[i].Matnr, arr[i].Edatu);

        if(position)
        {
          position.positionId = parseInt(arr[i].Posex); //Set position to retrieve it when reading Discount
          position.refreshPricesBySAP(arr[i]);
        }
        else
        {

          //console.log("Position not found - Fuck");
        }
      }


    };

    this.refreshDiscountsPrices = function(discounts)
    {
      if(!discounts || !discounts.results || (discounts.results.length <= 0))
      {
        return;
      }
      var positionsDiscountArr = _.groupBy(discounts.results, "Posex" );
      if(positionsDiscountArr)
      {
        for(var prop in positionsDiscountArr)
        {
          var position = this.getPositionByPosex(prop);
          if(position)
        	  position.refreshDiscountsPricesBySAP(positionsDiscountArr[prop]);
          else {
            //console.log("Position not found - Fuck");
          }
        }
      }

    };
    this.getPositionByMatnr = function(matnr)
    {
      if(!this.positions || this.positions.length <= 0)
        return;

      var req = matnr
      var result = null;
      for(var i = 0; i< this.positions.length; i++)
      {
        if(this.positions[i].productId === matnr)
        {
          result = this.positions[i];
          break;
        }
      }
      return result;
    };

    this.getPositionByMatnrRequestDate = function(matnr, reqDate)
    {
      if(!this.positions || this.positions.length <= 0)
        return;

      //var req = matnr
      var result = null;
      for(var i = 0; i< this.positions.length; i++)
      {
        if(this.positions[i].productId === matnr && (utils.Date.compareDate(this.positions[i].wantedDate, reqDate)==0))
        {
          result = this.positions[i];
          break;
        }
      }
      return result;
    };
    this.getPositionByPosex= function(posex)
    {
      if(!this.positions || this.positions.length <= 0)
        return;

      var req = _.isString(posex) ? parseInt(posex) : posex;
      var result = null;
      for(var i = 0; i< this.positions.length; i++)
      {
        if(parseInt(this.positions[i].getId()) === parseInt(req))
        {
          result = this.positions[i];
          break;
        }
      }
      return result;
    };
    this.assignPositionDiscountToAll=function(position)
    {
    	if(!this.positions || this.positions.length <=1)
    	{
    		sap.m.MessageToast.show(model.i18n.getText("NO_POSITION_IN_WHICH_COPY"));
    		return;
    	}
    	for(var i= 0; i< this.positions.length; i++)
		{
    		if(this.positions[i].positionId !== position.positionId)
    		{
    			this.positions[i].copyDiscount(position.getDiscount());
    		}
		}

    };
    this.getPosition=function(posId)
    {

    	return _.find(this.positions, {positionId:posId});
    };




//** destinazione alternativa
    this.setAlternativeDestination = function(dest)
    {
      this.alternativeDestination = dest;
      this.destination = dest.constructor;
    };

    this.removeAlternativeDestination = function()
    {
        delete this.alternativeDestination;//Francesco
        // this.alternativeDestination = {name:"", street:"", streetNumber:"", city:"",zipCode:"",tel:"",nation:""}; //Lorenzo, I don't know the logic about it, but a delete removes the property and the ruins the binding in orderCreate

    };

    //**camion gru
    this.setCamionGruData = function(data)
    {
      this.gru = data;
    };

    this.removeCamionGruData = function()
    {
      delete this.gru;
    };

    this.setDefaultDiscounts = function(discountObj)
    {
      this.defaultDiscount = discountObj;
    };

    this.getDefaultDiscount = function()
    {
      return this.defaultDiscount? this.defaultDiscount : null;
    };

//    this.assignDiscountDefaultValueToAll = function()
//    {
//      if(!this.defaultDiscount)
//        return;
//
//      for(var i = 0; i< this.positions.length; i++)
//      {
//        this.positions[i].discount.assignDiscountsValues(this.defaultDiscount);
//      }
//      return;
//
//    };

    this.setDeliveryType = function(deliveryType)
    {
      if(deliveryType !== "11")
        this.removeCamionGruData();

      this.deliveryType = deliveryType;
    };



    //** Francesco return only data without function


    this.getData = function () {
      //**
      var order = _.cloneDeep(this.getModel().getData());
      for (var prop in order) {
        if (typeof (order[prop]) === "function") {
          delete(order[prop]);
        } else if (prop === "positions") {
          var positions = order[prop];
          for (var i = 0; i < positions.length; i++) {
            for (var positionsProp in positions[i]) {
              if (typeof (positions[i][positionsProp]) === "function") {
                delete(positions[i][positionsProp]);
              }
            }
          }
        } else if (prop === "billsNotes") {
          var billsNotes = order[prop];
          for (var i = 0; i < billsNotes.length; i++) {
            for (var billsNotesProp in billsNotes[i]) {
              if (typeof (billsNotes[i][billsNotesProp]) === "function") {
                delete(billsNotes[i][billsNotesProp]);
              }
            }
          }
        } else if (prop === "salesNotes") {
          var salesNotes = order[prop];
          for (var i = 0; i < salesNotes.length; i++) {
            for (var salesNotesProp in salesNotes[i]) {
              if (typeof (salesNotes[i][salesNotesProp]) === "function") {
                delete(salesNotes[i][salesNotesProp]);
              }
            }
          }
        }

      }
      return order;
    };
    //**
    this.deleteAttach=function(attachId)
    {
    	var defer = Q.defer();

    	var attach = _.find(this.attachments, {posId:attachId});
    	var req = {
    			"IDrunr": attach.posId,
    			"IGuid":this.guid
    	}
    	var fSuccess = function(result)
    	{
    		_.remove(this.attachments, {posId:attachId});
    		defer.resolve(result);
    	}
    	fSuccess = _.bind(fSuccess, this);

    	var fError = function(err)
    	{
    		defer.reject(err);
    	}
    	fError = _.bind(fError, this);

    	model.odata.chiamateOdata.deletePreAttach(req, fSuccess, fError);
    	return defer.promise;
    };
    this.loadDefaultValues = function(req)
    {
      var deferDefault = Q.defer();

      var fSuccess = function(res)
      {
        var result = model.persistence.Serializer.loadDefault.fromSAPItems(res);
        //assegnare alla proprietà resa2 il valore dall' array result.
        var obj = _.find(result.results,{attributeName : "INCO2"});
        this.resa2 = obj.attributeValue;
        deferDefault.resolve(this);

      };
      var fError = function(err)
      {
        deferDefault.reject(err);
      };

      fSuccess = _.bind(fSuccess,this);
      fError = _.bind(fError,this);

      model.odata.chiamateOdata.loadDefaultValues(req,fSuccess,fError);

      return deferDefault.promise;
    };
//--------------------Version 2.0-----------------------------------------
    this.loadDiscount_v2 = function()
    {
      var defer = Q.defer();

      var fSuccess = function(res)
      {
        if(this.offerDiscounts)//Temp
        {
          defer.resolve(this);
        }
        else
        {
          this.offerDiscounts = [];
          var result = model.persistence.Serializer.discount_v2.fromSAPItems(res);
          if(result && result.results && result.results.length > 0)
          {
            //-----------Temp Mock -------------------------
              var nationalDiscount = _.find(result.results, {typeId:"MWST"});

              nationalDiscount =new model.DiscountItem(nationalDiscount);
              nationalDiscount.typeId="ZSS1";
              nationalDiscount.typeDescr="Sconto Base";
              nationalDiscount.value=35;
              nationalDiscount.isEditable=false;
              this.offerDiscounts.push(nationalDiscount);


              var regioDiscount = _.find(result.results, {typeId:"ZCTA"});
              regioDiscount = new model.DiscountItem(regioDiscount);
	            regioDiscount.typeId="ZSS2";
	            regioDiscount.typeDescr="Sconto Addizionale";
	            regioDiscount.value = Math.floor(Math.random()*40 +1);
	            regioDiscount.isEditable=false;
	            this.offerDiscounts.push(regioDiscount);


//              var customerDiscount = _.find(result.results, {typeId:"ZPBA"});
//              customerDiscount = new model.DiscountItem(customerDiscount);
//                customerDiscount.typeId="ZPSC";
//                customerDiscount.typeDescr="Sconto Cliente";
//                customerDiscount.isEditable=true;
//                customerDiscount.scale="%";
//                customerDiscount.value = Math.floor(Math.random()*40 +1);
//                customerDiscount.maxValue = Math.floor(Math.random()*10 + customerDiscount.value);
//                this.offerDiscounts.push(customerDiscount)
	            
	            var customerDiscount = _.find(result.results, {typeId:"ZSL3"});
                customerDiscount = new model.DiscountItem(customerDiscount);
                  customerDiscount.typeId="ZSL3";
                  customerDiscount.typeDescr="Sconto Cliente";
                  customerDiscount.isEditable=true;
                  customerDiscount.scale="%";
                  //Before-------------------------------------------
//                  customerDiscount.value = Math.floor(Math.random()*40 +1);
//                  customerDiscount.limiter=  customerDiscount.value;
                  //After------------------------------------------------------
                  
                  customerDiscount.limiter=  Math.floor(Math.random()*40 +1);
                  customerDiscount.value = customerDiscount.limiter + Math.floor(Math.random()*(customerDiscount.maxValue)+1);
                  
                  //-------------------------------------------------
                  //customerDiscount.maxValue =customerDiscount.maxValue + customerDiscount.value;
                  customerDiscount.maxValue =customerDiscount.maxValue;
                  customerDiscount.string = "(limite: "+customerDiscount.limiter+", max:"+customerDiscount.maxValue+")";
                  this.offerDiscounts.push(customerDiscount)

//              var orderDiscount = _.find(result.results, {typeId:"ZPSA"});
//              orderDiscount = new model.DiscountItem(orderDiscount);
//                orderDiscount.typeId="ZPSA";
//                orderDiscount.typeDescr="Sconto Discrezionale";
//                orderDiscount.isEditable=true;
//                orderDiscount.maxValue = Math.floor(Math.random()*10 + orderDiscount.value);
//                this.offerDiscounts.push(orderDiscount);

                var orderDiscount = _.find(result.results, {typeId:"ZSL4"});
                orderDiscount = new model.DiscountItem(orderDiscount);
                  orderDiscount.typeId="ZSL4";
                  orderDiscount.typeDescr="Sconto Discrezionale";
                  orderDiscount.isEditable=true;
                  //Before--------------------------------
//                  orderDiscount.value=0;
//                  orderDiscount.maxValue=20;
                  //After-----------------------------------
                  orderDiscount.value=Math.floor(Math.random()*20+1);
                  orderDiscount.maxValue=20;
                  orderDiscount.minValue=10;//It will be another price voice
                  //---------------------------------------------
                  orderDiscount.string = "(max: "+orderDiscount.maxValue+")";
                  this.offerDiscounts.push(orderDiscount);


            //----------------------------------------------
            //this.offerDiscounts = result.results;
          }
          defer.resolve(this);

        }

      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {
        this.offerDiscounts=[];
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadDiscounts_v2(this.orderId, fSuccess, fError)

      return defer.promise;
    };
    this.updateDiscounts = function(discountsArray)
    {
      this.offerDiscounts = discountsArray;
    };
    //--------------------Discount Management----------------------------------
//    this._discount = {"value":"", "status":"", "statusDescr":""};
//	
//	this.initialize= function(value, status)
//	{
//		this._discount.value = value;
//		(status)? this._setStatus(status) : this._setStatus("INS");
//		
//		
//	};
	
	this.setOfferDiscountStatus=function(user, refused, value)
	{
//		if(!this._discount.value)
//		{
//			console.log("discount must be initialize first");
//			return;
//		}
			
		
		if(user.type =="AG")
		{
			if(!value)
				return null;
			else
			{
				//this._discount.value = value;
				this._setStatus("INS");
			}
		}
		else if(refused)
		{
			this._setStatus("REJ");
		}
		else if(user.type == "TM")
		{
			if(!value)//Yet To check if discount equals to the past discount
			{
				this._setStatus("ATM");
			}
			else
			{
				//this._discount.value = value;
				this._setStatus("MTM");
				
			}
		}
		else if(user.type == "AM")
		{
			if(!value)// || (this._discount.value == value))
			{
				this._setStatus("AAM");
			}
			else
			{
//				this._discount.value = value;
				this._setStatus("MAM");
				
			}
		}
	};
	
	this._setStatus = function(status)
	{
		this.orderStatus=status;
		this.orderStatusDescr = this.getStatusDescription(status);
	};
	
	this.getStatusDescription = function(status)
	{
		if(!status)
		{
			console.log("Error: status undefined");
			return null;
		}
		var result = "";
		
		switch(status){
		
		case "AAM" : 
			result = model.i18n.getText("Approvato AM");
			break;
			
		case "MAM":
			result=model.i18n.getText("Approvato AM Con Modifiche");
			break;
		
		case "ATM":
			result=model.i18n.getText("Approvato TM");
			break;
		
		case "MTM":
			result=model.i18n.getText("Approvato TM Con Modifiche");
			break;
		
		case "INS":
			result=model.i18n.getText("Inserito");
			break;
		case "REJ":
			result=model.i18n.getText("refused");
			break;
			
		}
		return result;
	};
//	this.getDiscountValue=function()
//	{
//		return this._discount.value;
//	};
	this.getOrderStatus=function()
	{
		return this.orderStatus;
	};
	this.getOrderStatusDescr=function()
	{
		return this.orderStatusDescr;
	};
	this.isEditable= function(user)
	{
		
//		if(user.type=="AG" || user.type == "TM")
//		{
//			if(this.orderStatus !== "INS")
//				return (this.orderStatus == "REJ") && (user.type == "AG");
//			else
//				return true;
//		}
		if(user.type == "AG")
			return (this.orderStatus == "REJ") //|| this.orderStatus == "");
		else if(user.type == "TM")
			return (this.orderStatus == "INS")
		else if(user.type=="AM")
		{
			if(this.orderStatus == "INS" || this.orderStatus == "REJ")
				return false;
			else
				return true;
		}
	};
	this.isConvertible = function()
	{


		return (this.orderStatus == "APP" || this.orderStatus == "MPP" || this.orderStatus == ""); //|| this.orderReason == "NWF"


	};
    //------------------------------------------------------------------------
    
      this.transformToOrder = function(offer)
      {
          var defer = Q.defer();
          
          var sapOffer = model.persistence.Serializer.offer.toSAP(offer);
          var fSuccess = function(res){
              
              defer.resolve(res);
          };
          var fError = function(err)
          {
              defer.reject(err);
          };
          
          
          model.odata.chiamateOdata.transformToOrder(sapOffer,fSuccess,fError);
          return defer.promise;
      };
    

//-----------------------Version 2.0 End-------------------------------------------
    return this;
  };

  return Offer;


})();
