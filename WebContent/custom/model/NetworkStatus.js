jQuery.sap.declare("model.NetworkStatus");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.i18n");

model.NetworkStatus = {
		
		_networkStatusSet : [],
		_defer : Q.defer(),
		loadNetworkStatus : function()
		{
			this._defer = Q.defer();
			
			var fSuccess = function(res)
			{
				this._newtworkStatusSet = [];
				for(var i = 0 ; res && res.results && i< res.results.length ; i++ )
				{
					this._networkStatusSet.push(res.results[i]);
				}
				model.persistence.Storage.session.save("networkStatus", this._networkStatusSet);
				
				this._defer.resolve(this._networkStatusSet);
			}
			fSuccess = _.bind(fSuccess, this);
			
			var fError = function(err)
			{
				this._networkStatusSet = [];
				model.persistence.Storage.session.remove("networkStatus");
				this._defer.reject(err);
			}
			fError = _.bind(fError, this);
			
			model.odata.chiamateOdata.loadNetworkStatus(fSuccess, fError);
			
			
			
			return this._defer.promise;
		},
		getStatusDescr : function(statusId, type)
		{
			var res = ""
			if(!this._networkStatusSet || this._networkStatusSet.length == 0)
				this._networkStatusSet = model.persistence.Storage.session.get("networkStatus");
			
			
			var status = _.find(this._networkStatusSet, {Stato : statusId, Flowt: type});
			
			if(!status && type=="A")
				return model.i18n.getText("discountToComplete");
			
			res = status ? status.StatoDescr : "";
			return res;
			
		}
}