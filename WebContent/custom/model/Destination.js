jQuery.sap.declare("model.Destination");

model.Destination = (function () {

  Destination = function (serializedData) {
    this.address = undefined;
    this.city = undefined;
    this.cityCode = undefined;
    this.company = undefined;
    this.country = undefined;
    this.numAddr = undefined;
    //    this.country_text = undefined;
    this.customer = undefined; //codice cliente
    this.codiceDestinazione = undefined; //codice destinazione
    //    this.destination_text  = undefined;
    //    this.distribution_channel = undefined;
    //    this.division = undefined;
    this.email = undefined;
    this.fax = undefined;
    this.postal_code = undefined;
    this.region = undefined;
    //    this.region_text = undefined;
    //    this.sales_organization = undefined;
    this.telephone = undefined;
    this.regio = undefined;
    //**
    this.destinations = [];
    
    this.loadDestinations = function (params) {
      var defer = Q.defer();
      var fSuccess = function (result) {
        //console.log(result);
        this.destinations = [];

        if (result && result.results && result.results.length > 0) {
          for (var i = 0; i < result.results.length; i++) {
            var destination = model.persistence.Serializer.destination.fromSAP(result.results[i]);
            this.destinations.push(destination);
          }

        }
        defer.resolve(this.destinations);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        this.destinations = [];
        //console.log("Error loading Customers");
        defer.reject(err);
      };
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.getDestinations(params, fSuccess, fError);
      return defer.promise;
    };
    //**

    
    //**
    this.addDestination = function(req)
    {
      var defer = Q.defer();
      
      var destination = model.persistence.Serializer.destination.toSAP(req);
      var fSuccess = function(res) {
      
      defer.resolve(res);
    };
    var fError = function (err) {
      defer.reject(err);
    };
    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);

    model.odata.chiamateOdata.addNewDestination(destination,fSuccess,fError);
      
      
      return defer.promise;
    };
    //**
    
    
    
    this.initialize = function (codice) {
      this.customer = codice;
    };

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };

    this.getId = function () {
      return this.codiceDestinazione;
    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };
    
    
    
    //se viene passato un oggetto, vengono settati i dati, else viene settato solo il codice cliente.
    if (_.isObject(serializedData)) {
      this.update(serializedData);
    } else {
      this.initialize(serializedData);
    }

    return this;
  };
  return Destination;


})();