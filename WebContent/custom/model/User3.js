jQuery.sap.declare("model.User");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.odata.chiamateOdata");


model.User = ( function (){

  User  = function(data)
  {
    this.username = "";
    this.password  ="";
    this.fullName = "";
    this.customer = "";
    this.name1="";
    this.mail ="";
    this.type = "agent";
    this.organizationData =[];

    this.setCredential = function(mail, pwd)
    {
      this.mail = mail;
      this.password = pwd;
    };

    this.getNewCredential = function()
    {
      this.mail = "";
      this.password = "";
      return new sap.ui.model.json.JSONModel({"username": this.mail, "password": this.password});
    };


    this.getCredential = function()
    {
      return {"username": this.mail, "password": this.password};
    };

    this.update=function(data)
    {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

    this.setUserInfo = function(id)
    {
      var defer = Q.defer();
      var fSuccess = function(result)
      {
        result = _.map(result, _.bind(function(item)
        {
          return model.persistence.Serializer.userInfo.fromSAP(item.results);
        }, this));

        var userInfo = _.find(result, { mail: this.mail });
        if(!userInfo)
          defer.reject("userInfo not found");
        else
        {
          this.update(userInfo);
          defer.resolve(userInfo);
          //console.log("userInfo");
        }
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {
        defer.reject(err);
      };
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.getUserById(id,fSuccess,fError);

      return defer.promise;

    };

    this.getModel = function()
    {
      return new sap.ui.model.json.JSONModel(this);

    };


    this.doLogin = function(u,p)
    {
        var loginDefer = Q.defer();
        utils.Busy.show();

        var fSuccess = function(result)
        {
          utils.Busy.hide();
          var user = model.persistence.Serializer.userInfo.fromSAP(result.results);
          model.persistence.Storage.session.save("user", user);
          loginDefer.resolve(user);
          

        };
        fSuccess = _.bind(fSuccess, this);

        var fError  = function(err)
        {
          utils.Busy.hide();
          loginDefer.reject(err);
        };
        fError = _.bind(fError, this);


        model.odata.chiamateOdata.getUserById(u, fSuccess, fError);

        return loginDefer.promise;
      };

      if(data)
        this.update(data);

      return this;
  };


return User;



// return {
//
//   setCurrentUser: function(userInfo)
//   {
//     _username = userInfo.username ;
//     _password = userInfo.password ;
//     _type = userInfo.type;
//
//   },
//   getCurrentUser: function()
//   {
//     return this;
//   },
//   getUsername : function()
//   {
//     return this._username;
//   },
//   setUserName : function(value)
//   {
//     this._username = value;
//   },
//   setPassword : function(value)
//   {
//     this._password  = value;
//   },
//
//   getNewCredential : function()
//   {
//     this._username = "";
//     this._password = "";
//     return this.getModel();
//   },
//
//   getModel:function()
//   {
//     var model = new sap.ui.model.json.JSONModel();
//     var user = this.getCurrentUser();
//     model.setData(user);
//     return model;
//   },
//
//   doLogin : function()
//   {
//
//       var defer = Q.defer();
//       utils.Busy.show();
//
//       var fSuccess = function(result)
//       {
//         utils.Busy.hide();
//         var user = _.find(result, { username: this._username , password : this._password});
//         if(!user)
//           defer.reject("user not found");
//         else {
//           this.setCurrentUser(user);
//           //console.log("userInLogin");
//           //console.log(user);
//
//           model.persistence.Storage.session.save("user", user);
//           defer.resolve(user);
//         }
//       };
//       var fError = function(err)
//       {
//         utils.Busy.hide();
//         defer.reject(err);
//       };
//       fSuccess = _.bind(fSuccess, this);
//       fError = _.bind(fError,this);
//
//       $.getJSON("custom/model/mock/data/users.json")
//         .success(fSuccess)
//         .fail(fError);
//
// //---------------To use when linked to SAP/HanaDB--------------------//
//
//
//       //   var tok = username + ':' + password;
// 			// var hash = btoa(tok);
// 			// var auth = "Basic " + hash;
//       //
// 			// //console.log(hash);
// 			// //console.log(auth);
//       //
// 			// $.ajaxSetup({
// 			// 	headers: {
// 			// 		"Authorization": auth
// 			// 	}
// 			// });
//         // var url = setting.Core.serverUrl + "ZFIORI_SRV/$metadata"
//         //
//   			// $.ajax({
//   			// 	url: url,
//   			// 	type: "GET", //or POST?
//   			// 	// dataType: "jsonp",
//   			// 	xhrFields: {
//   			// 		withCredentials: true
//   			// 	},
//   			// 	beforeSend: function (request) {
//   			// 		request.setRequestHeader("Authorization", auth);
//   			// 	},
//   			// 	success: success,
//   			// 	error: error
//   			// });
// //-------------------------------------------------------------------------------//
//
//       return defer.promise;
//     },
//
//     sessionSave : function()
//     {
//       var user = {username : this._username, type: this._type};
//       model.persistence.Storage.session.save("user", user );
//     }
//
//
//
//
//
//   };

  })();
