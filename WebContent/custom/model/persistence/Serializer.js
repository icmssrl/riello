jQuery.sap.declare("model.persistence.Serializer");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.NetworkStatus");
model.persistence.Serializer = {
    customer: {
        fromSAP: function (sapData) {
            var c = {};
            //------From Odata-------------------------------------------------
            c.isBlocked = (sapData.IsBlocked === "X" || sapData.Isblocked === "X");
            //      c.altroDestFlag= sapData.altroDest ? (sapData.altroDest === "X") : true;
            //      c.cantiereFlag=sapData.cantiere ? (sapData.cantiere === "X"): true;
            c.registry = {};
            c.registry.userId = sapData.Uname ? sapData.Uname : "";
            c.registry.fioriUserType = sapData.Ustyp ? sapData.Ustyp : "";
            c.registry.society = sapData.Bukrs ? sapData.Bukrs : "";
            c.registry.salesOrg = sapData.Vkorg ? sapData.Vkorg : "";
            c.registry.distrCh = sapData.Vtweg ? sapData.Vtweg : "";
            c.registry.division = sapData.Spart ? sapData.Spart : "";
            c.registry.divisionName = sapData.divisionName ? sapData.divisionName : ""; //
            c.registry.areaManager = sapData.Vkbur ? sapData.Vkbur : "";
            c.registry.areaManagerName = sapData.VkburBezei ? sapData.VkburBezei : "";
            c.registry.territoryManager = sapData.Vkgrp ? sapData.Vkgrp : "";
            c.registry.territoryManagerText = sapData.VkgrpBezei ? sapData.VkgrpBezei : "";
            c.registry.agentCode = sapData.Cdage ? sapData.Cdage : "";
            c.registry.agentName = sapData.CdageName1 ? sapData.CdageName1 : "";
            c.registry.id = sapData.Kunnr ? sapData.Kunnr : "";
            c.registry.recordType = sapData.Rctyp ? sapData.Rctyp : "";
            c.registry.customerName = sapData.Name1 ? sapData.Name1 : "";
            c.registry.companyName = sapData.Name1 ? sapData.Name1 : "";
            c.registry.userName = sapData.Name2 ? sapData.Name2 : ""; //
            c.registry.companyName2 = sapData.Name2 ? sapData.Name2 : ""; //
            c.registry.land = sapData.Land1 ? sapData.Land1 : "";
            c.registry.nation = sapData.landName ? sapData.landName : ""; //
            c.registry.nation = sapData.Country ? sapData.Country : c.registry.nation; //maybe to change
            c.registry.city = sapData.Ort01 ? sapData.Ort01 : "";
            c.registry.city = sapData.City1 ? sapData.City1 : c.registry.city;
            c.registry.postalCode = sapData.Pstlz ? sapData.Pstlz : "";
            c.registry.postalCode = sapData.PostCode1 ? sapData.PostCode1 : c.registry.postalCode;
            c.registry.prov = sapData.Regio ? sapData.Regio : ""; //
            c.registry.prov = sapData.Region ? sapData.Region : c.registry.prov;
            c.registry.street = sapData.Stras ? sapData.Stras : "";
            c.registry.street = sapData.Street ? sapData.Street : c.registry.street;
            c.registry.numAddr = sapData.HouseNum1 ? sapData.HouseNum1 : "";
            c.registry.taxCode = sapData.Stcd1 ? sapData.Stcd1 : ""; //
            c.registry.salesOrgName = sapData.salesOrgName ? sapData.salesOrgName : ""; //
            c.registry.VATNumber = sapData.Stceg ? sapData.Stceg : "";
            c.registry.localVATNum = sapData.Stcd2 ? sapData.Stcd2 : "";
            c.registry.segCode = sapData.Katr6 ? sapData.Katr6 : "";
            c.registry.segCodeDescr = sapData.Katr6Vtext ? sapData.Katr6Vtext : "";
            c.registry.isPublicAdministration = false;
            c.registry.aufsd0 = sapData.Aufsd0 ? sapData.Aufsd0 : ""; //Blocco Screener
            c.registry.aufsdk = sapData.Aufsdk ? sapData.Aufsdk : ""; //Blocco Amministrativo
            c.sales = {};
            c.sales.clientType = sapData.Kdgrp ? sapData.Kdgrp : "";
            c.sales.clientTypeDescr = sapData.KdgrpKtext ? sapData.KdgrpKtext : "";
            c.sales.billType = sapData.Zwels ? sapData.Zwels : ""; //Metodo di Pagamento?
            c.sales.billTypeDescr = sapData.ZwelsText1 ? sapData.ZwelsText1 : "";
            c.sales.billFreq = sapData.Perfk ? sapData.Perfk : "";
            c.sales.billFreqDescr = sapData.PerfkLtext ? sapData.PerfkLtext : "";
            c.sales.paymentCond = sapData.ZtermArea ? sapData.ZtermArea : "";
            c.sales.paymentCondDescr = sapData.ZtermAreaText1 ? sapData.ZtermAreaText1 : "";
            c.sales.paymentCond2 = sapData.Zterm ? sapData.Zterm : c.sales.paymentCond;
            c.sales.paymentCond2Descr = sapData.ZtermText1 ? sapData.ZtermText1 : c.sales.paymentCond2Descr;
            c.sales.resa = sapData.Inco1 ? sapData.Inco1 : "";
            c.sales.resaDescr = sapData.Inco1Bezei ? sapData.Inco1Bezei : "";
            c.sales.incoterms2 = sapData.Inco2 ? sapData.Inco2 : "";
            c.sales.carrier = sapData.Bzirk ? sapData.Bzirk : "";
            c.sales.carrierDescr = sapData.BzirkBztxt ? sapData.BzirkBztxt : "";
            c.sales.transport = sapData.Msped ? sapData.Msped : ""; //Mezzo di spedizione
            c.sales.transportDescr = sapData.MspedName1 ? sapData.MspedName1 : "";
            c.sales.notes = sapData.NotaZ001 ? sapData.NotaZ001 : "";
            c.sales.aufsdv = sapData.Aufsdv ? sapData.Aufsdv : ""; //Blocco Commerciale
            c.bank = {};
            c.bank.iban = sapData.Iban ? sapData.Iban : "";
            // c.bank.bankId = sapData.Bankl ? sapData.Bankl : "";
            c.bank.descr = sapData.Banka ? sapData.Banka : "";
            c.bank.bankNation = sapData.Banks ? sapData.Banks : "";
            c.bank.accountNum = sapData.Bankn ? sapData.Bankn : "";
            c.bank.abicab = sapData.Bankl ? sapData.Bankl : "";
            c.bank.cin = sapData.Bkont ? sapData.Bkont : "";
            c.bank.Bvtyp = sapData.Bvtyp ? sapData.Bvtyp : "";
            c.contact = {};
            c.contact.phone = sapData.Telf1 ? sapData.Telf1 : "";
            c.contact.phone = sapData.TelNumber ? sapData.TelNumber : c.contact.phone;
            c.contact.mobile = sapData.mobile ? sapData.mobile : "";
            c.contact.mobile = sapData.MobNumber ? sapData.MobNumber : c.contact.mobile;
            c.contact.fax = sapData.Telfx ? sapData.Telfx : "";
            c.contact.fax = sapData.FaxNumber ? sapData.FaxNumber : c.contact.fax;
            c.contact.contactType = sapData.type ? sapData.contactType : "";
            c.contact.mail = sapData.SmtpAddr ? sapData.SmtpAddr : "";
            c.enables = {};
            c.enables.registry = (sapData && sapData.Xanag && sapData.Xanag == "X") ? false : true;
            c.enables.contacts = (sapData && sapData.Xcont && sapData.Xcont == "X") ? false : true;
            c.enables.bank = (sapData && sapData.Xbank && sapData.Xbank == "X") ? false : true;
            c.enables.sales = (sapData && sapData.Xvend && sapData.Xvend == "X") ? false : true;
            c.registry.isPublicAdministration = (sapData && sapData.Xispa && sapData.Xispa == "X") ? true : false;
            c.registry.village = sapData.City2;
            //-----Version 2.0.2--Odata Mapping-----------------------
            c.discountStatus = sapData.Zsl3Stat;
            c.discountStatusDescr = model.NetworkStatus.getStatusDescr(c.discountStatus, "A");
            c.discountStartDate = sapData.Zsl3Datab;
            c.discountExpireDate = sapData.Zsl3Datbi;
            c.avgAgOrderDiscount = sapData.Zsu4 ? Math.abs(parseFloat(sapData.Zsu4)):"";
            
            c.validDiscountFlag = c.discountExpireDate > new Date();
            c.customerDiscountConditions = [];
            //---------------Customer Discount Array ------------------
            var nationalDiscount = {
                "typeId": "ZSL1"
                , "typeDescr": model.i18n.getText("ZSL1")
                , "value": sapData.Zsl1
                , "note": ""
                , "status": ""
                , "statusDescr": ""
                , "proposedValue": ""
                , "proposedNote": ""
                , "isEditable": false
            };
            c.customerDiscountConditions.push(nationalDiscount);
            var additionalDiscount = {
                "typeId": "ZSL2"
                , "typeDescr": model.i18n.getText("ZSL2")
                , "value": sapData.Zsl2
                , "note": ""
                , "status": ""
                , "statusDescr": ""
                , "proposedValue": ""
                , "proposedNote": ""
                , "isEditable": false
            , };
            c.customerDiscountConditions.push(additionalDiscount);
            var customerDiscount = {
                "typeId": "ZSL3"
                , "typeDescr": model.i18n.getText("ZSL3")
                , "value": sapData.Zsl3Actual
                , "note": sapData.Zsl3NoteActual
                , "status": sapData.Zsl3Stat
                , "statusDescr": model.NetworkStatus.getStatusDescr(sapData.Zsl3Stat, "A")
                , "proposedValue": sapData.Zsl3Age
                , "proposedNote": sapData.Zsl3NoteAge
                , "isEditable": true
            , };
            c.customerDiscountConditions.push(customerDiscount);
            return c;
        }
        , toSAP: function (c) {
            var s = {};
            s.Kunnr = c.registry.id ? c.registry.id : "";
            s.Bukrs = c.registry.society;
            s.Vkorg = c.registry.salesOrg;
            s.Vtweg = c.registry.distrCh;
            s.Spart = c.registry.division;
            s.Cdage = c.registry.agentCode;
            s.Vkbur = c.registry.areaManager;
            s.Vkgrp = c.registry.territoryManager;
            s.Name1 = c.registry.customerName;
            s.Street = c.registry.street;
            s.HouseNum1 = c.registry.numAddr;
            s.PostCode1 = c.registry.postalCode;
            s.City1 = c.registry.city;
            s.City2 = c.registry.village;
            s.Country = c.registry.nation ? c.registry.nation : c.registry.land;
            s.Region = c.registry.prov;
            s.Stcd1 = c.registry.taxCode;
            s.Stcd2 = c.registry.VATNumber ? c.registry.VATNumber.substring(2, c.registry.VATNumber.length) : c.registry.localVATNum;
            s.Stceg = c.registry.VATNumber;
            s.Katr6 = c.registry.segCode;
            s.TelNumber = c.contact.phone;
            s.MobNumber = c.contact.mobile;
            s.FaxNumber = c.contact.fax;
            s.SmtpAddr = c.contact.mail;
            s.Kdgrp = c.sales.clientType;
            s.Zterm = c.sales.paymentCond; //maybe c.sales.paymentCond2
            s.ZtermArea = c.sales.paymentCond;
            s.Zwels = c.sales.billType;
            s.Bzirk = c.sales.carrier;
            s.Inco1 = c.sales.resa;
            s.Inco2 = c.sales.incoterms2;
            s.Perfk = c.sales.billFreq;
            s.NotaZ001 = c.sales.notes;
            s.Banks = c.bank.bankNation;
            s.Bankl = c.bank.abcab;
            s.Bankn = c.bank.accountNum;
            s.Bkont = c.bank.cin;
            s.Bvtyp = c.bank.Bvtyp;
            s.Iban = c.bank.iban;
            s.Xispa = c.registry.isPublicAdministration ? "X" : "";
            s.Zsl3Age="0";
            s.Zsl3NoteAge="";
            
            if(c.customerDiscountConditions && c.customerDiscountConditions.length > 0)
            {
            	var zsl3Cond = _.find(c.customerDiscountConditions, {
                    typeId: "ZSL3"
                });
            	if(zsl3Cond && zsl3Cond.value)
            		s.Zsl3Age= zsl3Cond.value.toString();
            	if(zsl3Cond && zsl3Cond.note)
            		s.Zsl3NoteAge=zsl3Cond.note;
            }
//            s.Zsl3Age = _.find(c.customerDiscountConditions, {
//                typeId: "ZSL3"
//            }).value ? (_.find(c.customerDiscountConditions, {
//                typeId: "ZSL3"
//            }).value).toString() : 0;
//            s.Zsl3NoteAge = _.find(c.customerDiscountConditions, {
//                typeId: "ZSL3"
//            }).note ? _.find(c.customerDiscountConditions, {
//                typeId: "ZSL3"
//            }).note : "";
            return s;
        }
        , updateToSAP: function (c) {
            var d = {};
            d.Kunnr = c.registry.id;
            d.Bukrs = c.registry.society;
            d.Vkorg = c.registry.salesOrg;
            d.Vtweg = c.registry.distrCh;
            d.Spart = c.registry.division;
            d.Cdage = c.registry.agentCode;
            d.Street = c.registry.street;
            d.HouseNum1 = c.registry.numAddr;
            d.TelNumber = c.contact.phone;
            d.MobNumber = c.contact.mobile;
            d.FaxNumber = c.contact.fax;
            d.SmtpAddr = c.contact.mail;
            d.City1 = c.registry.city;
            d.City2 = c.registry.village;
            d.NotaZ001 = c.sales.notes;
            d.Region = c.registry.prov;
            d.PostCode1 = c.registry.postalCode;
            d.Name1 = c.registry.customerName;
            //d.Zterm = c.sales.paymentCond;
            //d.ZtermArea = c.sales.paymentCond;
            d.Zwels = c.sales.billType;
            // d.Inco1 = c.sales.resa;
            // d.Inco2 = c.sales.incoterms2;
            d.Perfk = c.sales.billFreq;
            d.NotaZ001 = c.sales.notes;
            d.Banks = c.bank.bankNation;
            d.Bankl = c.bank.abicab;
            d.Bankn = c.bank.accountNum;
            d.Bkont = c.bank.cin;
            d.Bvtyp = c.bank.Bvtyp;
            d.Iban = c.bank.iban;
            return d;
        }
    }
    , customerStatus: {
        fromSAP: function (sapData) {
            var cs = {};
            cs.customerId = sapData.MKunnr ? sapData.MKunnr : 0;
            cs.totalDebt = sapData.totalDebt ? sapData.totalDebt : 0;
            cs.totalOrder = sapData.totalOrder ? sapData.totalOrder : 0;
            cs.usedDebt = sapData.MPfatt ? sapData.MPfatt : 0; //fatture
            //cs.payedOrder = sapData.payedOrder ? sapData.payedOrder : 0;
            cs.society = sapData.MBukrs ? sapData.MBukrs : 0;
            cs.payedOrder = sapData.MKlprz ? sapData.MKlprz : 0; //fido
            return cs;
        }
        , toSap: function (data) {}
    }
    , destination: {
        fromSAP: function (sapData) {
            var d = {};
            d.address = sapData.Street ? sapData.Street : "";
            d.numAddr = sapData.HouseNum1 ? sapData.HouseNum1 : "";
            d.city = sapData.City1 ? sapData.City1 : "";
            d.codiceDestinazione = sapData.KunnrWe ? sapData.KunnrWe : "";
            //      d.cityCode = sapData.CityCode ? sapData.CityCode : "";
            d.cityCode = sapData.PostCode1 ? sapData.PostCode1 : "";
            d.company = sapData.Name1 ? sapData.Name1 : "";
            d.country = sapData.Country ? sapData.Country : "";
            d.prov = sapData.Region ? sapData.Region : "";
            d.customer = sapData.KunnrAg ? sapData.KunnrAg : "";
            return d;
        }
        , fromCustomer: function (customerData) {
            var d = {};
            d.address = customerData.registry.street + ", " + customerData.registry.numAddr;
            d.city = customerData.registry.city;
            d.company = customerData.registry.companyName;
            d.country = customerData.registry.nation;
            //d.country_text = sapData.country_text;
            d.customer = customerData.registry.id;
            d.destination = "" //codice destinazione
                //d.destination_text = sapData.destination_text;
                //d.distribution_channel = sapData.distribution_channel;
                //d.division = sapData.division;
            d.email = customerData.contact.mail;
            d.fax = customerData.contact.fax;
            d.postal_code = customerData.registry.postalCode;
            d.region = customerData.registry.prov;
            //d.region_text = sapData.region_text;
            //d.sales_organization = sapData.sales_organization;
            d.telephone = customerData.contact.phone;
            return d;
        }
        , toSAP: function (d) {
            var toSapData = {};
            toSapData.Cdage = d.Cdage;
            toSapData.KunnrAg = d.KunnrAg;
            toSapData.Parvw = d.Parvw;
            toSapData.Spart = d.Spart;
            toSapData.Vkorg = d.Vkorg;
            toSapData.Vtweg = d.Vtweg;
            toSapData.City1 = d.city;
            toSapData.Street = d.street;
            toSapData.PostCode1 = d.zipCode;
            toSapData.Name1 = d.name;
            toSapData.Country = d.nation;
            toSapData.Region = d.prov;
            return toSapData;
        }
    }
    , delivery: {
        fromSAP: function (s) {
            var d = {};
            d.orderId = s.VbelnSo;
            d.deliveryId = s.Vbeln;
            d.bollaNum = s.Xabln;
            d.state = s.Stcon;
            d.goodsMov = s.WadatIst;
            d.deliveryDate = s.Lfdat;
            d.preTel = s.Pretel;
            d.contactPer = s.Persc;
            d.contactTel = s.Nrtel;
            d.sender = s.KunnrM;
            d.senderName = s.Name1M;
            d.senderStreet = s.StreetM;
            d.senderAddrNum = s.HouseNum1M;
            d.senderPostCode = s.PostCode1M;
            d.senderCity = s.City1M;
            d.senderRegion = s.RegionM;
            d.senderCountry = s.CountryM;
            d.destination = s.KunnrD;
            d.destinationName = s.Name1D;
            d.destinationStreet = s.StreetD;
            d.destinationAddrNum = s.HouseNum1D;
            d.destinationPostCode = s.PostCode1D;
            d.destinationCity = s.City1D;
            d.destinationRegion = s.RegionD;
            d.destinationCountry = s.CountryD;
            d.filiale = s.Filiale;
            return d;
        }
    }
    , delivery_item: {
        fromSAP: function (s) {
            var i = {};
            i.orderId = s.VbelnSo
            i.deliveryId = s.Vbeln
            i.trakingDate = s.Dtevt;
            i.trakingTime = s.Hrevt;
            i.opPoint = s.Ptope;
            i.descr = s.Descr;
            i.trakId = s.Cdevt;
            return i;
        }
    }
    , discount: {
        fromSAP: function (sapData) {
            var d = {};
            // d.productId = sapData.productId;
            //----------------Old-------------------------
            // d.orderId = sapData.Vbeln;
            // d.positionId = sapData.Posex;
            // d.scale = (sapData.Konwa !== "") ? sapData.Konwa : "%";
            // d.type = sapData.Kschl;
            // d.name = sapData.Vtext;
            // d.unitVal = sapData.Kbetr;
            // d.unit = sapData.Kmein;
            // d.Kpein = sapData.Kpein;
            // d.Krech = sapData.Krech;
            //
            // d.agentDiscount = {};
            // d.agentDiscount.unitVal = sapData.agentDiscount;
            // d.firstLocDiscount = {};
            // d.firstLocDiscount.unitVal = sapData.firstLocDiscount;
            // d.secondLocDiscount = {};
            // d.secondLocDiscount.unitVal = sapData.secondLocDiscount;
            // d.allegedCommRC = {};
            // d.allegedCommRC.unitVal = sapData.allegedCommRC;
            // d.addTraspRC = {};
            // d.addTraspRC.unitVal = sapData.addTraspRC;
            // d.addTraspAutomRC = {};
            // d.addTraspAutomRC.unitVal = sapData.addTraspAutomRC;
            // d.IVA = {};
            // d.IVA.unitVal = sapData.IVA;
            // d.thirdLocDiscount = {};
            // d.thirdLocDiscount.unitVal = sapData.thirdLocDiscount;
            // d.currency = sapData.currency;
            // d.agentDiscount = {};
            // d.agentDiscount.unitVal = sapData.agentDiscount;
            // d.firstLocDiscount = {};
            // d.firstLocDiscount.unitVal = sapData.firstLocDiscount;
            // d.secondLocDiscount={};
            // d.secondLocDiscount.unitVal = sapData.secondLocDiscount;
            // d.allegedCommRC={};
            // d.allegedCommRC.unitVal = sapData.allegedCommRC;
            // d.addTraspRC={};
            // d.addTraspRC.unitVal = sapData.addTraspRC;
            // d.addTraspAutomRC={};
            // d.addTraspAutomRC.unitVal = sapData.addTraspAutomRC;
            // d.IVA={};
            // d.IVA.unitVal = sapData.IVA;
            // d.thirdLocDiscount={};
            // d.thirdLocDiscount.unitVal = sapData.thirdLocDiscount;
            // d.currency = sapData.currency;
            //-----------------------------------------------------
            //----------New -----------------------------------------
            d.orderId = sapData.Vbeln;
            d.positionId = sapData.Posex;
            d.scale = (sapData.Konwa !== "") ? sapData.Konwa : "%";
            d.currency = sapData.Konwa;
            d.typeId = sapData.Kschl;
//            d.typeDescr = sapData.KschlText;
            d.typeDescr = _.find(["ZSL1", "ZSL2", "ZSL3", "ZSL4"], _.bind(function(item){ return item == d.typeId}, this)) ? model.i18n.getText(_.find(["ZSL1", "ZSL2", "ZSL3", "ZSL4"], _.bind(function(item){ return item == d.typeId}, this))) : sapData.KschlText ;

            
            d.value = Math.abs(parseFloat(sapData.Kbetr));


            d.unit = sapData.Kmein;
            d.priceUnit = sapData.Kpein;
            d.Krech = sapData.Krech;
            d.total = sapData.Kwert;
            d.price = Math.abs(parseFloat(sapData.Kwert));
            d.isEditable = sapData.Edit == "X" ? true : false;
            // d.price.unitVal = sapData.price;
            return d;
        }
        , fromSAPItems: function (ds) {
            var d = {};
            var discounts = [];
            if (ds && ds.results.length > 0) {
                var posDiscounts = _.groupBy(ds.results, "Posex");
                for (var prop in posDiscounts) {
                    var item = {}
                    item.orderId = posDiscounts[prop][0].Vbeln;
                    item.positionId = posDiscounts[prop][0].Posex;
                    item.discountArray = [];
                    for (var i = 0; i < posDiscounts[prop].length; i++) {
                        item.discountArray.push(this.fromSAP(posDiscounts[prop][i]));
                    }
                    discounts.push(item);
                }
            }
            return discounts;
        }
        , fromSAPByPos: function (data) {
            var item = {}
            if (!data || data.length <= 0) return item;
            item.orderId = data[0].Vbeln;
            item.positionId = data[0].Posex;
            item.discountArray = [];
            for (var i = 0; i < data.length; i++) {
                item.discountArray.push(this.fromSAP(data[i]));
            }
            return item;
        }, // var d = {};
        // if (ds && ds.length > 0) {
        //   d.price = (_.find(ds, {
        //     Kschl: 'ZPBA'
        //   })) ? this.fromSAP(_.find(ds, {
        //     Kschl: 'ZPBA'
        //   })) : {};
        //   d.agentDiscount = (_.find(ds, {
        //     Kschl: 'ZSAG'
        //   })) ? this.fromSAP(_.find(ds, {
        //     Kschl: 'ZSAG'
        //   })) : {};
        //   d.firstLocDiscount = (_.find(ds, {
        //     Kschl: 'ZSS1'
        //   })) ? this.fromSAP(_.find(ds, {
        //     Kschl: 'ZSS1'
        //   })) : {};
        //   d.secondLocDiscount = (_.find(ds, {
        //     Kschl: 'ZSS2'
        //   })) ? this.fromSAP(_.find(ds, {
        //     Kschl: 'ZSS2'
        //   })) : {};
        //   d.thirdLocDiscount = (_.find(ds, {
        //     Kschl: 'ZSS3'
        //   })) ? this.fromSAP(_.find(ds, {
        //     Kschl: 'ZSS3'
        //   })) : {};
        //   }
        //
        //   return d;
        // },
        fromSAPToSet: function (d) {
            var res = {};
            res.typeId = d.Kschl;
            res.typeDescr = _.find(["ZSL1", "ZSL2", "ZSL3", "ZSL4"], _.bind(function(item){ return item == res.typeId}, this)) ? model.i18n.getText(_.find(["ZSL1", "ZSL2", "ZSL3", "ZSL4"], _.bind(function(item){ return item == res.typeId}, this))) : d.KschlText ;
            res.value = d.Kbetr;
            res.price = d.Kwert;
            res.total = d.Kwert;
            res.isEditable = d.Edit == "X" ? true : false;
            res.currency = d.Waers ? d.Waers : "EUR";
            //      res.scale = d.Kmein;
            res.scale = d.Konwa;
            return res;
        }
        , fromSAPItemsToSet: function (d) {
            var res = [];
            if (d.PriceSimulationCondSet && d.PriceSimulationCondSet.results.length > 0) {
                var discounts = d.PriceSimulationCondSet.results;
                for (var i = 0; i < discounts.length; i++) {
                    res.push(this.fromSAPToSet(discounts[i]));
                }
            }
            return res;
        }
        , toSAP: function (d) {
            var o = {};
            o.Vbeln = "";
            o.Posex = (parseInt(d.positionId)).toString(); //(parseInt(pos.positionId + 1)).toString();
            o.Kschl = d.typeId;
            o.Kbetr = d.value ? (d.value).toString() : "0";
            o.Konwa = d.scale;
            o.Kpein = "0";
            return o;
        }
    }
    , hierarchyNode: {
        fromSAP: function self(sapData) {
            var n = {};
            n.society = sapData.Bukrs;
            n.salesOrg = sapData.Vkorg;
            n.distrCh = sapData.Vtweg;
            n.division = sapData.Spart;
            n.last = sapData.Ultimo;
            n.productId = sapData.Figlio ? sapData.Figlio : "";
            n.salesOrg = sapData.salesOrg ? sapData.salesOrg : "";
            n.distrCh = sapData.distrCh ? sapData.distrCh : "";
            n.division = sapData.division ? sapData.division : "";
            n.description = sapData.Ltext ? sapData.Ltext : "";
            n.parentId = sapData.Padre ? sapData.Padre : "";
            n.level = sapData.Livello ? sapData.Livello : "";
            n.productPicUrl = sapData.Path ? sapData.Path : "";
            // n.items = [];
            // if(sapData.items && sapData.items.length > 0)
            // {
            //   for(var i = 0 ; i<sapData.items.length; i++)
            //   {
            //     //Maybe to correct
            //     sapData.items[i].parentId = n.productId;
            //     n.items.push(self(sapData.items[i]));
            //   }
            // }
            return n;
        }
    , }
    , link: {
        fromSAP: function (s) {
            var l = {};
            l.orderId = s.Vbeln;
            l.attachType = s.ObjType;
            l.attachId = s.ObjId;
            l.attachDescr = s.ObjDescr;
            return l;
        }
    }
    , order: {
        fromSAP: function (sapData) {
            var o = {};
            
            
            
            //-----------Price Voices-------------------------
            o.netPrice = parseFloat(sapData.Netwr);
            o.ivaPrice = parseFloat(sapData.Mwsbp);
            o.transportPrice = parseFloat(sapData.SoTranspTot);
            o.totalPrice = parseFloat(sapData.SoTot);
            o.agentCommission = parseFloat(sapData.PrvTot);
            //------------------------------------------------
            o.orderId = sapData.Vbeln;
            o.guid = sapData.Guid ? sapData.Guid : "";
            o.rifOrder = sapData.Bstkd;
            o.salesOrg = sapData.Vkorg;
            o.society = sapData.Bukrs;
            o.distrCh = sapData.Vtweg;
            o.division = sapData.Spart;
            o.areaManager = sapData.Vkbur;
            o.territoryManager = sapData.Vkgrp;
            //-------------------------------
            o.agentCode=sapData.KunnrZr;
            //---------------------------------
            o.customerId = sapData.KunnrAg;
            o.customerName = sapData.KunnrAgName;
            o.companyName = sapData.KunnrAgName;
            o.rifOrder = sapData.Bstkd;
            o.basketType = sapData.Auart;
            o.basketTypeDescr = sapData.AuartBezei;
            o.destination = sapData.KunnrWe;
            o.destinationName = sapData.KunnrWeName;
            o.paymentMethod = sapData.Zlsch;
            o.paymentMethodDescr = sapData.ZlschText1;
            o.paymentCondition = sapData.Zterm;
            o.paymentConditionDescr = sapData.ZtermText1;
            o.resa1 = sapData.Inco1;
            o.resa1Descr = sapData.Inco1Bezei;
            o.resa2 = sapData.Inco2;
            o.layout = sapData.LayoutFattura;
            o.layoutDescr = sapData.LayoutFatturaDesc;
            o.shippingType = sapData.Vsart;
            o.shippingTypeDescr = sapData.VsartBezei;
            o.transportArea = sapData.Bzirk;
            o.transportAreaDescr = sapData.BzirkBztxt;
            o.totalEvasion = sapData.Autlf;
            o.appointmentToDelivery = sapData.Sdabw;
            o.appointmentToDeliveryDescr = sapData.SdabwBezei;
            o.deliveryType = sapData.Vsart;
            o.deliveryTypeDescr = sapData.VsartBezei;
            // o.chargeTrasport = sapData.chargeTrasport;
            o.IVACode = sapData.Taxk1;
            o.IVACodeDescr = sapData.Taxk1Vtext;
            o.validDateList = sapData.Prsdt;
            o.promoCode = sapData.Promo ? sapData.Promo : "";
            //o.contactPerson = sapData.Asldcont ? sapData.Asldcont : "";
            //o.contactPersonTel = sapData.Asld2tel ? sapData.Asld2tel : "";
            // o.requestedDate = sapData.requestedDate;
            o.orderReason = sapData.Augru;
            o.orderReasonDescr = sapData.AugruBezei;
            o.shippmentDate = sapData.Edatu;
            o.requestedDate=sapData.Edatu;//Equals?
            
            // var tmpItems = model.persistence.order_item.fromSAPItems(sapData.positions);
            // o.positions = tmpItems.items;
            // o.billNote = sapData.billNote;
            // o.salesNote = sapData.salesNote;

            
            o.orderStatus = sapData.Stat;
            o.orderStatusDescr= model.NetworkStatus.getStatusDescr(o.orderStatus, "O");
            

            //--------Version 2 --- Discount Condition Values: Max//

            o.avgCustomer = sapData.Zsm3 ? Math.abs(parseFloat(sapData.Zsm3)) : "";
  	        o.avgAgDiscount = sapData.Zsu3 ? Math.abs(parseFloat(sapData.Zsu3)) : "";
            
            o.maxCustomerDiscount =sapData.Zsx3? Math.abs(parseFloat(sapData.Zsx3)) : "";
  	         
  	         
  	        o.maxOrderDiscount = sapData.Zsx4? Math.abs(parseFloat(sapData.Zsx4)) : "";
  	        
  	        o.avgAgOrderDiscount = sapData.Zsu4 ? Math.abs(parseFloat(sapData.Zsu4)):"";
            

            //-------------------------------------------------
            o.destination = sapData.KunnrWe;
            o.Stat = sapData.Stat;
            o.orderStatusDescr = model.NetworkStatus.getStatusDescr(o.Stat, "O");
            if (sapData.KunnrWe === "CANTIERE" || sapData.KunnrWe === "ALTRODEST") {
                o.alternativeDestination = {};
                o.alternativeDestination.constructor = sapData.KunnrWe;
                o.alternativeDestination.name = sapData.WeName1;
                o.alternativeDestination.street = sapData.WeStreet;
                o.alternativeDestination.city = sapData.WeCity;
                o.alternativeDestination.prov = sapData.WeRegion;
                o.alternativeDestination.zipCode = sapData.WePostCode1;
                o.alternativeDestination.nation = sapData.WeCountry;
                o.alternativeDestination.tel = sapData.WeTelnum;
                o.alternativeDestination.prov = sapData.WeRegion;
            }
            //--------------------Optional Correction 21/07/2016-----------------------------
            else {
                o.destinationName = sapData.WeName1;
                o.destinationStreet = sapData.WeStreet;
                o.destinationCity = sapData.WeCity;
                o.destinationProv = sapData.WeRegion;
                o.destinationZipCode = sapData.WePostCode1;
                o.destinationNation = sapData.WeCountry;
                o.destinationTel = sapData.WeTelnum;
            }
            //--------------------------------------------------------------------------------
            if (o.deliveryType === "11") //if gru
            {
                var g = {};
                g.location = sapData.Grupos;
                g.length = sapData.Grulen;
                g.contact = sapData.Grucon;
                g.tel = sapData.Grutel;
                g.notes = sapData.Grutxt;
                o.gru = g;
            }
            return o;
        }
        , toSAP: function (o) {
            var toSapData = {};
            //testata Ordine
            toSapData.Vbeln = "";
            
            
            toSapData.SessionUID= model.persistence.Storage.session.get("sessionUID");
            toSapData.Guid = o.guid;
            toSapData.Bukrs = o.codiciObj.society;
            toSapData.Vkorg = o.codiciObj.salesOrg;
            toSapData.Vtweg = o.codiciObj.distrCh;
            toSapData.Spart = o.codiciObj.division;
            toSapData.Vkgrp = o.codiciObj.territoryManager;
            toSapData.Vkbur = o.codiciObj.areaManager;
            toSapData.Auart = o.basketType; //tipo ordine vendita
            //codici agente e cliente e destinazione
            toSapData.KunnrAg = o.codiciObj.customerId;
            toSapData.KunnrWe = o.destination;
            if (o.alternativeDestination) {
                var altDest = o.alternativeDestination;
                toSapData.KunnrWe = altDest.constructor ? altDest.constructor : o.destination; //deve essere il codice destinazione
                toSapData.WeName1 = altDest.name ? altDest.name : "";
                toSapData.WeStreet = !altDest.streetNumber ? altDest.street : altDest.street + " " + altDest.streetNumber;
                toSapData.WeCity = altDest.city ? altDest.city : "";
                toSapData.WePostCode1 = altDest.zipCode ? altDest.zipCode : "";
                toSapData.WeCountry = altDest.nation ? altDest.nation : "";
                toSapData.WeTelnum = altDest.tel ? altDest.tel : "";
                toSapData.WeRegion = altDest.prov ? altDest.prov : "";
            }
            toSapData.KunnrZr = o.codiciObj.agentCode;
            toSapData.Edatu = o.requestedDate;
            toSapData.Bstkd = o.rifOrder;
            toSapData.Zterm = o.paymentCondition;
            toSapData.Zlsch = o.paymentMethod;
            toSapData.Inco1 = o.resa1;
            toSapData.Inco2 = o.resa2;
            toSapData.Vsart = o.deliveryType;
            if (o.deliveryType === "11") //if gru
            {
                var g = o.gru;
                toSapData.Grupos = g.location;
                toSapData.Grulen = g.length;
                toSapData.Grucon = g.contact;
                toSapData.Grutel = g.tel;
                toSapData.Grutxt = g.notes;
            }
            toSapData.Sdabw = o.appointmentToDelivery;
            toSapData.Bzirk = o.transportArea;
            toSapData.Autlf = o.totalEvasion;
            toSapData.Taxk1 = o.IVACode;
            toSapData.Augru = o.orderReason;
            toSapData.Prsdt = o.validDateList;
            toSapData.Promo = o.promoCode;
            toSapData.Asldcont = o.contactPerson;
            toSapData.Asld2tel = o.contactPersonTel;
            toSapData.LayoutFattura = o.layout;
            //
            //righe d'ordine
            var SalesOrderItemSet = [];
            if (o.positions && (o.positions).length > 0) {
                for (var i = 0; i < (o.positions).length; i++) {
                    var SalesOrderItem = {};
                    var pos = o.positions[i];
                    SalesOrderItem.Vbeln = "";
                    SalesOrderItem.Posex = (parseInt(pos.positionId)).toString(); //(parseInt(pos.positionId + 1)).toString();
                    SalesOrderItem.Matnr = pos.productId;
                    SalesOrderItem.Kwmeng = (pos.quantity).toString();
                    SalesOrderItem.Vrkme = pos.scale;
                    SalesOrderItem.Edatu = pos.wantedDate;
                    SalesOrderItem.Pstyv = ""; //se è un kit ZP2
                    SalesOrderItem.Netwr = (pos.totalListPrice).toString();
                    SalesOrderItem.Umson = (pos.isTribut) ? "X" : "";
                    SalesOrderItemSet.push(SalesOrderItem);
                }
            }
            //sconti
            var SalesOrderConditionSet = [];
            //---------------------Old----------------------------------------------------
            // if (o.positions && (o.positions).length > 0) {
            //   for (var i = 0; i < (o.positions).length; i++) {
            //
            //     if ((o.positions[i]).discount) {
            //       var price = {};
            //       var agentDiscount = {};
            //       var firstLocDiscount = {};
            //       var secondLocDiscount = {};
            //       var thirdLocDiscount = {};
            //
            //       var discount = o.positions[i].discount;
            //
            //       price.Vbeln = "";
            //       price.Posex = (discount.positionId + 1).toString();
            //       price.Kschl = "ZPBA";
            //       price.Kbetr = discount.price.unitVal ? (discount.price.unitVal).toString() : "0";
            //       price.Konwa = discount.scale;
            //       price.Kpein = "0";
            //
            //       // SalesOrderConditionSet.push(price);
            //
            //       agentDiscount.Vbeln = "";
            //       agentDiscount.Posex = (discount.positionId + 1).toString();
            //       agentDiscount.Kschl = "ZSAG";
            //       agentDiscount.Kbetr = discount.agentDiscount.unitVal ? (discount.agentDiscount.unitVal).toString() : "0";
            //       agentDiscount.Konwa = discount.scale;
            //       agentDiscount.Kpein = "0";
            //
            //       SalesOrderConditionSet.push(agentDiscount);
            //
            //       firstLocDiscount.Vbeln = "";
            //       firstLocDiscount.Posex = (discount.positionId + 1).toString();
            //       firstLocDiscount.Kschl = "ZSS1";
            //       firstLocDiscount.Kbetr = discount.firstLocDiscount.unitVal ? (discount.firstLocDiscount.unitVal).toString() : "0";
            //       firstLocDiscount.Konwa = discount.scale;
            //       firstLocDiscount.Kpein = "0";
            //
            //       SalesOrderConditionSet.push(firstLocDiscount);
            //
            //
            //       secondLocDiscount.Vbeln = "";
            //       secondLocDiscount.Posex = (discount.positionId + 1).toString();
            //       secondLocDiscount.Kschl = "ZSS2";
            //       secondLocDiscount.Kbetr = discount.secondLocDiscount.unitVal ? (discount.secondLocDiscount.unitVal).toString() : "0";
            //       secondLocDiscount.Konwa = discount.scale;
            //       secondLocDiscount.Kpein = "0";
            //
            //       SalesOrderConditionSet.push(secondLocDiscount);
            //
            //       thirdLocDiscount.Vbeln = "";
            //       thirdLocDiscount.Posex = (discount.positionId + 1).toString();
            //       thirdLocDiscount.Kschl = "ZSS3";
            //       thirdLocDiscount.Kbetr = discount.thirdLocDiscount.unitVal ? (discount.thirdLocDiscount.unitVal).toString() : "0";
            //       thirdLocDiscount.Konwa = discount.scale;
            //       thirdLocDiscount.Kpein = "0";
            //
            //       SalesOrderConditionSet.push(thirdLocDiscount);
            //
            //
            //     }
            //   }
            // }
            //------------------------------------------------------------------------------------------------------------
            //------------------------------------------New----------------------------------------------------------------
            if (o.positions && (o.positions).length > 0) {
                for (var i = 0; i < (o.positions).length; i++) {
                    if ((o.positions[i]).discount) {
                        var price = {};
                        var agentDiscount = {};
                        var firstLoc = {};
                        var secondLocDiscount = {};
                        var thirdLocDiscount = {};
                        var discount = o.positions[i].discount;
                        if (discount.discountArray && discount.discountArray.length > 0) {
                            var discountItems = discount.discountArray;
                            discount.positionId = discount.positionId ? discount.positionId: o.positions[i].positionId;
                            var positionNr = discount.positionId.toString();
                            for (var k = 0; k < discountItems.length; k++) {
                                if (discountItems[k].isEditable || discountItems[k].typeId == "ZSL4" || discountItems[k].typeId == "ZSL3") {
                                    discountItems[k].positionId = positionNr;
                                    var d = model.persistence.Serializer.discount.toSAP(discountItems[k]);
                                    SalesOrderConditionSet.push(d);
                                }
                            }
                        }
                    }
                }
            }
            //------------------------------------------------------------------------------------------------------------
            var SalesOrderTextSet = [];
            SalesOrderTextSet.push(model.persistence.Serializer.order_note.toSAP(o.billsNotes, "B"));
            SalesOrderTextSet.push(model.persistence.Serializer.order_note.toSAP(o.salesNotes, "S"));
            SalesOrderTextSet.push(model.persistence.Serializer.order_note.toSAP(o.creditNotes, "C"))
            toSapData.SalesOrderTextSet = SalesOrderTextSet;
            toSapData.SalesOrderItemSet = SalesOrderItemSet;
            toSapData.SalesOrderConditionSet = SalesOrderConditionSet;
            return toSapData;
        }
    }
    , orderList: {
        fromSAPItems: function (s) {
            var l = {
                "results": []
            };
            if (s && s.results && s.results.length > 0) {
                for (var i = 0; i < s.results.length; i++) {
                    l.results.push(this.fromSAP(s.results[i]));
                }
            }
            return l;
        }
        , fromSAP: function (s) {
            var o = {};
            o.orderId = s.Vbeln;
            o.orderDate = s.Audat;
            o.orderCreationDate = s.Erdat;
            o.society = s.BukrsVf;
            o.salesOrg = s.Vkorg;
            o.distrCh = s.Vtweg;
            o.division = s.Spart;
            o.areaManager = s.Vkbur;
            o.territoryManager = s.Vkgrp;
            o.agentCode = s.Cdage;
            o.agentName = s.Name1Cdage;
            o.customerId = s.Kunnr;
            o.customerName = s.Name1Kunnr;
            o.orderType = s.Auart;
            o.orderTypeDescr = s.BezeiAuart;
            o.orderReason = s.Augru;
            o.orderReasonDescr = s.BezeiAugru;
            o.validDateList = s.Erdat;
            o.shippmentDate = ((s.Dtcon === null) || (!s.Dtcon) || (parseInt(s.Dtcon) == 0)) ? "" : s.Dtcon;
            o.orderStatus = s.Stato;
            o.orderStatusDescr = s.StatoDef;
            o.Dtcon = s.Dtcon;
            o.rifOrder = s.Bstnk;
            o.Bstdk = s.Bstdk;
            //**
            o.currencyOnDoc = s.Waerk;
            o.totaleNetto = s.Netwr;
            o.totaleImposte = s.Mwsbp;
            o.totaleLordo = s.SoTot;
            o.flagConsegne = s.DelivDt;
            //**
            return o;
        }
    }
    , prodH: {
        fromSAPItems: function (s) {
            var res = [];
            for (var i = 0; s && s.results && s.results.length > 0 && i < s.results.length; i++) {
                var item = {
                    "familyId": s.results[i].Prodh
                    , "familyDescr": s.results[i].Vtext
                }
                res.push(item);
            }
            return {
                "results": res
            };
        }
    }
    , historyCart: {
        fromSAPItems: function (s) {
            var l = {
                "results": []
            };
            if (s && s.results && s.results.length > 0) {
                for (var i = 0; i < s.results.length; i++) {
                    l.results.push(this.fromSAP(s.results[i]));
                }
            }
            return l;
        }
        , fromSAP: function (s) {
            var c = {};
            c.orderId = s.Vbeln;
            c.cartCreationDate = new Date(s.Erdat);
            // c.cartCreationTime = s.Erzet;
            c.username = s.Uname;
            c.favorite = _.isEmpty(s.Xpref) ? false : true;
            c.flag = s.Loekz
            return c;
        }
        , toSAP: function (c) {
            var s = {};
            s.Vbeln = c.orderId;
            s.Xpref = c.favorite ? "X" : "";
            s.Uname = c.username;
            return s;
        }
    }
    , order_item: {
        toSAPItems: function (items) {
            var ret = {
                Items: []
            };
            for (var i = 0; i < items.length; i++) {
                var oItem = this.toSap(items[i]);
                ret.Items.push(oItem);
            }
            return ret;
        }
        , fromSAPItems: function (results) {
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        }
        , fromSAP: function (sapData) {
            var oi = {};
            oi.orderId = sapData.orderId;
            oi.positionId = sapData.positionId;
            oi.quantity = sapData.quantity;
            oi.totalListPrice = sapData.totalListPrice;
            oi.totalNetPrice = sapData.totalNetPrice;
            oi.discountApplyed = sapData.discountApplyed;
            oi.productId = sapData.productId;
            oi.scale = sapData.scale;
            oi.description = sapData.description;
            oi.unitListPrice = sapData.unitListPrice;
            oi.unitNetPrice = sapData.unitNetPrice;
            // oi.orderId = sapData.orderId;
            // oi.positionId = sapData.positionId;
            // oi.quantity = sapData.quantity;
            // oi.totalListPrice = sapData.totalListPrice;
            // oi.totalNetPrice = sapData.totalNetPrice;
            // oi.discountApplyed = sapData.discountApplyed;
            // oi.productId = sapData.productId;
            // oi.scale = sapData.scale;
            // oi.description = sapData.description;
            // oi.unitListPrice  = sapData.unitListPrice;
            // oi.unitNetPrice  = sapData.unitNetPrice;
            oi.orderId = sapData.Vbeln;
            oi.positionId = sapData.Posex;
            oi.quantity = sapData.Kwmeng ? parseInt(sapData.Kwmeng) : 0;
            oi.totalListPrice = sapData.ItemTot;
            oi.totalNetPrice = sapData.Netwr;
            oi.totalTaxPrice = sapData.Mwsbp;
            oi.totalTransport = sapData.ItemTransp;
            // oi.discountApplyed = sapData.discountApplyed ? sapData.discountApplyed : "";
            oi.productId = sapData.Matnr;
            oi.scale = sapData.Vrkme;
            oi.scaleDescr = sapData.Mseht;
            oi.currency = sapData.Waers;
            oi.description = sapData.Maktx;
            oi.shippmentDate = sapData.Edatu;
            oi.isKit = (sapData.Kit == "X");
            // oi.unitListPrice  = (oi.quantity > 0)? oi.totalListPrice/oi.quantity : oi.totalListPrice ;
            // oi.unitNetPrice  = (oi.quantity > 0)? oi.totalNetPrice/oi.quantity : oi.totalNetPrice ;
            oi.isTribut = (sapData.Umson == "X");
            return oi;
        }
        , toSAP: function (oi) {}
    }
    , order_note: {
        fromSAP: function (s) {
            var n = {};
            n.orderId = s.Vbeln;
            n.position = s.Posex;
            n.text = s.Text;
            n.id = s.TextId;
            return n;
        }
        , toSAP: function (d, type) {
            var s = {};
            if (d.length > 0) {
                s.Vbeln = "";
                s.Posex = "000000";
                s.Text = "";
                for (var i = 0; i < d.length; i++) {
                    s.Text += d[i].text;
                    if (i < d.length - 1) {
                        s.Text += "\n";
                    }
                }
                // s.Text = d.text;
                s.TextId = "";
                switch (type) {
                case "S":
                    s.TextId = "Z001";
                    break;
                case "B":
                    s.TextId = "Z002";
                    break;
                case "C":
                    s.TextId = "Z013";
                    break;
                default:
                    s.TextId = "";
                    break;
                }
            }
            return s;
        }
    }
    , product: {
        fromSAP: function (sapData) {
            var p = {};
            p.society = sapData.Bukrs;
            p.salesOrg = sapData.Vkorg;
            p.distrCh = sapData.Vtweg;
            p.division = sapData.Spart;
            p.last = sapData.Ultimo;
            p.productId = sapData.Matnr ? sapData.Matnr : "";
            p.productPicUrl = sapData.productPicUrl ? sapData.productPicUrl : ""; //we can set a default icon??
            p.scale = sapData.Meinh ? sapData.Meinh : "";
            p.description = sapData.Maktx ? sapData.Maktx : "no info";
            p.parentId = sapData.Figlio ? sapData.Figlio : "";
            //p.description = sapData.description ? sapData.description : "";
            p.unitListPrice = parseFloat(sapData.Condval) ? parseFloat(sapData.Condval) : 0;
            p.currency = sapData.Currncy ? sapData.Currncy : "";
            //p.unitNetPrice = parseFloat(sapData.unitNetPrice) ? sapData.unitNetPrice : 0;
            return p;
        }
        , toSAP: function (p) {
            var toSapData = {};
            toSapData.Matnr = p.productId;
            toSapData.scale = p.scale;
            toSapData.Maktg = p.description;
            toSapData.unitListPrice = p.unitListPrice;
            //toSapData.unitNetPrice = p.unitNetPrice;
            return toSapData;
        }
    }
    , blockingOrderReason: {
        fromSAP: function (sapData) {
            var b = {};
            b.orderId = sapData.Vbeln ? sapData.Vbeln : "";
            b.position = sapData.Posnr ? sapData.Posnr : "";
            b.blockCode = sapData.Cdblo ? sapData.Cdblo : "";
            b.blockType = sapData.Tipob ? sapData.Tipob : "";
            b.blockDescription = sapData.Descr ? sapData.Descr : "";
            b.blockerName = sapData.Ernam ? sapData.Ernam : "";
            b.blockDate = sapData.Erdat ? sapData.Erdat : "";
            b.releaserName = sapData.Rlusr ? sapData.Rlusr : "";
            b.releaseDate = sapData.Rldat ? sapData.Rldat : "";
            if (sapData.Rltim && sapData.Rltim.ms) {
                b.releaseTime = sapData.Rltim.ms;
            }
            else {
                b.releaseTime = "";
            }
            return b;
        }
        , toSAP: function (p) {
            var toSapData = {};
            return toSapData;
        }
    }
    , userInfo: {
        fromSAPItems: function (results) {
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        }
        , fromSAP: function (sapData) {
            var u = {
                "userGeneralData": {}
                , "organizationData": {
                    "results": []
                }
            };
            u.userGeneralData.username = sapData[0].Uname ? sapData[0].Uname : "";
            u.userGeneralData.name = sapData[0].Name1 ? sapData[0].Name1 : "";
            u.userGeneralData.fullName = sapData[0].NameText ? sapData[0].NameText : "";
            u.userGeneralData.customer = sapData[0].Cdage ? sapData[0].Cdage : ""; //maybe to change
            u.userGeneralData.userAlias = sapData[0].Ualias ? sapData[0].Ualias : "";
            u.userGeneralData.mail = sapData[0].SmtpAddr ? sapData[0].SmtpAddr : "";
            u.userGeneralData.gruppoClienti = sapData[0].Kdgrp ? sapData[0].Kdgrp : "";
            u.userGeneralData.userType = sapData[0].Ustyp ? sapData[0].Ustyp : "";
            u.type = sapData[0].Ustyp ? sapData[0].Ustyp : "";
            u.fullName = sapData[0].NameText ? sapData[0].NameText : "";
            u.sessionUID = sapData[0].SessionUID? sapData[0].SessionUID :"";
            //      u.canEditOrders = sapData[0].Xorders === "X" ? true : false;
            //      u.canEditCustomers = sapData[0].Xcustomers === "X" ? true : false;
            //u.userGeneralData.name = sapData[0].Name1 ? sapData[0].Name1 : "";
            for (var i = 0; i < sapData.length; i++) {
                var tempItem = {};
                tempItem.username = u.userGeneralData.username;
                tempItem.society = sapData[i].Bukrs ? sapData[i].Bukrs : "";
                tempItem.salesOrg = sapData[i].Vkorg ? sapData[i].Vkorg : "";
                tempItem.distributionChannel = sapData[i].Vtweg ? sapData[i].Vtweg : "";
                tempItem.description = sapData[i].Ltext ? sapData[i].Ltext : "";
                tempItem.position = sapData[i].Sel ? sapData[i].Sel : "";
                tempItem.areaManager = sapData[i].Vkbur ? sapData[i].Vkbur : "";
                tempItem.areaManagerText = sapData[i].VkburBezei ? sapData[i].VkburBezei : "";
                tempItem.territoryManager = sapData[i].Vkgrp ? sapData[i].Vkgrp : "";
                tempItem.territoryManagerText = sapData[i].VkgrpBezei ? sapData[i].VkgrpBezei : "";
                tempItem.division = sapData[i].Spart ? sapData[i].Spart : "";
                tempItem.altroDestFlag = (sapData[i].Xaltrodest == "X");
                tempItem.cantiereFlag = (sapData[i].Xcantiere == "X");
                tempItem.canEditListValidDate = (sapData[i].Xpricedat == "X");
                tempItem.canEditOrders = sapData[i].Xorders === "X" ? true : false;
                tempItem.canEditCustomers = sapData[i].Xcustomers === "X" ? true : false;
                tempItem.canEditDiscount = (sapData[i].Xdiscount === "X");
                tempItem.canEditQuotation= (sapData[i].Xquotation === "X");
                
                //        tempItem.userType = sapData[i].Ustyp ? sapData[i].Ustyp : "";
                //tempItem.agentDiscountValue = sapData[i].Kunnr_Discount ? sapData[i].Kunnr_Discount : 0;
                u.organizationData.results.push(tempItem);
            }
            return u;
        }
    }
    , loadTrackingOrderInfo: {
        fromSAP: function (sapData) {
            var b = {};
            //      b.orderId = sapData.Vbeln ? sapData.Vbeln : "";
            //      b.position = sapData.Posnr ? sapData.Posnr : "";
            //      b.blockCode = sapData.Cdblo ? sapData.Cdblo : "";
            //      b.blockType = sapData.Tipob ? sapData.Tipob : "";
            //      b.blockDescription = sapData.Descr ? sapData.Descr : "";
            //      b.blockerName = sapData.Ernam ? sapData.Ernam : "";
            //      b.blockDate = sapData.Erdat ? sapData.Erdat : "";
            //      b.releaserName = sapData.Rlusr ? sapData.Rlusr : "";
            //      b.releaseDate = sapData.Rldat ? sapData.Rldat : "";
            //      b.releaseTime = sapData.Rltim ? sapData.Rltim : "";
            return b;
        }
        , toSAP: function (p) {
            var toSapData = {};
            return toSapData;
        }
    }
    , loadDeliveryList: {
        fromSAP: function (sapData) {
            var b = {};
            b.codConsegna = sapData.Vbeln;
            //      b.orderId = sapData.Vbeln ? sapData.Vbeln : "";
            //      b.position = sapData.Posnr ? sapData.Posnr : "";
            //      b.blockCode = sapData.Cdblo ? sapData.Cdblo : "";
            //      b.blockType = sapData.Tipob ? sapData.Tipob : "";
            //      b.blockDescription = sapData.Descr ? sapData.Descr : "";
            //      b.blockerName = sapData.Ernam ? sapData.Ernam : "";
            //      b.blockDate = sapData.Erdat ? sapData.Erdat : "";
            //      b.releaserName = sapData.Rlusr ? sapData.Rlusr : "";
            //      b.releaseDate = sapData.Rldat ? sapData.Rldat : "";
            //      b.releaseTime = sapData.Rltim ? sapData.Rltim : "";
            return b;
        }
        , toSAP: function (p) {
            var toSapData = {};
            return toSapData;
        }
    }
    , practice: {
        fromSAP: function (s) {
            var p = {};
            p.society = s.Bukrs;
            p.salesOrg = s.Vkorg;
            p.distrCh = s.Vtweg;
            p.division = s.Spart;
            p.resoId = s.Vbeln;
            p.areaManager = s.Vkbur;
            p.territoryManager = s.Vkgrp;
            p.resoDate = s.Audat;
            p.renderType = s.Auart;
            p.renderTypeText = s.Ltext;
            p.renderReason = s.Augru;
            p.renderReasonText = s.Bezei;
            return p;
        }
        , fromSAPItems: function (result) {
            var items = [];
            if (result && result.results && result.results.length > 0) {
                var arr = result.results;
                for (var i = 0; i < arr.length; i++) {
                    items.push(model.persistence.Serializer.practice.fromSAP(arr[i]));
                }
            }
            return items;
        }
    }
    , reso: {
        fromSAP: function (sapData) {
            var r = {};
            r.renderType = sapData.Auart;
            r.renderTypeText = sapData.AuartText;
            r.resoId = sapData.VbelnCr;
            r.society = sapData.Bukrs;
            r.salesOrg = sapData.Vkorg;
            r.distrCh = sapData.Vtweg;
            r.division = sapData.Spart;
            r.areaManager = sapData.Vkbur;
            r.areaManagerText = sapData.VkburText;
            r.territoryManager = sapData.Vkgrp;
            r.territoryManagerText = sapData.VkgrpText;
            r.agentCode = sapData.Cdage;
            r.agentName = sapData.CdageName1;
            r.customerId = sapData.AgKunnr;
            r.customerName = sapData.AgName1;
            r.customerAddress = sapData.AgStreet;
            r.customerCountry = sapData.AgCountry;
            r.customerPostCode = sapData.AgPostCode1;
            r.customerCity = sapData.AgCity;
            r.customerTel = sapData.AgTelnum;
            r.buyerId = sapData.RgKunnr;
            r.buyerName = sapData.RgName1;
            r.buyerAddress = sapData.RgStreet;
            r.buyerCountry = sapData.RgCountry;
            r.buyerPostCode = sapData.RgPostCode1;
            r.buyerCity = sapData.RgCity;
            r.buyerTel = sapData.RgTelnum;
            r.receiverId = sapData.WeKunnr;
            r.receiverName = sapData.WeName1;
            r.receiverAddress = sapData.WeStreet;
            r.receiverCountry = sapData.WeCountry;
            r.receiverPostCode = sapData.WePostCode1;
            r.receiverCity = sapData.WeCity;
            r.receiverTel = sapData.WeTelnum;
            r.orderId = sapData.VbelnVa; //VbelVa?
            r.deliveryId = sapData.VbelnVl; //VbelnVl
            r.billNumber = sapData.Xabln; //Xabln
            r.billDate = sapData.Fkdat; //Fkdat?
            r.deliveryDate = sapData.Lfdat;
            r.wareMoveDate = sapData.Wadat;
            r.commercialInvoice = sapData.VbelnVf;
            r.accountingInvoice = sapData.Belnr;
            r.year = sapData.Gjahr;
            r.invoiceDate = sapData.Budat;
            r.docDate = sapData.Bldat;
            r.renderReason = sapData.Augru;
            r.renderReasonText = sapData.AugruText;
            r.appointment = sapData.Sdabw; //Sdabw;
            r.appointmentText = sapData.SdabwText;
            //	    r.textId=sapData.Txtid;
            //	    r.textContent=sapData.Ltext;
            r.salesNote = sapData.LtextZ001;
            r.bidNote = sapData.LtextZ006;
            r.nrReg = sapData.Nmatr;
            r.costs = sapData.Costi;
            r.deliveryBlock = sapData.Lifsk;
            r.invoiceBlock = sapData.Faksk;
            r.items = model.persistence.Serializer.reso_item.fromSAPItems(sapData);
            return r;
        }
        , toSAP: function (r) {
            var d = {};
            d.VbelnCr = "";
            d.Bukrs = r.society;
            d.Vkorg = r.salesOrg;
            d.Vtweg = r.distrCh;
            d.Spart = r.division;
            d.Vkbur = r.areaManager;
            d.Vkgrp = r.territoryManager;
            d.Cdage = r.agentCode;
            d.CdageName1 = r.agentName;
            d.Auart = r.renderType;
            d.AgKunnr = r.customerId;
            d.AgName1 = r.customerName;
            d.AgStreet = r.customerAddress;
            d.AgCountry = r.customerCountry;
            d.AgPostCode1 = r.customerPostCode;
            d.AgCity = r.customerCity;
            d.AgTelnum = r.customerTel;
            d.RgKunnr = r.buyerId;
            d.RgName1 = r.buyerName;
            d.RgStreet = r.buyerAddress;
            d.RgCountry = r.buyerCountry;
            d.RgPostCode1 = r.buyerPostCode;
            d.RgCity = r.buyerCity;
            d.RgTelnum = r.buyerTel;
            d.WeKunnr = r.receiverId;
            d.WeName1 = r.receiverName;
            d.WeStreet = r.receiverAddress;
            d.WeCountry = r.receiverCountry;
            d.WePostCode1 = r.receiverPostCode;
            d.WeCity = r.receiverCity;
            d.WeTelnum = r.receiverTel;
            d.VbelnVa = r.orderId;
            d.VbelnVl = r.deliveryId;
            d.Xabln = r.billNumber;
            d.Lfdat = r.deliveryDate;
            d.Wadat = r.wareMoveDate;
            d.VbelnVf = r.commercialInvoice;
            d.Fkdat = r.billDate;
            d.Belnr = r.accountingInvoice;
            d.Gjahr = r.year;
            d.Budat = r.invoiceDate;
            d.Bldat = r.docDate;
            d.Augru = r.renderReason;
            d.Sdabw = r.appointment;
            //		  d.Txtid=r.textId;
            //		  d.Ltext=r.textContent;
            d.LtextZ001 = r.salesNote;
            d.LtextZ006 = r.bidNote;
            d.Nmatr = r.nrReg;
            d.Costi = r.costs;
            d.Lifsk = r.deliveryBlock;
            d.Faksk = r.invoiceBlock;
            d.CR_ItemsSet = [];
            d.CR_MaterialsSet = [];
            d.Guid = r.guid;
            var items = [];
            var registries = [];
            if (r.items && r.items.length > 0) {
                for (var i = 0; i < r.items.length; i++) {
                    var reso_item = model.persistence.Serializer.reso_item.toSAP(r.items[i]);
                    if (reso_item) {
                        items.push(reso_item);
                        if (r.items[i].regFlag && r.items[i].registries && r.items[i].registries.length > 0) {
                            for (var j = 0; j < r.items[i].registries.length; j++) {
                                var reso_item_registry = model.persistence.Serializer.reso_item_registry.toSAP(r.items[i].registries[j]);
                                if (reso_item_registry) registries.push(reso_item_registry);
                            }
                        }
                    }
                }
            }
            d.CR_ItemsSet = items;
            d.CR_MaterialsSet = registries;
            return d;
        }
    }
    , reso_item: {
        fromSAPItems: function (sapData) {
            var res = [];
            if (sapData.CR_ItemsSet && sapData.CR_ItemsSet.results && sapData.CR_ItemsSet.results.length > 0) {
                res = [];
                for (var i = 0; i < sapData.CR_ItemsSet.results.length; i++) {
                    res.push(model.persistence.Serializer.reso_item.fromSAP(sapData.CR_ItemsSet.results[i]));
                }
            }
            if (sapData.CR_MaterialsSet && sapData.CR_MaterialsSet.results && sapData.CR_MaterialsSet.results.length > 0) {
                var registries = [];
                for (var i = 0; i < sapData.CR_MaterialsSet.results.length; i++) {
                    registries.push(model.persistence.Serializer.reso_item_registry.fromSAP(sapData.CR_MaterialsSet.results[i]));
                }
                registries = _.groupBy(registries, 'positionId');
                for (var j = 0; j < res.length; j++) {
                    var regNumArr = registries[res[j].positionId];
                    if (regNumArr && regNumArr.length > 0) {
                        res[j].registries = regNumArr;
                    }
                }
            }
            return res;
        }
        , fromSAP: function (sapData) {
            var r = {};
            r.resoId = sapData.VbelnCr;
            r.society = sapData.Bukrs;
            r.salesOrg = sapData.Vkorg;
            r.distrCh = sapData.Vtweg;
            r.division = sapData.Spart;
            r.areaManager = sapData.Vkbur;
            r.territoryManager = sapData.Vkgrp;
            r.agentCode = sapData.Cdage;
            r.positionId = sapData.Posnr;
            r.upperPositionId = sapData.Uepos;
            r.productId = sapData.Matnr;
            r.description = sapData.Maktx;
            r.regFlag = sapData.Xmatr;
            r.quantity = sapData.Menge ? parseInt(sapData.Menge) : 0;
            r.quantityToRender = sapData.Mengn ? parseInt(sapData.Mengn) : 0;
            r.renderedQuantity = sapData.Mengo ? parseInt(sapData.Mengo) : 0;
            r.creditNoteValue = sapData.Netwo;
            r.billNetValue = sapData.Netwr;
            return r;
        }
        , toSAP: function (r) {
            if (!r.quantityToRender || r.quantityToRender <= 0) return false;
            var d = {};
            d.VbelnCr = "";
            d.Bukrs = r.society;
            d.Vkorg = r.salesOrg;
            d.Vtweg = r.distrCh;
            d.Spart = r.division;
            d.Vkbur = r.areaManager;
            d.Vkgrp = r.territoryManager;
            d.Cdage = r.agentCode;
            d.Posnr = r.positionId;
            d.Uepos = r.upperPositionId;
            d.Matnr = r.productId;
            d.Maktx = r.description;
            d.Xmatr = r.regFlag ? "X" : "";
            d.Menge = r.quantity.toString();
            d.Mengn = r.quantityToRender.toString();
            d.Mengo = r.renderedQuantity.toString();
            d.Netwr = r.billNetValue;
            d.Netwo = r.creditNoteValue;
            return d;
        }
    }
    , reso_item_registry: {
        fromSAP: function (s) {
            var r = {};
            r.positionId = s.Posnr;
            r.regNum = s.Nmatr;
            return r;
        }
        , toSAP: function (r) {
            if (!r.selected) return false;
            var d = {};
            d.Posnr = r.positionId;
            d.Nmatr = r.regNum;
            return d;
        }
    }
    , deliveryDate: {
        fromSAP: function (sapData) {
            var d = {};
            /*
            "ICcode" : "",
              "ICname" : req.provincia,
              "IInco1" : req.incoterms,
              "ILand1" : req.nation,
              "ILfdat" : utils.ParseDate().formatDate(new Date(), "yyyy-MM-ddT00:00:00"),
              "IRegio" : req.prov,
              "ISdabw" : req.codElabSpeciale,
              "ISpart" : req.Spart,
              "IVkorg" : req.Vkorg,
              "IVsart" : req.shippingType,
              "IVtweg" : req.Vtweg,
              "IWerks" : ""
            */
            d.dataConsegna = sapData.ELfdat ? sapData.ELfdat : "";
            d.provincia = sapData.IRegio ? sapData.IRegio : "";
            d.incoterms = sapData.IInco1 ? sapData.IInco1 : "";
            d.city = sapData.ICname ? sapData.ICname : "";
            d.shippingType = sapData.IVsart ? sapData.IVsart : "";
            d.codElabSpeciale = sapData.ISdabw ? sapData.ISdabw : "";
            return d;
        }
    }
    , promo: {
        fromSAP: function (sapData) {
            var d = {};
            /*
            Bukrs:"SI01"
            Datab:Thu May 19 2016 02:00:00 GMT+0200 (ora legale Europa occidentale)
            Datbi:Sat Dec 31 2016 01:00:00 GMT+0100 (ora solare Europa occidentale)
            Gjahr:"2016"
            Promo:"2016/00001"
            Spart:"BR"
            Txpro:"SUPER PROMO TEST"
            Vkorg:"SA10"
            Vtweg:"DO"
            */
            d.promoCode = sapData.Promo ? sapData.Promo : "";
            d.descr = sapData.Txpro ? sapData.Txpro : "";
            d.dataInizio = sapData.Datab ? sapData.Datab : "";
            d.dataFine = sapData.Datbi ? sapData.Datbi : "";
            d.anno = sapData.Gjahr ? sapData.Gjahr : "";
            return d;
        }
    }
    , abnormalPracticesButtons: {
        fromSAPItems: function (results) {
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        }
        , fromSAP: function (sapData) {
            var o = {};
            o.descr = sapData.Ltext ? sapData.Ltext : "";
            o.defaultValue = sapData.Zdefault ? sapData.Zdefault : "";
            o.codice = sapData.Auart ? sapData.Auart : "";
            return o;
        }
    }
    , renderReasons: {
        fromSAPItems: function (results) {
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        }
        , fromSAP: function (sapData) {
            var o = {};
            /*
      <d:Bukrs>SI01</d:Bukrs>
<d:Vkorg>SA20</d:Vkorg>
<d:Vtweg>DO</d:Vtweg>
<d:Spart>RI</d:Spart>
<d:Augru>YR0</d:Augru>
<d:Bezei>AG – Scelta prod. errata</d:Bezei>
<d:Zdefault/>
<d:CostC>X</d:CostC>
<d:CostCDesc>Accollamento costi cliente</d:CostCDesc>
<d:CostA>X</d:CostA>
<d:CostADesc>Accollamento costi agente</d:CostADesc>
<d:CostS>X</d:CostS>
<d:CostSDesc>Accollamento costi sede</d:CostSDesc>
      */
            o.descr = sapData.Bezei ? sapData.Bezei : "";
            o.renderReason = sapData.Augru ? sapData.Augru : "";
            o.defaultReso = sapData.Zdefault ? sapData.Zdefault : "";
            o.costC = sapData.CostC ? sapData.CostC : "";
            o.costCDesc = sapData.CostCDesc ? sapData.CostCDesc : "";
            o.costA = sapData.CostA ? sapData.CostA : "";
            o.costADesc = sapData.CostADesc ? sapData.CostADesc : "";
            o.costS = sapData.CostS ? sapData.CostS : "";
            o.costSDesc = sapData.CostSDesc ? sapData.CostSDesc : "";
            return o;
        }
    }
    , commission: {
        fromSAP: function (d) {
            var c = {};
            c.society = d.Bukrs;
            c.salesOrg = d.Vkorg;
            c.distrCh = d.Vtweg;
            c.division = d.Spart;
            c.territoryManager = d.Vkgrp;
            c.areaManager = d.Vkbur;
            c.customer = d.Cdage;
            c.customerName = d.Name1Cdage;
            c.prefattura = d.Zpref; //?
            c.preOpYear = d.Gjahr;
            c.supplier = d.Lifnr;
            c.creationDate = d.Erdat;
            c.creator = d.Ernam;
            c.expireDate = d.Zscad;
            c.netValue = d.Zimpnetto;
            c.totValue = d.Zimplordo;
            c.currency = d.Waers;
            c.billOpYear = d.Gjahrfi;
            c.accountingInvoice = d.Belnr;
            c.rif = d.Xblnr;
            c.registrationDate = d.Budat;
            c.EseDocReg = d.Esedocreg;
            c.registrationDoc = d.Docreg;
            c.cancelFlag = d.Loekz;
            c.cancelDate = d.Datacanc;
            c.cancelUser = d.Usrcanc;
            c.trimester = d.Zperiod;
            return c;
        }
    }
    , loadDefault: {
        fromSAPItems: function (s) {
            var l = {
                "results": []
            };
            if (s && s.results && s.results.length > 0) {
                for (var i = 0; i < s.results.length; i++) {
                    l.results.push(this.fromSAP(s.results[i]));
                }
            }
            return l;
        }
        , /*
               "Bukrs" : "SI41",
                   "Vkorg" : "SA40",
                   "Vtweg" : "DO",
                   "Spart" : "BR",
                   "Atnam" : "ZGRUP",
                   "Atwrt" : "Z1"
               */
        fromSAP: function (sapData) {
            var ld = {};
            ld.society = sapData.Bukrs ? sapData.Bukrs : "";
            ld.salesOrg = sapData.Vkorg ? sapData.Vkorg : "";
            ld.distrCh = sapData.Vtweg ? sapData.Vtweg : "";
            ld.division = sapData.Spart ? sapData.Spart : "";
            ld.attributeName = sapData.Atnam ? sapData.Atnam : "";
            ld.attributeValue = sapData.Atwrt ? sapData.Atwrt : "";
            return ld;
        }
    }
    , discountConditions: {
        /*
        "Prodh1" : "F0001",
            "Prodh1Text" : "CALDAIE MURALI",
            "Prodh2" : "G0258",
            "Prodh2Text" : "CALDARIELLO C 2008",
            "Kschl" : "ZCTC",
            "Vtext" : "Addebito Trasp. RC %",
            "Kbetr" : "0.80",
            "Konwa" : "%",
            "Kpein" : "0",
            "Kmein" : "",
            "Kstbm" : "0.000",
            "Konms" : "",
            "KstbmText" : ""


        */
        fromSAPItems: function (s) {
            var l = {
                "results": []
            };
            if (s && s.results && s.results.length > 0) {
                for (var i = 0; i < s.results.length; i++) {
                    l.results.push(this.fromSAP(s.results[i]));
                }
            }
            return l;
        }
        , fromSAP: function (sapData) {
            var dc = {};
            dc.familyId = sapData.Prodh1 ? sapData.Prodh1 : "";
            dc.familyDescr = sapData.Prodh1Text ? sapData.Prodh1Text : "";
            dc.gammaId = sapData.Prodh2 ? sapData.Prodh2 : "";
            dc.gammaDescr = sapData.Prodh2Text ? sapData.Prodh2Text : "";
            dc.typeId = sapData.Kschl ? sapData.Kschl : "";
            dc.typeDescr = sapData.Vtext ? sapData.Vtext : "";
            dc.value = sapData.Kbetr ? sapData.Kbetr : "";
            dc.scale = sapData.Konwa ? sapData.Konwa : "";
            dc.priceUnit = sapData.Kpein ? sapData.Kpein : "";
            dc.scaglionamento = sapData.KstbmText ? sapData.KstbmText : "";
            return dc;
        }
    }
    , customerDiscountCondition: {
        fromSAPItems: function (s) {
            var l = {
                "results": []
            };
            if (s && s.results && s.results.length > 0) {
                for (var i = 0; i < s.results.length; i++) {
                    l.results.push(this.fromSAP(s.results[i]));
                }
            }
            return l;
        }
        , fromSAP: function (sapData) {
            var r = {
                results: []
            };
            var d = {};
            d.society = sapData.Bukrs ? sapData.Bukrs : "";
            d.agentCode = sapData.Kunnr ? sapData.Kunnr : "";
            d.salesOrg = sapData.Vkorg ? sapData.Vkorg : "";
            d.distrCh = sapData.Vtweg ? sapData.Vtweg : "";
            d.division = sapData.Spart ? sapData.Spart : "";
            d.typeId = sapData.Kschl;
            d.typeDescr = model.i18n.getText(sapData.Kschl);
            d.scale = sapData.Konwa ? sapData.Konwa : "%";
            d.value = sapData.Kbetr ? Math.abs(parseFloat(sapData.Kbetr)) : 0;
            d.discountStartDate = sapData.Datab ? sapData.Datab : null;
            d.discountExpireDate = sapData.Datbi ? sapData.Datbi : null;
            return d;
            //      d.validDate = sapData.Prsdt ? sapData.Prsdt : null;
            //      var dz1 = _.clone(d);
            //      var dz2 = _.clone(d);
            //      var dz3 = _.clone(d);
            //      var dz3x = _.clone(d);
            //      var dz4 = _.clone(d);
            //      if (sapData.Kschl === "ZSL1") {
            //        dz1.typeId = "ZSL1";
            //        dz1.typeDescr = model.i18n.getText(sapData.Kschl);
            //        dz1.value = sapData.Kbetr ? parseInt(sapData.Kbetr) : 0;
            //        dz1.discountStartDate = sapData.Datab ? sapData.Datab : null;
            //        dz1.discountExpireDate = sapData.Datbi ? sapData.Datbi : null;
            //        return dz1;
            //      }
            //      if (sapData.Kschl === "ZSL2") {
            //        dz2.typeId = "ZSL2";
            //        dz2.typeDescr = model.i18n.getText(sapData.Kschl);
            //        dz2.value = sapData.Kbetr ? parseInt(sapData.Kbetr) : 0;
            //        dz2.discountStartDate = sapData.Datab ? sapData.Datab : null;
            //        dz2.discountExpireDate = sapData.Datbi ? sapData.Datbi : null;
            //        return dz2;
            //      }
            //      if (sapData.Kschl === "ZSM3") {
            //        dz3.typeId = "ZSL3";
            //        dz3.typeDescr = model.i18n.getText(sapData.Kschl);
            //        dz3.maxValue = sapData.Kbetr ? parseInt(sapData.Kbetr) : 0;
            //        dz3.discountStartDate = sapData.Datab ? sapData.Datab : null;
            //        dz3.discountExpireDate = sapData.Datbi ? sapData.Datbi : null;
            //        return dz3;
            //      }
            //      if (sapData.Kschl === "ZSX3") {
            //        dz3x.typeId = "ZSX3";
            //        dz3x.typeDescr = model.i18n.getText(sapData.Kschl);
            //        dz3x.value = sapData.Kbetr ? parseInt(sapData.Kbetr) : 0;
            //        dz3x.discountStartDate = sapData.Datab ? sapData.Datab : null;
            //        dz3x.discountExpireDate = sapData.Datbi ? sapData.Datbi : null;
            //        return dz3x;
            //      }
            //      if (sapData.Kschl === "ZSX4") {
            //        dz4.typeId = "ZSX4";
            //        dz4.typeDescr = model.i18n.getText(sapData.Kschl);
            //        dz4.value = sapData.Kbetr ? parseInt(sapData.Kbetr) : 0;
            //        dz4.discountStartDate = sapData.Datab ? sapData.Datab : null;
            //        dz4.discountExpireDate = sapData.Datbi ? sapData.Datbi : null;
            //        return dz4;
            //      }
            //      return r;
            //
            //      c.agentCode = sapData.Cdage ? sapData.Cdage : "";
            //      c.customerId = sapData.Kunnr ? sapData.Kunnr : "";
            //      c.typeId = sapData.Kschl ? sapData.Kschl : "";
            //      c.typeDescr = sapData.Vtext ? sapData.Vtext : "";
            //      c.value = sapData.Kbetr ? parseFloat(sapData.Kbetr) : "";
            //      
            //      //c.scale = sapData.Konwa ? sapData.Konwa : "";
            //      c.scale = "%";
            //      c.priceUnit = sapData.Kpein ? sapData.Kpein : "";
            //      
            //      c.currency = sapData.Konwa;
            //      c.status = sapData.Stato;
            //      
            //
            //
            //      c.unit = sapData.Kmein;
            //
            //      c.Krech = sapData.Krech;
            //      c.total = sapData.Kwert;
            //
            //      c.isEditable = sapData.Edit == "X" ? true : false;
            //      c.maxValue = Math.floor(c.value+ Math.random()*10 + 1);
            //      c.proposedValue="";
            //      c.statusDescr="";
            //      //Mock with proposedValue if customerDiscount
            //      if(c.typeId =="ZPSC" )
            //    {
            //    	  switch(c.status){
            //    	  
            //    	  case "INS":
            //    		  c.statusDescr=model.i18n.getText("inserted");
            //    		  c.proposedValue=c.value;
            //    	  case "AAM": 
            //    		  c.statusDescr = model.i18n.getText("approvedAM");
            //    		  c.proposedValue= c.value ;
            //    	  	  break;
            //    	  case "MTM":
            //    		  c.statusDescr = model.i18n.getText("approvedTM");
            //    		  c.proposedValue =c.value ;
            //    		  break;
            //    	  case "MAM": 
            //    		  c.statusDescr = model.i18n.getText("modifiedAM");
            //    		  c.proposedValue= c.value +Math.floor(Math.random()*(c.maxValue - c.value)+1);
            //    	  	  break;
            //    	  case "MTM":
            //    		  c.statusDescr = model.i18n.getText("modifiedTM");
            //    		  c.proposedValue =c.value +Math.floor(Math.random()*(c.maxValue - c.value)+1);
            //    		  break;
            //    	  default:
            //    		  c.statusDescr="";
            //    		  c.proposedValue = c.value;
            //    		  break;
            //    		  
            //    	 
            //    		  
            //    		  
            //    	  }
            //    }
            //return d;
        }
    }
    , approvationsList: {
        fromSAPItems: function (s) {
            var l = {
                "results": []
            };
            if (s && s.results && s.results.length > 0) {
                for (var i = 0; i < s.results.length; i++) {
                    l.results.push(this.fromSAP(s.results[i]));
                }
            }
            return l;
        }
        , fromSAP: function (s) {
            var o = {};
            o.society = s.Bukrs;
            o.salesOrg = s.Vkorg;
            o.distrCh = s.Vtweg;
            o.division = s.Spart;
            o.areaManager = s.Vkbur;
            o.territoryManager = s.Vkgrp;
            o.agentCode = s.Cdage;
            o.agentName = s.CdageName1;
            o.customerId = s.Kunnr;
            o.customerName = s.Name1;
            //      if ((o.customerId % 5) == 0) {
            //        o.customerDiscountStatus = "ATM";
            //        o.customerDiscountStatusDescr = model.i18n.getText("approvedTM");
            //      } else if ((o.customerId % 4) == 0) {
            //        o.customerDiscountStatus = "INS";
            //        o.customerDiscountStatusDescr = model.i18n.getText("inserted");
            //      } else if ((o.customerId % 3) == 0) {
            //        o.customerDiscountStatus = "MTM";
            //        o.customerDiscountStatusDescr = model.i18n.getText("modifiedTM");
            //      } else if ((o.customerId % 2) == 0) {
            //        o.customerDiscountStatus = "AAM";
            //        o.customerDiscountStatusDescr = model.i18n.getText("approvedAM");
            //      } else {
            //
            //        o.customerDiscountStatus = "MAM";
            //        o.customerDiscountStatusDescr = model.i18n.getText("modifiedAM");
            //      }
            // o.customerDiscountStatus = s.Stato;
            // o.customerDiscountStatusDescr = s.StatoDef;
            o.customerDiscountValue = s.Zsl3Actual ? parseInt(s.Zsl3Actual) : 0;
            //      o.customerDiscountMax = parseInt(s.SoTot % 100) >= o.customerDiscountValue ? parseInt(s.SoTot % 100) : o.customerDiscountValue;
            //
            o.customerDiscountStatus = s.Zsl3Stat;
            o.customerDiscountStatusDescr = model.NetworkStatus.getStatusDescr(o.customerDiscountStatus, "A");
            o.nationalDiscount = s.Zsl1 ? Math.abs(parseInt(s.Zsl1)) : 35;
            o.regionalDiscount = s.Zsl2 ? Math.abs(parseInt(s.Zsl2)) : 0;
            o.avgAgDiscount = s.Zsu3 ? Math.abs(parseInt(s.Zsu3)) : 0;
            o.proposedValue = s.Zsl3Age ? Math.abs(s.Zsl3Age) : 0;
            o.proposedNote = s.Zsl3NoteAge;
            o.discountStartDate = s.Zsl3Datab;
            o.discountExpireDate = s.Zsl3Datbi;
            return o;
        }
    }
    , discount_v2: {
        fromSAP: function (sapData) {
            var d = {};
            d.orderId = sapData.Vbeln;
            //d.positionId=sapData.Posex;
            d.scale = (sapData.Konwa !== "") ? sapData.Konwa : "%";
            d.currency = sapData.Konwa;
            d.typeId = sapData.Kschl;
            d.typeDescr = sapData.KschlText;
            d.value = Math.abs(parseFloat(sapData.Kbetr));
            d.unit = sapData.Kmein;
            d.priceUnit = sapData.Kpein;
            d.Krech = sapData.Krech;
            d.total = sapData.Kwert;
            d.price = Math.abs(parseFloat(sapData.Kwert));
            d.isEditable = sapData.Edit == "X" ? true : false;
            d.maxValue = 30;
            // d.price.unitVal = sapData.price;
            return d;
        }
        , fromSAPItems: function (ds) {
            var l = {
                "results": []
            };
            if (ds && ds.results && ds.results.length > 0) {
                for (var i = 0; i < ds.results.length; i++) {
                    l.results.push(this.fromSAP(ds.results[i]));
                }
            }
            return l;
        }

    }
    , offersList: {
        fromSAPItems: function (s) {
            var l = {
                "results": []
            };
            if (s && s.results && s.results.length > 0) {
                for (var i = 0; i < s.results.length; i++) {
                    l.results.push(this.fromSAP(s.results[i]));
                }
            }
            return l;
        }
        , fromSAP: function (s) {
            var o = {};
            o.orderId = s.Vbeln;
            o.orderDate = s.Audat;
            o.orderCreationDate = s.Erdat;
            o.society = s.BukrsVf;
            o.salesOrg = s.Vkorg;
            o.distrCh = s.Vtweg;
            o.division = s.Spart;
            o.areaManager = s.Vkbur;
            o.territoryManager = s.Vkgrp;
            o.agentCode = s.Cdage;
            o.agentName = s.Name1Cdage;
            o.customerId = s.Kunnr;
            o.customerName = s.Name1Kunnr;
            o.orderType = s.Auart;
            o.orderTypeDescr = s.BezeiAuart;
            //------------------------------------------
            o.orderReason = s.Augru;
            o.orderReasonDescr = s.BezeiAugru;
            //-----------------------------------------
            //      o.orderReason = (parseInt(o.orderId) % 3) ? "QFO" : ((parseInt(o.orderId) % 2) ? "QFQ" : "NWF");
            //      o.orderReasonDescr = (o.orderReason == "QFO") ? "Da Ordine" : ((o.orderReason == "QFQ") ? "Da Offerta" : "No WorkFlow");
            //-----------------------------------------
            o.validDateList = s.Erdat;
            o.shippmentDate = ((s.Dtcon === null) || (!s.Dtcon) || (parseInt(s.Dtcon) == 0)) ? "" : s.Dtcon;
            // o.orderStatus = s.Stato;
            // o.orderStatusDescr = s.StatoDef;
            o.Dtcon = s.Dtcon;
            o.rifOrder = s.Bstnk;
            o.Bstdk = s.Bstdk;
            //**
            o.currencyOnDoc = s.Waerk;
            o.totaleNetto = s.Netwr;
            o.totaleImposte = s.Mwsbp;
            o.totaleLordo = s.SoTot;
            o.flagConsegne = s.DelivDt;
            //-----------------------------------------------------------------------------
            o.orderStatus = s.Stat;
            o.orderStatusDescr = model.NetworkStatus.getStatusDescr(s.Stat, "O");
            o.avgCustomer = s.Zsm3 ? Math.abs(parseFloat(s.Zsm3)) : "";
            o.avgAgDiscount = s.Zsu3 ? Math.abs(parseFloat(s.Zsu3)) : "";
            
            o.agentCommission = s.PrvTot ? Math.abs(parseFloat(s.PrvTot)):"";
            o.avgAgOrderDiscount = s.Zsu4 ? Math.abs(parseFloat(s.Zsu4)):"";
            //--------------Mock-------------------------------------------------------------------
            //      if (o.orderReason !== "NWF") {
            //        if ((o.orderId % 6) == 0) {
            //          o.orderStatus = "INS";
            //          o.orderStatusDescr = model.i18n.getText("inserted");
            //        } else if ((o.orderId % 5) == 0) {
            //          o.orderStatus = "ATM";
            //          o.orderStatusDescr = model.i18n.getText("approvedTM");
            //        } else if ((o.orderId % 4) == 0) {
            //          o.orderStatus = "REJ";
            //          o.orderStatusDescr = model.i18n.getText("refused");
            //        } else if ((o.orderId % 3) == 0) {
            //          o.orderStatus = "MTM";
            //          o.orderStatusDescr = model.i18n.getText("modifiedTM");
            //        } else if ((o.orderId % 2) == 0) {
            //          o.orderStatus = "AAM";
            //          o.orderStatusDescr = model.i18n.getText("approvedAM");
            //        } else {
            //
            //          o.orderStatus = "MAM";
            //          o.orderStatusDescr = model.i18n.getText("modifiedAM");
            //        }
            //      } else {
            //        o.orderStatus = "";
            //        o.orderStatusDescr = "";
            //      }
            //-------------------------------------------------------------------------------------------------
            //**
            return o;
        }
    }
    , //----------Maybe it's useless, just offerList is enough----------------------------------
    offerApprovationsList: {
        fromSAPItems: function (s) {
            var l = {
                "results": []
            };
            if (s && s.results && s.results.length > 0) {
                for (var i = 0; i < s.results.length; i++) {
                    l.results.push(this.fromSAP(s.results[i]));
                }
            }
            return l;
        }
        , fromSAP: function (s) {
            var o = {};
            o.orderId = s.Vbeln;
            o.orderDate = s.Audat;
            o.orderCreationDate = s.Erdat;
            o.society = s.BukrsVf;
            o.salesOrg = s.Vkorg;
            o.distrCh = s.Vtweg;
            o.division = s.Spart;
            o.areaManager = s.Vkbur;
            o.territoryManager = s.Vkgrp;
            o.agentCode = s.Cdage;
            o.agentName = s.Name1Cdage;
            o.customerId = s.Kunnr;
            o.customerName = s.Name1Kunnr;
            o.orderType = s.Auart;
            o.orderTypeDescr = s.BezeiAuart;
            //------------------------------------------
            o.orderReason = s.Augru;
            o.orderReasonDescr = s.BezeiAugru;
            //--------------------------------------------
            //--------Mock-----------------------------
            //      o.orderReason = (parseInt(o.orderId) % 3) ? "QFO" : "QFQ";
            //      o.orderReasonDescr = (o.orderReason == "QFO") ? "Da Ordine" : "Da Offerta";
            //-------------------------------------------
            o.validDateList = s.Erdat;
            o.shippmentDate = ((s.Dtcon === null) || (!s.Dtcon) || (parseInt(s.Dtcon) == 0)) ? "" : s.Dtcon;
            o.orderStatus = s.Stato;
            o.orderStatusDescr = s.StatoDef;
            o.Dtcon = s.Dtcon;
            o.rifOrder = s.Bstnk;
            o.Bstdk = s.Bstdk;
            //**
            o.currencyOnDoc = s.Waerk;
            o.totaleNetto = s.Netwr;
            o.totaleImposte = s.Mwsbp;
            o.totaleLordo = s.SoTot;
            o.flagConsegne = s.DelivDt;
            //**
            o.orderStatus = s.Stat;
            o.orderStatusDescr = model.NetworkStatus.getStatusDescr(s.Stat, "O");
            //-------------ReMock-------------------
            //      if ((o.orderId % 6) == 0) {
            //        o.orderStatus = "REJ";
            //        o.orderStatusDescr = model.i18n.getText("refused");
            //      } else if ((o.orderId % 5) == 0) {
            //        o.orderStatus = "ATM";
            //        o.orderStatusDescr = model.i18n.getText("approvedTM");
            //      } else if ((o.orderId % 4) == 0) {
            //        o.orderStatus = "INS";
            //        o.orderStatusDescr = model.i18n.getText("inserted");
            //      } else if ((o.orderId % 3) == 0) {
            //        o.orderStatus = "MTM";
            //        o.orderStatusDescr = model.i18n.getText("modifiedTM");
            //      } else if ((o.orderId % 2) == 0) {
            //        o.orderStatus = "AAM";
            //        o.orderStatusDescr = model.i18n.getText("approvedAM");
            //      } else {
            //
            //        o.orderStatus = "MAM";
            //        o.orderStatusDescr = model.i18n.getText("modifiedAM");
            //      }
            // o.orderStatus = s.Stato;
            // o.orderStatusDescr = s.StatoDef;
            //----------------------------------------------
            //      o.avgCustomer = (parseInt(o.customerId) % 30)
            //      o.avgAgDiscount = (parseInt(o.agentCode) % 30);
            o.avgCustomer = s.Zsm3 ? Math.abs(parseFloat(s.Zsm3)) : "";
            o.avgAgDiscount = s.Zsu3 ? Math.abs(parseFloat(s.Zsu3)) : "";
            o.agentCommission = s.PrvTot ? Math.abs(parseFloat(s.PrvTot)):"";
            o.avgAgOrderDiscount = s.Zsu4 ? Math.abs(parseFloat(s.Zsu4)):"";
            
            //---------------------------------------------------
            return o;
        }
    }
    , offer: {

        toSAP: function (o) {
            var s = {};
            s.Bukrs = o.society;
            s.Vkorg = o.salesOrg;
            s.Vtweg = o.distrCh;
            s.Spart = o.division;
            s.Vbeln = o.orderId;
            s.Auart = "ZOF";
            s.Guid = o.guid ? o.guid : "";
            return s;
        }
    }, //--------------------------------------------------------------------------------------
    offerApprovation: {
        toSAP: function (o) {
            var toSapData = {};
            var QuotationModItemSet = [];
            toSapData.IVbeln = o.orderId;
            toSapData.IUstyp = o.type;
            toSapData.IStat = o.stat;
            toSapData.INote = o.note;
            if (o.positions && o.positions.length > 0) {
                for (var i = 0; i < o.positions.length; i++) {
                    var position = o.positions[i];
                    var p = {};
                    p.Vbeln = o.orderId;
                    p.Posnr = position.positionId;
                    p.Zsl3 = (_.find(position.orderDiscounts, {
                        typeId: "ZSL3"
                    }).value).toString()
                    p.Zsl4 = (_.find(position.orderDiscounts, {
                        typeId: "ZSL4"
                    }).value).toString()
                    QuotationModItemSet.push(p);
                }
            }
            toSapData.QuotationModItemSet = QuotationModItemSet;
            return toSapData;
        }
        , //        toSAPItems: function (items) {
        //            var ret = {
        //                Items: []
        //            };
        //            for (var i = 0; i < items.length; i++) {
        //                var oItem = this.toSap(items[i]);
        //                ret.Items.push(oItem);
        //            }
        //            return ret;
        //        }
    }
};