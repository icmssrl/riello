jQuery.sap.declare("model.persistence.PersistenceManager");
jQuery.sap.require("jquery.sap.storage");
model.persistence.PersistenceManager = {
	
	//salva i dati del modello in localStorage
	saveModel: function(key, model) {
		//Get Storage object to use  
		if (model) {
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			oStorage.put(key, JSON.stringify(model));
		}

	},
    //salva dati user
    saveOrgDataSelection: function(key, data){
        if (data) {
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			oStorage.put(key, JSON.stringify(data));
		}
    },
    
    //recupera dati user
    loadUserSavedData: function(key) {
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var tmpData = JSON.parse(oStorage.get(key));
		return tmpData;
	},
	//recupera modello salvato in localStorage
	loadModel: function(key) {
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var tmpOrder = JSON.parse(oStorage.get(key));
		return tmpOrder;
	},

	//elimina modello da localStorage
	flushModel: function(key) {
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		oStorage.remove(key);
	},
    //elimina dati user salvati
    flushUserSavedData: function(key) {
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		oStorage.remove(key);
	},
	
	//controlla se modello e user sono presenti in localStorage
	checkSavedData: function(userData, model){
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var model = oStorage.get(model);
		var userData = oStorage.get(userData);
		return ((model!== undefined) && (model!== null) && (! _.isEmpty(JSON.parse(model)))
                && (userData!== undefined) && (userData!== null) && (! _.isEmpty(JSON.parse(userData))));
	}
}