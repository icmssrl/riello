jQuery.sap.declare("model.Customer");
jQuery.sap.require("model.Destination");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.CustomerStatus");
jQuery.sap.require("model.collections.CustomerStatuses");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("utils.IBAN");
jQuery.sap.require("model.CustomerDiscountRequester");

model.Customer = (function () {




  Customer = function (serializedData) {


    this.registry = {};
    this.contact = {};
    this.bank = {};
    this.sales = {};
    this.destinations = [];
    this.customerStatus = {};
    this.discountConditions = [];
    this.requestedDiscounts =[];
    this.isBlocked = (serializedData && serializedData.isBlocked) ? serializedData.isBlocked : false;
    this.enables = {
      "registry":true,
      "contacts":true,
      "bank":true,
      "sales":true
    };

    this.initialize = function(user, orgData)
    {
      this.registry.id="";
      this.registry.registryType="";
      this.registry.userName = orgData.organizationData.results[0].username; //why?
      this.registry.division = orgData.organizationData.results[0].division;
      this.registry.distrCh= orgData.organizationData.results[0].distributionChannel;
      this.registry.society =  orgData.organizationData.results[0].society;
      this.registry.salesOrg = orgData.organizationData.results[0].salesOrg;
      this.registry.agentCode = user.userGeneralData.customer;
      this.registry.areaManager = orgData.organizationData.results[0].areaManager;
      this.registry.territoryManager=orgData.organizationData.results[0].territoryManager;
      this.registry.isPublicAdministration=false;

      this.registry.aufsd0="";
      this.registry.aufsdk="";

      //--------------Version 2.0 --------------------------
      //Adding discountStatus if present or it's needed  a  customerDiscountLoadFunction when user is of type AG
      // this.discountValueMax = user.agentDiscountValue ? user.agentDiscountValue : 100;
      //this.discountValue = 0;
      //this.discountStatus = "";
      //-----------------------------------


    };

    this.initializeByCFIVACode = function(customer)
    {
      (customer.registry) ? this.setRegistry(customer.registry): null;
      (customer.sales) ? this.setSales(customer.sales): null;
      (customer.bank) ? this.setBank(customer.bank): null;
      (customer.contact) ? this.setContact(customer.contact): null;
      (customer.enables) ? this.setEnables(customer.enables):null;

    };

    this.checkKeyData=function()
    {
      var defer = Q.defer();
      var req  = {
        "IStcd1":this.registry.taxCode,
        "IStceg":this.registry.VATNumber,
        "Bukrs":this.registry.society,
        "Vkorg":this.registry.salesOrg,
        "Vtweg":this.registry.distrCh,
        "Spart":this.registry.division,
        "Cdage":this.registry.agentCode
      };
      var fSuccess = function(res)
      {
        var customer = model.persistence.Serializer.customer.fromSAP(res);
        this.initializeByCFIVACode(customer);

        defer.resolve(this);
      }
      fSuccess =_.bind(fSuccess, this);
      var fError = function(err)
      {

        if(!err.response.body)
        {
          defer.reject(err.message);
        }
        else {
          var error = JSON.parse(err.response.body);
          var msg = error.error.message.value;
          defer.reject(msg);
        }

      };
      fError = _.bind(fError, this);
      model.odata.chiamateOdata.checkCFeIVA(req, fSuccess, fError);
      return defer.promise;
    };

    this.getRequestedDiscounts = function()
    {
      var defer = Q.defer();

      var fSuccess = function(res)
      {
        this.requestedDiscounts = res;
        defer.resolve(this.requestedDiscounts);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {
        this.requestedDiscounts=[];
        defer.reject(err);
      }
      fError=_.bind(fError);

      if(!this.customerDiscountRequester)
        this.customerDiscountRequester = new model.CustomerDiscountRequester(this.getRefInfo(), this.requestedDiscounts);


      this.customerDiscountRequester.loadRequestedDiscounts()
      .then(fSuccess, fError);

      return defer.promise;








    };

    //Function giving the properties needed in odata requests
    this.getRefInfo = function()
    {
      var salesInfo = {
        "Vkorg": this.registry.salesOrg,
        "Vtweg":this.registry.distrCh,
        "Spart":this.registry.division,
        "Bukrs":this.registry.society,
        "Vkbur":this.registry.areaManager,
        "Vkgrp":this.registry.territoryManager,
        "Cdage":this.registry.agentCode,
        "Kunnr":this.registry.id

      }
      return salesInfo;
    };

    // initialize = function () {
    //
    //   this.registry = {
    //     id: undefined,
    //     registryType: undefined,
    //     companyName: undefined,
    //     VATNumber: undefined,
    //     street: undefined,
    //     numAddr: undefined,
    //     postalCode: undefined,
    //     city: undefined,
    //     prov: undefined,
    //     nation: undefined
    //   };
    //
    //   this.destinations=[];
    //
    //
    //   this.contact = {
    //     phone: undefined,
    //     mobile: undefined,
    //     fax: undefined,
    //     contactType: undefined,
    //     mail: undefined
    //   };
    //
    //   this.bank = {
    //     iban: undefined,
    //     descr: undefined,
    //     bankNation: undefined,
    //     accountNum: undefined,
    //     abcab: undefined,
    //     cin: undefined
    //   };
    //
    //   this.sales = {
    //     clientType: undefined,
    //     billType: undefined,
    //     billFreq: undefined,
    //     paymentCond: undefined,
    //     resa: undefined,
    //     incoterms2: undefined,
    //     carrier: undefined,
    //     transport: undefined,
    //     notes: undefined,
    //   };
    // };

    // this.setRegistry = function (id, registryType, companyName, customerName, VATNumber, street, numAddr, postalCode, city, prov, nation, taxCode, customerType) {
    //   this.registry.id = id;
    //   this.registry.registryType = registryType;
    //   this.registry.customerName = customerName;
    //   this.registry.companyName = companyName;
    //   this.registry.VATNumber = VATNumber;
    //   this.registry.street = street;
    //   this.registry.numAddr = numAddr;
    //   this.registry.postalCode = postalCode;
    //   this.registry.city = city;
    //   this.registry.prov = prov;
    //   this.registry.nation = nation;
    //   this.registry.taxCode = taxCode;
    //   this.registry.customerType = customerType;
    // };
    this.setEnables = function(enables)
    {
      for(var prop in enables)
      {
          this.enables[prop] = enables[prop];
      }
    };
    this.setRegistry = function(registry)
    {
      for(var prop in registry)
      {
        if(registry[prop])
        {
          this.registry[prop] = registry[prop];
        }
      }
    };
    this.setBank = function(bank)
    {
      for(var prop in bank)
      {
        if(bank[prop])
        {
          this.bank[prop] = bank[prop];
        }
      }
    };
    this.setBankByIBAN = function(iban)
    {
      var data = utils.IBAN.ibanToData(iban);
      // var iban = {
      //   nation: "",
      //   control: "",
      //   cin: "",
      //   abi: "",
      //   cab: "",
      //   conto: ""
      // };
      this.bank.iban = iban;
      //this.bank.descr =  undefined,
      this.bank.bankNation=data.nation,
      this.bank.accountNum =  data.conto,
      this.bank.abcab= data.abi+data.cab,
      this.bank.cin= data.cin
    };
    this.setSales = function(sales)
    {
      for(var prop in sales)
      {
        if(sales[prop])
        {
          this.sales[prop] = sales[prop];
        }
      }
    };
    this.setContact = function(contact)
    {
      for(var prop in contact)
      {
        if(contact[prop] )
        {
          this.contact[prop] = contact[prop];
        }
      }
    };
    this.addDestination = function(dest)
    {
     this.destinations.push(dest);
    };

    this.getDestinations = function()
    {
      return this.destinations;
    };

    this.setDestinations = function(dests)
    {
      return this.destinations = dests;
    },

    this.setDiscountCondition = function(disC)
    {
      return this.discountConditions  = disC;
    },

 this.loadDiscountConditions = function(req)
    {
      var defer =  Q.defer();

      var fSuccess = function(res)
      {
        var result = model.persistence.Serializer.discountConditions.fromSAPItems(res);
        this.setDiscountCondition(result);
        defer.resolve(this);
      };
      var fError = function(err)
      {
        defer.reject(err);
      };
      fSuccess = _.bind(fSuccess,this);
      fError = _.bind(fError,this);

      model.odata.chiamateOdata.loadDiscountConditions(req, fSuccess, fError);

      return defer.promise;
    };


    this.loadCustomerDiscountConditions = function(req)
    {
    	var defer =  Q.defer();

        var fSuccess = function(res)
        {
          var result = model.persistence.Serializer.customerDiscountCondition.fromSAPItems(res);
          this.customerDiscountConditions = result.results;
          defer.resolve(this);
        };

        var fError = function(err)
        {
        	this.customerDiscountConditions=[];
          defer.reject(err);
        };

        fSuccess = _.bind(fSuccess,this);
        fError = _.bind(fError,this);

        model.odata.chiamateOdata.loadCustomerDiscountConditions(req, fSuccess, fError);

        return defer.promise;
    };
    this.getCustomerDiscountValue = function()
    {

      var res = _.find(this.customerDiscountConditions, {typeId:"ZSL3"});

      return res ? res.value : 0;
    };
    this.getCustomerDiscount = function()
    {
    	return _.find(this.customerDiscountConditions, {typeId:"ZSL3"});
    	
    };
    this.setCustomerDiscountValue = function(value, status)
    {
      var item = _.find(this.customerDiscountConditions, {typeId:"ZSL3"});
      
      item.value = value;
      if(status)
    {
    	  item.status = status;
    	  this.discountStatus=status;
    }
    	  
      

    };
    this.setCustomerDiscountConditions=function(discounts)
    {
      this.customerDiscountConditions = discounts;
    };

    this.setMaxCustomerDiscountValue=function(value)
    {
      this.maxCustomerDiscount=value;
    };




    // this.setContact = function (phone, mobile, fax, contactType, mail) {
    //   this.contact.phone = phone;
    //   this.contact.mobile = mobile;
    //   this.contact.fax = fax;
    //   this.contact.contactType = contactType;
    //   this.contact.mail = mail;
    // };


    // this.setBank = function (iban, descr, nation, accountNum, abcab, cin) {
    //   this.bank.iban = iban;
    //   this.bank.descr = descr;
    //   this.bank.bankNation = nation;
    //   this.bank.accountNum = accountNum;
    //   this.bank.abcab = abcab;
    //   this.bank.cin = cin;
    // };


    // this.setSales = function (clientType, billType, billFreq, paymentCond, reso, incoterms2, carrier, transport, notes) {
    //   this.sales.clientType = clientType;
    //   this.sales.billType = billType;
    //   this.sales.billFreq = billFreq;
    //   this.sales.paymentCond = paymentCond;
    //   this.sales.resa = reso;
    //   this.sales.incoterms2 = incoterms2;
    //   this.sales.carrier = carrier;
    //   this.sales.transport = transport;
    //   this.sales.notes = notes;
    // };



    this.getAddress = function () {
      return this.registry;
    };
    this.getContact = function () {
      return this.contact;
    };

    this.getSales = function () {
      return this.sales;
    };
    this.getBank = function () {
      return this.bank;
    };

    this.getId = function () {
      return this.registry.id;
    };

    this.getCompanyName = function() {
      return this.registry.companyName;
    };

    this.getCustomerName = function() {
      return this.registry.customerName;
    };

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);

      return model;

    };


    this.setCustomerStatus = function(customerStatus)
    {
          model.collections.CustomerStatuses.getById(this.getId(),this.customer)
          .then(_.bind(function(result){this.customerStatus = result;},this));
    };

    this.sendToSAP = function(updateBool)
    {
      var defer = Q.defer();

      var sapCustomer = model.persistence.Serializer.customer.toSAP(this);

      var fSuccess = function(result)
      {

        //console.log(result);
        defer.resolve(result);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {

        //console.log(err);
        defer.reject(err);
      };
      fError=_.bind(fError, this);
      //
      // if(updateBool)
      // {
      //   model.odata.chiamateOdata.updateCustomer(sapCustomer, fSuccess, fError);
      // }
      // else
      // {
      //   model.odata.chiamateOdata.createCustomer(sapCustomer, fSuccess, fError);
      // }
      model.odata.chiamateOdata.createCustomer(sapCustomer, fSuccess, fError);

      return defer.promise;

    };

    this.updateToSAP = function()
    {
      var defer = Q.defer();
      var fSuccess = function(result)
      {

        //console.log(result);
        defer.resolve(result);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {

        //console.log(err);
        defer.reject(err);
      };
      fError=_.bind(fError, this);

      var data = model.persistence.Serializer.customer.updateToSAP(this);
      model.odata.chiamateOdata.updateCustomer(data, fSuccess, fError);
      return defer.promise;
    };
    
    this.refreshCustomerDiscount = function(item)
    {
//    	this.discountStatus = item.Stat;
//    	this.discountStatusDescr = model.NetworkStatus.getStatusDescr(item.Stat, "A");
    	
    	for(var i = 0; i < this.customerDiscountConditions.length ; i++)
    	{
    		if(this.customerDiscountConditions[i].typeId == "ZSL3")
    		{
    			this.customerDiscountConditions[i].typeId = "ZSL3";
    			this.customerDiscountConditions[i].typeDescr = model.i18n.getText("ZSL3");
    			this.customerDiscountConditions[i].status=item.Stat;
    			this.customerDiscountConditions[i].statusDescr = model.NetworkStatus.getStatusDescr(item.Stat, "A");
    			this.customerDiscountConditions[i].value=item.Sconto;
    			//this.customerDiscountConditions[i] = item;
    			break;
    		}
    	
    	}
    };

    this.update = function (data) {
      for (var prop in data) {
    	  
    	if(prop !== "discountExpireDate" && prop !== "discountStartDate")
    		this[prop] = data[prop];
    	else
    		this[prop] = new Date(data[prop]);
      }

      // //addDestination
//      if(!this.destinations || this.destinations.length === 0)
//      {
//        this.addDestination(model.persistence.Serializer.destination.fromCustomer(this));
//      }
      if(!this.customerStatus)
      {
        this.customerStatus = new model.CustomerStatus();
      }

    };

    // initialize();

//To review first instantation
    if (serializedData) {

      this.update(serializedData);




    }

    // if(!this.destinations || this.destinations.length === 0)
    // {
    //   this.destinations = [];
    //   if(this.registry)
    //   {
    //     this.destinations.push(_.cloneDeep(this.registry));
    //   }
    //
    // }
//------------------Version 2.0-------------------------------
// Evolution on which the customer Discount is in a flat structure
//
//    this.getDiscount = function()
//    {
//    	var discountData = {
//    			"value": this.discountValue ? this.discountValue : 0,
//    			"status":this.discountStatus ? this.discountStatus : "",
//    			"reason":this.discountReason ? this.discountReason : ""
//    	}
//    	return discountData;
//    };
//    
//    updateDiscount = function()
//    {
//    	var defer = Q.defer()
//    	
//    	var fSuccess = function(res)
//    	{
//    		
//    	}
//    	var fError = function(err)
//    	{
//    		
//    	}
//    	fSuccess = _.bind(fSuccess, this);
//    	fError = _.bind(fError, this);
//    	
//    	model.odata.chiamateOdata.updateCustomerDiscount(this.getDiscount(), fSuccess, fError);
//    	
//    	
//    	return defer.promise;
//    };
    
    


    return this;
  };
  return Customer;


})();
