jQuery.sap.declare( "model.Countries" );
jQuery.sap.require( "icms.Settings" );
jQuery.sap.require( "model.mock.Loader" );

model.Countries = {

	_countries: [],
	deferCountries: Q.defer(),
	getCountries: function ( forceReload ) {

		if ( forceReload ){
			this.deferCountries = Q.defer();
		}

		if ( this.deferCountries.promise.isFulfilled() ){
			this.deferCountries.resolve( this._countries );
		}
		else{

			var fSuccess = function (result) {
				this._countries = result;
				this.deferCountries.resolve( this._countries );
			};

			var fError = function (err) {
				this._countries = [];
				this.deferCountries.reject( err );
			};

			fSuccess = _.bind( fSuccess, this );
			fError = _.bind( fError, this );

			if ( icms.Settings.isMock ){

				model.mock.Loader.getJsonData( "countries", fSuccess, fError);
			}
			else{

				model.odata.Coutries.getCountrySet( {}, fSuccess, fError );
			}
		}

		return this.deferCountries.promise;
	}

};
