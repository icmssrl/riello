//jQuery.sap.declare("model.collections.CustomerStatuses");
//jQuery.sap.require("model.persistence.Serializer");
//jQuery.sap.require("model.Customer");
//
//
//
//model.collections.CustomerStatuses = ( function ()
//{
//  var customerStatuses = [];
//  var defer = Q.defer();
//
//  return {
//
//    getById : function(id, customer, defer)
//    {
//      //var deferId= Q.defer();
//      var fSuccess  = function(result)
//      {
//        var customerStatus = _.find(result, _.bind(function(item)
//        {
//          return item.getId() === id;
//        }, this));
//        defer.resolve(customerStatus);
//      }
//      fSuccess = _.bind(fSuccess, this);
//
//      var fError  = function(err)
//      {
//        deferId.reject(err);
//      }
//      fError = _.bind(fError, this);
//
//       this.loadCustomerStatuses(customer,defer)
//      .then(_.bind(this.loadCustomerInvoices,this))
//      .then(fSuccess, fError);
//
//      //return defer.promise;
//    },
//
//    //Maybe to move in a odataClass in the future
//
//    loadCustomerStatuses : function(params,defer)
//    {
//          //defer = Q.defer();
//          var fSuccess = function(result)
//          {
//            var add = _.bind(this.addCustomerStatuses, this);
//            if(result && result.length>0)
//            {
//              customerStatuses=[];
//              for(var i = 0; i <result.length; i++)
//              {
//                var data = model.persistence.Serializer.customerStatus.fromSAP(result[i]);
//                add(new model.CustomerStatus(data));
//              }
//
//            }
//            else if(result)
//            {
//                customerStatuses=[];
//
//                var data = model.persistence.Serializer.customerStatus.fromSAP(result);
//                add(new model.CustomerStatus(data));
//            }
//
//
//            //defer.resolve(params);
//          }
//          fSuccess = _.bind(fSuccess, this);
//
//          var fError = function(err)
//          {
//            customerStatuses  = [];
//            //console.log("Error loading CustomerStatuses");
//            defer.reject(err);
//          }
//          fError = _.bind(fError, this);
//
//          model.odata.chiamateOdata.getCustomerCreditLimit(params, fSuccess, fError);
//
//        //return defer.promise;
//    },
//
//
//    loadCustomerInvoices : function(params,defer)
//    {
//
//          //defer = Q.defer();
//          var fSuccess = function(result)
//          {
//              if(result)
//            {
//                var data = model.persistence.Serializer.customerStatus.fromSAP(result);
//                customerStatuses[0].usedDebt = data.usedDebt;
//            }
//            //defer.resolve(customerStatuses);
//          };
//          fSuccess = _.bind(fSuccess, this);
//
//          var fError = function(err)
//          {
//            customerStatuses  = [];
//            //console.log("Error loading CustomerStatuses");
//            defer.reject(err);
//          };
//          fError = _.bind(fError, this);
//
//          model.odata.chiamateOdata.getCustomerInvoiceSet(params, fSuccess, fError);
//
//          //return defer.promise;
//    },
//
//
//    //--------------------------------------------------------------------
//
//    addCustomerStatuses : function (customerStatus)
//    {
//      customerStatuses.push(customerStatus);
//    },
//
//    getModel : function()
//    {
//      var model = new sap.ui.model.json.JSONModel();
//      var data = {"customerStatuses":[]};
//      for(var i = 0; i< customerStatuses.length; i++)
//      {
//        data.customerStatuses.push(customerStatuses[i].getModel().getData());
//      }
//      model.setData(data);
//      return model;
//    }
//
//  }
//})();
jQuery.sap.declare("model.collections.CustomerStatuses");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Customer");



model.collections.CustomerStatuses = ( function ()
{
  var customerStatuses = [];
  var defer = Q.defer();

  return {

    getById : function(id, customer, defer)
    {
      //var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var customerStatus = _.find(result, _.bind(function(item)
        {
          return item.getId() === id;
        }, this));
        //defer.resolve(customerStatus);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

       this.loadCustomerStatuses(customer,defer)
      //.then(_.bind(this.loadCustomerInvoices,this))
      .then(fSuccess, fError);

      //return defer.promise;
    },

    //Maybe to move in a odataClass in the future

    loadCustomerStatuses : function(params,defer)
    {
        var load = _.bind(this.loadCustomerInvoices,this);
          //defer = Q.defer();
          var fSuccess = function(result)
          {
            var add = _.bind(this.addCustomerStatuses, this);
            if(result && result.length>0)
            {
              customerStatuses=[];
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.customerStatus.fromSAP(result[i]);
                add(new model.CustomerStatus(data));
              }

            }
            else if(result)
            {
                customerStatuses=[];

                var data = model.persistence.Serializer.customerStatus.fromSAP(result);
                add(new model.CustomerStatus(data));
            }

            load(params,defer);
            //defer.resolve(params);
          }
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            customerStatuses  = [];
            ////console.log("Error loading CustomerStatuses");
            defer.reject(err);
          }
          fError = _.bind(fError, this);

          model.odata.chiamateOdata.getCustomerCreditLimit(params, fSuccess, fError);

        //return defer.promise;
    },


    loadCustomerInvoices : function(params,defer)
    {

          //defer = Q.defer();
          var fSuccess = function(result)
          {
              if(result)
            {
                var data = model.persistence.Serializer.customerStatus.fromSAP(result);
                customerStatuses[0].usedDebt = data.usedDebt;
                params.customerStatus = customerStatuses[0];
            }
            defer.resolve(params);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            customerStatuses  = [];
            ////console.log("Error loading CustomerStatuses");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          model.odata.chiamateOdata.getCustomerInvoiceSet(params, fSuccess, fError);

          //return defer.promise;
    },


    //--------------------------------------------------------------------

    addCustomerStatuses : function (customerStatus)
    {
      customerStatuses.push(customerStatus);
    },

    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"customerStatuses":[]};
      for(var i = 0; i< customerStatuses.length; i++)
      {
        data.customerStatuses.push(customerStatuses[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  }
})();
