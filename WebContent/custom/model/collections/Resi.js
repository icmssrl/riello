//jQuery.sap.declare("model.collections.praticheAnomale.Resi");
//jQuery.sap.require("model.persistence.Serializer");
//
//
//
//model.collections.praticheAnomale.Resi = ( function ()
//{
//  var resi = [];
//  var defer = Q.defer();
//
//  return {
//
//    getByTypeAndShippmentId : function(type, shippmentId)
//    {
//      var deferId= Q.defer();
//      var fSuccess  = function(result)
//      {
//        var res = [];
//        var product = _.find(result, _.bind(function(item)
//        {
//          if(item.getRenderType() === type && item.getShippmentId() === shippmentId)
//              res.push(item);
//        }, this));
//
//        deferId.resolve({"resi": res});
//      };
//      fSuccess = _.bind(fSuccess, this);
//
//      var fError  = function(err)
//      {
//        res = [];
//        deferId.reject(err);
//      };
//      fError = _.bind(fError, this);
//
//      this.loadResi()
//      .then(fSuccess, fError);
//
//      return deferId.promise;
//    },
//
//    getByShippmentId : function(shippmentId)
//    {
//      var deferId= Q.defer();
//      var fSuccess  = function(result)
//      {
//        var arr = [];
//        var product = _.find(result, _.bind(function(item)
//        {
//          if(item.getShippmentId() === shippmentId)
//              arr.push(item);
//
//        }, this));
//
//        deferId.resolve({"productsAP": arr});
//      };
//      fSuccess = _.bind(fSuccess, this);
//
//      var fError  = function(err)
//      {
//        arr = [];
//        deferId.reject(err);
//      };
//      fError = _.bind(fError, this);
//
//      this.loadProductsAP()
//      .then(fSuccess, fError);
//
//      return deferId.promise;
//    },
//
//    getSingleProduct : function(productId)
//    {
//      var deferId= Q.defer();
//      var fSuccess  = function(result)
//      {
//        var product = _.find(result, _.bind(function(item)
//        {
//          return item.getId() === productId;
//
//        }, this));
//
//        deferId.resolve(product);
//      };
//      fSuccess = _.bind(fSuccess, this);
//
//      var fError  = function(err)
//      {
//
//        deferId.reject(err);
//      };
//      fError = _.bind(fError, this);
//
//      this.loadProductsAP()
//      .then(fSuccess, fError);
//
//      return deferId.promise;
//    },
//
//
//    loadResi : function()
//    {
//        if(defer && defer.promise.isFulfilled())
//        {
//          defer.resolve(resi);
//        }
//        else
//        {
//          defer = Q.defer();
//          var fSuccess = function(result)
//          {
//            var add = _.bind(this.aggiungiResi, this);
//            if(result && result.length>0)
//            {
//              resi=[];
//              for(var i = 0; i <result.length; i++)
//              {
//                var data = model.persistence.Serializer.resi.fromSAP(result[i]);
//                add(new model.praticheAnomale.Resi(data));
//              }
//
//            }
//            defer.resolve(resi);
//          };
//          fSuccess = _.bind(fSuccess, this);
//
//          var fError = function(err)
//          {
//            products  = [];
//            //console.log("Errore nel caricamento dei resi!!");
//            defer.reject(err);
//          };
//          fError = _.bind(fError, this);
//
//          $.getJSON("custom/model/mock/data/resi.json")
//            .success(fSuccess)
//            .fail(fError);
//        }
//        // defer = Q.defer();
//
//
//
//        return defer.promise;
//    },
//
//
//    //--------------------------------------------------------------------
//
//    aggiungiResi : function (reso)
//    {
//      resi.push(reso);
//    },
//
//    getModel : function()
//    {
//      var model = new sap.ui.model.json.JSONModel();
//      var data = {"resi":[]};
//      for(var i = 0; i< resi.length; i++)
//      {
//        data.resi.push(resi[i].getModel().getData());
//      }
//      model.setData(data);
//      return model;
//    }
//
//  };
//})();
