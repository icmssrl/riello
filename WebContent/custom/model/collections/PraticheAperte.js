
jQuery.sap.declare("model.collections.PraticheAperte");
//jQuery.sap.require("model.persistence.Serializer");
//jQuery.sap.require("model.Position");


model.collections.PraticheAperte = ( function ()
{
  var orders = [];
  var defer = Q.defer();

  return {

    loadOrders : function()
    {
      var deferId= Q.defer();

      var obj={};

      var fSuccess  = function(result)
      {

        obj={"results":result};
        deferId.resolve(obj);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        obj={};
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

      //this.loadPositions()
      //.then(fSuccess, fError);
      $.getJSON("custom/model/mock/data/openPractices.json")
            .success(fSuccess)
            .fail(fError);

      return deferId.promise;
    },

    loadOrdersByShippmentId : function(shippmentId)
    {
      var deferId= Q.defer();
      var obj={};
      var arr= [];
      var fSuccess  = function(result)
      {
        var filteredOrders = _.find(result, _.bind(function(item)
        {
          //return item.getProductId() === code;
          return item.shippmentId === shippmentId;

        }, this));
        arr.push(filteredOrders);
        obj={"results":arr};
        deferId.resolve(obj);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        arr=[];
        obj={};
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

      //this.loadPositions()
      //.then(fSuccess, fError);
      $.getJSON("custom/model/mock/data/openPractices.json")
            .success(fSuccess)
            .fail(fError);

      return deferId.promise;
    },





//    getModel : function()
//    {
//      var model = new sap.ui.model.json.JSONModel();
//      var data = {"orderlist":[]};
//      for(var i = 0; i< orders.length; i++)
//      {
//        data.orders.push(orders[i].getModel().getData());
//      }
//      model.setData(data);
//      return model;
//    }

  };
})();
