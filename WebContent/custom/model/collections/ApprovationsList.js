
jQuery.sap.declare("model.collections.ApprovationsList");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Position");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.CustomerDiscount");
jQuery.sap.require("model.CustomerDiscountRequester");
jQuery.sap.require("utils.Asynchronous");

model.collections.ApprovationsList = ( function ()
{
  // var orders = [];
  approvations=[];


  return {

    getApprovationsList :  function(req) {

      var defer= Q.defer();

      var obj={};

      var data = {
        "Bukrs" : req.society,
        "Vkorg" : req.salesOrg,
        "Vtweg" : req.distrCh,
        "Spart" : req.division,
        "Vkbur" : req.areaManager,
        "Ustyp" : req.userType,
        "Uname" : req.username,
//        "Cdage" : req.agentCode,
//        "Kunnr" : req.customerId,
//        "Stato": req.orderStatus
        //aggiungere i campi stato Ordine e stato consegna

      };
      //se sono un TM aggiungo anche il mio codice e lo stato INS
      if(req.userType === "TM"){
        data.Vkgrp = req.territoryManager;
        data.Zsl3Stat = "INS";
      }
       else //aggiungo la stringa degli stati
       {
         data.Zsl3Stat = "MTM' or Zsl3Stat eq 'ATM";
       }


      utils.Busy.show();


      var fSuccess  = function(result)
      {
        utils.Busy.hide();
        //console.log(result);
        // -------------Modified From L.C. 07/09/16 10.53----------------------------
        
       
        var res = {"results": []};
        for(var i = 0; result && result.results && result.results.length > 0 && i <result.results.length ; i++)
        {
        	var listItem = model.persistence.Serializer.approvationsList.fromSAP(result.results[i]);
        	listItem.customer = new model.Customer(model.persistence.Serializer.customer.fromSAP(result.results[i]));
        	res.results.push(listItem);
        }
      //---------------------Before------------------------------------------------
//			var res = model.persistence.Serializer.approvationsList.fromSAPItems(result);
//        for(var i = 0; res && res.results && res.results.length > 0 && i < res.results.length ; i++)
//        {     
//          var temp= {//registry object of customer needed for odata requests
//							"id":res.results[i].customerId,
//							"customerName":res.results[i].customerName,
//							"agentCode":res.results[i].agentCode,
//							"agentName":res.results[i].agentName,
//							"salesOrg":res.results[i].salesOrg,
//							"distrCh":res.results[i].distrCh,
//							"division":res.results[i].division,
//							"society":res.results[i].society
//						}
//						var customer = new model.Customer({"registry":temp});
//            var customerDiscountRequester = new model.CustomerDiscountRequester(customer);
//            customerDiscountRequester.maxCustomerDiscount= (res.results[i].customerDiscountMax);
//            customerDiscountRequester.value = (res.results[i].customerDiscountValue);
//
//            res.results[i].discountRequester = customerDiscountRequester;
         //-------------------------------------------------------------------------------------------
//        }
        approvations = res;
        //console.log(res);
        // obj={"results":result};
        defer.resolve(res);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        utils.Busy.hide();
        approvations={};
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      //this.loadPositions()
      //.then(fSuccess, fError);
      // $.getJSON("custom/model/mock/data/orderList.json")
      //       .success(fSuccess)
      //       .fail(fError);
      model.odata.chiamateOdata.loadApprovationsList(data, fSuccess, fError);

      return defer.promise;
    },
    
    multiApprove:function(items, user)
    {
    	var deferExt = Q.defer();
    	
    	this.items = items;
    	this.user = user;
    	
    	var approveFunction = function(loop, i)
    	{
    		var defer = Q.defer();
    		
    		
    		var fSuccess = function(res)
    		{
    			console.log("Updated items "+ i);
    			defer.resolve(res);
    		}
    		fSuccess = _.bind(fSuccess, this);
    		
    		var fError = function(err)
    		{
    			console.log("Error update items "+ i);
    			defer.reject(err);
    		}
    		fError = _.bind(fError, this);
    		
    		
    		var customerDiscountRequester = new model.CustomerDiscountRequester(this.items[i].customer);
    		customerDiscountRequester.updateCustomerDiscount(this.user)
    		.then(fSuccess, fError)
//    		var customerDiscount  =new model.CustomerDiscount();
//    		customerDiscount.initialize(this.items[i].customerDiscountValue, this.items[i].customerDiscountStatus);
//    		customerDiscount.setDiscount(this.user, this.items[i].customerDiscountValue);
//    		
//    		//---------------------------------------------------------------------------------
//    		var data = {
//    				"Vkorg" : this.items[i].customer.registry.salesOrg,                                
//    				"Vtweg" : this.items[i].customer.registry.distrCh,                                
//    				"Spart" : this.items[i].customer.registry.division,
//    				"Kunnr" : this.items[i].customer.registry.id,                                  
//    				"Cdage"	 : this.items[i].customer.registry.agentCode,
//    				"Ustyp"  :this.user.type,
//    				"Bukrs":this.items[i].customer.registry.society,
//    				"Kschl":"ZSL3",
//    				"Kappl":"V",
//    				"Sconto":parseFloat(this.items[i].customerDiscountValue).toString(),
//    				"Note":"",
//    				"Stat":customerDiscount.getDiscountStatus()
//    				    
//    				};
//    		model.odata.chiamateOdata.updateCustomerDiscount(data, fSuccess, fError);
    		
    		return defer.promise;
    	}
    	approveFunction = _.bind(approveFunction, this);
    	
    	utils.Asynchronous.asyncLoop({
    		length:this.items ? this.items.length : 0,
    		functionToLoop: _.bind(approveFunction, this),
    		callback: _.bind(function(){
    			deferExt.resolve()
    		}, this)
    	})
    	
    	return deferExt.promise;
    }
    



  };
})();
