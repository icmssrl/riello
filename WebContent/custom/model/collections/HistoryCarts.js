
jQuery.sap.declare("model.collections.HistoryCarts");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.HistoryCart");


model.collections.HistoryCarts = ( function ()
{


  var historyCarts = [];


  return {

      /********************************************/

      //Temp code

//      getById: function(id){
//        var local = model.persistence.Storage.local.get("cartsHistory");
//        var localArr = local.carts;
//        var result = $.grep(localArr, function(e){ return e.orderId == id; });
//        return result[0];
//      },

        getById : function(id)
        {
            var result = this.loadHistoryCarts();

            var historyCart = _.find(result, _.bind(function(item)
            {
              return item.orderId === parseInt(id);
            }, this));





          return historyCart;
        },

      /**********************************************/


//    getById : function(id)
//    {
//      var deferId= Q.defer();
//      var fSuccess  = function(result)
//      {
//        var historyCart = _.find(result, _.bind(function(item)
//        {
//          return item.orderId === parseInt(id);
//        }, this));
//        deferId.resolve(historyCart);
//      };
//      fSuccess = _.bind(fSuccess, this);
//
//      var fError  = function(err)
//      {
//        deferId.reject(err);
//      };
//      fError = _.bind(fError, this);
//
//      this.loadHistoryCarts()
//      .then(fSuccess, fError);
//
//      return deferId.promise;
//    },



    //Maybe to move in a odataClass in the future

//    loadHistoryCarts : function(forceReloaded)
//    {
//        if(defer && defer.promise.isFulfilled() && !forceReloaded)
//        {
//          defer.resolve(historyCarts);
//        }
//        else
//        {
//          defer = Q.defer();
//          var fSuccess = function(result)
//          {
//            var add = _.bind(this.addHistoryCarts, this);
//            if(result && result.length>0)
//            {
//              historyCarts=[];
//              for(var i = 0; i <result.length; i++)
//              {
//                var data = model.persistence.Serializer.historyCart.fromSAP(result[i]);
//                add(new model.HistoryCart(data));
//              }
//
//            }
//            defer.resolve(historyCarts);
//          };
//          fSuccess = _.bind(fSuccess, this);
//
//          var fError = function(err)
//          {
//            historyCarts  = [];
//            //console.log("Error loading history carts");
//            defer.reject(err);
//          }
//          fError = _.bind(fError, this);
//
//          $.getJSON("custom/model/mock/data/historyCarts.json")
//            .success(fSuccess)
//            .fail(fError);
//        }
//        return defer.promise;
//    },

       loadHistoryCarts : function(forceReloaded)
        {
            if(!!model.persistence.Storage.local.get("cartsHistory")){
                var local = model.persistence.Storage.local.get("cartsHistory");
                var localArr = local.carts;
                var add = _.bind(this.addHistoryCarts, this);

                  historyCarts=[];
                  for(var i = 0; i < localArr.length; i++)
                  {
                    var data = model.persistence.Serializer.historyCart.fromSAP(localArr[i]);
                    add(new model.HistoryCart(data));
                  }
                return historyCarts;
            }else{
                historyCarts  = [];
                //console.log("Error loading history carts");
            }


        },

    //--------------------------------------------------------------------

    addHistoryCarts : function (historyCart)
    {
      historyCarts.push(historyCart);
    },

    // getModel : function()
    // {
    //   var model = new sap.ui.model.json.JSONModel();
    //   var data = {"historyCarts":[]};
    //   for(var i = 0; i< historyCarts.length; i++)
    //   {
    //     data.historyCarts.push(historyCarts[i].getModel().getData());
    //   }
    //   model.setData(data);
    //   return model;
    // },
    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();

      model.setData(historyCarts);
      return model;
    },

    loadCarts : function(user, customer, pref)
    {
      var defer = Q.defer();

      var data = {};
      data.Uname = user.userGeneralData.username;
      data.Kunnr = customer.registry.id;
      
      if(pref)
        data.Xpref = "X";

      utils.Busy.show();
      var fSuccess = function(result)
      {
        historyCarts= {};

        utils.Busy.hide();
        //console.log(result);
        var res = model.persistence.Serializer.historyCart.fromSAPItems(result);
        for(var i = 0; i< res.results.length ; i++)
        {
          res.results[i]= new model.HistoryCart(res.results[i]);
        }
        historyCarts = res;
        //console.log(res);

        defer.resolve(res);

      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {
        utils.Busy.hide();
        historyCarts={};
        //console.log(err);
        var error = JSON.parse(err.response.body);
				var errorDetails = error.error.innererror.errordetails;
				var msg = "History Carts Loading Failed";
				var detailsMsg = _.where(errorDetails, function(item){return (item.code.indexOf("IWBEP")>0)});
				for(var i = 0; detailsMsg && detailsMsg.length && i < detailsMsg.length; i++)
				{
					msg += ", " +detailsMsg[i].message;
				}
				sap.m.MessageToast.show(msg);
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadCarts(data, fSuccess, fError);

      return defer.promise;
    }

  };

})();
