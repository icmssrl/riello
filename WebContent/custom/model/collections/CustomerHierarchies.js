jQuery.sap.declare("model.collections.CustomerHierarchies");
jQuery.sap.require("model.persistence.Serializer");
model.collections.CustomerHierarchies = ( function() {

  var customerHierarchy  = [];
  var hDefer = Q.defer();

  return {
    loadHierarchies: function()
    {
      var fSuccess = function(result)
      {
          if(hierarchy.length>0){
              hierarchy = [];
          }
        _.forEach(result, _.bind(function(item){

          var obj = model.persistence.Serializer.hierarchyNode.fromSAP(item);
          hierarchy.push(new model.HierarchyNode(obj));

        }, this));
        hDefer.resolve(hierarchy);

      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {
        hierarchy = [];
        ////console.log("Error loading Hierarchy");
        hDefer.reject(err);
      };
      fError = _.bind(fError, this);

      $.getJSON("custom/model/mock/data/hierarchy.json")
      .success(fSuccess)
      .fail(fError);

      return hDefer.promise;
    }

  };

})();
