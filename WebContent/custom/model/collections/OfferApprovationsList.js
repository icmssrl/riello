
jQuery.sap.declare("model.collections.OfferApprovationsList");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Position");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Offer");
jQuery.sap.require("model.odata.chiamateOdata");


model.collections.OfferApprovationsList = ( function ()
{
  // var orders = [];
  offerApprovations=[];


  return {

    getOfferApprovationsList :  function(req) {

      var defer= Q.defer();

      var obj={};

      var data = {
        "BukrsVf" : req.society,
        "Vkorg" : req.salesOrg,
        "Vtweg" : req.distrCh,
        "Spart" : req.division,
        "Vkbur" : req.areaManager,
        "Vkgrp" : req.territoryManager,
        "Cdage" : req.agentCode,
        "Kunnr" : req.customerId,
        "Stato": req.orderStatus,
        "Auart":"ZOF",
        "Vbtyp":"B"
        //aggiungere i campi stato Ordine e stato consegna

      };

      if(req.userType)
    {
    	  switch (req.userType){
    	  
    	  case "AM":
    	  	data.Stat = ["ATM", "MTM"];
    	  	break;
    	  case "TM":
    		data.Stat = ["INS"]
    		break;
    	  }
    		  
    }

      utils.Busy.show();


      var fSuccess  = function(result)
      {
        utils.Busy.hide();
        //console.log(result);
        var res = model.persistence.Serializer.offerApprovationsList.fromSAPItems(result);
        for(var i = 0; res && res.results && res.results.length > 0 && i < res.results.length ; i++)
        {
//          //Temp To Mock-----------------------------------------
//          switch(res.results[i].orderStatus){
//            case "B":
//              res.results[i].orderStatus = "I";
//              res.results[i].orderStatusDescr = "In Attesa";
//              break;
//            case "A":
//              res.results[i].orderStatus = "A";
//              res.results[i].orderStatusDescr = "Accettato";
//              break;
//            case "C":
//              res.results[i].orderStatus = "M";
//              res.results[i].orderStatusDescr = "Respinto Con Modifica";
//              break;
//            default:
//              res.results[i].orderStatus = "R";
//              res.results[i].orderStatusDescr = "Respinto";
//              break;
//          }


//---------------------------------------------------------------------------------
          res.results[i]= new model.Offer(res.results[i]);

        }
        offerApprovations = res;
        //console.log(res);
        // obj={"results":result};
        defer.resolve(res);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        utils.Busy.hide();
        offerApprovations={};
        defer.reject(err);
      }
      fError = _.bind(fError, this);


      model.odata.chiamateOdata.loadOfferApprovationsList(data, fSuccess, fError);

      return defer.promise;
    },



  };
})();
