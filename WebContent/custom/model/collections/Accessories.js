
jQuery.sap.declare("model.collections.Accessories");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.AccessoriesModel");


model.collections.Accessories = ( function ()
{
  var accessories = [];
  //var defer = Q.defer();

  return {

    getById : function(code)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var accessory = _.find(result, _.bind(function(item)
        {
          return item.getId() === code;
        }, this));
        if(product)
        {
          deferId.resolve(accessory); //
        }
        else
        {
          ////console.log("Accessories.js -- accessory not found!");
        }

      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      };

      fError = _.bind(fError, this);

      this.loadAllAccessories()
      .then(fSuccess, fError);

      return deferId.promise;
    },



    //Maybe to move in a odataClass in the future

    loadAllAccessories : function()
    {
        var defer = Q.defer();

        if(defer && defer.promise.isFulfilled())
        {
          defer.resolve(accessories);
        }
        else
        {
          var fSuccess = function(result)
          {
            if(result && result.length>0)
            {
              accessories=[];
              var add = _.bind(this.addAccessory, this);
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.product.fromSAP(result[i]);
                add(new model.AccessoriesModel(data));
              }

            }
            defer.resolve(accessories);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            accessories  = [];
            ////console.log("Error loading accessories");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/accessories.json")
            .success(fSuccess)
            .fail(fError);
        }
        // defer = Q.defer();



        return defer.promise;
    },

     loadAccessoriesByProductId : function(userInfo, productId)
    {
        var defer = Q.defer();

//        if(defer && defer.promise.isFulfilled())
//        {
//          defer.resolve(accessories);
//        }
//        else
//        {

          var fSuccess = function(result)
          {
            if(result && result.length>0)
            {
              accessories=[];
              var add = _.bind(this.addAccessory, this);
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.product.fromSAP(result[i]);

                add(new model.AccessoriesModel(data));
              }

            }
            defer.resolve(accessories);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            accessories  = [];
            ////console.log("Error loading accessories");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/accessories.json")
            .success(fSuccess)
            .fail(fError);
//        }




        return defer.promise;
    },

    addAccessory : function(accessory)
    {
      accessories.push(accessory);
    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"accessories":[]};
      for(var i = 0; i< accessories.length; i++)
      {
        data.accessories.push(accessories[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
