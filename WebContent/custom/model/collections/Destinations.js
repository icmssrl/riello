jQuery.sap.declare("model.collections.Destinations");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Destination");


model.collections.Destinations = ( function ()
{
  var destinations = [];
  var defer = Q.defer();

  return {

    getById : function(destinationCode)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var destination = _.find(result, _.bind(function(item)
        {
          return item.getId() === destination;
        }, this));
        deferId.resolve(destination);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      };
      fError = _.bind(fError, this);

      this.loadDestinations()
      .then(fSuccess, fError);

      return deferId.promise;
    },

    //Maybe to move in a odataClass in the future

    loadDestinations : function()
    {
        if(defer && defer.promise.isFulfilled() && !forceReloaded)
        {
          defer.resolve(destinations);
        }
        else
        {
          defer = Q.defer();
          var fSuccess = function(result)
          {
            var add = _.bind(this.addDestinations, this);
            if(result && result.length>0)
            {
              destinations=[];
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.destination.fromSAP(result[i]);
                add(new model.Destination(data));
              }

            }
            defer.resolve(destinations);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            destinations  = [];
            ////console.log("Error loading Destinations");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/destinations.json")
            .success(fSuccess)
            .fail(fError);
        }
        // defer = Q.defer();



        return defer.promise;
    },
    //--------------------------------------------------------------------

    addDestinations : function (destination)
    {
      destinations.push(destination);
    },

    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"destinations":[]};
      for(var i = 0; i< destinations.length; i++)
      {
        data.destinations.push(destinations[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
