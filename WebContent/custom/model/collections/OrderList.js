
jQuery.sap.declare("model.collections.OrderList");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Position");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.odata.chiamateOdata");


model.collections.OrderList = ( function ()
{
  // var orders = [];
  orders=[];


  return {

    getOrderList :  function(req) {

      var defer= Q.defer();

      var obj={};

      var data = {
        "BukrsVf" : req.society,
        "Vkorg" : req.salesOrg,
        "Vtweg" : req.distrCh,
        "Spart" : req.division,
        "Vkbur" : req.areaManager,
        "Vkgrp" : req.territoryManager,
        "Cdage" : req.agentCode,
        "Kunnr" : req.customerId,
        "Stato": req.orderStatus, 
        "Matnr": req.productId
       // "Auart":"ZRD"
        //aggiungere i campi stato Ordine e stato consegna

      };
      if(req.fromDate)
    	  data.fromDate=req.fromDate;
      if(req.toDate)
    	  data.toDate=req.toDate;
//-------------------------New Develop-----------------------------------
//      var documentType = model.persistence.Storage.session.get("documentType");
//      var propString = "Vbtyp eq '" + documentType + "'";
//      url = url.concat(" and ");
//      url = url.concat(propString);
//------------------------------------------------------------------------------------

      utils.Busy.show();


      var fSuccess  = function(result)
      {
        utils.Busy.hide();
        //console.log(result);
        var res = model.persistence.Serializer.orderList.fromSAPItems(result);
        orders = res;
        //console.log(res);
        // obj={"results":result};
        defer.resolve(res);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        utils.Busy.hide();
        orders={};
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      //this.loadPositions()
      //.then(fSuccess, fError);
      // $.getJSON("custom/model/mock/data/orderList.json")
      //       .success(fSuccess)
      //       .fail(fError);
      model.odata.chiamateOdata.loadOrderList(data, fSuccess, fError);

      return defer.promise;
    },

    activateState:function(order)
    {
      var defer = Q.defer();
      var data = {
        "Vbeln":order.orderId,
        "Vkorg":order.salesOrg,
        "Vtweg":order.distrCh,
        "Spart":order.division
      }
      utils.Busy.show();
      var refreshOrder = function(result)
      {

        model.odata.chiamateOdata.loadOrderHeader(order.orderId, fSuccess, fError)

      }
      refreshOrder = _.bind(refreshOrder, this);

      var getById = function(orderId)
      {
         for(var i=0; i<orders.results.length; i++)
        {
          if(orders.results[i].orderId === orderId)
          {
            return orders.results[i];
          }
        }
        return false;
      };
      getById = _.bind(getById, this);

      var fSuccess = function(result)
      {
        utils.Busy.hide();
        //console.log(result);
        var o = getById(order.orderId);
        o.orderStatus = result.Stato;
        defer.resolve(o);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError= function(err)
      {
        utils.Busy.hide();
				var error = JSON.parse(err.response.body);
				var errorDetails = error.error.innererror.errordetails;
				var msg = "Order unlock failed";
				var detailsMsg = _.where(errorDetails, function(item){return (item.code.indexOf("IWBEP")>0)});
				for(var i = 0; detailsMsg && detailsMsg.length && i < detailsMsg.length; i++)
				{
					msg += ", " +detailsMsg[i].message;
				}
				sap.m.MessageToast.show(msg);

        //console.log(err);
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.unblockOrder(data, refreshOrder, fError)

      return defer.promise;
    },
    
    
    
    noActivateState:function(order)
    {
      var defer = Q.defer();
      var data = {
        "Vbeln":order.orderId,
        "Vkorg":order.salesOrg,
        "Vtweg":order.distrCh,
        "Spart":order.division
      }
      utils.Busy.show();
      var refreshOrder = function(result)
      {

        model.odata.chiamateOdata.loadOrderHeader(order.orderId, fSuccess, fError)

      }
      refreshOrder = _.bind(refreshOrder, this);

      var getById = function(orderId)
      {
         for(var i=0; i<orders.results.length; i++)
        {
          if(orders.results[i].orderId === orderId)
          {
            return orders.results[i];
          }
        }
        return false;
      };
      getById = _.bind(getById, this);

      var fSuccess = function(result)
      {
        utils.Busy.hide();
        //console.log(result);
        var o = getById(order.orderId);
        o.orderStatus = result.Stato;
        defer.resolve(o);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError= function(err)
      {
        utils.Busy.hide();
				var error = JSON.parse(err.response.body);
				var errorDetails = error.error.innererror.errordetails;
				var msg = "Order Refuse failed";
				var detailsMsg = _.where(errorDetails, function(item){return (item.code.indexOf("IWBEP")>0)});
				for(var i = 0; detailsMsg && detailsMsg.length && i < detailsMsg.length; i++)
				{
					msg += ", " +detailsMsg[i].message;
				}
				sap.m.MessageToast.show(msg);

        //console.log(err);
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.unblockOrder(data, refreshOrder, fError)

      return defer.promise;
    }
    // loadOrders : function()
    // {
    //   var deferId= Q.defer();
    //
    //   var obj={};
    //
    //   var fSuccess  = function(result)
    //   {
    //
    //     obj={"results":result};
    //     deferId.resolve(obj);
    //   }
    //   fSuccess = _.bind(fSuccess, this);
    //
    //   var fError  = function(err)
    //   {
    //     obj={};
    //     deferId.reject(err);
    //   }
    //   fError = _.bind(fError, this);
    //
    //   //this.loadPositions()
    //   //.then(fSuccess, fError);
    //   $.getJSON("custom/model/mock/data/orderList.json")
    //         .success(fSuccess)
    //         .fail(fError);
    //
    //   return deferId.promise;
    // },
    //
    // loadOrdersByCustomerId : function(customerId)
    // {
    //   var deferId= Q.defer();
    //   var obj={};
    //   var arr= [];
    //   var fSuccess  = function(result)
    //   {
    //     var filteredOrders = _.find(result, _.bind(function(item)
    //     {
    //       //return item.getProductId() === code;
    //       return item.customerId === customerId;
    //
    //     }, this));
    //     arr.push(filteredOrders);
    //     obj={"results":arr};
    //     deferId.resolve(obj);
    //   }
    //   fSuccess = _.bind(fSuccess, this);
    //
    //   var fError  = function(err)
    //   {
    //     arr=[];
    //     obj={};
    //     deferId.reject(err);
    //   }
    //   fError = _.bind(fError, this);
    //
    //   //this.loadPositions()
    //   //.then(fSuccess, fError);
    //   $.getJSON("custom/model/mock/data/orderList.json")
    //         .success(fSuccess)
    //         .fail(fError);
    //
    //   return deferId.promise;
    // },





//    getModel : function()
//    {
//      var model = new sap.ui.model.json.JSONModel();
//      var data = {"orderlist":[]};
//      for(var i = 0; i< orders.length; i++)
//      {
//        data.orders.push(orders[i].getModel().getData());
//      }
//      model.setData(data);
//      return model;
//    }

  };
})();
