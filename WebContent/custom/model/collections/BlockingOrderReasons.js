
jQuery.sap.declare("model.collections.BlockingOrderReasons");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.BlockingOrderReason");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("utils.Busy");


model.collections.BlockingOrderReasons = ( function ()
{
  var blockingOrderReasons = [];
  //var defer = Q.defer();

  return {


    loadBlockingOrderReasons : function(id)
    {
        var defer = Q.defer();
        utils.Busy.show();
        
        if(defer && defer.promise.isFulfilled())
        {
          defer.resolve(blockingOrderReasons);
        }
        else
        {
          var fSuccess = function(result)
          {
              utils.Busy.hide();
            if(result.results && result.results.length>0)
            {
              blockingOrderReasons=[];
              var add = _.bind(this.addBlockingOrderReason, this);
              for(var i = 0; i <result.results.length; i++)
              {
                var data = model.persistence.Serializer.blockingOrderReason.fromSAP(result.results[i]);
                add(new model.BlockingOrderReason(data));
              }

            }
            defer.resolve(blockingOrderReasons);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
              utils.Busy.hide();
            blockingOrderReasons  = [];
            ////console.log("Error loading BlockingOrderReasons!");
            defer.reject(err);
          };
          fError = _.bind(fError, this);
            
        model.odata.chiamateOdata.loadBlockingOrderReasons(id, fSuccess, fError);

//          $.getJSON("custom/model/mock/data/blockingOrderReasons.json")
//            .success(fSuccess)
//            .fail(fError);
        }
       
        return defer.promise;
    },
  
      
    addBlockingOrderReason : function(blockingOrderReason)
    {
      blockingOrderReasons.push(blockingOrderReason);
    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"blockingOrderReasons":[]};
      for(var i = 0; i< blockingOrderReasons.length; i++)
      {
        data.blockingOrderReasons.push(blockingOrderReasons[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
