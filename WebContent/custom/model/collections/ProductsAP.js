jQuery.sap.declare("model.collections.ProductsAP");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.praticheAnomale.ProductAP");


model.collections.ProductsAP = ( function ()
{
  var products = [];
  var defer = Q.defer();

  return {

    getById : function(productId)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var res = [];
        var product = _.find(result, _.bind(function(item)
        {
          if(item.getId() === productId)
              res.push(item);
        }, this));

        deferId.resolve({"productsAP": res});
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        res = [];
        deferId.reject(err);
      };
      fError = _.bind(fError, this);

      this.loadProductsAP()
      .then(fSuccess, fError);

      return deferId.promise;
    },

    getByShippmentId : function(shippmentId)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var arr = [];
        var product = _.find(result, _.bind(function(item)
        {
          if(item.getShippmentId() === shippmentId)
              arr.push(item);

        }, this));

        deferId.resolve({"productsAP": arr});
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        arr = [];
        deferId.reject(err);
      };
      fError = _.bind(fError, this);

      this.loadProductsAP()
      .then(fSuccess, fError);

      return deferId.promise;
    },

    getSingleProduct : function(productId)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var product = _.find(result, _.bind(function(item)
        {
          return item.getId() === productId;

        }, this));

        deferId.resolve(product);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {

        deferId.reject(err);
      };
      fError = _.bind(fError, this);

      this.loadProductsAP()
      .then(fSuccess, fError);

      return deferId.promise;
    },


    loadProductsAP : function()
    {
        if(defer && defer.promise.isFulfilled())
        {
          defer.resolve(products);
        }
        else
        {
          defer = Q.defer();
          var fSuccess = function(result)
          {
            var add = _.bind(this.addProductsAP, this);
            if(result && result.length>0)
            {
              products=[];
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.productAP.fromSAP(result[i]);
                add(new model.praticheAnomale.ProductAP(data));
              }

            }
            defer.resolve(products);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            products  = [];
            //console.log("Error loading products!!");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/productsAP.json")
            .success(fSuccess)
            .fail(fError);
        }
        // defer = Q.defer();



        return defer.promise;
    },
    //--------------------------------------------------------------------

    addProductsAP : function (product)
    {
      products.push(product);
    },

    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"productsAP":[]};
      for(var i = 0; i< products.length; i++)
      {
        data.products.push(products[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
