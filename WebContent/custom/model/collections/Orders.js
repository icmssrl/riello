
jQuery.sap.declare("model.collections.Orders");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Order");


model.collections.Orders = ( function ()
{
  var orders = [];
  var defer = Q.defer();

  return {

    getById : function(id)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var order = _.find(result, _.bind(function(item)
        {
          return item.getId() === id;
        }, this));
        deferId.resolve(order);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      };
      fError = _.bind(fError, this);

      this.loadOrders()
      .then(fSuccess, fError);

      return deferId.promise;
    },



    //Maybe to move in a odataClass in the future

    loadOrders : function(forceReloaded)
    {
        if(defer && defer.promise.isFulfilled() && !forceReloaded)
        {
          defer.resolve(orders);
        }
        else
        {
          defer = Q.defer();
          var fSuccess = function(result)
          {
            var add = _.bind(this.addOrders, this);
            if(result && result.length>0)
            {
              orders=[];
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.order.fromSAP(result[i]);
                add(new model.Order(data));
              }

            }
            defer.resolve(orders);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            orders  = [];
            //console.log("Error loading Orders");
            defer.reject(err);
          }
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/orders.json")
            .success(fSuccess)
            .fail(fError);
        }
        return defer.promise;
    },
    //--------------------------------------------------------------------

    addOrders : function (order)
    {
      orders.push(order);
    },

    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"orders":[]};
      for(var i = 0; i< orders.length; i++)
      {
        data.orders.push(orders[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
