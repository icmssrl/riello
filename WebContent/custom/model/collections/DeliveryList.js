
jQuery.sap.declare("model.collections.DeliveryList");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("sap.m.MessageToast");



model.collections.DeliveryList = ( function ()
{
  var deliveryList = [];
  //var defer = Q.defer();

  return {


    loadDeliveryList : function(orderId)
    {
        var defer = Q.defer();
        
          var fSuccess = function(result)
          {
            if(result.results && result.results.length>0)
            {
              deliveryList=[];
              //var add = _.bind(this.addTrackingOrderInfo, this);
              for(var i = 0; i <result.results.length; i++)
              {
                var data = model.persistence.Serializer.loadDeliveryList.fromSAP(result.results[i]);
                deliveryList.push(data);
              }

            }
            defer.resolve(deliveryList);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            deliveryList  = [];
            
            var err = JSON.parse(err.response.body);
            sap.m.MessageToast.show(err.error.innererror.errordetails[2].message);
            
            ////console.log("Error loading DeliveryList!");
            defer.reject(err);
          };
          fError = _.bind(fError, this);
            
        model.odata.chiamateOdata.loadDeliveryList(orderId, fSuccess, fError);

//          $.getJSON("custom/model/mock/data/blockingOrderReasons.json")
//            .success(fSuccess)
//            .fail(fError);
        //}
       
        return defer.promise;
    },
  
      
//    addTrackingOrderInfo : function(data)
//    {
//      trackingOrderInfo.push(data);
//    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"deliveryList":[]};
      for(var i = 0; i< deliveryList.length; i++)
      {
        data.deliveryList.push(deliveryList[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
