
jQuery.sap.declare("model.collections.TrackingOrderInfo");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");


model.collections.TrackingOrderInfo = ( function ()
{
  var trackingOrderInfo = [];
  //var defer = Q.defer();

  return {


    loadTrackingOrderInfo : function(deliveryId)
    {
        var defer = Q.defer();
        
          var fSuccess = function(result)
          {
            if(result.results && result.results.length>0)
            {
              trackingOrderInfo=[];
              //var add = _.bind(this.addTrackingOrderInfo, this);
              for(var i = 0; i <result.results.length; i++)
              {
                var data = model.persistence.Serializer.loadTrackingOrderInfo.fromSAP(result.results[i]);
                trackingOrderInfo.push(data);
              }

            }
            defer.resolve(trackingOrderInfo);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            trackingOrderInfo  = [];
            //console.log("Error loading TrackingOrderInfo!");
            defer.reject(err);
          };
          fError = _.bind(fError, this);
            
        model.odata.chiamateOdata.loadTrackingOrderInfo(deliveryId, fSuccess, fError);
       
        return defer.promise;
    },
  
      
//    addTrackingOrderInfo : function(data)
//    {
//      trackingOrderInfo.push(data);
//    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"trackingOrderInfo":[]};
      for(var i = 0; i< trackingOrderInfo.length; i++)
      {
        data.trackingOrderInfo.push(trackingOrderInfo[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
