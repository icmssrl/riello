
jQuery.sap.declare("model.collections.Products");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.Product");


model.collections.Products = ( function ()
{
  var products = [];
  //var defer = Q.defer();

  return {

    getByInfo : function(user, productInfo)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        if(result && result.results && result.results.length > 0)
        {
          var product = model.persistence.Serializer.product.fromSAP(result.results[0]);
          product = new model.Product(product);
          deferId.resolve(product);
        }
        else {
          deferId.reject({"Error": "Product not existent"});
        }
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      };

      fError = _.bind(fError, this);

      var data = _.clone(user);
      data.Matnr = productInfo;
      // data.Maktx = productInfo;

      model.odata.chiamateOdata.getProductDetail(data, fSuccess, fError)

      return deferId.promise;
    },



    //Maybe to move in a odataClass in the future

    loadAllProducts : function()
    {
        var defer = Q.defer();

        if(defer && defer.promise.isFulfilled())
        {
          defer.resolve(products);
        }
        else
        {
          var fSuccess = function(result)
          {
            if(result && result.length>0)
            {
              products=[];
              var add = _.bind(this.addProduct, this);
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.product.fromSAP(result[i]);
                add(new model.Product(data));
              }

            }
            defer.resolve(products);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            products  = [];
            //console.log("Error loading Products");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/products2.json")
            .success(fSuccess)
            .fail(fError);
        }
        // defer = Q.defer();



        return defer.promise;
    },

     loadProducts : function(lvl2Id)
    {
        var defer = Q.defer();

//        if(defer && defer.promise.isFulfilled())
//        {
//          defer.resolve(products);
//        }
//        else
//        {

          var fSuccess = function(result)
          {
            if(result && result.length>0)
            {
              products=[];
              var add = _.bind(this.addProduct, this);
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.product.fromSAP(result[i]);
                if(data.parentId === lvl2Id)
                  add(new model.Product(data));
              }

            }
            defer.resolve(products);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            products  = [];
            //console.log("Error loading Products");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/products2.json")
            .success(fSuccess)
            .fail(fError);
//        }




        return defer.promise;
    },

    addProduct : function(product)
    {
      products.push(product);
    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"products":[]};
      for(var i = 0; i< products.length; i++)
      {
        data.products.push(products[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
