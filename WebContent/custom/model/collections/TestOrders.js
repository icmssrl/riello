
jQuery.sap.declare("model.collections.TestOrders");



model.collections.TestOrders = ( function ()
{
  var orders = [];
  var defer = Q.defer();

  return {

    loadTestOrdersById : function(orderID)
    {
      var deferId= Q.defer();

     

      var fSuccess  = function(result)
      {
        var singleOrder = _.find(result, _.bind(function(item)
        {
          //return item.getProductId() === code;
          return item.orderId === orderID;
            
        }, this));
       //console.log(singleOrder);
       
        deferId.resolve(singleOrder);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        obj={};
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

      //this.loadPositions()
      //.then(fSuccess, fError);
      $.getJSON("custom/model/mock/data/testOrders.json")
            .success(fSuccess)
            .fail(fError);
        
      return deferId.promise;
    },
      
    loadAllTestOrders : function()
    {
      var deferId= Q.defer();
      
     

      var fSuccess  = function(result)
      {
//        var list = _.find(result, _.bind(function(item)
//        {
//          //return item.getProductId() === code;
//          return item.orderId === orderID;
//            
//        }, this));
//       //console.log(list);
//       all.results.push(list)
        var all = {"orders": result};
        deferId.resolve(all);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        obj={};
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

      //this.loadPositions()
      //.then(fSuccess, fError);
      $.getJSON("custom/model/mock/data/testOrders.json")
            .success(fSuccess)
            .fail(fError);
        
      return deferId.promise;
    },
      



  };
})();