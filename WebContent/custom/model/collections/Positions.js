jQuery.sap.declare("model.collections.Positions");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Position");
jQuery.sap.require("utils.ParseDate");


model.collections.Positions = (function () {
  var positions = [];
  var defer = Q.defer();

  return {

    getFutureDateByProductId: function (code, quantity, date) {
      var deferId = Q.defer();
     
      var obj = {};
      var dateObject;
      
      var fSuccess = function (result) {
          
      
        var product = _.find(result, _.bind(function (item) {
          //return item.getProductId() === code;
          return item.productId === code;

        }, this));
        var formatDate = new Date(product.futureDate);
        var quantityAvailable = parseInt(product.quantity);
         
        if (typeof date === "undefined" ){
            obj.success = false;
            obj.availableDate = formatDate;
            deferId.resolve(obj);
        }
        else if(typeof date === "object"){
              dateObject = date;
          }else{
              dateObject = utils.ParseDate().toNewDate(date);

          }
          
          if(dateObject){
              if ((parseInt(quantity)) <= quantityAvailable) {
                  obj.success = true;
                  obj.availableDate = dateObject;
                  //arr.push(temp);
                } else {
                  if (dateObject >= formatDate) {
                    obj.success = true;
                    obj.availableDate = dateObject;
                    //arr.push(temp);
                  } else {
                    obj.success = false;
                    obj.availableDate = formatDate;
                    //arr.push(temp);
                  }
                }
          }

        
        deferId.resolve(obj);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        obj = {};
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

      //this.loadPositions()
      //.then(fSuccess, fError);
      $.getJSON("custom/model/mock/data/availability.json")
        .success(fSuccess)
        .fail(fError);

      return deferId.promise;
    },



    //Maybe to move in a odataClass in the future

    //    loadPositions : function()
    //    {
    //        if(defer && defer.promise.isFulfilled())
    //        {
    //          defer.resolve(positions);
    //        }
    //        else
    //        {
    //          var fSuccess = function(result)
    //          {
    //            if(result && result.length>0)
    //            {
    //              positions=[];
    //              var add = _.bind(this.addPosition, this);
    //              for(var i = 0; i <result.length; i++)
    //              {
    //                var data = result[i];
    //                add(new model.Position(data));
    //              }
    //
    //            }
    //            defer.resolve(positions);
    //          };
    //          fSuccess = _.bind(fSuccess, this);
    //
    //          var fError = function(err)
    //          {
    //            positions  = [];
    //            //console.log("Error loading Positions");
    //            defer.reject(err);
    //          };
    //          fError = _.bind(fError, this);
    //
    //          $.getJSON("custom/model/mock/data/availability.json")
    //            .success(fSuccess)
    //            .fail(fError);
    //        }
    //        // defer = Q.defer();
    //
    //
    //
    //        return defer.promise;
    //    },
    //    addPosition : function(position)
    //    {
    //      positions.push(position);
    //    },
    //--------------------------------------------------------------------


    //    getModel : function()
    //    {
    //      var model = new sap.ui.model.json.JSONModel();
    //      var data = {"positions":[]};
    //      for(var i = 0; i< positions.length; i++)
    //      {
    //        data.positions.push(positions[i].getModel().getData());
    //      }
    //      model.setData(data);
    //      return model;
    //    }

  };
})();