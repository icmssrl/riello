jQuery.sap.declare("model.collections.Hierarchies");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.HierarchyNode");
jQuery.sap.require("model.odata.chiamateOdata");

model.collections.Hierarchies = ( function() {

  this.hierarchy  = [];
  this.products = [];

  this.loadODataHierarchies = function(hierarchyInfo)
  {


    var hDefer = Q.defer();
    var fSuccess = function(result)
    {
      this.hierarchy=[];
      _.forEach(result.results, _.bind(function(item){

        var obj = model.persistence.Serializer.hierarchyNode.fromSAP(item);
        this.hierarchy.push(new model.HierarchyNode(obj));

      }, this));
      hDefer.resolve(this.hierarchy);

    };
    fSuccess = _.bind(fSuccess, this);

    var fError = function(err)
    {
      this.hierarchy = [];
      ////console.log("Error loading Hierarchy");
      hDefer.reject(err);
    };
    fError = _.bind(fError, this);


    model.odata.chiamateOdata.getCatalogueSet(hierarchyInfo, fSuccess, fError)

    return hDefer.promise;
  };
  loadODataHierarchies = _.bind(this.loadODataHierarchies, this);

  this.loadODataProducts = function (productInfo)
  {
    var pDefer = Q.defer();
    var fSuccess = function(result)
    {
      this.products=[];
      _.forEach(result.results, _.bind(function(item){

        var obj = model.persistence.Serializer.product.fromSAP(item);
        this.products.push(new model.Product(obj));

      }, this));
      pDefer.resolve(products);

    };
    fSuccess = _.bind(fSuccess, this);

    var fError = function(err)
    {
      this.products = [];
      //console.log("Error loading Products");
      pDefer.reject(err);
    };
    fError = _.bind(fError, this);


    model.odata.chiamateOdata.getProductsList(productInfo, fSuccess, fError)

    return pDefer.promise;
  };
  loadODataProducts = _.bind(this.loadODataProducts, this);

  return {

    loadCatalogue : function(success, error, userInfo, itemInfo){
      var data = _.clone(userInfo);
      if(itemInfo)
      {
        if(itemInfo.last === "X" || itemInfo.last !== "")
        {
          data.Figlio = itemInfo.productId;
          loadODataProducts(data)
            .then(success, error);
        }
        else {
          data.Padre = itemInfo.productId;
          loadODataHierarchies(data)
            .then(success, error)
        }
      }
      else {
        loadODataHierarchies(data)
          .then(success, error)
      }
    }


    // loadHierarchies: function()
    // {
    //   var fSuccess = function(result)
    //   {
    //       if(hierarchy.length>0){
    //           hierarchy = [];
    //       }
    //     _.forEach(result, _.bind(function(item){
    //
    //       var obj = model.persistence.Serializer.hierarchyNode.fromSAP(item);
    //       hierarchy.push(new model.HierarchyNode(obj));
    //
    //     }, this));
    //     hDefer.resolve(hierarchy);
    //
    //   };
    //   fSuccess = _.bind(fSuccess, this);
    //
    //   var fError = function(err)
    //   {
    //     hierarchy = [];
    //     //console.log("Error loading Hierarchy");
    //     hDefer.reject(err);
    //   };
    //   fError = _.bind(fError, this);
    //
    //   $.getJSON("custom/model/mock/data/hierarchy.json")
    //   .success(fSuccess)
    //   .fail(fError);
    //
    //   return hDefer.promise;
    // }

  };

})();
