jQuery.sap.declare("model.collections.Shippments");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Shippment");


model.collections.Shippments = ( function ()
{
  var shippments = [];
  var defer = Q.defer();

  return {

    // getByOrderId : function(orderId)
    // {
    //   var deferId= Q.defer();
    //   var fSuccess  = function(result)
    //   {
    //     var res = [];
    //     var shippment = _.find(result, _.bind(function(item)
    //     {
    //       if(item.getOrderId() === orderId)
    //           res.push(item);
    //     }, this));
    //
    //     deferId.resolve({"shippments": res});
    //   };
    //   fSuccess = _.bind(fSuccess, this);
    //
    //   var fError  = function(err)
    //   {
    //     res = [];
    //     deferId.reject(err);
    //   };
    //   fError = _.bind(fError, this);
    //
    //   this.loadShippments()
    //   .then(fSuccess, fError);
    //
    //   return deferId.promise;
    // },

    getByOrderId : function(orderId)
    {
      var deferId= Q.defer();

      var req = {
        "Vbelv": orderId,
        "VbtypV":"C"
      }
      var fSuccess  = function(result)
      {
        var res = [];

        res = result.results;
        deferId.resolve({"shippments": res});
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        res = [];
        deferId.reject(err);
      };
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.loadFollowingFunction(req, fSuccess, fError)
      

      return deferId.promise;
    },

    getByShippmentId : function(shippmentId)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {

        var shippment = _.find(result, _.bind(function(item)
        {
          return item.getId() === shippmentId;

        }, this));

        deferId.resolve(shippment);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {

        deferId.reject(err);
      };
      fError = _.bind(fError, this);

      this.loadShippments()
      .then(fSuccess, fError);

      return deferId.promise;
    },


    loadShippments : function()
    {
        if(defer && defer.promise.isFulfilled())
        {
          defer.resolve(shippments);
        }
        else
        {
          defer = Q.defer();
          var fSuccess = function(result)
          {
            var add = _.bind(this.addShippments, this);
            if(result && result.length>0)
            {
              shippments=[];
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.shippment.fromSAP(result[i]);
                add(new model.Shippment(data));
              }

            }
            defer.resolve(shippments);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            shippments  = [];
            //console.log("Error loading shippments list");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/shippments.json")
            .success(fSuccess)
            .fail(fError);
        }
        // defer = Q.defer();



        return defer.promise;
    },
    //--------------------------------------------------------------------

    addShippments : function (shippment)
    {
      shippments.push(shippment);
    },

    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"shippments":[]};
      for(var i = 0; i< shippments.length; i++)
      {
        data.shippments.push(shippments[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
