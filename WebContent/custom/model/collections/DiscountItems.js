jQuery.sap.require("model.DiscountItem");

jQuery.sap.declare("model.collections.DiscountItems");

model.collections.DiscountItems = {
		
//		checkCommonDiscounts : function(items, keepValue)
//        {
//        	if(!items || items.length == 0)
//        		return null;
//        	
//        	var result = true;
//        	var customerDiscount = _.find(items, {typeId:"ZSL3"});
//        	
//        	
//    		for(var i = 0; customerDiscount && i< items.length ; i++)
//    		{
//    			if(items[i].typeId == "ZSL4")
//    			{
//    				if(customerDiscount.value > customerDiscount.limiter)
//    				{
//    					result = false;
//    					items[i].isEditable = false;
//    					if(!keepValue)
//    						items[i].value = items[i].maxValue;
//    				}
//    				else
//    					items[i].isEditable=true;
//    				
//    				break;
//    			}
//    		}
//    		
//        	
//        	return result;
//        },
        
		checkCommonDiscounts : function(items, options)
        {
        	if(!items || items.length == 0)
        		return null;
        	
        	var result = true;
        	var customerDiscount = _.find(items, {typeId:"ZSL3"});
        	
        	
    		for(var i = 0; customerDiscount && i< items.length ; i++)
    		{
    			if(items[i].typeId == "ZSL4")
    			{
    				if(customerDiscount.value > customerDiscount.limiter)
    				{
    					result = false;
    					
    					if(!options || !options.keepEditable)
    						items[i].isEditable = false;
    					
    					if(!options || !options.keepValue)
    						items[i].value = items[i].maxValue;
    				}
    				else
    					items[i].isEditable=true;
    				
    				break;
    			}
    		}
    		
        	
        	return result;
        },
		
		
		
        /* Function to check if value is greater than setted MaxValue-> value = maxValue */
        checkZsl3DiscountValues : function(items, basketType)
        {
        	if(!items || items.length == 0)
    		return null;
	
	    	var customerDiscount = _.find(items, {typeId:"ZSL3"});
	    	var result = !((basketType == "ZRD") && (customerDiscount.value > customerDiscount.limiter));
	    	if(!result)
	    		customerDiscount.value = customerDiscount.limiter;
	    	return result;

        }
}

