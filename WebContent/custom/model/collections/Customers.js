jQuery.sap.declare("model.collections.Customers");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Customer");
jQuery.sap.require("model.User");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.persistence.Storage");

model.collections.Customers = ( function ()
{
  var customers = [];
  // var lastUser = "";
  // var lastDivision = "";
  var user = {};
  var defer = Q.defer();

  return {

    getById : function(id)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var customer = _.find(result, _.bind(function(item)
        {
          return item.getId() === id;
        }, this));
        deferId.resolve(customer);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

      this.loadCustomers()
      .then(fSuccess, fError)

      return deferId.promise;
    },
    
    getCustomerDetail:function(infos)
    {
      var defer = Q.defer();
      var fSuccess = function(result)
      {
        var data = model.persistence.Serializer.customer.fromSAP(result);
        var customer = new model.Customer(data);
        defer.resolve(customer);
      }
      fSuccess = _.bind(fSuccess, this);
      var fError = function(err)
      {
        ////console.log("Error loading Customer Detail");
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.getCustomerDetail(infos,fSuccess, fError);

      return defer.promise;


    },



    //Maybe to move in a odataClass in the future

    // loadCustomers : function(username, division, forceReload)//In case of an added customer , force reload list cause it could return additional info
    // {
    //     if(!username)
    //       username = lastUser;
    //
    //     if(!division)
    //       division = lastDivision;
    //
    //     if(defer && defer.promise.isFulfilled() && (lastUser === username) && (lastDivision === division) && !forceReload)
    //     {
    //       defer.resolve(customers);
    //     }
    //     else
    //     {
    //
    //       defer = Q.defer();
    //       var fSuccess = function(result)
    //       {
    //         lastUser = username;
    //         lastDivision = division;
    //         var add = _.bind(this.addCustomers, this);
    //         if(result && result.length>0)
    //         {
    //           customers=[];
    //           for(var i = 0; i <result.length; i++)
    //           {
    //             var data = model.persistence.Serializer.customer.fromSAP(result[i]);
    //             if(data.registry.userName === username && data.registry.division === division)
    //               add(new model.Customer(data));
    //           }
    //
    //         }
    //         defer.resolve(customers);
    //       }
    //       fSuccess = _.bind(fSuccess, this);
    //
    //       var fError = function(err)
    //       {
    //         customers  = [];
    //         //console.log("Error loading Customers");
    //         defer.reject(err);
    //       }
    //       fError = _.bind(fError, this);
    //
    //       $.getJSON("custom/model/mock/data/customers2.json")
    //         .success(fSuccess)
    //         .fail(fError);
    //
    //     }
    //     // defer = Q.defer();
    //
    //
    //
    //     return defer.promise;
    // },
    //--------------------------------------------------------------------
    loadODataCustomers : function(user)//In case of an added customer , force reload list cause it could return additional info
    {
        // if(!username)
        //   username = lastUser;
        //
        // if(!division)
        //   division = lastDivision;
        // if(defer && defer.promise.isFulfilled() && (lastUser === username) && (lastDivision === division) && !forceReload)


          var defer = Q.defer();
          var fSuccess = function(result)
          {
            ////console.log(result);
            customers=[];
            var add = _.bind(this.addCustomers, this);
            if(result && result.results && result.results.length>0)
            {

              for(var i = 0; i <result.results.length; i++)
              {
                var data = model.persistence.Serializer.customer.fromSAP(result.results[i]);
                add(new model.Customer(data));
              }

            }
            defer.resolve(customers);
          }
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            customers  = [];
            //console.log("Error loading Customers");
            defer.reject(err);
          }
          fError = _.bind(fError, this);



          var wo = model.persistence.Storage.session.get("workingUser");
          
        // defer = Q.defer();
        //if(user.Spart == "RI" && user.Vkorg=="SA20" && user.Bukrs== "SI01" && user.Vtweg == "DO" && user.Cdage)
          if(wo.organizationData.results[0].canEditDiscount && user.Cdage)
        	model.odata.chiamateOdata.getCustomerDiscountList(user, fSuccess, fError);
        else
        	model.odata.chiamateOdata.getCustomersList(user,fSuccess, fError);


        return defer.promise;
    },
    addCustomers : function (customer)
    {
      customers.push(customer);
    },

    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"customers":[]};
      for(var i = 0; i< customers.length; i++)
      {
        data.customers.push(customers[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  }
})();
// model.Customers = ( function (){
//
//   customers = [];
//
//
//   return
//   {
//     addCustomers : function (customer)
//     {
//       this.customers.push(customer);
//     },
//     getCustomers : function ()
//     {
//       return this.customers;
//     },
//     getCustomerById : function (id)
//     {
//       return _.find(this.customers, function (item) {
//         return item.getId() === id
//       });
//     },
//     getModel : function()
//     {
//       var model = new sap.ui.model.json.JSONModel();
//
//
//       this.loadCustomers()
//       .then(
//         _.bind(function(){
//           model.setData({customers: this.getCustomers()});
//           return model;
//         }, this);
//       );
//
//
//     },
//     loadCustomers : function()
//     {
//       if(this.defer && this.defer.promise.isFulfilled())
//       {
//         this.defer.resolve(this.customers);
//       }
//       this.defer = Q.defer();
//
//       var fSuccess = function(result)
//       {
//         if(result && result.length>0)
//         {
//           for(var i = 0; i <result.length; i++)
//           {
//             this.addCustomers(new Customer(result[i]));
//           }
//
//         }
//         this.defer.resolve(this.customers);
//       }
//       fSuccess = _.bind(fSuccess, this);
//
//       var fError = function(err)
//       {
//         this.customers  = [];
//         //console.log("Error loading Customers");
//         this.defer.reject(err);
//       }
//       fError = _.bind(fError, this);
//
//       $.getJSON("custom/model/mock/customers.json")
//         .success(fSuccess)
//         .fail(fError);
//
//       return this.defer.promise;
//     }
//   };
// })();
