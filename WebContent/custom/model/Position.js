jQuery.sap.declare("model.Position");
jQuery.sap.require("model.Product");
jQuery.sap.require("model.Discount");
jQuery.sap.require("model.Order");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("utils.Message");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.collections.DiscountItems");


model.Position = (function () {

  Position = function (serializedData) {




    initialize = function () {

      this.order = undefined;
      this.orderId = undefined;
      this.positionId = undefined;
      this.quantity = undefined;
      this.totalListPrice = undefined;
      this.totalNetPrice = undefined;
      this.discountApplied = undefined;

      //aggiungere le proprietà del prodotto
      this.product = undefined;
      this.productId = undefined;
      this.scale = undefined;
      this.description = undefined;
      this.unitListPrice = 0;
      this.unitNetPrice = 0;
      this.currency = "EUR";
      this.forcedPrice="";
      this.quantity = 0;
      this.totalListPrice = 0;
      
      this.totalNetPrice= 0;
      this.totalTaxPrice=0;
      this.totalTransport=0;
      this.forcedPriceExistence=false;
      this.isTribut=false;
      // this.discountApplyed = 0;
      this.orderDiscounts=[];
      this.discountAcceptable = true;
    };


    //Yet to see if it makes any sense

    this.create = function (order, product) {
      this.setProduct(product);
      //---------------------------------
    //  this.setOrder(order);
//-----------------------------------------
      // this.quantity = 0;
      // this.totalListPrice = 0;
      // this.discountApplyed = 0;
    };



//    this.calculateTotalListPrice = function () {
//
//      this.totalListPrice = this.unitListPrice * this.quantity;
//
//      return this.totalListPrice;
//    };

    //maybe private
    this.setProduct = function (product) {


      // var p = model.collections.Product.getById(productId);
    var temp = new model.Product(product);
    if(!this.product || !this.product.isProduct || !this.product.isProduct())
      this.product = temp;
      //product=this.product;
      //Maybe to delete and get this properties directly from product
      this.productId = product.productId;
      this.scale = product.scale;
      this.description = product.description;
      this.unitListPrice = product.unitListPrice;
      this.unitNetPrice = product.unitNetPrice;
      this.currency = product.currency;
      this.type=product.type;
      this.isSanctioned = product.isSanctioned;
      this.wantedDate = product.reqDate ? product.reqDate : undefined;
      this.availableDate = product.availableDate ? product.availableDate : undefined;
      this.quantity = product.reqQty ? product.reqQty : this.quantity;
      this.available = product.status;
      this.totalListPrice= product.totalPrice ? product.totalPrice : (this.totalListPrice ? this.totalListPrice : 0) ;

      // for (var prop in p) {
      //   this[prop] = p[prop];
      // }

    };

    //Maybe private
    this.setOrder = function (order) {
      // this.order= order;
      this.orderId = order.getId();
      this.positionId = order.getPositions().length+1;
    };
    //option = {keepValue, keepEditable}
    this.setOrderDiscounts = function(discounts, options)
    {
    
      if(!this.orderDiscounts)
    	  this.orderDiscounts= [];
      
      for(var i = 0; i< discounts.length ; i++)
      {
    	var discount = _.cloneDeep(discounts[i]);
    	discount.positionId = this.positionId;
    	var found = false;
    	for(var j=0 ; j< this.orderDiscounts.length; j++)
    	{
    		if(this.orderDiscounts[j].typeId == discount.typeId)
    		{
    			for(var prop in discount)
    			{
    				this.orderDiscounts[j][prop]=discount[prop];
    			}
    			//this.orderDiscounts[j]=discount;
    			found=true;
    			break;
    		}
    	}
    	if(!found)
    		this.orderDiscounts.push(discount);  
      }
//      if(!options || !options.keepEditable)
//      {
//    	  var keepValue = options ? options.keepValue : false;
//          if(!model.collections.DiscountItems.checkCommonDiscounts(this.orderDiscounts, keepValue))
//        	  this.discountAcceptable = false;
//          else
//        	  this.discountAcceptable=true;
//      }
      
   
    	  var keepValue = options ? options.keepValue : false;
    	  var keepEditable = options ? options.keepEditable:false;
          if(!model.collections.DiscountItems.checkCommonDiscounts(this.orderDiscounts, {"keepValue": keepValue, "keepEditable": keepEditable}))
        	  this.discountAcceptable = false;
          else
        	  this.discountAcceptable=true;
      
      
      //--------Added L.C. 09/09/16 11:22------------------
      
      this.updateDiscountOriginalFromRI();
      //---------------------------------------------------
    };
    
    this.getOrderDiscounts = function()
    {
      return this.orderDiscounts;
    };
    

    this.setDiscount = function (discount) {
      this.discount = discount;
      this.discount.setPosition(this);
      //this.unitListPrice = discount.price.unitVal;
      //this.discount.setQuantity(this.quantity);
    };
//-----------------Added L.C. 09/09/16 11:51-------------------------------
    this.updateDiscountOriginalFromRI = function()
    {
    	 this.setDiscount(new model.Discount({discountArray: this.orderDiscounts}));
    };
    this.updateDiscountRIFromOriginal = function()
    {
    	var arr = [];
    	for (var i= 0; this.discount.discountArray && this.discount.discountArray.length > 0 && i< this.discount.discountArray.length; i++)
		 {
    		if(this.discount.discountArray[i].typeId == "ZSL3" || this.discount.discountArray[i].typeId == "ZSL4")
    		{
    			var value = Math.abs(this.discount.discountArray[i].value);
    			var price = this.discount.discountArray[i].price;
    			var item = _.find(this.orderDiscounts, {typeId : this.discount.discountArray[i].typeId});
    			if(item)
    			{
    				this.discount.discountArray[i] = item;
        			this.discount.discountArray[i].value = value;
        			this.discount.discountArray[i].price = price;
    			}
    		}
		 }
    	
		   
    	 this.setOrderDiscounts(this.discount.discountArray);
    };
    //---------------------------------------------------------------------
    this.setQuantity = function (quantity) {
      this.quantity = quantity;
    //  this.calculateTotalListPrice();
    };

    this.setWantedDate = function (date) {
      // if (typeof date !== "object" && typeof date !== "undefined") {
      //
      //   this.wantedDate = utils.ParseDate().toNewDate(date);
      //
      // } else if (typeof date === "undefined") {
      //   this.wantedDate = undefined;
      //   return this.wantedDate;
      // }
      this.wantedDate= date;



    };
    this.getAvailability = function(user, reqQty, reqDate)
    {
      var defer = Q.defer();

      var fSuccess = function(result) {
        this.setWantedDate(result.reqDate);
        this.setQuantity(result.reqQty);
        this.availableDate = result.availableDate;
        this.available = result.status;
        this.type= result.type;
        this.isSanctioned = result.isSanctioned;
        defer.resolve(this);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {
        defer.reject(err);
      }
      fError = _.bind(fError, this);
      
      this.product.getAvailability(user, reqQty, reqDate)
        .then(fSuccess, fError);

      return defer.promise;

    };

    this.getComponents = function(orgData)
    {
    	var defer = Q.defer();

    	var req = {
    			"Vkorg":orgData.Vkorg,
    			"Vtweg":orgData.Vtweg,
    			"Spart":orgData.Spart,
    			"MengePadre":1,
    			"EdatuPadre": utils.ParseDate().formatDate(new Date(), "yyyy-MM-ddT00:00:00"),
    			"MatnrPadre":this.productId

    	}
        var fSuccess = function(result) {
    		this.components = [];
    		if(result && result.results && result.results.length>0)
    		{
    			for(var i = 0; i< result.results.length; i++)
    	          {
    	            this.components.push({
    	              "componentId": result.results[i].Matnr,
    	              "description": result.results[i].Maktx,
    	              "availability" : (result.results[i].Xmiss === "X") ? false : true,
    	              "stockQty": parseInt(result.results[i].Stock),
    	              "reqQty" : parseInt(result.results[i].Kmpmg),
    	              "qtyXkit" : parseInt(result.results[i].Mnghd),
    	              "availableQty": parseInt(result.results[i].Menge),
    	              //"availableDate": !!result.Matnr_ATP_CompSet.results[i].Datum ? new Date(result.Matnr_ATP_CompSet.results[i].Datum) : undefined,
    	              "availableDate": !!result.results[i].Datum ? (result.results[i].Datum) : undefined
    	            });
    	          }
    		}

          defer.resolve(this.components);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function(err)
        {
        	this.components = [];
          defer.reject(err);
        }
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getKitComponents(req, fSuccess, fError);


        return defer.promise;

    };

    this.setForcedPrice = function(value)
    {
    	this.forcedPrice = (value == 0)? "":value;
    	this.discount.setForcedPrice(value);
    };
    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };

    this.getDiscount = function () {

    if(!this.discount)
    {
    	var discount =new model.Discount();
    	this.setDiscount(discount);
    	this.discount = discount; //I think it's useless
    }
    this.discount=new model.Discount(this.discount);//Hope it doesn't break
     return this.discount;
    };

    this.getQuantity = function () {
      if (!this.quantity)
        this.quantity = 0;
      return this.quantity;
    };
    this.getProduct = function () {

      return this.product;
    };
    this.initializeProduct=function()
    {
      var product = {
        "productId": this.productId,
        "description": this.description,
        "orderId":this.orderId,
        "quantity":this.quantity,
        "scale":this.scale,

      }
      this.product = new model.Product(product);
      // oi.orderId = sapData.orderId;
      // oi.positionId = sapData.positionId;
      // oi.quantity = sapData.quantity;
      // oi.totalListPrice = sapData.totalListPrice;
      // oi.totalNetPrice = sapData.totalNetPrice;
      // oi.discountApplyed = sapData.discountApplyed;
      // oi.productId = sapData.productId;
      // oi.scale = sapData.scale;
      // oi.description = sapData.description;
      // oi.unitListPrice = sapData.unitListPrice;
      // oi.unitNetPrice = sapData.unitNetPrice;
    };
    this.refreshPriceValues = function(customer, iva)
    {
      var defer = Q.defer();
      var data = {
        "IVkorg":customer.registry.salesOrg,
        "IVtweg":customer.registry.distrCh,
        "ISpart":customer.registry.division,
        "IWerks":"",
        "IVkbur":customer.registry.areaManager,
        "IVkgrp":customer.registry.territoryManager,
        "ICdage":customer.registry.agentCode,
        "IKunag":customer.registry.id,
        "IKunwe":"",
        "IMatnr":this.productId,
        "IMenge":this.quantity,
        "IVrkme":"",
        "ITaxk1":iva ? iva:""
      };

      var fSuccess = function(result)
      {
        var product =this.getProduct();
        product.setPrice(result);
        this.setProduct(product);

        if(result.PriceSimulationCondSet)
        {
        
        	//if(!this.discount || !this.discount.discountArray || this.discount.discountArray.length == 0)
        	//{
        		var discount = this.getDiscount();
                discount.setDiscountItems(result);
                this.setDiscount(discount);
                this.forcedPriceExistence=false;
//                var forcedPriceItem =_.find(result.PriceSimulationCondSet.results, {Kschl:'ZPMA'});
//                if(forcedPriceItem)
//                {
//              	  this.forcedPriceExistence=true;
//              	  this.setForcedPrice(forcedPriceItem.Kbetr);//Added in 06/07 - After reading anomalies (Was it my fault?)
//                }
                var forcedPriceItem =_.find(discount.discountArray, {typeId:'ZPMA'});
                if(forcedPriceItem)
                {
              	  this.forcedPriceExistence=true;
              	  this.setForcedPrice(forcedPriceItem.value);//Added in 06/07 - After reading anomalies (Was it my fault?)
                }
        	//}
          
        }
        defer.resolve(this);
      }
      var fError = function(err)
      {
        //console.log("Error Loading Position Price Details");
        utils.Message.getSAPErrorMsg(err, "Error loading Position Price Details");
        defer.reject(err);
      }
      fSuccess =_.bind(fSuccess, this);
      fError = _.bind(fError, this);

      model.odata.chiamateOdata.getProductPriceDetail(data, fSuccess, fError, true);
      return defer.promise;
    };

    this.refreshPricesBySAP = function(data)
    {
      this.totalListPrice = data.ItemTot;
      this.totalNetPrice = data.Netwr;
      this.totalTaxPrice = data.Mwsbp;
      this.totalTransport = data.ItemTransp;
    };

    this.refreshDiscountsPricesBySAP = function(discountArr)
    {
      var discount = this.getDiscount();
      discount.positionId = this.positionId;
      discount.refreshDiscountPrices(discountArr);
      this.setDiscount(discount);
      var forcedPriceItem = _.find(discountArr, {Kschl:"ZPMA"});
      if(forcedPriceItem)
      {
    	  this.setForcedPrice(forcedPriceItem.Kbetr);
    	  this.forcedPriceExistence = true;
      }
//      else
//      {
//    	  this.forcedPriceExistence=false;
//      }
    };

    this.copyDiscount=function(srcDiscount)
    {
    	var dest= this.getDiscount();
    	if(!dest)
    	{
    		//console.log("discount not exists yet");
    		return;
    		// this.setDiscount(srcDiscount)?
    	}
    	dest.copyDiscount(srcDiscount);
    };

    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
        if(prop == "wantedDate" || prop == "availableDate")
        {
        	this[prop]=new Date(this[prop]);
        }
      }
      if(this.product)
    {
    	  this.product = new model.Product(this.product);
    }
    };
    
    this.checkDiscount = function()
    {
    	var discount = this.getDiscount();
    };

    initialize();

    this.getProductId = function () {
      return this.productId;
    };

    this.getId = function () {
      return this.positionId;
    };
    
    this._refreshId = function(value)
    {
    	this.positionId = value;
    	if(this.orderDiscounts && this.orderDiscounts.length > 0)
    	{
    		for(var i = 0; i< this.orderDiscounts.length; i++)
    		{
    			this.orderDiscounts[i].positionId = value;
    		}
    	}
    	if(this.discount && this.discount.discountArray)
    	{
    		this.discount.positionId = value;
    		for(var j = 0; j < this.discount.discountArray.length ; j++)
    		{
    			this.discount.discountArray[j].positionId = value;
    		}
    	}
    };

    if (serializedData) {
      this.update(serializedData);
    };
    
    

    return this;
  };
  return Position;


})();
