jQuery.sap.declare("model.odata.chiamateOdata");
jQuery.sap.require("icms.Settings");
jQuery.sap.require("icms.Component");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("model.persistence.Storage");
model.odata.chiamateOdata = {
        _serviceUrl: icms.Component.getMetadata().getConfig().settings.serverUrl
        , // _serviceUrl: sessionStorage.getItem("serverUrl"),
        getOdataModel: function () {
            if (!this._odataModel) {
                if (sessionStorage.getItem("serverUrl")) {
                    this._serviceUrl = sessionStorage.getItem("serverUrl");
                }
                // this._serviceUrl = icms.Component.getMetadata().getConfig().settings.serverUrl;
                this._odataModel = new sap.ui.model.odata.ODataModel(this._serviceUrl, true);
            }
            return this._odataModel;
        }
        , getUserById: function (id, success, error) {
        	
        	var vers = model.persistence.Storage.session.get("AppVersion");
            this.getOdataModel().read("XF_User_DataSet?$filter=Uname eq '" + id + "' and Ualias eq '"+vers+"'", {
        	//this.getOdataModel().read("XF_User_DataSet?$filter=Uname eq '" + id + "'", {
                success: success
                , error: error
                , async: true
            });
        }
        , login: function (user, pwd, success, error) {
            this.getOdataModel().read("XF_User_DataSet?$filter=Uname eq '" + user + "' and password eq '" + pwd + "'", {
                success: success
                , error: error
                , async: true
            });
        }
        , checkPwd: function (username, success, error) {
            var url = "PasswordCheckSet";
            url += "(IUname='" + username + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , setNewPassword: function (newPass, old, success, error) {
            var url = "PasswordNewSet";
            var isFirst = true;
            for (var prop in old) {
                if (isFirst) {
                    url = url.concat("(");
                    isFirst = false;
                }
                else {
                    url = url.concat(",");
                }
                var propString = prop + "='" + old[prop] + "'";
                url = url.concat(propString);
            }
            url = url.concat(")");
            this.getOdataModel().update(url, newPass, {
                success: success
                , error: error
            });
        }
        , //----------------------------Customer --------------------------------------------------------
        getCustomersList: function (req, success, error) {
            //req is an object containing parameters passed to Odata then It's built the url
            var url = "GetCustomerSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , //GetEntity
        getCustomerDetail: function (req, success, error) {
            // var url = "SingleCustomerSet"
            var url = "CustomerSet"
            var isFirst = true;
            for (var prop in req) {
                if (isFirst) {
                    url = url.concat("(");
                    isFirst = false;
                }
                else {
                    url = url.concat(",");
                }
                var propString = prop + "='" + req[prop] + "'";
                url = url.concat(propString);
            }
            url = url.concat(")");
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , createCustomer: function (data, success, error) {
            url = "CustomerSet"
            var entity = {};
            for (var prop in data) {
                if (!data[prop]) continue;
                entity[prop] = data[prop];
            }
            entity.Kunnr = "";
            this.getOdataModel().create(url, entity, {
                success: success
                , error: error
            });
        }
        , checkCFeIVA: function (req, success, error) {
            var url = "CustomerDefaultSet";
            url += "(IStcd1='" + req.IStcd1 + "',";
            url += "IStceg='" + req.IStceg + "',";
            url += "Bukrs='" + req.Bukrs + "',";
            url += "Vkorg='" + req.Vkorg + "',";
            url += "Vtweg='" + req.Vtweg + "',";
            url += "Spart='" + req.Spart + "',";
            url += "Cdage='" + req.Cdage + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , getDestinations: function (req, success, error) {
            var url = "CustomerPartnerSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , //GET ENTITY
        loadCustomerDiscountConditions: function (req, success, error) {
            url = "SDCondReadSet";
            var isFirst = true;
            for (var prop in req) {
                if (isFirst) {
                    url = url.concat("(");
                    isFirst = false;
                }
                else {
                    url = url.concat(",");
                }
                var propString = prop + "='" + req[prop] + "'";
                if (prop === "Prsdt") propString = prop + "=datetime'" + req[prop] + "'";
                url = url.concat(propString);
            }
            url = url.concat(")");
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , addNewDestination: function (req, success, error) {
            url = "CustomerPartnerSet";
            var entity = {};
            for (var prop in req) {
                if (!req[prop]) continue;
                entity[prop] = req[prop];
            }
            this.getOdataModel().create(url, entity, {
                success: success
                , error: error
            });
        }
        , updateCustomer: function (data, success, error) {
            var workingUser = model.persistence.Storage.session.get("workingUser");
            data.Ustyp = workingUser.type;
            url = "CustomerSet"
            url += "(Kunnr='" + data.Kunnr + "',";
            url += "Bukrs='" + data.Bukrs + "',";
            url += "Vkorg='" + data.Vkorg + "',";
            url += "Vtweg='" + data.Vtweg + "',";
            url += "Spart='" + data.Spart + "',";
            url += "Cdage='" + data.Cdage + "')";
            // var entity={};
            // for(var prop in data)
            // {
            //   if(!data[prop])
            //     continue;
            //   entity[prop]=data[prop];
            // }
            this.getOdataModel().update(url, data, {
                success: success
                , error: error
            });
        }
        , loadDiscountConditions: function (req, success, error) {
            var url = "CustomerProdhCondSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            url = url.concat("&$top=50");
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , loadRequestedDiscounts: function (req, success, error) {
            var url = "CustomerProdhCondSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            url = url.concat("&$top=50");
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , loadProdHList: function (req, success, error) {
            var url = "Prodh1Set";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , //------------------------------------------------------------------------------------------------------
        //---------------------Product----------------------------------------------------------------------------
        getCatalogueSet: function (req, success, error) {
            //req is an object containing parameters passed to Odata then It's built the url
            var url = "CatalogoSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , getProductsList: function (req, success, error) {
            var url = "CatalogoPriceSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , getProductDetail: function (req, success, error) {
            var url = "MatnrPriceSet";
            // var isFirst = true;
            // var filters = [];
            // var matFilters = [];
            //
            //
            // for (var prop in req) {
            //   if (_.isEmpty(req[prop]))
            //     continue;
            //   if (prop !== "Matnr" && prop !== "Maktx") {
            //     filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
            //   } else {
            //     if (prop === "Matnr")
            //       matFilters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
            //     // else {
            //     //   matFilters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.Contains, req[prop]));
            //     // }
            //   }
            //   // if(isFirst)
            //   // {
            //   //   url = url.concat("?$filter=");
            //   //   isFirst=false;
            //   // }
            //   // else {
            //   //   url = url.concat(" and ");
            //   // }
            //   // var propString = prop + " eq '"+req[prop]+"'";
            //   // url= url.concat(propString);
            // }
            // matFilters = new sap.ui.model.Filter({
            //   filters: matFilters,
            //   and: false
            // });
            // filters.push(matFilters);
            // // filters = new sap.ui.model.Filter({filters: filters, and:true});
            // this.getOdataModel().read(url, {
            //   filters: filters,
            //   success: success,
            //   error: error,
            //   async: true
            // });
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , getProductAvailability: function (productInfo, success, error) {
            var url = "Mat_ATP_CheckSet"
            var entity = {
                "CVkorg": productInfo.salesOrg
                , "CVtweg": productInfo.distrCh
                , "CSpart": productInfo.division
                , "CMatnr": productInfo.productId
                , "CMenge": productInfo.quantity
                , "CEdatu": productInfo.reqDate //'2016-03-06T00:00:00'
            };
            var isFirst = true;
            for (var prop in entity) {
                if (isFirst) {
                    url = url.concat("(");
                    isFirst = false;
                }
                else {
                    url = url.concat(",");
                }
                if (prop === "CMenge") var propString = prop + "=" + entity[prop];
                else if (prop === "CEdatu") var propString = prop + "=datetime'" + entity[prop] + "'";
                else var propString = prop + "='" + entity[prop] + "'";
                url = url.concat(propString);
            }
            url = url.concat(")?$expand=Matnr_ATP_CompSet");
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , getKitComponents: function (req, success, error) {
            var url = "Matnr_ATP_CompSet"
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                if (prop === "MengePadre") var propString = prop + " eq " + req[prop];
                else if (prop === "EdatuPadre") var propString = prop + " eq datetime'" + req[prop] + "'";
                else var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , getAccessories: function (req, success, error) {
            var url = "AccessoriesSet";
            // var filters=[];
            // for(var prop in req)
            // {
            //   filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
            // }
            // this.getOdataModel().read(url, {filters:filters, success:success, error:error, async:true});
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            // url = url.concat("?$top=20");
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , getProductPriceDetail: function (req, success, error, expand) {
            var url = "MatnrPriceSimulationSet";
            var isFirst = true;
            for (var prop in req) {
                if (isFirst) {
                    url = url.concat("(");
                    isFirst = false;
                }
                else {
                    url = url.concat(",");
                }
                if (prop == "IMenge") var propString = prop + "=" + req[prop];
                else var propString = prop + "='" + req[prop] + "'";
                url = url.concat(propString);
            }
            url = url.concat(")");
            if (expand) {
                url = url.concat("?$expand=PriceSimulationCondSet");
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , // getProductPrice:function(data, success, error, detail);
        // {
        //   var url =
        //   var isFirst = true;
        //   for (var prop in entity) {
        //
        //     if (isFirst) {
        //       url = url.concat("(");
        //       isFirst = false;
        //     } else {
        //       url = url.concat(",");
        //     }
        //     if (prop === "CMenge")
        //       var propString = prop + "=" + entity[prop];
        //     else if (prop === "CEdatu")
        //       var propString = prop + "=datetime'" + entity[prop] + "'";
        //     else
        //       var propString = prop + "='" + entity[prop] + "'";
        //     url = url.concat(propString);
        //   }
        // },
        //getBlockingOrderReasons: function(req, success, error)
        //{
        //  var url = "SOBlocksSet";
        //  var filters=[];
        //  for(var prop in req)
        //  {
        //    filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
        //  }
        //  // url = url.concat("?$top=20");
        //  this.getOdataModel().read(url, {filters:filters, success:success, error:error, async:true});
        //},
        //---------------------------------OrderList------------------------------------
        loadOrderList: function (req, success, error) {
            var url = "SalesOrderGetListSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop]) && !_.isDate(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                if (prop.indexOf("fromDate") >= 0) {
                    var propString = "Audat1 eq datetime'" + utils.ParseDate().formatDate(req[prop], "yyyy-MM-ddT00:00:00") + "'";
                    //var propString ="Audat1 eq '" +utils.ParseDate().formatDate(req[prop], "yyyy-MM-ddT00:00:00")+"'";
                    url = url.concat(propString);
                }
                else if (prop.indexOf("toDate") >= 0) {
                    var propString = "Audat2 eq datetime'" + utils.ParseDate().formatDate(req[prop], "yyyy-MM-ddT00:00:00") + "'";
                    //var propString =  "Audat2 eq '" +  utils.ParseDate().formatDate(req[prop], "yyyy-MM-ddT00:00:00")  + "'";
                    url = url.concat(propString);
                }
                else {
                    var propString = prop + " eq '" + req[prop] + "'";
                    url = url.concat(propString);
                }
            }
            //-------------------------New Develop-----------------------------------
            var documentType = model.persistence.Storage.session.get("documentType");
            if (documentType) {
                var propString = "Vbtyp eq '" + documentType + "'";
                url = url.concat(" and ");
                url = url.concat(propString);
            }
            //------------------------------------------------------------------------------------
            url = url.concat("&$top=100");
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , unblockOrder: function (data, success, error) {
            var url = "SOBlockResetSet(Vbeln='" + data.Vbeln + "')";
            this.getOdataModel().update(url, data, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadOrderHeader: function (orderId, success, error) {
            var url = "SalesOrderHeaderSet(Vbeln='" + orderId + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadBlockingOrderReasons: function (orderId, success, error) {
            var url = "SOBlocksSet?$filter=Vbeln eq '" + orderId + "'";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadOrderItems: function (orderId, success, error) {
            var url = "SalesOrderItemSet?$filter=Vbeln eq '";
            url += orderId + "'";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadOrderConditions: function (orderId, success, error) {
            var url = "SalesOrderConditionSet?$filter=Vbeln eq '";
            url += orderId + "'";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadOrderNotes: function (orderId, success, error) {
            var url = "SalesOrderTextSet?$filter=Vbeln eq '";
            url += orderId + "'";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , deletePreAttach: function (req, success, error) {
            var url = "SO_TabAttachSet(IDrunr='" + req.IDrunr + "',IGuid='" + req.IGuid + "')";
            this.getOdataModel().remove(url, {
                success: success
                , error: error
            });
        }
        , loadAttachmentLinks: function (req, success, error) {
            var url = "SO_LinksSet";
            // var filters = [];
            // for (var prop in req) {
            //   filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
            // }
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                //  filters: filters,
                success: success
                , error: error
            })
        }
        , //--------------------------Deliveries---------------------------------------------------------------
        loadDeliveries: function (req, success, error) {
            var url = "SODeliverySet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            //var filters = [];
            // for (var prop in req) {
            //   filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
            // }
            this.getOdataModel().read(url, {
                //filters: filters,
                success: success
                , error: error
            })
        }
        , loadTrackingInfos: function (req, success, error) {
            var url = "SODeliveryTrakingSet";
            // var filters = [];
            // for (var prop in req) {
            //   filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
            // }
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                //filters: filters,
                success: success
                , error: error
            })
        }
        , getPODUrl: function (req, success, error) {
            var url = "DisplayPodSet(IVbeln='" + req + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , loadShippmentPrintPdfUrl: function (req, success, error) {
            var url = "PrintDeliverySet(IVbeln='" + req + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , loadInvoicePrintPdfUrl: function (req, success, error) {
            var url = "PrintInvoiceSet(IVbeln='" + req + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , loadDeliveryPractices: function (req, success, error) {
            var url = "CSReturn4DeliverySet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                //filters: filters,
                success: success
                , error: error
            })
        }
        , loadFollowingFunction: function (req, success, error) {
            var url = "SalesDocFlowSet";
            // var filters = [];
            // for (var prop in req) {
            //   filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
            // }
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                //filters: filters,
                success: success
                , error: error
            })
        }
        , //--------------------------------------------------------------------------------------------------------
        getSelect: function (property, params, success, error) {
            var url = "F4_" + property + "Set";
            // var filters = [];
            // for (var prop in params) {
            //   filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, params[prop]));
            // }
            var isFirst = true;
            for (var prop in params) {
                if (_.isEmpty(params[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                //maybe to check the type in the future
                var propString = prop + " eq '" + params[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                // filters: filters,
                success: success
                , error: error
            })
        }
        , //------------Order----------------------------------------
        createOrder: function (order, success, error) {
            url = "SalesOrderHeaderSet";
            var entity = {};
            for (var prop in order) {
                if (!order[prop]) continue;
                entity[prop] = order[prop];
            }
            entity.Vbeln = "";
            entity.SessionUID= model.persistence.Storage.session.get("sessionUID");
            this.getOdataModel().create(url, entity, {
                success: success
                , error: error
            });
        }
        , getCustomerCreditLimit: function (data, success, error) {
            var url = "CustomerCreditLimitSet(MKunnr='" + data.getId() + "',MBukrs='" + data.registry.society + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , getCustomerInvoiceSet: function (data, success, error) {
            var usType = JSON.parse(sessionStorage.getItem("user")).type;
            var d = data.registry;
            var url = "CustomerInvSet(MUstyp='" + usType + "',MBukrs='" + d.society + "',MVkorg='" + d.salesOrg + "',MVtweg='" + d.distrCh + "',MSpart='" + d.division + "',MVkbur='" + d.areaManager + "',MVkgrp='" + d.territoryManager + "',MCdage='" + d.agentCode + "',MKunnr='" + data.getId() + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadDeliveryList: function (orderId, success, error) {
            var url = "SODeliverySet?$filter=VbelnSo eq '" + orderId + "'";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadTrackingOrderInfo: function (deliveryId, success, error) {
            var url = "";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadDefaultValues: function (params, success, error) {
            var url = "CustomizingDefaultSet";
            var isFirst = true;
            for (var prop in params) {
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + params[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , //-----------------------Cart---------------------------------------
        saveCart: function (info, success, error) {
            url = "ShopCartSet";
            // var entity = {};
            // for (var prop in order) {
            //   if (!order[prop])
            //     continue;
            //   entity[prop] = order[prop];
            // }
            // entity.Vbeln = "";
            this.getOdataModel().create(url, info, {
                success: success
                , error: error
            });
        }
        , loadCarts: function (req, success, error) {
            var url = "ShopCartSet";
            // var filters=[];
            // for (var prop in data) {
            //   if(!_.isEmpty(data[prop]))
            //   {
            //     filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, data[prop]));
            //   }
            // }
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                //  filters: filters,
                success: success
                , error: error
            });
        }
        , getCartByOrder: function (data, success, error) {
            var url = "ShopCartSet";
            if (!data.Xpref) data.Xpref = "";
            url += "(Uname='" + data.Uname + "',Vbeln='" + data.Vbeln + "',Xpref='" + data.Xpref + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , //---------------------------Printing OData-----------------------
        loadOrderPdf: function (orderId, success, error) {
            url = "PrintOdVSet(IVbeln='" + orderId + "')";
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , //---------------------------Date Trasporto-----------------------
        getTransportDate: function (req, success, error) {
            var url = "DeliveryDaysSet";
            var isFirst = true;
            for (var prop in req) {
                //      if (_.isEmpty(req[prop]))
                //        continue;
                if (isFirst) {
                    url = url.concat("(");
                    isFirst = false;
                }
                else {
                    url = url.concat(",");
                }
                var propString = "";
                if (prop === "ILfdat") {
                    propString = prop + "=datetime'" + req[prop] + "'";
                }
                else {
                    propString = prop + "='" + req[prop] + "'";
                }
                url = url.concat(propString);
            }
            url = url + ")";
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , //----------------------Other Functions---------------------------------------
        getAbnormalPracticesList: function (req, success, error) {
            var url = "F4_Auart_CRSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) {
                    continue;
                }
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , //  getAbnormalPractice:function(req, success, error, expand)
        //  {
        //	  //Temp-----------------------------------------------------------------------------------------------------------------
        //	  var url = "CR_HeaderSet"+
        //	  			"(VbelnCr='"+req.VbelnCr+"',"+
        //	  			"Bukrs='"+req.Bukrs+"',"+
        //	  			"Vkorg='"+req.Vkorg+"',"+
        //	  			"Vtweg='"+req.Vtweg+"',"+
        //	  			"Spart='"+req.Spart+"',"+
        //	  			"Vkbur='"+req.Vkbur+"',"+
        //	  			"Vkgrp='"+req.Vkgrp+"',"+
        //	  			"Cdage='"+req.Cdage+"',"+
        //	  			"Auart='"+req.Auart+"',"+
        //	  			"")";
        //
        //
        //	  //----------------------------------------------------------------------------------------------------------------------
        //	  if(expand)
        //		 {
        //		  url += "?$expand=CR_ItemsSet,CR_MaterialsSet";
        //		 }
        //	  this.getOdataModel().read(url, {success: success, error:error});
        //  },
        getPracticeByParameters: function (data, success, error, expand) {
            //----------------------------------------------------------------------------------------------------------------
            var url = "CR_HeaderSet";
            url += "(VbelnCr='" + data.VbelnCr + "',";
            url += "Bukrs='" + data.Bukrs + "',";
            url += "Vkorg='" + data.Vkorg + "',";
            url += "Vtweg='" + data.Vtweg + "',";
            url += "Spart='" + data.Spart + "',";
            url += "Vkbur='" + data.Vkbur + "',";
            url += "Vkgrp='" + data.Vkgrp + "',";
            url += "Cdage='" + data.Cdage + "',";
            url += "Nmatr='" + data.Nmatr + "',";
            url += "Auart='" + data.Auart + "',";
            url += "VbelnVl='" + data.VbelnVl + "',";
            url += "Xabln='" + data.Xabln + "')";
            //----------------------------------------------------------------------------------------------------------------------
            if (expand) {
                url += "?$expand=CR_ItemsSet,CR_MaterialsSet";
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , getCommissions: function (req, success, error) {
            var url = "AgentCommissionsSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop]) && prop !== "startDate" && prop !== "endDate") {
                    continue;
                }
                if (!((prop.indexOf("startDate") >= 0) || (prop.indexOf("endDate") >= 0))) {
                    if (isFirst) {
                        url = url.concat("?$filter=");
                        isFirst = false;
                    }
                    else {
                        url = url.concat(" and ");
                    }
                    var propString = prop + " eq '" + req[prop] + "'";
                    url = url.concat(propString);
                }
            }
            if (req.endDate) {
                var endDateFrmt = utils.ParseDate().formatDate(req.endDate, "yyyy-MM-ddT00:00:00");
                var endDateFilter = "Erdat le datetime'" + endDateFrmt + "'";
            }
            if (req.startDate) {
                var startDateFrmt = utils.ParseDate().formatDate(req.startDate, "yyyy-MM-ddT00:00:00");
                var startDateFilter = "Erdat ge datetime'" + startDateFrmt + "'";
            }
            if (startDateFilter && endDateFilter) {
                var dateString = " and (" + startDateFilter + " and " + endDateFilter + ")";
            }
            else if (startDateFilter) {
                var dateString = " and " + startDateFilter;
            }
            else if (endDateFilter) {
                var dateString = " and " + endDateFilter;
            }
            if (dateString) {
                url = url.concat(dateString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , resoCreate: function (entity, success, error) {
            var url = "CR_HeaderSet";
            this.getOdataModel().create(url, entity, {
                success: success
                , error: error
            });
        }
        , //---------------------Version 2.0------------------------------------------------
        /* using this function to load customer List when agent and division RI
         * 
         */
        getCustomerDiscountList: function (req, success, error) {
            var url = "DiscountListSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , updateCustomerDiscount: function (data, success, error) {
            var workingUser = model.persistence.Storage.session.get("workingUser");
            data.Ustyp = workingUser.type;
            url = "CustomerDiscountMaintainSet"
            url += "(Kunnr='" + data.Kunnr + "',";
            url += "Bukrs='" + data.Bukrs + "',";
            url += "Vkorg='" + data.Vkorg + "',";
            url += "Vtweg='" + data.Vtweg + "',";
            url += "Spart='" + data.Spart + "',";
            url += "Ustyp='" + data.Ustyp + "',";
            url += "Kappl='" + data.Kappl + "',";
            url += "Kschl='" + data.Kschl + "',";
            url += "Cdage='" + data.Cdage + "')";
            // var entity={};
            // for(var prop in data)
            // {
            //   if(!data[prop])
            //     continue;
            //   entity[prop]=data[prop];
            // }
            this.getOdataModel().update(url, data, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadApprovationsList: function (req, success, error) {
            var url = "DiscountListSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                if (req.Ustyp === "AM" && prop === "Zsl3Stat") {
                    propString = "(" + prop + " eq '" + req[prop] + "')";
                }
                url = url.concat(propString);
            }
            //    url = url.concat("&$top=100");
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadOffersList: function (req, success, error) {
            var url = "SalesOrderGetListSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                var propString = prop + " eq '" + req[prop] + "'";
                url = url.concat(propString);
            }
            url = url.concat("&$top=100");
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadOfferApprovationsList: function (req, success, error) {
            var url = "SalesOrderGetListSet";
            var isFirst = true;
            for (var prop in req) {
                if (_.isEmpty(req[prop])) continue;
                if (isFirst) {
                    url = url.concat("?$filter=");
                    isFirst = false;
                }
                else {
                    url = url.concat(" and ");
                }
                if (prop == 'Stat') {
                    var propString = "";
                    if (req[prop].length == 1) propString = prop + " eq '" + req[prop][0] + "'";
                    else {
                        propString += "(";
                        for (var j = 0; j < req[prop].length; j++) {
                            if (j != 0) propString += " or ";
                            propString += prop + " eq '" + req[prop][j] + "'";
                        }
                        propString += ")";
                    }
                    url = url.concat(propString);
                }
                else {
                    var propString = prop + " eq '" + req[prop] + "'";
                    url = url.concat(propString);
                }
            }
            url = url.concat("&$top=100");
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , loadDiscounts_v2: function (orderId, success, error) {
            var url = "SalesOrderConditionSet?$filter=Vbeln eq '";
            url += orderId + "'";
            this.getOdataModel().read(url, {
                success: success
                , error: error
                , async: true
            });
        }
        , transformToOrder: function (offer, success, error) {
                var url = "/QuotationToSOSet";
//                var entity = {};
//                for (var prop in offer) {
//                    if (!offer[prop]) continue;
//                    entity[prop] = data[prop];
//                }
                
                this.getOdataModel().create(url, offer, {
                    success: success
                    , error: error
                });
            },
            //--------------Version 2.0.0---------------------------------
        loadNetworkStatus: function (success, error) {
            var url = "NetworkStatusSet";
            this.getOdataModel().read(url, {
                success: success
                , error: error
            });
        }
        , loadMaxCustomerDiscountValue: function (data, success, error) {
                var url = "SDCondReadSet";
                url += "(Kunnr='" + data.Kunnr + "',";
                url += "Bukrs='" + data.Bukrs + "',";
                url += "Vkorg='" + data.Vkorg + "',";
                url += "Vtweg='" + data.Vtweg + "',";
                url += "Spart='" + data.Spart + "',";
                url += "Zzagente1='" + data.Zzagente1 + "',";
                url += "Prsdt=datetime'" + data.Prsdt + "',";
                url += "Kappl='V',";
                url += "Kschl='"+(data.Kschl? data.Kschl : "ZSM3")+"')";
                //url += "Kschl='ZSM3')";
                this.getOdataModel().read(url, {
                    success: success
                    , error: error
                });
            },
    
    offerApprovations : function(r,success, error)
    {
        var url = "QuotationModHeaderSet"; //QuotationModItemSet
        var entity = {};
            for (var prop in r) {
                if (!r[prop]) continue;
                entity[prop] = r[prop];
            }
            
            this.getOdataModel().create(url, entity, {
                success: success
                , error: error
            });
    
    }
            //------------------------------------------------------------------
    }
    //{
    // 	'Uname': 'TPANICHI',
    //     'Ustyp' : 'AM' ,
    //     'Bukrs' : 'SI01',
    //     'Vkorg' : 'SA20',
    //     'Vtweg' : 'DO',
    //     'Spart' : 'RI',
    //     'Vkbur' : 'C002',
    //     'Vkgrp' : 'C27'
    // }