jQuery.sap.declare("utils.Formatter");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("sap.ui.core.format.DateFormat");
jQuery.sap.require("model.i18n");

utils.Formatter = {

  formatName: function (value) {
    //var tempValue = value.toUpperCase();
    //console.log(value);
    return value;
  },

  formatCustomer: function (value) {
    return (!value) ? "Warning" : "None";
  },

  formatCustomerText: function (value) {
    if (value) {
      return "";
    } else {
      return "is not a customer! call him!";
    }
    return "";
  },

  canCreate: function (value) {
    if (value.toUpperCase().indexOf("agent".toUpperCase()) >= 0)
      return true;
    else {
      return false;
    }
  },
  canEdit: function (value) {
    if (value.toUpperCase().indexOf("agent".toUpperCase()) >= 0)
      return true;
    else {
      return false;
    }
  },

  canUnlockCustomer: function (value) { 
//    if (value.toUpperCase() !== "AG")
    if (value) {
      return false;
    } else {
      return true;
    }
  },
  
  showIconAufsd0RC: function (value) { 
    if (value.toUpperCase() !== "RC") {
      return false;
    } else {
      return true;
    }
  },
  showIconAufsd0CK: function (value) { 
    if (value.toUpperCase() !== "CK") {
      return false;
    } else {
      return true;
    }
  },
  showIconAufsdk01: function (value) { 
    if (value.toUpperCase() !== "01") {
      return false;
    } else {
      return true;
    }
  },
  showIconAufsdv01: function (value) { 
    if (value.toUpperCase() !== "01") {
      return false;
    } else {
      return true;
    }
  },

  isSelected: function (obj) {
    if (obj !== "")
      return true;
    return false;

  },
  // adaptCheckBoxValue: function (val) {
  //   if (_.isEmpty(val))
  //     return false;
  //   return val;
  // },
  adaptCheckBoxValue: function (val) {
    if (_.isEmpty(val))
      return false;
    return true;
  },

  formatColorCreditWorthiness: function (payedOrder, totalOrder) {
    var p = 0;
    if (totalOrder > 0) {
      p = (payedOrder / totalOrder) * 100;
    } else {
      p = parseFloat(payedOrder);
    }
    //      var p = parseInt(orderRate);
    if (p >= 60) {
      return "Error";
    }
    //    else if (p >= 30 && p < 60) {
    //        return "Warning";
    //      }
    else {
      return "Success";
    }
  },

  formatPercentage: function (actual, total) {
    if (actual) {
      if (total > 0) {
        var p = (actual / total) * 100;
        return parseInt(p);
      } else {
        return parseFloat(actual);
      }
    } else {
      return 0;
    }
  },

  formatDecimalValue: function (val) {
    val = parseFloat(val);
    return val.toFixed(2);
  },

  formatVisibleIcon: function (val) {
    if (typeof (val) === "undefined") {
      return false;
    } else return true;
  },



  formatAvailable: function (value) {
    //      if(typeof val === "undefined"){
    //          return false;
    //      }else return val;
    if (value === true) {
      return "./custom/img/tick.png";
    } else {
      return "./custom/img/warning.png";
    }
  },

  returnRowStatus: function (value) {
    if (!value) return;
    switch (value) {
    case "APERTO":
      return "None";
      break;
    case "CHIUSO":
      return "None";
      break;
    case "BLOCCATO":
      return "#ff4d4d";
      break;
    case "CONVERTITO":
      return "#b3ffb3";
      break;
    }
  },

  formatValueState: function (wantedDate, futureDate) {
    if (typeof wantedDate === "undefined" || typeof futureDate === "undefined") {
      return "None";
    } else if ((new Date(wantedDate)) >= (new Date(futureDate))) {
      return "Success";
    } else return "None";
  },

  formatDateState: function (requestedDate, shippmentDate) {
    var reqDate = utils.Formatter.formatDate(requestedDate);
    var shippDate = utils.Formatter.formatDate(shippmentDate);
    var fromR = reqDate.split("/");
    var req = new Date(fromR[2], fromR[1] - 1, fromR[0]);
    var fromS = shippDate.split("/");
    var shipp = new Date(fromS[2], fromS[1] - 1, fromS[0]);
    if (reqDate === "" || shippDate === "") {
      return "None";
    } else if (shipp > req) {
      return "Warning";
    } else return "None";
  },
  formatDate: function (date) {
    if (!date)
      return "";
    var d = new Date(date);
    var dateFormatter = sap.ui.core.format.DateFormat.getInstance({
      pattern: "dd/MM/yyyy"
    });
    d = dateFormatter.format(d);
    return d;
  },

  //  markAsFavorite: function(marker){
  //      if(marker===true){
  //          return "true";
  //      }else{
  //          return "false";
  //      }
  //  },

  formatColorCreditDebt: function (usedDebt, totalDebt) {
    //    var d = parseInt(debtRate);
    //    var t = parseInt(totalDebt);
    var d = (usedDebt / totalDebt) * 100;
    //    if(t === 0 && debtRate === 100)
    //    {
    //      return "Error";
    //    }

    if (d >= 80) {
      return "Error";
    } else if (d >= 30 && d < 80) {
      return "Warning";
    } else {
      return "Success";
    }

  },

  formatDateValue: function (value) {
    var res = "";
    if (typeof value === "object" && value !== null) {
      var d = utils.ParseDate().formatDate(value, "dd-MMM-yyyy");
      var ds = d.split("-");
      res = ds[0] + " " + ds[1] + " " + ds[2];
      return res;
      //      var d = value.split('-');
      //      res = d[2]+"/"+d[1]+"/"+d[0];
    } else if (typeof value === "string" && value.length > 0) {
      var d = value.split('-');
      if (d[0].length < 3) {
        res = d[2] + "-" + d[1] + "-" + d[0];
      } else {
        res = d[0] + "-" + d[1] + "-" + d[2];
      }
      return res;

    } else {
      return res;
    }


  },

  formatDateValuePos: function (value) {
    var res = "";
    if (typeof value === "object" && value !== null) {
      var d = utils.ParseDate().formatDate(value, "dd-MM-yyyy");
      return d;
      //      var d = value.split('-');
      //      res = d[2]+"/"+d[1]+"/"+d[0];
    } else if (typeof value === "string" && value.length > 0) {
      return new Date(value);

    } else {
      return res;
    }


  },

  formatDateValueOrderHeader: function (value) {
    var res = "";
    if (typeof value === "object" && value !== null) {
      var d = utils.ParseDate().formatDate(value, "yyyy-MM-dd");
      return d;
      //      var d = value.split('-');
      //      res = d[2]+"/"+d[1]+"/"+d[0];
    } else if (typeof value === "string" && value.length > 0) {
      var d = value.split('-');
      res = d[2] + "-" + d[1] + "-" + d[0];
      return new Date(res);

    } else if (!value) {
      var d = new Date();
      d.setTime(d.getTime() + (1000 * 3600 * 24)); //metto la data di domani
      //var z =  utils.ParseDate().formatDate(d, "dd-MM-yyyy");
      return d;
    } else {
      return res;
    }


  },


  formatDecimal: function (value) {
    if (value !== undefined) {
      var r = parseFloat(value);
      return r.toFixed(2);
    }
    return false;

  },
  isAgent: function (type) {
    return (type === "AG");
  },

  formatUnlockEnable: function (value) {
    return (value === "AM" || value === "TM");
  },

  formatShowBlockReasons: function (value) {
    return (value === "B");
  },
  
  formatGetIconFollowingFunctions: function (value) {
	    var documentType;
	    documentType = model.persistence.Storage.session.get("documentType");
	    if (documentType === "H")
	    return "sap-icon://sys-cancel";
	    else
	    return "sap-icon://bbyd-active-sales";
	  },
	  formatGetTooltipFollowingFunctions: function (value, valueH) {
	    var documentType;
	    documentType = model.persistence.Storage.session.get("documentType");
	    if (documentType === "H")
	    return valueH;
	    else
	    return value;
	  },

	  formatShowFollowingFunctions: function (userType, orderStatus) {
	    var documentType;
	    documentType = model.persistence.Storage.session.get("documentType");
	    if (documentType !== "H")
	      return true;

	    return ((userType === "AM" || userType === "TM") && orderStatus === "B");
	  },
  

  formatUnlockOrderBtn: function (userType, orderStatus) {
    return ((userType === "AM" || userType === "TM") && orderStatus === "B");
  },



  setComponentVisibility: function (checkBtn, isKit) {
    if (!checkBtn || !isKit)
      return false;
    return true;
  },



  formatBlockDate: function (val) {
    return utils.ParseDate().formatDate(val, "dd-MM-yyyy");
  },


  formatPrice: function (val) {
    if (val) {
      return val.toLocaleString();

    } else {
      return val;
    }

  },

  formatRegType: function (regType, input) {
    if ((regType === "P" && input == "taxCode") || (regType === "C" && input == "vatNum"))
      return true;
    return false;



  },

  formatflagConsegneBtn: function (val) {
    if (val && val === "X") {
      return true;
    } else {
      return false;
    }
  },

  formatGruSwitch: function (value) {
    if (value === "11")
      return true;
    return false;
  },
  isNotGru: function (value) {
    if (value !== "11")
      return true;
    return false;
  },

  isNotConstruction: function (value) {
    if (value === "CANTIERE")
      return false;
    return true;
  },

  isOrderHeaderVisible: function (userEn, appEn) {
    return userEn ? appEn : userEn;
  },

  formatReleaseBlockDate: function (val) {
    if (val === 0 || val === "" || val === undefined || val === null) {
      return "";
    } else {
      return utils.ParseDate().formatDate(val, "dd-MM-yyyy");
    }
  },

  timeToMs: function (s) {
    if (s === 0 || s === "" || s === undefined || s === null) {
      return "";
    } else {
      var ms = s % 1000;
      s = (s - ms) / 1000;
      var secs = s % 60;
      s = (s - secs) / 60;
      var mins = s % 60;
      var hrs = (s - mins) / 60;
      if (hrs < 10)
        hrs = "0" + hrs;
      if (mins < 10)
        mins = "0" + mins;
      return hrs + ':' + mins + ':' + secs;
    }


  },
  formatDimensionByType: function (type) {
    if (type === "Desktop") {
      return "17em";
    }
    if (type === "Tablet") {
      return "13em";
    }
    if (type === "Phone") {
      return "12em";
    }
    return "17em";
  },
  isCartVisible: function (userEn, wr) {
    if (userEn && wr)
      return true;
    else {
      return false;
    }
  },

  formatTextValue: function (text) {
    var s = text.substring(0, 10);
    return s;
  },

  mobileTextButton: function (text)
  {
    var isMobile = sap.ui.Device.system.phone;
    if(isMobile)
    {
      return "";
    }
    else return text;

  },
  formatRegistryType:function(val)
  {
    switch(val)
    {
      case "AM":
        return "SM";
        break;
      case "TM":
        return "AM";
        break;
      default:
        return val;
        break;
    }
  },

  
  formatTribut: function (val) {
	    if (val === "SA10")
	      return true;
	    else 
	      return false;
	    
  },
 
  formatDateWithTimezone:function(value)
  {
	  var dt = new Date(value);
	  dt.setTime(dt.getTime()+dt.getTimezoneOffset()*60*1000);
	  var dd = dt.getDate() <= 9 ? "0"+dt.getDate(): dt.getDate() ;
	  var mm = (dt.getMonth()+1) <= 9 ? "0"+(dt.getMonth()+1) : (dt.getMonth()+1);
	  var yyyy = dt.getFullYear();
	  return dd+"/"+mm+"/"+yyyy;

  },
  formatCustomerDiscountStatus:function(type, status)
  {
    if(type !== "ZSL3")
      return "None";

    if(!status)
      return "None";
    if(status == "APP" || status=="MPP")
      return "Success";
    if(status == "ATM" || status =="MTM")
      return "Error";
    if(status == "M" || status== "INS")
      return "Warning";
  },
  formatCustomerDiscountStatusText:function(type, status)
  {
    if(type !== "ZSL3")
      return "";

    if(!status)
      return "";
    if(status == "APP" || status == "MPP")
    	
      return model.i18n.getText("approvedAM");
    if(status == "ATM" || status == "MTM")
      return model.i18n.getText("approvedTM");
    if(status == "INS")
      return model.i18n.getText("inserted");
    if(status== "REJ")
    	return model.i18n.getText("refused");
  },

  formatCustomerStatusByDiscount:function(status)
  {
    if(!status)
      return "None";
    if(status == "APP" || status == "MPP" )
      return "Success";
    if(status == "ATM" || status == "MTM")
      return "Error";
    if(status == "M" || status == "INS")
      return "Warning";
  },
  formatCustomerStatusTextByDiscount:function(status)
  {
    if(!status)
    {
	status="I";
	return model.i18n.getText("inserted");
    }
    	
    if(status == "S")
      return model.i18n.getText("approved");
    if(status == "N")
      return model.i18n.getText("refused");
    if(status == "M")
      return model.i18n.getText("modifiedRefused");
    if(status == "I")
    	return model.i18n.getText("inserted")
  },
 
  isDiscountEditable:function(isEditable, status, userType)
  {
	  if(!isEditable)
		  return false;
	  if(userType == "AG")
		  return (status == "");
	  
	  if(userType=="TM")
	{
		  return (status == "INS");
			  
	}
	  else if(userType == "AM")
		{
			  return (status !== "INS");
				  
		}
	  
  },
  isDiscountEditableFromList:function(status, userType)
  {
	  
	  if(userType == "AG" || userType=="TM")
	{
		  return (status == "INS");
			  
	}
	  else if(userType == "AM")
		{
			  return (status !== "INS");
				  
		}
	  
  },
  formatOfferStatusIconColor:function(status)
  {
	  if(!status)
		  return "";
	  if(status == "REJ")
		  return "red";
	  if(status == "INS")
		  return "yellow";
	  if(status.indexOf("TM")>0)
		  return "orange";
	  if(status.indexOf("PP")>0)
		  return "green";
	  else
		  return "";
	  
  },
    
    formatDimensionByTypeOfferInfo : function(type)
    {
    if (type === "Desktop") {
      return "41em";
    }
    if (type === "Tablet") {
      return "21em";
    }
    if (type === "Phone") {
      return "12em";
    }
    return "41em";
    }





};
