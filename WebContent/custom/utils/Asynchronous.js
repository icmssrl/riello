jQuery.sap.declare("utils.Asynchronous");

//funzione per loop di chiamate Asincrone
utils.Asynchronous = {
  
  asyncLoop : function(o){
   var i=-1; 
   var loop = function()
   { 
    i++; 
    if(i==o.length)
    {
     o.callback(); 
     return;
    } 
    o.functionToLoop(loop, i); 
   }; 
   loop();//init
   
  }
 
};