jQuery.sap.declare("utils.ParseDate");

utils.ParseDate = (function () {

    ParseDate = function () {



//    this.MONTH_NAMES=new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
      this.MONTH_NAMES=new Array('Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
this.DAY_NAMES=new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat');



    this.toNewDate = function(dateStr){
        var parts;
        if(dateStr.indexOf('/')>0){
            parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }else if(dateStr.indexOf('-')>0){
            parts = dateStr.split("-");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
    };


//    this.newDateToSlashSeparator= function(val){
//
//    };
//
//    this.newDateToDashSeparator= function(val){
//
//    };



    // ------------------------------------------------------------------
    // formatDate (oggetto_data, formato)
    // formato si deve inserire come stringa con i separatori '-' o '/'
    // Ritorna la data nel formato specificato.
    // ------------------------------------------------------------------
    this.formatDate = function(date,format) {

        date = new Date(date); //Lorenz's fault
        format=format+"";
        var result="";
        var i_format=0;
        var c="";
        var token="";
        var y=date.getYear()+"";
        var M=date.getMonth()+1;
        var d=date.getDate();
        var E=date.getDay();
        var H=date.getHours();
        var m=date.getMinutes();
        var s=date.getSeconds();
        var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
        // Converte parti reali dell'oggetto data in versione formattata
        var value=new Object();
        if (y.length < 4) {y=""+(y-0+1900);}
        value["y"]=""+y;
        value["yyyy"]=y;
        value["yy"]=y.substring(2,4);
        value["M"]=M;
        value["MM"]=this.LZ(M);
        value["MMM"]=this.MONTH_NAMES[M-1];
        value["NNN"]=this.MONTH_NAMES[M+11];
        value["d"]=d;
        value["dd"]=this.LZ(d);
        value["E"]=this.DAY_NAMES[E+7];
        value["EE"]=this.DAY_NAMES[E];
        value["H"]=H;
        value["HH"]=this.LZ(H);
        if (H==0){value["h"]=12;}
        else if (H>12){value["h"]=H-12;}
        else {value["h"]=H;}
        value["hh"]=this.LZ(value["h"]);
        if (H>11){value["K"]=H-12;} else {value["K"]=H;}
        value["k"]=H+1;
        value["KK"]=this.LZ(value["K"]);
        value["kk"]=this.LZ(value["k"]);
        if (H > 11) { value["a"]="PM"; }
        else { value["a"]="AM"; }
        value["m"]=m;
        value["mm"]=this.LZ(m);
        value["s"]=s;
        value["ss"]=this.LZ(s);
        while (i_format < format.length) {
            c=format.charAt(i_format);
            token="";
            while ((format.charAt(i_format)==c) && (i_format < format.length)) {
                token += format.charAt(i_format++);
                }
            if (value[token] != null) { result=result + value[token]; }
            else { result=result + token; }
            }
        return result;
	};



    this.LZ = function(x) {
        return(x<0||x>9?"":"0")+x;
    };

    return this;
    };
  return ParseDate;

})();
