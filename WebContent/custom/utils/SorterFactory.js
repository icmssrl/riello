jQuery.sap.declare("utils.SorterFactory");

utils.SorterFactory = {
		
		offerStatus: [{key: "AAM", value:3},
		              {key:"MAM", value:3},
		              {key: "ATM", value:2},
		              {key:"MTM", value:2},
		              {key: "INS", value:1},
		              {key:"REJ", value:1}],
		
      offerReason: [{key: "QFO", value:3},
	              {key:"QFQ", value:2},
	              {key: "NWF", value:1}],
	              
		getSorterOfferByStatus: function()
		{
			var sorter = new sap.ui.model.Sorter("orderStatus");
			sorter.fnCompare = function(a, b)
			{
				var aItem = _.find(this.offerStatus, {key: a});
				var aVal = aItem? aItem.value : 0;
				var bItem = _.find(this.offerStatus, {key:b});
				var bVal = bItem? bItem.value : 0;
				if(aVal>bVal)
					return 1;
				if(aVal == bVal)
					return 0;
				if(aVal < bVal)
					return -1;
				
			}
			
			return sorter;
		},
		
		getSorterOfferByReason:function()
		{
			var sorter = new sap.ui.model.Sorter("orderReason");
			sorter.fnCompare = function(a, b)
			{
				var aItem = _.find(this.offerReason, {key: a});
				var aVal = aItem? aItem.value : 0;
				var bItem = _.find(this.offerReason, {key:b});
				var bVal = bItem? bItem.value : 0;
				if(aVal>bVal)
					return 1;
				if(aVal == bVal)
					return 0;
				if(aVal < bVal)
					return -1;
				
			}
			
			return sorter;
		},
		
		getOfferSorter:function()
		{
			var sorters = [];
			sorters.push(this.getSorterOfferByStatus())
			sorter.push(this.getSorterOfferByReason());
			sorter.push(new sap.ui.model.Sorter("orderId", {descending:true}));
			return sorters;
		}
}