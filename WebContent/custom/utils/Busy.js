jQuery.sap.declare("utils.Busy");

utils.Busy = {

  busyDialog: new sap.m.BusyDialog(),

  show: function () {
    //sap.ui.core.BusyIndicator.show();
    utils.Busy.busyDialog.open();
  },

  hide: function () {
    //sap.ui.core.BusyIndicator.hide();
    utils.Busy.busyDialog.close();
  },
  get:function()
  {
    return utils.Busy.busyDialog;
  }
};
