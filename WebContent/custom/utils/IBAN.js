jQuery.sap.declare("utils.IBAN");

utils.IBAN = {

  ibanToData : function(stringIban)
  {
    if(stringIban.length!=27)
    {
      return "Error in format";
    }
    
    var iban = {
      nation: "",
      control: "",
      cin: "",
      abi: "",
      cab: "",
      conto: ""
    };
    
    iban.nation = stringIban.substr(0,2);
    iban.control = stringIban.substr(2,2);
    iban.cin = stringIban.substr(4,1);
    iban.abi = stringIban.substr(5,5);
    iban.cab = stringIban.substr(10,5);
    iban.conto = stringIban.substr(15);
    
    return iban;
  }
};
