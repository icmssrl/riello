jQuery.sap.declare("utils.Date");

utils.Date = {
	// Compare date based on the day, the arguments must be date
	compareDate:function(dateA, dateB)
	{
		if(!_.isDate(dateA) || !_.isDate(dateB))
		{
			return null;
		}
		if(dateA.getFullYear() == dateB.getFullYear())
		{
			if(dateA.getMonth() == dateB.getMonth())
			{
				return dateA.getDate() - dateB.getDate();
			}
			else
				return dateA.getMonth() - dateB.getMonth();
		}
		else
			return dateA.getFullYear() - dateB.getFullYear();
	}
	
}