jQuery.sap.declare("utils.ObjectUtils");

utils.ObjectUtils =
{
  // getKeys : function self(obj, path)
  getKeys : function (obj, path)
  {
    var propArray = [];

    if(!path)
    {
      path = "";
    }
    for(prop in obj)
    {
      if(_.isFunction(obj[prop]))
        continue;

      if(_.isObject(obj[prop]))
      {
        if(_.isArray(obj[prop]))
         {
           for(var i = 0; obj[prop] && (i< obj[prop].length); i++)
           {

            //  propArray = propArray.concat(self(obj[prop][i], prop+"/"+i+"/"));
             propArray = propArray.concat(this.getKeys(obj[prop][i], prop+"/"+i+"/"));

           }
         }
         else if(_.isDate(obj[prop]))
         {
            propArray.push(path+prop);
         }
         else
        {
            // propArray = propArray.concat(self(obj[prop], prop+"/"));
            propArray = propArray.concat(this.getKeys(obj[prop], prop+"/"));
         }
      }
      else
      {
        propArray.push(path+prop);
      }

    }
    return propArray;
  },

  getValues : function(obj, key)
  {
    var props = key.split("/");
    var result = obj;
    var i = 0;
    while(i<props.length)
    {
      if(result)
      {
        result = result[props[i]] ? result[props[i]] : undefined;

      }
      i++;
    }
    return result;
  },





}
