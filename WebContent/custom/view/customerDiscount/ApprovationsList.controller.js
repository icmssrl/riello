jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.ApprovationsList");

jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");

jQuery.sap.require("model.Current");
jQuery.sap.require("model.Customer");
jQuery.sap.require("model.CustomerRequester");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("utils.Message");
jQuery.sap.require("model.CustomerDiscount");


view.abstract.AbstractMasterController.extend("view.customerDiscount.ApprovationsList",
				{

					onExit : function() {

					},

					onInit : function() {

						// this.router =
						// sap.ui.core.UIComponent.getRouterFor(this);
						// this.router.attachRoutePatternMatched(this.handleRouteMatched,
						// this);
						view.abstract.AbstractController.prototype.onInit
								.apply(this, arguments);
						this.uiModel.setProperty("/searchProperty", [
								"orderId", "customerName" ]);

						this.userModel = new sap.ui.model.json.JSONModel();
						this.getView().setModel(this.userModel, "userModel");
						
						this.customerDiscountModel = new sap.ui.model.json.JSONModel();
						this.getView().setModel(this.customerDiscountModel, "cd");

					},

					handleRouteMatched : function(evt) {

						var name = evt.getParameter("name");

						if (name !== "customerDiscountApprovations") {
							return;
						}
						this.initializeCustomerDialog();
						this.user = model.persistence.Storage.session.get("workingUser");
						this.userModel.setData(this.user);

						this.approvationsListModel = new sap.ui.model.json.JSONModel();
            this.approvationsListModel.setSizeLimit(500);
						this.getView().setModel(this.approvationsListModel, "approvationsList");

						this.reqData = this.getUserInfo();
						if (!!sessionStorage.getItem("customerSession")
								&& sessionStorage.getItem("customerSession") === "true") {
							this.reqData = this.getCustomerInfo();
						}
						this.obblData = _.pick(this.reqData,
								[ "salesOrg", "division", "distrCh", "society",
										"areaManager" ]);
						
//						this.getView().setBusy(true);
//
//						model.collections.ApprovationsList.getApprovationsList(this.reqData) // forceReload
//						.then(
//								_.bind(function(res) {
//									// console.log(res);
//									this._sortList(res.results);
//									this.approvationsListModel.setData(res);
//									
//									this.approvationsListModel.refresh(true);
//									this.getView().setBusy(false);
//
//								}, this),
//								_.bind(function(err) {
//									this.getView().setBusy(false);
//									sap.m.MessageToast.show(utils.Message
//											.getError(err));
//								}, this));
						this.loadList();


						this.odataReq = this.getUserInfo();

					//	this.resetODataFilter();



					},
					
					loadList : function()
					{
						this.getView().setBusy(true);
						
						model.collections.ApprovationsList.getApprovationsList(this.reqData) // forceReload
						.then(
								_.bind(function(res) {
									// console.log(res);
									this._sortList(res.results);
									this.approvationsListModel.setData(res);
									
									this.approvationsListModel.refresh(true);
									this.getView().setBusy(false);

								}, this),
								_.bind(function(err) {
									this.getView().setBusy(false);
									sap.m.MessageToast.show(utils.Message
											.getError(err));
								}, this));
					},
					_sortList:function(list)
					{					                
						
						this.getStatusValue = function(item, type)
						{
							
							if(type== "AM")
								var discountStatusMap = {"INS": 1,
				                      "MTM":3,
				                      "ATM":3,
				                      "AAM":2,
				                      "MAM":2};
							else if(type == "TM")
								var discountStatusMap = {"INS": 3,
				                      "MTM":1,
				                      "ATM":1,
				                      "AAM":2,
				                      "MAM":2}
								
							
							return discountStatusMap[item.customerDiscountStatus]; 
							
							
						}
						
						list.sort(_.bind(function(a, b){
							
							
							var aDiscountStatus = this.getStatusValue(a, this.user.type);
							var bDiscountStatus = this.getStatusValue(b, this.user.type);
							if(aDiscountStatus > bDiscountStatus)
								return -1;
							if(aDiscountStatus < bDiscountStatus)
								return 1;
							else
							{
								if(a.customerId<b.customerId)
									return 1;
								if(a.customerId > b.customerId)
									return -1;
								else
									return 0;
							}
							
							
						}, this))
						
					},
					onInfoPress : function(evt) {


						this.navToCustomerDetails(evt);
					},
					onDiscountSelectionChange : function(evt)
					{
						var src =evt.getParameter('listItem');
						var discount = src.getBindingContext("approvationsList").getObject();
						var customerDiscount = new model.CustomerDiscount();
						customerDiscount.initialize(discount.customerDiscountValue, discount.customerDiscountStatus);
						if(!customerDiscount.isEditable(this.user) || discount.customerDiscountStatus == "APP" || discount.customerDiscountStatus == "MPP")
						//if(discount.customerDiscountStatus !== "I")
						{
							sap.m.MessageToast.show(model.i18n.getText("impossibleModifyThisCustomerDiscount")+" "+discount.customerId);
							src.setSelected(false);
						}
					},

					onEditPress:function(evt)
					{
						var src = evt.getSource();
						this.selectedRow = src.getParent();
						var selectedItem = src.getBindingContext("approvationsList").getObject();
						//this.customerDiscountRequester =_.cloneDeep(selectedItem.discountRequester);
						this.customerDiscountRequester = new model.CustomerDiscountRequester(selectedItem.customer);
						this.selectedItem = selectedItem;
						this.selectedDiscount = new model.CustomerDiscount();
						this.selectedDiscount.initialize(this.selectedItem.customerDiscountValue, this.selectedItem.customerDiscountStatus);


						 this.customerDiscountRequester.loadMaxCustomerDiscountValue(this.user, true)
						  .then(_.bind(function(res){
							  this.customerDiscountModel.setData(this.customerDiscountRequester.getModel().getData());
						      //this.getView().setModel(this.customerDiscountModel, "cd");

//						    if(!this.customerDiscountDialog)
//						    {
//						      this.customerDiscountDialog = sap.ui.xmlfragment("view.dialog.CustomerDiscountDialog", this);
//						    }
//						    var page = this.getView().byId("approvationsListPageId");
//
//						    page.addDependent(this.customerDiscountDialog);
						    this.customerDiscountDialog.open();
							  
						  }, this))
						
					},

					onCustomerDiscountValueChange:function(evt)
					{
//						this.customerDiscountModel.setProperty("/value", parseFloat(this.customerDiscountModel.getProperty("/value")));
//						this.customerDiscountModel.refresh();
						
						if(parseFloat(this.customerDiscountModel.getProperty("/value"))>parseFloat(this.customerDiscountModel.getProperty("/maxCustomerDiscount")))
					    {
						  this.customerDiscountModel.setProperty("/value", parseFloat(this.customerDiscountModel.getProperty("/maxCustomerDiscount")));
					    	
						  sap.m.MessageBox.show(model.i18n.getText("customerDiscountInputError_maxValueOverflow"));
					    }
						
						this.customerDiscountModel.refresh();
					},
					onCustomerDiscountClose:function(evt)
					{
						this.customerDiscountDialog.close();
					},
					onCustomerDiscountOK:function(evt)
					{
						var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
						var that = this;
						sap.m.MessageBox.confirm(
								model.i18n.getText("approveChanges"), {
								styleClass: bCompact? "sapUiSizeCompact" : "",
								actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
								onClose: function(oAction) { 
									if(oAction==="YES"){
										that.customerDiscountDialog.close();
										
										//----Modified from L.C. 07/09/16----------------------------------------
										that.customerDiscountRequester.updateCustomerDiscount(that.user) 
										.then(_.bind(function(res){
											console.log(that.customer);
											sap.m.MessageToast.show(model.i18n.getText("discountRequestSendSuccess"));
											
											that.selectedRow.setBackgroundColor("#b3ffb3");
											
											that.selectedItem.customerDiscountStatus=that.customerDiscountRequester.status;
											that.selectedItem.customerDiscountValue=that.customerDiscountRequester.value;
											that.selectedItem.customerDiscountStatusDescr=that.customerDiscountRequester.statusDescr;
											that.getView().getModel("approvationsList").refresh();
											//this.loadList();
											
											}, that),
											_.bind(function(err)
											{
												sap.m.MessageToast.show(model.i18n.getText("errorDiscountUpdate"));
											}, that))
										
										//-------------------Before--------------------------------------------------------------
//										this.selectedDiscount.setDiscount(this.user, this.customerDiscountRequester.value);
//										this.selectedItem.customerDiscountValue = this.selectedDiscount.getDiscountValue();
//										this.selectedItem.customerDiscountStatus = this.selectedDiscount.getDiscountStatus();
//										this.selectedItem.customerDiscountStatusDescr = this.selectedDiscount.getDiscountStatusDescr();
//										
//										this.getView().getModel("approvationsList").refresh();
//										sap.m.MessageToast.show(model.i18n.getText("discountModifingSuccess"));
//									
										//----------------------------------------------------------------------------------------------
									}else{
										return;
									}
								}.bind(this)
							}
						);
						

					},





					onFilterPress : function() {
						this.filterModel = model.filters.Filter
								.getModel(
										this.approvationsListModel.getData().results,
										"orders");
						this.getView().setModel(this.filterModel, "filter");
						var page = this.getView().byId("approvationsListPageId");
						this.filterDialog = sap.ui.xmlfragment(
								"view.dialog.filterDialog", this);
						page.addDependent(this.filterDialog);
						this.filterDialog.open();

					},
					onFilterDialogClose : function() {
						this.filterDialog.close();
					},

					onFilterPropertyPress : function(evt) {

						var parentPage = sap.ui.getCore().byId("parent");
						var elementPage = sap.ui.getCore().byId("children");
						// console.log(this.getView().getModel("filter").getData().toString());
						var navCon = sap.ui.getCore().byId("navCon");
						var selectedProp = evt.getSource().getBindingContext(
								"filter").getObject();
						this.getView().getModel("filter").setProperty(
								"/selected", selectedProp);
						this.elementListFragment = sap.ui.xmlfragment(
								"view.fragment.filterList", this);
						elementPage.addContent(this.elementListFragment);

						navCon.to(elementPage, "slide");
						this.getView().getModel("filter").refresh();
					},

					onBackFilterPress : function(evt) {
						// this.addSelectedFilterItem();
						this.navConBack();
						this.getView().getModel("filter").setProperty(
								"/selected", "");
						this.elementListFragment.destroy();
					},
					navConBack : function() {
						var navCon = sap.ui.getCore().byId("navCon");
						navCon.to(sap.ui.getCore().byId("parent"), "slide");
						this.elementListFragment.destroy();
					},
					afterOpenFilter : function(evt) {
						var navCon = sap.ui.getCore().byId("navCon");
						if (navCon.getCurrentPage().getId() == "children")
							navCon.to(sap.ui.getCore().byId("parent"), "slide");
						this.getView().getModel("filter").setProperty(
								"/selected", "");
					},

					onSearchFilter : function(oEvt) {
						var aFilters = [];
						var sQuery = oEvt.getSource().getValue();

						if (sQuery && sQuery.length > 0) {

							// var filter = new sap.ui.model.Filter("value",
							// sap.ui.model.FilterOperator.Contains, sQuery);

							// var filter = new
							// sap.ui.model.Filter({path:"value",
							// test:function(val)
							// {
							// var property= val.toString().toUpperCase();
							// return
							// (property.indexOf(sQuery.toString().toUpperCase())>=0)
							// }});

							aFilters.push(this.createFilter(sQuery, "value"));
						}

						// update list binding
						var list = sap.ui.getCore().byId("filterList");

						var binding = list.getBinding("items");
						binding.filter(aFilters);
					},
					createFilter : function(query, property) {
						var filter = new sap.ui.model.Filter({
							path : property,
							test : function(val) {
								var prop = val.toString().toUpperCase();
								return (prop.indexOf(query.toString()
										.toUpperCase()) >= 0)
							}
						});
						return filter;
					},
					onFilterDialogClose : function(evt) {
						if (this.elementListFragment) {
							this.elementListFragment.destroy();
						}
						if (this.filterDialog) {
							this.filterDialog.close();
							this.filterDialog.destroy();
						}
					},
					onFilterDialogOK : function(evt) {
						var filterItems = model.filters.Filter
								.getSelectedItems("orders");
						if (this.elementListFragment)
							this.elementListFragment.destroy();
						this.filterDialog.close();
						this.getView().getModel("filter").setProperty(
								"/selected", "");
						this.handleFilterConfirm(filterItems);
						this.filterDialog.destroy();
						delete (this.filterDialog);
					},
					handleFilterConfirm : function(selectedItems) {
						var filters = [];
						_.forEach(selectedItems, _.bind(function(item) {
							filters.push(this.createFilter(item.value,
									item.property));
						}, this));
						var table = this.getView().byId("approvationsListTable");
						var binding = table.getBinding("items");
						binding.filter(filters);
					},
					onResetFilterPress : function() {
						model.filters.Filter.resetFilter("orders");
						if (this.elementListFragment) {
							this.elementListFragment.destroy();
						}
						if (this.filterDialog) {
							this.filterDialog.close();
							this.filterDialog.destroy();
						}
						var table = this.getView().byId("approvationsListTable");
						var binding = table.getBinding("items");
						binding.filter();
						// sap.m.MessageToast.show("All filters cleared!");
						// //console.log(model.filters.Filter.getSelectedItems("customers"));
					},

					navToCustomerDetails : function(evt) {

						var src = evt.getSource();
						var selectedItem = src.getBindingContext("approvationsList").getObject();



						var parameters ={
							"Bukrs": selectedItem.society,
							"Vkorg":selectedItem.salesOrg,
							"Vtweg":selectedItem.distrCh,
							"Spart":selectedItem.division,
							"Cdage":selectedItem.agentCode
						}

						this.getView().getModel("appStatus").setProperty("/customerRequester", parameters);
						model.persistence.Storage.session.save("customerRequester", parameters);

						var id = selectedItem.customerId;
						this.router.navTo("fullCustomerDetail4Approvation", {
							id : id
						});

					},

					onSearch : function(evt) {
						var src = evt.getSource();
						this.searchValue = src.getValue();
						var searchProperty = this.uiModel
								.getProperty("/searchProperty");

						this.applyFilter(this.searchValue, searchProperty);

					},
					applyFilter : function(value, params) {

						var table = this.getView().byId("approvationsListTable");
						if (!table.getBinding("items").oList
								|| table.getBinding("items").oList.length === 0)
							return;

						var temp = table.getBinding("items").oList[0]; // a
																		// template
																		// just
																		// to
																		// recover
																		// the
																		// data
																		// types
						var filtersArr = [];
						// var props = utils.ObjectUtils.getKeys(temp);

						if (!_.isEmpty(params)) {
							if (!_.isArray(params)) {
								params = [ params ];
							}
							for (var i = 0; i < params.length; i++) {

								switch (typeof (utils.ObjectUtils.getValues(
										temp, params[i]))) {
								case "undefined":
									break;
								case "string":
									filtersArr
											.push(new sap.ui.model.Filter(
													params[i],
													sap.ui.model.FilterOperator.Contains,
													value));
									break;
								case "number":
									filtersArr.push(new sap.ui.model.Filter(
											params[i],
											sap.ui.model.FilterOperator.EQ,
											value));
									break;
								}

							}
							var filter = new sap.ui.model.Filter({
								filters : filtersArr,
								and : false
							});
							table.getBinding("items").filter(filter);
							return;
						}
						table.getBinding("items").filter();
					},






					keyUpFunc : function(e) {
						if (e.keyCode == 27) {
							// codice per il pulsante escape per evitare che lo
							// user chiuda il dialog via ESC


							$(document).off("keyup");
							// this.router.navTo("launchpad");
						}
					},



					populateStatusSelect : function() {

						var defer = Q.defer();
						var workingUser = model.persistence.Storage.session
								.get("workingUser");
						this.orgData = {
							"Bukrs" : workingUser.organizationData.results[0].society,
							"Vkorg" : workingUser.organizationData.results[0].salesOrg,
							"Vtweg" : workingUser.organizationData.results[0].distributionChannel,
							"Spart" : workingUser.organizationData.results[0].division
						};

						var item = {
							"type" : "Stato",
							"namespace" : "orderStatus"
						};
						utils.Collections.getOdataSelect(item.type,
								this.orgData).then(_.bind(function(result) {
							this.getView().setModel(result, item.namespace);
							defer.resolve(result);
						}, this), _.bind(function(err) {
							defer.reject(err);
						}, this))
						return defer.promise;

					},

					onAcceptPress:function(evt)
					{
						var items = this.getView().getModel("approvationsList").getData().results;
						var selectedItems = _.where(items, {selected:true});
						if(selectedItems.length === 0){
							sap.m.MessageToast.show(model.i18n.getText("noItemsSelected"));
							return;
						}
						var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
						sap.m.MessageBox.confirm(
								model.i18n.getText("approveChanges"), {
								styleClass: bCompact? "sapUiSizeCompact" : "",
								actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
								onClose: _.bind(function(oAction) { 
									if(oAction==="YES"){
										//Modified 14/09/16 L.C. 12.03
										//Using approvationList single approve--------------------------------------
										var requester = new model.CustomerDiscountRequester(selectedItems[0].customer);
										requester.updateCustomerDiscount(this.user)
										.then(_.bind(function (res) {
											sap.m.MessageToast.show(model.i18n.getText("discountRequestSendSuccess"));
											var selRow = this.getView().byId("approvationsListTable").getSelectedItem();
											selRow.setBackgroundColor("#b3ffb3");
											this.getView().byId("approvationsListTable").removeSelections();
											selectedItems[0].customerDiscountStatus=requester.status;
											selectedItems[0].customerDiscountStatusDescr=requester.statusDescr;
											this.getView().getModel("approvationsList").refresh();
											
											//this.loadList();
											
										}, this), 
										_.bind(function(err){
											sap.m.MessageToast.show(model.i18n.getText("errorDiscountUpdate"));
											this.getView().byId("approvationsListTable").removeSelections();
										}, this))
										
										
										//------------------Modified from L.C. 07/09/16 11.53-----------------------------------------
										//----------Using an approvationsList function to approve massively selectedItems------------
//										model.collections.ApprovationsList.multiApprove(selectedItems, this.user)
//										.then(_.bind(function (res) {
//											this.getView().getModel("approvationsList").refresh();
//											sap.m.MessageToast.show(model.i18n.getText("multipleModifingSuccess"));
//											this.getView().byId("approvationsListTable").removeSelections();
//											
//										}, this), 
//										_.bind(function(err){
//											sap.m.MessageToast.show(model.i18n.getText("multipleModifingError"));
//											this.getView().byId("approvationsListTable").removeSelections();
//										}, this))
										
										//-------------Before--------------------------------------------------------------------------
//										for(var i= 0; selectedItems && selectedItems.length> 0 && i < selectedItems.length; i++)
//										{
											
											
//											this.selectedDiscount = new model.CustomerDiscount();
//											this.selectedDiscount.initialize(selectedItems[i].customerDiscountValue, selectedItems[i].customerDiscountStatus);
//											this.selectedDiscount.setDiscount(this.user);
//											selectedItems[i].customerDiscountStatus=this.selectedDiscount.getDiscountStatus();
//											selectedItems[i].customerDiscountStatusDescr=this.selectedDiscount.getDiscountStatusDescr();
//											delete(this.selectedDiscount);
				
											
//										}
										
//										this.getView().getModel("approvationsList").refresh();
//										sap.m.MessageToast.show(model.i18n.getText("multipleModifingSuccess"));
//										this.getView().byId("approvationsListTable").removeSelections();
//-----------------------------------------------------------------------------------------------------------
									}else{
										return;
									}
								}, this)
							}
						);
					},
					initializeCustomerDialog : function()
					{
						if(!this.customerDiscountDialog)
						    {
						      this.customerDiscountDialog = sap.ui.xmlfragment("view.dialog.CustomerDiscountDialog", this);
						    }
						    var page = this.getView().byId("approvationsListPageId");

						    page.addDependent(this.customerDiscountDialog);
					}
//					onRejectPress:function(evt)
//					{
//						var items = this.getView().getModel("approvationsList").getData().results;
//						var selectedItems = _.where(items, {selected:true});
//						for(var i= 0; selectedItems && selectedItems.length> 0 && i < selectedItems.length; i++)
//						{
//							selectedItems[i].customerDiscountStatus="R";
//							selectedItems[i].customerDiscountStatusDescr="Respinto"
//						}
//						this.getView().getModel("approvationsList").refresh();
//						this.getView().byId("approvationsListTable").removeSelections();
//					}



				});
