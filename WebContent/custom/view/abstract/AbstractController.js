jQuery.sap.require("utils.Validator");
jQuery.sap.require("utils.ObjectUtils")
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("model.persistence.PersistenceManager");
jQuery.sap.declare("view.abstract.AbstractController");
sap.ui.core.mvc.Controller.extend("view.abstract.AbstractController", {
    onInit: function () {
        //    //** risetto il css
        //    var workingUserFromSession = model.persistence.Storage.session.get("workingUser");
        //    if (workingUserFromSession) {
        //      var division = workingUserFromSession.organizationData.results[0].division;
        //      jQuery.sap.includeStyleSheet("custom/css/custom" + division + ".css", "custom_style");
        //    }
        //    //**
        this.cssReload();
        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
        this.uiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.uiModel, "ui");
        var userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(userModel, "user");
        var wrkUsrModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(wrkUsrModel, "wo");
        this.setDataLocation();
    }
    , handleRouteMatched: function (evt) {
        this.clearValueState();
        var user = model.persistence.Storage.session.get("user");
        this.getView().getModel("user").setData(user);
        if (model.persistence.Storage.session.get("workingUser")) {
            var wrkUsr = model.persistence.Storage.session.get("workingUser");
            this.getView().getModel("wo").setData(wrkUsr);
        }
        //var user = model.persistence.Storage.session.get("user");
        var user = model.persistence.Storage.session.get("workingUser");
        var customer = model.persistence.Storage.session.get("currentCustomer");
        if (user) {
            this.uiModel.setProperty("/wrCust", user.organizationData.results[0].canEditCustomers);
            this.uiModel.setProperty("/wrOrd", user.organizationData.results[0].canEditOrders);
            this.uiModel.setProperty("/wrListValidDate", user.organizationData.results[0].canEditListValidDate)
        }
        if (customer) {
        	var cr = customer.registry;
        	//var isNewTradePolicies = (cr.division == "RI" && cr.distrCh == "DO" && cr.salesOrg == "SA20" && cr.society == "SI01");
        	var isNewTradePolicies = user ? user.organizationData.results[0].canEditQuotation : (cr.division == "RI" && cr.distrCh == "DO" && cr.salesOrg == "SA20" && cr.society == "SI01");
        	var isApproved = !isNewTradePolicies || (customer.discountStatus == "APP" || customer.discountStatus == "MPP");
            this.uiModel.setProperty("/wrOrd", !customer.isBlocked && isApproved);
        }
        var deviceType = "Desktop";
        if (sap.ui.Device.system.tablet) {
            deviceType = "Tablet";
        }
        else if (sap.ui.Device.system.phone) {
            deviceType = "Phone";
        }
        this.getView().getModel("ui").setProperty("/deviceType", deviceType);
    }
    , onHomePress: function (evt) {
        if (model.Current.getCustomer()) {
            model.Current.removeOrder();
        }
        //     var checkSavedOrderExistence = model.persistence.PersistenceManager.checkSavedData("localUserDataBackup","orderBackup");
        //     if(checkSavedOrderExistence){
        //        model.persistence.PersistenceManager.flushUserSavedData("localUserDataBackup");
        //        model.persistence.PersistenceManager.flushModel("orderBackup");
        //     }
        if (window.backupInterval) {
            var appModel = this.getView().getModel("appStatus");
            appModel.setProperty("/intervalIsRunning", false);
            clearInterval(window.backupInterval);
        }
        if (model.persistence.Storage.session.get("currentProduct")) {
            model.persistence.Storage.session.remove("currentProduct");
        }
        if (this.getView().getModel("appStatus").getProperty("/customerRequester")) {
            this.getView().getModel("appStatus").setProperty("/customerRequester", "");
        }
        if (model.persistence.Storage.session.get("customerRequester")) {
            model.persistence.Storage.session.remove("customerRequester");
        }
        this.router.navTo("launchpad");
        //    if (model.persistence.Storage.session.get("society")) {
        //      var society = model.persistence.Storage.session.get("society");
        //    }
        //    var appModel = this.getView().getModel("appStatus");
        //    appModel.setProperty("/navBackVisible", false);
        //    var workingUser = model.persistence.Storage.session.get("workingUser");
        //    var division = workingUser.organizationData.results[0].division;
        //    this.router.navTo("launchpad", {
        //      division: division
        //    });
    }
    , onAfterRendering: function () {
        this.setElementsToValidate();
        this.clearValueState();
        $(document).off("keypress");
    }, //---------------------Functions to check input field--------------------------
    _checkingValues: []
    , _checkingAltDests: []
    , _checkingGru: []
    , setElementsToValidate: function (params) {
        this._checkingValues = [];
        var inputsId = _.chain($('input')).map(function (item) {
            var innerIndex = item.id.indexOf("-inner");
            if (innerIndex) return item.id.substring(0, innerIndex);
            return item.id;
        }).value();
        var controls = _.map(inputsId, function (item) {
            return sap.ui.getCore().byId(item);
        });
        // //console.log("controls" +controls);
        //**
        if (params === "altDest") {
            var cont = _.compact(controls);
            var array = [];
            for (var i = 0; i < cont.length; i++) {
                if (cont[i].data().reqAltDest) {
                    array.push(cont[i]);
                }
            }
            this._checkingAltDests = array;
            for (var i = 0; i < this._checkingAltDests.length; i++) {
                this._checkingAltDests[i].attachChange(null, this.checkInputField, this);
            }
        }
        else
        if (params === "gru") {
            var cont = _.compact(controls);
            var array = [];
            for (var i = 0; i < cont.length; i++) {
                if (cont[i].data().reqGru) {
                    array.push(cont[i]);
                }
            }
            this._checkingGru = array;
            for (var i = 0; i < this._checkingGru.length; i++) {
                this._checkingGru[i].attachChange(null, this.checkInputField, this);
            }
        }
        else {
            //**
            this._checkingValues = _.compact(controls);
            for (var i = 0; i < this._checkingValues.length; i++) {
                this._checkingValues[i].attachChange(null, this.checkInputField, this);
            }
        }
    }
    , clearValueState: function () {
        if (this._checkingValues) {
            for (var i = 0; i < this._checkingValues.length; i++) {
                this._checkingValues[i].setValueState("None");
            }
        }
    }
    , validateCheck: function () {
        var result = true;
        if (!this._checkingValues || this._checkingValues.length === 0) return true;
        for (var i = 0; i < this._checkingValues.length; i++) {
            if (!this.checkInputField(this._checkingValues[i])) {
                if (this._checkingValues[i].data("req") === "true") {
                    result = false;
                }
            }
        }
        return result;
    }
    , checkInputField: function (evt) {
        var control = evt.getSource ? evt.getSource() : evt;
        var infoControl = control.data("req");
        var typeControl = control.data("input");
        var correct = true;
        if (infoControl === "true" || infoControl === "changed" || control.getValue() !== "") {
            switch (typeControl) {
            case "createCustomer": //It's specific of customerCreate
                if (!control.getVisible()) //If invisible is a not required field so it's ok
                {
                    correct = true;
                    break;
                }
                if (infoControl === "changed") correct = true;
                else correct = utils.Validator.required(control);
                var panel = this.getPanel(control);
                if (!correct) {
                    if (!this._createCustomerErrValues) {
                        this._createCustomerErrValues = [];
                    }
                    var err = {
                        "control": control
                        , "panel": panel
                    };
                    this._createCustomerErrValues.push(err);
                    this.setErrorInPanel(panel, true);
                }
                else {
                    if (this._createCustomerErrValues && this._createCustomerErrValues.length > 0) {
                        _.remove(this._createCustomerErrValues, {
                            control: control
                            , panel: panel
                        });
                        if (!_.find(this._createCustomerErrValues, {
                                panel: panel
                            })) {
                            this.setErrorInPanel(panel, false);
                        }
                    }
                }
                break;
            default:
                correct = utils.Validator.required(control);
                break;
            }
        }
        // switch(infoControl)
        // {
        //   "true":
        //     correct = utils.Validator.required(control);
        //     break;
        //   "edit":
        //     var type= control.data("")
        //
        //   default:
        //     // correct = utils.Validator.required(control);
        //     break;
        //
        // }
        return correct;
    }
    , getFailedControls: function () {
        var result = [];
        _.forEach(this._checkingValues, _.bind(function (item) {
            if (!this.checkInputField(item)) {
                result.push(item);
            }
        }, this));
        return result;
    }, //--------------------------------------------------------------------------------------
    onSearch: function (evt) {
        var src = evt.getSource();
        this.searchValue = src.getValue();
        var searchProperty = this.uiModel.getProperty("/searchProperty");
        this.applyFilter(this.searchValue, searchProperty);
    }
    , applyFilter: function (value, params) {
        var list = this.getView().byId("list");
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0) return;
        var temp = list.getBinding("items").oList[0];
        var filter = this.assignFilters(temp, value, params);
        // a template just to recover the data types
        // var filtersArr = [];
        // // var props = utils.ObjectUtils.getKeys(temp);
        //
        // if (!_.isEmpty(params)) {
        //   if (!_.isArray(params)) {
        //     params = [params];
        //   }
        //   for (var i = 0; i < params.length; i++) {
        //
        //
        //     switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
        //     case "undefined":
        //       break;
        //     case "string":
        //       filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
        //       break;
        //     case "number":
        //       filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
        //       break;
        //     }
        //
        //   }
        //   var filter = new sap.ui.model.Filter({
        //     filters: filtersArr,
        //     and: false
        //   });
        list.getBinding("items").filter(filter);
    }
    , assignFilters: function (temp, value, params) {
        var filtersArr = [];
        // var props = utils.ObjectUtils.getKeys(temp);
        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {
                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                case "undefined":
                    break;
                case "string":
                    filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                    break;
                case "number":
                    filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                    break;
                }
            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr
                , and: false
            });
            return filter;
        }
        return filtersArr;
    }
    , onLogoutPress: function (oEvent) {
        if (model.Current && model.Current.getCustomer()) {
            model.Current.removeOrder();
        }
        //    var checkSavedOrderExistence = model.persistence.PersistenceManager.checkSavedData("localUserDataBackup","orderBackup");
        //    if(checkSavedOrderExistence){
        //        model.persistence.PersistenceManager.flushUserSavedData("localUserDataBackup");
        //        model.persistence.PersistenceManager.flushModel("orderBackup");
        //    }
        if (window.backupInterval) {
            var appModel = this.getView().getModel("appStatus");
            appModel.setProperty("/intervalIsRunning", false);
            clearInterval(window.backupInterval);
        }
        this.doLogout();
    }
    , doLogout: function () {
        //resetto dati
        $.ajax({
            type: "GET", //url: icms.Component.getMetadata().getConfig().settings.logoffUrl //Clear SSO cookies: SAP Provided service to do that
            url: sessionStorage.getItem("logoffUrl")
        });
        //console.log("Completely logged off.");
        sessionStorage.clear();
        this.setDataLocation();
        this.router.navTo("login");
    }, //funzione back da rivedere
    onBackPress: function (evt) {
        var route = location.hash;
        if (route.indexOf("edit") > 0) {
            this.router.navTo("customerDetail", {
                id: this.customer.getId()
            });
        }
        else {
            this.router.navTo("launchpad");
            //      if (model.persistence.Storage.session.get("division")) {
            //        var division = model.persistence.Storage.session.get("division");
            //      }
            //      this.router.navTo("launchpad", {
            //        division: division
            //      });
        }
    }, //------------------------User Functions------------------------------------------
    getUserInfo: function () {
        var selectedOrg = model.persistence.Storage.session.get("workingUser");
        var u = {
            "username": selectedOrg.organizationData.results[0].username
            , "userType": selectedOrg.type
            , "society": selectedOrg.organizationData.results[0].society
            , "salesOrg": selectedOrg.organizationData.results[0].salesOrg
            , "distrCh": selectedOrg.organizationData.results[0].distributionChannel
            , "division": selectedOrg.organizationData.results[0].division
            , "areaManager": selectedOrg.organizationData.results[0].areaManager
            , "territoryManager": selectedOrg.organizationData.results[0].territoryManager
            , "agentCode": selectedOrg.userGeneralData.customer
        }
        return u;
    }
    , getCustomerInfo: function () {
        var customer = model.persistence.Storage.session.get("currentCustomer");
        var c = {
            "society": customer.registry.society
            , "salesOrg": customer.registry.salesOrg
            , "distrCh": customer.registry.distrCh
            , "division": customer.registry.division
            , "areaManager": customer.registry.areaManager
            , "territoryManager": customer.registry.territoryManager
            , "agentCode": customer.registry.agentCode
            , "customerId": customer.registry.id
        }
        return c;
    }
    , getOrgDataForSAP: function () {
        var selectedOrg = model.persistence.Storage.session.get("workingUser");
        var u = {
            "Bukrs": selectedOrg.organizationData.results[0].society
            , "Vkorg": selectedOrg.organizationData.results[0].salesOrg
            , "Vtweg": selectedOrg.organizationData.results[0].distributionChannel
            , "Spart": selectedOrg.organizationData.results[0].division
        , }
        return u;
    }
    , navToLaunchpad: function (user) {
        var society = model.persistence.Storage.session.get("society");
        if (!society && user.getUserSocieties().length === 1) {
            society = user.getUserSocieties()[0];
            model.persistence.Storage.session.save("society", society);
        }
        if (society) {
            jQuery.sap.includeStyleSheet("custom/css/custom" + society + ".css", "custom_style");
            //lancio l'app
            this.launchApp("launchpad", society);
        }
        else {
            this.launchApp("soLaunchpad");
        }
    }
    , launchApp: function (url, society) {
        if (url === "launchpad") {
            if (society) this.router.navTo(url, {
                society: society
            });
        }
        else {
            this.router.navTo(url);
        }
    }
    , cssReload: function () {
        //** risetto il css
        var division = model.persistence.Storage.session.get("division");
        if (division) {
            //var division = workingUserFromSession.organizationData.results[0].division;
            jQuery.sap.includeStyleSheet("custom/css/custom" + division + ".css", "custom_style");
        }
        //**
    }
    , setDataLocation: function () {
        var origin = location.origin;
        if ((navigator.appName).indexOf("Microsoft") > -1) {
            origin = location.host; //ie10 e minori
        }
        var path = location.pathname;
        var servers = icms.Component.getMetadata().getConfig().settings.servers;
        var actualPointer = icms.Component.getMetadata().getConfig().settings.serverUrl;
        //---------------Function to apply----------------------------------------------
        for (var i = 0; i < servers.length; i++) {
            if (servers[i].serverUrl.indexOf(origin) >= 0) {
                actualPointer = servers[i].serverUrl;
                sessionStorage.setItem("serverUrl", servers[i].serverUrl);
                sessionStorage.setItem("logoffUrl", servers[i].logoffUrl);
                sessionStorage.setItem("path", servers[i].path);
                sessionStorage.setItem("serverId", servers[i].id);
                break;
            }
        }
        //-----------------------------------------------------------------------------------
        //-----------------Function To Test in local ----------------------------------------
        // if(path.indexOf("Test")>0)
        // {
        //   sessionStorage.setItem("serverUrl", servers[3].serverUrl);
        //   sessionStorage.setItem("logoffUrl", servers[3].logoffUrl);
        //
        // }
        // else if(path.indexOf("C:"))
        // {
        //   sessionStorage.setItem("serverUrl", servers[1].serverUrl);
        //   sessionStorage.setItem("logoffUrl", servers[1].logoffUrl);
        //
        // }
        //---------------------------------------------------------------------------------------
        if (!sessionStorage.getItem("serverUrl")) {
            sessionStorage.setItem("serverUrl", icms.Component.getMetadata().getConfig().settings.serverUrl);
            var newPath = actualPointer.substring(0, (actualPointer.indexOf("/sap/")) + 1);
            sessionStorage.setItem("path", newPath);
        }
        if (!sessionStorage.getItem("logoffUrl")) {
            sessionStorage.setItem("logoffUrl", icms.Component.getMetadata().getConfig().settings.logoffUrl);
        }
        if (!sessionStorage.getItem("serverId")) {
            sessionStorage.setItem("serverId", icms.Component.getMetadata().getConfig().settings.id);
        }
        //-------------------Maybe  it will be useless---------------------------------------------------------------
        var serverModel = new sap.ui.model.json.JSONModel();
        var server = {
            "serverUrl": sessionStorage.getItem("serverUrl")
            , "logoffUrl": sessionStorage.getItem("logoffUrl")
            , "path": sessionStorage.getItem("path")
            , "serverId": sessionStorage.getItem("serverId")
        };
        serverModel.setData(server);
        this.getView().setModel(serverModel, "server");
        //-------------------------------------------------------------------------------------------
    }, //------------------is Enabled to V2??---------------------------------------------------
    checkQuotationEnabledToTradePolicies: function (wu) {
            var isEnabled = false;
            if (!wu) {
                var wu = model.persistence.Storage.session.get("workingUser");
            }
            var orgDataOfWu = wu.organizationData.results[0];
            isEnabled = (orgDataOfWu.canEditQuotation);
//            if (orgDataOfWu.distributionChannel === "DO" && orgDataOfWu.division === "RI" && orgDataOfWu.salesOrg === "SA20" && orgDataOfWu.society === "SI01") {
//                isEnabled = true;
//            }
            
            model.persistence.Storage.session.save("isEnabledToQuotationTradePolicies", orgDataOfWu.canEditQuotation);
            return isEnabled;
        },
        //---------------------------------------------------------------------------------------
    onTransformToOrder: function (offer) {
        var def = Q.defer();
        fSuccess = function (res) {
            sap.m.MessageToast.show(model.i18n.getText("multipleModifingSuccess") + " : "+ res.Vbeln);
//            var offer = _.find(this.getView().getModel("offerList").getData().results, {"orderId" : res.Vbeln});
//            offer.orderStatus = "CONVERTITO";
//            offer.orderStatusDescr = model.i18n.getText("converted");
//            this.getView().getModel("offerList").refresh();
            def.resolve(res);
        };
        fError = function (err) {
            sap.m.MessageToast.show("ERR: " + utils.Message.getError(err));
            def.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        var offerModel = new model.Offer();
        offerModel.transformToOrder(offer).then(fSuccess, fError);
        
        return def.promise;
    },
});