jQuery.sap.require("utils.Validator");
jQuery.sap.require("utils.ObjectUtils")
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.Current");
jQuery.sap.declare("view.abstract.AbstractMasterController");


view.abstract.AbstractController.extend("view.abstract.AbstractMasterController", {





  getFiltersOnList : function(evt)
  {
    var list = this.getView().byId("list");
    if(!list || !list.getBinding("items"))
      return null;

    return list.getBinding("items").aFilters;
  },

  removeFiltersOnList:function(evt)
  {
    var list = this.getView().byId("list");
    if(!list || !list.getBinding("items"))
      return false;

    list.getBinding("items").aFilters = [];
  }








});
