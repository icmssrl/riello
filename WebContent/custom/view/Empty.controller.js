jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Orders");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Order");
jQuery.sap.require("view.abstract.AbstractController");


view.abstract.AbstractController.extend("view.Empty", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		
		var name = evt.getParameter("name");

		if ( name !== "empty" ){
			return;
		}

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		this.customer = model.Current.getCustomer();
		if(!this.customer)
		{
			customerData = model.persistence.Storage.session.get("currentCustomer");
			this.customer = new model.Customer(customerData);
		}

		this.getView().setModel(this.customer.getModel(), "customer");




	},

});
