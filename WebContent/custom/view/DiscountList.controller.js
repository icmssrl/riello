jQuery.sap.require("sap.m.MessageBox");
//jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.CustomerRequester");
jQuery.sap.require("utils.Message");

view.abstract.AbstractMasterController.extend("view.CustomersList", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
		//YET TO ADAPT TO LISTED ELEMENTS ->IN refreshList

		
	},


	handleRouteMatched: function (evt) {

		this.uiModel.setProperty("/searchValue", "");


		var name = evt.getParameter("name");

//    if(sap.ui.Device.system.tablet && sap.ui.Device.orientation.portrait)
//    {
//      sap.ui.getCore().byId("splitApp-Master").setVisible(true);
//    }

		// if ( name !== "customersList" && name !== "customerDetail" && name != "customerEdit" && name!=="emptyCustomer"){
			if ( name !== "discountList" && name !=="emptyDiscount"){
			return;
		}

		





	},

	
	onFilterPress:function()
	{
		this.filterModel = model.filters.Filter.getModel(this.customersModel.getData().customers, "customers");
		this.getView().setModel(this.filterModel, "filter");
		var page = this.getView().byId("discountsListPageId");
		this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
		page.addDependent(this.filterDialog);
		this.filterDialog.open();

	},
	onFilterDialogClose:function()
	{
		this.filterDialog.close();
	},

	onFilterPropertyPress:function(evt)
	{

		var parentPage = sap.ui.getCore().byId("parent");
		var elementPage = sap.ui.getCore().byId("children");
		//console.log(this.getView().getModel("filter").getData().toString());
		var navCon = sap.ui.getCore().byId("navCon");
		var selectedProp = 	evt.getSource().getBindingContext("filter").getObject();
		this.getView().getModel("filter").setProperty("/selected", selectedProp);
		this.elementListFragment = sap.ui.xmlfragment("view.fragment.filterList", this);
		elementPage.addContent(this.elementListFragment);

		navCon.to(elementPage, "slide");
		this.getView().getModel("filter").refresh();
	},

	onBackFilterPress:function(evt)
	{
		// this.addSelectedFilterItem();
		this.navConBack();
		this.getView().getModel("filter").setProperty("/selected", "");
		this.elementListFragment.destroy();
	},
	navConBack:function()
	{
		var navCon = sap.ui.getCore().byId("navCon");
		navCon.to(sap.ui.getCore().byId("parent"), "slide");
		this.elementListFragment.destroy();
	},
	afterOpenFilter:function(evt)
	{
		var navCon = sap.ui.getCore().byId("navCon");
		if(navCon.getCurrentPage().getId()== "children")
			navCon.to(sap.ui.getCore().byId("parent"), "slide");
		this.getView().getModel("filter").setProperty("/selected", "");
	},

	onSearchFilter:function(oEvt)
	{
			var aFilters = [];
			var sQuery = oEvt.getSource().getValue();

			if (sQuery && sQuery.length > 0) {

					// var filter = new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.Contains, sQuery);

				// 	var filter = new sap.ui.model.Filter({path:"value", test:function(val)
				// {
				// 	var property= val.toString().toUpperCase();
				// 	return (property.indexOf(sQuery.toString().toUpperCase())>=0)
				// }});

					aFilters.push(this.createFilter(sQuery, "value"));
				}

			// update list binding
			var list = sap.ui.getCore().byId("filterList");
			var binding = list.getBinding("items");
			binding.filter(aFilters);
	},
	createFilter:function(query, property)
	{
		var filter = new sap.ui.model.Filter({path:property, test:function(val)
		{
			var prop= val.toString().toUpperCase();
			return (prop.indexOf(query.toString().toUpperCase())>=0)
		}});
		return filter;
	},
	onFilterDialogClose:function(evt)
	{
		if (this.elementListFragment) {this.elementListFragment.destroy();}
		if (this.filterDialog) {
			this.filterDialog.close();
			this.filterDialog.destroy();
		}
	},
	onFilterDialogOK:function(evt)
	{
		var filterItems = model.filters.Filter.getSelectedItems("discounts");
		if(this.elementListFragment)
			this.elementListFragment.destroy();
		this.filterDialog.close();
		this.getView().getModel("filter").setProperty("/selected", "");
		this.handleFilterConfirm(filterItems);
		this.filterDialog.destroy();
		delete(this.filterDialog);
	},
	handleFilterConfirm: function(selectedItems)
	{
		var filters = [];
		_.forEach(selectedItems, _.bind(function(item)
	{
		filters.push(this.createFilter(item.value, item.property));
	},
	this));
		var list = this.getView().byId("list");
		var binding = list.getBinding("items");
		binding.filter(filters);
	},
	onResetFilterPress: function()
	{
		model.filters.Filter.resetFilter("discounts");
		// //console.log(model.filters.Filter.getSelectedItems("customers"));
	}








});
