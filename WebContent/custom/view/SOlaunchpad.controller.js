jQuery.sap.require("model.Tiles");
jQuery.sap.require("model.ui.LogoTile");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.i18n");

//jQuery.sap.require("model.User");

view.abstract.AbstractController.extend("view.SOlaunchpad", {

  onInit: function () {
    this.router = sap.ui.core.UIComponent.getRouterFor(this);
    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    // view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.tileModel = new sap.ui.model.json.JSONModel();

    this.getView().setModel(this.tileModel, "tiles");

    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");
    //** risetto il css 
    this.cssReload();
    //**

  },

//  onAfterRendering: function(evt){
//    this.user = model.persistence.Storage.session.get("user");
//
//    this.userModel.setData(this.user);
//    this.refreshTiles(this.user);
//
//
//  },

  handleRouteMatched: function (oEvent) {


    var oParameters = oEvent.getParameters();

    if (oParameters.name !== "soLaunchpad") {
      return;
    }
    
//    if(model.persistence.Storage.session.get("society")){
//        model.persistence.Storage.session.remove("society");
//    }
      
    if(model.persistence.Storage.session.get("division")){
        model.persistence.Storage.session.remove("division");
    }

    var appModel = this.getView().getModel("appStatus");
      if(appModel.getProperty("/navBackVisible")===true){
          appModel.setProperty("/navBackVisible", false);
      }
    this.user = model.persistence.Storage.session.get("user");


      this.userModel.setData(this.user);
      this.refreshTiles(this.user);
  },

 

//  onTilePress: function (oEvent) {
//    //recupero la tile
//    var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
//    model.persistence.Storage.session.save("society", tilePressed.society);
//    var companies = _.where(this.user.organizationData.results, {'society': tilePressed.society});
//    var appModel = this.getView().getModel("appStatus");
//        appModel.setProperty("/navBackVisible", true);
//    var cloneUser = _.cloneDeep(this.user);
//    if(cloneUser.organizationData.results.length>0){
//        cloneUser.organizationData.results = [];
//    }
//    for(var i = 0; i<companies.length; i++){
//        cloneUser.organizationData.results.push(companies[i]);
//    }    
//    model.persistence.Storage.session.save("workingUser", cloneUser);
//      
//    if (_.chain(companies).pluck('division').uniq().value().length === 1){
//        var division = companies[0].division;
//        model.persistence.Storage.session.save("division", division);
//        this.launchApp("launchpad", division);
//    }else{
//        this.launchApp("divisionLaunchpad", tilePressed.society);
//    } 
//    
//    
//  },
    
   onTilePress: function (oEvent) {
    //recupero la tile
    var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
    
   if(tilePressed.division === "CF" ){
    var companies = _.where(this.user.organizationData.results, {'society': "SI41"});
    var cloneUser = _.cloneDeep(this.user);
    if(cloneUser.organizationData.results.length>0){
        cloneUser.organizationData.results = [];
    }
    for(var i = 0; i<companies.length; i++){
        cloneUser.organizationData.results.push(companies[i]);
    } 
    
   // if(companies.length == 1)
    	model.persistence.Storage.session.save("workingUser", cloneUser);
    
   }else{
      
       var company = _.find(this.user.organizationData.results, {division : tilePressed.division})
        var cloneUser = _.cloneDeep(this.user);
        if(cloneUser.organizationData.results.length>0){
            cloneUser.organizationData.results = [];
        }
//        for(var i = 0; i<company.length; i++){
            cloneUser.organizationData.results.push(company);
//        }    
        model.persistence.Storage.session.save("workingUser", cloneUser);
        
   }
    model.persistence.Storage.session.save("division", tilePressed.division);
//    jQuery.sap.includeStyleSheet("custom/css/custom"+tilePressed.division+".css","custom_style");
//    var appModel = this.getView().getModel("appStatus");
//        appModel.setProperty("/navBackVisible", true);
    
      

    this.launchApp("launchpad", tilePressed.division);
   
    
    
  },

  onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    },

  launchApp: function (url, division) {
//      var appModel = this.getView().getModel("appStatus");
//      if(appModel.getProperty("/navBackVisible")===false){
//          appModel.setProperty("/navBackVisible", true);
//      }
      if(url === "launchpad"){
         this.router.navTo(url);
         jQuery.sap.includeStyleSheet("custom/css/custom"+division+".css","custom_style");
      
     }
  },

  //**
  refreshTiles: function(user)
  {
//    var companies = _.chain(this.user.organizationData.results).pluck('division').uniq().value();
//    var tiles = [];
//    for(var i = 0; i<companies.length; i++)
//    {
//      tiles.push(model.Tiles.getDivisionTile(companies[i]));
//    }
    
    //** se è presente come società FIT e solo quella, allora mostro solo quella tile.
    var companies = [];
    var society = _.chain(this.user.organizationData.results).pluck('society').uniq().value();
    if(society.length===1 && society[0]==="SI41")
    {
      companies.push("CF");
    }
    else
    {
      companies = _.chain(this.user.organizationData.results).pluck('division').uniq().value();
    }
    var tiles = [];
    for(var i = 0; i<companies.length; i++)
    {
      tiles.push(model.Tiles.getDivisionTile(companies[i]));
    }
    //**
    
    this.tileModel.setData(tiles);
    // var session = sessionStorage.getItem("customerSession") ? model.persistence.Storage.session.get("customerSession") : false;
    // var tile = model.Tiles.getMenu(user.type, session);
    // this.tileModel.setData(tile);
    // this.tileModel
  }
});



//jQuery.sap.require("model.Tiles");
//jQuery.sap.require("model.ui.LogoTile");
//jQuery.sap.require("view.abstract.AbstractController");
//jQuery.sap.require("model.persistence.Storage");
//jQuery.sap.require("model.i18n");
//
////jQuery.sap.require("model.User");
//
//view.abstract.AbstractController.extend("view.SOlaunchpad", {
//
//  onInit: function () {
//    this.router = sap.ui.core.UIComponent.getRouterFor(this);
//    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
//    // view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
//    this.tileModel = new sap.ui.model.json.JSONModel();
//
//    this.getView().setModel(this.tileModel, "tiles");
//
//    this.userModel = new sap.ui.model.json.JSONModel();
//    this.getView().setModel(this.userModel, "userModel");
//
//
//  },
//
//  onAfterRendering: function(evt){
//    this.user = model.persistence.Storage.session.get("user");
//
//    this.userModel.setData(this.user);
//    this.refreshTiles(this.user);
//  },
//
//  handleRouteMatched: function (oEvent) {
//
//
//    var oParameters = oEvent.getParameters();
//
//    if (oParameters.name !== "soLaunchpad") {
//      return;
//    }
////    this.user = model.persistence.Storage.session.get("user");
////
////    this.userModel.setData(this.user);
////    this.refreshTiles(this.user);
//  },
//
//  // onLogoutPress: function (oEvent) {
//  //   this.doLogout();
//  // },
//  //
//  // doLogout: function () {
//  //   //resetto dati
//  //   sessionStorage.clear();
//  //   this.router.navTo("login");
//  // },
//
//  onTilePress: function (oEvent) {
//    //recupero la tile
//    var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
//    var url = tilePressed.url;
//    model.persistence.Storage.session.save("division", tilePressed.division);
//
//    //lancio l'app
//    this.launchApp(url);
//  },
//
//  onLinkToUserInfoPress: function(evt){
//      this.router.navTo("changePassword");
//    },
//
//  launchApp: function (url) {
//    this.router.navTo(url);
//  },
//
//  //**
//  refreshTiles: function(user)
//  {
//    var divisions = _.chain(this.user.organizationData).pluck('division').uniq().value();
//    var tiles = [];
//    for(var i = 0; i<divisions.length; i++)
//    {
//      tiles.push(model.Tiles.getDivisionTile(divisions[i]));
//    }
//    this.tileModel.setData(tiles);
//    // var session = sessionStorage.getItem("customerSession") ? model.persistence.Storage.session.get("customerSession") : false;
//    // var tile = model.Tiles.getMenu(user.type, session);
//    // this.tileModel.setData(tile);
//    // this.tileModel.refresh();
//  }
//});
