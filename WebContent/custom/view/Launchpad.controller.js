jQuery.sap.require("model.Tiles");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.i18n");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("model.persistence.PersistenceManager");
jQuery.sap.require("model.Current");

view.abstract.AbstractController.extend("view.Launchpad", {

  onInit: function () {
    this.router = sap.ui.core.UIComponent.getRouterFor(this);
    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    //view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.tileModel = new sap.ui.model.json.JSONModel();

    this.getView().setModel(this.tileModel, "tiles");

    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");

    //Remove customerRequest ->session used to choose agent
    //** risetto il css
    this.cssReload(); //in abstract
    //**


  },

  onExit: function () {
    if (this.dataOrgDialog) {
      //    				this.dataOrgDialog.destroy();
    }
  },

  handleRouteMatched: function (oEvent) {


    var oParameters = oEvent.getParameters();

    //    var selectedDivision = model.persistence.Storage.session.get("division");
    var selectedDivision = model.persistence.Storage.session.get("division");
    //jQuery.sap.includeStyleSheet("custom/css/custom"+selectedCompany+division+".css","custom_style");

    if (oParameters.name !== "launchpad") {
      return;
    }

    this.user = model.persistence.Storage.session.get("user");

    this.workingUser = model.persistence.Storage.session.get("workingUser");


    //model.persistence.Storage.session.remove("customerRequester");

    this.appModel = this.getView().getModel("appStatus");


    if (this.user.organizationData.results.length === 1) {
      this.appModel.setProperty("/navBackVisible", false);
    } else {
      if (sessionStorage.getItem("customerSession")) {
        this.appModel.setProperty("/navBackVisible", false);
      } else {
        this.appModel.setProperty("/navBackVisible", true);
      }
    }

//    if (this.dataOrgDialog) {
//      this.dataOrgDialog.destroy();
//    }


    if (selectedDivision === "CF") {
      var companies = _.where(this.user.organizationData.results, {
        'society': "SI41"
      });
      if (companies.length === 1) {
        this.appModel.setProperty("/changeWorkingSetButtonVisible", false);
        var cloneUser = _.cloneDeep(this.user);
        if (cloneUser.organizationData.results.length > 0) {
          cloneUser.organizationData.results = [];
        }
        cloneUser.organizationData.results.push(companies[0]);
        model.persistence.Storage.session.save("workingUser", cloneUser);
      } else {
        this.appModel.setProperty("/changeWorkingSetButtonVisible", true);
      }

    } else {
      this.appModel.setProperty("/changeWorkingSetButtonVisible", false);
    }



    if (this.workingUser) {
      this.userModel.setData(this.workingUser);
      this.refreshTiles(this.workingUser);
    }

    if (!sessionStorage.getItem("customerSession") && this.workingUser.organizationData.results.length > 1) {
      this.dataOrgModel = new sap.ui.model.json.JSONModel();
      this.dataOrgModel.setData({
        results: companies
      });
      this.getView().setModel(this.dataOrgModel, "dataOrg");

      if(!this.dataOrgDialog)
    	  this.dataOrgDialog = sap.ui.xmlfragment("view.dialog.selectOrganizationData", this);

      var page = this.getView().byId("launchpadPageId");
      page.addDependent(this.dataOrgDialog);
      this.dataOrgDialog.open();

      //**
      var list = sap.ui.getCore().byId("dataOrgSelectListId");
      if (!list.getSelectedItem()) {
        list.setSelectedItem(list.getItems()[0]);
      }
      //**
      // $(document).keyup(_.bind(this.keyUpFunc, this));

    }

    if (sessionStorage.getItem("customerSession")) {
      this.appModel.setProperty("/changeWorkingSetButtonVisible", false);
    }

    this.cleanSession();//Function to call to clean session values


  },

  cleanSession :function(evt)
  {
    if(model.persistence.Storage.session.get("OrderType"))
      model.persistence.Storage.session.remove("OrderType");

    if(model.persistence.Storage.session.get("sourceList"))
      model.persistence.Storage.session.remove("sourceList")
  },
  keyUpFunc: function (e) {
    if (e.keyCode == 27) {
      // codice per il pulsante escape per evitare che lo user chiuda il dialog via ESC
      sap.m.MessageToast.show(model.i18n.getText("PLEASE_MAKE_A_CHOICE"));
      //                if(this.dataOrgDialog){
      //                              this.dataOrgDialog.destroy();
      //                      }
    }
    //                      if(this.workingUser)
    //                      model.persistence.Storage.session.remove("workingUser");

    //                      this.router.navTo("soLaunchpad");
    //                      }
  },




  onDataOrgDialogConfirm: function (evt) {
    //var source = evt.getSource();
    var list = sap.ui.getCore().byId("dataOrgSelectListId");
    var dataOrg = this.dataOrgModel.getData().results;
    var selectedResult;
    var oContext = list.getSelectedItem();
    if (oContext === null || oContext === undefined) {
      sap.m.MessageBox.alert(model.i18n.getText("PLEASE_MAKE_A_CHOICE"), {
        title: model.i18n.getText("ATTENTION"),
        onClose: null

      });
      return;
    }
    var path = oContext.getBindingContextPath();
    var currentIndex = parseInt(path.split('/')[path.split('/').length - 1]);
    selectedResult = dataOrg[currentIndex];
    var workingUsr = _.cloneDeep(this.user);
    if (workingUsr.organizationData.results.length > 0) {
      workingUsr.organizationData.results = [];
    }
    workingUsr.organizationData.results.push(selectedResult);
    model.persistence.Storage.session.save("workingUser", workingUsr);
    this.workingUser = workingUsr;

    this.userModel.setData(this.workingUser);
    this.refreshTiles(this.workingUser);
    sap.m.MessageToast.show(model.i18n.getText("WORKING_USER_SAVED"));
    if (this.dataOrgDialog) {
      this.dataOrgDialog.close();
      //this.dataOrgDialog.destroy();
    }
    this.appModel.setProperty("/changeWorkingSetButtonVisible", true);
  },

  onChangeWorkingSet: function (evt) {
    if (!this.dataOrgDialog) {
      this.dataOrgDialog = sap.ui.xmlfragment("view.dialog.selectOrganizationData", this);
    }
     var page = this.getView().byId("launchpadPageId");
     page.addDependent(this.dataOrgDialog);

    this.dataOrgDialog.open();

    var list = sap.ui.getCore().byId("dataOrgSelectListId");
    if (!list.getSelectedItem()) {
      list.setSelectedItem(list.getItems()[0]);
    }
    // $(document).keyup(_.bind(this.keyUpFunc, this));
  },




  onTilePress: function (oEvent) {
    //recupero la tile
    var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
    var url = tilePressed.url;
    //verifico se chiudo la sessione cliente
    if (tilePressed.title === model.i18n.getText("CLOSE_SESSION")) {
      model.persistence.Storage.session.remove("currentCustomer");
      model.persistence.Storage.session.remove("customerSession");
      var selectedDivision = model.persistence.Storage.session.get("division");

      if (selectedDivision === "CF") {
        var companies = _.where(this.user.organizationData.results, {
          'society': "SI41"
        });
        if (companies.length === 1) {
          this.appModel.setProperty("/changeWorkingSetButtonVisible", false);
        } else {
          this.appModel.setProperty("/changeWorkingSetButtonVisible", true);
        }
      }


      if (this.user.organizationData.results.length === 1) {
        this.appModel.setProperty("/navBackVisible", false);
      } else {
        this.appModel.setProperty("/navBackVisible", true);
      }

      //      var division = model.persistence.Storage.session.get("division");
      //      this.closeCustomerSession(url, division);
      if (this.workingUser) {
        this.userModel.setData(this.workingUser);
        this.refreshTiles(this.workingUser);
      } else {
        this.userModel.setData(this.user);
        this.refreshTiles(this.user);

      }
    }
    if (tilePressed.url === "newOrder" || tilePressed.url == "newOffer") {
      var username = this.getUserInfo().username;
      var selectedCustomerId = this.getCustomerInfo().customerId;
      var checkSavedOrderExistence = model.persistence.PersistenceManager.checkSavedData(username + selectedCustomerId + "UBackup", username + selectedCustomerId + "OBackup");
      if (checkSavedOrderExistence) {
        var orgData = this.getOrgDataForSAP();
        var backupUserData = model.persistence.PersistenceManager.loadUserSavedData(username + selectedCustomerId + "UBackup");
        if (backupUserData.username === username && backupUserData.customerId === selectedCustomerId && backupUserData.Bukrs === orgData.Bukrs && backupUserData.Vkorg === orgData.Vkorg && backupUserData.Vtweg === orgData.Vtweg && backupUserData.Spart === orgData.Spart) {
          var that = this;
          sap.m.MessageBox.confirm(
            model.i18n._getLocaleText("RESTORE_ORDER"), {
              title: model.i18n._getLocaleText("RESTORE_ORDER_TITLE"),
              actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
              onClose: function (oAction) {
                if (oAction === "YES") {
                  that.appModel.setProperty("/restoreOrder", true);
                  that.launchApp(url);
                } else {
                  that.appModel.setProperty("/restoreOrder", false);
                  model.persistence.PersistenceManager.flushUserSavedData(username + selectedCustomerId + "UBackup");
                  model.persistence.PersistenceManager.flushModel(username + selectedCustomerId + "OBackup");
                  that.launchApp(url);
                }
              }
            }
          );

        } else {
          //lancio l'app
          this.launchApp(url);
        }

      } else {
        //lancio l'app
        this.launchApp(url);
      }
    } else {
      //lancio l'app
      this.launchApp(url);
    }


  },

  onLinkToUserInfoPress: function (evt) {
    $(document).off("keyup");
    this.router.navTo("changePassword");
  },

  //  closeCustomerSession: function (url, division) {
  //
  //
  ////      $(document).off("keyup");
  //      this.router.myNavBack(url, {division: division});
  //  },

  launchApp: function (url) {
    var isPhone = sap.ui.Device.system.phone;
    switch (url) {
    case "customersList":
      if (!!isPhone) {
        this.router.navTo("customersListMobile");
      } else {
        this.router.navTo(url);
      }
      break;

    case "emptyHistoryCarts":
      if (!!isPhone) {
        this.router.navTo("historyCartsListMobile");
      } else {
        this.router.navTo(url);
      }
      break;

    case "noDataSplitDetail":
      if (!!isPhone) {
        this.router.navTo("shippment.shippmentsListMobile");
      } else {
        this.router.navTo(url);
      }
      break;

    case "emptyROHierarchy":
      if (!!isPhone) {
        this.router.navTo("readOnlyHierarchyMobile");
      } else {
        this.router.navTo(url);
      }
      break;
    default:
      this.router.navTo(url);
    }
    $(document).off("keyup");
    //    this.router.navTo(url);
  },

  //**
  refreshTiles: function (user) {
    var isPhone = sap.ui.Device.system.phone;
    var session = sessionStorage.getItem("customerSession") ? model.persistence.Storage.session.get("customerSession") : false;
    var customer = model.Current.getCustomer() ? model.persistence.Storage.session.get("currentCustomer") : undefined;
    // var tile = model.Tiles.getMenu(user, session, isPhone, customer); //I'd modify here to check user permits to create customers and orders
    // this.tileModel.setData(tile);
    // this.tileModel.refresh();
    this.getView().setBusy(true);
    model.Tiles.getMenu(user, session, isPhone, customer) //I'd modify here to check user permits to create customers and orders
    .then(_.bind(function(res){
    	this.getView().setBusy(false);
      this.tileModel.setData(res);
      this.tileModel.refresh();
    }, this))
  },
  navBack: function () {


    //    var division = model.persistence.Storage.session.get("division");
    //    var society = model.persistence.Storage.session.get("society");
    $(document).off("keyup");
    //    if (this.user.organizationData && _.chain(this.user.organizationData.results).pluck('society').uniq().value().length === 1) {
    //      //this.router.navTo("divisionLaunchpad", {division:division});
    //      this.router.navTo("divisionLaunchpad", {
    //        society: society
    //      });
    //    } else if (this.user.organizationData && _.chain(this.user.organizationData.results).pluck('society').uniq().value().length > 1) {
    //      this.router.navTo("soLaunchpad", {
    //        society: society
    //      });
    //    }
    this.router.navTo("soLaunchpad");
    model.persistence.Storage.session.remove("workingUser");

  }
});
