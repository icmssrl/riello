jQuery.sap.require("utils.Validator");

sap.ui.controller("view.user.User", {

	onInit: function () {

		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
	},


	handleRouteMatched: function (evt) {

		var name = evt.getParameter("name");
    
    
		if ( name !== "user"){
			return;
		}

	},
  //validazioni
	validateEmail: function (evt) {
		var control = sap.ui.getCore().byId(evt.getParameter("id"));
		utils.Validator.email(control);
	},

	validateRequired: function (evt) {
		var control = sap.ui.getCore().byId(evt.getParameter("id"));
		utils.Validator.required(control);
	},
	validateFiscalCode: function (evt) {
		var control = sap.ui.getCore().byId(evt.getParameter("id"));
		utils.Validator.vat(control);
	},
	validateTel: function (evt) {
		var control = sap.ui.getCore().byId(evt.getParameter("id"));
		utils.Validator.tel_fax(control);
	},

	popUpFiscalCode: function (evt) {
		var control = sap.ui.getCore().byId(evt.getParameter("id"));

		if (control.getValueState() !== "Success") {
			var controlDialog = new sap.m.Dialog();
			controlDialog.setTitle(this.T.getText("WARNING_FC"));
			var text = new sap.m.Text({
				text: this.T.getText("INVALID_FISCAL_CODE")
			});
			controlDialog.addContent(text);

			controlDialog.addButton(new sap.m.Button({
				id: "dd_del",
				text: "OK"
			}).attachPress(function () {
				control.focus();
				controlDialog.close();
				controlDialog.destroy();
			}));
			controlDialog.open();
		}
	},
  
  goBack: function(evt){
    this.router.navTo("login");
  }
  
//  validatePostalCode: function (evt) {
//		var control = sap.ui.getCore().byId(evt.getParameter("id"));
//		var val = control.getValue();
//		if (val === undefined) {
//			return;
//		}
//
//		//val = val.replace(/\s+/g, '');
//
//		var cISO = this.model.getProperty("/country");
//		if (cISO === undefined) {
//			return;
//		}
//		if (utils.Validator.postalCode(val, cISO)) {
//			control.setValueState("Success");
//		} else {
//			control.setValueState("Error");
//		}
//	},

  
  //	validatePass: function (evt) {
//		var control1 = sap.ui.getCore().byId("views.fragment.user.--fragEdit--pass1");
//		var control2 = sap.ui.getCore().byId("views.fragment.user.--fragEdit--pass2");
//		utils.Validator.passwords(control1, control2);
//	},
//	validatePassOld: function (evt) {
//		var control1 = sap.ui.getCore().byId("views.fragment.changePassword.--fragEdit--passOld1");
//		var control2 = sap.ui.getCore().byId("views.fragment.changePassword.--fragEdit--passOld2");
//		utils.Validator.passwords(control1, control2);
//	},
//	validatePassNew: function (evt) {
//		var control1 = sap.ui.getCore().byId("views.fragment.changePassword.--fragEdit--passNew1");
//		var control2 = sap.ui.getCore().byId("views.fragment.changePassword.--fragEdit--passNew2");
//		utils.Validator.passwords(control1, control2);
//	},
//	validatePassReset: function (evt) {
//		var control1 = sap.ui.getCore().byId("views.fragment.resetPassword.confirmation.--fragEdit--passNew1");
//		var control2 = sap.ui.getCore().byId("views.fragment.resetPassword.confirmation.--fragEdit--passNew2");
//		utils.Validator.passwords(control1, control2);
//	},
    



});
