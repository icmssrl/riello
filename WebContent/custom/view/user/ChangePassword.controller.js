jQuery.sap.require("sap.m.MessageBox");

jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.User");
jQuery.sap.require("utils.Message");


view.abstract.AbstractController.extend("view.user.ChangePassword", {

  onInit: function () {
    this.router = sap.ui.core.UIComponent.getRouterFor(this);
    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);

    this.editPass = new sap.ui.model.json.JSONModel();
    this.editPass.setProperty("/passOld", "")
    this.editPass.setProperty("/passNew", "")
    this.editPass.setProperty("/passNewConfirm", "")
    this.getView().setModel(this.editPass, "editPass");
    

    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");
    this.userModel.setProperty("/modPass", false);
    this.userModel.setProperty("/confirmChange", false);
    //** risetto il css 
    this.cssReload(); //in abstract
    //**
  },
  
  handleRouteMatched: function (evt) {
    var userInfo = model.persistence.Storage.session.get("user");
    if (userInfo) {
      userInfo = userInfo.userGeneralData;
    }
    var userType = "";
    var name = evt.getParameter("name");
    if (name !== "changePassword") {
      return;
    };
    
    this.user = new model.User(userInfo);
    this.userModel.setProperty("/username", userInfo.username);
    this.userModel.setProperty("/customer", userInfo.customer);
    this.userModel.setProperty("/fullName", userInfo.fullName);
    this.userModel.setProperty("/mail", userInfo.mail);
    if (userInfo.userType === "AG") {
      userType = "Agente";
    } else if (userInfo.userType === "TM") {
      userType = "Territory Manager";
    } else if (userInfo.userType === "AM") {
      userType = "Area Manager";
    }
    this.userModel.setProperty("/type", userType);
    this.userModel.setProperty("/name", userInfo.name);

    this.initialize();
    this.passInsertOld = false;
    this.passInsertNew = false;
    this.passInsertNewConfirm = false;
  },
  initialize:function(){
//	  this.editPass = new sap.ui.model.json.JSONModel();
	  this.editPass.setProperty("/passOld", "");
	  this.editPass.setProperty("/passNew", "");
	  this.editPass.setProperty("/passNewConfirm", "");
	  this.editPass.refresh();
	  
//	  this.getView().setModel(this.editPass, "editPass");
    

//	  this.userModel = new sap.ui.model.json.JSONModel();
	  this.userModel.setProperty("/modPass", false);
	  this.userModel.setProperty("/confirmChange", false);
	  this.userModel.refresh();
//	  this.getView().setModel(this.userModel, "userModel");
	  
  },
  navBack: function () {
    this.router.myNavBack();
    this.editPass.setProperty("/passOld", "");
    this.editPass.setProperty("/passNew", "");
    this.editPass.setProperty("/passNewConfirm", "");
    this.userModel.setProperty("/confirmChange", false);
    this.userModel.setProperty("/modPass", false);
  },

  modifyPassword: function () {
    if (this.userModel.getProperty("/modPass") === false) {
      this.userModel.setProperty("/modPass", true);
    } else {
      this.userModel.setProperty("/modPass", false);
    };
  },
  liveChange:function(evt)
  {
	  var src = evt.getSource();
	  if(src.getId().indexOf("passOld")>=0)
	  {
		  this.editPass.setProperty("/passOld", src.getValue());
	  }
	  else if(src.getId().indexOf("passNewConfirm")>=0)
	  {
		  this.editPass.setProperty("/passNewConfirm", src.getValue());
	  }
	  else if(src.getId().indexOf("passNew")>=0)
	  {
		  this.editPass.setProperty("/passNew", src.getValue());
	  }
	  
	  if( this.editPass.getProperty("/passNew") && this.editPass.getProperty("/passNewConfirm") && this.editPass.getProperty("/passOld"))
	  {
		  this.userModel.setProperty("/confirmChange", true);
	  }
	  else
	  {
		  this.userModel.setProperty("/confirmChange", false);
	  }
	  this.userModel.refresh();
  },
//  liveChange1: function (evt) {
//    if (evt.getParameters().value !== "") {
//      this.passInsertOld = true;
//    } else {
//      this.passInsertOld = false;
//    };
//    if (this.passInsertOld === true && this.passInsertNew === true && this.passInsertNewConfirm === true) {
//      this.userModel.setProperty("/confirmChange", true);
//    } else {
//      this.userModel.setProperty("/confirmChange", false);
//    };
//  },
//
//  liveChange2: function (evt) {
//    if (evt.getParameters().value !== "") {
//      this.passInsertNew = true;
//    } else {
//      this.passInsertNew = false;
//    };
//    if (this.passInsertOld === true && this.passInsertNew === true && this.passInsertNewConfirm === true) {
//      this.userModel.setProperty("/confirmChange", true);
//    } else {
//      this.userModel.setProperty("/confirmChange", false);
//    };
//  },
//
//  liveChange3: function (evt) {
//    if (evt.getParameters().value !== "") {
//      this.passInsertNewConfirm = true;
//    } else {
//      this.passInsertNewConfirm = false;
//    };
//    if (this.passInsertOld === true && this.passInsertNew === true && this.passInsertNewConfirm === true) {
//      this.userModel.setProperty("/confirmChange", true);
//    } else {
//      this.userModel.setProperty("/confirmChange", false);
//    };
//  },
  confirmChange : function()
  {
	  var newPwd1 = this.editPass.getProperty("/passNew");
	  var newPwd2 = this.editPass.getProperty("/passNewConfirm");
	  if(newPwd1 !== newPwd2)
	  {
		  sap.m.MessageBox.alert(model.i18n.getText("PASSWORD_NOT_EQUALS"));
		  return
	  }
	  //------------------------------------------------
	  //newPwd1=jQuery.base64.encode(newPwd1);
//	  newPwd1=encodeURIComponent(jQuery.base64.encode(newPwd1));
//	  //newPwd2=jQuery.base64.encode(newPwd2);
//	  newPwd2=encodeURIComponent(jQuery.base64.encode(newPwd2));
//	  
	  var oldPwd =  this.editPass.getProperty("/passOld");
//	  //oldPwd=jQuery.base64.encode(oldPwd);
//	  oldPwd=encodeURIComponent(jQuery.base64.encode(oldPwd));
	  
	  this.getView().setBusy(true);
	  this.user.setNewPassword(newPwd1, newPwd2, oldPwd)
	  .then(_.bind(function(result){
		  sap.m.MessageToast.show(model.i18n.getText("CHANGE_PWD_SUCCESS"));
		  jQuery.sap.delayedCall(3000, this, _.bind(function(){
			 
			  this.user = new model.User();
			  
			  this.getView().setBusy(false);
			  this.router.navTo("login");
			  this.doLogout();
		  }, this));
	  }, this),
			_.bind(function(err){
				this.getView().setBusy(false);
				sap.m.MessageBox.alert(utils.Message.getError(err));
			}, this))
  }
  //    confirmChange: function () {
  //        var passOld = this.editPass.getData().passOld;
  //        var passNew = this.editPass.getData().passNew;
  //        var passConfirm = this.editPass.getData().passNewConfirm;
  //        if (passOld !== $.parseJSON(atob(localStorage.getItem("user"))).p) {
  //            sap.m.MessageBox.alert(this._getLocaleText("passOldDiff"), this._getLocaleText("avvisoPass"));
  //        } else {
  //            if (passOld === passNew) {
  //                sap.m.MessageBox.alert(this._getLocaleText("passUgualiOldNew"), this._getLocaleText("avvisoPass"));
  //            } else {
  //                if (passNew !== passConfirm) {
  //                    sap.m.MessageBox.alert(this._getLocaleText("passDiverse"), this._getLocaleText("avvisoPass"));
  //                } else {
  //                    this.userModel.setProperty("/modPass", false);
  //
  //                   
  //        this.editPass.setProperty("/passOld", "");
  //        this.editPass.setProperty("/passNew", "");
  //        this.editPass.setProperty("/passNewConfirm", "");
  //        this.userModel.setProperty("/confirmChange", false);
  //   }

});