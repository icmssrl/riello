jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.OrderList");
jQuery.sap.require("model.collections.BlockingOrderReasons");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.collections.DeliveryList");
jQuery.sap.require("model.collections.TrackingOrderInfo");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.CustomerRequester");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("utils.Message");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.praticheAnomale.Reso");

view.abstract.AbstractMasterController.extend(
				"view.praticheAnomale.ReturnList",
				{

					onExit : function() {

					},

					onInit : function() {

						// this.router =
						// sap.ui.core.UIComponent.getRouterFor(this);
						// this.router.attachRoutePatternMatched(this.handleRouteMatched,
						// this);
						view.abstract.AbstractController.prototype.onInit
								.apply(this, arguments);
						this.uiModel.setProperty("/searchProperty", [
								"orderId", "customerName" ]);

						this.userModel = new sap.ui.model.json.JSONModel();
						this.getView().setModel(this.userModel, "userModel");

					},

					handleRouteMatched : function(evt) {

						var name = evt.getParameter("name");
			            
						if (name !== "returnList") {
							return;
						}
						
						//------------------TO CHECK-----------------------------------
						var documentType;
						
			            documentType = 'H';
						model.persistence.Storage.session.save("documentType",documentType);
						//---------------------------------------------------------------
						this.user = model.persistence.Storage.session
								.get("user");
						this.userModel.setData(this.user);

						this.orderListModel = new sap.ui.model.json.JSONModel();
						this.getView().setModel(this.orderListModel,
								"orderList");

						this.reqData = this.getUserInfo();
						if (!!sessionStorage.getItem("customerSession")
								&& sessionStorage.getItem("customerSession") === "true") {
							this.reqData = this.getCustomerInfo();
						}
						this.obblData = _.pick(this.reqData,
								[ "salesOrg", "division", "distrCh", "society",
										"areaManager" ]);
						this.getView().setBusy(true);
						model.collections.OrderList.getOrderList(this.reqData) // forceReload
						.then(
								_.bind(function(res) {
									// console.log(res);

									this.orderListModel.setData(res);
									this.orderListModel.refresh(true);
									this.getView().setBusy(false);

								}, this),
								_.bind(function(err) {
									this.getView().setBusy(false);
									sap.m.MessageToast.show(utils.Message
											.getError(err));
								}, this));

						if (this.blockingReasonsDialog) {
							this.blockingReasonsDialog.destroy();
						}

						this.odataReq = this.getUserInfo();

						this.resetODataFilter();

						// model.collections.OrderList.loadOrders(true)
						// //forceReload
						// .then(_.bind(function(res){
						// //console.log(res);
						//
						// this.orderListModel.setData(res);
						// this.orderListModel.refresh(true);
						//
						// }, this));

						// ***********Primo metodo usato mettendo il filtro
						// direttamente nella view*********/////////////
						//
						// if (!!sessionStorage.getItem("customerSession") &&
						// sessionStorage.getItem("customerSession")==="true"){
						// var customerData =
						// model.persistence.Storage.session.get("currentCustomer");
						// if(!!customerData && !!customerData.registry.id){
						// var customerIdValue = customerData.registry.id;
						// this.applyFilter(customerIdValue, "customerId");
						// }
						// var oTable = this.getView().byId("orderListTable");
						//
						// }else{
						// this.applyFilter("", "customerId");
						// return;
						// }
						// ***********************************************************************

					},

					onInfoPress : function(evt) {
						var oTable = this.getView().byId("orderListTable");
					},

					onLinkToPdfPress : function(evt) {
						// sap.m.URLHelper.redirect(this._getVal(evt), true);
						// var url =
						// "http://sap-nwg.sapit.riello.lan/eweb/PDF/0101036203.pdf";
						var orderId = evt.getSource().getBindingContext(
								"orderList").getObject().orderId;
						utils.Busy.show();
						var fSuccess = function(result) {
							utils.Busy.hide();
							sap.m.URLHelper.redirect(result.EUrl, true); // To
																			// check
																			// popup
																			// block
							// var w = window;
							// w.open(result.EUrl, '_blank');
							// sap.m.URLHelper.redirect(result.EUrl);
						}
						fSuccess = _.bind(fSuccess, this);
						var fError = function(err) {
							utils.Busy.hide();
							utils.Message.getError(err);
						}
						fError = _.bind(fError, this);

						model.odata.chiamateOdata.loadOrderPdf(orderId,
								fSuccess, fError);

					},

					onAfterRendering : function() {

						// Set Row Color Conditionally

						// var oModel = oTable.getModel();
						// var rowCount = oTable.getVisibleRowCount();
						// //var rowCount = oTable.getVisibleItems().length;
						// //number of visible rows
						// var rowStart = oTable.getFirstVisibleRow();
						// //starting Row index
						// var currentRowContext;
						// for (var i = 0; i < rowCount; i++) {
						// currentRowContext = oTable.getContextByIndex(rowStart
						// + i); //content
						// // Remove Style class else it will overwrite
						// oTable.getRows()[i].$().removeClass("yellow");
						// oTable.getRows()[i].$().removeClass("green");
						// oTable.getRows()[i].$().removeClass("red");
						// var cellValue = oModel.getProperty("amount",
						// currentRowContext); // Get Amount
						// // Set Row color conditionally
						// if (cellValue >= 1500) {
						// oTable.getRows()[i].$().addClass("green");
						// } else if (cellValue < 1500 && cellValue > 1000) {
						// oTable.getRows()[i].$().addClass("yellow");
						// } else {
						// oTable.getRows()[i].$().addClass("red");
						// }
						// }
					},

					onItemPress : function(evt) {
						var src = evt.getSource();
						var selectedItem = src.getBindingContext("p")
								.getObject();
						this.getView().getModel("appStatus").setProperty(
								"/currentProduct", selectedItem);
						// this.getView().getModel("appStatus").setProperty("/masterCntrl",
						// this.getView().getController());
						this.navToProduct(selectedItem.getId());

					},

					// refreshList: function (evt) {
					// //var filters = this.getFiltersOnList();
					// //this.productsModel =
					// model.collections.Products.getModel();
					//
					// this.orderListModel.setData();
					//
					// // if (filters)
					// //
					// this.getView().byId("list").getBinding("items").filter(filters);
					//
					// },
					onFilterPress : function() {
						this.filterModel = model.filters.Filter
								.getModel(
										this.orderListModel.getData().results,
										"orders");
						this.getView().setModel(this.filterModel, "filter");
						var page = this.getView().byId("orderListPageId");
						this.filterDialog = sap.ui.xmlfragment(
								"view.dialog.filterDialog", this);
						page.addDependent(this.filterDialog);
						this.filterDialog.open();

					},
					onFilterDialogClose : function() {
						this.filterDialog.close();
					},

					onFilterPropertyPress : function(evt) {

						var parentPage = sap.ui.getCore().byId("parent");
						var elementPage = sap.ui.getCore().byId("children");
						// console.log(this.getView().getModel("filter").getData().toString());
						var navCon = sap.ui.getCore().byId("navCon");
						var selectedProp = evt.getSource().getBindingContext(
								"filter").getObject();
						this.getView().getModel("filter").setProperty(
								"/selected", selectedProp);
						this.elementListFragment = sap.ui.xmlfragment(
								"view.fragment.filterList", this);
						elementPage.addContent(this.elementListFragment);

						navCon.to(elementPage, "slide");
						this.getView().getModel("filter").refresh();
					},

					onBackFilterPress : function(evt) {
						// this.addSelectedFilterItem();
						this.navConBack();
						this.getView().getModel("filter").setProperty(
								"/selected", "");
						this.elementListFragment.destroy();
					},
					navConBack : function() {
						var navCon = sap.ui.getCore().byId("navCon");
						navCon.to(sap.ui.getCore().byId("parent"), "slide");
						this.elementListFragment.destroy();
					},
					afterOpenFilter : function(evt) {
						var navCon = sap.ui.getCore().byId("navCon");
						if (navCon.getCurrentPage().getId() == "children")
							navCon.to(sap.ui.getCore().byId("parent"), "slide");
						this.getView().getModel("filter").setProperty(
								"/selected", "");
					},

					onSearchFilter : function(oEvt) {
						var aFilters = [];
						var sQuery = oEvt.getSource().getValue();

						if (sQuery && sQuery.length > 0) {

							// var filter = new sap.ui.model.Filter("value",
							// sap.ui.model.FilterOperator.Contains, sQuery);

							// var filter = new
							// sap.ui.model.Filter({path:"value",
							// test:function(val)
							// {
							// var property= val.toString().toUpperCase();
							// return
							// (property.indexOf(sQuery.toString().toUpperCase())>=0)
							// }});

							aFilters.push(this.createFilter(sQuery, "value"));
						}

						// update list binding
						var list = sap.ui.getCore().byId("filterList");

						var binding = list.getBinding("items");
						binding.filter(aFilters);
					},
					createFilter : function(query, property) {
						var filter = new sap.ui.model.Filter({
							path : property,
							test : function(val) {
								var prop = val.toString().toUpperCase();
								return (prop.indexOf(query.toString()
										.toUpperCase()) >= 0)
							}
						});
						return filter;
					},
					onFilterDialogClose : function(evt) {
						if (this.elementListFragment) {
							this.elementListFragment.destroy();
						}
						if (this.filterDialog) {
							this.filterDialog.close();
							this.filterDialog.destroy();
						}
					},
					onFilterDialogOK : function(evt) {
						var filterItems = model.filters.Filter
								.getSelectedItems("orders");
						if (this.elementListFragment)
							this.elementListFragment.destroy();
						this.filterDialog.close();
						this.getView().getModel("filter").setProperty(
								"/selected", "");
						this.handleFilterConfirm(filterItems);
						this.filterDialog.destroy();
						delete (this.filterDialog);
					},
					handleFilterConfirm : function(selectedItems) {
						var filters = [];
						_.forEach(selectedItems, _.bind(function(item) {
							filters.push(this.createFilter(item.value,
									item.property));
						}, this));
						var table = this.getView().byId("orderListTable");
						var binding = table.getBinding("items");
						binding.filter(filters);
					},
					onResetFilterPress : function() {
						model.filters.Filter.resetFilter("orders");
						if (this.elementListFragment) {
							this.elementListFragment.destroy();
						}
						if (this.filterDialog) {
							this.filterDialog.close();
							this.filterDialog.destroy();
						}
						var table = this.getView().byId("orderListTable");
						var binding = table.getBinding("items");
						binding.filter();
						// sap.m.MessageToast.show("All filters cleared!");
						// //console.log(model.filters.Filter.getSelectedItems("customers"));
					},

					navToOrderDetails : function(evt) {

						var src = evt.getSource();
						var selectedItem = src.getBindingContext("orderList")
								.getObject();
						var id = selectedItem.orderId;
						
						var currentPractice = this._convertOrderToReso(selectedItem);
						
						model.persistence.Storage.session.save("currentPractice", currentPractice);
//            var documentType;
//            documentType = model.persistence.Storage.session.get("documentType");
//            if (documentType === "H")
//  						this.router.navTo("orderInfo", {
//  							id : id
//  						});
//            else
//  						this.router.navTo("orderInfo", {
//  							id : id
//  						});
						this.router.navTo("resoDetail",{id :id});

					},

					_convertOrderToReso:function(orderItem)
					{
						var reso = {};
						for(var prop in orderItem)
						{
							if(prop == "orderReason")
							{
								reso.renderReason = orderItem[prop];
								continue;
							}
								
							if(prop == "orderType")
							{
								reso.renderType = orderItem[prop];
								continue;
							}
							
							if(prop == "orderTypeDescr")
							{
								reso.renderTypeText = orderItem[prop];
								continue;
							}
							if(prop == "orderReasonDescr")
							{
								reso.renderReasonText = orderItem[prop];
								continue;
							}
							if(prop == "orderId")
							{
								reso.resoId = orderItem[prop];
								continue;
							}
							if(prop == "orderDate")
							{
								reso.resoDate = orderItem[prop];
								continue;
							}
							reso[prop]= orderItem[prop];
								
						}
						return new model.praticheAnomale.Reso(reso);
					},
					onSearch : function(evt) {
						var src = evt.getSource();
						this.searchValue = src.getValue();
						var searchProperty = this.uiModel
								.getProperty("/searchProperty");

						this.applyFilter(this.searchValue, searchProperty);

					},
					applyFilter : function(value, params) {

						var table = this.getView().byId("orderListTable");
						if (!table.getBinding("items").oList
								|| table.getBinding("items").oList.length === 0)
							return;

						var temp = table.getBinding("items").oList[0]; // a
																		// template
																		// just
																		// to
																		// recover
																		// the
																		// data
																		// types
						var filtersArr = [];
						// var props = utils.ObjectUtils.getKeys(temp);

						if (!_.isEmpty(params)) {
							if (!_.isArray(params)) {
								params = [ params ];
							}
							for (var i = 0; i < params.length; i++) {

								switch (typeof (utils.ObjectUtils.getValues(
										temp, params[i]))) {
								case "undefined":
									break;
								case "string":
									filtersArr
											.push(new sap.ui.model.Filter(
													params[i],
													sap.ui.model.FilterOperator.Contains,
													value));
									break;
								case "number":
									filtersArr.push(new sap.ui.model.Filter(
											params[i],
											sap.ui.model.FilterOperator.EQ,
											value));
									break;
								}

							}
							var filter = new sap.ui.model.Filter({
								filters : filtersArr,
								and : false
							});
							table.getBinding("items").filter(filter);
							return;
						}
						table.getBinding("items").filter();
					},

					onOrderHeaderPress : function(evt) {
						this.router.navTo("newOrder");
					},

					onUnlockPress : function(evt) {
						var src = evt.getSource();
						var item = src.getBindingContext("orderList")
								.getObject();
						model.collections.OrderList.activateState(item).then(
								_.bind(function(result) {
									this.orderListModel.refresh();
								}, this));

					},
					onNoUnlockPress : function(evt) {
						var src = evt.getSource();
						var item = src.getBindingContext("orderList")
								.getObject();
						model.collections.OrderList.noActivateState(item).then(
								_.bind(function(result) {
									this.orderListModel.refresh();
								}, this));

					},

					onShowBlockingReasonsPress : function(evt) {
						var src = evt.getSource();
						var selectedItem = src.getBindingContext("orderList")
								.getObject();
						var idOrder = selectedItem.orderId;
						this.blockingReasonsDialog = sap.ui.xmlfragment(
								"view.dialog.showBlockingReasons", this);
						var page = this.getView().byId("orderListPageId");
						page.addDependent(this.blockingReasonsDialog);
						// this.blockingReasonModel = new
						// sap.ui.model.json.JSONModel();
						// this.getView().setModel(this.blockingReasonModel,
						// "blockOrderReason");
						this.getView().setBusy(true);
						model.collections.BlockingOrderReasons
								.loadBlockingOrderReasons(idOrder)
								// forceReload
								.then(
										_
												.bind(
														function(res) {
															// console.log(res);
															this
																	.getView()
																	.setModel(
																			model.collections.BlockingOrderReasons
																					.getModel(),
																			"blockOrderReason");
															this
																	.getView()
																	.setBusy(
																			false);
															this.blockingReasonsDialog
																	.open();
															// this.blockingReasonModel.setData(res.getModel());
															// this.blockingReasonModel.refresh(true);

														}, this));

						$(document).keyup(_.bind(this.keyUpFunc, this));
					},

					keyUpFunc : function(e) {
						if (e.keyCode == 27) {
							// codice per il pulsante escape per evitare che lo
							// user chiuda il dialog via ESC

							if (this.blockingReasonsDialog) {
								this.blockingReasonsDialog.destroy();
							}
							$(document).off("keyup");
							// this.router.navTo("launchpad");
						}
					},

					onBlockReasonsDialogOk : function(evt) {
						if (this.blockingReasonsDialog) {
							this.blockingReasonsDialog.close();
							this.blockingReasonsDialog.destroy();
						}
						// sap.m.MessageToast.show(model.i18n._getLocaleText("NO_PARTNER_CHOOSEN"));

					},

					handlePressOfCustomerName : function(evt) {
						// var isTouch = sap.ui.Device.support.touch;
						// if(!isTouch){
						// return;
						// }else{
						var src = evt.getSource();
						var position = src.getBindingContext("orderList")
								.getObject();
						this.orderListRowModel = new sap.ui.model.json.JSONModel();
						this.orderListRowModel.setData(position);
						this.getView().setModel(this.orderListRowModel,
								"orderListRow");

						// create popover
						if (!this._oPopoverCustomerName) {
							this._oPopoverCustomerName = sap.ui
									.xmlfragment(
											"view.dialog.popovers.mobilePopoverOrderList",
											this);
							this.getView().addDependent(
									this._oPopoverCustomerName);
							// this._oPopover.bindElement(position);
						}

						// delay because addDependent will do a async
						// rerendering and the actionSheet will immediately
						// close without it.
						jQuery.sap.delayedCall(0, this, function() {
							this._oPopoverCustomerName.openBy(src);
						});
						// }

					},

					handlePressOfShippmentDate : function(evt) {

						var src = evt.getSource();
						var position = src.getBindingContext("orderList")
								.getObject();
						this.orderListShippmentDateModel = new sap.ui.model.json.JSONModel();
						this.orderListShippmentDateModel.setData(position);
						this.getView().setModel(
								this.orderListShippmentDateModel,
								"orderListShippmentDate");

						// create popover
						if (!this._oPopoverShippmentDate) {
							this._oPopoverShippmentDate = sap.ui
									.xmlfragment(
											"view.dialog.popovers.mobilePopoverShippmentDate",
											this);
							this.getView().addDependent(
									this._oPopoverShippmentDate);
							// this._oPopover.bindElement(position);
						}

						// delay because addDependent will do a async
						// rerendering and the actionSheet will immediately
						// close without it.
						jQuery.sap.delayedCall(0, this, function() {
							this._oPopoverShippmentDate.openBy(src);
						});
						// }

					},

					openAddPartnerDialog : function(evt) {
						this.addPartnersDialog = sap.ui.xmlfragment(
								"view.dialog.addPartnersDialog", this);
						var page = this.getView().byId("detailPageId");
						page.addDependent(this.addPartnersDialog);
						model.collections.Partners.loadPartners().then(
								_.bind(function(res) {
									// console.log(res);
									this.refreshPartnerList(res);
									this.addPartnersDialog.open();
								}, this));

						$(document).keyup(_.bind(this.keyUpFunc, this));
					},

					onODataFilterPress : function(evt) {
						// Temp----------------------------------------------------------
						// sap.m.MessageToast.show("Work In Progress");
						// -----------------------------------------------------------------

						// ----------------OLD---------------------------------------------------
						// if (!this.odataFilterDialog) {
						// this.odataFilterDialog =
						// sap.ui.xmlfragment("view.dialog.odataOrderListFilterDialog",
						// this);
						// }
						// var page = this.getView().byId("orderListPageId");
						// page.addDependent(this.odataFilterDialog);
						//
						// if (!this.filterODataModel) {
						//
						// this.resetODataFilter();
						//
						// }
						// this.populateStatusSelect()
						// .finally(_.bind(function(res){
						// this.odataFilterDialog.open();
						// }, this))

						// -----------------------------------------------------------------------------------------
						//
						if (!this.odataFilterDialog) {
							this.odataFilterDialog = sap.ui.xmlfragment(
									"view.dialog.odataOrderListFilterDialog2",
									this);
						}
						var page = this.getView().byId("orderListPageId");
						page.addDependent(this.odataFilterDialog);

						if (!this.filterODataModel) {

							this.resetODataFilter();

						}
						this.populateStatusSelect()  //.fin--why?
						.then(_.bind(function(res) {
							this.odataFilterDialog.open();
						}, this))

					},

					// _getODataFilterPropsByUser: function () {
					// var props = [{
					// "key": "customerId",
					// "name": model.i18n.getText("CUSTOMER")
					// }
					// // {
					// // "key": "statoOrdine",
					// // "name": model.i18n.getText("ORDERSTATUS")
					// // }
					// // ,{
					// // "key": "statoConsegna",
					// // "name": model.i18n.getText("DELIVERY_STATUS")
					// // }
					// ];
					// if (this.user.type === "AG") {
					//
					// this.obblData["territoryManager"] =
					// this.reqData["territoryManager"];
					// this.obblData["agentCode"] = this.reqData["agentCode"];
					// return props;
					// }
					// props.push({
					// "key": "agentCode",
					// "name": model.i18n.getText("AGENT")
					// });
					// if (this.user.type === "TM") {
					// this.obblData["territoryManager"] =
					// this.reqData["territoryManager"];
					// return props;
					// }
					// if (this.user.type === "AM") {
					// props.push({
					// "key": "territoryManager",
					// "name": model.i18n.getText("TERRITORYMANAGER")
					// });
					// return props;
					// }
					// },
					resetODataFilter : function(evt) {
						if (!this.filterODataModel) {
							this.filterODataModel = new sap.ui.model.json.JSONModel();
						}
						this.filterData = {
							"orderStatus" : "",
							"customerId" : "",
							"agentCode" : "",
							"territoryManager" : ""

						};
						this.filterODataModel.setData(this.filterData);
						this.getView().setModel(this.filterODataModel, "fo");
						this.filterODataModel.refresh();
					},

					// resetODataFilter: function (evt) {
					//
					// if (!this.filterODataModel) {
					// this.filterODataModel = new
					// sap.ui.model.json.JSONModel();
					// this.filteringProps = this._getODataFilterPropsByUser();
					// }
					// this.filterData = {
					// "orderStatus":"",
					// "customerId":"",
					// "agentCode":"",
					// "territoryManager":"",
					// "areaManager":"",
					// // "items": [{
					// // "props": this.filteringProps,
					// // "selectedProp": "",
					// // "value": "",
					// // "id": 0
					// // }]
					// };
					// this.filterODataModel.setData(this.filterData);
					// this.getView().setModel(this.filterODataModel, "fo");
					// this.filterODataModel.refresh();
					// },

					// removeODataFilter: function () {
					// this.resetODataFilter();
					// this.odataFilterDialog.close();
					// var reqData = this.reqData;
					// model.collections.OrderList.getOrderList(reqData)
					// //forceReload
					// .then(_.bind(function (res) {
					// //console.log(res);
					// utils.Busy.hide();
					// this.orderListModel.setData(res);
					// this.orderListModel.refresh(true);
					//
					// }, this));
					//
					// },
					// onAddRowPress: function () {
					//
					// var inputProps = [];
					// for (var i = 0; i < this.filteringProps.length; i++) {
					// var found = false;
					// for (var j = 0; j < this.filterData.items.length; j++) {
					// if (this.filterData.items[j].selectedProp ===
					// this.filteringProps[i].key) {
					// found = true;
					// break;
					// }
					// }
					// if (!found) {
					// inputProps.push(this.filteringProps[i]);
					// }
					// }
					//
					// this.filterData.items.push({
					// "props": inputProps,
					// "selectedProp": "",
					// "value": "",
					// "id": this.filterData.items.length
					// });
					// this.filterODataModel.refresh();
					//
					// },
					// onDeleteRowPress: function (oEvent) {
					// var oList = oEvent.getSource();
					// var oItem = oEvent.getParameter("listItem");
					// var obj = oItem.getBindingContext("fo").getObject();
					// _.remove(this.filterData.items, {
					// "id": obj.id
					// });
					// this._prepareFilterProp();
					// this.filterODataModel.refresh();
					// },
					// _prepareFilterProp: function (evt) {
					// var assignedProps = [];
					// for (var i = 0; i < this.filterData.items.length; i++) {
					//
					// this._assignPropToList(this.filterData.items[i],
					// assignedProps);
					// assignedProps.push(this.filterData.items[i].selectedProp);
					// }
					// },
					// _assignPropToList: function (item, props) {
					// item.props = [];
					// for (var i = 0; i < this.filteringProps.length; i++) {
					// var found = false;
					// for (var j = 0; j < props.length; j++) {
					// if (props[j] === this.filteringProps[i].key) {
					// found = true;
					// break;
					// }
					// }
					// if (!found) {
					// item.props.push(this.filteringProps[i]);
					// }
					// }
					// },
					// onODataFilterCancelPress: function (evt) {
					// this.odataFilterDialog.close();
					// },

					onODataFilterCancelPress : function(evt) {
						this.filterData = {
							"orderStatus" : "",
							"territoryManager" : "",
							"agentCode" : "",
							"customerId" : ""
						}
						this.getView().getModel("fo").setData(this.filterData);
						this.odataFilterDialog.close();
					},
					// onODataFilterOKPress: function (evt) {
					// this.odataFilterDialog.close();
					// utils.Busy.show();
					// var reqData = {};
					//    
					// for (var i = 0; i < this.filterData.items.length; i++) {
					// if (!_.isEmpty(this.filterData.items[i].value) &&
					// !_.isEmpty(this.filterData.items[i].selectedProp)) {
					// reqData[this.filterData.items[i].selectedProp] =
					// this.filterData.items[i].value;
					// }
					// }
					// if(this.filterData.orderStatus)
					// {
					// reqData["orderStatus"] = this.filterData.orderStatus;
					// }
					// if (_.isEmpty(reqData)) {
					// reqData = this.reqData;
					// } else {
					// for (var prop in this.obblData) {
					// reqData[prop] = this.obblData[prop];
					// }
					// }
					// model.collections.OrderList.getOrderList(reqData)
					// //forceReload
					// .then(_.bind(function (res) {
					// //console.log(res);
					// utils.Busy.hide();
					// this.orderListModel.setData(res);
					// this.orderListModel.refresh(true);
					//
					// }, this));
					//
					// if (this.blockingReasonsDialog) {
					// this.blockingReasonsDialog.destroy();
					// }
					// },
					onODataFilterOKPress : function(evt) {
						this.odataFilterDialog.close();
						utils.Busy.show();
						// delete(this.filterData.items);
						var reqData = {};
						for ( var prop in this.filterData) {
							if (this.filterData[prop])
								reqData[prop] = this.filterData[prop]
						}

						if (_.isEmpty(reqData)) {
							reqData = this.reqData;
						} else {
							for ( var prop in this.obblData) {
								reqData[prop] = this.obblData[prop];
							}
						}
						utils.Busy.show();
						model.collections.OrderList.getOrderList(reqData) // forceReload
						.then(_.bind(function(res) {
							// console.log(res);
							// utils.Busy.hide();
							utils.Busy.hide();
							this.orderListModel.setData(res);
							this.orderListModel.refresh(true);

						}, this), _.bind(function(e) {
							sap.m.MessageToast.show(utils.Message.getError(e));
							utils.Busy.hide();
						}, this));

					},
					onRemoveODataFilter : function(evt) {
						this.odataFilterDialog.close();
						this.resetODataFilter();
						// this.filterData = {
						// "orderStatus":"",
						// "territoryManager":"",
						// "agentCode":"",
						// "customerId":""
						// }
						// this.getView().getModel("fo").refresh();

						var reqData = this.reqData;
						utils.Busy.show();
						model.collections.OrderList.getOrderList(reqData) // forceReload
						.then(_.bind(function(res) {
							// console.log(res);
							utils.Busy.hide();
							this.orderListModel.setData(res);
							this.orderListModel.refresh(true);

						}, this),_.bind(function(e) {
							utils.Busy.hide();
						}, this));
					},

					onTrackingPress : function(evt) {
						var src = evt.getSource();
						var selectedItem = src.getBindingContext("d")
								.getObject();

						// var idConsegna = selectedItem.orderId;
						this.trackingDialog = sap.ui.xmlfragment(
								"view.dialog.trackingDialog", this);
						var page = this.getView().byId("orderListPageId");
						page.addDependent(this.trackingDialog);
						// =======
						// var idConsegna = selectedItem.text;
						// if(!this.trackingDialog){
						// this.trackingDialog =
						// sap.ui.xmlfragment("view.dialog.trackingDialog",
						// this);
						// var page = this.getView().byId("orderListPageId");
						// page.addDependent(this.blockingReasonsDialog);
						// }
						// >>>>>>> 38d77f94cda465c2018279434a771b54e0d2309a
						// this.blockingReasonModel = new
						// sap.ui.model.json.JSONModel();
						// this.getView().setModel(this.blockingReasonModel,
						// "blockOrderReason");
						selectedItem.loadTrackingInfos() // forceReload
						.then(_.bind(function(res) {
							// console.log(res);
							this.getView().getModel("d").setData(res);

							this.trackingDialog.open();
							this._oPopover.close();
							// this.blockingReasonModel.setData(res.getModel());
							// this.blockingReasonModel.refresh(true);

						}, this));

						$(document).keyup(_.bind(this.keyUpFunc, this));

					},
					onTrackingDialogOk : function(evt) {
						this.trackingDialog.close();
					},
					onOpenAssignActionSheetPress : function(evt) {

						var position = evt.getSource().getBindingContext(
								"orderList").getObject();
						var order = new model.Order(position); //
						// var orderId = position.orderId;
						var oButton = evt.getSource();
						utils.Busy.show();
						var fSuccess = function(result) {
							utils.Busy.hide();
							if (!this._oPopover) {
								this._oPopover = sap.ui.xmlfragment(
										"view.dialog.deliveryPopover", this);

								this.getView().addDependent(this._oPopover);
								// this._oPopover.bindElement("/ProductCollection/0");
							}

							var deliveryModel = new sap.ui.model.json.JSONModel();
							var data = {};

							data.components = result;
							// for(var i = 0; i<result.length; i++)
							// {
							// (data.components).push({label:"Consegna",
							// text:result[i].deliveryId});
							// }

							// =======
							// data.components = [];
							// for(var i = 0; i<result.length; i++)
							// {
							// (data.components).push({label:"Consegna",
							// text:result[i].codConsegna});
							// }
							// >>>>>>> 38d77f94cda465c2018279434a771b54e0d2309a

							deliveryModel.setData(data);
							// deliveryModel.setProperty("/description","Lista
							// Consegne");
							this.getView().setModel(deliveryModel, "d");

							// delay because addDependent will do a async
							// rerendering and the actionSheet will immediately
							// close without it.

							// jQuery.sap.delayedCall(0, this, function () {
							if (!sap.ui.Device.system.phone) {
								this._oPopover.setPlacement("Left");
							}
							this._oPopover.openBy(oButton);
							// });
						};

						var fError = function(err) {
							utils.Busy.hide();
							sap.m.MessageToast
									.show(utils.Message.getError(err));
						};

						fSuccess = _.bind(fSuccess, this);
						fError = _.bind(fError, this);

						// model.collections.DeliveryList.loadDeliveryList(position.orderId)
						// .then(fSuccess, fError);
						order.loadDeliveries().then(fSuccess, fError);

					},

					onShippmentsInfoPress : function(evt) {
						var src = evt.getSource();
						var delivery = (src.getBindingContext("d") && src
								.getBindingContext("d").getObject()) ? src
								.getBindingContext("d").getObject() : this
								.getView().getModel("d").getData();
						var urlBase = "http://www.riellogroup.com/spedizioni/?token=";
						var url = urlBase + delivery.deliveryId;
						sap.m.URLHelper.redirect(url, true);
					},

					onShippmentPrintPress : function(evt) {
						var src = evt.getSource();
						var delivery = (src.getBindingContext("d") && src
								.getBindingContext("d").getObject()) ? src
								.getBindingContext("d").getObject() : this
								.getView().getModel("d").getData();
						utils.Busy.show();
						delivery
								.getShippmentPrintPdf()
								.then(
										_.bind(function(result) {

											utils.Busy.hide();

										}, this),
										_
												.bind(
														function(err) {
															utils.Busy.hide();
															sap.m.MessageToast
																	.show(model.i18n
																			.getText("ERROR_LOADING_SHIPPING_PDF"));
														}, this))

					},
					onInvoicePrintPress : function(evt) {
						var src = evt.getSource();
						var delivery = (src.getBindingContext("d") && src
								.getBindingContext("d").getObject()) ? src
								.getBindingContext("d").getObject() : this
								.getView().getModel("d").getData();
						utils.Busy.show();
						delivery
								.getInvoice()
								// invoices?
								.then(
										_
												.bind(
														function(result) {

															if (!delivery.invoices
																	|| delivery.invoices.length == 0) {
																utils.Busy
																		.hide();
																sap.m.MessageToast
																		.show(model.i18n
																				.getText("INVOICE_NOT_EXISTENT"));
															} else {
																delivery
																		.getInvoicePrintPdf(
																				delivery.invoices[0].Vbeln)
																		.then(
																				_
																						.bind(
																								function(
																										res) {
																									utils.Busy
																											.hide();
																								},
																								this),
																				_
																						.bind(
																								function(
																										err) {
																									sap.m.MessageToast
																											.show(model.i18n
																													.getText("ERROR_LOADING_INVOICE_PDF"));
																									utils.Busy
																											.hide();
																								},
																								this))
															}

														}, this),
										_
												.bind(
														function(err) {
															utils.Busy.hide();
															sap.m.MessageToast
																	.show(model.i18n
																			.getText("ERROR_LOADING_INVOICE"));
														}, this))

					},
					onFollowingFunctionsPress : function(evt) {
						var src = evt.getSource()
								.getBindingContext("orderList").getObject();
						model.persistence.Storage.session.save(
								"deliveriesOrder", src);
						this.router.navTo("noDataSplitDetail");
					},

					populateStatusSelect : function() {

						var defer = Q.defer();
						var workingUser = model.persistence.Storage.session
								.get("workingUser");
						this.orgData = {
							"Bukrs" : workingUser.organizationData.results[0].society,
							"Vkorg" : workingUser.organizationData.results[0].salesOrg,
							"Vtweg" : workingUser.organizationData.results[0].distributionChannel,
							"Spart" : workingUser.organizationData.results[0].division
						};

						var item = {
							"type" : "Stato",
							"namespace" : "orderStatus"
						};
						utils.Collections.getOdataSelect(item.type,
								this.orgData).then(_.bind(function(result) {
							this.getView().setModel(result, item.namespace);
							defer.resolve(result);
						}, this), _.bind(function(err) {
							defer.reject(err);
						}, this))
						return defer.promise;

					}

					,
					handleOdataFilterValueHelp : function(evt) {

						this.customerRequester = new model.CustomerRequester(
								this.odataReq);
						this.jobTypes = Object.freeze({
							"AG" : 1,
							"TM" : 2,
							"AM" : 3
						});
						// var sInputValue = evt.getSource().getValue();
						//
						// this.inputId = evt.getSource().getId();

						// choose dialog type Based on user type
						var src = evt.getSource().getId();
						var chooseDialog = function(result) {

							switch (src) {
							case "custFilterInput":
								this.getView().getModel("appStatus")
										.setProperty("/odataOut", "customer");
								this
										.getView()
										.getModel("appStatus")
										.setProperty("/odataIn", this.user.type);
								this.handleCustomerValueHelp(result);
								break;
							case "agFilterInput":
								this.getView().getModel("appStatus")
										.setProperty("/odataOut", "AG");
								this
										.getView()
										.getModel("appStatus")
										.setProperty("/odataIn", this.user.type);
								this.handleAgentValueHelp(result);
								break;
							case "tmFilterInput":
								this.getView().getModel("appStatus")
										.setProperty("/odataOut", "TM");
								this
										.getView()
										.getModel("appStatus")
										.setProperty("/odataIn", this.user.type);
								this.handleTMValueHelp(result);
								break;

							}

						}
						chooseDialog = _.bind(chooseDialog, this);

						this.customerRequester.loadCustomersList().then(
								chooseDialog);

						// create a filter for the binding
						// this._agentValueHelpDialog.getBinding("items").filter([new
						// Filter(
						// "Name",
						// sap.ui.model.FilterOperator.Contains, sInputValue
						// )]);

						// open value help dialog filtered by the input value

					},
					handleCustomerValueHelp : function(result) {

						if (this.user.type == "AG") {

							var custModel = new sap.ui.model.json.JSONModel();
							custModel.setData({
								"results" : result
							});
							this.getView().setModel(custModel, "cust");
							if (!this._CustomerValueHelpDialog) {
								this._CustomerValueHelpDialog = sap.ui
										.xmlfragment(
												"view.dialog.CustomerValueHelpDialog",
												this);
							}
							this.getView().addDependent(
									this._CustomerValueHelpDialog);
							this._CustomerValueHelpDialog.open();
							// sap.ui.getCore().byId("backBtn").setVisible(false);
						} else if (this.user.type == "TM") {
							var agModel = new sap.ui.model.json.JSONModel();
							agModel.setData({
								"results" : result
							});
							this.getView().setModel(agModel, "ag");
							if (!this.TM_CustomerValueHelpDialog) {
								this.TM_CustomerValueHelpDialog = sap.ui
										.xmlfragment(
												"view.dialog.TM_CustomerValueHelpDialog",
												this);
							}

							this.getView().addDependent(
									this.TM_CustomerValueHelpDialog);
							sap.ui.getCore().byId("OLbackBtn")
									.setVisible(false);
							var navCon = sap.ui.getCore().byId("OLTMAGNavCon");
							navCon.to("OLAGList");
							this.TM_CustomerValueHelpDialog.open();
						} else if (this.user.type == "AM") {
							var tmModel = new sap.ui.model.json.JSONModel();
							tmModel.setData({
								"results" : result
							});
							this.getView().setModel(tmModel, "tm");
							if (!this.TM_CustomerValueHelpDialog) {
								this.TM_CustomerValueHelpDialog = sap.ui
										.xmlfragment(
												"view.dialog.TM_CustomerValueHelpDialog",
												this);
							}
							this.getView().addDependent(
									this.TM_CustomerValueHelpDialog);
							sap.ui.getCore().byId("OLbackBtn")
									.setVisible(false);
							var navCon = sap.ui.getCore().byId("OLTMAGNavCon");
							navCon.to("OLTMList");
							this.TM_CustomerValueHelpDialog.open();
						}

					},
					handleAgentValueHelp : function(result) {
						if (this.user.type == "TM") {

							var agModel = new sap.ui.model.json.JSONModel();
							agModel.setData({
								"results" : result
							});
							this.getView().setModel(agModel, "ag");
							if (!this._AgentValueHelpDialog) {
								this._AgentValueHelpDialog = sap.ui
										.xmlfragment(
												"view.dialog.AgentValueHelpDialog",
												this);
							}
							this.getView().addDependent(
									this._AgentValueHelpDialog);
							this._AgentValueHelpDialog.open();
							// sap.ui.getCore().byId("backBtn").setVisible(false);
						} else if (this.user.type == "AM") {
							var tmModel = new sap.ui.model.json.JSONModel();
							tmModel.setData({
								"results" : result
							});
							this.getView().setModel(tmModel, "tm");
							if (!this.TM_CustomerValueHelpDialog) {
								this.TM_CustomerValueHelpDialog = sap.ui
										.xmlfragment(
												"view.dialog.TM_CustomerValueHelpDialog",
												this);
							}
							this.getView().addDependent(
									this.TM_CustomerValueHelpDialog);
							var navCon = sap.ui.getCore().byId("OLTMAGNavCon");
							sap.ui.getCore().byId("OLbackBtn")
									.setVisible(false);
							navCon.to("OLTMList");
							this.TM_CustomerValueHelpDialog.open();
						}
					},
					handleTMValueHelp : function(result) {
						if (this.user.type == "AM") {

							var tmModel = new sap.ui.model.json.JSONModel();
							tmModel.setData({
								"results" : result
							});
							this.getView().setModel(tmModel, "tm");
							if (!this._TMValueHelpDialog) {
								this._TMValueHelpDialog = sap.ui.xmlfragment(
										"view.dialog.TMValueHelpDialog", this);
							}
							this.getView()
									.addDependent(this._TMValueHelpDialog);
							this._TMValueHelpDialog.open();
							// sap.ui.getCore().byId("backBtn").setVisible(false);
						} else {
							// console.log("Impossible: TM can be seen only by
							// AM");
						}
					},

					handleAgentSearch : function(oEvent) {

						var sValue = oEvent.getParameter("value") ? oEvent
								.getParameter("value") : oEvent
								.getParameter("newValue");
						var params = [ "registry/agentName",
								"registry/agentCode" ];

						var oBinding = oEvent.getSource().getBinding("items") ? oEvent
								.getSource().getBinding("items")
								: sap.ui.getCore().byId("agentList")
										.getBinding("items");

						oBinding.filter([]);

						if (sValue) {
							var filters = this.assignFilters(oBinding.oList[0],
									sValue, params);
							// var filters = [];
							// filters.push(new
							// sap.ui.model.Filter("registry/agentName",
							// sap.ui.model.FilterOperator.Contains, sValue));
							// filters.push(new
							// sap.ui.model.Filter("registry/agentCode",
							// sap.ui.model.FilterOperator.Contains, sValue));

							oBinding.filter(filters);
						}

					},
					handleCustomerSearch : function(oEvent) {

						var sValue = oEvent.getParameter("value") ? oEvent
								.getParameter("value") : oEvent
								.getParameter("newValue");
						var params = [ "registry/customerName", "registry/id" ];

						var oBinding = oEvent.getSource().getBinding("items") ? oEvent
								.getSource().getBinding("items")
								: sap.ui.getCore().byId("customerList")
										.getBinding("items");
						oBinding.filter([]);

						if (sValue) {
							var filters = this.assignFilters(oBinding.oList[0],
									sValue, params);
							// var filters = [];
							// filters.push(new
							// sap.ui.model.Filter("registry/agentName",
							// sap.ui.model.FilterOperator.Contains, sValue));
							// filters.push(new
							// sap.ui.model.Filter("registry/agentCode",
							// sap.ui.model.FilterOperator.Contains, sValue));

							oBinding.filter(filters);
						}

					},
					handleTMSearch : function(oEvent) {

						var sValue = oEvent.getParameter("value") ? oEvent
								.getParameter("value") : oEvent
								.getParameter("newValue");
						var params = [ "registry/territoryManager",
								"registry/territoryManagerText" ];

						var oBinding = oEvent.getSource().getBinding("items") ? oEvent
								.getSource().getBinding("items")
								: sap.ui.getCore().byId("customerList")
										.getBinding("items");
						oBinding.filter([]);

						if (sValue) {
							var filters = this.assignFilters(oBinding.oList[0],
									sValue, params);
							// var filters = [];
							// filters.push(new
							// sap.ui.model.Filter("registry/agentName",
							// sap.ui.model.FilterOperator.Contains, sValue));
							// filters.push(new
							// sap.ui.model.Filter("registry/agentCode",
							// sap.ui.model.FilterOperator.Contains, sValue));

							oBinding.filter(filters);
						}

					},
					handleAgentDialogClose : function(oEvent) {

						var src = oEvent;
						if (oEvent.getParameters
								&& oEvent.getParameter("selectedItem")) {

							src = oEvent.getParameter("selectedItem")
						}
						var selectedAgent = src.getBindingContext("ag")
								.getObject();
						// this.filterData.areaManager =
						// selectedAgent.registry.areaManager;
						// this.filterData.territoryManager =
						// selectedAgent.registry.territoryManager;
						this.filterData.agentCode = selectedAgent.registry.agentCode;
						this.getView().getModel("fo").refresh();
						if (oEvent.getParameters
								&& oEvent.getParameter("selectedItem")) {
							oEvent.getSource().getBinding("items").filter([]);
						} else {
							var list = sap.ui.getCore().byId("agentList");
							list.getBinding("items").filter([]);
						}
						// this.refreshView(this.customer);
					},
					handleCustomerDialogClose : function(oEvent) {

						var src = oEvent;
						if (oEvent.getParameters
								&& oEvent.getParameter("selectedItem")) {
							src = oEvent.getParameter("selectedItem");
						}
						var selectedCustomer = src.getBindingContext("cust")
								.getObject();
						// this.filterData.territoryManager =
						// selectedCustomer.registry.territoryManager;
						// this.filterData.agentCode =
						// selectedCustomer.registry.agentCode;
						// this.filterData.agentName =
						// selectedCustomer.registry.agentName;
						this.filterData.customerId = selectedCustomer.registry.id;
						this.getView().getModel("fo").refresh();
						// if(oEvent.getParameters &&
						// oEvent.getParameter("selectedItem"))
						// {
						// oEvent.getSource().getBinding("items").filter([]);
						// }
						// else {
						// var list = sap.ui.getCore().byId("customerList");
						// list.getBinding("items").filter([]);
						// }

						// this.refreshView(this.customer);
					},
					handleTMDialogClose : function(oEvent) {

						var src = oEvent;
						if (oEvent.getParameters
								&& oEvent.getParameter("selectedItem")) {
							src = oEvent.getParameter("selectedItem");
						}
						var selectedTM = src.getBindingContext("tm")
								.getObject();
						// this.filterData.territoryManager =
						// selectedCustomer.registry.territoryManager;
						// this.filterData.agentCode =
						// selectedCustomer.registry.agentCode;
						// this.filterData.agentName =
						// selectedCustomer.registry.agentName;
						this.filterData.territoryManager = selectedTM.registry.territoryManager;
						this.getView().getModel("fo").refresh();
						// if(oEvent.getParameters &&
						// oEvent.getParameter("selectedItem"))
						// {
						// oEvent.getSource().getBinding("items").filter([]);
						// }
						// else {
						// var list = sap.ui.getCore().byId("customerList");
						// list.getBinding("items").filter([]);
						// }

						// this.refreshView(this.customer);
					},
					onTMPress : function(evt) {
						var src = evt.getSource();
						var tmSel = src.getBindingContext("tm").getObject();

						var odataOut = this.getView().getModel("appStatus")
								.getProperty("/odataOut");
						var odataIn = this.getView().getModel("appStatus")
								.getProperty("/odataIn");
						if (this.jobTypes[odataIn] <= "TM") {
							sap.m.MessageToast.show("Error loading TM");
							// console.log("Error: You can't be here, you are a
							// lower profile user");
							this.TM_CustomerValueHelpDialog.close();
							return;
						}
						if (odataOut == "TM") {
							this.filterData.territoryManager = tmSel.registry.territoryManager;
							this.getView().getModel("fo").refresh();
							this.TM_CustomerValueHelpDialog.close();
							return;
						}

						var loadAgents = function(result) {
							var agentModel = new sap.ui.model.json.JSONModel();
							agentModel.setData({
								"results" : result
							});
							this.getView().setModel(agentModel, "ag");
							var navCon = sap.ui.getCore().byId("OLTMAGNavCon");
							navCon.to("OLAGList");
							sap.ui.getCore().byId("OLbackBtn").setVisible(true);
						}
						loadAgents = _.bind(loadAgents, this);
						this.customerRequester
								.setTerritoryManager(tmSel.registry.territoryManager);
						this.customerRequester.loadCustomersList().then(
								loadAgents);
					},
					// onAgentSelected:function(evt)
					// {
					// var src = evt.getSource();
					//	
					// this.customerRequester.removeTerritoryManager();
					// this._TMValueHelpDialog.close();
					// var navCon = sap.ui.getCore().byId("OLTMAGNavCon");
					// navCon.to("OLTMList");
					// this.handleAgentDialogClose(src);
					// },
					onAgentPress : function(evt) {
						var src = evt.getSource();
						var agSel = src.getBindingContext("ag").getObject();
						var odataOut = this.getView().getModel("appStatus")
								.getProperty("/odataOut");
						var odataIn = this.getView().getModel("appStatus")
								.getProperty("/odataIn");
						if (this.jobTypes[odataIn] <= "AG") {
							sap.m.MessageToast.show("Error loading AG");
							// console.log("Error: You can't be here, you are a
							// lower profile user");
							this.TM_CustomerValueHelpDialog.close();
							return;
						}
						if (odataOut == "AG") {
							this.filterData.agentCode = agSel.registry.agentCode;
							this.getView().getModel("fo").refresh();
							this.TM_CustomerValueHelpDialog.close();
							return;
						}

						var loadCustomers = function(result) {
							var custModel = new sap.ui.model.json.JSONModel();
							custModel.setData({
								"results" : result
							});
							this.getView().setModel(custModel, "cust");
							var navCon = sap.ui.getCore().byId("OLTMAGNavCon");
							navCon.to("OLCustList");
							sap.ui.getCore().byId("OLbackBtn").setVisible(true);
						}
						loadCustomers = _.bind(loadCustomers, this);
						this.customerRequester
								.setAgent(agSel.registry.agentCode);
						this.customerRequester.loadCustomersList().then(
								loadCustomers);

					},
					onCustomerPress : function(evt) {
						var src = evt.getSource();
						var custSel = src.getBindingContext("cust").getObject();
						this.filterData.customerId = custSel.registry.id;
						this.getView().getModel("fo").refresh();
						this.TM_CustomerValueHelpDialog.close();
						// var odataOut =
						// this.getView().getModel("appStatus").getProperty("/odataOut");
						// var odataIn =
						// this.getView().getModel("appStatus").getProperty("/odataIn");
					},
					_adjustODataRequest : function(itemType) {
						var navCon = sap.ui.getCore().byId("OLTMAGNavCon");
						switch (itemType) {
						case "TM":
							if (this.user.type == "AM") {
								this.customerRequester.removeTerritoryManager();
								sap.ui.getCore().byId("OLbackBtn").setVisible(
										false);
								navCon.to("OLTMList");
							} else {
								// console.log("It's not possible");

							}
							break;

						case "AG":
							if (this.user.type == "AM"
									|| this.user.type == "TM") {
								this.customerRequester.removeAgent();
								navCon.to("OLAGList");
								if (this.user.type == "TM")
									sap.ui.getCore().byId("OLbackBtn")
											.setVisible(false);

							} else {
								// console.log("It's not possible");

							}
							break;
						default:
							// console.log("Where are you?");
							break;

						}
					},
					onBackOdataFilterPress : function(evt) {
						var src = evt.getSource();
						var navCon = sap.ui.getCore().byId("OLTMAGNavCon");
						var actualPage = navCon.getCurrentPage().getId();
						switch (actualPage) {
						case "OLAGList":

							this._adjustODataRequest("TM");

							break;
						case "OLTMList":
							// console.log("how does it possible?");
							break;
						case "OLCustList":
							this._adjustODataRequest("AG");

							break;

						}

					},
					onSuggestOdataFilterDialogClose : function(evt) {
						this.TM_CustomerValueHelpDialog.close();
					},
					onPODPress : function(evt) {
						var deliveryId = this.getView().getModel("d").getData().deliveryId;
						this.getView().setBusy(true);
						var fSuccess = function(res) {
							this.getView().setBusy(false);
							if (res.EUrl) {
								sap.m.URLHelper.redirect(res.EUrl, true);
							} else {
								sap.m.MessageToast.show(model.i18n
										.getText("errorLoadingPODUrl"));
							}
						};
						fSuccess = _.bind(fSuccess, this);

						var fError = function(err) {
							this.getView().setBusy(false);
							sap.m.MessageToast
									.show(utils.Message.getError(err));

						}
						fError = _.bind(fError, this);

						model.odata.chiamateOdata.getPODUrl(deliveryId,
								fSuccess, fError);
					}

				// onTMAgentDialogClose:function(evt)
				// {
				// this.customerRequester.removeTerritoryManager();
				// this._TMValueHelpDialog.close();
				// var navCon = sap.ui.getCore().byId("TMAGNavCon");
				// navCon.to("TMList");
				// },

				});
