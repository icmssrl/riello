jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Shippments");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.praticheAnomale.ResoConNC");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.praticheAnomale.Reso");
jQuery.sap.require("view.praticheAnomale.AbstractReso");

view.praticheAnomale.AbstractReso.extend("view.praticheAnomale.ResoConNC", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.praticheAnomale.AbstractReso.prototype.onInit.apply(this, arguments);

//        this.productsAPModel = new sap.ui.model.json.JSONModel();
//        this.getView().setModel(this.productsAPModel, "productsAP");

	},


	handleRouteMatched: function (evt) {

		//view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "praticheAnomale.ResoConNC"){
			return;
		}
		view.praticheAnomale.AbstractReso.prototype.handleRouteMatched.apply(this, arguments);
//        if(this.createRenderDialog){
//            this.createRenderDialog.destroy();
//        }
//
//		var id = evt.getParameters().arguments.shippmentId;
//
//        this.user = model.persistence.Storage.session.get("workingUser");
//        this.userModel = new sap.ui.model.json.JSONModel(this.user);
//        this.getView().setModel(this.userModel, "userModel");
//
//        this.resoConNC = new model.praticheAnomale.ResoConNC();
//				this.order = new model.Order(model.persistence.Storage.session.get("deliveriesOrder"));
//        var currentShippment = new model.Delivery(model.persistence.Storage.session.get("selectedDelivery"));
//       
//				
//
//        
//
//        this.refreshView(this.resoConNC);
//
//        this.populateSelect();
//
//		this.resoConNC.getProductsAP(id)
//		.then(
//
//			_.bind(function(result)
//			{
//
//				this.productsAPModel.setData(result);
//				this.productsAPModel.refresh(true);
//
//			}, this)
//		);
//
//
//        //console.log(this.resoConNC);


	},

//	refreshView: function (data) {
//        this.getView().setModel(data.getModel(), "resoConNC");
//    },

//	populateSelect: function () {
//    var pToSelArr = [
//      {
//        "type": "renderReason",
//        "namespace": "renderReason"
//      },
//      {
//        "type": "appointmentType",
//        "namespace": "appointmentType"
//      },
//      {
//        "type": "noteTypes",
//        "namespace": "noteTypes"
//      }
//    ];
//
//    _.map(pToSelArr, _.bind(function (item) {
//      utils.Collections.getModel(item.type)
//        .then(_.bind(function (result) {
//          this.getView().setModel(result, item.namespace);
//
//        }, this))
//    }, this));
//  },

    onRenderQuantityChange: function(evt){
        var source = evt.getSource();
    },

    navBack: function () {
        this.router.myNavBack();
    },
    
//    createPracticeSummaryPress: function(evt){
//        var source = evt.getSource();
//        var that = this;
//        sap.m.MessageBox.show(
//            model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE"), {
//              icon: sap.m.MessageBox.Icon.QUESTION,
//              title: model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE_TITLE"),
//              actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
//              onClose: _.bind(function(oAction)
//                { / * create render * /
//                    //console.log(oAction);
//                    if(oAction==="YES")
//                    {
//                    	that.reso.sendToSAP()
//                    	.then(_.bind(function(res){
//                    		sap.m.MessageToast.show(res);
//                    	}, that), 
//                    	_.bind(function(err){
//                    		sap.m.MessageToast.show(utils.Message.getError(err));
//                    	}, that))
////                        that.createRenderDialog = sap.ui.xmlfragment("view.fragment.renderConfirmPage", that);
////                        var page = that.getView().byId("ResoConNCPage");
////                        page.addDependent(that.createRenderDialog);
////                        that.createRenderDialog.open();
//
//                    }
//                    else{
//                        return;
//                    }
//                }, that)
//            }
//        );
//
//
//    },

    onCreatePracticeDialogConfirm: function(evt){

        /* TODO: Send data to SAP*/

        sap.m.MessageToast.show(model.i18n.getText("AP_PRACTICE_CREATED"));

        if(this.createRenderDialog){
            this.createRenderDialog.close();
            this.createRenderDialog.destroy();
        }
        setTimeout(this.router.navTo("noDataSplitDetail"), 4000);
    },



    onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    },
    
   
    
//onChangeQuantityToRender: function (evt) {
////	var parameters= {};
////	parameters.VbelnCr = "";
////	parameters.Bukrs = this.order.society;
////	parameters.Vkorg = this.order.salesOrg;
////	parameters.Vtweg = this.order.distrCh;
////	parameters.Spart = this.order.division;
////	parameters.Vkbur = this.order.areaManager;
////	parameters.Vkgrp = this.order.territoryManager;
////	parameters.Cdage = this.order.agentCode;
//	
//    var src = evt.getSource();
//    var position = evt.getSource().getBindingContext("reso").getObject();
//    
//    var value = src.getValue();
////    var selectedItem = src.getBindingContext("productsAP").getObject();
//    if(!this.matricolaDialog)
//		{
//    	this.matricolaDialog = sap.ui.xmlfragment("view.dialog.showMatricolaDialog", this);
//		}
//    var page = this.getView().byId("ResoConNCPage");
//    page.addDependent(this.matricolaDialog);
//    
// // Multi-select if required
//	
//	this.matricolaDialog.setMultiSelect(true);
//	if(this.matricolaDialog.getBinding("items"))
//		this.matricolaDialog.getBinding("items").filter([]);
//
//	
//	if(!this.regModel)
//	{
//		this.regModel = new sap.ui.model.json.JSONModel();
//		this.getView().setModel(this.regModel, "reg");
//	}
//	this.regModel.setData(position);
//	// toggle compact style
//	jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.matricolaDialog);
//	this.matricolaDialog.open();
//
//  
////    this.loadPractice(parameters) //forceReload
////      .then(_.bind(function (res) {
////        //console.log(res);
////
////        this.matricolaDialog.open();
////
////
////      }, this));
//
//    $(document).keyup(_.bind(this.keyUpFunc, this));
//  },

      keyUpFunc: function (e) {
        if (e.keyCode == 27) {
          // codice per il pulsante escape per evitare che lo user chiuda il dialog via ESC

          if (this.matricolaDialog) {
            this.matricolaDialog.close();
          }
          $(document).off("keyup");
   
        }
      },

      
      
      loadPractice: function(param)
      {
      	var defer = Q.defer();
      	
      	var fSuccess = function(result)
      	{
      		var reso = model.persistence.Serializer.reso.fromSAP(result);
      		defer.resolve(reso);
      	}
      	fSuccess = _.bind(fSuccess,this);
      	
      	var fError = function(err)
      	{
      		var error = utils.Message.getError(err);
      		defer.reject(error);
      	}
      	fError = _.bind(fError, this);
      	
      	
      	model.odata.chiamateOdata.getPracticeByParameters(param, fSuccess, fError, true);
      	
      	return defer.promise;
      },
      
//      onMatricolaDialogOk: function (evt) {
//          if (this.matricolaDialog) {
//            this.matricolaDialog.close();
//           
//          }
//
//        },
        

 
//		handleSearchOnMatricolaDialog: function(oEvent) {
//			var sValue = oEvent.getParameter("value");
//			var oFilter = new Filter("Name", sap.ui.model.FilterOperator.Contains, sValue);
//			var oBinding = oEvent.getSource().getBinding("items");
//			oBinding.filter([oFilter]);
//		},
 
//		onMatricolaDialogClose: function(oEvent) {
//			var aContexts = oEvent.getParameter("selectedContexts");
//			if (aContexts.length) {
//				MessageToast.show("You have chosen " + aContexts.map(function(oContext) { return oContext.getObject().Name; }).join(", "));
//			}
//			oEvent.getSource().getBinding("items").filter([]);
//		}

});
