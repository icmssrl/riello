jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Shippments");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.praticheAnomale.RichiestaRicambi");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");

view.abstract.AbstractController.extend("view.praticheAnomale.RichiestaRicambi", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.productsAPModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.productsAPModel, "productsAP");

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "praticheAnomale.RichiestaRicambi"){
			return;
		}

		var id = evt.getParameters().arguments.shippmentId;

        this.user = model.persistence.Storage.session.get("user");
        this.userModel = new sap.ui.model.json.JSONModel(this.user);
        this.getView().setModel(this.userModel, "userModel");

        this.richiestaRicambi = new model.praticheAnomale.RichiestaRicambi();

        this.delivery = model.persistence.Storage.session.get("selectedDelivery");
				this.order = model.persistence.Storage.session.get("deliveriesOrder");
        // var initParameter = {
        //       orderId : currentShippment.orderId ? currentShippment.orderId : "",
        //       shippmentId : id ? id : "",
        //       committerName : this.user.fullName ? this.user.fullName : "",
        //       payerName : this.user.fullName ? this.user.fullName : "",
        //       receiverName : this.user.fullName ? this.user.fullName : "",
        //       areaManager : this.user.organizationData[0].areaManager ? this.user.organizationData[0].areaManager : "",
        //       territoryManager : this.user.organizationData[0].territoryManager ? this.user.organizationData[0].territoryManager : "",
        //       billNumber : currentShippment.billNumber ? currentShippment.billNumber : "",
        //       billDate : currentShippment.billDate ? currentShippment.billDate : "",
        //       commercialInvoice : currentShippment.commercialInvoice ? currentShippment.commercialInvoice : "",
        //       accountingInvoice : currentShippment.accountingInvoice ? currentShippment.accountingInvoice : "",
        //       invoiceDate : currentShippment.invoiceDate ? currentShippment.invoiceDate : ""
        //
        // };

        this.richiestaRicambi.initialize(this.delivery, this.order);

        this.refreshView(this.richiestaRicambi);

        this.populateSelect();

        this.richiestaRicambi.getProductsAP(id)
		.then(

			_.bind(function(result)
			{

				this.productsAPModel.setData(result);
				this.productsAPModel.refresh(true);

			}, this)
		);


        //console.log(this.richiestaRicambi);



	},

	refreshView: function (data) {
        this.getView().setModel(data.getModel(), "richiestaRicambi");
    },

	populateSelect: function () {
    var pToSelArr = [
      {
        "type": "renderReason",
        "namespace": "renderReason"
      },
      {
        "type": "appointmentType",
        "namespace": "appointmentType"
      },
      {
        "type": "noteTypes",
        "namespace": "noteTypes"
      }
    ];

    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.getModel(item.type)
        .then(_.bind(function (result) {
          this.getView().setModel(result, item.namespace);

        }, this))
    }, this));
  },

    onRenderQuantityChange: function(evt){
        var source = evt.getSource();
    },

    navBack: function () {
        this.router.myNavBack();
    },

    createPracticeSummaryPress: function(evt){
        var source = evt.getSource();
        var that = this;
        sap.m.MessageBox.show(
            model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE"), {
              icon: sap.m.MessageBox.Icon.QUESTION,
              title: model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE_TITLE"),
              actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
              onClose: function(oAction)
                { / * create render * /
                    //console.log(oAction);
                    if(oAction==="YES"){



                        that.createRenderDialog = sap.ui.xmlfragment("view.fragment.renderConfirmPage", that);
                        var page = that.getView().byId("richiestaRicambiPage");
                        page.addDependent(that.createRenderDialog);
                        that.createRenderDialog.open();

                    }else{
                        return;
                    }
                }
            }
        );


    },

    onCreatePracticeDialogConfirm: function(evt){

        /* TODO: Send data to SAP*/

        sap.m.MessageToast.show(model.i18n.getText("AP_PRACTICE_CREATED"));

        if(this.createRenderDialog){
            this.createRenderDialog.close();
            this.createRenderDialog.destroy();
        }
        setTimeout(this.router.navTo("noDataSplitDetail"), 4000);
    },



    onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    }


});
