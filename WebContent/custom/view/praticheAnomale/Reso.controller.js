jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Shippments");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.praticheAnomale.ResoConNC");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.praticheAnomale.Reso");
jQuery.sap.require("view.praticheAnomale.AbstractReso");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Shippments");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.praticheAnomale.ResoConNC");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.praticheAnomale.Reso");
//jQuery.sap.declare("view.praticheAnomale.AbstractReso");

view.abstract.AbstractController.extend("view.praticheAnomale.Reso", {
	
	
	
	_propInputMap : {
			"renderReason": "orderReasonCombo",
			"costs":"costsCombo",
			"appointment":"appointmentCombo"
	},

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

//        this.productsAPModel = new sap.ui.model.json.JSONModel();
//        this.getView().setModel(this.productsAPModel, "productsAP");

	},


	handleRouteMatched: function (evt) {

		
		
		var routeName = evt.getParameter("name");

		if ( routeName !== "praticheAnomale.ResoConNC" && routeName !== "praticheAnomale.ResoSenzaNC"  && routeName!=="praticheAnomale.MancanzaMerce" && routeName !== "praticheAnomale.RichiestaRicambi"){
			return;
		}
		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var id = evt.getParameters().arguments.shippmentId;
		this.type = evt.getParameters().arguments.type;
		
		
		if(this.type == "RR")
			this.type="";
		
		this.uiModel.setProperty("/reqCost", false);
		this.uiModel.refresh();
        this.user = model.persistence.Storage.session.get("workingUser");
        this.userModel = new sap.ui.model.json.JSONModel(this.user);
        this.getView().setModel(this.userModel, "userModel");

        //this.resoConNC = new model.praticheAnomale.ResoConNC();
        this.reso = new model.praticheAnomale.Reso();
		this.order = new model.Order(model.persistence.Storage.session.get("deliveriesOrder"));
        this.delivery = new model.Delivery(model.persistence.Storage.session.get("selectedDelivery"));
        
//		Q.all([this.reso.load(), this.populateSelect()])
//		.spread(_.bind(function(resoRes, popSel){
//			this.refreshView(this.reso);
//			
//		}, this))
        utils.Busy.show()
        this.reso.initializeByDelivery(this.delivery, this.order, this.type)
        .then(_.bind(function(res){
//        	this.refreshView(this.reso);
        	utils.Busy.hide()
        	this.refreshView();
        	
        }, this), _.bind(function(err){
        	
        	 sap.m.MessageBox.error(err, {
        		          icon: sap.m.MessageBox.Icon.ERROR,
        		          title: model.i18n.getText("DOSSIER_ERROR"),
        		          actions: [sap.m.MessageBox.Action.OK],
        		          onClose: _.bind(function(oAction) {
        		        	  utils.Busy.hide();
        		        	  this.router.myNavBack();
        		        	  
        		          },this)
        		      }
        		    );
        }, this));
	},

	refreshView: function (data) {
		this.populateSelect();
        this.refreshResoModel();
        this.populateOrderReasonSelect()
        .then(_.bind(function(res){
        	this.onOrderReasonChange();
        	this.refreshResoModel();
        }, this),(_.bind(function(){
        	this.refreshResoModel();
        }, this)));
    },
    refreshResoModel:function()
    {
    	if(!this.resoModel)
		{
			this.resoModel=new sap.ui.model.json.JSONModel();
		}
		this.resoModel.setData(this.reso);
		this.getView().setModel(this.resoModel, "reso");
    },

    populateOrderReasonSelect:function()
    {
    	var defer = Q.defer();
    	var item = {"type":"Augru_CR", "namespace":"renderReason"};
    	
		this.orgData = {
										"Bukrs": this.user.organizationData.results[0].society,
										"Vkorg": this.user.organizationData.results[0].salesOrg,
										"Vtweg": this.user.organizationData.results[0].distributionChannel,
										"Spart": this.user.organizationData.results[0].division
									};
    	utils.Collections.getOdataSelect(item.type, this.orgData)
    	.then(_.bind
    		(function(result)
    	{
    		this.getView().setModel(result, item.namespace);
    		//this._populateCostsSelect();
    		defer.resolve();
    	}, this),
    	(_.bind
        		(function(err)
        	{
        		sap.m.MessageToast.show(utils.Message.getError(err));
        		defer.reject();
        	}, this)));
    	return defer.promise;
    	
    },
    onOrderReasonChange:function(evt)
    {
    	this.onRequiredFieldChange(evt);
    	var value = "";
    	var selectedReason={};
    	var orderReasons = this.getView().getModel("renderReason").getData().results;
    	if(this.reso.renderReason)
    	{
    		value=this.reso.renderReason;
    		selectedReason = _.find(orderReasons, {Augru:value});
    	}
    	else 
    	{
    		
    		if(orderReasons && orderReasons.length>0)
    		{
    			if(orderReasons.length == 1)
    			{
    				value = orderReasons[0].Augru;
    			}
    			else
    			{
    				selectedReason= _.find(orderReasons, {Zdefault:'X'});
    				if(selectedReason)
    				{
    					value = selectedReason.Augru;
    				}
    			}
    			
    		}
    	}
    	if(value)
    	{
    		this.reso.renderReason= value;
    	}
    	if(selectedReason)
    	{
    		this.populateCostsSelect(selectedReason);
    		
    	}
    },
    populateCostsSelect:function(orderReasonData)
    {
    	var costsModel = new sap.ui.model.json.JSONModel();
    	
    	var data = {"results":[]};
    	
    	
    	var setItemData = function(key, descr)
    	{
    		var obj = {"Cost": key, "Txt": descr};
    		data.results.push(obj);
    	}
    	setItemData = _.bind(setItemData, this)
    	
    	for(var prop in orderReasonData)
    	{
    		if(orderReasonData[prop] == 'X')
    		{
    			switch(prop){
    			case "CostA":
    				setItemData("A", orderReasonData.CostADesc);
    				break;
    			case "CostC":
    				setItemData("C", orderReasonData.CostCDesc);
    				break;
    			case "CostS":
    				setItemData("S", orderReasonData.CostSDesc);
    				break;
    			default:
    				break;
    			}
    		}
    	}
    	costsModel.setData(data);
    	if(data.results.length == 0)
    	{
    		this.uiModel.setProperty("/reqCost", false);
    		
    	}
    	else
    	{
    		this.uiModel.setProperty("/reqCost", true);
    	}
    	this.uiModel.refresh();
    	this.getView().setModel(costsModel, "costs");
    	
    },
    
	populateSelect: function () {
    var pToSelArr = [
     
      {
        "type": "Sdabw_CR",
        "namespace": "appointmentType"
      },
//      {
//        "type": "noteTypes",
//        "namespace": "noteTypes"
//      }
    ];

    var workingUser = model.persistence.Storage.session.get("workingUser");
	this.orgData = {
									"Bukrs": workingUser.organizationData.results[0].society,
									"Vkorg": workingUser.organizationData.results[0].salesOrg,
									"Vtweg": workingUser.organizationData.results[0].distributionChannel,
									"Spart": workingUser.organizationData.results[0].division
								};
	_.map(pToSelArr, _.bind(function(item)
	{
		utils.Collections.getOdataSelect(item.type, this.orgData)
		.then(_.bind
			(function(result)
		{
			this.getView().setModel(result, item.namespace);
	
		}, this))
	}, this));
  },

    onRenderQuantityChange: function(evt){
        var source = evt.getSource();
    },

    navBack: function () {
        this.router.myNavBack();
    },

  
    onCreatePracticeDialogConfirm: function(evt){

        /*  Send data to SAP*/

       
        if(this.createRenderDialog){
            this.createRenderDialog.close();
           
        }
        this.router.myNavBack();
       
    },



    onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    },
    
   
    
    onChangeQuantityToRender: function (evt) {
//    	var parameters= {};
//    	parameters.VbelnCr = "";
//    	parameters.Bukrs = this.order.society;
//    	parameters.Vkorg = this.order.salesOrg;
//    	parameters.Vtweg = this.order.distrCh;
//    	parameters.Spart = this.order.division;
//    	parameters.Vkbur = this.order.areaManager;
//    	parameters.Vkgrp = this.order.territoryManager;
//    	parameters.Cdage = this.order.agentCode;
    	
        var src = evt.getSource();
        var position = evt.getSource().getBindingContext("reso").getObject();
        
        var value = src.getValue();
        
        var resetRegistriesSelection = function(position)
		{
			if(position && position.registries && position.registries.length > 0)
			{
				for(var i = 0; i< position.registries.length ; i++)
					{
						if(position.registries[i].selected)
							{
							position.registries[i].selected = false;
							}
					}
			}
		}
		resetRegistriesSelection = _ .bind(resetRegistriesSelection, this); //It resets only the data values
		resetRegistriesSelection(position);
		
        if(parseInt(value) > parseInt(position.quantity - position.renderedQuantity))
        {
        	sap.m.MessageBox.alert(model.i18n.getText("INPUT_QUANTITY_GREATER_THAN_AVAILABLE_QUANTITY"), {title:model.i18n.getText("INPUT_ERROR")});
        	position.quantityToRender = 0;
        	this.getView().getModel("reso").refresh();
        	return;
        }
        if(value == 0)
        {
        	return;
        }
        	
        if(!position.regFlag)
        	return;
        
        //var value = src.getValue();
//        var selectedItem = src.getBindingContext("productsAP").getObject();
        if(!this.matricolaDialog)
  		{
        	this.matricolaDialog = sap.ui.xmlfragment("view.dialog.showMatricolaDialog", this);
  		}
//        var page = this.getView().byId("ResoConNCPage");
        var page = this.getView().byId("ResoPage");
        page.addDependent(this.matricolaDialog);
        
     // Multi-select if required
		
//		this.matricolaDialog.setMultiSelect(true);
//		this.matricolaDialog.setRememberSelections(true);
        
		// clear the old search filter
        
//		if(this.matricolaDialog.getBinding("items"))
//			this.matricolaDialog.getBinding("items").filter([]);
		if(!this.regModel)
    	{
    		this.regModel = new sap.ui.model.json.JSONModel();
    		this.getView().setModel(this.regModel, "reg");
    	}
		this.regModel.setData(position);
		
		
		// toggle compact style
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.matricolaDialog);
		
		this.matricolaDialog.open();
		var src = sap.ui.getCore().byId("registriesList");
		var oBinding = src.getBinding("items");
		oBinding.filter([]);
      
//        this.loadPractice(parameters) //forceReload
//          .then(_.bind(function (res) {
//            //console.log(res);
//
//            this.matricolaDialog.open();
//    
//
//          }, this));

        $(document).keyup(_.bind(this.keyUpFunc, this));
      },

      keyUpFunc: function (e) {
        if (e.keyCode == 27) {
          // codice per il pulsante escape per evitare che lo user chiuda il dialog via ESC

          if (this.matricolaDialog) {
            this.matricolaDialog.close();
          }
          $(document).off("keyup");
   
        }
      },

      
      
      loadPractice: function(param)
      {
      	var defer = Q.defer();
      	
      	var fSuccess = function(result)
      	{
      		var reso = model.persistence.Serializer.reso.fromSAP(result);
      		defer.resolve(reso);
      	}
      	fSuccess = _.bind(fSuccess,this);
      	
      	var fError = function(err)
      	{
      		var error = utils.Message.getError(err);
      		defer.reject(error);
      	}
      	fError = _.bind(fError, this);
      	
      	
      	model.odata.chiamateOdata.getPracticeByParameters(param, fSuccess, fError, true);
      	
      	return defer.promise;
      },
      
      onMatricolaDialogOK: function (evt) {
    	  
    	  var list = sap.ui.getCore().byId("registriesList");
//    	  var contexts = evt.getParameter('selectedContexts');
    	  var contexts = list.getSelectedContexts();
    	  if(contexts && contexts.length > 0)
    	{
    		  var position = _.find(this.reso.items, {positionId:contexts[0].getObject().positionId});
    		  position.quantityToRender = parseInt(position.quantityToRender);
    		  var msg= model.i18n.getText("SELECTED_REGISTRIES_NUM")+" "+contexts.length+"\n";
    		  msg += model.i18n.getText("REQUESTED_REGISTRIES_NUM")+ " "+position.quantityToRender;
    		  if(contexts.length < position.quantityToRender)
    			 {
    			  
    			  sap.m.MessageBox.alert(msg, {title: model.i18n.getText("INPUT_ERROR")});
    			  
    			  this.getView().getModel("reso").refresh();
    			  return;
    			 }
    		  else if(contexts.length > position.quantityToRender)
 			 {
 			  sap.m.MessageBox.alert(msg, {title: model.i18n.getText("INPUT_ERROR")});
 			  
 			  this.getView().getModel("reso").refresh();
 			  return;
 			 }
    		  else
    		  {
    			  for(var i = 0; i < contexts.length; i++ )
     			 {
     			  	contexts[i].getObject().selected= true;
     			 }
    			  if (this.matricolaDialog) {
    		            this.matricolaDialog.close();
    		           
    		          }
    		  }
    		 
    	}
    	  else
    		  {
    		  	sap.m.MessageBox.alert(model.i18n.getText("ERROR:NO_ITEM_SELECTED"));
    		  }
    	  
          

        },
        onMatricolaDialogClose:function(evt)
        {
        	 var list = sap.ui.getCore().byId("registriesList");

       	  	var contexts = list.getSelectedContexts();
       	  	if(contexts && contexts.length > 0)
       	  	{
       		  var position = _.find(this.reso.items, {positionId:contexts[0].getObject().positionId});
	       		for(var i = 0; i < contexts.length; i++ )
				 {
				  	contexts[i].getObject().selected= false;
				 }
       	  	}
       	  	this.matricolaDialog.close();
        },
        

 
        handleSearchOnMatricolaDialog: function(oEvent) {
			var sValue = oEvent.getParameter("newValue");
			var oFilter = new sap.ui.model.Filter("regNum", sap.ui.model.FilterOperator.Contains, sValue);
			var src = sap.ui.getCore().byId("registriesList");
			var oBinding = src.getBinding("items");
			oBinding.filter([oFilter]);
		},
		
		
		createPracticeSummaryPress: function(evt){
	        var source = evt.getSource();
	        
	        if(!this.checkFlag())
	        {
	        	sap.m.MessageToast.show(model.i18n.getText("CONFIRM_INTEGRITY_FIRST"));
	        	return;
	        }
	        var res = this._checkPracticeValue();
	        if(!res.success)
	        {
	        	var msgTitle=model.i18n.getText("MISSING_INPUT_VALUES");
	        	var msg = model.i18n.getText("MISSING_VALUES")+"\n";
	        	for(var i = 0; res.errors && res.errors.length && i < res.errors.length ; i++)
	        	{
	        		var txt = res.errors[i].toUpperCase()
	        		msg += model.i18n.getText(txt)+"\n";
	        	}
	        	sap.m.MessageBox.error(msg, {
	        		title:msgTitle, 
	        		onClose: _.bind(function(){
	        			return;
	        		}, this)});
	        	
	        }
	        else
	        {
	            var that = this;
		        sap.m.MessageBox.show(
		            model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE"), {
		              icon: sap.m.MessageBox.Icon.QUESTION,
		              title: model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE_TITLE"),
		              actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
		              onClose: _.bind(function(oAction)
		                { / * create render * /
		                    //console.log(oAction);
		                    if(oAction==="YES")
		                    {
		                    	utils.Busy.show();
		                    	that.reso.sendToSAP()
		                    	.then(_.bind(function(res){
		                    		utils.Busy.hide();
		                    		
		                    		if(!this.createRenderDialog)
		                    		{
		                    			this.createRenderDialog = sap.ui.xmlfragment("view.fragment.renderConfirmPage", this);
		                    	       
		                    		}
		                    		 var page = this.getView().byId("ResoPage");
		                    	      page.addDependent(this.createRenderDialog);
		                    	      this.getView().getModel("reso").refresh();
		                    	      this.createRenderDialog.open();
//		                    		jQuery.sap.delayedCall(2000, that, _.bind(function(){
//		                    			that.router.myNavBack();
//		                    		}, that))
		                    		
		                    	}, that), 
		                    	_.bind(function(err){
		                    		utils.Busy.hide();
		                    		sap.m.MessageToast.show(utils.Message.getError(err));
		                    	}, that))
//		                        that.createRenderDialog = sap.ui.xmlfragment("view.fragment.renderConfirmPage", that);
//		                        var page = that.getView().byId("ResoConNCPage");
//		                        page.addDependent(that.createRenderDialog);
//		                        that.createRenderDialog.open();

		                    }
		                    else{
		                        return;
		                    }
		                }, that)
		            }
		        );
	        }
	 


	    },
	    
	    _checkPracticeValue:function()
	    {
	    	var propErrors = [];
	    	var reqCost = this.uiModel.getProperty("/reqCost");
	    	var result = this.reso.checkPractice(reqCost);
	    	if(!result.success)
	    	{
	    		propErrors = result.errors;
	    		for(var i =0; propErrors && propErrors.length && propErrors.length > 0 && i< propErrors.length; i++)
	    		{
	    			var srcId = this._propInputMap[propErrors[i]];
	    			if(srcId)
	    			{
	    				var src = this.getView().byId(srcId);
		    			src.setValueState("Error");
	    			}
	    		}
	    	}
	    	return result;
	    	
	    	
	    	
	    },
	    openUploadFileDialog: function (evt) {
	        if (!this.uploadDialog) {
	          this.uploadDialog = sap.ui.xmlfragment("view.dialog.uploadFile", this);
	          var page = this.getView().byId("ResoPage");
	          page.addDependent(this.uploadDialog);
	        }
	        this.uploadDialog.open();
	        var fileUploader = sap.ui.getCore().byId("fileUploader");
	        fileUploader.clear();

	        $(document).keyup(_.bind(this.keyUpFunc, this));

	      },
	      onCloseUploadDialogPress: function (evt) {
	    	    if (this.uploadDialog) {
	    	      this.uploadDialog.close();
	    	      //this.uploadDialog.destroy();
	    	    }

	    	  },
    	  handleUploadComplete: function (oEvent) {
    		    var sResponse = oEvent.getParameter("response");
    		    var status = oEvent.getParameter("status").toString();
    		    var dataFile = "";
    		    if (status && status[0]==="2") {
    		      if (sResponse) {
    		        var response = oEvent.getParameter("response");
    		        var indexIGuid = response.indexOf("IGuid=");
    		        var indexIGuid2 = response.indexOf(",");
    		        var IGuid = response.substring(indexIGuid, indexIGuid2).split("'")[1];
    		        var indexIDrunr = response.indexOf("IDrunr=");
    		        var indexIDrunr2 = response.indexOf(")");
    		        var IDrunr = response.substring(indexIDrunr, indexIDrunr2).split("'")[1];
    		        this.reso.guid = IGuid;
    		        dataFile = { 
    		          name: this.fileName,
    		          posId: IDrunr,
    		          type: this.fileType
    		        };
    		        this.reso.attachments.push(dataFile);
    		      }
    		      sap.m.MessageToast.show(model.i18n._getLocaleText("UPLOAD_OK"));
    		    } else {
    		      dataFile = {};
    		      sap.m.MessageToast.show(model.i18n._getLocaleText("UPLOAD_KO"));
    		    }
    		    this.getView().getModel("reso").refresh();
    		    // this.refreshView(this.order);

    		  },

    		  handleUploadPress: function (oEvent) {
    		    // testModel = new sap.ui.model.json.JSONModel();
    		    // this.getView().setModel(testModel, "test");

    		    var uploadData = {};
    		    var fileUploader = sap.ui.getCore().byId("fileUploader");
    		    if (!fileUploader.getValue()) {
    		      sap.m.MessageToast.show(model.i18n._getLocaleText("CHOOSE_FILE_FIRST"));
    		      return;
    		    } else {
    		      fileUploader.removeAllHeaderParameters();
    		      fileUploader.setSendXHR(true);

    		      var modello = model.odata.chiamateOdata.getOdataModel();
    		      modello.refreshSecurityToken();
    		      var header = new sap.ui.unified.FileUploaderParameter({
    		        name: "x-csrf-token",
    		        value: modello.getHeaders()['x-csrf-token']
    		      });

    		      fileUploader.addHeaderParameter(header);
    		      // this.fileName = fileUploader.oFileUpload.files[0].name;
    		      this.fileType = fileUploader.oFileUpload.files[0].type;

    		      if (this.reso.guid) {
    		        var guid = "|" + this.reso.guid;
    		        var headerSlug = new sap.ui.unified.FileUploaderParameter({
    		          name: "slug",
    		          value: this.fileName + "|" + this.fileType + guid
    		        });
    		      } else {
    		        var headerSlug = new sap.ui.unified.FileUploaderParameter({
    		          name: "slug",
    		          value: this.fileName + "|" + this.fileType
    		        });
    		      }

    		      fileUploader.addHeaderParameter(headerSlug);

    		      fileUploader.upload();
    		    }

    		    if (this.uploadDialog) {
    		      this.uploadDialog.close();
    		      //this.uploadDialog.destroy();
    		    }
    		  },


    		//** per gestire i tipi di file
    		  handleTypeMissmatch: function (oEvent) {
    		    var aFileTypes = oEvent.getSource().getFileType();
    		    jQuery.each(aFileTypes, function (key, value) {
    		      aFileTypes[key] = "*." + value
    		    });
    		    var sSupportedFileTypes = aFileTypes.join(", ");
    		    sap.m.MessageToast.show("The file type *." + oEvent.getParameter("fileType") +
    		      " is not supported. Choose one of the following types: " +
    		      sSupportedFileTypes);
    		  },

    		  handleValueChange: function (oEvent) {

    		    //console.log(oEvent.getSource());
    		    var name = oEvent.getParameter("newValue");
    		    if(name.length> 50)
    		    {
    		      if(!this.fileNameDialog)
    		      {
    		        this.fileNameDialog = sap.ui.xmlfragment("view.dialog.fileNameDialog", this);
    		      }
    		      this.fileModel = new sap.ui.model.json.JSONModel();
    		      this.fileModel.setData({"name":name});
    		      this.getView().setModel(this.fileModel, "fn");
    		      var page = this.getView().byId("ResoPage");
    		      page.addDependent(this.fileNameDialog);
    		      this.fileNameDialog.open();
    		    }
    		    else
    		    {
    		      this.fileName = name;
    		      sap.m.MessageToast.show(model.i18n._getLocaleText("PRESS_UPLOAD_FILE") + " " +
    		        oEvent.getParameter("newValue") + "'");
    		    }


    		  },

    		  onFileNameOK:function(evt)
    		  {
    		    var name = this.getView().getModel("fn").getProperty("/name");
    		    if(name.length <= 50)
    		    {
    		      this.fileName = name;
    		      sap.m.MessageToast.show(model.i18n._getLocaleText("PRESS_UPLOAD_FILE") + " " +
    		        evt.getParameter("newValue") + "'");
    		      this.fileNameDialog.close();
    		    }
    		    else {
    		      sap.m.MessageToast.show(model.i18n._getLocaleText("REDEFINE_FILE_NAME"));
    		    }
    		  },
    		  onFileNameClose:function(evt)
    		  {
    		    this.fileNameDialog.close();
    		    this.uploadDialog.close();
    		  },
		    onRequiredFieldChange:function(evt)
		    {
		    	var source =evt.getSource();
		    	if(source.getValue() !== "")
		    	{
		    		source.setValueState("None");
		    	}
		    	else
		    	{
		    		source.setValueState("Error");
		    	}
		    },
		    checkFlag:function(evt)
		    {
		    	return this.reso.confirmIntegrityFlag;
		    }

 
//		onMatricolaDialogClose: function(oEvent) {
//			var aContexts = oEvent.getParameter("selectedContexts");
//			if (aContexts.length) {
//				MessageToast.show("You have chosen " + aContexts.map(function(oContext) { return oContext.getObject().Name; }).join(", "));
//			}
//			oEvent.getSource().getBinding("items").filter([]);
//		}

});


