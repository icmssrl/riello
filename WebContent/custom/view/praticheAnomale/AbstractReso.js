jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Shippments");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.praticheAnomale.ResoConNC");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.praticheAnomale.Reso");
jQuery.sap.declare("view.praticheAnomale.AbstractReso");

view.abstract.AbstractController.extend("view.praticheAnomale.AbstractReso", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

//        this.productsAPModel = new sap.ui.model.json.JSONModel();
//        this.getView().setModel(this.productsAPModel, "productsAP");

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		

        
		var id = evt.getParameters().arguments.shippmentId;
		this.type = evt.getParameters().arguments.type;
		
        this.user = model.persistence.Storage.session.get("workingUser");
        this.userModel = new sap.ui.model.json.JSONModel(this.user);
        this.getView().setModel(this.userModel, "userModel");

        //this.resoConNC = new model.praticheAnomale.ResoConNC();
        this.reso = new model.praticheAnomale.Reso();
		this.order = new model.Order(model.persistence.Storage.session.get("deliveriesOrder"));
        this.delivery = new model.Delivery(model.persistence.Storage.session.get("selectedDelivery"));
        
//		Q.all([this.reso.load(), this.populateSelect()])
//		.spread(_.bind(function(resoRes, popSel){
//			this.refreshView(this.reso);
//			
//		}, this))
        this.reso.initializeByDelivery(this.delivery, this.order, this.type)
        .then(_.bind(function(res){
//        	this.refreshView(this.reso);
        	
        	this.refreshView();
        	
        }, this));
	},

	refreshView: function (data) {
		this.populateSelect();
        this.refreshResoModel();
        this.populateOrderReasonSelect()
        .then(_.bind(function(res){
        	this.onOrderReasonChange();
        	this.refreshResoModel();
        }, this),(_.bind(function(){
        	this.refreshResoModel();
        }, this)));
    },
    refreshResoModel:function()
    {
    	if(!this.resoModel)
		{
			this.resoModel=new sap.ui.model.json.JSONModel();
		}
		this.resoModel.setData(this.reso);
		this.getView().setModel(this.resoModel, "reso");
    },

    populateOrderReasonSelect:function()
    {
    	var defer = Q.defer();
    	var item = {"type":"Augru_CR", "namespace":"renderReason"};
    	
		this.orgData = {
										"Bukrs": this.user.organizationData.results[0].society,
										"Vkorg": this.user.organizationData.results[0].salesOrg,
										"Vtweg": this.user.organizationData.results[0].distributionChannel,
										"Spart": this.user.organizationData.results[0].division
									};
    	utils.Collections.getOdataSelect(item.type, this.orgData)
    	.then(_.bind
    		(function(result)
    	{
    		this.getView().setModel(result, item.namespace);
    		//this._populateCostsSelect();
    		defer.resolve();
    	}, this),
    	(_.bind
        		(function(err)
        	{
        		sap.m.MessageToast.show(utils.Message.getError(err));
        		defer.reject();
        	}, this)));
    	return defer.promise;
    	
    },
    onOrderReasonChange:function(evt)
    {
    	
    	var value = "";
    	var selectedReason={};
    	var orderReasons = this.getView().getModel("renderReason").getData().results;
    	if(this.reso.renderReason)
    	{
    		value=this.reso.renderReason;
    		selectedReason = _.find(orderReasons, {Augru:value});
    	}
    	else 
    	{
    		
    		if(orderReasons && orderReasons.length>0)
    		{
    			if(orderReasons.length == 1)
    			{
    				value = orderReasons[0].Augru;
    			}
    			else
    			{
    				selectedReason= _.find(orderReasons, {Zdefault:'X'});
    				if(selectedReason)
    				{
    					value = selectedReason.Augru;
    				}
    			}
    			
    		}
    	}
    	if(value)
    	{
    		this.reso.renderReason= value;
    	}
    	if(selectedReason)
    	{
    		this.populateCostsSelect(selectedReason);
    		
    	}
    },
    populateCostsSelect:function(orderReasonData)
    {
    	var costsModel = new sap.ui.model.json.JSONModel();
    	
    	var data = {"results":[]};
    	
    	var setItemData = function(key, descr)
    	{
    		var obj = {"Cost": key, "Txt": descr};
    		data.results.push(obj);
    	}
    	setItemData = _.bind(setItemData, this)
    	
    	for(var prop in orderReasonData)
    	{
    		if(orderReasonData[prop] == 'X')
    		{
    			switch(prop){
    			case "CostA":
    				setItemData("A", orderReasonData.CostADesc);
    				break;
    			case "CostC":
    				setItemData("C", orderReasonData.CostCDesc);
    				break;
    			case "CostS":
    				setItemData("S", orderReasonData.CostSDesc);
    				break;
    			default:
    				break;
    			}
    		}
    	}
    	costsModel.setData(data);
    	this.getView().setModel(costsModel, "costs");
    },
    
	populateSelect: function () {
    var pToSelArr = [
     
      {
        "type": "Sdabw",
        "namespace": "appointmentType"
      },
//      {
//        "type": "noteTypes",
//        "namespace": "noteTypes"
//      }
    ];

    var workingUser = model.persistence.Storage.session.get("workingUser");
	this.orgData = {
									"Bukrs": workingUser.organizationData.results[0].society,
									"Vkorg": workingUser.organizationData.results[0].salesOrg,
									"Vtweg": workingUser.organizationData.results[0].distributionChannel,
									"Spart": workingUser.organizationData.results[0].division
								};
	_.map(pToSelArr, _.bind(function(item)
	{
		utils.Collections.getOdataSelect(item.type, this.orgData)
		.then(_.bind
			(function(result)
		{
			this.getView().setModel(result, item.namespace);
	
		}, this))
	}, this));
  },

    onRenderQuantityChange: function(evt){
        var source = evt.getSource();
    },

    navBack: function () {
        this.router.myNavBack();
    },

   


    

    onCreatePracticeDialogConfirm: function(evt){

        /* TODO: Send data to SAP*/

        sap.m.MessageToast.show(model.i18n.getText("AP_PRACTICE_CREATED"));

        if(this.createRenderDialog){
            this.createRenderDialog.close();
            this.createRenderDialog.destroy();
        }
        setTimeout(this.router.navTo("noDataSplitDetail"), 4000);
    },



    onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    },
    
   
    
    onChangeQuantityToRender: function (evt) {
//    	var parameters= {};
//    	parameters.VbelnCr = "";
//    	parameters.Bukrs = this.order.society;
//    	parameters.Vkorg = this.order.salesOrg;
//    	parameters.Vtweg = this.order.distrCh;
//    	parameters.Spart = this.order.division;
//    	parameters.Vkbur = this.order.areaManager;
//    	parameters.Vkgrp = this.order.territoryManager;
//    	parameters.Cdage = this.order.agentCode;
    	
        var src = evt.getSource();
        var position = evt.getSource().getBindingContext("reso").getObject();
        
        var value = src.getValue();
        if(value > position.quantity)
        {
        	sap.m.MessageBox.alert(model.i18n.getText("INPUT_QUANTITY_GREATER_THAN_ORIGINAL_QUANTITY"), {title:model.i18n.getText("INPUT_ERROR")});
        	position.quantityToRender = 0;
        	this.getView().getModel("reso").refresh();
        	return;
        }
        
        if(!position.regFlag)
        	return;
        
        var value = src.getValue();
//        var selectedItem = src.getBindingContext("productsAP").getObject();
        if(!this.matricolaDialog)
  		{
        	this.matricolaDialog = sap.ui.xmlfragment("view.dialog.showMatricolaDialog", this);
  		}
//        var page = this.getView().byId("ResoConNCPage");
        var page = this.getView().byId("ResoPage");
        page.addDependent(this.matricolaDialog);
        
     // Multi-select if required
		
//		this.matricolaDialog.setMultiSelect(true);
//		this.matricolaDialog.setRememberSelections(true);
        
		// clear the old search filter
        
//		if(this.matricolaDialog.getBinding("items"))
//			this.matricolaDialog.getBinding("items").filter([]);
		if(!this.regModel)
    	{
    		this.regModel = new sap.ui.model.json.JSONModel();
    		this.getView().setModel(this.regModel, "reg");
    	}
		this.regModel.setData(position);
		
		var resetRegistriesSelection = function()
		{
			if(position && position.registries && position.registries.length > 0)
			{
				for(var i = 0; i< position.registries.length ; i++)
					{
						if(position.registries[i].selected)
							{
							position.registries[i].selected = false;
							}
					}
			}
		}
		resetRegistriesSelection = _ .bind(resetRegistriesSelection, this); //It resets only the data values
		resetRegistriesSelection();
		// toggle compact style
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.matricolaDialog);
		
		this.matricolaDialog.open();
		var src = sap.ui.getCore().byId("registriesList");
		var oBinding = src.getBinding("items");
		oBinding.filter([]);
      
//        this.loadPractice(parameters) //forceReload
//          .then(_.bind(function (res) {
//            //console.log(res);
//
//            this.matricolaDialog.open();
//    
//
//          }, this));

        $(document).keyup(_.bind(this.keyUpFunc, this));
      },

      keyUpFunc: function (e) {
        if (e.keyCode == 27) {
          // codice per il pulsante escape per evitare che lo user chiuda il dialog via ESC

          if (this.matricolaDialog) {
            this.matricolaDialog.close();
          }
          $(document).off("keyup");
   
        }
      },

      
      
      loadPractice: function(param)
      {
      	var defer = Q.defer();
      	
      	var fSuccess = function(result)
      	{
      		var reso = model.persistence.Serializer.reso.fromSAP(result);
      		defer.resolve(reso);
      	}
      	fSuccess = _.bind(fSuccess,this);
      	
      	var fError = function(err)
      	{
      		var error = utils.Message.getError(err);
      		defer.reject(error);
      	}
      	fError = _.bind(fError, this);
      	
      	
      	model.odata.chiamateOdata.getPracticeByParameters(param, fSuccess, fError, true);
      	
      	return defer.promise;
      },
      
      onMatricolaDialogOK: function (evt) {
    	  
    	  var list = sap.ui.getCore().byId("registriesList");
//    	  var contexts = evt.getParameter('selectedContexts');
    	  var contexts = list.getSelectedContexts();
    	  if(contexts && contexts.length > 0)
    	{
    		  var position = _.find(this.reso.items, {positionId:contexts[0].getObject().positionId});
    		  
    		  var msg= model.i18n.getText("SELECTED_REGISTRIES_NUM")+contexts.length+"\n";
    		  msg += model.i18n.getText("REQUESTED_REGISTRIES_NUM")+position.quantityToRender;
    		  if(contexts.length < position.quantityToRender)
    			 {
    			  
    			  sap.m.MessageBox.alert(msg, {title: model.i18n.getText("INPUT_ERROR")});
    			  position.quantityToRender=0;
    			  this.getView().getModel("reso").refresh();
    			  return;
    			 }
    		  else if(contexts.length > position.quantityToRender)
 			 {
 			  sap.m.MessageBox.alert(msg, {title: model.i18n.getText("INPUT_ERROR")});
 			  position.quantityToRender=0;
 			  this.getView().getModel("reso").refresh();
 			  return;
 			 }
    		  else
    		  {
    			  for(var i = 0; i < contexts.length; i++ )
     			 {
     			  	contexts[i].getObject().selected= true;
     			 }
    			  if (this.matricolaDialog) {
    		            this.matricolaDialog.close();
    		           
    		          }
    		  }
    		 
    	}
    	  
          

        },
        onMatricolaDialogClose:function(evt)
        {
        	 var list = sap.ui.getCore().byId("registriesList");

       	  	var contexts = list.getSelectedContexts();
       	  	if(contexts && contexts.length > 0)
       	  	{
       		  var position = _.find(this.reso.items, {positionId:contexts[0].getObject().positionId});
	       		for(var i = 0; i < contexts.length; i++ )
				 {
				  	contexts[i].getObject().selected= false;
				 }
       	  	}
       	  	this.matricolaDialog.close();
        },
        

 
        handleSearchOnMatricolaDialog: function(oEvent) {
			var sValue = oEvent.getParameter("newValue");
			var oFilter = new sap.ui.model.Filter("regNum", sap.ui.model.FilterOperator.Contains, sValue);
			var src = sap.ui.getCore().byId("registriesList");
			var oBinding = src.getBinding("items");
			oBinding.filter([oFilter]);
		},
		
		
		createPracticeSummaryPress: function(evt){
	        var source = evt.getSource();
	        var that = this;
	        sap.m.MessageBox.show(
	            model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE"), {
	              icon: sap.m.MessageBox.Icon.QUESTION,
	              title: model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE_TITLE"),
	              actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
	              onClose: _.bind(function(oAction)
	                { / * create render * /
	                    //console.log(oAction);
	                    if(oAction==="YES")
	                    {
	                    	utils.Busy.show();
	                    	that.reso.sendToSAP()
	                    	.then(_.bind(function(res){
	                    		utils.Busy.hide();
	                    		
	                    		sap.m.MessageToast.show(res);
	                    		
	                    		this.router.myNavBack();
	                    	}, that), 
	                    	_.bind(function(err){
	                    		utils.Busy.hide();
	                    		sap.m.MessageToast.show(utils.Message.getError(err));
	                    	}, that))
//	                        that.createRenderDialog = sap.ui.xmlfragment("view.fragment.renderConfirmPage", that);
//	                        var page = that.getView().byId("ResoConNCPage");
//	                        page.addDependent(that.createRenderDialog);
//	                        that.createRenderDialog.open();

	                    }
	                    else{
	                        return;
	                    }
	                }, that)
	            }
	        );


	    },
 
//		onMatricolaDialogClose: function(oEvent) {
//			var aContexts = oEvent.getParameter("selectedContexts");
//			if (aContexts.length) {
//				MessageToast.show("You have chosen " + aContexts.map(function(oContext) { return oContext.getObject().Name; }).join(", "));
//			}
//			oEvent.getSource().getBinding("items").filter([]);
//		}

});
