jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Shippments");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.praticheAnomale.ResoConNC");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.praticheAnomale.Reso");

view.abstract.AbstractController.extend("view.praticheAnomale.ResoDetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "resoDetail"){
			return;
		}

        
		//var id = evt.getParameters().arguments.resoId;

        this.user = model.persistence.Storage.session.get("workingUser");
        this.userModel = new sap.ui.model.json.JSONModel(this.user);
        this.getView().setModel(this.userModel, "userModel");

        //this.resoConNC = new model.praticheAnomale.ResoConNC();
        this.reso = new model.praticheAnomale.Reso();
        
		this.order = new model.Order(model.persistence.Storage.session.get("deliveriesOrder"));
        this.delivery = new model.Delivery(model.persistence.Storage.session.get("selectedDelivery"));
        var practice = model.persistence.Storage.session.get("currentPractice");
        if(practice)
        	this.reso.initialize(practice);
//		Q.all([this.reso.load(), this.populateSelect()])
//		.spread(_.bind(function(resoRes, popSel){
//			this.refreshView(this.reso);
//			
//		}, this))
        var loadReso = _.bind(this.reso.load, this);
        var loadLinks = _.bind(this.reso.loadLinks, this);
        
        utils.Busy.show();
        this.reso.load(this.order, this.delivery)
        .then(_.bind(function(res){
        	
//        	this.refreshView(this.reso);
        	this.refreshView();
        	this.reso.loadLinks()
        	.then(_.bind(function(res){
        		this.refreshView();
        		utils.Busy.hide();
        	}, this),
        	_.bind(function(err){
        		sap.m.MessageToast.show(utils.Message.getError(err));
        		utils.Busy.hide();
        		
        	}, this))
        	
        }, this),
        _.bind(function(err){
        	
       	 sap.m.MessageBox.error(err, {
       		          icon: sap.m.MessageBox.Icon.ERROR,
       		          title: model.i18n.getText("READING_PRACTICE_ERROR"),
       		          actions: [sap.m.MessageBox.Action.OK],
       		          onClose: _.bind(function(oAction) {
       		        	  utils.Busy.hide();
       		        	  this.router.myNavBack();
       		        	  
       		          },this)
       		      }
       		    );
       }, this));
        
     

 


	},
	refreshView:function()
	{
		if(!this.resoModel)
			{
				this.resoModel=new sap.ui.model.json.JSONModel();
			}
		this.resoModel.setData(this.reso);
		this.getView().setModel(this.resoModel, "reso");
	},

//	refreshView: function (data) {
//        this.getView().setModel(data.getModel(), "resoConNC");
//    },

//	populateSelect: function () {
//		
//	    var workingUser = model.persistence.Storage.session.get("workingUser");
//	    this.orgData = {
//	      "Bukrs": workingUser.organizationData.results[0].society,
//	      "Vkorg": workingUser.organizationData.results[0].salesOrg,
//	      "Vtweg": workingUser.organizationData.results[0].distributionChannel,
//	      "Spart": workingUser.organizationData.results[0].division
//	    };
//    var pToSelArr = [
//      {
//        "type": "renderReason",
//        "namespace": "renderReason"
//      },
//      {
//        "type": "appointmentType",
//        "namespace": "appointmentType"
//      },
//      {
//        "type": "noteTypes",
//        "namespace": "noteTypes"
//      }
//    ];
//
//    _.map(pToSelArr, _.bind(function (item) {
//      utils.Collections.getModel(item.type)
//        .then(_.bind(function (result) {
//          this.getView().setModel(result, item.namespace);
//
//        }, this))
//    }, this));
//  },

//    onRenderQuantityChange: function(evt){
//        var source = evt.getSource();
//    },

    navBack: function () {
        this.router.myNavBack();
    },
    onPositionPress : function(evt)
    {
    	var src = evt.getSource().getBindingContext("reso").getObject();
    	if(!this.regDialog)
    		{
    			this.regDialog = sap.ui.xmlfragment("view.dialog.registryNumDialog", this);
    		}
    	var page = this.getView().byId("resoPage");
    	
    	if(!this.regModel)
    	{
    		this.regModel = new sap.ui.model.json.JSONModel();
    		this.getView().setModel(this.regModel, "reg");
    	}
    	this.regModel.setData(src);
    	   // this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog", this);
    	    
    	    //this.regDialog.setModel(discountModel, "r");
    	    
    	    page.addDependent(this.regDialog);
    	    this.regDialog.open();
    },
    onRegistryDialogOk:function(evt)
    {
    	this.regDialog.close();
    },

//    createPracticeSummaryPress: function(evt){
//        var source = evt.getSource();
//        var that = this;
//        sap.m.MessageBox.show(
//            model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE"), {
//              icon: sap.m.MessageBox.Icon.QUESTION,
//              title: model.i18n._getLocaleText("CONFIRM_RENDERING_CREATE_TITLE"),
//              actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
//              onClose: function(oAction)
//                { / * create render * /
//                    //console.log(oAction);
//                    if(oAction==="YES"){
//
//                        that.createRenderDialog = sap.ui.xmlfragment("view.fragment.renderConfirmPage", that);
//                        var page = that.getView().byId("ResoConNCPage");
//                        page.addDependent(that.createRenderDialog);
//                        that.createRenderDialog.open();
//
//                    }else{
//                        return;
//                    }
//                }
//            }
//        );
//
//
//    },

//    onCreatePracticeDialogConfirm: function(evt){
//
//        /* TODO: Send data to SAP*/
//
//        sap.m.MessageToast.show(model.i18n.getText("AP_PRACTICE_CREATED"));
//
//        if(this.createRenderDialog){
//            this.createRenderDialog.close();
//            this.createRenderDialog.destroy();
//        }
//        setTimeout(this.router.navTo("noDataSplitDetail"), 4000);
//    },



    onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    }


});
