jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Shippments");
jQuery.sap.require("model.collections.OrderList");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");

view.abstract.AbstractMasterController.extend("view.shippment.ShippmentsList", {

  onExit: function () {



	},



  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.uiModel.setProperty("/searchProperty", ["deliveryId", "destinationName"]);

    //this.shippmentListModel = new sap.ui.model.json.JSONModel();
    //this.getView().setModel(this.shippmentListModel, "ship");
    this.orderModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.orderModel, "order");
  },


  handleRouteMatched: function (evt) {

    var name = evt.getParameter("name");

    //    if(sap.ui.Device.system.tablet && sap.ui.Device.orientation.portrait)
    //    {
    //      sap.ui.getCore().byId("splitApp-Master").setVisible(true);
    //    }

    //       if(this.dataOrgDialog){
    //            this.dataOrgDialog.destroy();
    //        }

    if (name !== "shippment.shippmentsList" && name !== "shippment.shippmentDetail" && name !== "noDataSplitDetail") {
      return;
    }


    this.order = model.persistence.Storage.session.get("deliveriesOrder") ? new model.Order(model.persistence.Storage.session.get("deliveriesOrder")) : null;
    this.orderModel.setData(this.order);
    // model.persistence.Storage.session.remove("deliveriesOrder");

    //** spostato fuori il caricamento

    this.orderListModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.orderListModel, "orderList");
    this.reqData = this.getUserInfo();
    model.collections.OrderList.getOrderList(this.reqData) //forceReload
      .then(_.bind(function (res) {
        //console.log(res);
        this.orderListModel.setData(res);
        this.orderListModel.refresh();
      }, this));
    //**
    if (!this.order) {

      //     this.orderListModel = new sap.ui.model.json.JSONModel();
      //     this.getView().setModel(this.orderListModel, "orderList");
      //
      //     if (!!sessionStorage.getItem("customerSession") && sessionStorage.getItem("customerSession")==="true"){
      //       var customerData = model.persistence.Storage.session.get("currentCustomer");
      //       if(!!customerData && !!customerData.registry.id){
      //           var customerIdValue = customerData.registry.id;
      //             model.collections.OrderList.loadOrdersByCustomerId(customerIdValue) //forceReload
      //             .then(_.bind(function(res){
      //                 //console.log(res);
      //
      //                 this.orderListModel.setData(res);
      //                 this.orderListModel.refresh(true);
      //
      //                 this.dataOrgDialog = sap.ui.xmlfragment("view.dialog.selectOrder", this);
      //                 var page = this.getView().byId("shippmentsListPageId");
      //                 page.addDependent(this.dataOrgDialog);
      //                 this.dataOrgDialog.open();
      //
      //                 $(document).keyup(_.bind(this.keyUpFunc, this));
      //
      //               }, this));
      //
      //       }
      //
      //
      //       }else{
      //       model.collections.OrderList.loadOrders(true) //forceReload
      //       .then(_.bind(function(res){
      //             //console.log(res);
      //
      //             this.orderListModel.setData(res);
      //             this.orderListModel.refresh(true);
      //
      //             this.dataOrgDialog = sap.ui.xmlfragment("view.dialog.selectOrder", this);
      //             var page = this.getView().byId("shippmentsListPageId");
      //             page.addDependent(this.dataOrgDialog);
      //             this.dataOrgDialog.open();
      //
      //             $(document).keyup(_.bind(this.keyUpFunc, this));
      //
      //        }, this));
      //
      //     }
      //
      // }

      this.reqData = this.getUserInfo();
      if (!!sessionStorage.getItem("customerSession") && sessionStorage.getItem("customerSession") === "true") {
        this.reqData = this.getCustomerInfo();
      }
      //      //**
      //				this.orderListModel= new sap.ui.model.json.JSONModel();
      //				this.getView().setModel(this.orderListModel, "orderList");
      //				utils.Busy.show();
      //				model.collections.OrderList.getOrderList(this.reqData) //forceReload
      //	      .then(_.bind(function(res){
      //	            //console.log(res);
      //	            
      //	            this.orderListModel.setData(res);
      //	            this.orderListModel.refresh();
      //**
      this.dataOrgDialog = sap.ui.xmlfragment("view.dialog.selectOrder", this);
      var page = this.getView().byId("shippmentsListPageId");
      page.addDependent(this.dataOrgDialog);
      //              utils.Busy.hide();
      this.dataOrgDialog.open();

      //**
      //              $(document).keyup(_.bind(this.keyUpFunc, this));
      //	          }, this), _.bind(function(err){
      //	        	  			utils.Busy.hide();
      //							sap.m.MessageToast.show(utils.Message.getError(err));
      //						}, this))
      //**

    }





    this.user = model.persistence.Storage.session.get("user");

    this.loadDeliveries();
    // if(this.order)
    // {
    // 	// model.collections.Shippments.getByOrderId(this.order)
    // 	utils.Busy.show();
    // 	this.order.loadDeliveries()
    // 	.then(_.bind(function(res){
    // 			////console.log(res);
    //
    // 			//this.orderModel = new sap.ui.model.json.JSONModel(this.order);
    // 			utils.Busy.hide();
    // 			this.orderModel.setData(this.order)
    // 			this.orderModel.refresh();
    // 			model.persistence.Storage.session.save("deliveriesOrder", this.order);
    // 			if(!this.order.deliveries || this.order.deliveries.length == 0)
    // 			{
    // 				sap.m.MessageToast.show(model.i18n.getText("NO_DELIVERIES"));
    // 			}
    // 			//this.shippmentListModel.setData(res);
    // 			//this.shippmentListModel.refresh(true);
    // 			this.refreshList();
    // 		}, this),
    // 		_.bind(function(err)
    // 		{
    // 			utils.Busy.hide();
    // 			sap.m.MessageToast.show(utils.Message.getError(err));
    // 			this.order=null;
    // 		}, this));
    //
    //
    // 	//this.refreshList();
    // }





  },
  loadDeliveries: function () {
    if (this.order) {
      // model.collections.Shippments.getByOrderId(this.order)
      utils.Busy.show();
      this.order.loadDeliveries()
        .then(_.bind(function (res) {
            ////console.log(res);

            //this.orderModel = new sap.ui.model.json.JSONModel(this.order);
            utils.Busy.hide();
            this.orderModel.setData(this.order)
            this.orderModel.refresh();
            model.persistence.Storage.session.save("deliveriesOrder", this.order);
            if (!this.order.deliveries || this.order.deliveries.length == 0) {
              sap.m.MessageToast.show(model.i18n.getText("NO_DELIVERIES"));
            }
            //this.shippmentListModel.setData(res);
            //this.shippmentListModel.refresh(true);
            this.refreshList();
          }, this),
          _.bind(function (err) {
            utils.Busy.hide();
            sap.m.MessageToast.show(utils.Message.getError(err));
            this.order = null;
            var that = this;
            //**
            //					jQuery.sap.delayedCall(3000, this, function () {
            //						this.router.navTo("orderList");
            //					});

            model.persistence.Storage.session.remove("deliveriesOrder");
            sap.m.MessageBox.confirm(model.i18n.getText("NO_DELIVERIES_RESELECT"), {
//              onClose: function (evt) {
//                that.onCloseDialog(evt);
//              }
            onClose: function(oAction){
            	if(oAction == "CANCEL")
            	{
            		that.router.navTo("launchpad");
            	}
            	else
            	{
            		that.router.navTo("orderList")
            	}
            }	
            	
            });
            //**

          }, this));


      //this.refreshList();
    }


  },

  onCloseDialog: function (evt) {
    var page = this.getView().byId("shippmentsListPageId");

    if (evt === "OK") {
      if (!this.dataOrgDialog) {
        this.dataOrgDialog = sap.ui.xmlfragment("view.dialog.selectOrder", this);
      }
      page.addDependent(this.dataOrgDialog);
      this.dataOrgDialog.open();
    }
  },

  keyUpFunc: function (e) {
    if (e.keyCode == 27) {
      // codice per il pulsante escape per evitare che lo user chiuda il dialog via ESC

      //                    if(this.dataOrgDialog){
      //                            this.dataOrgDialog.destroy();
      //                    }
      $(document).off("keyup");
      this.router.navTo("launchpad");
    }
  },

  onItemPress: function (evt) {
    var src = evt.getSource();
    var selectedItem = src.getBindingContext("order").getObject();
    //this.getView().getModel("appStatus").setProperty("/currentSelectedShipping", selectedItem);
    model.persistence.Storage.session.save("selectedDelivery", selectedItem);
    // this.getView().getModel("appStatus").setProperty("/masterCntrl", this.getView().getController());

    this.router.navTo("shippment.shippmentDetail", {
      id: selectedItem.getId()
    });


  },

  handleSearchOnOrderDialog: function (oEvent) {
    var sValue = oEvent.getParameter("value");
    var oFilter = new sap.ui.model.Filter("orderId", sap.ui.model.FilterOperator.Contains, sValue);
    var oBinding = oEvent.getSource().getBinding("items");
    oBinding.filter([oFilter]);
  },

  handleConfirmOnOrderDialog: function (oEvent) {

    //var aContexts = oEvent.getParameter("selectedContexts");
    var src = oEvent.getParameter("selectedItem").getBindingContext("orderList").getObject();
    this.order = new model.Order(src);
    if (src) {
      //sap.m.MessageToast.show(model.i18n._getLocaleText("ORDER_CHOSEN") + " : "+ src.orderId);
      model.persistence.Storage.session.save("deliveriesOrder", this.order);
      this.loadDeliveries();

    } else {
      this.order = null;
      this.router.navTo("launchpad");
    }

    //oEvent.getSource().getBinding("items").filter([]);
    // if(this.order)
    // {
    //
    // 		// model.collections.Shippments.getByOrderId(this.order)
    // 		utils.Busy.show();
    // 		this.order.loadDeliveries()
    // 		.then(_.bind(function(res){
    // 				////console.log(res);
    //
    // 				//this.orderModel = new sap.ui.model.json.JSONModel(this.order);
    // 				utils.Busy.hide();
    // 				this.orderModel.setData(this.order)
    // 				this.orderModel.refresh();
    // 				model.persistence.Storage.session.save("deliveriesOrder", this.order);
    // 				if(!this.order.deliveries || this.order.deliveries.length == 0)
    // 				{
    // 					sap.m.MessageToast.show(model.i18n.getText("NO_DELIVERIES"));
    // 				}
    // 				//this.shippmentListModel.setData(res);
    // 				//this.shippmentListModel.refresh(true);
    // 				this.refreshList();
    // 			}, this),
    // 			_.bind(function(err)
    // 			{
    // 				utils.Busy.hide();
    // 				sap.m.MessageToast.show(utils.Message.getError(err));
    // 				this.order=null;
    // 			}, this));
    //
    //
    // 		//this.refreshList();
    //
    // }

    // this.order.loadDeliveries()
    // .then(_.bind(function(res){
    //
    // 		this.orderModel.setData(this.order);
    // 		this.orderModel.refresh();
    //
    //   }, this));
  },

  handleCloseOnOrderDialog: function (evt) {

    sap.m.MessageToast.show(model.i18n._getLocaleText("SELECT_ORDER"), {
      duration: 5000
    });
    //model.persistence.Storage.session.remove("deliveriesOrder");
    //this.order = null;
    this.cleanSession();
    //				jQuery.sap.delayedCall(1000, this, function(){
    //					this.router.navTo("launchpad");
    //				})

  },
  onHomePress: function () {

    view.abstract.AbstractController.prototype.onHomePress.apply(this, arguments);
    //model.persistence.Storage.session.remove("deliveriesOrder");
    this.cleanSession();

  },
  cleanSession: function () {
    model.persistence.Storage.session.remove("deliveriesOrder");
    if (model.persistence.Storage.session.get("selectedDelivery")) {
      model.persistence.Storage.session.remove("selectedDelivery");
    }
    this.order = null;
  },
  refreshList: function (evt) {
    var filters = this.getFiltersOnList();
    //		this.customersModel = model.collections.Customers.getModel();
    //
    //
    //		this.getView().setModel(this.customersModel, "c");


    if (filters)
      this.getView().byId("list").getBinding("items").filter(filters);

  },
  onFilterPress: function () {
    this.filterModel = model.filters.Filter.getModel(this.orderModel.getData().deliveries, "deliveries");
    this.getView().setModel(this.filterModel, "filter");
    var page = this.getView().byId("shippmentsListPageId");
    this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
    page.addDependent(this.filterDialog);
    this.filterDialog.open();

  },
  onFilterDialogClose: function () {
    this.filterDialog.close();
  },

  onFilterPropertyPress: function (evt) {

    var parentPage = sap.ui.getCore().byId("parent");
    var elementPage = sap.ui.getCore().byId("children");
    //console.log(this.getView().getModel("filter").getData().toString());
    var navCon = sap.ui.getCore().byId("navCon");
    var selectedProp = evt.getSource().getBindingContext("filter").getObject();
    this.getView().getModel("filter").setProperty("/selected", selectedProp);
    this.elementListFragment = sap.ui.xmlfragment("view.fragment.filterList", this);
    elementPage.addContent(this.elementListFragment);

    navCon.to(elementPage, "slide");
    this.getView().getModel("filter").refresh();
  },

  onBackFilterPress: function (evt) {
    // this.addSelectedFilterItem();
    this.navConBack();
    this.getView().getModel("filter").setProperty("/selected", "");
    this.elementListFragment.destroy();
  },
  navConBack: function () {
    var navCon = sap.ui.getCore().byId("navCon");
    navCon.to(sap.ui.getCore().byId("parent"), "slide");
    this.elementListFragment.destroy();
  },
  afterOpenFilter: function (evt) {
    var navCon = sap.ui.getCore().byId("navCon");
    if (navCon.getCurrentPage().getId() == "children")
      navCon.to(sap.ui.getCore().byId("parent"), "slide");
    this.getView().getModel("filter").setProperty("/selected", "");
  },

  onSearchFilter: function (oEvt) {
    var aFilters = [];
    var sQuery = oEvt.getSource().getValue();

    if (sQuery && sQuery.length > 0) {

      // var filter = new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.Contains, sQuery);

      // 	var filter = new sap.ui.model.Filter({path:"value", test:function(val)
      // {
      // 	var property= val.toString().toUpperCase();
      // 	return (property.indexOf(sQuery.toString().toUpperCase())>=0)
      // }});

      aFilters.push(this.createFilter(sQuery, "value"));
    }

    // update list binding
    var list = sap.ui.getCore().byId("filterList");
    var binding = list.getBinding("items");
    binding.filter(aFilters);
  },
  createFilter: function (query, property) {
    var filter = new sap.ui.model.Filter({
      path: property,
      test: function (val) {
        var prop = val.toString().toUpperCase();
        return (prop.indexOf(query.toString().toUpperCase()) >= 0)
      }
    });
    return filter;
  },
  onFilterDialogClose: function (evt) {
    if (this.elementListFragment) {
      this.elementListFragment.destroy();
    }
    if (this.filterDialog) {
      this.filterDialog.close();
      this.filterDialog.destroy();
    }
  },
  onFilterDialogOK: function (evt) {
    var filterItems = model.filters.Filter.getSelectedItems("deliveries");
    if (this.elementListFragment)
      this.elementListFragment.destroy();
    this.filterDialog.close();
    this.getView().getModel("filter").setProperty("/selected", "");
    this.handleFilterConfirm(filterItems);
    this.filterDialog.destroy();
    delete(this.filterDialog);
  },
  handleFilterConfirm: function (selectedItems) {
    var filters = [];
    _.forEach(selectedItems, _.bind(function (item) {
        filters.push(this.createFilter(item.value, item.property));
      },
      this));
    var list = this.getView().byId("list");
    var binding = list.getBinding("items");
    binding.filter(filters);
  },
  onResetFilterPress: function () {
    model.filters.Filter.resetFilter("deliveries");
    // //console.log(model.filters.Filter.getSelectedItems("customers"));
  },


  toOrderListPress: function (evt) {
    //        if(this.dataOrgDialog){
    //                this.dataOrgDialog.destroy();
    //        }
    $(document).off("keyup");
    //        this.router.navTo("orderList");
    //** riutilizzo in modo improprio la funzione perché tanto è quello che deve fare!
    
    //this.onCloseDialog("OK");
    this.router.navTo("orderList");
  }








});