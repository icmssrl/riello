jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Shippments");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.collections.PraticheAperte");
jQuery.sap.require("model.collections.PraticheAperte");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.i18n");
jQuery.sap.require("sap.ui.core.format.DateFormat");

view.abstract.AbstractController.extend("view.shippment.ShippmentDetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
		this.deliveryModel = new sap.ui.model.json.JSONModel();

		this.getView().setModel(this.deliveryModel, "delivery");
        // this.shippmentDetailModel = new sap.ui.model.json.JSONModel();
        // this.getView().setModel(this.shippmentDetailModel, "shipDetail");

	},


	handleRouteMatched: function (evt) {

		
		var routeName = evt.getParameter("name");

		if ( routeName !== "shippment.shippmentDetail"){
			return;
		}
		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		this.id = evt.getParameters().arguments.id;

		this.clearPreviousData();
    this.user = model.persistence.Storage.session.get("user");
    this.userModel = new sap.ui.model.json.JSONModel(this.user);
    this.getView().setModel(this.userModel, "userModel");


// 		model.collections.Shippments.getByShippmentId(this.id)
// 		.then(
//
// 			_.bind(function(result)
// 			{
// 				this.shippmentDetailModel.setData(result);
// 				this.shippmentDetailModel.refresh(true);
// //				this.refreshView(result);
// 			}, this)
// 		);

			this.delivery = model.persistence.Storage.session.get("selectedDelivery") ? new model.Delivery(model.persistence.Storage.session.get("selectedDelivery")):null;
			//model.persistence.Storage.session.remove("selectedDelivery");
			this.deliveryModel.setData(this.delivery);
			utils.Busy.show();
			this.delivery.getInvoice()
			.then(_.bind(function(result){

				this.deliveryModel.setData(this.delivery);
				this.deliveryModel.refresh();
				utils.Busy.hide();

			}, this),
			_.bind(function(err){
				utils.Busy.hide();
				sap.m.MessageToast.show(model.i18n.getText("ERROR_LOADING_INVOICE"));

			}, this))

      var req = this.getOrgDataForSAP();
    
     
      this.delivery.getAbnormalPracticesList(req)
      .then(_.bind(function(res){
    	  //-------------------Maybe only temporary---------------------------------------------
//    	  _.remove(res.items, function(item){ return item.codice == ""}) ;
    	  
    	  for(var i = 0; res.items && res.items.length > 0 && i< res.items.length; i++)
    	{
    		  if(res.items[i].codice == "")
    			  {
    			  	//res.items[i].descr = model.i18n.getText("MISSING_GOODS");

    			  	res.items[i].codice="RR";

    			  	res.items[i].codice="MG";

    			  }
    			 
    	}
    	  //------------------------------------------------------------------------------------
        this.buttonsList = res; 
        model.persistence.Storage.session.save("buttonsList",this.buttonsList);
      }));
      
      utils.Busy.show();
      this.delivery.getOpenPracticesList(req)
      .then(_.bind(function(res)
    		  {
    	  		utils.Busy.hide();
    	  		this.deliveryModel.setData(this.delivery);
    		  },this),
    		 _.bind(function(err)
			 {
    			 utils.Busy.hide();
    			 sap.m.MessageToast.show(utils.Message.getError(err));
			 },this))
      

        // this.orderListModel = new sap.ui.model.json.JSONModel();
        // this.getView().setModel(this.orderListModel, "orderList");


        // model.collections.praticheAnomale.PraticheAperte.loadOrdersByShippmentId(this.id) //forceReload
        // .then(_.bind(function(res){
        //     //console.log(res);
				//
        //     this.orderListModel.setData(res);
        //     this.orderListModel.refresh(true);
				//
        // }, this));
        


	},

  
  
  
  

	refreshView : function(data)//maybe to abstract
	{
//			var enable = {"editable": false};
//			var page = this.getView().byId("detailPage");
//			this.getView().setModel(data.getModel(), "c");
//			enable.editable=false;
//			var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar", this);
//			page.setFooter(toolbar);
//			var enableModel = new sap.ui.model.json.JSONModel(enable);
//			this.getView().setModel(enableModel, "en");

	},
	clearPreviousData:function()
	{
		if(model.persistence.Storage.session.get("currentPractice"))
			model.persistence.Storage.session.remove("currentPractice");
	},
    openCreatePracticeActionSheet : function (oEvent) {
			var oButton = oEvent.getSource();

			// create action sheet only once
			if (!this._actionSheet) {
				this._actionSheet = sap.ui.xmlfragment(
					"view.fragment.createPracticeActionSheet",
					this
				);
				this.getView().addDependent(this._actionSheet);
			}

			this._actionSheet.openBy(oButton);
    },


//    onCreatePracticePress: function(evt){
//        //var detail = this.detailModel.getData();
//        model.persistence.Storage.session.save("selectedDelivery", this.delivery);
//        var idButton = evt.getParameter("id");
//        switch(idButton){
//            case "resoConNotaCredito":
//                this.router.navTo("praticheAnomale.ResoConNC",  {shippmentId : this.id});
//                break;
//            case "resoSenzaNotaCredito":
//                this.router.navTo("praticheAnomale.ResoSenzaNC",  {shippmentId : this.id});
//                break;
//            case "richiestaRicambi":
//                this.router.navTo("praticheAnomale.RichiestaRicambi",  {shippmentId : this.id});
//                break;
//            case "mancanzaMerce":
//                this.router.navTo("praticheAnomale.MancanzaMerce",  {shippmentId : this.id});
//                break;
//
//        }
//    },

    showPracticeDetails: function(evt){
    	var src = evt.getSource();
    	var reso = src.getBindingContext("delivery").getObject();
    	model.persistence.Storage.session.save("currentPractice", reso);
    	this.router.navTo("resoDetail", {id:reso.resoId});
        
    },

    onCreatePracticeDialogConfirm: function(evt){

        /* TODO: Send data to SAP*/

//        sap.m.MessageToast.show(model.i18n._getLocaleText("AP_PRACTICE_CREATED"));

        if(this.createRenderDialog){
            this.createRenderDialog.close();
            this.createRenderDialog.destroy();
        }

    },

    navBackToMobileMaster: function(evt){
      this.router.navTo("shippment.shippmentsListMobile");
    },


    // onLinkToUserInfoPress: function(evt){
    //   this.router.navTo("changePassword");
    // },

		onShippmentPrintPress:function(evt)
	  {
			utils.Busy.show();
			this.delivery.getShippmentPrintPdf()
			.then(_.bind(function(res){
				utils.Busy.hide();
			}, this),
			_.bind(function(err){
				sap.m.MessageToast.show(model.i18n.getText("ERROR_LOADING_SHIPPING_PDF"));
				utils.Busy.hide();
			}, this))



	  },
		onInvoicePrintPress:function()
		{
			// this.delivery.getInvoice() //invoices?
			// .then(_.bind(function(result){
			// 	if(result)
			// 	{
			// 		this.delivery.getInvoicePrintPdf(result);
			// 	}
			//
			// }, this),
			// _.bind(function(err){
			// 		sap.m.MessageToast.show("Error loading Invoice");
			// }, this)
			// )

			if(!this.delivery.invoices || this.delivery.invoices.length == 0)
			{
				sap.m.MessageToast.show(model.i18n.getText("INVOICE_NOT_EXISTENT"));
			}
			else {
				utils.Busy.show();
				this.delivery.getInvoicePrintPdf(this.delivery.invoices[0].Vbeln)
				.then(_.bind(function(res){
					utils.Busy.hide();
				}, this),
				_.bind(function(err){
					sap.m.MessageToast.show(model.i18n.getText("ERROR_LOADING_INVOICE_PDF"));
					utils.Busy.hide();
				}, this))

			}

		},
  
  
  onOpenAssignActionSheetPress: function(oEvent){
       
   var oButton = oEvent.getSource();
            var auartCase = model.persistence.Storage.session.get("buttonsList");
            var buttons = _.bind(this.createStatusButtons, this);
            var button;
   // create action sheet only once
   if (!this._actionSheet) {
    this._actionSheet = sap.ui.xmlfragment(
     "view.fragment.createPracticeActionSheet",
     this
    );
    this.getView().addDependent(this._actionSheet);
   }
            if(this._actionSheet.getButtons().length > 0) 
            this._actionSheet.destroyButtons();
            for(var i = 0; i < auartCase.items.length; i++){
                button = buttons(auartCase.items[i]);
                this._actionSheet.addButton(button);
            
            }
   this._actionSheet.openBy(oButton);
  
    },
    
    createStatusButtons: function(b){
        var newButton = new sap.m.Button(
            {
                text: b.descr,
                id : b.codice,
                class: "assignStatusButtonClass",
                press: [this.assignNextStatusButton, this]
            }
        );
        return newButton;
    },
    
    assignNextStatusButton: function(evt){
        var source = evt.getSource();
        var codice = source.getId();
        var route  = "praticheAnomale.ResoConNC";
        switch(codice){
            case "ZRS":
                route = "praticheAnomale.ResoSenzaNC";
                this.router.navTo(route, {shippmentId : this.id,  type:codice});
                break;
                
//            case "richiestaRicambi":
//                this.router.navTo("praticheAnomale.RichiestaRicambi",{
//                		shippmentId : this.id
//                		
//                		});
//                break;
                
            case "ZST":
            	route="praticheAnomale.MancanzaMerce";
            	this.router.navTo(route, {shippmentId: this.id, type:codice});
            	break;
                
            case "RR":
                route = "praticheAnomale.RichiestaRicambi";
                this.router.navTo(route, {shippmentId : this.id,  type:codice});
                break;
                
            case "ZRE":
                route  = "praticheAnomale.ResoConNC";
                this.router.navTo(route,  {shippmentId : this.id, type:codice});
                break;
                
            default:
            	route  = "praticheAnomale.ResoConNC";
            	this.router.navTo(route,  {shippmentId : this.id, type:codice});
            	break;
            	
        }
        //this.router.navTo(route,  {shippmentId : this.id, type:codice});

    },
    
    //onPracticeItemPress:function(evt)
    //{
    //	var src = evt.getSource();//.getBindingContext("").getObject.id
    //	this.router.navTo("resoDetail", {id:"0160041949"});
    	
    //}
  
  
  


});
