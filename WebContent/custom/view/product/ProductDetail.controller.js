jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.Positions");
jQuery.sap.require("model.collections.Accessories");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.Discount");
jQuery.sap.require("view.product.AbstractProductDetail");
jQuery.sap.require("utils.Date");

// view.abstract.AbstractController.extend("view.ProductDetail", {
view.product.AbstractProductDetail.extend("view.product.ProductDetail", {

  // onExit: function () {
  //
  // },
  //
  //
  // onInit: function () {
  //
  //
  // 	view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
  //
  // },

  checkRoute: function (route) {
    return (route === "productDetail" || route === "productDetailMobile");

  },

  handleRouteMatched: function (evt) {
  //
  view.product.AbstractProductDetail.prototype.handleRouteMatched.apply(this, arguments);
  // var routeName = evt.getParameter("name");
  // if(this.checkRoute(routeName))
    // this.initializeCart();
},

  initializeCart : function()
  {
    if(!model.Current.getHistoryCart())
    {
      return;
    }
    if(this.cart)
      return;

    this.cart = model.Current.getHistoryCart();//To add setOrder

    _.forEach(this.cart.positions, _.bind(function(item)
    {
      var position = new model.Position();

      
      var product= new model.Product(item.product);
      position.create(this.order, product);
      position.setQuantity(item.quantity);
      // position.setWantedDate(new Date(utils.Formatter.formatDateValue((this.reqModel.getData().reqDate))));
      position.setWantedDate(new Date(this.order.requestedDate));
      this.order.addPosition(position);
//--------------------Old------------------------------------
      // var discount = new model.Discount();
      // discount.initialize(this.order.getId(), position);
      // position.setDiscount(discount);
//----------------------------------------------------------------

//---------------------New----------------------------------
      var discount = new model.Discount();
      discount.update(position.discount);
      position.setDiscount(discount);
//--------------------------------------------------------------
    }, this))

  },

  navBackToMobileMaster: function(evt){
      this.router.navTo("productsListMobile");
    },

  onAddPress: function (evt) {


	  var msgs = [];
        var accessoriesList = this.getView().byId("accessoriesList");
        var selectedAccessories = [];
        // if(accessoriesList && this.accessories && this.accessories.length>0 ){
        //     var accessoriesArr = accessoriesList.getModel("accessories").getData().items;
        // }
    //var tempPos = [];

    if (!this.cart) {
      this.cart = new model.Cart();

      this.cart.create(this.order.getId(), this.customer.getId());
    }

    //------------------------------------------------
    var position = new model.Position();
    var checkProductId = this.product.productId;
    var productExist= false;
    var requestedDate = this.reqModel.getData().reqDate ? this.reqModel.getData().reqDate : new Date();
    //-----------------------Removed Check on previous Existence ----------------------------------
//    if (this.order.positions) {
//      var positionsArr = this.order.positions;
//      for (var i = 0; i < positionsArr.length; i++) {
//        if (checkProductId === positionsArr[i].productId) {
//         // sap.m.MessageToast.show(model.i18n._getLocaleText("PRODUCT_ALREADY_ADDED"));
//          msgs.push(model.i18n._getLocaleText("PRODUCT_ALREADY_ADDED") + " : " + this.product.getId());
//          productExist=true;
//          break;
//        }
//      }
//    }
//------------------------------------------------------------------------------------------------------------
  //----------------------- Check on previous Existence Matnr + Requested Date ----------------------------------
  if (this.order.positions) {
    var positionsArr = this.order.positions;
    for (var i = 0; i < positionsArr.length; i++) {
      if (checkProductId === positionsArr[i].productId && utils.Date.compareDate(requestedDate,positionsArr[i].wantedDate)==0) {
       // sap.m.MessageToast.show(model.i18n._getLocaleText("PRODUCT_ALREADY_ADDED"));
        msgs.push(model.i18n._getLocaleText("productAlreadyAddedInThisDate") + " : " + this.product.getId());
        productExist=true;
        break;
      }
    }
  }
//------------------------------------------------------------------------------------------------------------
    if(!productExist)
    {
      position.create(this.order, this.product);
      //**
      if (this.reqModel.getData().reqQty) {
        position.setQuantity(this.reqModel.getData().reqQty);
      } else {
        position.setQuantity(1);
      }
      position.setWantedDate(requestedDate);
      
      //tempPos.push(position);
      
      // position.setQuantity(this.reqModel.getData().reqQty);
//      if(this.reqModel.getData().reqDate)
//      {
//        position.setWantedDate(new Date(this.reqModel.getData().reqDate));
//      }
//      else {
//        position.setWantedDate(new Date(this.order.requestedDate));
//      }
      //**

     // var discount = new model.Discount();
      //position.setDiscount(discount);
      
      //discount.initialize(this.order.getId(), position);
      this.order.addPosition(position);
      msgs.push(model.i18n._getLocaleText("PRODUCT_ADDED") + " : " + this.product.getId());
      
      /* New Management Availability and Price */
      //this.getView().setBusy(true);
      
      //this._addPosition(position, this.reqModel.getData().reqQty, requestedDate );
      
      //this.userInfo = this.getOrgDataForSAP();
//      position.getAvailability(this.userInfo, this.reqModel.getData().reqQty, requestedDate)
//      .then(_.bind(function(res){
//    	  var defer = Q.defer();
//    	  this.order.refreshFullEvasionAvailableDate();
//    	  var fSuccess = function(result)
//    	  {
//    		  msgs.push(model.i18n._getLocaleText("PRODUCT_ADDED") + " : " + this.product.getId());
//    		  this.order.addPosition(position);
//    		  defer.resolve(result);
//    	  }
//    	  fSuccess = _.bind(fSuccess, this);
//    	  
//    	  var fError = function(err)
//    	  {
//    		  sap.m.MessageToast.show(utils.Message.getError(err));
//    		  defer.reject(err);
//    	  }
//    	  fError = _.bind(fError, this);
//    	 
//    	  position.refreshPriceValues(this.customer, this.order.IVACode)
//    	  .then(fSuccess, fError)
//    	        
//    	    
//    	  return defer.promise;
//      }, this),
//    		  
//    	_.bind(function(err){
//    		sap.m.MessageToast.show(utils.Message.getError(err));
//    	}, this))
     
      
      
      
      /*---------------------------------------*/
      
      
      //sap.m.MessageToast.show(model.i18n._getLocaleText("PRODUCT_ADDED") + " : " + this.product.getId());
      //msgs.push(model.i18n._getLocaleText("PRODUCT_ADDED") + " : " + this.product.getId());
      
      
      //Temp-----------------------------------------------------------------------------------------
      // position.setDiscount(discount);
      // //console.log(position);
      // this.order.addPosition(position);
      // sap.m.MessageToast.show(model.i18n._getLocaleText("PRODUCT_ADDED") + " : " + this.product.getId());
      //-------------------------------------------------------------------------------------
      //To Add -----Maybe it's better done in Cart---------------------------------------------------------------------------
      // discount.getDiscountValues(this.product, this.customer)
      //
      // .then(_.bind(function(res)
      // {
      //   position.setDiscount(res);
      //   //console.log(position);
      //   this.order.addPosition(position);
      //   sap.m.MessageToast.show(model.i18n._getLocaleText("PRODUCT_ADDED") + " : " + this.product.getId());
      // }, this))
      //------------------------------------------------------------------------------------------------------




    }



    //        }, this));


        if(this.accessories && accessoriesList.getSelectedItems().length>0){
            var selectedItems = accessoriesList.getSelectedItems();
            for (var a = 0; a < selectedItems.length; a++){
                var oContext = selectedItems[a].getBindingContext("accessories");
                // var path = oContext.getPath();
                // var currentIndex = parseInt(path.split('/')[path.split('/').length - 1]);
                selectedAccessories.push(oContext.getObject());
            }
        }


        if(selectedAccessories.length>0){
            for(var aa = 0; aa<selectedAccessories.length; aa++){
                var realPositions = this.order.positions; //----->To check------
                var foundDuplicate;
                //-------------removed check on previous existence----------------------------------
//                var found = _.find(realPositions, {productId : selectedAccessories[aa].productId});
//                if(found)
//                {
//                  found.setQuantity(parseInt(found.quantity)+1);
//                  msgs.push(model.i18n._getLocaleText("PRODUCT_ALREADY_ADDED") + " : " + selectedAccessories[aa].productId);
//                  continue;
//                }
                //------------------------------------------------------------------------------------------
                //-----------------New Check on Matnr and RequestedDate----------------------------------------
                
                
              
                var found = _.find(realPositions, _.bind(function(item){
                	
                	return ((item.productId == selectedAccessories[aa].productId)&& (utils.Date.compareDate(requestedDate, item.wantedDate)== 0));
                	
                	
                }, this));
	              if(found)
	              {
	                //found.setQuantity(parseInt(found.quantity)+1);
	                msgs.push(model.i18n._getLocaleText("productAlreadyAddedInThisDate") + " : " + selectedAccessories[aa].productId);
	                continue;
	              }
                //-----------------------------------------------------------------------------------------
                var newPosition = new model.Position();
                newPosition.create(this.order, selectedAccessories[aa]);
                newPosition.setQuantity(1);
                if(this.reqModel.getData().reqDate)
                {
                  newPosition.setWantedDate(new Date(this.reqModel.getData().reqDate));
                }
                else {
                  newPosition.setWantedDate(new Date(this.order.requestedDate));
                }
                //tempPos.push(newPosition);
                //this._addPosition(newPosition, 1, new Date(newPosition.wantedDate));
                this.order.addPosition(newPosition);
           
                
                msgs.push(model.i18n._getLocaleText("PRODUCT_ADDED") + " : " + selectedAccessories[aa].productId)
               
               
      }

    }
   
    model.Current.setOrder(this.order);
   this.cart.setPositions(this.order.getPositions());
    
    this.getView().byId("accessoriesList").removeSelections();
    //console.log(this.order);



    // sap.m.MessageToast.show(model.i18n._getLocaleText("PRODUCT_ADDED") + " : " + this.product.getId());

    //-----------------------Msg Management------------------------------------------------------------
    

    var msgFunction = function(msgs, i)
    {
    	var defer = Q.defer();
    	var fSuccess = function(msgs, i)
    	{
    		if(i== msgs.length)
    		{
    			defer.resolve(this);
    		}
    		else
    		{
    			msgFunction(msgs, i);
    		}
    	}
    	sap.m.MessageToast.show(msgs[i], {
    		duration:1000,
    		onClose: _.bind(fSuccess, this, msgs, i+1)});//calculateFunction:function()
    	//{
//    	if (this.order && this.order.positions && this.order.positions.length > 0) {
//    		var availableDateFuncArray = [];
//    		var refreshPricesFuncArray = [];
//    		for(var i=0; i<this.order.positions.length; i++)
//    		{
//    			if(!this.order.positions[i].availableDate)
//    				availableDateFuncArray.push(_.bind(this.availableDateFunction, this, this.order.positions[i]));
//    			if(!this.order.positions[i].discount)
//    				refreshPricesFuncArray.push(_.bind(this.availableDateFunction, this, this.order.positions[i]));
//    			
//    			
//    		}
//          var funcArray= availableDateFuncArray.concat(refreshPriceFuncArray);
//    		Q.all(funcArray)
//    		.then(_.bind(function(res){
//    			this.onSimulate();
//    		}, this))
//    		
//    	}
    //},
    	
    	return defer.promise;
    }
    msgFunction=_.bind(msgFunction, this);
    
    msgFunction(msgs, 0);
    
    //--------------------------------------------------------------------------------------------------




  },

navBackToMobileMaster: function(evt){
      this.router.navTo("productsListMobile");
    },

  goToCart : function()
  {
	  model.Current.setOrder(this.order);
	  this.cart.setPositions(this.order.getPositions());
	  if(model.persistence.Storage.session.get("OrderType")=="Offer")
	      this.router.navTo("offerCartFullView");
	    else
	      this.router.navTo("cartFullView");

  },
  
  _addPosition:function(position, reqQty, reqDate)
  {
	  var _defer = Q.defer();
	  this.userInfo = this.getOrgDataForSAP();
	  this.getView().setBusy(true);
	  position.getAvailability(this.userInfo, this.reqModel.getData().reqQty, reqDate)
      .then(_.bind(function(res){
    	  //var defer = Q.defer();
    	  this.order.refreshFullEvasionAvailableDate();
    	  var fSuccess = function(result)
    	  {
    		  this.getView().setBusy(false);
    		  sap.m.MessageToast.show(model.i18n._getLocaleText("PRODUCT_ADDED") + " : " + this.product.getId(), {duration:500});
    		  this.order.addPosition(position);
    		  model.Current.setOrder(this.order);
    		  this.cart.setPositions(this.order.getPositions());
    		  _defer.resolve(result);
    		  //defer.resolve(result);
    	  }
    	  fSuccess = _.bind(fSuccess, this);
    	  
    	  var fError = function(err)
    	  {
    		  this.getView().setBusy(false);
    		  sap.m.MessageToast.show(utils.Message.getError(err));
    		  _defer.reject(err);
    		  //defer.reject(err);
    	  }
    	  fError = _.bind(fError, this);
    	 
    	  position.refreshPriceValues(this.customer, this.order.IVACode)
    	  .then(fSuccess, fError)
    	        
    	    
    	  //return defer.promise;
      }, this),
    		  
    	_.bind(function(err){
    		sap.m.MessageToast.show(utils.Message.getError(err));
    		_defer.reject(err);
    	}, this))
    	
    	return _defer.promise;
  }




});
