jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Products");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Catalogue");

view.abstract.AbstractMasterController.extend("view.product.ProductsList", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.uiModel.setProperty("/searchProperty", ["description", "productId"]);

    this.uiModel.setProperty("/last", false);
    this.initializeCatalogue();
    this.createList();


  },


  handleRouteMatched: function (evt) {

    this.routeName = evt.getParameter("name");
    this.uiModel.setProperty("/searchValue", "");
    //**---now-----------------------
//    if (this.routeName === "empty") {
//      this.paths = [];
//      this.initializeCatalogue();
//      this.createList();
//    }
    //**

    
    ((this.routeName === "productList") || (this.routeName === "empty") || (this.routeName === "productDetail") || this.routeName==="productDetailMobile") ? this.getView().getModel("appStatus").setProperty("/en", {
        "wr": true
      }): null;
      ((this.routeName === "readOnlyHierarchy") || (this.routeName === "emptyROHierarchy") || (this.routeName === "productRODetail") || this.routeName==="productROMobileDetail") ? this.getView().getModel("appStatus").setProperty("/en", {
        "wr": false
      }): null;
      
      if (this.routeName !== "productsList" && this.routeName != "empty" && this.routeName !== "readOnlyHierarchy" && this.routeName !== "emptyROHierarchy") {
          return;
        }


//      var user = model.persistence.Storage.session.get("user");
      var user = model.persistence.Storage.session.get("workingUser");
      this.uiModel.setProperty("/wrCust", user.organizationData.results[0].canEditCustomers);
      this.uiModel.setProperty("/wrOrd", user.organizationData.results[0].canEditOrders);
      
   
    //**devo capire da quale route vengo e inizializzare o meno il catalogo
    // this.initializeCatalogue();

//----------Moved top now----------------------------------------------
//    ((this.routeName === "productList") || (this.routeName === "empty")) ? this.getView().getModel("appStatus").setProperty("/en", {
//      "wr": true
//    }): null;
//    ((this.routeName === "readOnlyHierarchy") || (this.routeName === "emptyROHierarchy")) ? this.getView().getModel("appStatus").setProperty("/en", {
//      "wr": false
//    }): null;
//---------------------------------------------------------------------------

//    var user = model.persistence.Storage.session.get("user");
//    this.uiModel.setProperty("/wrCust", user.canEditCustomers);
//    this.uiModel.setProperty("/wrOrd", user.canEditOrders);

    //this.getView().getModel("appStatus").setProperty("/enableFilterButton", false);

    this.paths = [];
    this.initializeCatalogue();
    this.createList();



    //    model.collections.Products.loadProducts(true) //forceReload
    //      .then(_.bind(this.refreshList, this));

    // this.initializeCart();


  },

  initializeCart : function()
  {
    if(!model.Current.getHistoryCart())
    {
      return;
    }
    if(this.cart)
      return;

    this.cart = model.Current.getHistoryCart();//To add setOrder

    _.forEach(this.cart.positions, _.bind(function(item)
    {
      var position = new model.Position();
      var product= new model.Product(item.product);
      position.create(this.order, product);
      position.setQuantity(item.quantity);
      // position.setWantedDate(new Date(utils.Formatter.formatDateValue((this.reqModel.getData().reqDate))));
      position.setWantedDate(new Date(this.order.requestedDate));
      this.order.addPosition(position);
      var discount = new model.Discount();
      discount.initialize(this.order.getId(), position);
      position.setDiscount(discount);

    }, this))

  },
  initializeCatalogue: function () {
    this.userInfo = this.getOrgDataForSAP();
    this.catalogue = new model.Catalogue();
  },
  createList: function (item) {
    // if(_.isEmpty(item))
    //   item = undefined;
    // else {
    //   item = this.catalogue.getById(item);
    // }
    if (item && item.last === "X") {
      this.uiModel.setProperty("/last", true);
    } else {
      this.uiModel.setProperty("/last", false);
    }
    this.catalogue.getHierarchy(this.userInfo, item)
      .then(_.bind(this.refreshList, this, item));
  },

  onItemPress: function (evt) {
    var src = evt.getSource();

    var selectedItem = src.getBindingContext("h").getObject();

    if (selectedItem.isProduct()) {
      //this.getView().getModel("appStatus").setProperty("/product", selectedItem);
      model.persistence.Storage.session.save("currentProduct", selectedItem);
      if ((this.routeName === "productList") || (this.routeName === "empty") || this.routeName === "productDetail" || (this.routeName === "productsListMobile")){
        if(!sap.ui.Device.system.phone){
        this.router.navTo("productDetail", {
          id: selectedItem.productId
        });
      }
      else
      {
      	this.router.navTo("productDetailMobile", {
          id: selectedItem.productId
        });
      }
        return;
        
       }
      else {
        if ((this.routeName === "readOnlyHierarchy") || (this.routeName === "emptyROHierarchy") || this.routeName === "productRODetail" || (this.routeName === "readOnlyHierarchyMobile"))
       
          if(!sap.ui.Device.system.phone){
        this.router.navTo("productRODetail", {
            id: selectedItem.productId
          });
      }
      else
      {
      	this.router.navTo("productROMobileDetail", {
          id: selectedItem.productId
        });
      }
        return;

      }
      return;
    }
    this.createList(selectedItem);



  },



  refreshList: function (item) {

    var filters = this.getFiltersOnList();


    if (!this.paths || this.paths.length == 0) {
      this.paths = [];
      this.addCrumbPath("Catalogo", "");
    } else {
      if (item) {
        this.addCrumbPath(item.description, item);
      }
    }
    this.hierarchyModel = this.catalogue.getModel();
    this.getView().setModel(this.hierarchyModel, "h");

    ////ENABLE ADVANCED FILTER AND DIRECT CALL HERE---HOW?

    // this.refreshCrumbToolbar();
    if (filters)
      this.getView().byId("list").getBinding("items").filter(filters);

    //console.log();
    // if(!this.hierarchyModel){
    // this.hierarchyModel = this.catalogue.getModel();
    // this.getView().setModel(this.hierarchyModel, "h");
    // }

  },
  handleLinkPress: function (evt) {
    var src = evt.getSource();
    var target = src.getTarget();
    this.removeCrumbPath(target);

  },
  addCrumbPath: function (val, item) {
    var pathItem = {};
    pathItem.value = val;
    if (_.isEmpty(item)) {
      pathItem.parent = "";
      pathItem.item = undefined;
    } else {
      pathItem.parent = item.productId;
      pathItem.item = item;
    }
    this.paths.push(pathItem);
    this.refreshCrumbToolbar();
    // var item = sap.ui.xmlfragment("view.list.breadCrumbList", this);
    // var toolbar= this.getView().byId("crumbPaths");
    // this.pathModel.setProperty("/paths", this.paths);
    // this.pathModel.refresh();
    // list.bindAggregation("items", {path:"p>/paths", template:item});
    // var toolbar = this.getView().byId("crumbPaths");
    // toolbar.addContent(new sap.m.Link({text:val, id:type, class:"hierachyLink", press : [this.handleLinkPress, this]}));
  },
  removeCrumbPath: function (target) {
    var index = -1;
    var item = {};
    for (var i = 0; i < this.paths.length; i++) {
      if (this.paths[i].parent === target) {
        item = this.paths[i].item;
        index = i;
        break;
      }
    }
    if (index < this.paths.length - 1) {
      this.paths = this.paths.slice(0, index);
    }
    this.createList(item);


  },

  refreshCrumbToolbar: function () {
    var toolbar = this.getView().byId("crumbToolbar");
    // toolbar.destroyContent();
    var toolbarCnt = toolbar.getContent();
    if (toolbarCnt.length > 1) {
      for (var i = 1; i < toolbarCnt.length; i++) {
        toolbar.removeContent(toolbarCnt[i]);
      }
    }
    for (var i = 0; i < this.paths.length; i++) {
      // toolbar.addContent(new sap.m.Link({text:this.paths[i].value, target:this.paths[i].type, class:"hierachyLink", press : [this.handleLinkPress, this]}));
      toolbar.addContent(new sap.m.Link({
        text: this.paths[i].value,
        target: this.paths[i].parent,
        press: [this.handleLinkPress, this]
      }).addStyleClass("hierarchyLink"));
    }
  },



  onFilterPress: function () {
    this.filterModel = model.filters.Filter.getModel(this.hierarchyModel.getData().hierarchy, "hierarchyItems");
    this.getView().setModel(this.filterModel, "filter");
    var page = this.getView().byId("productsListPageId");
    this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
    page.addDependent(this.filterDialog);
    this.filterDialog.open();

  },
  onFilterDialogClose: function () {
    this.filterDialog.close();
  },

  onFilterPropertyPress: function (evt) {

    var parentPage = sap.ui.getCore().byId("parent");
    var elementPage = sap.ui.getCore().byId("children");
    //console.log(this.getView().getModel("filter").getData().toString());
    var navCon = sap.ui.getCore().byId("navCon");
    var selectedProp = evt.getSource().getBindingContext("filter").getObject();
    this.getView().getModel("filter").setProperty("/selected", selectedProp);
    this.elementListFragment = sap.ui.xmlfragment("view.fragment.filterList", this);
    elementPage.addContent(this.elementListFragment);

    navCon.to(elementPage, "slide");
    this.getView().getModel("filter").refresh();
  },

  onBackFilterPress: function (evt) {
    // this.addSelectedFilterItem();
    this.navConBack();
    this.getView().getModel("filter").setProperty("/selected", "");
    this.elementListFragment.destroy();
  },
  navConBack: function () {
    var navCon = sap.ui.getCore().byId("navCon");
    navCon.to(sap.ui.getCore().byId("parent"), "slide");
    this.elementListFragment.destroy();
  },
  afterOpenFilter: function (evt) {
    var navCon = sap.ui.getCore().byId("navCon");
    if (navCon.getCurrentPage().getId() == "children")
      navCon.to(sap.ui.getCore().byId("parent"), "slide");
    this.getView().getModel("filter").setProperty("/selected", "");
  },

  onSearchFilter: function (oEvt) {
    var aFilters = [];
    var sQuery = oEvt.getSource().getValue();

    if (sQuery && sQuery.length > 0) {

      // var filter = new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.Contains, sQuery);

      // 	var filter = new sap.ui.model.Filter({path:"value", test:function(val)
      // {
      // 	var property= val.toString().toUpperCase();
      // 	return (property.indexOf(sQuery.toString().toUpperCase())>=0)
      // }});

      aFilters.push(this.createFilter(sQuery, "value"));
    }

    // update list binding
    var list = sap.ui.getCore().byId("filterList");
    var binding = list.getBinding("items");
    binding.filter(aFilters);
  },
  createFilter: function (query, property) {
    var filter = new sap.ui.model.Filter({
      path: property,
      test: function (val) {
        var prop = val.toString().toUpperCase();
        return (prop.indexOf(query.toString().toUpperCase()) >= 0)
      }
    });
    return filter;
  },
  onFilterDialogClose: function (evt) {
    if (this.elementListFragment) {
      this.elementListFragment.destroy();
    }
    if (this.filterDialog) {
      this.filterDialog.close();
      this.filterDialog.destroy();
    }
  },
  onFilterDialogOK: function (evt) {
    var filterItems = model.filters.Filter.getSelectedItems("hierarchyItems");
    if (this.elementListFragment)
      this.elementListFragment.destroy();
    this.filterDialog.close();
    this.getView().getModel("filter").setProperty("/selected", "");
    this.handleFilterConfirm(filterItems);
    this.filterDialog.destroy();
    delete(this.filterDialog);
  },
  handleFilterConfirm: function (selectedItems) {
    var filters = [];
    _.forEach(selectedItems, _.bind(function (item) {
        filters.push(this.createFilter(item.value, item.property));
      },
      this));
    var list = this.getView().byId("list");
    var binding = list.getBinding("items");
    binding.filter(filters);
  },
  onResetFilterPress: function () {
    model.filters.Filter.resetFilter("hierarchyItems");
    // //console.log(model.filters.Filter.getSelectedItems("customers"));
    var list = this.getView().byId("list");
    var binding = list.getBinding("items");
    binding.filter();
    if (this.elementListFragment) {
      this.elementListFragment.destroy();
    }
    if (this.filterDialog) {
      this.filterDialog.close();
      this.filterDialog.destroy();
    }
  },

  onCartPress: function (evt) {
    var crumbToolbar = this.getView().byId("crumbToolbar");
    var content = crumbToolbar.getContent();
    var lv2 = sap.ui.getCore().byId("lv2Node");
    var lv1 = sap.ui.getCore().byId("lv1Node");

    if (content.length >= 1) {

      if (content[2]) {
        if (!!lv2)
          lv2.destroy();
        crumbToolbar.removeContent(content[2]);
      }
      if (!!lv1)
        lv1.destroy();
      crumbToolbar.removeContent(content[1]);
    }
    this.hierarchyModel = undefined;
    
    if(model.persistence.Storage.session.get("OrderType")=="Offer")
        this.router.navTo("offerCartFullView");
      else
        this.router.navTo("cartFullView");
   


    // var src = evt.getSource();
    // src.setIcon("sap-icon://add-product");
    // src.detachPress(this.onCartPress, this);
    // src.attachPress(null, this.onShowProductPress, this);
  },
  // onShowProductPress:function(evt)
  // {
  // 	if(this.getView().getModel("appStatus").getProperty("/currentProduct"))
  // 	{
  // 		var lastProductId = this.getView().getModel("appStatus").getProperty("/currentProduct").productId;
  // 		this.navToProduct(lastProductId, "flip");
  // 		return;
  // 	}
  //
  // 	this.router.navTo("empty");



  // },
  navToProduct: function (id) {

    this.router.navTo("productDetail", {
      id: id
    });
    // var src = this.getView().byId("navId");
    // src.setIcon("sap-icon://cart");
    // src.detachPress(this.onShowProductPress, this);
    // src.attachPress(null, this.onCartPress, this);
  },



  onOrderHeaderPress: function (evt) {
    var crumbToolbar = this.getView().byId("crumbToolbar");
    var content = crumbToolbar.getContent();
    var lv2 = sap.ui.getCore().byId("lv2Node");
    var lv1 = sap.ui.getCore().byId("lv1Node");

    if (content.length >= 1) {

      if (content[2]) {
        if (!!lv2)
          lv2.destroy();
        crumbToolbar.removeContent(content[2]);
      }
      if (!!lv1)
        lv1.destroy();
      crumbToolbar.removeContent(content[1]);
    }
    this.hierarchyModel = undefined;
    this.router.navTo("newOrder");

  },

  // onDirectSearchPress: function (evt) {
  //   var productInfo = this.uiModel.getProperty("/searchValue");
  //
  //   this.uiModel.setProperty("/searchValue", "");
  //   if (_.isEmpty(productInfo)) {
  //     sap.m.MessageToast.show("Entry a value on search field");
  //     return;
  //   }
  //   this.removeFiltersOnList();
  //   this.refreshList();
  //   this.catalogue.getProductByInfo(this.userInfo, productInfo)
  //     .then(_.bind(function (result) {
  //       var product = result;
  //       this.getView().getModel("appStatus").setProperty("/product", product);
  //       if ((this.routeName === "productList") || (this.routeName === "empty") || this.routeName === "productDetail")
  //         this.router.navTo("productDetail", {
  //           id: product.productId
  //         });
  //       else {
  //         if ((this.routeName === "readOnlyHierarchy") || (this.routeName === "emptyROHierarchy") || this.routeName === "productRODetail")
  //           this.router.navTo("productRODetail", {
  //             id: product.productId
  //           });
  //       }
  //     }, this))
  // },

  onDirectSearchPress:function()
	{
		var page = this.getView().byId("productsListPageId");
		if(!this.directSearchDialog)
			this.directSearchDialog = sap.ui.xmlfragment("view.dialog.productDirectSearchDialog", this);

		page.addDependent(this.directSearchDialog);
		this.directSearchDialog.open();
	},
	onDirectSearchOK:function(evt)
	{
		// var value = this.uiModel.getProperty("/searchValue");
		// if(_.isEmpty(value))
    // {
    //   sap.m.MessageToast.show("Entry a value on search field");
    //   return;
    // }
		// this.getView().getModel("appStatus").setProperty("/customerRequester", this.customerRequester.getParameters());
		// model.persistence.Storage.session.save("customerRequester", this.customerRequester.getParameters());
		// this.directSearchDialog.close();
		// this.router.navTo("customerDetail",{id: value} );

  //-------------------------------------------------------------------------------------------------------------------
  var productInfo = this.uiModel.getProperty("/searchValue");

  this.uiModel.setProperty("/searchValue", "");
  if (_.isEmpty(productInfo)) {
    sap.m.MessageToast.show("Entry a value on search field");
    return;
  }
  this.directSearchDialog.close();
  this.removeFiltersOnList();
  this.refreshList();
  this.catalogue.getProductByInfo(this.userInfo, productInfo)
    .then(_.bind(function (result) {
      var product = result;
      this.getView().getModel("appStatus").setProperty("/product", product);
      model.persistence.Storage.session.save("currentProduct", product);
      if ((this.routeName === "productList") || (this.routeName === "empty") || this.routeName === "productDetail")
        this.router.navTo("productDetail", {
          id: product.productId
        });
      else {
        
        if ((this.routeName === "readOnlyHierarchy") || (this.routeName === "emptyROHierarchy") || this.routeName === "productRODetail")
          this.router.navTo("productRODetail", {
            id: product.productId
          });
      }
    }, this))
  //---------------------------------------------------------------------------------------------------------------
	},
	onDirectSearchClose:function(evt)
	{
		this.directSearchDialog.close();
	},
	onDirectSearchAfter:function(evt)
	{
		delete(this.directSearchDialog);
	},








});
