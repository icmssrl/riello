jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Message");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.Positions");
jQuery.sap.require("model.collections.Accessories");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.Discount");
jQuery.sap.require("model.Product");
jQuery.sap.require("model.Catalogue");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.declare("view.product.AbstractProductDetail");

view.abstract.AbstractController.extend("view.product.AbstractProductDetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		// this.uiModel.setProperty("/en", {check: false});
		
		var routeName = evt.getParameter("name");
		if(!this.checkRoute(routeName))
			return;

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);

		var canAddToCart = this.getView().getModel("ui").getProperty("/wrOrd");
		if(canAddToCart)
		{
			if(routeName == "productRODetail" || routeName == "productROMobileDetail")
			{
				canAddToCart = false;
			}
		}
		this.getView().getModel("ui").setProperty("/canAddToCart", canAddToCart);

		var id = evt.getParameters().arguments.id;

		this.order = model.Current.getOrder();

		//Now it's here, maybe the next code needs to be moved
		//As we are in orderCreqtion, maybe we need another property to verify the application status
		if(!this.order)
		{
			var orderData = model.persistence.Storage.session.get("currentOrder");
			this.order  = new model.Order(orderData);

		};

		this.customer = model.Current.getCustomer();

		//Now it's here, maybe the next code needs to be moved
		//As we are in orderCreqtion, maybe we need another property to verify the application status
		if(!this.customer)
		{
			var customerData = model.persistence.Storage.session.get("currentCustomer");
			this.customer  = new model.Customer(customerData);

		};

		this.reqModel = new sap.ui.model.json.JSONModel({"reqDate":null, "reqQty":1});

        //**
        // if(this.order.requestedDate && typeof this.order.requestedDate === "object"){
				//   if(this.order.requestedDate && typeof this.order.requestedDate === "object"){
        //     var parseDt = utils.ParseDate().formatDate(this.order.requestedDate, "dd-MM-yyyy")
        // }
				if(_.isDate(this.order.requestedDate))
				{
					var date = new Date(this.order.requestedDate);
					this.reqModel.setProperty("/reqDate", date);
				}

				this.getView().setModel(this.reqModel, "req");
        //**
		this.resModel = new sap.ui.model.json.JSONModel({"availableDate":""});
		this.getView().setModel(this.resModel, "res");

		var requestAvailabilityPanel = this.getView().byId("productRequestPanelId");
		if(!!requestAvailabilityPanel && requestAvailabilityPanel.getExpanded())
		requestAvailabilityPanel.setExpanded(false);

		//this.product = this.getView().getModel("appStatus").getProperty("/product");
		this.product = new model.Product(model.persistence.Storage.session.get( "currentProduct"));
		//delete(this.getView().getModel("appStatus").getData().product);


		this.userInfo = this.getOrgDataForSAP();
		this.catalogue = new model.Catalogue();

		
		var iva = (this.order && this.order.IVACode)? this.order.IVACode : undefined;
		
		utils.Busy.show();
		

		//Final version with price simulation-----------------------------------------------------
		Q.all([this.catalogue.getAccessoriesByProduct(this.userInfo, this.product.productId), this.product.getPriceDetail(this.customer, iva)])
		.spread(_.bind( function(accessoriesRes, priceRes){
			utils.Busy.hide();
			this.accessories = accessoriesRes;
			this.accessoriesModel = new sap.ui.model.json.JSONModel();
			this.accessoriesModel.setData({"items":this.accessories});
			this.getView().setModel(this.accessoriesModel, "accessories");
			//console.log(priceRes);
			this.refreshView(this.product);

		}, this), _.bind(function(err){
			utils.Busy.hide();
			utils.Message.getSAPErrorMsg(err, "Error loading Product");
			this.refreshView(this.product);
		}, this))
		//---------------------------------------------------------------------------------------

		// this.catalogue.getAccessoriesByProduct(this.userInfo, this.product.productId)
		// .then(_.bind( function(accessoriesRes){
		// 	utils.Busy.hide();
		// 	this.accessories = accessoriesRes;
		// 	this.accessoriesModel = new sap.ui.model.json.JSONModel();
		// 	this.accessoriesModel.setData({"items":this.accessories});
		// 	this.getView().setModel(this.accessoriesModel, "accessories");
		//
		// 	this.refreshView(this.product);
		//
		// }, this), _.bind(function(err){
		// 	utils.Busy.hide();
		// 	utils.Message.getSAPErrorMsg(err, "Error loading Product");
		// 	this.refreshView(this.product);
		// }, this))










	},

	refreshView : function(data)//maybe to abstract
	{
			var enable = {"editable": false};
			var page = this.getView().byId("detailPage");
			this.getView().setModel(data.getModel(), "p");

			enable.editable=false;
			var toolbar = sap.ui.xmlfragment("view.fragment.catalogueToolbar", this);
			page.setFooter(toolbar);
			this.getView().setModel(this.customer.getModel(), "customer");




			// var enableModel = new sap.ui.model.json.JSONModel(enable);
			// this.getView().setModel(enableModel, "en");

	},


	onProductRequestPress:function(evt)
	{
		var src = evt.getSource();
		var panel = src.getParent().getParent();
		if(panel.getExpanded())
			panel.setExpanded(false);
		else {
			panel.setExpanded(true);
		}

	},



	onConfirmCheckPress:function(evt)
	{
		this.uiModel.setProperty("/en", {check:true});
		var reqDate = this.getView().getModel("req").getData().reqDate;
		var reqQty = this.getView().getModel("req").getData().reqQty;
		var userInfo = this.getOrgDataForSAP();

		this.getView().setBusy(true);
		this.product.getAvailability(userInfo, reqQty, reqDate)
		.then(_.bind(
			function(result){
				
				this.refreshView(result);
				this.getView().setBusy(false);
			}, this),
			_.bind(function(err){
				sap.m.MessageToast.show(utils.Message.getError(err));
				this.getView().setBusy(false);
			}, this))



	},
	onDisplayComponentsPress:function(evt)
	{
		if (! this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("view.dialog.componentsPopover", this);
				this.getView().addDependent(this._oPopover);
				// this._oPopover.bindElement("/ProductCollection/0");
			}

			// delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
			var oButton = evt.getSource();
			jQuery.sap.delayedCall(0, this, function () {
				this._oPopover.openBy(oButton);
			});
	},
	onReqQtyChange:function(evt)
	{
		var src = evt.getSource();
		var value = src.getValue();
		this.product.setQuantity(value);
		//To add------------------------------------------
		var iva = undefined;
		if(this.order && this.order.IVACode)
			iva = this.order.IVACode;
		
		this.product.getPriceDetail(this.customer, iva)
		.then(_.bind(function(result){
			this.refreshView(this.product);
		}, this))
		//------------------------------------------
	}








});
