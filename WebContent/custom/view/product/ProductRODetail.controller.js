jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.Positions");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.Discount");
jQuery.sap.require("view.product.AbstractProductDetail");

view.product.AbstractProductDetail.extend("view.product.ProductRODetail", {


	checkRoute : function(route)
	{
		return (route === "productRODetail") || (route === "productROMobileDetail");

	},
	refreshView : function(data)//maybe to abstract
	{
			var enable = {"editable": false};
			var page = this.getView().byId("detailROPage");
			this.getView().setModel(data.getModel(), "p");
			enable.editable=false;
			var toolbar = sap.ui.xmlfragment("view.fragment.catalogueToolbar", this);
			page.setFooter(toolbar);
			this.getView().setModel(this.customer.getModel(), "customer");
			// var enableModel = new sap.ui.model.json.JSONModel(enable);
			// this.getView().setModel(enableModel, "en");

	},
    
    navBackToMobileMaster: function(evt){
      this.router.navTo("readOnlyHierarchyMobile");
    },
    
	onAddPress : function(evt)
	{

				if(!this.cart)
				{
					this.cart = new model.Cart();

					this.cart.create(this.order.getId(), this.customer.getId());
				}

			//------------------------------------------------
			var position = new model.Position();
            var checkProductId = this.product.productId;
            if(this.order.positions){
            var positionsArr = this.order.positions;
            for(var i=0; i<positionsArr.length; i++){
                if(checkProductId===positionsArr[i].productId){
                    sap.m.MessageToast.show(model.i18n.getText("Warning!! Product already added before"));
                    return;
                }
            }
            }

	    position.create(this.order, this.product);
			//Maybe to move the logic in position
			position.setQuantity(this.reqModel.getData().reqQty);
			if(this.reqModel.getData().reqDate)
			{
				position.setWantedDate(new Date((this.reqModel.getData().reqDate)));
			}
			else {
				position.setWantedDate(new Date(this.order.requestedDate));
			}
			// position.setWantedDate(this.reqModel.getData().reqDate);
			//------------------------------------------------
	    this.order.addPosition(position);
	    model.Current.setOrder(this.order);
	    this.cart.setPositions(this.order.getPositions());
	    //console.log(this.order);

			var discount = new model.Discount();
			discount.initialize(this.order.getId(), position);
			// .then(_.bind(function()
			// {
				position.setDiscount(discount);
				//console.log(position);
	      sap.m.MessageToast.show(model.i18n.getText("Product added: "+this.product.getId()));
			// }, this))






	}





});
