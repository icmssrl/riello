jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.Positions");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("utils.Busy");

view.abstract.AbstractController.extend("view.fattureProvvigioni.fattureProvvigioni", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    this.dateModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.dateModel, "f");
    var date = new Date();
    var fromDate = date.setDate(date.getDate()-5);
    this.dateModel.setProperty("/fromDate", new Date(fromDate));
    this.dateModel.setProperty("/toDate", new Date());
    
//    this.valueStateModel.setProperty("valueState", "");

  },


  handleRouteMatched: function (evt) {

    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    var routeName = evt.getParameter("name");

    if (routeName !== "billsCommissions") {
      return;
    }
    if(!this.dateModel.getData().fromDate && !this.dateModel.getData().toDate)
    {
      this.dateModel.setProperty("/fromDate", new Date());
      this.dateModel.setProperty("/toDate", new Date());
    }
    
    
    this.uiModel.setProperty("/enableRegion", false);
    
    this.commissionsModel = new sap.ui.model.json.JSONModel();
    this.commissionsData={"results":[]};
    this.commissionsModel.setData(this.commissionsData);
    this.getView().setModel(this.commissionsModel, "c");
  },
  onFind: function()
  {
    var workingUser = model.persistence.Storage.session.get("workingUser");
    this.orgData = {
      "Bukrs": workingUser.organizationData.results[0].society,
      "Vkorg": workingUser.organizationData.results[0].salesOrg,
      "Vtweg": workingUser.organizationData.results[0].distributionChannel,
      "Spart": workingUser.organizationData.results[0].division
      
    };
    var req = _.clone(this.orgData);
    //---------------It's needed to be an agent? -------------------
    
    
   
    if(workingUser.userGeneralData.userType=="AG")
    {
    	req.Cdage = workingUser.userGeneralData.customer;
    }
    req.startDate = this.dateModel.getProperty("/fromDate");
    req.endDate=this.dateModel.getProperty("/toDate");
    
    var fSuccess = function(res)
    {
      this.commissionsData.results = [];
    	if(res && res.results && res.results.length>0)
    	{
    		for(var i= 0; i<res.results.length; i++)
    			{
    				this.commissionsData.results.push(model.persistence.Serializer.commission.fromSAP(res.results[i]));
    			}
    	}
      else if(res && res.results && res.results.length === 0)
      {
        sap.m.MessageToast.show(model.i18n.getText("NO_DATA_FOUND"));
      }
    	this.commissionsModel.setData(this.commissionsData);
    }
    fSuccess=_.bind(fSuccess, this);
    
    var fError = function(err)
    {
    	this.commissionsData.results=[];
    	this.commissionsModel.setData(this.commissionsData);
    	sap.m.MessageToast.show(utils.Message.getError(err));
    }
    fError = _.bind(fError, this);
    
    model.odata.chiamateOdata.getCommissions(req, fSuccess, fError);
    
  },
  
  onFindTransDate: function()
  {
    sap.m.MessageBox.show("Under Construction");
  }
  
  
});
