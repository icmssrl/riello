jQuery.sap.require("model.User");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("model.NetworkStatus");
jQuery.sap.require("model.persistence.Storage");

view.abstract.AbstractController.extend("view.Login", {

  onExit: function () {

  },


  onInit: function () {
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    this.credentialModel = new sap.ui.model.json.JSONModel();

    this.machineModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.machineModel, "srv");
  },


  handleRouteMatched: function (evt) {

    this.clearValueState();
    var name = evt.getParameter("name");

    if (name !== "login") {
      return;
    }

    if(sessionStorage.getItem("serverId") !== "prd" && sessionStorage.getItem("serverId"))
    {
    	var idServer = sessionStorage.getItem("serverId");
    	var serverText;
    	if(idServer == "test")
    	{
    		serverText = model.i18n.getText("testServer");
    	}
    	if(idServer == "dev")
    	{
    		serverText = model.i18n.getText("developerServer");
    	}
    	this.machineModel.setProperty("/info", serverText);
    }
    this.user = new model.User();
    //var credentialModel = this.user.getNewCredential();
    this.getView().setModel(this.credentialModel, "usr");
    this.clearView();
    model.i18n.setModel(this.getView().getModel("i18n"));

    var locationPath = location.pathname;
    var webCntStr = _.find(location.pathname.split("/"), function(item){ return item.indexOf("WebContent")>=0;});
    webCntStr = webCntStr? webCntStr : "WebContent_XF08101600";
    this.version = webCntStr.substr(webCntStr.indexOf("_")+1, webCntStr.length);
    
  },
  
  onAfterRendering: function (evt) {
    view.abstract.AbstractController.prototype.onAfterRendering.apply(this, arguments);
    $(document).on("keypress", _.bind(function (evt) {
      if (evt.keyCode === 13 && (this.validateCheck())) {
        this.onLoginPress();
      }

    }, this));
  },

  chiamataOData: function (user, pwd) {
	    var defOdata = Q.defer();
	    fError = function (err) {
	    	defOdata.reject(err);
	      sap.m.MessageBox.alert("Utente SAP non trovato", {
	        title: "Attenzione"
	      });
	    };
	    fSuccess = function () {
	      defOdata.resolve();
	      //console.log("credenziali sap");
	    };

	    var success = _.bind(fSuccess, this);
	    var error = _.bind(fError, this);

	    //var tok = "ICMSDEV" + ':' + "icms2016";
	    var tok = user + ":" + pwd;
	    var auth;
	    //**
//	    if (undefined === btoa) {
//	      auth = BASE64.encode;
//	    } else if (window.btoa) {
//	      auth = window.btoa(tok);
//	    } else { //for <= IE9
//	      auth = jQuery.base64.encode(tok);
//	    }
	    auth=jQuery.base64.encode(tok);
	    //**

	    /*
	    if (window.btoa) {
	        auth = window.btoa(tok);
	    } else { //for <= IE9
	        auth = jQuery.base64.encode(tok);
	    }
	    */
	    // var auth = btoa(tok);


	//**
//	    $.ajaxSetup({
//	      headers: {
//	       "Authorization": "Basic " + auth,
//	        "X-CSFR-TOKEN": "fetch"
//	      }
//	    });
	//**    
	    
	    //var url = icms.Component.getMetadata().getConfig().settings.serverUrl;
	    var url = sessionStorage.getItem("serverUrl");
	    $.ajax({
	      url: url,
	      type: "GET",
	      username: user,
	      password: pwd,
	      success: success,
	      error: error,
	      async: false
	    });

	    return defOdata.promise;

  },

  onLoginPress: function (evt) {
    /*
      prima di fare la login cmq faccio una logout
    */
    this.onLogoutPress();
    //**********************

//    var username = this.getView().getModel("usr").getProperty("/username");
//    var pwd = this.getView().getModel("usr").getProperty("/password");
    
    var username = this.getView().byId("login_usr").getValue();
    var pwd = this.getView().byId("login_pwd").getValue();
    model.persistence.Storage.session.save("AppVersion", this.version);

    this.user.setCredential(username, pwd);
    var doLogin = _.bind(this.user.doLogin, this);
    //    var choosePath = _.bind(this.choosePath, this);
    //    var society;
    var division;
    var appModel = this.getView().getModel("appStatus");
    if (appModel.getProperty("/navBackVisible")) {
      appModel.setProperty("/navBackVisible", false);
    }
//------------------------OLD-----------------------------------------------------------------------------------------
//    this.chiamataOData(username, pwd)
//      .then(doLogin(this.user.getCredential().username, this.user.getCredential().password)
//        .then(_.bind(
//          function (result) {
//
//
//            this.user.update(result);
//            //            this.navToLaunchpad(this.user);
//            if (this.user.type === "AM" || this.user.type === "TM") {
//              appModel.setProperty("/isAMTM", true);
//            } else {
//              appModel.setProperty("/isAMTM", false);
//            }
//
//            if (result.organizationData && result.organizationData.results.length === 1) {
//              division = result.organizationData.results[0].division;
//              model.persistence.Storage.session.save("division", division);
//              model.persistence.Storage.session.save("workingUser", this.user);
//              this.router.navTo("launchpad");
//              jQuery.sap.includeStyleSheet("custom/css/custom" + division + ".css", "custom_style");
//            } else {
//
//              this.router.navTo("soLaunchpad");
//            }
//
//
//
//            ////////*******Previous solution with second level tiles****///////
//
//            //                        if (result.organizationData && _.chain(result.organizationData.results).pluck('society').uniq().value().length === 1) {
//            //                            society = result.organizationData.results[0].society;
//            //                            if(result.organizationData.results.length === 1){
//            //                                division = result.organizationData.results[0].division;
//            //                                model.persistence.Storage.session.save("society", society);
//            //                                model.persistence.Storage.session.save("division", division);
//            //                                model.persistence.Storage.session.save("workingUser", this.user);
//            //                                this.router.navTo("launchpad", {"division": division});
//            //                                jQuery.sap.includeStyleSheet("custom/css/custom"+society+division+".css","custom_style");
//            //                            }else{
//            //                                model.persistence.Storage.session.save("society", society) ;
//            //                                this.router.navTo("divisionLaunchpad", {"society":society});
//            //                            }
//            //
//            //                        } else {
//            //                          if (_.chain(result.organizationData.results).pluck('society').uniq().value().length > 1) {
//            //                                this.router.navTo("soLaunchpad");
//            //                          }
//            //                        }
//
//            /////////*************/////////////////////
//
//          }, this)));
    //----------------------------------------------------------------------------------------------------------------------------
    
    //------------------NEW------------------------------------------------------------------------------
    
    var loginError = _.bind(this.errorFunc, this);
    
    var loginSuccess = function(result)
    {
    	this.getView().setBusy(false);
    	if (this.user.type === "AM" || this.user.type === "TM") {
            appModel.setProperty("/isAMTM", true);
          } else {
            appModel.setProperty("/isAMTM", false);
          }
    	model.NetworkStatus.loadNetworkStatus();
    	var result = model.persistence.Serializer.userInfo.fromSAP(result.results);
    	model.persistence.Storage.session.save("sessionUID", result.sessionUID);
          if (result.organizationData && result.organizationData.results.length === 1) {
            division = result.organizationData.results[0].division;
            model.persistence.Storage.session.save("division", division);
            model.persistence.Storage.session.save("workingUser", this.user);
            //model.persistence.Storage.session.save("sessionUID", result.sessionUID);
            this.router.navTo("launchpad");
            jQuery.sap.includeStyleSheet("custom/css/custom" + division + ".css", "custom_style");
          } else {

            this.router.navTo("soLaunchpad");
          }
    }
    loginSuccess = _.bind(loginSuccess, this);
    
   
    
    this.getView().setBusy(true);
    this.chiamataOData(username, pwd)
    .then(_.bind(function(res){
    	this.user.checkPassword()
    	.then(_.bind(function(result){
    		this.user.getUserData()
    		.then(loginSuccess, loginError);
    	},this),
    	_.bind(function(err){
    		this.getView().setBusy(false);
    		var msg = utils.Message.getError(err);
    		var code = utils.Message.getCode(err);
    		sap.m.MessageToast.show(msg);
    		if(code.indexOf("071")>=0)
    		{
    			this.handleNewPassword();
    		}
    		
    	}, this))
    }, this),
    	loginError)
      
    
    
    //------------------------------------------------------------------------------------------------------------
  },
  handleNewPassword : function()
  {
	  if(!this.newPwdDialog)
		{
		  this.newPwdDialog= sap.ui.xmlfragment("view.dialog.newPwdDialog", this);
		}
  	var page = this.getView().byId("loginPage");
  	page.addDependent(this.newPwdDialog);
  	this.newPwdDialog.open();
  },
  handleNewPwdOkPress:function(evt)
  {
	  var newPwd1=this.getView().getModel("usr").getProperty("/newPwd1");
	  var newPwd2=this.getView().getModel("usr").getProperty("/newPwd2");
	  if(newPwd1 !== newPwd2)
	  {
		  sap.m.MessageBox.alert(model.i18n.getText("PASSWORD_NOT_EQUALS"));
		  return
	  }
//	  newPwd1=jQuery.base64.encode(newPwd1);
//	  newPwd2=jQuery.base64.encode(newPwd2);
	  this.getView().setBusy(true);
	  this.user.setNewPassword(newPwd1, newPwd2)
	  .then(_.bind(function(result){
		  sap.m.MessageToast.show(model.i18n.getText("CHANGE_PWD_SUCCESS"));
		  jQuery.sap.delayedCall(3000, this, _.bind(function(){
			  this.newPwdDialog.close();
			  this.user = new model.User();
			  this.clearView();
			  this.getView().setBusy(false);
			  this.doLogout();
		  }, this));
	  }, this),
			_.bind(function(err){this.errorFunc(err);}, this))
  },
  
  handleNewPwdCancelPress:function(evt)
  {
	  this.newPwdDialog.close();
	  this.handleNewPwdResetPress();
  },
  handleNewPwdResetPress:function(evt)
  {
	  this.getView().getModel("usr").setProperty("/newPwd1", "");
	  this.getView().getModel("usr").setProperty("/newPwd2", "");
	  this.getView().getModel("usr").refresh();
  },
  errorFunc: function(err)
  {
	  this.getView().setBusy(false);
	  sap.m.MessageToast.show(utils.Message.getError(err));
  },
  clearView : function()
  {
	  this.credentialModel.setData({"username":"", "password":"", "newPwd1":"", "newPwd2":""});
  },


  //  choosePath: function () {
  //    if (!this.user.organizationData || _.chain(this.user.organizationData).pluck('society').uniq().value().length === 1) {
  //      this.router.navTo("launchpad");
  //
  //    } else {
  //      if (_.chain(this.user.organizationData).pluck('society').uniq().value().length > 1) {
  //        this.router.navTo("soLaunchpad");
  //      }
  //    }
  //  },

  onSwitchLanguage: function (oEvent) {
    var control = oEvent.getSource();
    var buttonID = control.getId();
    var oI18nModel;
    switch (buttonID) {
    case "loginId--italyFlag":

      sap.ui.getCore().getConfiguration().setLanguage("it_IT");
      sessionStorage.setItem("language", "it_IT");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      location.reload();

      break;
    case "loginId--britainFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_EN");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      location.reload();
      break;
    case "loginId--usaFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_US");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      location.reload();
      break;
    case "loginId--spainFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_EN");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      break;
    case "loginId--germanyFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_EN");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      break;
    case "loginId--franceFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_EN");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      break;
    default:
      sap.ui.getCore().getConfiguration().setLanguage("it_IT");
      sessionStorage.setItem("language", "it_IT");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });



    }
    //        var state = control.getState();
    //        var i18nModel;
    //        if (state) {
    //            sap.ui.getCore().getConfiguration().setLanguage("EN");
    //            //i18nModel = new sap.ui.model.resource.ResourceModel({bundleUrl: "custom/i18n/text_en.properties"});
    //            //model.i18n.setModel(i18nModel);
    //            //sap.ui.getCore().setModel(i18nModel, "i18n");
    //        } else {
    //            sap.ui.getCore().getConfiguration().setLanguage("IT");
    ////            i18nModel = new sap.ui.model.resource.ResourceModel({bundleUrl:"custom/i18n/text_it.properties"});
    ////            model.i18n.setModel(i18nModel);
    //            //sap.ui.getCore().setModel(i18nModel, "i18n");
    //        }
  }








});