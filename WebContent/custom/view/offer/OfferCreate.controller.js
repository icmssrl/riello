jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("model.collections.Orders");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Product");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Discount");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Destination");
//jQuery.sap.require("model.persistence.PersistenceManager");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.collections.DiscountItems");

view.abstract.AbstractMasterController.extend("view.offer.OfferCreate", {

  onExit: function () {

  },


  onInit: function () {
    this.filelist = [];
    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    //this.notesModel = new sap.ui.model.json.JSONModel();
    //this.notesModel.setData({"billsItems":[], "salesItems":[]});
    //this.getView().setModel(this.notesModel,"notes");

    var oModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(oModel);


    this.newAddress = {
      name: "",
      street: "",
      streetNumber: "",
      zipCode: "",
      city: "",
      nation: "",
      tel: "",
      prov: "",

    };


    this.fdestinationsModel = new sap.ui.model.json.JSONModel();
    this.fdestinationsModel.setData(this.newAddress);
    this.getView().setModel(this.fdestinationsModel, "fdest");

//    this.camionGruModel = new sap.ui.model.json.JSONModel();
//    this.getView().setModel(this.camionGruModel, "camionGru");

    var gruModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(gruModel, "gru");
    
    this.orderModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.orderModel, "o");
    
    this.destinationsModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.destinationsModel, "d");
    
    this.initializeOfferOrderSelectionDialog();
  },





  //---------------------------NEW------------------------------------------------------------------------------------------------
  handleRouteMatched: function (evt) {


    var name = evt.getParameter("name");


    if (name !== "newOffer") {
      return;
    }

    model.persistence.Storage.session.save("OrderType", "Offer");//Probably to erase
    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    this.appModel = this.getView().getModel("appStatus");

    //**
    this.uiModel.setProperty("/orderInfo", false);
    this.uiModel.setProperty("/orderCreate", true);

    this.uiModel.setProperty("/newDestination", true);
    this.uiModel.setProperty("/enableRegion", false);
    this.uiModel.refresh();
    //**
    
    this.user = model.persistence.Storage.session.get("workingUser");
    this.getView().getModel("wo").setProperty("/isNewTradePolicies", this.checkQuotationEnabledToTradePolicies());
    
    var deviceType = "Desktop";
    if (sap.ui.Device.system.tablet) {
      deviceType = "Tablet";
    } else if (sap.ui.Device.system.phone) {
      deviceType = "Phone";
    }
    this.getView().getModel("ui").setProperty("/deviceType", deviceType);

    this.getView().getModel("ui").setProperty("/cantiereFlag", this.user.organizationData.results[0].cantiereFlag);
    //**
    //this.setElementsToValidate();
    this.resetValues();

    this.constructorData = _.clone(this.newAddress);
    this.constructorData.constructor = "CANTIERE";
    this.altDestData = _.clone(this.newAddress);
    this.altDestData.constructor = "ALTRODEST";
    //**
    this.createBackupData();

    //--------------User-------------------------------------------------------------
    //  this.user = model.persistence.Storage.session.get("workingUser");
    //  var userModel


    //------------------------------------------------------------------------------

    //----------------------------------------customerModel----------------------------
    this.customer = model.Current.getCustomer();
    if (!this.customer) {
      var customerData = model.persistence.Storage.session.get("currentCustomer");
      this.customer = new model.Customer(customerData);
    }
    this.getView().setModel(this.customer.getModel(), "customer");
    //-------------------------------------------------------------------------------


    this.populateSelect();
    this.populateDestinationsSelect();//Why asynchronous? It's a mock json data...
    this.loadPreviousOrder();

    this.loadDestinations()//Why async? It's in sessionStorage
      .then(_.bind(function () {
        this.setOrderModel();
        this.selectOrderOfferType();
        
      }, this))



  },
  setOrderModel: function () {
    this.order = model.Current.getOrder();
   if (this.order) {

      var destinations = {
        "items": []
      };
      
      this.order.initializeCustomerDiscounts(this.customer);//Stay Tuned
      
      destinations.items = this.customer.destinations;
      this.destinationsModel.setData(destinations);
      //this.destinationsModel = new sap.ui.model.json.JSONModel(destinations);
      //this.getView().setModel(this.destinationsModel, "d");
      this.orderModel.setData(this.order);
      this.setDefaultValues();
    }


    if (!this.order) {
      this.order = new model.Order();
      //**
      //this.order.requestedDate = this.getView().byId("orderHeaderDate").getProperty("dateValue");
      //**
      this.order.create(this.customer);
      //** in seguito a questa carichiamo i valori di default da una tabella custom e risettiamo il modello order.

      utils.Busy.show();
      var fSuccess = function (res) {
        utils.Busy.hide();
        this.orderModel.setData(this.order);
        this.setDefaultValues();

      };
      var fError = function (err) {
        utils.Busy.hide();
        sap.m.MessageToast.show(utils.Message.getSAPErrorMsg());
        //console.log(utils.Message.getSAPErrorMsg());
      };
      fSuccess = _.bind(fSuccess, this);
      fError = _.bind(fError, this);

      var req = this.getOrgDataForSAP();
      this.order.loadDefaultValues(req).
      then(fSuccess, fError);

      
      //**



    }

    /* queste funzioni sono state spostate nella funzione di successo della loaddefaulvalues, e richiamate anche in caso di
    presenza ordine.
      this.orderModel = new sap.ui.model.json.JSONModel();
      this.orderModel.setData(this.order);
      if (!!this.orderModel.getData().resa1 && this.orderModel.getData().resa1 !== "EXW")
    	{
    		if (!!this.orderModel.getData().contactPerson){
    			this.order.contactPerson = "";
    			this.orderModel.getData().contactPerson = "";
    		}

    	}

    	if(this.orderModel.getData().resa1 === "EXW" || this.order.resa1 === "EXW"){
    		this.getView().byId("contactPersonLabel").setVisible(true);
    		this.getView().byId("contactPersonInput").setVisible(true);
    	}

      this.getView().setModel(this.orderModel, "o");
      this.initializeValues();
      this.setIntervalLocalSaving();
      */
  },

  createBackupData: function () {
    var infoForReq = this.getOrgDataForSAP();

    ///***Creazione dati di backup per lo user in localStorage*****//////////
    var backupUserData = {};
    var username = this.getUserInfo().username;
    var selectedCustomerId = this.getCustomerInfo().customerId;
    backupUserData.username = username;
    backupUserData.customerId = selectedCustomerId;
    backupUserData.Bukrs = infoForReq.Bukrs;
    backupUserData.Vkorg = infoForReq.Vkorg;
    backupUserData.Vtweg = infoForReq.Vtweg;
    backupUserData.Spart = infoForReq.Spart;
    model.persistence.PersistenceManager.saveOrgDataSelection(username + selectedCustomerId + "UBackup", backupUserData);
  },
  loadPreviousOrder: function () {
    var restoreOrder = this.appModel.getProperty("/restoreOrder");
    var username = this.getUserInfo().username;
    var selectedCustomerId = this.getCustomerInfo().customerId;
    if (restoreOrder === true) {

      this.appModel.setProperty("/restoreOrder", false); //To check
      var orderBackup = model.persistence.PersistenceManager.loadModel(username + selectedCustomerId + "OBackup");
      var cloneOrder = jQuery.extend(true, {}, orderBackup);
      //        this.order = new model.Order();
      //        this.order.update(orderBackup);

      if (!!cloneOrder.fullEvasionAvailableDate) {
        cloneOrder.fullEvasionAvailableDate = new Date(cloneOrder.fullEvasionAvailableDate);
      }
      if (!!cloneOrder.requestedDate) {
        cloneOrder.requestedDate = new Date(cloneOrder.requestedDate);
      }
      if (!!cloneOrder.validDateList) {
        cloneOrder.validDateList = new Date(cloneOrder.validDateList);
      }

      this.order = new model.Order();
      this.order.update(cloneOrder);
      //        var positionsArr = [];

      if (cloneOrder.positions || cloneOrder.positions.length >= 0) {
        //delete this.order.positions;
    	  this.order.positions = [];//
    	  
        for (var i = 0; i < cloneOrder.positions.length; i++) {
          var position = new model.Position();
          var product = new model.Product();
          if (!!cloneOrder.positions[i].availableDate)
            cloneOrder.positions[i].availableDate = new Date(cloneOrder.positions[i].availableDate);
          if (!!cloneOrder.positions[i].wantedDate)
            cloneOrder.positions[i].wantedDate = new Date(cloneOrder.positions[i].wantedDate);
          position.update(cloneOrder.positions[i]);
          if (cloneOrder.positions[i].product) {
            if (!!cloneOrder.positions[i].product.availableDate)
              cloneOrder.positions[i].product.availableDate = new Date(cloneOrder.positions[i].product.availableDate);
            if (!!cloneOrder.positions[i].product.reqDate)
              cloneOrder.positions[i].product.reqDate = new Date(cloneOrder.positions[i].product.reqDate);
            product.update(cloneOrder.positions[i].product);

            position.setProduct(product);


          }
          //this.order.addPosition(position);
          this.order.positions.push(position);
          
          //-----------------------Old-------------------------------------
          // var discount = new model.Discount();
          // discount.initialize(orderModel.getId(), position);
          // position.setDiscount(discount);
          //--------------------------------------------------------------------
          //-----------------------New----------------------------------------
          var discount = new model.Discount();
          discount.update(position.discount);
          position.setDiscount(discount);
          //------------------------------------------------------------
          //                        orderModel.addPosition(position);
          //                        positionsArr.push(position);


        }
        //                    orderModel.positions = positionsArr;
        model.Current.setOrder(this.order);
        //                    model.persistence.Storage.session.save("currentOrder", orderModel);
        //this.order = orderModel;
        // this.order.requestedDate = this.getView().byId("orderHeaderDate").getProperty("dateValue");
        if (!this.cart) {
          this.cart = new model.Cart();

          this.cart.create(this.order.getId(), this.customer.getId());
        }
        this.cart.setPositions(this.order.getPositions());
      }
      // this.refreshView(this.order);
    }
  },

  setIntervalLocalSaving: function () {
    //--------------------------Set Interval local order saving ----------------------------------------------------------

    var username = this.getUserInfo().username;
    var selectedCustomerId = this.getCustomerInfo().customerId;

    var intervalIsRunning = this.appModel.getProperty("/intervalIsRunning");
    if (intervalIsRunning === true) {
      //don't do nothing
    } else {
      //start the interval
      var that = this;
      window.backupInterval = setInterval(function () {
        model.persistence.PersistenceManager.saveModel(username + selectedCustomerId + "OBackup", that.order);
        that.appModel.setProperty("/intervalIsRunning", true);
      }, 5000);
    }
  },
  loadDestinations: function () {
    ////*************Destination loading**********************//////////

    var infoForReq = this.getOrgDataForSAP();
    var agentCode = this.getCustomerInfo().agentCode;
    infoForReq.KunnrAg = JSON.parse(sessionStorage.getItem("currentCustomer")).registry.id;
    infoForReq.Parvw = "WE";
    infoForReq.Cdage = agentCode;
    delete infoForReq.Bukrs;

    var defer = Q.defer();
    var fSuccess = function (result) {
      this.dests = {
        "items": []
      };
      //destinations.items = this.customer.destinations;
      (this.dests).items = result;
      this.customer.setDestinations(result);
      //**
      //this.destinationsModel = new sap.ui.model.json.JSONModel(this.dests);
      this.destinationsModel.setData(this.dests);
      //this.getView().setModel(this.destinationsModel, "d");
      defer.resolve(result);

    };

    var fError = function (err) {
      //console.log(err);
      defer.reject(err);
      sap.m.MessageToast.show("problems loading destinations");
    };

    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);
    var destModel = new model.Destination;
    destModel.loadDestinations(infoForReq).then(fSuccess, fError);

    return defer.promise;
    //----------------------------------------------------------------------------------
    //**
  },

  //-------------------------------------------------------------------------------------------------------------------------------
  resetValues: function () {
    this.resetForm();
    this.resetGruForm();
    var constructorSwitch = this.getView().byId("constructorSwitch");
    constructorSwitch.setState(false);
    var gruSwitch = this.getView().byId("gruSwitch");
    gruSwitch.setState(false);

  },
  initializeValues: function () {
    this.initializeDestinations();
    this.initializeGru();
    this.initializeIVA();
    this.initializeOrderType();
    this.initializeNotes();
    this.getView().getModel("o").refresh();

  },
  initializeNotes: function () {
    var notesData = this.getView().getModel("notes").getData();
    if (notesData && notesData.results && notesData.results.length > 0)

    {
      var notes = notesData.results;
      var billNote = _.find(notes, {
        Tdid: "Z002"
      });
      var salesNote = _.find(notes, {
        Tdid: "Z001"
      });
      var creditNote = _.find(notes, {
        Tdid: "Z013"
      });

      this.order.billNoteTxt = billNote ? billNote.Tdtext : "";
      this.order.salesNoteTxt = salesNote ? salesNote.Tdtext : "";
      this.order.creditNoteTxt = creditNote ? creditNote.Tdtext : "";
      this.getView().getModel("o").refresh();
      this.getView().byId("creditNotes").setValue(null);
      this.getView().byId("billsNotes").setValue(null);
      this.getView().byId("salesNotes").setValue(null);


    }
  },
  initializeDestinations: function () {

    (!this.order.alternativeDestination || !this.order.alternativeDestination.constructor) ? this.getView().byId("constructorSwitch").setState(false): this.getView().byId("constructorSwitch").setState(true);

    if (this.order.destination === "ALTRODEST") {
      this.getView().byId("constructorSwitch").setState(false);
      this.getView().byId("destTypes").setSelectedKey("02");
      this.order.finalDestination = _.cloneDeep(this.order.alternativeDestination);
      this.altDestData = _.cloneDeep(this.order.alternativeDestination);
      
      if(this.order.alternativeDestination.prov)
	  {
	  	this.uiModel.setProperty("/enableRegion", true);
	  }

      this.getView().getModel("appStatus").setProperty("/enableDestination", false);
    } else if (this.order.destination === "CANTIERE") {
      this.getView().byId("constructorSwitch").setState(true);
      this.getView().byId("destTypes").setSelection("false");;
      this.order.finalDestination = this.order.alternativeDestination;
      this.constructorData = this.order.alternativeDestination;
      if(this.order.alternativeDestination.prov)
	  {
	  	this.uiModel.setProperty("/enableRegion", true);
	  }
      this.getView().getModel("appStatus").setProperty("/enableDestination", false);
    } else {
      this.getView().byId("constructorSwitch").setState(false);
      this.getView().byId("destTypes").setSelectedKey("01");


      //-------------------------------------Old-----------------------------
      //this.order.finalDestination = this.order.destinationName;

      //-----------------------------------------------------------

      //------------New-----------------------------------------------

      var destinations = this.destinationsModel.getData();
      if(!this.order.destination)
    {
    	  var customerDest = _.find(destinations.items, {codiceDestinazione:this.customer.registry.id});

    	  this.order.destination = customerDest ? customerDest.codiceDestinazione : destinations.items[0].codiceDestinazione;
    }
      var destinationItem = _.find(destinations.items, {codiceDestinazione : this.order.destination});
//      this.order.finalDestination = {
//        name: destinations.items[0].company,
//        street: destinations.items[0].address,
//        streetNumber: destinations.items[0].numAddr,
//        zipCode: this._formatZipCode(destinations.items[0].cityCode),
//        city: destinations.items[0].city,
//        nation: destinations.items[0].country,
//        prov: destinations.items[0].city //To change
//      };
      
      if(destinationItem)
    	  this.order.finalDestination = {
    	        name: destinationItem.company,
    	        street: destinationItem.address,
    	        streetNumber: destinationItem.numAddr,
    	        zipCode: this._formatZipCode(destinationItem.cityCode),
    	        city: destinationItem.city,
    	        nation: destinationItem.country,
    	        prov: destinationItem.prov
    	      };

      //------------------------------------------------------------------
      this.getView().getModel("appStatus").setProperty("/enableDestination", true);
      this.getView().getModel("o").refresh();
    }
  },
  initializeIVA: function () {

    if (!this.order.IVACode) {
      //      this.getView().byId("IVATypes").setSelectedKey("1");
      //      var fullIVAItem = _.find(this.getView().getModel("ivaType").getData().results, {"Taxk1":"1"});//To change with flag
      var fullIVAItem = _.find(this.getView().getModel("ivaType").getData().results, {
        "Xfulltax": "X"
      });
      this.order.IVACode = fullIVAItem.Taxk1;
      this.getView().byId("IVATypes").setSelectedKey(fullIVAItem.Taxk1);
      this.getView().getModel("ui").setProperty("/previousIVA", fullIVAItem);
    }
  },
  initializeOrderType: function () {
    var item = {
      "type": "Auart",
      "namespace": "basketType"
    };
    var workingUser = model.persistence.Storage.session.get("workingUser");
    this.orgData = {
      "Bukrs": workingUser.organizationData.results[0].society,
      "Vkorg": workingUser.organizationData.results[0].salesOrg,
      "Vtweg": workingUser.organizationData.results[0].distributionChannel,
      "Spart": workingUser.organizationData.results[0].division
    };

    utils.Collections.getOdataSelect(item.type, this.orgData)
      .then(_.bind(function (result) {
        if (!this.getView().getModel(item.namespace))
          this.getView().setModel(result, item.namespace);

        if (!this.order.basketType)
          this.order.basketType = result.getData().results[0].Auart;
        this.getView().getModel("o").setData(this.order);

      }, this))
  },


  initializeCart: function () {
    if (!model.Current.getHistoryCart()) {

      return;
    }
    // if(this.cart)
    //   return;

    this.cart = model.Current.getHistoryCart(); //To add setOrder

    _.forEach(this.cart.positions, _.bind(function (item) {
      var position = new model.Position();
      var product = new model.Product(item.product);
      position.create(this.order, product);
      position.setQuantity(item.quantity);
      // position.setWantedDate(new Date(utils.Formatter.formatDateValue((this.reqModel.getData().reqDate))));
      position.setWantedDate(new Date(this.order.requestedDate));
      this.order.addPosition(position);
      var discount = new model.Discount();
      discount.initialize(this.order.getId(), position);
      position.setDiscount(discount);

    }, this))


  },




  refreshView: function (data) {
    this.getView().setModel(data.getModel(), "o");
    // var masterCntrl = this.getView().getModel("appStatus").getProperty("/masterCntrl");
    // masterCntrl.refreshList();
  },
  _formatZipCode: function (cap) {
    var numArray = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    var min = _.chain(numArray).map(function (item) {
      return cap.indexOf(item)
    }).remove(function (obj) {
      return obj >= 0
    }).min().value()
    var result = _.clone(cap);
    result = cap.substring(min, cap.length);
    return result;
  },

  //Feed
  onPost: function (oEvent) {

    var id = oEvent.getSource().getId();
    var noteType = id.substr(id.lastIndexOf("-") + 1, id.length);


    // create new entry
    var sValue = oEvent.getParameter("value");
    var oEntry = {
      author: model.persistence.Storage.session.get("user").fullName,
      text: sValue
    };

    this.order.addNote(noteType, oEntry);

    // update model
    this.getView().getModel("o").setData(this.order);
    // this.getView().getModel("o").refresh();
    // this.refreshView(this.order);

  },



  onSavePress: function () {

    var saveButton = this.getView().byId("saveButton");
    saveButton.setEnabled(false);

    model.Current.setOrder(this.order);
    model.persistence.Storage.session.save("currentOrder", this.order);

  },
  onResetPress: function () {
    this.order = new model.Order();
    this.getView().getModel("o").refresh();
  },

  populateDestinationsSelect: function () {


    var name = "destinationTypes_" + sap.ui.getCore().getConfiguration().getLanguage();
    var pToSelArr = [
      {
        "type": name,
        "namespace": "dest"
       }
   		];

    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.getModel(item.type)
        .then(_.bind(function (result) {
          //this.user = model.persistence.Storage.session.get("workingUser");
          var model = new sap.ui.model.json.JSONModel();
          var data = _.cloneDeep(result.getData());
          if (!this.user.organizationData.results[0].altroDestFlag) {
            _.remove(data.items, {
              id: "02"
            });
          }
          model.setData(data);
          this.getView().setModel(model, item.namespace);

        }, this))
    }, this));
  },


  keyUpFunc: function (e) {
    if (e.keyCode == 27) {
      // codice per il pulsante escape per evitare che lo user chiuda il dialog via ESC

      if (this.insertNewAddressDialog) {
        // this.insertNewAddressDialog.destroy();
        this.insertNewAddressDialog.close();
      }
      $(document).off("keyup");
      //                    this.router.navTo("launchpad");
    }
  },

  handleDestinationChange: function (evt) {
    var source = evt.getSource();
    var selectedItem = source.getSelectedItem().getKey();
    var finalDestButton = this.getView().byId("finalDestination");
    //    finalDestButton.setValue("");
    this.order.destination = null;
    this.order.finalDestination = {};
    if (selectedItem === "01") {
      this.appModel.setProperty("/enableDestination", true);
      //**
      this.order.removeAlternativeDestination();
      this.initializeDestinations();
      //**
    } else {
      var cb = this.getView().byId("destinationComboBox");
      //= "ALTRODEST" ?
      // cb.setValue(cb.getDefaultSelectedItem()); //Lorenzo commented this
      //this.resetForm();

      this.appModel.setProperty("/enableDestination", false);
      this.insertNewAddressDialog = sap.ui.xmlfragment("view.dialog.insertNewAddress", this);
      var page = this.getView().byId("orderCreate");
      this.getView().getModel("fdest").setData(this.altDestData);
      page.addDependent(this.insertNewAddressDialog);
      this.insertNewAddressDialog.open();
      $(document).keyup(_.bind(this.keyUpFunc, this));
      // source.setSelection("false"); // Maybe to move in handleRoute
    }
    this.getView().byId("constructorSwitch").setState(false);
  },
  handleSwitchGru: function (evt) {
    var isGru = evt.getParameter("state");
    this.getView().getModel("appStatus").setProperty("/enDeliveryTypes", isGru);
    if (isGru) {

      // this.initializeGru();
      // this.resetGruForm();
      if (sap.ui.getCore().byId("dialogCamionGru"))
        sap.ui.getCore().byId("dialogCamionGru").destroy(true);
      this.camionGru = sap.ui.xmlfragment("view.dialog.camionGru", this);
      var page = this.getView().byId("orderCreate");
      page.addDependent(this.camionGru);
      this.getView().getModel("gru").setData(this.gruData);
      this.camionGru.open();

      $(document).keyup(_.bind(this.keyUpFunc, this));
    } else {
      this.order.setDeliveryType("");
      this.getView().getModel("o").refresh();
    }

  },
  resetGruForm: function () {


      var tempGruData = {};
      tempGruData.location = "";
      tempGruData.length = 0;
      tempGruData.contact = "";
      tempGruData.tel = "";
      tempGruData.notes = "";

      this.getView().getModel("gru").setData(tempGruData);

  },

  initializeGru: function () {

    if (this.order.gru) {
      this.gruData = _.clone(this.order.gru);
      this.getView().getModel("gru").setData(_.clone(this.gruData));
      return;
    }

      this.gruData = {};
      this.gruData.location = "";
      this.gruData.length = 0;
      this.gruData.contact = "";
      this.gruData.tel = "";
      this.gruData.notes = "";


    this.getView().getModel("gru").setData(_.clone(this.gruData));
  },
  onCamionGruSave: function (evt) {
    this.setElementsToValidate("gru");
    if (this.validateCheckGru()) {
      this.order.setDeliveryType("11");
      this.order.setCamionGruData(this.getView().getModel("gru").getData());

      this.camionGru.close();
      if (this.camionGru) {
        this.camionGru.destroy();
      }
      this.getView().getModel("o").refresh();
    }

  },

  validateCheckGru: function () {
    var result = true;
    if (!this._checkingGru || this._checkingGru.length === 0)
      return true;


    for (var i = 0; i < this._checkingGru.length; i++) {
      if (!this.checkInputFieldGru(this._checkingGru[i])) {
        if (this._checkingGru[i].data("reqGru") === "true") {
          result = false;
        }
      }
    }
    return result;
  },

  checkInputFieldGru: function (evt) {

    var control = evt.getSource ? evt.getSource() : evt;
    var infoControl = control.data("reqGru");
    var typeControl = control.data("input");
    var correct = true;
    if (infoControl === "true" || control.getValue() !== "") {
      switch (typeControl) {


        default: correct = utils.Validator.required(control);
        break;


      }
    }
    return correct;
  },

  onCamionGruCancel: function (evt) {
    this.order.setDeliveryType(this.order.deliveryType);

    this.camionGru.close();
    if (this.camionGru) {
      this.camionGru.destroy();
    }
    this.getView().getModel("o").refresh();
    if (this.order.deliveryType !== "11") {
      this.getView().byId("gruSwitch").setState(false);
    }

  },

  onCamionGruReset: function (evt) {
    this.resetGruForm();
  },

  handleSwitchConstructor: function (selected) {
    this.constructor = selected.getParameter("state");
    this.getView().byId("destTypes").setSelection("false");
    if (selected.getParameter("state")) {

      this.appModel.setProperty("/enableDestination", false);
      //this.resetForm();
      this.getView().getModel("fdest").setData(this.constructorData);
      this.insertNewAddressDialog = sap.ui.xmlfragment("view.dialog.insertNewAddress", this);
      var page = this.getView().byId("orderCreate");
      page.addDependent(this.insertNewAddressDialog);
      this.insertNewAddressDialog.open();
      $(document).keyup(_.bind(this.keyUpFunc, this));
    } else {
      // this.order.alternativeDestination.constructor = "";
      this.order.alternativeDestination = {};
      this.order.destination = null;
      this.initializeDestinations();
      // this.order.finalDestination = {};
    }
    this.getView().getModel("o").refresh();
  },


  resetForm: function (form) {
    var newAddress = {
      name: "",
      street: "",
      streetNumber: "",
      zipCode: "",
      city: "",
      nation: "",
      tel: ""
    };
    this.fdestinationsModel.setData(newAddress);



  },



  setFinalDestination: function (evt) {
    var source = evt.getSource ? evt.getSource() : null;
    //var fDestInput = this.getView().byId("finalDestination");

    //---------------------CHECK ME!------------------------------
    // var selectedItem = source.getSelectedItem().getText();
    // this.order.finalDestination = {};
    // (this.order.finalDestination).street = selectedItem;
    //------------------------------------------------------------
    var selectedDest = source ? source.getSelectedItem().getBindingContext("d").getObject() : evt;
    this.order.finalDestination = {
      name: evt.name ? evt.name : selectedDest.company,
      street: evt.street ? evt.street : selectedDest.address,
      streetNumber: evt.streetNumber ? evt.streetNumber : selectedDest.numAddr,
      zipCode: this._formatZipCode(evt.zipCode ? evt.zipCode : selectedDest.cityCode),
      city: evt.city ? evt.city : selectedDest.city,
      nation: evt.nation ? evt.nation : selectedDest.country,
      prov:evt.prov? evt.prov: selectedDest.prov
    //To change
    };
    this.getView().getModel("o").refresh();
    //fDestInput.setValue(selectedItem);
  },

  onInsertNewAddressCancel: function (obj) {
    //**
    var sw = this.getView().byId("constructorSwitch");
    if (sw.getState()) {
      sw.setState(false);
    }
    var c = this.getView().byId("destTypes")
      // c.setVisible(true);
      //c.setSelection("false");
    this.order.destination = null;
    c.setSelectedKey("01");
    this.initializeDestinations();
    // c.setValue(c.getDefaultSelectedItem());
    // //**
    //---------------------------------------------------------------------------
    //var finalDestButton = this.getView().byId("finalDestination");
    //finalDestButton.setValue("");
    //---------------------------------------------------------------------------
    if (this.insertNewAddressDialog) {
      // this.insertNewAddressDialog.destroy();
      this.insertNewAddressDialog.close();
    }
  },

  onInsertNewAddressReset: function (evt) {
    if (this.getView().getModel("fdest").getData().constructor == "ALTRODEST") {
      this.resetAltDest();
    }
    if (this.getView().getModel("fdest").getData().constructor == "CANTIERE") {
      this.resetConstructor();
    }
    this.getView().getModel("fdest").refresh();
  },
  resetAltDest: function (evt) {
    for (var prop in this.newAddress) {
      this.altDestData[prop] = this.newAddress[prop]
    }
    this.altDestData.constructor = "ALTRODEST";
  },
  resetConstructor: function (evt) {
    for (var prop in this.newAddress) {
      this.constructorData[prop] = this.newAddress[prop]
    }
    this.constructorData.constructor = "CANTIERE";
  },

  validateCheckAltDest: function () {
    var result = true;
    if (!this._checkingAltDests || this._checkingAltDests.length === 0)
      return true;


    for (var i = 0; i < this._checkingAltDests.length; i++) {
      if (!this.checkInputFieldAltDest(this._checkingAltDests[i])) {
        if (this._checkingAltDests[i].data("reqAltDest") === "true") {
          result = false;
        }
      }
    }
    return result;
  },

  checkInputFieldAltDest: function (evt) {

    var control = evt.getSource ? evt.getSource() : evt;
    var infoControl = control.data("reqAltDest");
    var typeControl = control.data("input");
    var correct = true;
    if (infoControl === "true" || control.getValue() !== "") {
      switch (typeControl) {


        default: correct = utils.Validator.required(control);
        break;


      }
    }
    return correct;
  },



  onInsertNewAddressConfirm: function () {
    this.setElementsToValidate("altDest");
    if (this.validateCheckAltDest()) {
      var fDestModel = this.getView().getModel("fdest").getData();
      var d = this.getView().byId("constructorSwitch").getState();

      //------------------------It should have been done before----------------------------
      // if (d) {
      //   fDestModel.constructor = "CANTIERE";
      //
      //   // this.getView().byId("destTypes").setVisible(false);
      // } else {
      //   fDestModel.constructor = "ALTRODEST";
      //   // this.getView().byId("destTypes").setVisible(true);
      // }
      //---------------------------------------------------------------------
      this.order.setAlternativeDestination(_.clone(fDestModel));
      //----------------------------OLD-------------------------------------------------------------
      // this.order.finalDestination = {};
      // (this.order.finalDestination).street = this.order.alternativeDestination.street;
      //------------------------------------------------------------------------------------------
      this.setFinalDestination(_.clone(fDestModel));
      //var street = fDestModel.street;
      //var streetNumber = fDestModel.streetNumber;

      //    var fDestInput = this.getView().byId("finalDestination");
      //    fDestInput.setValue(this.order.alternativeDestination.street + " " + this.order.alternativeDestination.streetNumber);
      if (this.insertNewAddressDialog) {
        this.insertNewAddressDialog.close();
        // this.insertNewAddressDialog.destroy();
      }
      this.getView().getModel("o").refresh();


      //
      // for (var prop in fDestModel) {
      //   fDestModel[prop] = "";
      // }
    }
  },

  handleResaSelectionChange: function (evt) {
    var source = evt.getSource();
    var selectedItem = source.getSelectedItem().getKey();


	    if (selectedItem === "11" || selectedItem === "EXW") {

	    if(selectedItem === "11")
	    	this.getView().getModel("ui").setProperty("/fromAppointment", true);
	    if(selectedItem === "EXW")
	    	this.getView().getModel("ui").setProperty("/fromAppointment", false);
	      //this.appModel.setProperty("/enableContactPerson", true);
	      if(!this.insertContactPerson)
	    	  this.insertContactPerson = sap.ui.xmlfragment("view.dialog.insertContactPerson", this);
	      var page = this.getView().byId("orderCreate");

	      page.addDependent(this.insertContactPerson);
	      this.insertContactPerson.open();
	      $(document).keyup(_.bind(this.keyUpFunc, this));
	    }
//	    else {
//	    	this.order.contactPerson = "";
//	      //this.appModel.setProperty("/enableContactPerson", false);
//
//
//	    }


  },

  onInsertContactPersonReset: function (evt) {
    var source = evt.getSource();
    var inputField = sap.ui.getCore().byId("contactPersonDialogInput");
    var inputFieldTel = sap.ui.getCore().byId("contactPersonTelDialogInput");
    inputField.setValue("");
    inputFieldTel.setValue("");
  },

  onInsertContactPersonCancel: function (evt) {
    var source = evt.getSource();
    var inputField = sap.ui.getCore().byId("contactPersonDialogInput");
    var inputFieldTel = sap.ui.getCore().byId("contactPersonTelDialogInput");
    inputField.setValue("");
    inputFieldTel.setValue("");
    if (this.insertContactPerson) {
      this.insertContactPerson.close();
    }
  },

  onInsertContactPersonConfirm: function (evt) {
    var source = evt.getSource();
    var contactPersonFromDialog = sap.ui.getCore().byId("contactPersonDialogInput");
    var contactPersonInput = this.getView().byId("contactPersonInput");
    contactPersonInput.setValue(contactPersonFromDialog.getValue());
    if (this.insertContactPerson) {
      this.insertContactPerson.close();
    }
  },



  populateSelect: function () {
    //HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
    //HP2 : Every collections contains an array of params where a select is needed
    // var pToSelArr = [
    // 	{"type":"registryTypes", "namespace":"rt"},
    // 	{"type":"billTypes", "namespace":"bt"},
    // 	{"type":"contactTypes", "namespace":"ct"},
    // 	{"type":"paymentConditions", "namespace": "pc"},
    // 	{"type": "places", "namespace":"p"},
    //         {"type": "customerTypes", "namespace":"customerType"}
    // 	];
    var pToSelArr = [
      {
        "type": "Bzirk",
        "namespace": "carrier"
      }, //transportArea
      {
        "type": "Zwels",
        "namespace": "billType"
      }, //or PayMode
      // {"type":"contactTypes", "namespace":"ct"},ContactType missing
//      {
//        "type": "Zterm",
//        "namespace": "payCond"
//      }, //paymentCondition
      // {"type":"Perfk", "namespace": "billFreq"},
      {
        "type": "Sdabw",
        "namespace": "deliveryApt"
      },
      {
        "type": "Vsart",
        "namespace": "deliveryType"
      },
      {
        "type": "Augru",
        "namespace": "orderReason"
      },
      {
        "type": "Inco1",
        "namespace": "resa"
      },
      {
        "type": "Auart",
        "namespace": "basketType"
      },
      {
        "type": "Taxk1",
        "namespace": "ivaType"
      },
      {
        "type": "Vsart",
        "namespace": "shippingType"
      },
      {
        "type": "Land1",
        "namespace": "land"
      },
      {
        "type": "Promo",
        "namespace": "promo"
      },
      {
        "type": "Tdid",
        "namespace": "notes"
      },
      {
        "type": "LayoutFattura",
        "namespace": "layout"
      }
      // {"type": "places", "namespace":"p"},LandName missing
      // {"type": "customerTypes", "namespace":"customerType"}, customerType missing
      ];

    var workingUser = model.persistence.Storage.session.get("workingUser");
    this.orgData = {
      "Bukrs": workingUser.organizationData.results[0].society,
      "Vkorg": workingUser.organizationData.results[0].salesOrg,
      "Vtweg": workingUser.organizationData.results[0].distributionChannel,
      "Spart": workingUser.organizationData.results[0].division
    };
    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.getOdataSelect(item.type, this.orgData)
        .then(_.bind(function (result) {
          if (item.namespace === "promo") {
            var promoArray = result.getData().results;
            var blankLine = {};
            blankLine.Txpro = "";
            blankLine.Promo = "";
            promoArray.unshift(blankLine);
          }
          this.getView().setModel(result, item.namespace);


          // if(item.namespace === "basketType")
          // {
          //   var btBox = this.getView().byId("basketTypeBox");
          //   btBox.setSelectedItem(btBox.getItems()[0]);
          // }
        }, this))
    }, this));


  },

  //Upload Files

  onAddProductsPress: function (evt) {
    if (this.order.resa1 === "EXW" && this.order.contactPerson.trim().length === 0) {
      sap.m.MessageBox.warning(model.i18n._getLocaleText("CONTACT_PERSON_NOT_FILLED"), {
        title: model.i18n._getLocaleText("WARNING"),
        onClose: null
      });
      return;
    } else {
      this.setOrder();
      // if (this.order) {
      //   if (typeof this.order.requestedDate !== "undefined" && typeof this.order.requestedDate !== "object") {
      //     var newD = utils.ParseDate().toNewDate(this.order.requestedDate);
      //     this.order.requestedDate = newD;
      //
      //   } else if (this.order.requestedDate === null) {
      //     this.order.requestedDate = this.getView().byId("orderHeaderDate").getProperty("dateValue");
      //   }
      //   for (var i = 0; i < this.order.positions.length; i++) {
      //     if (!this.order.positions[i].wantedDate || this.order.positions[i].wantedDate == "" || this.order.positions[i].wantedDate === null) {
      //       this.order.positions[i].wantedDate = _.isDate(this.order.requestedDate) ? this.order.requestedDate : new Date();
      //     }
      //
      //   }
      //   model.Current.setOrder(this.order);
      //   model.persistence.Storage.session.save("currentOrder", this.order);
      // }
      var isPhone = sap.ui.Device.system.phone;
      if (!!isPhone) {
        this.router.navTo("productsListMobile");
      } else {
        this.router.navTo("empty");
      }
    }
  },

  setOrder: function () {
    if (this.order) {
      if (typeof this.order.requestedDate !== "undefined" && typeof this.order.requestedDate !== "object") {
        var newD = utils.ParseDate().toNewDate(this.order.requestedDate);
        this.order.requestedDate = newD;

      } else if (this.order.requestedDate === null) {
        this.order.requestedDate = this.getView().byId("orderHeaderDate").getProperty("dateValue");
      }
      if (typeof this.order.validDateLst !== "undefined" && typeof this.order.validDateLst !== "object") {
        var newDv = utils.ParseDate().toNewDate(this.order.validDateLst);
        this.order.validDateLst = newDv;

      } else if (this.order.validDateList === null) {
        this.order.validDateList = this.getView().byId("validDateList").getProperty("dateValue");
      }
      for (var i = 0; i < this.order.positions.length; i++) {
        if (!this.order.positions[i].wantedDate || this.order.positions[i].wantedDate == "" || this.order.positions[i].wantedDate === null) {
          this.order.positions[i].wantedDate = _.isDate(this.order.requestedDate) ? this.order.requestedDate : new Date();
        }

      }
      model.Current.setOrder(this.order);
      //model.persistence.Storage.session.save("currentOrder", this.order);
    }
  },

  //funzioni per allegare file
  ////////////**************************************///////////////////////////////////
  ////////////Funzioni che gestiscono gli upload

  openUploadFileDialog: function (evt) {
    if (!this.uploadDialog) {
      this.uploadDialog = sap.ui.xmlfragment("view.dialog.uploadFile", this);
      var page = this.getView().byId("orderCreate");
      page.addDependent(this.uploadDialog);
    }
    this.uploadDialog.open();
    var fileUploader = sap.ui.getCore().byId("fileUploader");
    fileUploader.clear();

    $(document).keyup(_.bind(this.keyUpFunc, this));

  },

  onCloseUploadDialogPress: function (evt) {
    if (this.uploadDialog) {
      this.uploadDialog.close();
      //this.uploadDialog.destroy();
    }

  },


  //** eventulamente per cancellare i file
  onDeleteFilePress: function (evt) {
    var file = evt.getParameters().listItem;
    var fileCtx = file.getBindingContext("o").getObject();
    this.getView().setBusy(true);
    this.order.deleteAttach(fileCtx.posId)
      .then(_.bind(function (res) {
          this.getView().getModel("o").refresh();
          this.getView().setBusy(false);
        }, this),
        _.bind(function (err) {
          sap.m.MessageToast.show(utils.Message.getError(err));
          this.getView().setBusy(false);
        }, this))
  },
  //**
  handleUploadComplete: function (oEvent) {
    var sResponse = oEvent.getParameter("response");
    var status = oEvent.getParameter("status").toString();
    var dataFile = "";
    if (status && status[0] === "2") {
      if (sResponse) {
        var response = oEvent.getParameter("response");
        var indexIGuid = response.indexOf("IGuid=");
        var indexIGuid2 = response.indexOf(",");
        var IGuid = response.substring(indexIGuid, indexIGuid2).split("'")[1];
        var indexIDrunr = response.indexOf("IDrunr=");
        var indexIDrunr2 = response.indexOf(")");
        var IDrunr = response.substring(indexIDrunr, indexIDrunr2).split("'")[1];
        this.order.guid = IGuid;
        dataFile = {
          name: this.fileName,
          posId: IDrunr,
          type: this.fileType,
          url: model.odata.chiamateOdata._serviceUrl + "SO_TabAttachSet(IGuid='" + IGuid + "',IDrunr='" + IDrunr + "')/$value"
        };
        this.order.attachments.push(dataFile);
      }
      sap.m.MessageToast.show(model.i18n._getLocaleText("UPLOAD_OK"));
    } else {
      dataFile = {};
      sap.m.MessageToast.show(model.i18n._getLocaleText("UPLOAD_KO"));
    }
    this.getView().getModel("o").refresh();
    // this.refreshView(this.order);

  },

  handleUploadPress: function (oEvent) {
    // testModel = new sap.ui.model.json.JSONModel();
    // this.getView().setModel(testModel, "test");

    var uploadData = {};
    var fileUploader = sap.ui.getCore().byId("fileUploader");
    if (!fileUploader.getValue()) {
      sap.m.MessageToast.show(model.i18n._getLocaleText("CHOOSE_FILE_FIRST"));
      return;
    } else {
      fileUploader.removeAllHeaderParameters();
      fileUploader.setSendXHR(true);

      var modello = model.odata.chiamateOdata.getOdataModel();
      modello.refreshSecurityToken();
      var header = new sap.ui.unified.FileUploaderParameter({
        name: "x-csrf-token",
        value: modello.getHeaders()['x-csrf-token']
      });

      fileUploader.addHeaderParameter(header);
      // this.fileName = fileUploader.oFileUpload.files[0].name;
      this.fileType = fileUploader.oFileUpload.files[0].type;

      if (this.order.guid) {
        var guid = "|" + this.order.guid;
        var headerSlug = new sap.ui.unified.FileUploaderParameter({
          name: "slug",
          value: this.fileName + "|" + this.fileType + guid
        });
      } else {
        var headerSlug = new sap.ui.unified.FileUploaderParameter({
          name: "slug",
          value: this.fileName + "|" + this.fileType
        });
      }

      fileUploader.addHeaderParameter(headerSlug);

      fileUploader.upload();
    }

    if (this.uploadDialog) {
      this.uploadDialog.close();
      //this.uploadDialog.destroy();
    }
  },


  //** per gestire i tipi di file
  handleTypeMissmatch: function (oEvent) {
    var aFileTypes = oEvent.getSource().getFileType();
    jQuery.each(aFileTypes, function (key, value) {
      aFileTypes[key] = "*." + value
    });
    var sSupportedFileTypes = aFileTypes.join(", ");
    sap.m.MessageToast.show("The file type *." + oEvent.getParameter("fileType") +
      " is not supported. Choose one of the following types: " +
      sSupportedFileTypes);
  },

  handleValueChange: function (oEvent) {

    //console.log(oEvent.getSource());
    var name = oEvent.getParameter("newValue");
    if (name.length > 50) {
      if (!this.fileNameDialog) {
        this.fileNameDialog = sap.ui.xmlfragment("view.dialog.fileNameDialog", this);
      }
      this.fileModel = new sap.ui.model.json.JSONModel();
      this.fileModel.setData({
        "name": name
      });
      this.getView().setModel(this.fileModel, "fn");
      var page = this.getView().byId("orderCreate");
      page.addDependent(this.fileNameDialog);
      this.fileNameDialog.open();
    } else {
      this.fileName = name;
      sap.m.MessageToast.show(model.i18n._getLocaleText("PRESS_UPLOAD_FILE") + " " +
        oEvent.getParameter("newValue") + "'");
    }


  },

  onFileNameOK: function (evt) {
    var name = this.getView().getModel("fn").getProperty("/name");
    if (name.length <= 50) {
      this.fileName = name;
      sap.m.MessageToast.show(model.i18n._getLocaleText("PRESS_UPLOAD_FILE") + " " +
        evt.getParameter("newValue") + "'");
      this.fileNameDialog.close();
    } else {
      sap.m.MessageToast.show(model.i18n._getLocaleText("REDEFINE_FILE_NAME"));
    }
  },
  onFileNameClose: function (evt) {
    this.fileNameDialog.close();
    this.uploadDialog.close();
  },



  ///////////******************************************//////////////////////////////





  //
  //  formatAttribute: function (sValue) {
  //    jQuery.sap.require("sap.ui.core.format.FileSizeFormat");
  //    if (jQuery.isNumeric(sValue)) {
  //      return sap.ui.core.format.FileSizeFormat.getInstance({
  //        binaryFilesize: false,
  //        maxFractionDigits: 1,
  //        maxIntegerDigits: 3
  //      }).format(sValue);
  //    } else {
  //      return sValue;
  //    }
  //  },
  //
  //  onChange: function (oEvent) {
  //    var oUploadCollection = oEvent.getSource();
  //    var filelist = [];
  //    filelist.push(oEvent.getParameters().files[0]);
  //    // Header Token
  //    var oCustomerHeaderToken = new sap.m.UploadCollectionParameter({
  //      name: "x-csrf-token",
  //      value: "securityTokenFromModel"
  //    });
  //    oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
  //  },
  //
  //  onFileDeleted: function (oEvent) {
  //    var oData = this.getView().byId("UploadCollection").getModel().getData();
  //    if (oData.items) {
  //      var aItems = jQuery.extend(true, {}, oData).items;
  //    } else {
  //      var aItems = [];
  //    }
  //
  //    var sDocumentId = oEvent.getParameter("documentId");
  //    jQuery.each(aItems, function (index) {
  //      if (aItems[index] && aItems[index].documentId === sDocumentId) {
  //        aItems.splice(index, 1);
  //      };
  //    });
  //    this.getView().byId("UploadCollection").getModel().setData({
  //      "items": aItems
  //    });
  //    var oUploadCollection = oEvent.getSource();
  //    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
  //    sap.m.MessageToast.show("FileDeleted event triggered.");
  //  },
  //
  //  onFileRenamed: function (oEvent) {
  //    var oData = this.getView().byId("UploadCollection").getModel().getData();
  //    if (oData.items) {
  //      var aItems = jQuery.extend(true, {}, oData).items;
  //    } else {
  //      var aItems = [];
  //    }
  //    var sDocumentId = oEvent.getParameter("documentId");
  //    jQuery.each(aItems, function (index) {
  //      if (aItems[index] && aItems[index].documentId === sDocumentId) {
  //        aItems[index].fileName = oEvent.getParameter("item").getFileName();
  //      };
  //    });
  //    this.getView().byId("UploadCollection").getModel().setData({
  //      "items": aItems
  //    });
  //    sap.m.MessageToast.show("FileRenamed event triggered.");
  //  },
  //
  //  onFileSizeExceed: function (oEvent) {
  //    sap.m.MessageToast.show("FileSizeExceed event triggered.");
  //  },
  //
  //  onTypeMissmatch: function (oEvent) {
  //    sap.m.MessageToast.show("TypeMissmatch event triggered.");
  //  },
  //
  //  onUploadComplete: function (oEvent) {
  //    var oData = this.getView().byId("UploadCollection").getModel().getData();
  //    if (oData.items) {
  //      var aItems = jQuery.extend(true, {}, oData).items;
  //    } else {
  //      var aItems = [];
  //    }
  //    var oItem = {};
  //    var sUploadedFile = oEvent.getParameter("files")[0].fileName;
  //    // at the moment parameter fileName is not set in IE9
  //    if (!sUploadedFile) {
  //      var aUploadedFile = (oEvent.getParameters().getSource().getProperty("value")).split(/\" "/);
  //      sUploadedFile = aUploadedFile[0];
  //    }
  //    oItem = {
  //      "documentId": jQuery.now().toString(), // generate Id,
  //      "fileName": sUploadedFile,
  //      "mimeType": "",
  //      "thumbnailUrl": "",
  //      "url": "",
  //      "attributes": [
  //        {
  //          "title": "Uploaded By",
  //          "text": model.persistence.Storage.session.get("user").fullName,
  //						},
  //        {
  //          "title": "Uploaded On",
  //          "text": new Date(jQuery.now()).toLocaleDateString()
  //						},
  //        {
  //          "title": "File Size",
  //          "text": "505000"
  //						}
  //					]
  //    };
  //    aItems.unshift(oItem);
  //    this.getView().byId("UploadCollection").getModel().setData({
  //      "items": aItems
  //    });
  //    var oUploadCollection = oEvent.getSource();
  //    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
  //    // delay the success message for to notice onChange message
  //    setTimeout(function () {
  //      sap.m.MessageToast.show("UploadComplete event triggered.");
  //    }, 4000);
  //  },
  //
  //  onSelectChange: function (oEvent) {
  //    var oUploadCollection = sap.ui.getCore().byId(this.getView().getId() + "--UploadCollection");
  //    oUploadCollection.setShowSeparators(oEvent.getParameters().selectedItem.getProperty("key"));
  //  },
  //  onBeforeUploadStarts: function (oEvent) {
  //    // Header Slug
  //    var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
  //      name: "slug",
  //      value: oEvent.getParameter("fileName")
  //    });
  //    oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
  //
  //
  //
  //
  //
  //    sap.m.MessageToast.show("BeforeUploadStarts event triggered.");
  //  },
  //  onUploadTerminated: function (oEvent) {
  //    // get parameter file name
  //    var sFileName = oEvent.getParameter("fileName");
  //    // get a header parameter (in case no parameter specified, the callback function getHeaderParameter returns all request headers)
  //    var oRequestHeaders = oEvent.getParameters().getHeaderParameter();
  //  },


  // fine Upload files



  onEvasionChange: function (evt) {
    var val = evt.getParameter("state");
    this.order.totalEvasion = val ? "X" : "";
    //console.log(this.order.totalEvasion.toString());
  },

  handleTransportChange: function (evt) {
    var source = evt.getSource();
    var selectedItem = source.getSelectedItem().getKey();


    this.order.setDeliveryType(selectedItem); //Maybe a repetition

    //    var finalDestButton = this.getView().byId("finalDestination");
    //    finalDestButton.setValue("");
    // if (selectedItem === "11") {
    //   //this.appModel.setProperty("/enableDestination", true);
    //   //**
    //   //this.order.removeAlternativeDestination();
    //   //**
    //   var cb = this.getView().byId("camionGruComboBox");
    //   cb.setValue(cb.getDefaultSelectedItem());
    //   //this.resetForm();
    //   //this.appModel.setProperty("/enableDestination", false);
    //   this.camionGru = sap.ui.xmlfragment("view.dialog.camionGru", this);
    //   var page = this.getView().byId("orderCreate");
    //   page.addDependent(this.camionGru);
    //   this.camionGru.open();
    //   $(document).keyup(_.bind(this.keyUpFunc, this));
    //   source.setSelection("false");
    // } else {
    //   this.order.removeCamionGruData();
    //      var cb = this.getView().byId("camionGruComboBox");
    //      cb.setValue(cb.getDefaultSelectedItem());
    //      //this.resetForm();
    //      //this.appModel.setProperty("/enableDestination", false);
    //      this.camionGru = sap.ui.xmlfragment("view.dialog.camionGru", this);
    //      var page = this.getView().byId("orderCreate");
    //      page.addDependent(this.camionGru);
    //      this.camionGru.open();
    //      $(document).keyup(_.bind(this.keyUpFunc, this));
    //      source.setSelection("false");
    // }

  },

  onCartPress: function (evt) {
    if (this.order.resa1 === "EXW" && this.order.contactPerson.trim().length === 0) {
      sap.m.MessageBox.warning(model.i18n._getLocaleText("CONTACT_PERSON_NOT_FILLED"), {
        title: model.i18n._getLocaleText("WARNING"),
        onClose: null
      });
      return;
    } else {
      this.setOrder();

      this.router.navTo("offerCartFullView");
    }

  },

  setDefaultValues: function () {

//    this.orderModel = new sap.ui.model.json.JSONModel();
//    this.orderModel.setData(this.order);//Commented 15/09/16, model already initialized in onInit
    if (!!this.orderModel.getData().resa1 && this.orderModel.getData().resa1 !== "EXW") {
    	/* Why?  15/09/16*/
//      if (!!this.orderModel.getData().contactPerson) {
//        this.order.contactPerson = "";
//        this.orderModel.getData().contactPerson = "";
//      }
      /* */
    }

    if (this.orderModel.getData().resa1 === "EXW" || this.order.resa1 === "EXW") {
      this.getView().byId("contactPersonLabel").setVisible(true);
      this.getView().byId("contactPersonInput").setVisible(true);
    }

    //this.getView().setModel(this.orderModel, "o"); //Commented 15/09/16, model already initialized in onInit
    this.initializeValues();
    this.setIntervalLocalSaving();
  },

  onPreviewPress: function (evt) {
    var src = evt.getSource();
    var link = src.getBindingContext("o").getObject().url;
    sap.m.URLHelper.redirect(link, true);
  },

  onIVAChange: function (evt) {
    var selectedIVA = evt.getSource().getSelectedItem().getBindingContext("ivaType").getObject();
    console.log(this.getView().getModel("ui").getProperty("/previousIVA"));
    var txt = "Allegare i seguenti documenti:\n";
    txt += "\t i.	Dichiarazione IVA agevolata del cliente;\n";
    txt += "\t ii.	Concessione edilizia / Permesso a costruire / DIA";

    //if(this.getView().getModel("ui").getProperty("/previousIVA").Taxk1 == "1")
    if (this.getView().getModel("ui").getProperty("/previousIVA").Xfulltax == "X") {
      sap.m.MessageBox.alert(txt, {
        title: model.i18n.getText("infoIVA")
      });
    }

    this.getView().getModel("ui").setProperty("/previousIVA", selectedIVA)

  },

  onPressDeliveryType: function () {

    if (sap.ui.getCore().byId("dialogCamionGru"))
      sap.ui.getCore().byId("dialogCamionGru").destroy(true);

    this.camionGru = sap.ui.xmlfragment("view.dialog.camionGru", this);
    var page = this.getView().byId("orderCreate");
    page.addDependent(this.camionGru);
this.getView().getModel("gru").setData(_.clone(this.gruData));
    this.camionGru.open();
  },


  populateRegionSelect: function (evt) {
	    var req = {
	      "Land1": ""
	    };
	    req.Land1 = evt.getSource().getSelectedKey();
	    utils.Collections.getOdataSelect("Regio", req)
	      .then(_.bind(function (result) {
	        result.setSizeLimit(500);
	        this.getView().setModel(result, "prov");
	        this.uiModel.setProperty("/enableRegion", true);
	      }, this));

	  },
	
	  
	  
	  
	  
	initializeOfferOrderSelectionDialog:function()
	{
		 this.offerOrderDialog = sap.ui.xmlfragment("view.dialog.SelectOfferOrderDialog", this);
	    var page = this.getView().byId("orderCreate");
	    page.addDependent(this.offerOrderDialog);
		
		    //this.offerOrderDialog.open();
	},
	selectOrderOfferType: function()
	{
		if(!this.order.getSelectedType())
			this.openOfferOrderSelectionDialog();
	},
	
	openOfferOrderSelectionDialog:function()
	{
		this.uiModel.setProperty("/offerOrderSelected", false);
		this.offerOrderDialog.open();
	},
	onOfferOrderSelectionChange:function(evt)
	{
		this.uiModel.setProperty("/offerOrderSelected", evt.getParameter("selected"));
	},
	onSelectOfferOrderDialogConfirm:function(evt)
	{
		var src = evt.getSource();
		var selectedItem = sap.ui.getCore().byId("documentTypeSelectListId").getSelectedItem().getBindingContext("basketType").getObject();
		this.order.basketType=selectedItem.Auart; 
		this.order.setSelectedType(selectedItem.Auart);
		this.order.importCustomerDiscount();
		
		this.getView().getModel("o").refresh();
		this.offerOrderDialog.close();
	},
	
	onBasketTypeChange:function(evt)
	{
		var src = evt.getSource();
		if(this.order.basketType == "ZRD")
		{
			model.collections.DiscountItems.checkZsl3DiscountValues(this.order.commonDiscounts, this.order.basketType);
			model.collections.DiscountItems.checkCommonDiscounts(this.order.commonDiscounts);
			var failed = [];
			if(this.order.positions && this.order.positions.length > 0)
			{
				for(var i = 0; i< this.order.positions.length ; i++)
				{
					if(!model.collections.DiscountItems.checkZsl3DiscountValues(this.order.positions[i].orderDiscounts, this.order.basketType))
					{
						this.order.positions[i].updateDiscountOriginalFromRI();
						failed.push(this.order.positions[i]);	
					}	
				}
			}
			if(failed.length > 0)
			{
				var failString = model.i18n.getText("discountZsl3ValueChanged");
				_.map(failed, _.bind(function(item){ failString += '\n'+ item.positionId+" - "+" "+item.productId+" "+item.description;}, this));
				sap.m.MessageBox.show(failString, {title: model.i18n.getText("alert")});
			}
		}
		
		
	}
	






});
