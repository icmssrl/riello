jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.OffersList");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Customer");
jQuery.sap.require("model.CustomerRequester");
jQuery.sap.require("model.Offer");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("utils.Message");
jQuery.sap.require("utils.SorterFactory");
jQuery.sap.require("model.odata.chiamateOdata");

view.abstract.AbstractMasterController
		.extend(
				"view.offer.OffersList",
				{
					onExit : function() {
					},
					onInit : function() {
						// this.router =
						// sap.ui.core.UIComponent.getRouterFor(this);
						// this.router.attachRoutePatternMatched(this.handleRouteMatched,
						// this);
						view.abstract.AbstractController.prototype.onInit
								.apply(this, arguments);
						this.uiModel.setProperty("/searchProperty", [
								"customerId", "customerName", "orderId",
								"agentCode", "agentName" ]);
						this.userModel = new sap.ui.model.json.JSONModel();
						this.getView().setModel(this.userModel, "userModel");

						this.incompleteTxtModel = new sap.ui.model.json.JSONModel();
						this.getView().setModel(this.incompleteTxtModel,
								"incomplete");

						this.flagsModel = new sap.ui.model.json.JSONModel();
						this.getView().setModel(this.flagsModel, "flags");
					},
					handleRouteMatched : function(evt) {
						var name = evt.getParameter("name");
						if (name !== "offersList") {
							return;
						}
						model.persistence.Storage.session.save("sourceList",
								"offerList");
						model.persistence.Storage.session.remove("flagged");
						this.user = model.persistence.Storage.session
								.get("user");
						this.userModel.setData(this.user);
						this.offerListModel = new sap.ui.model.json.JSONModel();
						this.getView().setModel(this.offerListModel,
								"offerList");
						this.reqData = this.getUserInfo();
						if (!!sessionStorage.getItem("customerSession")
								&& sessionStorage.getItem("customerSession") === "true") {
							this.reqData = this.getCustomerInfo();
						}
						this.obblData = _.pick(this.reqData,
								[ "salesOrg", "division", "distrCh", "society",
										"areaManager" ]);
						this.getView().setBusy(true);
						this.refreshOfferList(this.reqData);
						// model.collections.OffersList.getOffersList(this.reqData)
						// // forceReload
						// .then(
						// _.bind(function(res) {
						// // console.log(res);
						// this.sortList(res.results);
						// this.offerListModel.setData(res);
						// this.offerListModel.refresh(true);
						// this.overwriteExistingOffers();
						// this.getView().setBusy(false);
						//
						// }, this),
						// _.bind(function(err) {
						// this.getView().setBusy(false);
						// sap.m.MessageToast.show(utils.Message.getError(err));
						// }, this));
						this.odataReq = this.getUserInfo();
						// this.resetODataFilter();
					},
					sortList : function(list) {
						this.offerStatusMap = [ {
							key : "AAM",
							value : 3
						}, {
							key : "MAM",
							value : 3
						}, {
							key : "ATM",
							value : 2
						}, {
							key : "MTM",
							value : 2
						}, {
							key : "INS",
							value : 1
						}, {
							key : "REJ",
							value : 1
						} ];
						this.offerReasonMap = [ {
							key : "QFO",
							value : 3
						}, {
							key : "QFQ",
							value : 2
						}, {
							key : "NWF",
							value : 1
						} ];
						list
								.sort(_
										.bind(
												function(a, b) {
													var statusA = a.orderStatus;
													var statusB = b.orderStatus;
													var statusItemA = _
															.find(
																	this.offerStatusMap,
																	{
																		key : statusA
																	});
													var statusItemB = _
															.find(
																	this.offerStatusMap,
																	{
																		key : statusB
																	});
													var statusValA = statusItemA ? statusItemA.value
															: 0;
													var statusValB = statusItemB ? statusItemB.value
															: 0;
													if (statusValA > statusValB)
														return -1;
													if (statusValA < statusValB)
														return 1;
													else {
														var reasonA = a.orderReason;
														var reasonB = b.orderReason;
														var reasonItemA = _
																.find(
																		this.offerReasonMap,
																		{
																			key : reasonA
																		});
														var reasonItemB = _
																.find(
																		this.offerReasonMap,
																		{
																			key : reasonB
																		});
														var reasonValA = reasonItemA ? reasonItemA.value
																: 0;
														var reasonValB = reasonItemB ? reasonItemB.value
																: 0;
														if (reasonValA > reasonValB)
															return -1;
														if (reasonValA < reasonValB)
															return 1;
														else {
															if (a.orderId < b.orderId)
																return 1;
															if (a.orderId > b.orderId)
																return -1;
															else
																return 0;
														}
													}
												}, this));
					},
					overwriteExistingOffers : function() {
						this.tempOffers = model.persistence.Storage.session
								.get("temporaryOffers");
						if (this.tempOffers && this.tempOffers.length > 0) {
							var loadedOffers = this.offerListModel.getData().results;
							for (var i = 0; loadedOffers
									&& loadedOffers.length > 0
									&& i < loadedOffers.length; i++) {
								var found = _.find(this.tempOffers, {
									orderId : loadedOffers[i].orderId
								});
								if (found) {
									loadedOffers[i].orderStatus = found.orderStatus;
									loadedOffers[i].orderStatusDescr = found.orderStatusDescr;
									loadedOffers[i].offerDiscounts = found.offerDiscounts;
									loadedOffers[i].positionsDiscounts = found.positionsDiscounts;
									loadedOffers[i].agentCommission = found.agentCommission;
								}
							}
							this.offerListModel.setData({
								"results" : loadedOffers
							});
							this.offerListModel.refresh(true);
						}
					},
					onOfferSelectionChange : function(evt) {
						var src = evt.getParameter('listItem');
						var offer = src.getBindingContext("offerList")
								.getObject();
						// if(offer.orderStatus !== "A")
						if (!offer.isConvertible()) {
							sap.m.MessageToast.show(model.i18n
									.getText("impossibleTransformToOrderOffer")
									+ " " + offer.orderId);
							src.setSelected(false);
						}
					},
					onInfoPress : function(evt) {
						this.navToOrderDetails(evt);
					},
					navToOrderDetails : function(evt) {
						var src = evt.getSource();
						this.selectedOffer = src.getBindingContext("offerList")
								.getObject();
						var id = this.selectedOffer.orderId;
						// this.selectedOffer.loadDiscount_v2().then(_.bind(function
						// (res) {
						model.persistence.Storage.session.save("selectedOffer",
								this.selectedOffer);
						this.router.navTo("offerInfo", {
							id : id
						});
						// }, this), _.bind(function (err) {
						// sap.m.MessageToast.show(utils.Message.getError(err));
						// }, this))
					},
					onDiscountPress : function(evt) {
						var src = evt.getSource();
						this.selectedOffer = src.getBindingContext("offerList")
								.getObject();
						this.getView().setBusy(true);
						this.selectedOffer
								.loadDiscount_v2()
								.then(
										_
												.bind(
														function(res) {
															this
																	.getView()
																	.setBusy(
																			false);
															if (!this.discountModel) {
																this.discountModel = new sap.ui.model.json.JSONModel();
																this
																		.getView()
																		.setModel(
																				this.discountModel,
																				"d");
															}
															if (!this.enableModel) {
																this.enableModel = new sap.ui.model.json.JSONModel();
																this
																		.getView()
																		.setModel(
																				this.enableModel,
																				"enable");
															}
															this.enableModel
																	.setData({
																		"editable" : true
																	})
															var discountData = {
																"discountArray" : []
															};
															for (var i = 0; this.selectedOffer.offerDiscounts
																	&& this.selectedOffer.offerDiscounts.length > 0
																	&& i < this.selectedOffer.offerDiscounts.length; i++) {
																discountData.discountArray
																		.push(_
																				.clone(this.selectedOffer.offerDiscounts[i]));
															}
															this.discountModel
																	.setData(discountData);
															if (!this.discountDialog) {
																this.discountDialog = sap.ui
																		.xmlfragment(
																				"view.dialog.discountDialog_v2",
																				this);
															}
															var page = this
																	.getView()
																	.byId(
																			"offerListPageId");
															page
																	.addDependent(this.discountDialog);
															this.discountDialog
																	.open();
														}, this),
										_.bind(function(err) {
											sap.m.MessageToast
													.show(utils.Message
															.getError(err));
										}, this))
					},
					onResetDiscountPress : function(evt) {
						var discountData = {
							"discountArray" : []
						};
						for (var i = 0; this.selectedOffer.offerDiscounts
								&& this.selectedOffer.offerDiscounts.length > 0
								&& i < this.selectedOffer.offerDiscounts.length; i++) {
							discountData.discountArray
									.push(_
											.clone(this.selectedOffer.offerDiscounts[i]));
						}
						this.discountModel.setData(discountData);
						// this.discountDialog.close();
					},
					onDiscountDialogClose : function(evt) {
						this.discountDialog.close();
					},
					onDiscountDialogOK : function(evt) {
						this.selectedOffer.orderStatus = "I";
						this.selectedOffer.orderStatusDescr = "In Attesa";
						this.selectedOffer.updateDiscounts(this.getView()
								.getModel("d").getData().discountArray);
						this.tempOffers = model.persistence.Storage.session
								.get("temporaryOffers") ? model.persistence.Storage.session
								.get("temporaryOffers")
								: [];
						this.tempOffers.push(this.selectedOffer);
						model.persistence.Storage.session.save(
								"temporaryOffers", this.tempOffers);
						this.getView().getModel("offerList").refresh();
						this.discountDialog.close();
					},
					// , onTransformPress: function (evt) {
					// var selectedOffers =
					// _.filter(this.getView().getModel("offerList").getData().results,
					// {
					// selected: true
					// });
					// if (selectedOffers && selectedOffers.length === 0) {
					// sap.m.MessageToast.show(model.i18n.getText("noItemsSelected"));
					// return;
					// }
					// var bCompact =
					// !!this.getView().$().closest(".sapUiSizeCompact").length;
					// sap.m.MessageBox.confirm(model.i18n.getText("approveChanges"),
					// {
					// styleClass: bCompact ? "sapUiSizeCompact" : ""
					// , actions: [sap.m.MessageBox.Action.YES,
					// sap.m.MessageBox.Action.NO]
					// , onClose: function (oAction) {
					// if (oAction === "YES") {
					// if (selectedOffers && selectedOffers.length > 0) {
					// this.offer = selectedOffers[0];
					// this.onTransformToOrder(selectedOffers[0]).then(_.bind(function
					// (res) {
					// var offer =
					// _.find(this.getView().getModel("offerList").getData().results,
					// {
					// "orderId": this.offer.orderId
					// });
					// this.offer.orderStatus = "CONVERTITO";
					// this.offer.orderStatusDescr =
					// model.i18n.getText("converted");
					// this.getView().getModel("offerList").refresh();
					// }, this));
					// // for (var i = 0; i < selectedOffers.length; i++) {
					// //
					// _.remove(this.getView().getModel("offerList").getData().results,
					// selectedOffers[i]);
					// // }
					// }
					// // this.getView().getModel("offerList").refresh();
					// //
					// sap.m.MessageToast.show(model.i18n.getText("multipleModifingSuccess"));
					// }
					// else {
					// return;
					// }
					// }.bind(this)
					// });
					// }
					// ---------New Version Trasform Offer To Order
					// ---------------------------------------//
					onTransformPress : function(evt) {
						var selectedOffers = _.filter(this.getView().getModel(
								"offerList").getData().results, {
							selected : true
						});
						if (selectedOffers && selectedOffers.length === 0) {
							sap.m.MessageToast.show(model.i18n
									.getText("noItemsSelected"));
							return;
						}
						var bCompact = !!this.getView().$().closest(
								".sapUiSizeCompact").length;
						sap.m.MessageBox.confirm(model.i18n
								.getText("approveChanges"), {
							styleClass : bCompact ? "sapUiSizeCompact" : "",
							actions : [ sap.m.MessageBox.Action.YES,
									sap.m.MessageBox.Action.NO ],
							onClose : function(oAction) {
								if (oAction === "YES") {
									if (selectedOffers
											&& selectedOffers.length > 0) {

										this._openFlagsDialog();
										// this.offer = selectedOffers[0];
										// this.onTransformToOrder(selectedOffers[0]).then(_.bind(function
										// (res) {
										// var offer =
										// _.find(this.getView().getModel("offerList").getData().results,
										// {
										// "orderId": this.offer.orderId
										// });
										// this.offer.orderStatus =
										// "CONVERTITO";
										// this.offer.orderStatusDescr =
										// model.i18n.getText("converted");
										// this.getView().getModel("offerList").refresh();
										// }, this));

									}

								} else {
									return;
								}
							}.bind(this)
						});
					},
					_openFlagsDialog : function() {
						if (!this.flagDialog)
							this.flagDialog = sap.ui.xmlfragment(
									"view.dialog.OrderFlagsDialog", this);
						var page = this.getView().byId("offerListPageId");
						this.flagsModel.setData({
							"check1" : false,
							"check2" : false
						});
						page.addDependent(this.flagDialog);
						this.flagDialog.open();
					},
					onOkFlagDialogPress : function(evt) {
						var selectedOffers = _.filter(this.getView().getModel(
								"offerList").getData().results, {
							selected : true
						});
						if (selectedOffers && selectedOffers.length > 0) {

							this._openFlagsDialog();
							this.offer = selectedOffers[0];
							model.persistence.Storage.session.save(
									"selectedOffer", this.offer);
							this.flagDialog.close();
							this.getView().setBusy(true);
							this
									.onTransformToOrder(selectedOffers[0])
									.then(
											_
													.bind(
															function(res) {

																this
																		.getView()
																		.setBusy(
																				false);
																var offer = _
																		.find(
																				this
																						.getView()
																						.getModel(
																								"offerList")
																						.getData().results,
																				{
																					"orderId" : this.offer.orderId
																				});
																this.offer.orderStatus = "CONVERTITO";
																this.offer.orderStatusDescr = model.i18n
																		.getText("converted");
																this
																		.getView()
																		.getModel(
																				"offerList")
																		.refresh();

															}, this),
											(_
													.bind(
															function(err) {
																this
																		.getView()
																		.setBusy(
																				false);
																var error = JSON
																		.parse(err.response.body).error
																if (error.code == "ZXF/092" || error.code == "ZXF/087") {
																	var that = this;
																	sap.m.MessageBox
																			.confirm(
																					model.i18n
																							.getText("attachmentsNeeded"),
																					{
																						actions : [
																								sap.m.MessageBox.Action.YES,
																								sap.m.MessageBox.Action.NO ],
																						onClose : _
																								.bind(
																										function(
																												oAction) {
																											if (oAction == "YES") {
																												model.persistence.Storage.session.save("flagged", "X");
																												that.router
																														.navTo(
																																"offerInfo",
																																{
																																	id : that.offer.orderId
																																});
																											}
																										},
																										that)
																					})
																}

															}, this)));

						}
					},
					onCancelFlagDialogPress : function(evt) {
						this.flagDialog.close();
					}
					// -----------------------------------------------------------------------------------------------------
					,
					onCustomerDiscountValueChange : function(evt) {
						this.customerDiscountModel.setProperty("/value",
								parseFloat(this.customerDiscountModel
										.getProperty("/value")));
						this.customerDiscountModel.refresh();
					},
					onCustomerDiscountClose : function(evt) {
						this.customerDiscountDialog.close();
					},
					onCustomerDiscountOK : function(evt) {
						this.customerDiscountDialog.close();
						this.selectedDiscount.customerDiscountValue = this.customerDiscountRequester.value; // Temp
						this.selectedDiscount.customerDiscountStatus = "M"; // Temp
						this.selectedDiscount.customerDiscountStatusDescr = "Modificato" // Temp
						this.getView().getModel("offerList").refresh();
						sap.m.MessageToast.show(model.i18n
								.getText("discountModifingSuccess"))
						// this.customerDiscountRequester.saveCustomerDiscountValue();
						// this.refreshView(this.customer);
					},
					onFilterPress : function() {
						this.filterModel = model.filters.Filter
								.getModel(
										this.offerListModel.getData().results,
										"orders");
						this.getView().setModel(this.filterModel, "filter");
						var page = this.getView().byId("offerListPageId");
						this.filterDialog = sap.ui.xmlfragment(
								"view.dialog.filterDialog", this);
						page.addDependent(this.filterDialog);
						this.filterDialog.open();
					},
					onFilterDialogClose : function() {
						this.filterDialog.close();
					},
					onFilterPropertyPress : function(evt) {
						var parentPage = sap.ui.getCore().byId("parent");
						var elementPage = sap.ui.getCore().byId("children");
						// console.log(this.getView().getModel("filter").getData().toString());
						var navCon = sap.ui.getCore().byId("navCon");
						var selectedProp = evt.getSource().getBindingContext(
								"filter").getObject();
						this.getView().getModel("filter").setProperty(
								"/selected", selectedProp);
						this.elementListFragment = sap.ui.xmlfragment(
								"view.fragment.filterList", this);
						elementPage.addContent(this.elementListFragment);
						navCon.to(elementPage, "slide");
						this.getView().getModel("filter").refresh();
					},
					onBackFilterPress : function(evt) {
						// this.addSelectedFilterItem();
						this.navConBack();
						this.getView().getModel("filter").setProperty(
								"/selected", "");
						this.elementListFragment.destroy();
					},
					navConBack : function() {
						var navCon = sap.ui.getCore().byId("navCon");
						navCon.to(sap.ui.getCore().byId("parent"), "slide");
						this.elementListFragment.destroy();
					},
					afterOpenFilter : function(evt) {
						var navCon = sap.ui.getCore().byId("navCon");
						if (navCon.getCurrentPage().getId() == "children")
							navCon.to(sap.ui.getCore().byId("parent"), "slide");
						this.getView().getModel("filter").setProperty(
								"/selected", "");
					},
					onSearchFilter : function(oEvt) {
						var aFilters = [];
						var sQuery = oEvt.getSource().getValue();
						if (sQuery && sQuery.length > 0) {
							// var filter = new sap.ui.model.Filter("value",
							// sap.ui.model.FilterOperator.Contains, sQuery);
							// var filter = new
							// sap.ui.model.Filter({path:"value",
							// test:function(val)
							// {
							// var property= val.toString().toUpperCase();
							// return
							// (property.indexOf(sQuery.toString().toUpperCase())>=0)
							// }});
							aFilters.push(this.createFilter(sQuery, "value"));
						}
						// update list binding
						var list = sap.ui.getCore().byId("filterList");
						var binding = list.getBinding("items");
						binding.filter(aFilters);
					},
					createFilter : function(query, property) {
						var filter = new sap.ui.model.Filter({
							path : property,
							test : function(val) {
								var prop = val.toString().toUpperCase();
								return (prop.indexOf(query.toString()
										.toUpperCase()) >= 0)
							}
						});
						return filter;
					},
					onFilterDialogClose : function(evt) {
						if (this.elementListFragment) {
							this.elementListFragment.destroy();
						}
						if (this.filterDialog) {
							this.filterDialog.close();
							this.filterDialog.destroy();
						}
					},
					onFilterDialogOK : function(evt) {
						var filterItems = model.filters.Filter
								.getSelectedItems("orders");
						if (this.elementListFragment)
							this.elementListFragment.destroy();
						this.filterDialog.close();
						this.getView().getModel("filter").setProperty(
								"/selected", "");
						this.handleFilterConfirm(filterItems);
						this.filterDialog.destroy();
						delete (this.filterDialog);
					},
					handleFilterConfirm : function(selectedItems) {
						var filters = [];
						_.forEach(selectedItems, _.bind(function(item) {
							filters.push(this.createFilter(item.value,
									item.property));
						}, this));
						var table = this.getView().byId("offerListTable");
						var binding = table.getBinding("items");
						binding.filter(filters);
					},
					onResetFilterPress : function() {
						model.filters.Filter.resetFilter("orders");
						if (this.elementListFragment) {
							this.elementListFragment.destroy();
						}
						if (this.filterDialog) {
							this.filterDialog.close();
							this.filterDialog.destroy();
						}
						var table = this.getView().byId("offerListTable");
						var binding = table.getBinding("items");
						binding.filter();
						// sap.m.MessageToast.show("All filters cleared!");
						// //console.log(model.filters.Filter.getSelectedItems("customers"));
					},
					navToCustomerDetails : function(evt) {
						var src = evt.getSource();
						var selectedItem = src.getBindingContext("offerList")
								.getObject();
						var parameters = {
							"Bukrs" : selectedItem.society,
							"Vkorg" : selectedItem.salesOrg,
							"Vtweg" : selectedItem.distrCh,
							"Spart" : selectedItem.division,
							"Cdage" : selectedItem.agentCode
						}
						this.getView().getModel("appStatus").setProperty(
								"/customerRequester", parameters);
						model.persistence.Storage.session.save(
								"customerRequester", parameters);
						var id = selectedItem.customerId;
						this.router.navTo("fullCustomerDetail4Approvation", {
							id : id
						});
					},
					onSearch : function(evt) {
						var src = evt.getSource();
						this.searchValue = src.getValue();
						var searchProperty = this.uiModel
								.getProperty("/searchProperty");
						this.applyFilter(this.searchValue, searchProperty);
					},
					applyFilter : function(value, params) {
						var table = this.getView().byId("offerListTable");
						if (!table.getBinding("items").oList
								|| table.getBinding("items").oList.length === 0)
							return;
						var temp = table.getBinding("items").oList[0]; // a
						// template
						// just
						// to
						// recover
						// the
						// data
						// types
						var filtersArr = [];
						// var props = utils.ObjectUtils.getKeys(temp);
						if (!_.isEmpty(params)) {
							if (!_.isArray(params)) {
								params = [ params ];
							}
							for (var i = 0; i < params.length; i++) {
								switch (typeof (utils.ObjectUtils.getValues(
										temp, params[i]))) {
								case "undefined":
									break;
								case "string":
									filtersArr
											.push(new sap.ui.model.Filter(
													params[i],
													sap.ui.model.FilterOperator.Contains,
													value));
									break;
								case "number":
									filtersArr.push(new sap.ui.model.Filter(
											params[i],
											sap.ui.model.FilterOperator.EQ,
											value));
									break;
								}
							}
							var filter = new sap.ui.model.Filter({
								filters : filtersArr,
								and : false
							});
							table.getBinding("items").filter(filter);
							return;
						}
						table.getBinding("items").filter();
					},
					keyUpFunc : function(e) {
						if (e.keyCode == 27) {
							// codice per il pulsante escape per evitare che lo
							// user chiuda il dialog via ESC
							$(document).off("keyup");
							// this.router.navTo("launchpad");
						}
					},
					populateStatusSelect : function() {
						var defer = Q.defer();
						var workingUser = model.persistence.Storage.session
								.get("workingUser");
						this.orgData = {
							"Bukrs" : workingUser.organizationData.results[0].society,
							"Vkorg" : workingUser.organizationData.results[0].salesOrg,
							"Vtweg" : workingUser.organizationData.results[0].distributionChannel,
							"Spart" : workingUser.organizationData.results[0].division
						};
						var item = {
							"type" : "Stato",
							"namespace" : "orderStatus"
						};
						utils.Collections.getOdataSelect(item.type,
								this.orgData).then(_.bind(function(result) {
							this.getView().setModel(result, item.namespace);
							defer.resolve(result);
						}, this), _.bind(function(err) {
							defer.reject(err);
						}, this))
						return defer.promise;
					},
					onAcceptPress : function(evt) {
						var items = this.getView().getModel("offerList")
								.getData().results;
						var selectedItems = _.where(items, {
							selected : true
						});
						for (var i = 0; selectedItems
								&& selectedItems.length > 0
								&& i < selectedItems.length; i++) {
							selectedItems[i].customerDiscountStatus = "S";
							selectedItems[i].customerDiscountStatusDescr = "Approvato"
						}
						this.getView().getModel("offerList").refresh();
						this.getView().byId("offerListTable")
								.removeSelections();
					},
					onRejectPress : function(evt) {
						var items = this.getView().getModel("offerList")
								.getData().results;
						var selectedItems = _.where(items, {
							selected : true
						});
						for (var i = 0; selectedItems
								&& selectedItems.length > 0
								&& i < selectedItems.length; i++) {
							selectedItems[i].customerDiscountStatus = "R";
							selectedItems[i].customerDiscountStatusDescr = "Respinto"
						}
						this.getView().getModel("offerList").refresh();
						this.getView().byId("offerListTable")
								.removeSelections();
					},
					onDiscountSliderChange : function(evt) {
						var src = evt.getSource();
						var srcValue = src.getValue();
						var discountItem = src.getBindingContext("d")
								.getObject();
						discountItem.value = srcValue;
						this.getView().getModel("d").refresh()
					},
					refreshOfferList : function(reqData) {
						model.collections.OffersList.getOffersList(reqData) // forceReload
						.then(
								_.bind(function(res) {
									// console.log(res);
									this.sortList(res.results);
									this.offerListModel.setData(res);
									this.offerListModel.refresh(true);
									this.overwriteExistingOffers();
									this.getView().setBusy(false);
								}, this),
								_.bind(function(err) {
									this.getView().setBusy(false);
									sap.m.MessageToast.show(utils.Message
											.getError(err));
								}, this));
					},
					handlePressOfShippmentDate : function(evt) {
						this._showIncompleteTextPopover(evt, "shippmentDate");
					},
					handlePressOfCustomerName : function(evt) {
						this._showIncompleteTextPopover(evt, "customerName");
					},
					handlePressOfAgentName : function(evt) {
						this._showIncompleteTextPopover(evt, "agentName");
					},
					handlePressOfOrderReason : function(evt) {
						this
								._showIncompleteTextPopover(evt,
										"orderReasonDescr");
					},
					_showIncompleteTextPopover : function(evt, property) {
						var src = evt.getSource();
						var obj = src.getBindingContext("offerList")
								.getObject();
						var txt = obj[property];
						this.incompleteTxtModel.setProperty("/text", txt);
						this.incompleteTxtModel.refresh();

						if (!this._oPopover) {
							this._oPopover = sap.ui
									.xmlfragment(
											"view.dialog.popovers.incompleteTextPopover",
											this);
							this.getView().addDependent(this._oPopover);

						}

						// delay because addDependent will do a async
						// rerendering and the actionSheet will immediately
						// close without it.

						jQuery.sap.delayedCall(0, this, function() {
							this._oPopover.openBy(src);
						});

					},
					onLinkToPdfPress : function(evt) {
						// sap.m.URLHelper.redirect(this._getVal(evt), true);
						// var url =
						// "http://sap-nwg.sapit.riello.lan/eweb/PDF/0101036203.pdf";
						var offerId = evt.getSource().getBindingContext(
								"offerList").getObject().orderId;
						this.getView().setBusy(true);
						var fSuccess = function(result) {
							this.getView().setBusy(false);
							sap.m.URLHelper.redirect(result.EUrl, true); // To
																			// check
																			// popup
																			// block
							// var w = window;
							// w.open(result.EUrl, '_blank');
							// sap.m.URLHelper.redirect(result.EUrl);
						}
						fSuccess = _.bind(fSuccess, this);
						var fError = function(err) {
							this.getView().setBusy(false);
							sap.m.MessageToast.show(utils.Message.getError(err));
						}
						fError = _.bind(fError, this);

						model.odata.chiamateOdata.loadOrderPdf(offerId,
								fSuccess, fError);

					},

				});