jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.Positions");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.collections.DiscountItems");
jQuery.sap.require("utils.Asynchronous");

view.abstract.AbstractController.extend("view.offer.OfferCartFull", {
    onExit: function () {}
    , onInit: function () {
        // this.router = sap.ui.core.UIComponent.getRouterFor(this);
        // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.valueStateModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.valueStateModel, "vs");
        this.valueStateModel.setProperty("valueState", "");
        
        this.enableModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.enableModel, "enable");
    }
    , handleRouteMatched: function (evt) {
        var routeName = evt.getParameter("name");
        if (routeName !== "offerCartFullView") {
            return;
        }
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        this.order = model.Current.getOrder();
        if (!this.order) {
            var orderData = model.persistence.Storage.session.get("currentOrder");
            this.order = new model.Order(orderData);
        };
        this.customer = model.Current.getCustomer();
        if (!this.customer) {
            var customerData = model.persistence.Storage.session.get("currentCustomer");
            this.customer = new model.Customer(customerData);
        }
        this.getView().setModel(this.customer.getModel(), "customer");
        this.user = model.persistence.Storage.session.get("user");
        this.cartModel = this.order.getModel();
        this.getView().setModel(this.cartModel, "c");
        this.appModel = this.getView().getModel("appStatus");
        this.appModel.setProperty("/enableSaveButton", false);
        var checkBox = this.getView().byId("dichiarazioneCartCheckBox");
        var checkBox1 = this.getView().byId("dichiarazioneCartCheckBox1");
        //    if(!checkBox)
        //    {
        //    checkBox = this.getView().byId("dichiarazioneCartCheckBox1");
        //    }
        checkBox.setSelected(false);
        checkBox1.setSelected(false);
        this.getView().getModel("ui").setProperty("/componentsVisibility", true);
        this.getView().getModel("ui").setProperty("/visibleDiscounts", false);
        this.getView().getModel("ui").setProperty("/toggleBtnTxt", model.i18n.getText("showDefaultVariableDiscounts"));
        this.enableModel.setProperty("/editable", true);
        this.enableModel.refresh();
        //this.order.assignOrderDiscountToPos();
        
        //this.calculateFunction();
        this.calculateFunction_v2();
        //this.onSimulate();
        //sap.m.MessageToast.show(model.i18n._getLocaleText("CONFIRM_ORDER_FLAG"));
    }
    , onSave: function () {
    	
    	if(this.order.basketType == "ZRD")
    	{
    		var flagSelected = this.getView().getModel("appStatus").getProperty("/enableSaveButton");
            if (!flagSelected) {
                sap.m.MessageToast.show(model.i18n._getLocaleText("CONFIRM_ORDER_FLAG"));
                return;
            }
    	}
        
        var order = this.order.getData();
        //console.log(order);
        var username = this.getUserInfo().username;
        var selectedCustomerId = this.getCustomerInfo().customerId;
        var checkSavedOrderExistence = model.persistence.PersistenceManager.checkSavedData(username + selectedCustomerId + "UBackup", username + selectedCustomerId + "OBackup");
        if (checkSavedOrderExistence) {
            model.persistence.PersistenceManager.flushUserSavedData(username + selectedCustomerId + "UBackup");
            model.persistence.PersistenceManager.flushModel(username + selectedCustomerId + "OBackup");
        }
        if (window.backupInterval) {
            clearInterval(window.backupInterval);
        }
        this.appModel.setProperty("/intervalIsRunning", false);
        var positions = this.order.positions;
        for (var pos = 0; pos < positions.length; pos++) {
            if (positions[pos].quantity === 0 || positions[pos].quantity === undefined) {
                sap.m.MessageBox.alert(model.i18n._getLocaleText("0_QUANTITY"), {
                    title: model.i18n._getLocaleText("WARNING")
                });
                return;
            }
        }
        var that = this;
        var arr = [];
        utils.Busy.show();
        // this.busyDialog = utils.Busy;
        ///////****************************************************/////////////
        sap.m.MessageBox.show(model.i18n._getLocaleText("CONFIRM_CART_SAVE_TEXT"), {
            icon: sap.m.MessageBox.Icon.QUESTION
            , title: model.i18n._getLocaleText("CONFIRM_CART_SAVE_TITLE")
            , actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO]
            , onClose: _.bind(function (oAction) {
                / * test save * /
                //console.log(oAction);
                if (oAction === "YES") {
                    //TODO invio dati del ordine a SAP
                    // this.busyDialog.show();
                    var fSuccess = function (res) {
                        utils.Busy.hide();
                        // this.busyDialog.hide();
                        //console.log(res);
                        model.persistence.Storage.session.remove("currentHistoryCart");
                        model.Current.removeOrder();
                        sap.m.MessageToast.show(model.i18n._getLocaleText("ORDER_SAVED") + ": " + res.Vbeln);
                        ///////////********************************//////////////////////
                        that.order.orderId = res.Vbeln; //Maybe
                        var orderType = res.Auart;
                        if (orderType === "ZRD") {
                            this.routeTonav = "orderList";
                        }
                        else {
                            this.routeTonav = "offersList";
                        }
                        sap.m.MessageBox.show(model.i18n._getLocaleText("ASK_TO_STORIFY_CART"), {
                            icon: sap.m.MessageBox.Icon.QUESTION
                            , title: model.i18n._getLocaleText("STORIFY_CART_TITLE")
                            , actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO]
                            , onClose: function (oAction) {
                                // / * test save * /
                                //console.log(oAction);
                                if (oAction === "YES") {
                                    var cart = new model.Cart();
                                    cart.sendToSAP(that.order, that.user, false).then(
                                        //!Warning! Enter in success function but messageToast not showing
                                        _.bind(function (result) {
                                            sap.m.MessageToast.show(model.i18n.getText("CART_SAPSAVING_SUCCESS"));
                                            jQuery.sap.delayedCall(1000, that, function () {
                                                that.router.navTo(this.routeTonav);
                                            });
                                        }, that), _.bind(function (err) {
                                            sap.m.MessageToast.show(model.i18n.getText("CART_SAPSAVING_ERROR"));
                                            jQuery.sap.delayedCall(1000, that, function () {
                                                that.router.navTo(this.routeTonav);
                                            });
                                        }, that))
                                }
                                else {
                                    jQuery.sap.delayedCall(1000, that, function () {
                                        that.router.navTo(this.routeTonav);
                                    });
                                    //                    if (!!model.persistence.Storage.session.get("currentOrder")) {
                                    //                      model.Current.removeOrder();
                                    //                    }
                                    //                    setTimeout(that.router.navTo("newOrder"), 4000);
                                    //that.router.navTo("newOrder");
                                }
                            }
                        });
                        ///////////*******************************/////////////////////
                    };
                    fSuccess = _.bind(fSuccess, this);
                    var fError = function (err) {
                        utils.Busy.hide();
                        var error = JSON.parse(err.response.body);
                        var errorDetails = error.error.innererror.errordetails;
                        var msg = "Order creation failed";
                        var detailsMsg = _.where(errorDetails, function (item) {
                            return (item.code.indexOf("IWBEP") > 0);
                        });
                        for (var i = 0; detailsMsg && detailsMsg.length && i < detailsMsg.length; i++) {
                            msg += ", " + detailsMsg[i].message;
                        }
                        sap.m.MessageToast.show(msg);
                    };
                    fError = _.bind(fError, this);
                    //aggiungo all'ordine da inviare a SAP i campi del codice cliente e agente
                    var customer = JSON.parse(sessionStorage.getItem("currentCustomer"));
                    var codiciObj = {};
                    codiciObj.distrCh = customer.registry.distrCh;
                    codiciObj.society = customer.registry.society;
                    codiciObj.salesOrg = customer.registry.salesOrg;
                    codiciObj.division = customer.registry.division;
                    codiciObj.agentCode = customer.registry.agentCode;
                    codiciObj.customerId = customer.registry.id;
                    codiciObj.areaManager = customer.registry.areaManager;
                    codiciObj.territoryManager = customer.registry.territoryManager;
                    order.codiciObj = codiciObj;
                    that.order.sendToSAP(order).then(fSuccess, fError);
                }
                else {
                    utils.Busy.hide();
                    return;
                }
            }, that)
        });
        ///////****************************************************/////////////
    }
    , onShowProductPress: function (evt) {
        //    var product = this.getView().getModel("appStatus").getProperty("/currentProduct");
        //    if (product)
        //      this.router.navTo("productDetail", {
        //        id: product.getId()
        //      });
        //    else {
        //      this.router.navTo("empty");
        //    }
        var isPhone = sap.ui.Device.system.phone;
        if (!!isPhone) {
            this.router.navTo("productsListMobile");
        }
        else {
            this.router.navTo("empty");
        }
    }
    , onOrderHeaderPress: function (evt) {
        this.router.navTo("newOffer");
        this.appModel.setProperty("/restoreOrder", false); //I'm not sure it has any sense in here
    }
    , /********
       Primo metodo delete Rows che elimina dinamicamente dal model bindato alla view
       la posizione dall'array positions
       *************************************/ ///////
    onCancelRowPress: function (evt) {
        var position = evt.getParameters().listItem;
        var oContext = position.getBindingContext("c");
        var oModel = oContext.getModel();
        var row = oContext.getObject();
        var positionId = row.getId();
        this.order.removePosition(positionId);
        model.Current.setOrder(this.order);
        //        var oData = oModel.getData();
        //        var currentRowIndex = parseInt(oContext.getPath().split('/')[oContext.getPath().split('/').length - 1]);
        //        oData.positions.splice(currentRowIndex,1);
        oModel.updateBindings();
        this.onSimulate();
        this.getView().getModel("c").refresh(true);
    }
    , onChangeQuantity: function (evt) {
        var inputID = evt.getParameters().id;
        var inputField = sap.ui.getCore().byId(inputID);
        var compareDate = new Date();
        var obj = evt.getSource().getBindingContext("c").getObject();
        // Temp------------------------------------------------------
        //obj.calculateTotalListPrice();
        //-------------------------------------------------------
        if (isNaN(parseInt(evt.getSource().getValue()))) {
            evt.getSource().setValueState("Error");
            sap.m.MessageToast.show(model.i18n.getText("numberInputNeeded"));
            return;
        }
        var requestedDate = obj.wantedDate ? obj.wantedDate : this.order.requestedDate; //.toLocaleDateString();
        this.userInfo = this.getOrgDataForSAP();
        this.getView().setBusy(true);
        obj.getAvailability(this.userInfo, obj.quantity, requestedDate).then(_.bind(function (result) {
            this.order.refreshFullEvasionAvailableDate();
            //-------------------Temp-------------------------------
            // this.cartModel.refresh(true);
            //------------------------------------------------------
            //-----------------------To Add------------------------
            var position = result;
            position.refreshPriceValues(this.customer, this.order.IVACode).then(_.bind(function (res) {
                    this.getView().setBusy(false);
                    //console.log("Loaded discount");
                    //console.log(res);
                    this.cartModel.refresh(true);
                    this.onSimulate();
                }, this), _.bind(function (err) {
                    sap.m.MessageToast.show(utils.Message.getError(err));
                    this.getView().setBusy(false);
                }, this))
                //--------------------------------------------------
                // this.order.refreshFullEvasionAvailableDate();
                // this.cartModel.refresh(true);
        }, this), _.bind(function (err) {
            sap.m.MessageToast.show(utils.Message.getError(err));
            this.getView().setBusy(false);
        }, this));
        //var cModel = this.getView().getModel("c");
    }
    , onSetDiscountPress: function (evt) {
        var src = evt.getSource();
        var position = src.getBindingContext("c").getObject();
        var discount = position.getDiscount();
        //    var discountModel = new sap.ui.model.json.JSONModel(_.cloneDeep(discount.getModel_new().getData()));
        if (!this.discountModel) {
            this.discountModel = new sap.ui.model.json.JSONModel();
        }
        this.discountModel.setData(_.cloneDeep(discount.getModel_new().getData()))
        var page = this.getView().byId("cartPage");
        // this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog", this);
        if (!this.discountDialog) {
            this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog", this);
        }
        this.discountDialog.setModel(this.discountModel, "d");
        this.discountModel.setProperty("/en", {
            "wr": true
        });
        page.addDependent(this.discountDialog);
        this.discountDialog.open();
    }
    , onSetDiscountPress_v2: function (evt) {
        var src = evt.getSource();
        var position = src.getBindingContext("c").getObject();
        var discounts = position.getOrderDiscounts();
        //	    var discountModel = new sap.ui.model.json.JSONModel(_.cloneDeep(discount.getModel_new().getData()));
        if (!this.discountModel) {
            this.discountModel = new sap.ui.model.json.JSONModel();
        }
        this.discountModel.setData({
            "discountArray": _.cloneDeep(discounts)
        });
        var page = this.getView().byId("cartPage");
        // this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog", this);
        if (!this.discountDialog) {
            this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog_v2", this);
        }
        this.discountDialog.setModel(this.discountModel, "d");
        //	    this.discountModel.setProperty("/en", {"wr": true});
        page.addDependent(this.discountDialog);
        this.discountDialog.open();
    }
    , onDiscountDialogClose: function (evt) {
        this.discountDialog.close();
    }, //  onDiscountDialogOK: function (evt) {
    //
    //    //console.log(this.order);
    //    var discount = this.discountModel.getData();
    //    var position = _.find(this.order.positions, {positionId:discount.positionId});
    //    position.getDiscount().update(discount);
    //    this.discountDialog.close();
    //    this.refreshOrder();
    //
    //  },
    onDiscountDialogOK: function (evt) {
        //console.log(this.order);
        var discountPosition = this.discountModel.getData().discountArray[0].positionId;
        var position = _.find(this.order.positions, {
            positionId: discountPosition
        });
        position.setOrderDiscounts(this.discountModel.getData().discountArray);
        //	    if(!model.collections.DiscountItems.checkCommonDiscounts( this.discountModel.getData().discountArray))
        //		{
        //			
        //			this.order.setBasketType("ZOF");
        //			
        //		}
        this.order.checkPositionsDiscount();
        //this.order.checkOrderType();
        //this.order.refreshAgentCommission();
        this.discountDialog.close();
        this.refreshOrder();
    }
    , onResetDiscountPress: function (evt) {
        var discount = this.discountModel.getData();
        var position = _.find(this.order.positions, {
            positionId: discount.positionId
        });
        //    var discountModel = new sap.ui.model.json.JSONModel(_.cloneDeep(position.getDiscount().getModel_new().getData()));
        this.discountModel.setData({
                "discountArray": _.cloneDeep(position.getOrderDiscount())
            })
            //this.discountDialog.setModel(discountModel, "d");
    }
    , onSetForcedPrice: function (evt, discount) {
        if (discount) {
            var val = discount.value;
            var pos = _.find(this.order.positions, function (item) {
                return ((parseInt(item.positionId) == parseInt(discount.positionId)) || (item.positionId == discount.positionId));
            });
        }
        else {
            var val = evt.getSource().getValue();
            var pos = evt.getSource().getBindingContext("c").getObject();
        }
        val = isNaN(parseInt(val)) ? "" : parseInt(val);
        pos.setForcedPrice(val);
        this.onSimulate();
    }, //----------------------------NEW---------------------------------------------------
    availableDateFunction: function (obj) {
        var defer = Q.defer();
        var fSuccess = function (res) {
            this.order.refreshFullEvasionAvailableDate();
            this.cartModel.refresh(true);
            defer.resolve(res);
        }
        fSuccess = _.bind(fSuccess, this);
        var fError = function (err) {
            this.cartModel.refresh(true);
            defer.reject(err);
        }
        fError = _.bind(fError, this);
        var requestedDate = obj.wantedDate;
        var reqQty = obj.quantity;
        var product = obj.product;
        this.userInfo = this.getOrgDataForSAP();
        obj.getAvailability(this.userInfo, reqQty, requestedDate).then(fSuccess, fError)
        return defer.promise;
    }
    , refreshPriceValues: function (position) {
        var defer = Q.defer();
        var fSuccess = function (res) {
            //console.log("Loaded discount");
            //console.log(res);
            this.cartModel.refresh(true);
            defer.resolve(res);
        }
        fSuccess = _.bind(fSuccess, this);
        
        var fError = function (err) {
            defer.reject(err);
        }
        fError = _.bind(fError, this);
        position.refreshPriceValues(this.customer, this.order.IVACode).then(fSuccess, fError)
        return defer.promise;
    }
//    , calculateFunction: function () {
//        if (this.order && this.order.positions && this.order.positions.length > 0) {
//            var availableDatePromiseArray = [];
//            var refreshPricesPromiseArray = [];
//            for (var i = 0; i < this.order.positions.length; i++) {
////                if (!this.order.positions[i].availableDate) {
////                    var availableDateFunction = _.bind(this.availableDateFunction, this);
////                    availableDatePromiseArray.push(availableDateFunction(this.order.positions[i]));
////                }
//            	
//            	var availableDateFunction = _.bind(this.availableDateFunction, this);
//    			availableDatePromiseArray.push( availableDateFunction(this.order.positions[i]));
//    			
////                if (!this.order.positions[i].discount) {
////                    var refreshPriceFunction = _.bind(this.refreshPriceValues, this);
////                    refreshPricesPromiseArray.push(refreshPriceFunction(this.order.positions[i]));
////                }
//    			
//                  var refreshPriceFunction = _.bind(this.refreshPriceValues, this);
//                  refreshPricesPromiseArray.push(refreshPriceFunction(this.order.positions[i]));
//
//            }
//            var promiseArray = availableDatePromiseArray.concat(refreshPricesPromiseArray);
//            //      this.getView().setBusy(true);
//            utils.Busy.show();
//            Q.allSettled(promiseArray).then(_.bind(function (res) {
//                this.onSimulate();
//            }, this), _.bind(function (err) {
//                utils.Message.getError(err);
//            }, this)).fin(_.bind(function () {
//                //this.getView().setBusy(false);
//                utils.Busy.hide();
//            }, this))
//        }
 //   } //---------------------------------------------------------------------------------------------------------------------
    ,refreshPositionsPrices: function () {
        var positions = this.order.positions;
        _.forEach(positions, _.bind(function (item) {
            item.refreshPriceValues(this.customer).then(_.bind(function (res) {
                //console.log("Loaded discount");
                //console.log(res);
                this.cartModel.refresh(true);
                //this.onSimulate();
            }, this))
        }, this));
    }, // -------------------OLD-------------------------
    //availableDateFunction: function (obj) {
    //
    //
    //    var requestedDate = obj.wantedDate;
    //    var reqQty = obj.quantity;
    //    var product = obj.product;
    //    this.userInfo = this.getOrgDataForSAP();
    //    obj.getAvailability(this.userInfo, reqQty, requestedDate)
    //    .then(_.bind(function(result)
    //    {
    //
    //      this.order.refreshFullEvasionAvailableDate();
    //      this.cartModel.refresh(true);
    //      // this.refreshPositionsPrices();
    //      // this.cartModel.refresh(true);
    //    } ,this),_.bind(function(err){
    //    	this.cartModel.refresh(true);
    ////      this.refreshPositionsPrices();
    //
    //    },this))
    //
    //
    //
    //  },
    //refreshPriceValues:function(position)
    //{
    //  position.refreshPriceValues(this.customer)
    //  .then(_.bind(function(res){
    //    //console.log("Loaded discount");
    //    //console.log(res);
    //    this.cartModel.refresh(true);
    //    //this.onSimulate();
    //  }, this))
    //},
    //calculateFunction: function () {
    //
    //	  if (this.order && this.order.positions && this.order.positions.length > 0) {
    //	      //var defer = Q.defer();
    //	      for (var i = 0; i < this.order.positions.length; i++)
    //	      {
    //	    	  if(!this.order.positions[i].availableDate)
    //	    	  this.availableDateFunction(this.order.positions[i]);
    //	    	  if(!this.order.positions[i].discount)
    //	    		  this.refreshPriceValues(this.order.positions[i]);
    //	      }
    //
    //
    //	      //return defer.promise;
    //	    }
    //},
    //----------------------------------------------------------------------------
    //	var asyncLoop = function(o)
    //	{
    //		var i=-1;
    //		var loop = function()
    //		{
    //			i++;
    //			if(i==o.length)
    //			{
    //				o.callback();
    //				return;
    //			}
    //			o.functionToLoop(loop, i);
    //		} loop();//init}
    //	};
    //
    //    if (this.order && this.order.positions && this.order.positions.length > 0) {
    //    	asyncLoop({
    //    		length : 5,
    //    		functionToLoop : function(loop, i)
    //    		{
    //    			setTimeout(function()
    //    			{ document.write('Iteration ' + i + ' <br>');
    //    				loop();
    //    			},1000);
    //    		},
    //    		callback : function()
    //    		{
    //    			document.write('All done!');
    //    		}
    //    	});
    //    }
    // },
    //dialog visible
    onVisibleCell: function () {
        var table = sap.ui.getCore().getElementById("discountTable");
        // if(!table.getAggregation("items") || table.getAggregation("items").length <= 0)
        //   return;
        var cella = table.getAggregation("items")[0].getAggregation("cells")[1];
        var rows = table.getAggregation("items");
        for (var i = 1; i < rows.length; i++) {
            var c = rows[i].getAggregation("cells")[3];
            c.setVisible(false);
        }
        cella.setEditable(false);
        table.rerender();
        //    var position = evt.getParameters().listItem;
        //    var oContext = position.getBindingContext("c");
        //    var oModel = oContext.getModel();
        //    var row = oContext.getObject();
        //    var positionId = row.getId();
        //    this.order.removePosition(positionId);
        //        var oData = oModel.getData();
        //        var currentRowIndex = parseInt(oContext.getPath().split('/')[oContext.getPath().split('/').length - 1]);
        //        oData.positions.splice(currentRowIndex,1);
        //    oModel.updateBindings();
        //    this.getView().getModel("c").refresh(true);
        //
    }
    , onChangeDate: function (data) {
        var id = data.getSource().getId();
        var nPos = id.substring(id.lastIndexOf("-") + 1);
        var d = data.getParameter("value");
        var d1 = new Date(d);
        this.cartModel.getData().positions[nPos].wantedDate = d1;
        //console.log(d);
        //-----------------------------------------------------------------------
        var requestedDate = d1 //.toLocaleDateString();
        this.userInfo = this.getOrgDataForSAP();
        this.getView().setBusy(true);
        this.cartModel.getData().positions[nPos].getAvailability(this.userInfo, this.cartModel.getData().positions[nPos].quantity, requestedDate).then(_.bind(function (result) {
            this.order.refreshFullEvasionAvailableDate();
            this.cartModel.refresh(true);
            this.getView().setBusy(false);
        }, this), _.bind(function (err) {
            sap.m.MessageToast.show(utils.Message.getError(err));
            this.getView().setBusy(false);
        }, this));
    }
    , onDisplayComponentsPress: function (evt) {
        var position = evt.getSource().getBindingContext("c").getObject();
        if (!this._oPopover) {
            this._oPopover = sap.ui.xmlfragment("view.dialog.componentsPopover", this);
            this.getView().addDependent(this._oPopover);
            // this._oPopover.bindElement("/ProductCollection/0");
        }
        this.kitModel = new sap.ui.model.json.JSONModel();
        this.kitModel.setData(position.product);
        this.getView().setModel(this.kitModel, "p");
        // delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
        var oButton = evt.getSource();
        jQuery.sap.delayedCall(0, this, function () {
            this._oPopover.openBy(oButton);
        });
    }
    , checkSelectedCheckBox: function (evt) {
        var checkBox = this.getView().byId("dichiarazioneCartCheckBox");
        var checkBox1 = this.getView().byId("dichiarazioneCartCheckBox1");
        var saveButton = this.getView().byId("saveOrderButton");
        if (checkBox.getSelected() && checkBox1.getSelected()) {
            //saveButton.setEnabled(true);
            this.getView().getModel("appStatus").setProperty("/enableSaveButton", true);
        }
        else {
            this.getView().getModel("appStatus").setProperty("/enableSaveButton", false);
            //saveButton.setEnabled(false);
        }
        //        var source = evt.getSource();
        //        var saveButton = this.getView().byId("saveOrderButton");
        //        var val = source.getSelected();
        //        if(val === true){
        //            saveButton.setEnabled(true);
        //        }else{
        //            saveButton.setEnabled(false);
        //        }
    }, //   -----------------------OLD--------------------------------------------------
    //    onAssignDiscountPress:function(evt)
    //    {
    //      var src = evt.getSource();
    //      var discount = this.discountDialog.getModel("d").getData().ref;
    //      var appliedDiscounts = discount.getAppliedDiscounts();
    //      this.order.setDefaultDiscounts(appliedDiscounts);
    //      this.order.assignDiscountDefaultValueToAll();
    //      this.discountDialog.close();
    //    },
    //-------------------------------------------------------------------------------------
    checkPositions: function () {
        var positions = this.order.positions;
        for (var pos = 0; pos < positions.length; pos++) {
            if (positions[pos].quantity === 0 || positions[pos].quantity === undefined) {
                sap.m.MessageBox.alert(model.i18n._getLocaleText("0_QUANTITY"), {
                    title: model.i18n._getLocaleText("WARNING")
                });
                return false;
            }
        }
        return true;
    }
    , assignCustomerInfo: function () {
        var customer = JSON.parse(sessionStorage.getItem("currentCustomer"));
        var codiciObj = {};
        codiciObj.distrCh = customer.registry.distrCh;
        codiciObj.society = customer.registry.society;
        codiciObj.salesOrg = customer.registry.salesOrg;
        codiciObj.division = customer.registry.division;
        codiciObj.agentCode = customer.registry.agentCode;
        codiciObj.customerId = customer.registry.id;
        codiciObj.areaManager = customer.registry.areaManager;
        codiciObj.territoryManager = customer.registry.territoryManager;
        this.order.codiciObj = codiciObj;
    }
    , refreshOrder: function () {
        var order = this.order.getData();
        //console.log(order);
        var username = this.getUserInfo().username;
        var selectedCustomerId = this.getCustomerInfo().customerId;
        // var checkSavedOrderExistence = model.persistence.PersistenceManager.checkSavedData(username+selectedCustomerId+"UBackup",username+selectedCustomerId+"OBackup");
        // if(checkSavedOrderExistence){
        //     model.persistence.PersistenceManager.flushUserSavedData(username+selectedCustomerId+"UBackup");
        //     model.persistence.PersistenceManager.flushModel(username+selectedCustomerId+"OBackup");
        // }
        // if (window.backupInterval){
        //     clearInterval(window.backupInterval);
        // }
        // this.appModel.setProperty("/intervalIsRunning", false);
        var positions = this.order.positions;
        for (var pos = 0; pos < positions.length; pos++) {
            if (positions[pos].quantity === 0 || positions[pos].quantity === undefined) {
                sap.m.MessageBox.alert(model.i18n._getLocaleText("0_QUANTITY"), {
                    title: model.i18n._getLocaleText("WARNING")
                });
                return;
            }
        }
        var customer = JSON.parse(sessionStorage.getItem("currentCustomer"));
        var codiciObj = {};
        codiciObj.distrCh = customer.registry.distrCh;
        codiciObj.society = customer.registry.society;
        codiciObj.salesOrg = customer.registry.salesOrg;
        codiciObj.division = customer.registry.division;
        codiciObj.agentCode = customer.registry.agentCode;
        codiciObj.customerId = customer.registry.id;
        codiciObj.areaManager = customer.registry.areaManager;
        codiciObj.territoryManager = customer.registry.territoryManager;
        order.codiciObj = codiciObj;
        this.getView().setBusy(true);
        this.order.refreshOrderBySAP(order).then(_.bind(function (res) {
            this.getView().setBusy(false);
            //console.log("OK-SIMULA");
            //console.log(res);
            //sap.m.MessageToast.show(model.i18n.getText("SIMULATION_SUCCESS"))
            this.cartModel.setData(this.order);
            this.cartModel.refresh();
        }, this), _.bind(function (err) {
            this.getView().setBusy(false);
            //console.log("ERROR-SIMULA");
            //console.log(err);
            var error = JSON.parse(err.response.body);
            var errorTxt = error.error.message.value;
            //var error = JSON.parse(err.response.body);
            sap.m.MessageToast.show(errorTxt);
            this.cartModel.refresh();
        }, this))
    }
    , onDiscountCondChange: function (evt) {
        var src = evt.getSource().getBindingContext("d").getObject();
        if (src.typeId == "ZPMA") //Maybe this check should be moved to model.discount
        {
            this.onSetForcedPrice(evt, src);
        }
    }
    , onAssignDiscountPress: function (evt) {
        var src = this.discountDialog.getModel("d").getData();
        var position = this.order.getPosition(src.positionId);
        position.setDiscount(new model.Discount(src));
        this.order.assignPositionDiscountToAll(position);
        this.discountDialog.close();
        this.refreshOrder();
    }
    , onDuplicateDiscountPress: function (evt) {
        // sap.m.URLHelper.redirect("http://www.google.it", true);
        // return;
        this.copyDiscountPopover = sap.ui.xmlfragment("view.dialog.discountCopyPopover", this);
        var pos = evt.getSource().getBindingContext("c").getObject();
        this.copyDiscountPopover.setModel(this._getOtherPositionsModel(pos), "others");
        this.selectedDiscount = evt.getSource().getBindingContext("c").getObject().getDiscount();
        var page = this.getView().byId("cartPage");
        page.addDependent(this.copyDiscountPopover);
        var oButton = evt.getSource();
        jQuery.sap.delayedCall(0, this, function () {
            this.copyDiscountPopover.openBy(oButton);
        });
        // sap.m.MessageToast.show("Functionality yet to be implemented");
    }
    , _getOtherPositionsModel: function (pos) {
        if (!this.otherPosModel) {
            this.otherPosModel = new sap.ui.model.json.JSONModel();
        }
        this.otherPosData = {
            positions: []
        };
        for (var i = 0; i < this.order.positions.length; i++) {
            if (this.order.positions[i].positionId !== pos.positionId) {
                this.otherPosData.positions.push(this.order.positions[i]);
            }
        }
        this.otherPosModel.setData(this.otherPosData);
        return this.otherPosModel;
    }
    , onOKDiscountCopyButton: function (evt) {
        var src = evt.getSource();
        var list = sap.ui.getCore().byId("discountPositionList");
        var items = list.getSelectedItems();
        for (var i = 0; i < items.length; i++) {
            var position = this.order.getPosition(items[i].getBindingContext("others").getObject().positionId);
            position.getDiscount().copyDiscount(this.selectedDiscount);
        }
        this.refreshOrder();
        this.copyDiscountPopover.close();
        this.selectedDiscount = {};
    }
    , onCloseDiscountCopyButton: function (evt) {
        this.selectedDiscount = {};
        this.copyDiscountPopover.close();
    }
    , onSimulate: function (evt) {
        var order = this.order.getData();
        //console.log(order);
        var username = this.getUserInfo().username;
        var selectedCustomerId = this.getCustomerInfo().customerId;
        // var checkSavedOrderExistence = model.persistence.PersistenceManager.checkSavedData(username+selectedCustomerId+"UBackup",username+selectedCustomerId+"OBackup");
        // if(checkSavedOrderExistence){
        //     model.persistence.PersistenceManager.flushUserSavedData(username+selectedCustomerId+"UBackup");
        //     model.persistence.PersistenceManager.flushModel(username+selectedCustomerId+"OBackup");
        // }
        // if (window.backupInterval){
        //     clearInterval(window.backupInterval);
        // }
        // this.appModel.setProperty("/intervalIsRunning", false);
        var positions = this.order.positions;
        for (var pos = 0; pos < positions.length; pos++) {
            if (positions[pos].quantity === 0 || positions[pos].quantity === undefined) {
                sap.m.MessageBox.alert(model.i18n._getLocaleText("0_QUANTITY"), {
                    title: model.i18n._getLocaleText("WARNING")
                });
                return;
            }
        }
        var customer = JSON.parse(sessionStorage.getItem("currentCustomer"));
        var codiciObj = {};
        codiciObj.distrCh = customer.registry.distrCh;
        codiciObj.society = customer.registry.society;
        codiciObj.salesOrg = customer.registry.salesOrg;
        codiciObj.division = customer.registry.division;
        codiciObj.agentCode = customer.registry.agentCode;
        codiciObj.customerId = customer.registry.id;
        codiciObj.areaManager = customer.registry.areaManager;
        codiciObj.territoryManager = customer.registry.territoryManager;
        order.codiciObj = codiciObj;
        this.getView().setBusy(true);
        this.order.refreshOrderBySAP(order).then(_.bind(function (res) {
            this.getView().setBusy(false);
            //console.log("OK-SIMULA");
            //console.log(res);
            sap.m.MessageToast.show(model.i18n.getText("SIMULATION_SUCCESS"))
            this.cartModel.setData(this.order);
        }, this), _.bind(function (err) {
            this.getView().setBusy(false);
            //console.log("ERROR-SIMULA");
            //console.log(err);
            var error = JSON.parse(err.response.body);
            var errorTxt = error.error.message.value;
            //var error = JSON.parse(err.response.body);
            sap.m.MessageToast.show(errorTxt);
        }, this))
    }
    , onOrderDiscountChange: function () {
        this.order.assignOrderDiscountToPos();
        this.order.checkPositionsDiscount();
        this.order.checkOrderType();
        //      this.order.refreshAgentCommission();
        //      this.getView().getModel("c").refresh();
    }
    , onOrderDiscountSliderChange: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("c").getObject();
        discountItem.value = srcValue;
        this.getView().getModel("c").refresh();
        //    		this.order.refreshAgentCommission();
        //    		this.getView().getModel("c").refresh();
    }
    , onDiscountSliderChange: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("d").getObject();
        discountItem.value = srcValue;
        this.discountDialog.getModel("d").refresh();
        //    		this.order.refreshAgentCommission();
        //    		this.getView().getModel("c").refresh();
    }
    , onOrderDiscountValueChosen: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("c").getObject();
        if (discountItem.typeId == "ZSL3") {
            if (!model.collections.DiscountItems.checkCommonDiscounts(this.order.commonDiscounts)) {
                if (this.order.getSelectedType() == "ZRD") sap.m.MessageBox.alert(model.i18n.getText("orderOfferChange_maxValue"));
                else sap.m.MessageBox.alert(model.i18n.getText("offerChange_maxValue"));
            }
        }
        this.getView().getModel("c").refresh();
        //		var discountItem = src.getBindingContext("c").getObject();
        //		if(srcValue > discountItem.maxValue)
        //		{
        //			sap.m.MessageBox.information(model.i18n.getText("orderOfferChange_maxValue"));
        //			
        //		}
    }
    , onDiscountValueChosen: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("d").getObject();
        if (discountItem.typeId == "ZSL3") {
            if (!model.collections.DiscountItems.checkCommonDiscounts(this.discountModel.getData().discountArray)) {
                if (this.order.getSelectedType() == "ZRD") sap.m.MessageBox.alert(model.i18n.getText("orderOfferChange_maxValue"));
                else sap.m.MessageBox.alert(model.i18n.getText("offerChange_maxValue"));
            }
        }
        this.discountDialog.getModel("d").refresh();
        //		if(srcValue > discountItem.maxValue)
        //		{
        //			sap.m.MessageBox.information(model.i18n.getText("orderOfferChange_maxValue"));
        //			
        //		}
    }
    , onOrderInputDiscountChange: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("c").getObject();
        
        var limit = this.order.basketType == "ZOF" ? discountItem.maxValue : (discountItem.limiter? discountItem.limiter : discountItem.maxValue);
		//if(srcValue > discountItem.maxValue)
        if(srcValue > limit)
		{
			sap.m.MessageBox.information(model.i18n.getText("discountInputError_maxValueOverflow"));
			//discountItem.value = discountItem.maxValue;
			discountItem.value = limit;
		
		}
        
        if (discountItem.typeId == "ZSL3") {
            if (!model.collections.DiscountItems.checkCommonDiscounts(this.order.commonDiscounts)) {
                if (this.order.getSelectedType() == "ZRD") sap.m.MessageBox.alert(model.i18n.getText("orderOfferChange_maxValue"));
                else sap.m.MessageBox.alert(model.i18n.getText("offerChange_maxValue"));
            }
        }
        this.getView().getModel("c").refresh();
        //		this.order.refreshAgentCommission();
        //		this.getView().getModel("c").refresh();
    }
    , onInputDiscountChange: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("d").getObject();
        var limit = this.order.basketType == "ZOF" ? discountItem.maxValue : (discountItem.limiter? discountItem.limiter : discountItem.maxValue);
		//if(srcValue > discountItem.maxValue)
        if(srcValue > limit)
		{
			sap.m.MessageBox.information(model.i18n.getText("discountInputError_maxValueOverflow"));
			//discountItem.value = discountItem.maxValue;
			discountItem.value = limit;
		}
        if (discountItem.typeId == "ZSL3") {
            if (!model.collections.DiscountItems.checkCommonDiscounts(this.discountModel.getData().discountArray)) {
                if (this.order.getSelectedType() == "ZRD") sap.m.MessageBox.alert(model.i18n.getText("orderOfferChange_maxValue"));
                else sap.m.MessageBox.alert(model.i18n.getText("offerChange_maxValue"));
            }
        }
        this.discountDialog.getModel("d").refresh();
        //		this.order.refreshAgentCommission();
        //		this.getView().getModel("c").refresh();
    }
    , onShowDefaultPress: function (evt) {
        var src = evt.getSource();
        var pressed = src.getPressed();
        var text = pressed ? model.i18n.getText("hideVariableDiscounts") : model.i18n.getText("showDefaultVariableDiscounts");
        src.setText(text);
        this.uiModel.setProperty("/visibleDiscounts", pressed);
        this.uiModel.setProperty("/toggleBtnTxt", text);
        this.uiModel.refresh();
    }
    , onAssignToAllPress: function (evt) {
        this.order.assignOrderDiscountToPos();
        this.order.checkPositionsDiscount();
        //this.order.checkOrderType();
        //To move maybe in success function of refreshOrder
        //this.order.refreshAgentCommission();
        //------------ Added L.C. 09/09/16 12.40------------------
        this.refreshOrder();
        //this.getView().getModel("c").refresh();
    },
    
    calculateFunction_v2 : function()
    {
    	
    	
          if (this.order && this.order.positions && this.order.positions.length > 0) {

    		
              var that = this;
              utils.Asynchronous.asyncLoop(
            {
            	length:that.order.positions.length,
            	functionToLoop:function(loop, i)
            	{
            		 var fSuccess = function (res) {
                        
                         loop();
                     }
                     var fError = function (err) {
                    	 sap.m.MessageToast.show(utils.Message.getError(err));
                         loop();
                     }
                     fSuccess = _.bind(fSuccess, that);
                     fError = _.bind(fError, that);
                     var refreshPriceFunction = _.bind(that.refreshPriceValues, that, that.order.positions[i]);
                     if(i == 0)
                    	 utils.Busy.show();
                     
                     that.availableDateFunction(that.order.positions[i])
                     .then(refreshPriceFunction, refreshPriceFunction)
                     .then(fSuccess, fError)
                     
            	},
            	callback: _.bind(function(){
            		utils.Busy.hide();
            		that.onSimulate()}, that)
            })
          };
          
        
//    	if(this.order && this.order.positions && this.order.positions.length > 0)
//    	{
//    		var fillPosition = function(position)
//    		{
//    			var defer = Q.defer();
//    			var fSuccess = function(res)
//    			{
//    				defer.resolve(res);
//    			}
//    			var fError = function(err)
//    			{
//    				sap.m.MessageToast.show(utils.Message.getError(err));
//    				defer.resolve(err);
//    			}
//    			fSuccess = _.bind(fSuccess, this);
//    			fError = _.bind(fError, this);
//    			
//    			var refreshPrice = _.bind(this.refreshPriceValues, this, position);
//    			var availability = _.bind(this.availableDateFunction, this, position);
//    			availability()
//    			.then(refreshPrice, refreshPrice)
//    			.then(fSuccess, fError)
//    			
//    			
//    			return defer.promise;
//    		}
//    		fillPosition=_.bind(fillPosition, this);
//    		
//    		var promiseArray = [];
//    		for(var i = 0; i<this.order.positions.length ; i++)
//    		{
//    			promiseArray.push(fillPosition(this.order.positions[i]));
//    		}
//	         
//	          utils.Busy.show();
//	          Q.allSettled(promiseArray).then(_.bind(function (res) {
//	              this.onSimulate();
//	          }, this), _.bind(function (err) {
//	              utils.Message.getError(err);
//	          }, this)).fin(_.bind(function () {
//	              //this.getView().setBusy(false);
//	              utils.Busy.hide();
//	          }, this))
//
//    	}

    	
    }
});