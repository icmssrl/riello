jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Orders");
jQuery.sap.require("model.collections.TestOrders");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Offer");
jQuery.sap.require("model.Current");
jQuery.sap.require("utils.Busy");
// jQuery.sap.require("sap.m.URLHelper");
jQuery.sap.require("view.abstract.AbstractMasterController");
view.abstract.AbstractMasterController.extend("view.offer.OfferInfo", {
    onExit: function () {}
    , onInit: function () {
        // this.router = sap.ui.core.UIComponent.getRouterFor(this);
        // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        //this.notesModel = new sap.ui.model.json.JSONModel();
        //this.notesModel.setData({"billsItems":[], "salesItems":[]});
        //this.getView().setModel(this.notesModel,"notes");
        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");
        var oModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(oModel);
        this.orderModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.orderModel, "order");
        this.gruModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.gruModel, "gru");
        this.enableModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.enableModel, "enable");
        /* Temporary*/
        this.cModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.cModel, "c");
        
        
        // this.positionModel = new sap.ui.model.json.JSONModel();
        // this.getView().setModel(this.positionModel, "position");
        
        //Flag model - requested declarations
        this.flagsModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.flagsModel, "flags");
    }
    , handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");
        var id = evt.getParameters().arguments.id;
        if (name !== "offerInfo") {
            return;
        }
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        this.user = model.persistence.Storage.session.get("user");
        this.userModel.setData(this.user);
        
        this.isTradePolicies = this.checkQuotationEnabledToTradePolicies();
        //**
        this.uiModel.setProperty("/orderInfo", true);
        this.uiModel.setProperty("/orderCreate", false);
        this.uiModel.setProperty("/componentsVisibility", false);
        this.getView().getModel("ui").setProperty("/visibleDiscounts", false);
        this.getView().getModel("ui").setProperty("/toggleBtnTxt", model.i18n.getText("showDefaultVariableDiscounts"));
        this.uiModel.refresh();
        //**
        var flagged = (model.persistence.Storage.session.get("flagged") == "X")
        this.flagsModel.setData({check1:flagged, check2:flagged});
        this.flagsModel.refresh();
        //**
        this.enableModel.setProperty("/sourceList", model.persistence.Storage.session.get("sourceList"));
        var sourceList = this.enableModel.getProperty("/sourceList");
        var offerStatus = model.persistence.Storage.session.get("selectedOffer").orderStatus;
        var offer = new model.Offer(model.persistence.Storage.session.get("selectedOffer"));
        this.offer = offer;
        this.enableModel.setProperty("/editable", offer.isEditable(this.user));
        this.enableModel.setProperty("/convertible", offer.isConvertible());
        this.enableModel.setProperty("/isCorrected", false);
        this.enableModel.setProperty("/isRefused", this.offer.orderStatus == "REJ");
        this.cModel.setProperty("/basketType", "ZOF");
        this.cModel.refresh();
        //**
        this.order = new model.Order();
        utils.Busy.show();
        this.order.load(id)
            //  model.collections.TestOrders.loadTestOrdersById(id) //forceReload
            .then(_.bind(function (res) {
                //console.log(res);
                utils.Busy.hide();
                this.order = res;
                var reqDt = (model.persistence.Storage.session.get("selectedOffer")).validDateList;
                this.order.requestedDate = new Date(reqDt);
                var shippDt = (model.persistence.Storage.session.get("selectedOffer")).shippmentDate;	
                this.order.shippmentDate = new Date(shippDt);
                this.orderModel.setData(this.order);
                this.orderModel.refresh(true);
                this.gruModel.setData(this.order.gru);
                this.populateNotesText();
                //this.populateCommonDiscount();
                this.saveInitialOffer();
                //If user is AM/TM, than discount values can be modified as they want
                if(this.user.type !== "AG")
                {
                	for(var i=0; i< this.order.positions.length;i++)
                	{
                		var pos = this.order.positions[i];
                		var discount = _.find(pos.discount.discountArray, {typeId:"ZSL4"})
                		discount.isEditable=true;
                		
                		var orderDiscount = _.find(pos.orderDiscounts, {typeId:"ZSL4"})
                		orderDiscount.isEditable=true;
                	}
                }
                // this.positionModel.setData(this.order.positions);
                // this.positionModel.refresh(true);
                //var oTable = this.getView().byId("orderListTable");
            }, this), _.bind(function (err) {
                utils.Busy.hide();
            }, this));
        //    this.getView().getModel("notes").refresh();
        //    this.customer = model.Current.getCustomer();
        //    if (!this.customer) {
        //      var customerData = model.persistence.Storage.session.get("currentCustomer");
        //      this.customer = new model.Customer(customerData);
        //    }
        //    this.getView().setModel(this.customer.getModel(), "customer");
        //
        //    this.order = model.Current.getOrder();
        //
        //    if (!this.order) {
        //      this.order = new model.Order();
        //      this.order.create(this.customer.registry.id)
        //        .then(_.bind(function () {
        //          this.orderModel = this.order.getModel();
        //          this.getView().setModel(this.orderModel, "o");
        //          var destinations = {
        //            "items": []
        //          };
        //          destinations.items = this.customer.destinations;
        //          this.destinationsModel = new sap.ui.model.json.JSONModel(destinations);
        //          this.getView().setModel(this.destinationsModel, "d");
        //        }, this));
        //
        //    }
        //
        //
        //
        //
        //
        //
        //    this.populateSelect();
        //    // this.getView().byId("regAlrtBtn").setVisible(false);
        //    // this.getView().byId("ctAlrtBtn").setVisible(false);
        //    // this.getView().byId("bkAlrtBtn").setVisible(false);
        //    // this.getView().byId("slsAlrtBtn").setVisible(false);
    }
    , populateNotesText: function () {
        var item = {
            "type": "Tdid"
            , "namespace": "notes"
        };
        var workingUser = model.persistence.Storage.session.get("workingUser");
        this.orgData = {
            "Bukrs": workingUser.organizationData.results[0].society
            , "Vkorg": workingUser.organizationData.results[0].salesOrg
            , "Vtweg": workingUser.organizationData.results[0].distributionChannel
            , "Spart": workingUser.organizationData.results[0].division
        };
        this.getView().setBusy(true);
        utils.Collections.getOdataSelect(item.type, this.orgData).then(_.bind(function (result) {
            //Starting from Here?
            var notes = result.getData().results;
            var billNote = _.find(notes, {
                Tdid: "Z002"
            });
            var salesNote = _.find(notes, {
                Tdid: "Z001"
            });
            var creditNote = _.find(notes, {
                Tdid: "Z013"
            });
            this.order.billNoteTxt = billNote ? billNote.Tdtext : "";
            this.order.salesNoteTxt = salesNote ? salesNote.Tdtext : "";
            this.order.creditNoteTxt = creditNote ? creditNote.Tdtext : "";
            this.getView().getModel("order").refresh();
            this.getView().setBusy(false);
        }, this));
    }
    , populateCommonDiscount: function (evt) {
        var selectedOffer = model.persistence.Storage.session.get("selectedOffer");
        this.order.setCommonDiscounts(selectedOffer.offerDiscounts);
        if (selectedOffer.positionsDiscounts) {
            for (var i = 0; this.order.positions && i < this.order.positions.length; i++) {
                var discountArray = _.filter(selectedOffer.positionsDiscounts, {
                    positionId: this.order.positions[i].positionId
                });
                this.order.positions[i].setOrderDiscounts(discountArray);
            }
        }
        if (selectedOffer.agentCommission) this.order.agentCommission = selectedOffer.agentCommission;
    }, //Previous Version-------------------------------
    onDiscountPress: function (evt) {
        var src = evt.getSource();
        var position = src.getBindingContext("order").getObject();
        var discount = position.getDiscount();
        //var discount = position.discount;
        var discountModel = discount.getModel_new();
        //discountModel.setData(discount);
        //    for (var prop in discount) {
        //        if (_.isObject(this[prop])) {
        //          _.merge(discountModel.getData()[prop], discount[prop]);
        //        } else {
        //          discountModel.getData()[prop] = discount[prop];
        //        }
        //      }
        var page = this.getView().byId("orderInfoId");
        this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialogRO", this);
        this.discountDialog.setModel(discountModel, "d");
        discountModel.setProperty("/en", {
            "wr": false
        });
        page.addDependent(this.discountDialog);
        this.discountDialog.open();
    }, //--------------------------------------------------------------------------
    onSetDiscountPress_v2: function (evt) {
        var src = evt.getSource();
        var position = src.getBindingContext("order").getObject();
        var discounts = position.getOrderDiscounts();
        //	    var discountModel = new sap.ui.model.json.JSONModel(_.cloneDeep(discount.getModel_new().getData()));
        if (!this.discountModel) {
            this.discountModel = new sap.ui.model.json.JSONModel();
        }
        this.discountModel.setData({
            "discountArray": _.cloneDeep(discounts)
        });
        var page = this.getView().byId("offerInfoId");
        // this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog", this);
        if (!this.discountDialog) {
            this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog_v2", this);
        }
        this.discountDialog.setModel(this.discountModel, "d");
        //	    this.discountModel.setProperty("/en", {"wr": true});
        page.addDependent(this.discountDialog);
        this.discountDialog.open();
    }
    , onDiscountDialogClose: function (evt) {
        this.discountDialog.close();
    }
    , onDiscountDialogOK: function (evt) {
        //console.log(this.order);
        var discountPosition = this.discountModel.getData().discountArray[0].positionId;
        var position = _.find(this.order.positions, {
            positionId: discountPosition
        });
        
        position.setOrderDiscounts(this.discountModel.getData().discountArray, {keepEditable:(this.user.type == "AM" || this.user.type == "TM"), keepValue: (this.user.type == "AM" || this.user.type == "TM")} );
        this.enableModel.setProperty("/isCorrected", this.order.checkPositionsDiscount() && this.offer.orderStatus == "REJ");
        //this.order.refreshAgentCommission();
        this.discountDialog.close();
        this.getView().getModel("order").refresh();
    }, // refreshDiscount: function (evt) {
    //   var discount = this.discountDialog.getModel("d").getData().ref;
    //   this.discountDialog.setModel(discount.refreshModel(), "d");
    // },
    toOrderListPress: function (evt) {
        this.router.navTo("offerList");
    }, // onLinkPress:function(evt)
    // {
    //   // sap.m.MessageToast.show("Functionality not implemented yet");
    //   var src = evt.getSource();
    //   var url = icms.Component.getMetadata().getConfig().settings.serverUrl;
    //   var attachId = evt.getSource().getBindingContext("order").getObject().attachId;
    //   url += "SO_ContentAttachSet(IObjId='"+attachId+"')/$value";
    //   sap.m.URLHelper.redirect(url, true);
    // },
    //
    //  refreshView: function (data) {
    //    this.getView().setModel(data.getModel(), "o");
    //    // var masterCntrl = this.getView().getModel("appStatus").getProperty("/masterCntrl");
    //    // masterCntrl.refreshList();
    //  },
    //
    //  //Feed
    //  onPost: function (oEvent) {
    //
    //    var id = oEvent.getSource().getId();
    //    var noteType = id.substr(id.lastIndexOf("-") + 1, id.length);
    //
    //
    //    // create new entry
    //    var sValue = oEvent.getParameter("value");
    //    var oEntry = {
    //      author: model.persistence.Storage.session.get("user").fullName,
    //      text: sValue
    //    };
    //
    //    this.order.addNote(noteType, oEntry);
    //
    //    // update model
    //    var oModel = this.order.getModel();
    //    this.getView().getModel("o").refresh();
    //
    //  },
    //
    //
    //
    //  onSavePress: function () {
    //
    //    var saveButton = this.getView().byId("saveButton");
    //    saveButton.setEnabled(false);
    //
    //    model.Current.setOrder(this.order);
    //    model.persistence.Storage.session.save("currentOrder", this.order);
    //
    //
    //  },
    //  onResetPress: function () {
    //    this.order = new model.Order();
    //    this.getView().getModel("o").refresh();
    //  },
    //
    //
    //  onAddProductsPress: function (evt) {
    //    if (this.order) {
    //      model.Current.setOrder(this.order);
    //      model.persistence.Storage.session.save("currentOrder", this.order);
    //    }
    //    this.router.navTo("empty");
    //  },
    //
    //  //funzioni per allegare file
    //
    //
    formatAttribute: function (sValue) {
        jQuery.sap.require("sap.ui.core.format.FileSizeFormat");
        if (jQuery.isNumeric(sValue)) {
            return sap.ui.core.format.FileSizeFormat.getInstance({
                binaryFilesize: false
                , maxFractionDigits: 1
                , maxIntegerDigits: 3
            }).format(sValue);
        }
        else {
            return sValue;
        }
    }
    , onPressDeliveryType: function () {
        if (sap.ui.getCore().byId("dialogCamionGru")) sap.ui.getCore().byId("dialogCamionGru").destroy(true);
        this.camionGru = sap.ui.xmlfragment("view.dialog.camionGru", this);
        var page = this.getView().byId("orderInfoId");
        page.addDependent(this.camionGru);
        this.camionGru.open();
    }
    , onOkPress: function (evt) {
        if (this.camionGru) {
            this.camionGru.close();
        }
    }
    , onKitPress: function (evt) {
        var position = evt.getSource().getBindingContext("order").getObject();
        var src = evt.getSource();
        if (!this._oPopover) {
            this._oPopover = sap.ui.xmlfragment("view.dialog.componentsPopover", this);
            this.getView().addDependent(this._oPopover);
            // this._oPopover.bindElement("/ProductCollection/0");
        }
        var req = this.getOrgDataForSAP();
        this.getView().setBusy(true);
        position.getComponents(req).then(_.bind(function (result) {
            this.kitModel = new sap.ui.model.json.JSONModel();
            this.kitModel.setData(position);
            this.getView().setModel(this.kitModel, "p");
            // delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
            this.getView().setBusy(false);
            jQuery.sap.delayedCall(0, this, function () {
                this._oPopover.openBy(src);
            });
        }, this), _.bind(function (err) {
            this.getView().setBusy(false);
            sap.m.MessageToast.show(utils.Message.getError(err));
        }, this))
    }
    , toOfferListPress: function (evt) {
        this.router.navTo("offersList");
    }
    , toOfferApprovationsListPress: function (evt) {
        this.router.navTo("offerApprovationsList");
    }
    , onAcceptPress: function (evt) {
        this.order.setOfferDiscountStatus(this.user, (this.checkIfChangedPositionDiscounts()) ? false : "M");
        this.order.offerApprovation(this.user, this.order).then(_.bind(function () {
            this.router.navTo("offerApprovationsList");
        }, this), _.bind(function (err) {
            sap.m.MessageToast.show(utils.Message.getError(err));
        }));
    }
    , onRejectPress: function (evt) {
        this.order.setOfferDiscountStatus(this.user, "R");
        this.order.offerApprovation(this.user, this.order).then(_.bind(function () {
            this.router.navTo("offerApprovationsList");
        }, this), _.bind(function (err) {
            sap.m.MessageToast.show(utils.Message.getError(err));
        }));
    }
    , onEditPress: function (evt) {
        var selectedOffer = model.persistence.Storage.session.get("selectedOffer");
        selectedOffer.offerDiscounts = this.order.commonDiscounts;
        selectedOffer.agentCommission = this.order.agentCommission;
        selectedOffer.positionsDiscounts = [];
        for (var i = 0; this.order.positions && i < this.order.positions.length; i++) {
            selectedOffer.positionsDiscounts = selectedOffer.positionsDiscounts.concat(this.order.positions[i].getOrderDiscounts());
        }
        this.order.setOfferDiscountStatus(this.user);
        var temporaryOffers = model.persistence.Storage.session.get("temporaryOffers") ? model.persistence.Storage.session.get("temporaryOffers") : [];
        if (this.enableModel.getProperty("/sourceList") == "offerList") {
            selectedOffer.orderStatus = this.order.getOrderStatus();
            selectedOffer.orderStatusDescr = this.order.getOrderStatusDescr();
            var found = false;
            for (var i = 0; i < temporaryOffers.length && !found; i++) {
                if (temporaryOffers[i].orderId == selectedOffer.orderId) {
                    temporaryOffers[i] = selectedOffer;
                }
            }
            if (!found) temporaryOffers.push(selectedOffer);
            model.persistence.Storage.session.save("temporaryOffers", temporaryOffers);
            this.router.navTo("offersList");
        }
        //    else
        //    {
        //      selectedOffer.orderStatus = "M";
        //      selectedOffer.orderStatusDescr="Respinto Con Modifica";
        //      var found = false;
        //      for(var i = 0; i< temporaryOffers.length && !found ; i++)
        //      {
        //        if(temporaryOffers[i].orderId == selectedOffer.orderId)
        //        {
        //          temporaryOffers[i]= selectedOffer;
        //        }
        //      }
        //
        //      if(!found)
        //          temporaryOffers.push(selectedOffer);
        //
        //      model.persistence.Storage.session.save("temporaryOffers", temporaryOffers);
        //      this.router.navTo("offerApprovationsList");
        //    }
        //    
    }
    , onDiscountSliderChange: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("order").getObject();
        discountItem.value = srcValue;
        this.getView().getModel("order").refresh();
    }
    , onOrderDiscountValueChosen: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("order").getObject();
        if (discountItem.typeId == "ZSL3") {
        	if(this.user.type == "AG")
        	{
        		if (!model.collections.DiscountItems.checkCommonDiscounts(this.order.commonDiscounts)) {
                    if (this.offer.orderStatus == "REJ" && this.enableModel.getData().sourceList == "offersList") {
                        sap.m.MessageBox.show(model.i18n.getText("discountMustBeInLimit"));
                    }
                }
        	}
            
        }
        else if (discountItem.typeId == "ZSL4") {
            if ((this.user.type == "TM" || this.user.type == "AM") && this.enableModel.getData().sourceList == "offerApprovationsList") {
                if (srcValue < 10) {
//                    sap.m.MessageBox.show(model.i18n.getText("orderDiscountMustBeOverMinValue"));
//                    src.setValue(10);
//                    discountItem.value = 10;
                }
            }
        }
        this.getView().getModel("order").refresh();
        //			var discountItem = src.getBindingContext("c").getObject();
        //			if(srcValue > discountItem.maxValue)
        //			{
        //				sap.m.MessageBox.information(model.i18n.getText("orderOfferChange_maxValue"));
        //				
        //			}
    }
    , onDiscountValueChosen: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("d").getObject();
        if (discountItem.typeId == "ZSL3") {
        	if(this.user.type == "AG")
        	{
        		if (!model.collections.DiscountItems.checkCommonDiscounts(this.discountModel.getData().discountArray)) {
                    if (this.offer.orderStatus == "REJ" && this.enableModel.getData().sourceList == "offersList") {
                        sap.m.MessageBox.show(model.i18n.getText("discountMustBeInLimit"));
                    }
                }
        	}
            
        }
        else if (discountItem.typeId == "ZSL4") {
            if ((this.user.type == "TM" || this.user.type == "AM") && this.enableModel.getData().sourceList == "offerApprovationsList") {
                if (srcValue < 10) {
//                    sap.m.MessageBox.show(model.i18n.getText("orderDiscountMustBeOverMinValue"));
//                    src.setValue(10);
//                    discountItem.value = 10;
                }
            }
        }
        this.discountDialog.getModel("d").refresh();
        //			if(srcValue > discountItem.maxValue)
        //			{
        //				sap.m.MessageBox.information(model.i18n.getText("orderOfferChange_maxValue"));
        //				
        //			}
    }
    , onOrderInputDiscountChange: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("order").getObject();
        if (srcValue > discountItem.maxValue) {
           // sap.m.MessageBox.information(model.i18n.getText("orderOfferChange_maxValue"));
        	sap.m.MessageBox.information(model.i18n.getText("discountInputError_maxValueOverflow"));
			discountItem.value = discountItem.maxValue;
        }
        if (discountItem.typeId == "ZSL3") {
        	if(this.user.type == "AG")
        	{
        		if (!model.collections.DiscountItems.checkCommonDiscounts(this.order.commonDiscounts)) {
                    if (this.offer.orderStatus == "REJ" && this.enableModel.getData().sourceList == "offersList") {
                        sap.m.MessageBox.show(model.i18n.getText("discountMustBeInLimit"));
                    }
                }
        	}
            
        }
        else if (discountItem.typeId == "ZSL4") {
            if ((this.user.type == "TM" || this.user.type == "AM") && this.enableModel.getData().sourceList == "offerApprovationsList") {
                if (srcValue < 10) {
//                    sap.m.MessageBox.show(model.i18n.getText("orderDiscountMustBeOverMinValue"));
//                    src.setValue(10);
//                    discountItem.value = 10;
                }
            }
        }
        this.getView().getModel("order").refresh();
        //			this.order.refreshAgentCommission();
        //			this.getView().getModel("c").refresh();
    }
    , onOrderDiscountChange: function () {
        this.order.assignOrderDiscountToPos();
        //      this.order.refreshAgentCommission();
        //      this.getView().getModel("c").refresh();
    }
    , onOrderDiscountSliderChange: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("order").getObject();
        discountItem.value = srcValue;
        this.getView().getModel("order").refresh();
        //    		this.order.refreshAgentCommission();
        //    		this.getView().getModel("c").refresh();
    }
    , onDiscountSliderChange: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("d").getObject();
        discountItem.value = srcValue;
        this.discountDialog.getModel("d").refresh();
        //    		this.order.refreshAgentCommission();
        //    		this.getView().getModel("c").refresh();
    }, //    onOrderInputDiscountChange:function(evt)
    //    {
    //    	var src = evt.getSource();
    //		var srcValue = src.getValue();
    //		
    //		var discountItem = src.getBindingContext("order").getObject();
    //		
    ////		this.order.refreshAgentCommission();
    ////		this.getView().getModel("c").refresh();
    //    },
    onInputDiscountChange: function (evt) {
        var src = evt.getSource();
        var srcValue = src.getValue();
        var discountItem = src.getBindingContext("d").getObject();
        
        if (srcValue > discountItem.maxValue) {
            // sap.m.MessageBox.information(model.i18n.getText("orderOfferChange_maxValue"));
         	sap.m.MessageBox.information(model.i18n.getText("discountInputError_maxValueOverflow"));
 			discountItem.value = discountItem.maxValue;
         }
        if (discountItem.typeId == "ZSL3" && this.user.type == "AG") {
            if (!model.collections.DiscountItems.checkCommonDiscounts(this.discountModel.getData().discountArray)) {
                if (this.offer.orderStatus == "REJ" && this.enableModel.getData().sourceList == "offersList") {
                    sap.m.MessageBox.show(model.i18n.getText("discountMustBeInLimit"));
                }
            }
        }
        else if (discountItem.typeId == "ZSL4") {
            if ((this.user.type == "TM" || this.user.type == "AM") && this.enableModel.getData().sourceList == "offerApprovationsList") {
                if (srcValue < 10) {
//                    sap.m.MessageBox.show(model.i18n.getText("orderDiscountMustBeOverMinValue"));
//                    src.setValue(10);
//                    discountItem.value = 10;
                }
            }
        }
        this.discountDialog.getModel("d").refresh();
        //		this.order.refreshAgentCommission();
        //		this.getView().getModel("c").refresh();
    }
    , onShowDefaultPress: function (evt) {
        var src = evt.getSource();
        var pressed = src.getPressed();
        var text = pressed ? model.i18n.getText("hideVariableDiscounts") : model.i18n.getText("showDefaultVariableDiscounts");
        src.setText(text);
        this.uiModel.setProperty("/visibleDiscounts", pressed);
        this.uiModel.setProperty("/toggleBtnTxt", text);
        this.uiModel.refresh();
    }
    , onAssignToAllPress: function (evt) {
        this.order.assignOrderDiscountToPos((this.user.type == "AM" || this.user.type == "TM"));
        this.enableModel.setProperty("/isCorrected", this.order.checkPositionsDiscount() && this.offer.orderStatus == "REJ");
        //this.order.refreshAgentCommission();
        this.getView().getModel("order").refresh();
    }
    , saveInitialOffer: function (evt) {
        model.persistence.Storage.session.save("initialOffer", this.order);
    }
    , checkIfChangedPositionDiscounts: function () {
        var initialOffer = model.persistence.Storage.session.get("initialOffer");
        var equal = true;
        for (var i = 0; initialOffer && initialOffer.positions.length > 0 && i < initialOffer.positions.length; i++) {
            for (var j = 0; j < this.order.positions[i].orderDiscounts.length; j++) {
                if (this.order.positions[i].orderDiscounts[j] !== initialOffer.positions[i].orderDiscounts[j]) {
                    equal = false;
                    break;
                }
            }
            if (!equal) break;
        }
        return equal;
    }
    , onTransformToOrderPress: function () {
        
    	var offer = this.order;
        	
     
        var flagData = this.flagsModel.getData();
        if(!flagData.check1 || !flagData.check2)
        {
        	//sap.m.MessageToast.show(model.i18n._getLocaleText("CONFIRM_ORDER_FLAG"));
        	this._openFlagsDialog();
    		return;
        }
        sap.m.MessageBox.confirm(model.i18n.getText("approveChanges"), {
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO]
            , onClose: function (oAction) {
                if (oAction === "YES") {
                    //controllo di essere un agente e lo stato rejected
                    if (this.user.type === "AG" && offer.orderStatus === "REJ") {
                        offer.offerApprovation(this.user, offer).then(_.bind(function () {
                            this.router.navTo("offersList");
                        }, this), _.bind(function (err) {
                            sap.m.MessageToast.show(utils.Message.getError(err));
                        }, this));
                    }
                    else {
                        this.onTransformToOrder(offer).then(_.bind(function () {
                            this.router.navTo("offersList");
                        }, this), _.bind(function (err) {
                            sap.m.MessageToast.show(utils.Message.getError(err));
                        }, this));
                    }
                }
                else {
                    return;
                }
            }.bind(this)
        });
        
    },
    
    _openFlagsDialog : function() {
		if (!this.flagDialog)
			this.flagDialog = sap.ui.xmlfragment(
					"view.dialog.OrderFlagsDialog", this);
		var page = this.getView().byId("offerInfoId");
		this.flagsModel.setData({
			"check1" : false,
			"check2" : false
		});
		page.addDependent(this.flagDialog);
		this.flagDialog.open();
	},
	
	onOkFlagDialogPress : function(evt) {
		
			this.flagDialog.close();
			this.onTransformToOrderPress();

		
	},
	
	onCancelFlagDialogPress : function(evt) {
		this.flagDialog.close();
	},
    //------------------Attachments---------------------------------------------------------------
    
    openUploadFileDialog: function (evt) {
        if (!this.uploadDialog) {
          this.uploadDialog = sap.ui.xmlfragment("view.dialog.uploadFile", this);
          var page = this.getView().byId("offerInfoId");
          page.addDependent(this.uploadDialog);
        }
        this.uploadDialog.open();
        var fileUploader = sap.ui.getCore().byId("fileUploader");
        fileUploader.clear();

        //$(document).keyup(_.bind(this.keyUpFunc, this));

      },

      onCloseUploadDialogPress: function (evt) {
        if (this.uploadDialog) {
          this.uploadDialog.close();
          //this.uploadDialog.destroy();
        }

      },


      //** eventulamente per cancellare i file
      onDeleteFilePress: function (evt) {
        var file = evt.getParameters().listItem;
        var fileCtx = file.getBindingContext("order").getObject();
        this.getView().setBusy(true);
        this.order.deleteAttach(fileCtx.posId)
          .then(_.bind(function (res) {
              this.getView().getModel("order").refresh();
              this.getView().setBusy(false);
            }, this),
            _.bind(function (err) {
              sap.m.MessageToast.show(utils.Message.getError(err));
              this.getView().setBusy(false);
            }, this))
      },
      //**
      handleUploadComplete: function (oEvent) {
        var sResponse = oEvent.getParameter("response");
        var status = oEvent.getParameter("status").toString();
        var dataFile = "";
        if (status && status[0] === "2") {
          if (sResponse) {
            var response = oEvent.getParameter("response");
            var indexIGuid = response.indexOf("IGuid=");
            var indexIGuid2 = response.indexOf(",");
            var IGuid = response.substring(indexIGuid, indexIGuid2).split("'")[1];
            var indexIDrunr = response.indexOf("IDrunr=");
            var indexIDrunr2 = response.indexOf(")");
            var IDrunr = response.substring(indexIDrunr, indexIDrunr2).split("'")[1];
            this.order.guid = IGuid;
            dataFile = {
              name: this.fileName,
              posId: IDrunr,
              type: this.fileType,
              url: model.odata.chiamateOdata._serviceUrl + "SO_TabAttachSet(IGuid='" + IGuid + "',IDrunr='" + IDrunr + "')/$value"
            };
            this.order.attachments.push(dataFile);
          }
          sap.m.MessageToast.show(model.i18n._getLocaleText("UPLOAD_OK"));
        } else {
          dataFile = {};
          sap.m.MessageToast.show(model.i18n._getLocaleText("UPLOAD_KO"));
        }
        this.getView().getModel("order").refresh();
        // this.refreshView(this.order);

      },

      handleUploadPress: function (oEvent) {
        // testModel = new sap.ui.model.json.JSONModel();
        // this.getView().setModel(testModel, "test");

        var uploadData = {};
        var fileUploader = sap.ui.getCore().byId("fileUploader");
        if (!fileUploader.getValue()) {
          sap.m.MessageToast.show(model.i18n._getLocaleText("CHOOSE_FILE_FIRST"));
          return;
        } else {
          fileUploader.removeAllHeaderParameters();
          fileUploader.setSendXHR(true);

          var modello = model.odata.chiamateOdata.getOdataModel();
          modello.refreshSecurityToken();
          var header = new sap.ui.unified.FileUploaderParameter({
            name: "x-csrf-token",
            value: modello.getHeaders()['x-csrf-token']
          });

          fileUploader.addHeaderParameter(header);
          // this.fileName = fileUploader.oFileUpload.files[0].name;
          this.fileType = fileUploader.oFileUpload.files[0].type;

          if (this.order.guid) {
            var guid = "|" + this.order.guid;
            var headerSlug = new sap.ui.unified.FileUploaderParameter({
              name: "slug",
              value: this.fileName + "|" + this.fileType + guid
            });
          } else {
            var headerSlug = new sap.ui.unified.FileUploaderParameter({
              name: "slug",
              value: this.fileName + "|" + this.fileType
            });
          }

          fileUploader.addHeaderParameter(headerSlug);

          fileUploader.upload();
        }

        if (this.uploadDialog) {
          this.uploadDialog.close();
          //this.uploadDialog.destroy();
        }
      },


      //** per gestire i tipi di file
      handleTypeMissmatch: function (oEvent) {
        var aFileTypes = oEvent.getSource().getFileType();
        jQuery.each(aFileTypes, function (key, value) {
          aFileTypes[key] = "*." + value
        });
        var sSupportedFileTypes = aFileTypes.join(", ");
        sap.m.MessageToast.show("The file type *." + oEvent.getParameter("fileType") +
          " is not supported. Choose one of the following types: " +
          sSupportedFileTypes);
      },

      handleValueChange: function (oEvent) {

        //console.log(oEvent.getSource());
        var name = oEvent.getParameter("newValue");
        if (name.length > 50) {
          if (!this.fileNameDialog) {
            this.fileNameDialog = sap.ui.xmlfragment("view.dialog.fileNameDialog", this);
          }
          this.fileModel = new sap.ui.model.json.JSONModel();
          this.fileModel.setData({
            "name": name
          });
          this.getView().setModel(this.fileModel, "fn");
          var page = this.getView().byId("offerInfoId");
          page.addDependent(this.fileNameDialog);
          this.fileNameDialog.open();
        } else {
          this.fileName = name;
          sap.m.MessageToast.show(model.i18n._getLocaleText("PRESS_UPLOAD_FILE") + " " +
            oEvent.getParameter("newValue") + "'");
        }


      },

      onFileNameOK: function (evt) {
        var name = this.getView().getModel("fn").getProperty("/name");
        if (name.length <= 50) {
          this.fileName = name;
          sap.m.MessageToast.show(model.i18n._getLocaleText("PRESS_UPLOAD_FILE") + " " +
            evt.getParameter("newValue") + "'");
          this.fileNameDialog.close();
        } else {
          sap.m.MessageToast.show(model.i18n._getLocaleText("REDEFINE_FILE_NAME"));
        }
      },
      onFileNameClose: function (evt) {
        this.fileNameDialog.close();
        this.uploadDialog.close();
      },
      onPreviewPress: function (evt) {
    	    var src = evt.getSource();
    	    var link = src.getBindingContext("order").getObject().url;
    	    sap.m.URLHelper.redirect(link, true);
    	  },
    /*
	  keyUpFunc: function (e) {
		    if (e.keyCode == 27) {
		      // codice per il pulsante escape per evitare che lo user chiuda il dialog via ESC

		      if (this.uploadDialog) {
		        // this.insertNewAddressDialog.destroy();
		        this.uploadDialog.close();
		      }
		      $(document).off("keyup");
		      //                    this.router.navTo("launchpad");
		    }
		  },
     */
});