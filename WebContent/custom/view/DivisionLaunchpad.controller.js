jQuery.sap.require("model.Tiles");
jQuery.sap.require("model.ui.LogoTile");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.i18n");

//jQuery.sap.require("model.User");

view.abstract.AbstractController.extend("view.DivisionLaunchpad", {

  onInit: function () {
    this.router = sap.ui.core.UIComponent.getRouterFor(this);
    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    // view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.tileModel = new sap.ui.model.json.JSONModel();

    this.getView().setModel(this.tileModel, "tiles");

    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");
    //** risetto il css 
    this.cssReload();
    //**
    

  },

//  onAfterRendering: function(evt){
//    this.user = model.persistence.Storage.session.get("user");
//
//    this.userModel.setData(this.user);
//    this.refreshTiles(this.user);
//
//
//  },

  handleRouteMatched: function (oEvent) {


    var oParameters = oEvent.getParameters();

    if (oParameters.name !== "divisionLaunchpad") {
      return;
    }
      
    this.user = model.persistence.Storage.session.get("user");
    
    if (this.user.organizationData 
       && _.chain(this.user.organizationData.results).pluck('society').uniq().value().length === 1){
        var appModel = this.getView().getModel("appStatus");
          if(appModel.getProperty("/navBackVisible")===true){
              appModel.setProperty("/navBackVisible", false);
          }
    }
      
    
      
    this.selectedCompany = oParameters.arguments.society ? oParameters.arguments.society : model.persistence.Storage.session.get("society") ;
      
//    var appModel = this.getView().getModel("appStatus");
//      if(!sessionStorage.getItem("customerSession")){
//        appModel.setProperty("/navBackVisible", true);
//      }else{
//          appModel.setProperty("/navBackVisible", false);
//      }
      
    
      
    if(!this.selectedCompany){
        this.selectedCompany = this.user.organizationData.results[0].society;
        model.persistence.Storage.session.save("society", this.selectedCompany);
    }
    this.workingUser = model.persistence.Storage.session.get("workingUser");
    if(this.workingUser){
        this.userModel.setData(this.workingUser);
        this.refreshTiles(this.workingUser); 
    }else{
        var companies = _.where(this.user.organizationData.results, {'society': this.selectedCompany});
        var cloneUser = _.cloneDeep(this.user);
        if(cloneUser.organizationData.results.length>0){
                cloneUser.organizationData.results = [];
        }
        for(var i = 0; i<companies.length; i++){
            cloneUser.organizationData.results.push(companies[i]);
        } 
        model.persistence.Storage.session.save("workingUser", cloneUser);
        this.userModel.setData(cloneUser);
        this.refreshTiles(cloneUser);
        
    }
    
    
        
    
     

  },



  onTilePress: function (oEvent) {
    //recupero la tile
    var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
    var url = tilePressed.url;
    model.persistence.Storage.session.save("division", tilePressed.division);
    var division = _.where(this.user.organizationData.results, {'division': tilePressed.division});
    this.workingUser = model.persistence.Storage.session.get("workingUser");
    var cloneUser = _.cloneDeep(this.workingUser);
    if(cloneUser.organizationData.results.length>0){
            cloneUser.organizationData.results = [];
    }
    for(var i = 0; i<division.length; i++){
        cloneUser.organizationData.results.push(division[i]);
    } 
    model.persistence.Storage.session.save("workingUser", cloneUser);

    jQuery.sap.includeStyleSheet("custom/css/custom"+tilePressed.society+tilePressed.division+".css","custom_style");

    //lancio l'app
    this.launchApp(url, tilePressed.division);
  },

  onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    },

  launchApp: function (url, division) {
      if(url === "launchpad")
    this.router.navTo(url, {division: division});
    
  },
    
  navBack: function(evt){
      //this.router.myNavBack();
      
      var society = model.persistence.Storage.session.get("society");


//    $(document).off("keyup");
    if (this.user.organizationData 
       && _.chain(this.user.organizationData.results).pluck('society').uniq().value().length > 1){
        this.router.navTo("soLaunchpad", {society:society});
    }
    if(model.persistence.Storage.session.get("workingUser"))
      model.persistence.Storage.session.remove("workingUser");
   
  },

  //**
  refreshTiles: function(user)
  {
    var companies = _.chain(user.organizationData.results).pluck('division').uniq().value();
    var tiles = [];
    for(var i = 0; i<companies.length; i++)
    {
      tiles.push(model.Tiles.getDivisionTile(this.selectedCompany, companies[i]));
    }
    this.tileModel.setData(tiles);
    // var session = sessionStorage.getItem("customerSession") ? model.persistence.Storage.session.get("customerSession") : false;
    // var tile = model.Tiles.getMenu(user.type, session);
    // this.tileModel.setData(tile);
    // this.tileModel.refresh();
  }
});


