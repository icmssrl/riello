jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Orders");
jQuery.sap.require("model.collections.TestOrders");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("utils.Busy");
// jQuery.sap.require("sap.m.URLHelper");
jQuery.sap.require("view.abstract.AbstractMasterController");

view.abstract.AbstractMasterController.extend("view.order.OrderInfo", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    //this.notesModel = new sap.ui.model.json.JSONModel();
    //this.notesModel.setData({"billsItems":[], "salesItems":[]});
    //this.getView().setModel(this.notesModel,"notes");

    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");

    var oModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(oModel);

    this.orderModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.orderModel, "order");

    this.gruModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.gruModel, "gru");
    
    // this.positionModel = new sap.ui.model.json.JSONModel();
    // this.getView().setModel(this.positionModel, "position");

  },


  handleRouteMatched: function (evt) {

    
    var name = evt.getParameter("name");

    var id = evt.getParameters().arguments.id;
    

    if (name !== "orderInfo") {
      return;
    }
    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    this.user = model.persistence.Storage.session.get("user");
    this.userModel.setData(this.user);
    
    //**
    this.uiModel.setProperty("/orderInfo",true);
    this.uiModel.setProperty("/orderCreate",false);
    this.uiModel.setProperty("/componentsVisibility", false);
    this.uiModel.setProperty("/showCommission", this.checkQuotationEnabledToTradePolicies());
    this.uiModel.refresh();
    //**
    
    this.order = new model.Order();
    utils.Busy.show();
    this.order.load(id)
      //  model.collections.TestOrders.loadTestOrdersById(id) //forceReload
      .then(_.bind(function(res){
            //console.log(res);
            utils.Busy.hide();
            this.order = res;
            var reqDt = (model.persistence.Storage.session.get("selectedOrder")).validDateList;
            this.order.requestedDate = new Date(reqDt);
            var shippDt = (model.persistence.Storage.session.get("selectedOrder")).shippmentDate;	
            this.order.shippmentDate = new Date(shippDt);
           
            this.orderModel.setData(this.order);
            this.orderModel.refresh(true);
            this.gruModel.setData(this.order.gru);
            this.populateNotesText();
            // this.positionModel.setData(this.order.positions);
            // this.positionModel.refresh(true);
            //var oTable = this.getView().byId("orderListTable");

          }, this), _.bind(function(err){utils.Busy.hide();}, this));

    //    this.getView().getModel("notes").refresh();

//    this.customer = model.Current.getCustomer();
//    if (!this.customer) {
//      var customerData = model.persistence.Storage.session.get("currentCustomer");
//      this.customer = new model.Customer(customerData);
//    }
//    this.getView().setModel(this.customer.getModel(), "customer");
//
//    this.order = model.Current.getOrder();
//
//    if (!this.order) {
//      this.order = new model.Order();
//      this.order.create(this.customer.registry.id)
//        .then(_.bind(function () {
//          this.orderModel = this.order.getModel();
//          this.getView().setModel(this.orderModel, "o");
//          var destinations = {
//            "items": []
//          };
//          destinations.items = this.customer.destinations;
//          this.destinationsModel = new sap.ui.model.json.JSONModel(destinations);
//          this.getView().setModel(this.destinationsModel, "d");
//        }, this));
//
//    }
//
//
//
//
//
//
//    this.populateSelect();
//    // this.getView().byId("regAlrtBtn").setVisible(false);
//    // this.getView().byId("ctAlrtBtn").setVisible(false);
//    // this.getView().byId("bkAlrtBtn").setVisible(false);
//    // this.getView().byId("slsAlrtBtn").setVisible(false);


  },

  populateNotesText: function () {
	    
	    
	    var item= {
	    	  "type":"Tdid",
	    	  "namespace":"notes"
	      };
	     
	      
	     

	    var workingUser = model.persistence.Storage.session.get("workingUser");
	    this.orgData = {
	      "Bukrs": workingUser.organizationData.results[0].society,
	      "Vkorg": workingUser.organizationData.results[0].salesOrg,
	      "Vtweg": workingUser.organizationData.results[0].distributionChannel,
	      "Spart": workingUser.organizationData.results[0].division
	    };
	    
	    this.getView().setBusy(true);
	    
	      utils.Collections.getOdataSelect(item.type, this.orgData)
	        .then(_.bind(function (result) {
	        	//Starting from Here?
	        	var notes = result.getData().results;
	     		var billNote = _.find(notes, {Tdid:"Z002"});
	     		var salesNote = _.find(notes, {Tdid:"Z001"});
	     		var creditNote = _.find(notes, {Tdid:"Z013"});
	     		
	     		this.order.billNoteTxt=billNote? billNote.Tdtext:"";
	     		this.order.salesNoteTxt=salesNote ? salesNote.Tdtext:"";
	     		this.order.creditNoteTxt=creditNote ? creditNote.Tdtext:"";
	     		this.getView().getModel("order").refresh();
	     		this.getView().setBusy(false);

	          
	         
	        }, this));
	   


	  },
  onDiscountPress: function (evt) {
    var src = evt.getSource();
    var position = src.getBindingContext("order").getObject();
    var discount = position.getDiscount();
    //var discount = position.discount;
    var discountModel = discount.getModel_new();
    //discountModel.setData(discount);
//    for (var prop in discount) {
//        if (_.isObject(this[prop])) {
//          _.merge(discountModel.getData()[prop], discount[prop]);
//        } else {
//          discountModel.getData()[prop] = discount[prop];
//        }
//      }


    var page = this.getView().byId("orderInfoId");
    this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialogRO", this);
    this.discountDialog.setModel(discountModel, "d");
    discountModel.setProperty("/en", {"wr": false});
    page.addDependent(this.discountDialog);

    this.discountDialog.open();
  },
  onDiscountDialogClose: function (evt) {
    this.discountDialog.close();

  },
  // onDiscountDialogOK: function (evt) {
  //
  //   //console.log(this.order);
  //   this.discountDialog.close();
  //
  // },
  // refreshDiscount: function (evt) {
  //   var discount = this.discountDialog.getModel("d").getData().ref;
  //   this.discountDialog.setModel(discount.refreshModel(), "d");
  // },
  toOrderListPress:function(evt){
        this.router.navTo("orderList");

             
//        var documentType = model.persistence.Storage.session.get("documentType");
//		if (documentType === "H") {
//        this.router.navTo("returnList");
//      } else {
//        this.router.navTo("orderList");
//      };

  },
  // onLinkPress:function(evt)
  // {
  //   // sap.m.MessageToast.show("Functionality not implemented yet");
  //   var src = evt.getSource();
  //   var url = icms.Component.getMetadata().getConfig().settings.serverUrl;
  //   var attachId = evt.getSource().getBindingContext("order").getObject().attachId;
  //   url += "SO_ContentAttachSet(IObjId='"+attachId+"')/$value";
  //   sap.m.URLHelper.redirect(url, true);
  // },


//
//  refreshView: function (data) {
//    this.getView().setModel(data.getModel(), "o");
//    // var masterCntrl = this.getView().getModel("appStatus").getProperty("/masterCntrl");
//    // masterCntrl.refreshList();
//  },
//
//  //Feed
//  onPost: function (oEvent) {
//
//    var id = oEvent.getSource().getId();
//    var noteType = id.substr(id.lastIndexOf("-") + 1, id.length);
//
//
//    // create new entry
//    var sValue = oEvent.getParameter("value");
//    var oEntry = {
//      author: model.persistence.Storage.session.get("user").fullName,
//      text: sValue
//    };
//
//    this.order.addNote(noteType, oEntry);
//
//    // update model
//    var oModel = this.order.getModel();
//    this.getView().getModel("o").refresh();
//
//  },
//
//
//
//  onSavePress: function () {
//
//    var saveButton = this.getView().byId("saveButton");
//    saveButton.setEnabled(false);
//
//    model.Current.setOrder(this.order);
//    model.persistence.Storage.session.save("currentOrder", this.order);
//
//
//  },
//  onResetPress: function () {
//    this.order = new model.Order();
//    this.getView().getModel("o").refresh();
//  },
//

//
//  onAddProductsPress: function (evt) {
//    if (this.order) {
//      model.Current.setOrder(this.order);
//      model.persistence.Storage.session.save("currentOrder", this.order);
//    }
//    this.router.navTo("empty");
//  },
//
//  //funzioni per allegare file
//
//
 formatAttribute: function (sValue) {
   jQuery.sap.require("sap.ui.core.format.FileSizeFormat");
   if (jQuery.isNumeric(sValue)) {
     return sap.ui.core.format.FileSizeFormat.getInstance({
       binaryFilesize: false,
       maxFractionDigits: 1,
       maxIntegerDigits: 3
     }).format(sValue);
   } else {
     return sValue;
   }
 },
  
  onPressDeliveryType: function()
  {
    
    if(sap.ui.getCore().byId("dialogCamionGru"))
        sap.ui.getCore().byId("dialogCamionGru").destroy(true);
    
    
    
      this.camionGru = sap.ui.xmlfragment("view.dialog.camionGru", this);
      var page = this.getView().byId("orderInfoId");
      page.addDependent(this.camionGru);
    
      this.camionGru.open();
  },
  
  onOkPress: function(evt)
  {
    if(this.camionGru)
    {
      this.camionGru.close();
    }
  },
  
  onKitPress : function(evt)
  {
	  var position = evt.getSource().getBindingContext("order").getObject();
	  var src = evt.getSource();
		if (! this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("view.dialog.componentsPopover", this);

				this.getView().addDependent(this._oPopover);
				// this._oPopover.bindElement("/ProductCollection/0");
		}
		var req = this.getOrgDataForSAP();
		this.getView().setBusy(true);
		position.getComponents(req)
		.then(_.bind(function(result){
			 this.kitModel = new sap.ui.model.json.JSONModel();
		    this.kitModel.setData(position);
		    this.getView().setModel(this.kitModel, "p");

			// delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
			
		    this.getView().setBusy(false);
			jQuery.sap.delayedCall(0, this, function () {
				this._oPopover.openBy(src);
			});
		}, this), 
		_.bind(function(err){
			this.getView().setBusy(false);
			sap.m.MessageToast.show(utils.Message.getError(err));
		}, this))
	   
		
  }
  




});
