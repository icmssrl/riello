jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Orders");
jQuery.sap.require("model.collections.TestOrders");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Product");
jQuery.sap.require("view.abstract.AbstractMasterController");
view.abstract.AbstractMasterController.extend("view.historyCart.CartInfo", {
    onExit: function () {}
    , onInit: function () {
        // this.router = sap.ui.core.UIComponent.getRouterFor(this);
        // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        //this.notesModel = new sap.ui.model.json.JSONModel();
        //this.notesModel.setData({"billsItems":[], "salesItems":[]});
        //this.getView().setModel(this.notesModel,"notes");
        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");
        var oModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(oModel);
        this.orderModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.orderModel, "order");
        this.gruModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.gruModel, "gru");
        // this.positionModel = new sap.ui.model.json.JSONModel();
        // this.getView().setModel(this.positionModel, "position");
    }
    , handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");
        var id = evt.getParameters().arguments.id;
        if (name !== "historyCartsDetail") {
            return;
        }
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        this.user = model.persistence.Storage.session.get("workingUser");
        this.userModel.setData(this.user);
        this.uiModel.setProperty("/orderInfo", true);
        this.uiModel.setProperty("/orderCreate", false);
        this.uiModel.setProperty("/componentsVisibility", false);
        this.order = new model.Order();
        utils.Busy.show();
        this.order.loadCart(id)
            //  model.collections.TestOrders.loadTestOrdersById(id) //forceReload
            .then(_.bind(function (res) {
                //console.log(res);
                utils.Busy.hide();
                this.order = res;
                this.orderModel.setData(this.order);
                this.orderModel.refresh(true);
                this.gruModel.setData(this.order.gru);
                // this.positionModel.setData(this.order.positions);
                // this.positionModel.refresh(true);
                //var oTable = this.getView().byId("orderListTable");
                //se l'ordine non è del customer che ho in sessione cliente non permetto l'attivazione del carrello
                if (this.order.customerId !== model.Current.getCustomer().registry.id) {
                    this.uiModel.setProperty("/wrOrd", false);
                    this.uiModel.refresh();
                }
                else {
                    this.uiModel.setProperty("/wrOrd", true);
                    this.uiModel.refresh();
                }
            }, this));
        //    this.getView().getModel("notes").refresh();
        //    this.customer = model.Current.getCustomer();
        //    if (!this.customer) {
        //      var customerData = model.persistence.Storage.session.get("currentCustomer");
        //      this.customer = new model.Customer(customerData);
        //    }
        //    this.getView().setModel(this.customer.getModel(), "customer");
        //
        //    this.order = model.Current.getOrder();
        //
        //    if (!this.order) {
        //      this.order = new model.Order();
        //      this.order.create(this.customer.registry.id)
        //        .then(_.bind(function () {
        //          this.orderModel = this.order.getModel();
        //          this.getView().setModel(this.orderModel, "o");
        //          var destinations = {
        //            "items": []
        //          };
        //          destinations.items = this.customer.destinations;
        //          this.destinationsModel = new sap.ui.model.json.JSONModel(destinations);
        //          this.getView().setModel(this.destinationsModel, "d");
        //        }, this));
        //
        //    }
        //
        //
        //
        //
        //
        //
        //    this.populateSelect();
        //    // this.getView().byId("regAlrtBtn").setVisible(false);
        //    // this.getView().byId("ctAlrtBtn").setVisible(false);
        //    // this.getView().byId("bkAlrtBtn").setVisible(false);
        //    // this.getView().byId("slsAlrtBtn").setVisible(false);
    }
    , onDiscountPress: function (evt) {
        var src = evt.getSource();
        var position = src.getBindingContext("order").getObject();
        var discount = position.getDiscount();
        //var discount = position.discount;
        var discountModel = discount.getModel_new();
        //discountModel.setData(discount);
        //    for (var prop in discount) {
        //        if (_.isObject(this[prop])) {
        //          _.merge(discountModel.getData()[prop], discount[prop]);
        //        } else {
        //          discountModel.getData()[prop] = discount[prop];
        //        }
        //      }
        var page = this.getView().byId("orderInfoId");
        this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialogRO", this);
        this.discountDialog.setModel(discountModel, "d");
        discountModel.setProperty("/en", {
            "wr": false
        });
        page.addDependent(this.discountDialog);
        this.discountDialog.open();
    }
    , onDiscountDialogClose: function (evt) {
        this.discountDialog.close();
    }
    , // onDiscountDialogOK: function (evt) {
    //
    //   //console.log(this.order);
    //   this.discountDialog.close();
    //
    // },
    refreshDiscount: function (evt) {
        var discount = this.discountDialog.getModel("d").getData().ref;
        this.discountDialog.setModel(discount.refreshModel(), "d");
    }
    , toOrderListPress: function (evt) {
        this.router.navTo("orderList");
    }
    , formatAttribute: function (sValue) {
        jQuery.sap.require("sap.ui.core.format.FileSizeFormat");
        if (jQuery.isNumeric(sValue)) {
            return sap.ui.core.format.FileSizeFormat.getInstance({
                binaryFilesize: false
                , maxFractionDigits: 1
                , maxIntegerDigits: 3
            }).format(sValue);
        }
        else {
            return sValue;
        }
    }
    , activateHistoryCart: function (evt) {
        // var order = new model.Order();
        // order.create(this.customer);
        // for(var i = 0; i< this.cart.positions.length; i++)
        // {
        // 	order.addPosition(this.cart.positions[i]);
        // }
        // order.setPositions(this.cart.getPositions());
        this.order.orderId = "";
        var pos = this.order.positions;
        for (var i = 0; i < pos.length; i++) {
            pos[i] = new model.Position(pos[i]);
            pos[i].positionId = i + 1; // changed, it was ""
            pos[i].product = new model.Product();
            pos[i].product.init(pos[i].productId, pos[i].description, pos[i].scale, pos[i].unitListPrice, pos[i].currency, pos[i].isKit);
            //---------------New 22/06/16-------------------------
            pos[i].discount = null;
            pos[i].orderDiscounts = [];
            pos[i].wantedDate = new Date();
            pos[i].availableDate = null;
            //----------------------------------------------------
        }
        this.order.billsNotes = [];
        this.order.salesNotes = [];
        this.order.requestedDate = new Date();
        this.order.validDateList = new Date();
        this.order.fullEvasionAvailableDate = null;
        model.Current.setOrder(this.order);
        // if(this.cart.customerId || this.cart.customerName){
        //     var cloneOrder = jQuery.extend(true, {} , this.cart);
        //     cloneOrder.customerId = this.customer.registry.id;
        //     cloneOrder.customerName = this.customer.registry.customerName;
        //     if(!!cloneOrder.fullEvasionAvailableDate){
        //         delete cloneOrder.fullEvasionAvailableDate;
        //     }
        //     var orderModel = new model.Order();
        //     var positionsArr = [];
        //
        //     var product = new model.Product();
        //     orderModel.update(cloneOrder);
        //     if(cloneOrder.positions || cloneOrder.positions.length >= 0)
        //         {
        //             delete orderModel.positions;
        //
        //             for(var i = 0; i< cloneOrder.positions.length; i++){
        //                 var position = new model.Position();
        //                 position.update(cloneOrder.positions[i]);
        //                 if(position.product){
        //
        //                     delete position.product;
        //                     product.update(cloneOrder.positions[i].product);
        //                     position.setProduct(product);
        //                 }
        //
        //                 var discount = new model.Discount();
        //                 discount.initialize(orderModel.getId(), position);
        //                 position.setDiscount(discount);
        //                 positionsArr.push(position);
        //
        //
        //             }
        //             orderModel.setPositions(positionsArr);
        //             model.Current.setOrder(orderModel);
        //             model.persistence.Storage.session.save("currentOrder", orderModel);
        //         }
        //
        //
        // }
        (this.checkQuotationEnabledToTradePolicies()) ? this.router.navTo("newOffer"): this.router.navTo("newOrder");
    }
    , onPressDeliveryType: function (evt) {
        if (sap.ui.getCore().byId("dialogCamionGru")) sap.ui.getCore().byId("dialogCamionGru").destroy(true);
        this.camionGru = sap.ui.xmlfragment("view.dialog.camionGru", this);
        var page = this.getView().byId("orderInfoId");
        page.addDependent(this.camionGru);
        this.camionGru.open();
    }
    , onOkPress: function (evt) {
        if (this.camionGru) {
            this.camionGru.close();
        }
    }
    , onKitPress: function (evt) {
        var position = evt.getSource().getBindingContext("order").getObject();
        var src = evt.getSource();
        if (!this._oPopover) {
            this._oPopover = sap.ui.xmlfragment("view.dialog.componentsPopover", this);
            this.getView().addDependent(this._oPopover);
            // this._oPopover.bindElement("/ProductCollection/0");
        }
        var req = this.getOrgDataForSAP();
        this.getView().setBusy(true);
        position.getComponents(req).then(_.bind(function (result) {
            this.kitModel = new sap.ui.model.json.JSONModel();
            this.kitModel.setData(position);
            this.getView().setModel(this.kitModel, "p");
            this.getView().setBusy(false);
            // delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
            jQuery.sap.delayedCall(0, this, function () {
                this._oPopover.openBy(src);
            });
        }, this), _.bind(function (err) {
            this.getView().setBusy(false);
            sap.m.MessageToast.show(utils.Message.getError(err));
        }, this))
    }
});