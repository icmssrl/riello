jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.HistoryCarts");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");

view.abstract.AbstractMasterController.extend("view.historyCart.HistoryCartsList", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
		this.uiModel.setProperty("/searchProperty", ["orderId", "customerName"]);
	},


	handleRouteMatched: function (evt) {

		var name = evt.getParameter("name");

//    if(sap.ui.Device.system.tablet && sap.ui.Device.orientation.portrait)
//    {
//      sap.ui.getCore().byId("splitApp-Master").setVisible(true);
//    }

		if (name !== "historyCartsList" && name !== "historyCartsDetail" && name != "emptyHistoryCarts") {
            return;
        }


//		this.user = model.persistence.Storage.session.get("user");
		this.user = model.persistence.Storage.session.get("workingUser");
		this.customer = model.persistence.Storage.session.get("currentCustomer");
		this.division = model.persistence.Storage.session.get("division");
		if(!this.division)
		{
			this.division = _.chain(this.user.organizationData).pluck('division').uniq().value()[0];
		}
//		model.collections.Customers.loadCustomers(this.user.username, this.division)//forceReload
//
//		.then(_.bind(this.refreshList, this));

        // model.collections.HistoryCarts.loadHistoryCarts(true);
				model.collections.HistoryCarts.loadCarts(this.user, this.customer)
				.then(_.bind(function(res){
					this.refreshList();
				}, this))






//        this.test = model.collections.HistoryCarts.loadHistoryCarts();
//        this.carts = model.persistence.Storage.local.get("cartsHistory");
//        this.cartsListModel = new sap.ui.model.json.JSONModel(this.carts);
//        this.getView().setModel(this.cartsListModel, "carts");
//        sap.ui.getCore().setModel(this.cartsListModel, "cartsList");
//





	},

	onItemPress : function(evt)
  {
    var src = evt.getSource();
    var selectedItem = src.getBindingContext("globalCartList").getObject();
		this.getView().getModel("appStatus").setProperty("/currentCart", selectedItem);
		// this.getView().getModel("appStatus").setProperty("/masterCntrl", this.getView().getController());
		this.router.navTo("historyCartsDetail",  {id : selectedItem.orderId});


  },

refreshList : function(evt)
	{

		var filters = this.getFiltersOnList();


        this.cartsListModel = model.collections.HistoryCarts.getModel();

        sap.ui.getCore().setModel(this.cartsListModel, "globalCartList");
        this.getView().setModel(this.cartsListModel, "globalCartList");

        var carts = this.cartsListModel.getData();

        // this.filterCartModel = new sap.ui.model.json.JSONModel();
        // var resetPos = jQuery.extend(true, {} , carts);
        //
        // for(var i = 0; i<resetPos.historyCarts.length; i++){
        //     if(resetPos.historyCarts[i].positions && resetPos.historyCarts[i].positions.length > 0){
        //         resetPos.historyCarts[i].positions = [];
        //     }
        // }
        // this.filterCartModel.setData(resetPos);


		if(filters)
			this.getView().byId("list").getBinding("items").filter(filters);

	},
	onFilterPress:function()
	{
		this.filterModel = model.filters.Filter.getModel(this.cartsListModel.getData().results, "cartsList");
		this.getView().setModel(this.filterModel, "filter");
		var page = this.getView().byId("historyCartsListPageId");
		this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
		page.addDependent(this.filterDialog);
		this.filterDialog.open();

	},
	onFilterDialogClose:function()
	{
		this.filterDialog.close();
	},

	onFilterPropertyPress:function(evt)
	{

		var parentPage = sap.ui.getCore().byId("parent");
		var elementPage = sap.ui.getCore().byId("children");
		//console.log(this.getView().getModel("filter").getData().toString());
		var navCon = sap.ui.getCore().byId("navCon");
		var selectedProp = 	evt.getSource().getBindingContext("filter").getObject();
		this.getView().getModel("filter").setProperty("/selected", selectedProp);
		this.elementListFragment = sap.ui.xmlfragment("view.fragment.filterList", this);
		elementPage.addContent(this.elementListFragment);

		navCon.to(elementPage, "slide");
		this.getView().getModel("filter").refresh();
	},

	onBackFilterPress:function(evt)
	{
		// this.addSelectedFilterItem();
		this.navConBack();
		this.getView().getModel("filter").setProperty("/selected", "");
		this.elementListFragment.destroy();
	},
	navConBack:function()
	{
		var navCon = sap.ui.getCore().byId("navCon");
		navCon.to(sap.ui.getCore().byId("parent"), "slide");
		this.elementListFragment.destroy();
	},
	afterOpenFilter:function(evt)
	{
		var navCon = sap.ui.getCore().byId("navCon");
		if(navCon.getCurrentPage().getId()== "children")
			navCon.to(sap.ui.getCore().byId("parent"), "slide");
		this.getView().getModel("filter").setProperty("/selected", "");
	},

	onSearchFilter:function(oEvt)
	{
			var aFilters = [];
			var sQuery = oEvt.getSource().getValue();

			if (sQuery && sQuery.length > 0) {

					// var filter = new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.Contains, sQuery);

				// 	var filter = new sap.ui.model.Filter({path:"value", test:function(val)
				// {
				// 	var property= val.toString().toUpperCase();
				// 	return (property.indexOf(sQuery.toString().toUpperCase())>=0)
				// }});

					aFilters.push(this.createFilter(sQuery, "value"));
				}

			// update list binding
			var list = sap.ui.getCore().byId("filterList");
			var binding = list.getBinding("items");
			binding.filter(aFilters);
	},
	createFilter:function(query, property)
	{
		var filter = new sap.ui.model.Filter({path:property, test:function(val)
		{
			var prop= val.toString().toUpperCase();
			return (prop.indexOf(query.toString().toUpperCase())>=0)
		}});
		return filter;
	},
	onFilterDialogClose:function(evt)
	{
		if (this.elementListFragment) {this.elementListFragment.destroy();}
		if (this.filterDialog) {
			this.filterDialog.close();
			this.filterDialog.destroy();
		}
	},
	onFilterDialogOK:function(evt)
	{
		var filterItems = model.filters.Filter.getSelectedItems("cartsList");
		if(this.elementListFragment)
			this.elementListFragment.destroy();
		this.filterDialog.close();
		this.getView().getModel("filter").setProperty("/selected", "");
		this.handleFilterConfirm(filterItems);
		this.filterDialog.destroy();
		delete(this.filterDialog);
	},
	handleFilterConfirm: function(selectedItems)
	{
		var filters = [];
		_.forEach(selectedItems, _.bind(function(item)
	{
		filters.push(this.createFilter(item.value, item.property));
	},
	this));
		var list = this.getView().byId("list");
		var binding = list.getBinding("items");
		binding.filter(filters);
	},
	onResetFilterPress: function()
	{
		model.filters.Filter.resetFilter("cartsList");
		// //console.log(model.filters.Filter.getSelectedItems("customers"));
        var list = this.getView().byId("list");
        var binding = list.getBinding("items");
        binding.filter();
        if (this.elementListFragment) {
          this.elementListFragment.destroy();
        }
        if (this.filterDialog) {
          this.filterDialog.close();
          this.filterDialog.destroy();
        }
	},

    onShowOnlyFavoritesPress: function(evt){
        var src = evt.getSource();
        var text = src.getProperty("text");

        // var list = this.getView().byId("list");
        // var binding = list.getBinding("items");
        // if(text === model.i18n.getText("SHOW_ONLY_FAVORITES")){
        //     var filters = new sap.ui.model.Filter({ path: "favorite", operator: sap.ui.model.FilterOperator.EQ, value1: true})
				//
				//
        //     binding.filter(filters);
//            var findFavorite = _.where(list.getBinding("items").getModel().getData().historyCarts, {"favorite":true});
//            this.getView().getModel("globalCartList").setProperty("/historyCarts",findFavorite);
				var pressed = (text === model.i18n.getText("SHOW_ONLY_FAVORITES"));//src.getPressed();
				model.collections.HistoryCarts.loadCarts(this.user, pressed)
				.then(_.bind(function(result)
			{
				this.refreshList();
				var text = pressed ? model.i18n.getText("SHOW_ALL_ITEMS") : model.i18n.getText("SHOW_ONLY_FAVORITES");
				src.setText(text);
			}, this))

				// if(src.getPressed())
				// {
				//
				// 	src.setText(model.i18n.getText("SHOW_ALL_ITEMS"));
				// }
        // else{
        //     // binding.filter();
        //     src.setText(model.i18n.getText("SHOW_ONLY_FAVORITES"));
        // }
    }








});
