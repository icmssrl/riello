jQuery.sap.require("sap.m.MessageBox");
//jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.CustomerRequester");
jQuery.sap.require("utils.Message");

view.abstract.AbstractMasterController.extend("view.customer.CustomersList", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
		
		this.eventBus = sap.ui.getCore().getEventBus();
		this.eventBus.subscribe("discountChannel", "discountUpdated", this.updateList, this);
		this.uiModel.setProperty("/last", false);
		this.initializeList();
	},


	handleRouteMatched: function (evt) {

		this.uiModel.setProperty("/searchValue", "");


		var name = evt.getParameter("name");

//    if(sap.ui.Device.system.tablet && sap.ui.Device.orientation.portrait)
//    {
//      sap.ui.getCore().byId("splitApp-Master").setVisible(true);
//    }

		// if ( name !== "customersList" && name !== "customerDetail" && name != "customerEdit" && name!=="emptyCustomer"){
			if ( name !== "customersList" && name !=="emptyCustomer"){
			return;
		}

		//YET TO ADAPT TO LISTED ELEMENTS ->IN refreshList
		if(model.persistence.Storage.session.get("workingUser"))
	    {
	    	 var wrkUsr = model.persistence.Storage.session.get("workingUser");
	    	 this.getView().getModel("wo").setData(wrkUsr);
	    }
		this.initializeList();





	},

	initializeList:function()
	{
		this.user = model.persistence.Storage.session.get("user");
		// this.division = model.persistence.Storage.session.get("division");
		// if(!this.division)
		// {
		// 	this.division = _.chain(this.user.organizationData).pluck('division').uniq().value()[0];
		// }
		var userInfo = this.getUserInfo();//Temporary Function to get Only necessary properties

		this.customerRequester = new model.CustomerRequester(userInfo);
		this.paths=[];
		this.refreshCrumbToolbar();
		// this.pathModel = new sap.ui.model.json.JSONModel({"paths": this.paths});
		// this.getView().setModel(this.pathModel, "p");

		if(this.user.type !== "AG")
		{
			this.addCrumbPath(this.user.userGeneralData.username, this.user.type);
		}
		this.updateList();



		// model.collections.Customers.loadOdataCustomers(this.user)//forceReload
		//
		// .then(_.bind(this.refreshList, this));

	},

	addCrumbPath : function(val, type)
	{
		this.paths.push({"value":val, "type": type});
		this.refreshCrumbToolbar();
		// var item = sap.ui.xmlfragment("view.list.breadCrumbList", this);
		// var toolbar= this.getView().byId("crumbPaths");
		// this.pathModel.setProperty("/paths", this.paths);
		// this.pathModel.refresh();
		// list.bindAggregation("items", {path:"p>/paths", template:item});
		// var toolbar = this.getView().byId("crumbPaths");
		// toolbar.addContent(new sap.m.Link({text:val, id:type, class:"hierachyLink", press : [this.handleLinkPress, this]}));
	},
	removeCrumbPath:function(target)
	{
		var index = -1;
		for(var i = 0; i< this.paths.length; i++)
		{
			if(this.paths[i].type === target)
			{
				index=i+1;
				break;
			}
		}
		if(index < this.paths.length)
		{
			for(var i = this.paths.length-1; i>index-1; i--)
			{
				if(this.paths[i].type === "AG")
				{
					this.customerRequester.removeAgent();
				}
				else if(this.paths[i].type === "TM")
				{
					this.customerRequester.removeTerritoryManager();
				}
			}
			this.paths = this.paths.slice(0, index);
			this.refreshCrumbToolbar();
			this.updateList();
		}



	},
	refreshCrumbToolbar: function()
	{
		var toolbar= this.getView().byId("crumbPaths");
		toolbar.destroyContent();
		for(var i = 0; i< this.paths.length; i++)
		{
			// toolbar.addContent(new sap.m.Link({text:this.paths[i].value, target:this.paths[i].type, class:"hierachyLink", press : [this.handleLinkPress, this]}));
			toolbar.addContent(new sap.m.Link({text:this.paths[i].value, target:this.paths[i].type, press : [this.handleLinkPress, this]}).addStyleClass("hierarchyLink"));
		}
	},
	handleLinkPress : function(evt)
	{
		var src = evt.getSource();
		var target = src.getTarget();
		this.removeCrumbPath(target);

	},
	updateList: function()
	{
		this.getView().setBusy(true);
		this.customerRequester.loadCustomersList()
			.then(_.bind(this.createList, this), 
					_.bind(function(err){
						sap.m.MessageToast.show(utils.Message.getError(err));
						this.getView().setBusy(false);
						}, this));
	},

	getUserInfo : function()
	{
		var selectedOrg = model.persistence.Storage.session.get("workingUser");
		var u = {
			"username" : selectedOrg.organizationData.results[0].username,
			"userType" :  selectedOrg.type,
			"society" : selectedOrg.organizationData.results[0].society,
			"salesOrg" : selectedOrg.organizationData.results[0].salesOrg,
			"distrCh": selectedOrg.organizationData.results[0].distributionChannel,
			"division": selectedOrg.organizationData.results[0].division,
			"areaManager": selectedOrg.organizationData.results[0].areaManager,
			"territoryManager" : selectedOrg.organizationData.results[0].territoryManager,
			"agentCode" : selectedOrg.userGeneralData.customer
		}
		return u;
	},
	onItemPress : function(evt)
  {
    //pulisco la searchBar
    this.getView().byId("searchBar").setValue("");
    //**
    var src = evt.getSource();
    var selectedItem = src.getBindingContext("c").getObject();
		if(!this.customerRequester.hasTerritoryManager())
		{
			this.addCrumbPath(selectedItem.registry.territoryManagerText, "TM");
			this.customerRequester.setTerritoryManager(selectedItem.registry.territoryManager);
			this.updateList();
			// this.paths.push(selectedItem.registry.territoryManager);
		}
		else if(!this.customerRequester.hasAgent())
		{
			this.addCrumbPath(selectedItem.registry.agentName, "AG");
			this.customerRequester.setAgent(selectedItem.registry.agentCode);
			this.updateList();
			// this.paths.push(selectedItem.registry.agentCode);
		}
		else {
			// this.getView().getModel("appStatus").setProperty("/currentClient", selectedItem);
			this.getView().getModel("appStatus").setProperty("/customerRequester", this.customerRequester.getParameters());
			model.persistence.Storage.session.save("customerRequester", this.customerRequester.getParameters());
			// this.getView().getModel("appStatus").setProperty("/masterCntrl", this.getView().getController());
			this.router.navTo("customerDetail",  {id : selectedItem.getId()});
		}



  },
	onResetListPress : function(evt)
	{
		if(evt.getParameter("refreshButtonPressed"))
		{
			this.removeFiltersOnList();
			if(!this.customerRequester)
			{
				this.customerRequester = new model.customerRequester(model.persistence.Storage.session.get("customerRequester"));
			}
			this.updateList();
		}

	},
	refreshList : function(evt)
	{
		var filters = this.getFiltersOnList();
		this.customersModel = model.collections.Customers.getModel();


		this.getView().setModel(this.customersModel, "c");


		if(filters)
			this.getView().byId("list").getBinding("items").filter(filters);

	},
	createList : function(evt)
	{
		this.getView().setBusy(false);
		this.customersModel = model.collections.Customers.getModel();
		var list = this.getView().byId("list");

		this.getView().setModel(this.customersModel, "c");

		var item = sap.ui.xmlfragment("view.list.customerList", this);
		//We must create dinamically the list because the property binding will change
		if(this.customerRequester.hasAgent()) //if requester is an agent
		{
			//Enable Filter and direct Call -->How?
			this.uiModel.setProperty("/last", true);
			this.uiModel.setProperty("/searchProperty", ["registry/customerName", "registry/id"]);
			item = sap.ui.xmlfragment("view.list.customerList", this);


		}
		else if(this.customerRequester.hasTerritoryManager()) // if it's a TM
		{
			this.uiModel.setProperty("/last", false);
			this.uiModel.setProperty("/searchProperty", ["registry/agentCode", "registry/agentName"]);
			item = sap.ui.xmlfragment("view.list.agentList", this);
		}
		else
		{ //if it's an AM
			this.uiModel.setProperty("/last", false);
			this.uiModel.setProperty("/searchProperty", ["registry/territoryManager", "registry/territoryManagerText"]);
			item = sap.ui.xmlfragment("view.list.territoryManagerList", this);
		}
		list.bindAggregation("items", {path: "c>/customers", template : item});


		if(filters)
			this.getView().byId("list").getBinding("items").filter(filters);

	},

	onDirectSearchPress:function()
	{
		var page = this.getView().byId("customersListPageId");
		if(!this.directSearchDialog)
			this.directSearchDialog = sap.ui.xmlfragment("view.dialog.customerDirectSearchDialog", this);

		page.addDependent(this.directSearchDialog);
		this.directSearchDialog.open();
	},
	onDirectSearchOK:function(evt)
	{
		var value = this.uiModel.getProperty("/searchValue");
		if(_.isEmpty(value))
    {
      sap.m.MessageToast.show("Entry a value on search field");
      return;
    }
		this.getView().getModel("appStatus").setProperty("/customerRequester", this.customerRequester.getParameters());
		model.persistence.Storage.session.save("customerRequester", this.customerRequester.getParameters());
		this.directSearchDialog.close();
		this.router.navTo("customerDetail",{id: value} );
	},
	onDirectSearchClose:function(evt)
	{
		this.directSearchDialog.close();
	},
	onDirectSearchAfter:function(evt)
	{
		delete(this.directSearchDialog);
	},
	onFilterPress:function()
	{
		this.filterModel = model.filters.Filter.getModel(this.customersModel.getData().customers, "customers");
		this.getView().setModel(this.filterModel, "filter");
		var page = this.getView().byId("customersListPageId");
		this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
		page.addDependent(this.filterDialog);
		this.filterDialog.open();

	},
	onFilterDialogClose:function()
	{
		this.filterDialog.close();
	},

	onFilterPropertyPress:function(evt)
	{

		var parentPage = sap.ui.getCore().byId("parent");
		var elementPage = sap.ui.getCore().byId("children");
		//console.log(this.getView().getModel("filter").getData().toString());
		var navCon = sap.ui.getCore().byId("navCon");
		var selectedProp = 	evt.getSource().getBindingContext("filter").getObject();
		this.getView().getModel("filter").setProperty("/selected", selectedProp);
		this.elementListFragment = sap.ui.xmlfragment("view.fragment.filterList", this);
		elementPage.addContent(this.elementListFragment);

		navCon.to(elementPage, "slide");
		this.getView().getModel("filter").refresh();
	},

	onBackFilterPress:function(evt)
	{
		// this.addSelectedFilterItem();
		this.navConBack();
		this.getView().getModel("filter").setProperty("/selected", "");
		this.elementListFragment.destroy();
	},
	navConBack:function()
	{
		var navCon = sap.ui.getCore().byId("navCon");
		navCon.to(sap.ui.getCore().byId("parent"), "slide");
		this.elementListFragment.destroy();
	},
	afterOpenFilter:function(evt)
	{
		var navCon = sap.ui.getCore().byId("navCon");
		if(navCon.getCurrentPage().getId()== "children")
			navCon.to(sap.ui.getCore().byId("parent"), "slide");
		this.getView().getModel("filter").setProperty("/selected", "");
	},

	onSearchFilter:function(oEvt)
	{
			var aFilters = [];
			var sQuery = oEvt.getSource().getValue();

			if (sQuery && sQuery.length > 0) {

					// var filter = new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.Contains, sQuery);

				// 	var filter = new sap.ui.model.Filter({path:"value", test:function(val)
				// {
				// 	var property= val.toString().toUpperCase();
				// 	return (property.indexOf(sQuery.toString().toUpperCase())>=0)
				// }});

					aFilters.push(this.createFilter(sQuery, "value"));
				}

			// update list binding
			var list = sap.ui.getCore().byId("filterList");
			var binding = list.getBinding("items");
			binding.filter(aFilters);
	},
	createFilter:function(query, property)
	{
		var filter = new sap.ui.model.Filter({path:property, test:function(val)
		{
			var prop= val.toString().toUpperCase();
			return (prop.indexOf(query.toString().toUpperCase())>=0)
		}});
		return filter;
	},
	onFilterDialogClose:function(evt)
	{
		if (this.elementListFragment) {this.elementListFragment.destroy();}
		if (this.filterDialog) {
			this.filterDialog.close();
			this.filterDialog.destroy();
		}
	},
	onFilterDialogOK:function(evt)
	{
		var filterItems = model.filters.Filter.getSelectedItems("customers");
		if(this.elementListFragment)
			this.elementListFragment.destroy();
		this.filterDialog.close();
		this.getView().getModel("filter").setProperty("/selected", "");
		this.handleFilterConfirm(filterItems);
		this.filterDialog.destroy();
		delete(this.filterDialog);
	},
	handleFilterConfirm: function(selectedItems)
	{
		var filters = [];
		_.forEach(selectedItems, _.bind(function(item)
	{
		filters.push(this.createFilter(item.value, item.property));
	},
	this));
		var list = this.getView().byId("list");
		var binding = list.getBinding("items");
		binding.filter(filters);
	},
	onResetFilterPress: function()
	{
		model.filters.Filter.resetFilter("customers");
		// //console.log(model.filters.Filter.getSelectedItems("customers"));
	}








});
