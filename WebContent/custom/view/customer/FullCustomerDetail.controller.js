jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.CustomerRequester");
jQuery.sap.require("utils.Busy");

view.abstract.AbstractController.extend("view.customer.FullCustomerDetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
		
	    this.customerModel = new sap.ui.model.json.JSONModel();
	    this.getView().setModel(this.customerModel,"c");
	
	    this.destinationModel = new sap.ui.model.json.JSONModel();
	    this.getView().setModel(this.destinationModel, "fdest");

		this.customerDiscountModel = new sap.ui.model.json.JSONModel();
		this.getView().setModel(this.customerDiscountModel, "cd");
	},


	handleRouteMatched: function (evt) {


		var routeName = evt.getParameter("name");

		if ( routeName !== "fullCustomerDetail"){
			return;
		}
		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
//		var id = evt.getParameters().arguments.id;

        this.customer = new model.Customer(model.persistence.Storage.session.get("currentCustomer"));
        
        var id = this.customer.registry.id;

		var enableModel = new sap.ui.model.json.JSONModel();
		enableModel.setData({"detail": true});
		this.customerModel.setData(this.customer);

		this.customerDiscountRequester = new model.CustomerDiscountRequester(this.customer);
    //**
    this.populateSelect();
    this.uiModel.setProperty("/newDestination", true);
    this.onInsertNewAddressReset();
    //**

//		var reqData = this.getView().getModel("appStatus").getProperty("/customerRequester") ? this.getView().getModel("appStatus").getProperty("/customerRequester") : model.persistence.Storage.session.get("customerRequester");
//		this.customerRequester = new model.CustomerRequester();
//		this.customerRequester.setParameters(reqData);
		// model.collections.Customers.getById(id)
		// .then(
		//
		// 	_.bind(function(result)
		// 	{
		// 		this.customer = result;
		// 		// this.refreshView(result, routeName)
		// 		this.refreshView(result);
		// 	}, this)
		// );

//		this.customerRequester.loadCustomerDetail(id)
//			.then(_.bind(function(result)
//						{
//							utils.Busy.hide();
//							this.customer = result;
//							// this.refreshView(result, routeName)
//              this.loadDestination();
//							this.refreshView(result);
//						}, this),
//						_.bind(function(error)
//							{
//									sap.m.MessageToast.show("Error loading Customer Detail");
//
//										// this.refreshView(result, routeName)
//									this.refreshView();
//							}, this)
//
//			);

	},

    navBackToMobileMaster: function(evt){
      this.router.navTo("launchpad");
    },

  loadDestination: function () {
    var infoForReq = this.getOrgDataForSAP();
    infoForReq.KunnrAg = this.customer.registry.id;
    infoForReq.Cdage = this.customer.registry.agentCode;
    infoForReq.Parvw = "WE";
    delete infoForReq.Bukrs;

    var defer = Q.defer();
    var fSuccess = function (result) {
      this.dests = {
        "items": []
      };
      (this.dests).items = result;
      var cModel = new model.Customer(this.customer);
      cModel.setDestinations(result);

      //**

      this.getView().getModel("c").setData(cModel);
      defer.resolve(result);

    };
    var fError = function (err) {
      //console.log(err);
      defer.reject(err);
      sap.m.MessageToast.show("problems loading destinations");
    };

    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);
    var destModel = new model.Destination;
    destModel.loadDestinations(infoForReq).then(fSuccess, fError);

    return defer.promise;

  },


  onInsertNewAddressReset: function (evt) {
    var cleanDest = new model.Destination();
    this.destinationModel.setData(cleanDest);
    this.getView().getModel("fdest").refresh();
  },

  onInsertNewAddressCancel: function () {
    //this.onInsertNewAddressReset();
    this.insertNewAddressDialog.close();
  },

  onAddDestination: function () {
    if (!this.insertNewAddressDialog) {
      this.insertNewAddressDialog = sap.ui.xmlfragment("view.dialog.insertNewAddress", this);
      var page = this.getView().byId("fullCustomerDetailPage");
      page.addDependent(this.insertNewAddressDialog);
    }
    this.onInsertNewAddressReset();
    this.insertNewAddressDialog.open();
  },
  onInsertNewAddressConfirm: function () {

    var fDestModel = this.getView().getModel("fdest").getData();


    var infoForReq = this.getOrgDataForSAP();
    infoForReq.KunnrAg = this.customer.registry.id;
    infoForReq.Cdage = this.customer.registry.agentCode;
    infoForReq.Parvw = "WE";
    delete infoForReq.Bukrs;
     for(var prop in fDestModel)
    {
      if(fDestModel[prop] && !_.isFunction(fDestModel[prop]))
      infoForReq[prop] = fDestModel[prop];
    }

    var fSuccess = function () {
      utils.Busy.hide();
      if (this.insertNewAddressDialog) {
        this.insertNewAddressDialog.close();
      }
      this.loadDestination();
    };
    var fError = function (err) {
      utils.Busy.hide();
      if (this.insertNewAddressDialog) {
        this.insertNewAddressDialog.close();
      }
    };
    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);

    model.Destination().addDestination(infoForReq).then(fSuccess,fError);
  },



  populateSelect: function () {
    var pToSelArr = [
      {
        "type": "Land1",
        "namespace": "land"
      }
    ];

    var workingUser = model.persistence.Storage.session.get("workingUser");
    this.orgData = {
      "Bukrs": workingUser.organizationData.results[0].society,
      "Vkorg": workingUser.organizationData.results[0].salesOrg,
      "Vtweg": workingUser.organizationData.results[0].distributionChannel,
      "Spart": workingUser.organizationData.results[0].division
    };
    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.getOdataSelect(item.type, this.orgData)
        .then(_.bind(function (result) {
          if (item.namespace === "codElabSpeciale" || item.namespace === "shippingType") {
            result.getData().results.unshift({});
          }
          this.getView().setModel(result, item.namespace);
        }, this));
    }, this));

  },

  populateRegionSelect: function (evt) {
    var req = {
      "Land1": ""
    };
    req.Land1 = evt.getSource().getSelectedKey();
    utils.Collections.getOdataSelect("Regio", req)
      .then(_.bind(function (result) {
        result.setSizeLimit(500);
        this.getView().setModel(result, "prov");
        this.uiModel.setProperty("/enableRegion", true);
      }, this));

  },

	onCustomerDiscountEditPress:function(evt)
  {

      this.customerDiscountModel=this.customerDiscountRequester.getModel();
      this.getView().setModel(this.customerDiscountModel, "cd");

    if(!this.customerDiscountDialog)
    {
      this.customerDiscountDialog = sap.ui.xmlfragment("view.dialog.CustomerDiscountDialog", this);
    }
    var page = this.getView().byId("fullCustomerDetailPage");

    page.addDependent(this.customerDiscountDialog);
    this.customerDiscountDialog.open();
  },

  onCustomerDiscountValueChange:function(evt)
	{
		this.customerDiscountModel.setProperty("/value", parseFloat(this.customerDiscountModel.getProperty("/value")));
		this.customerDiscountModel.refresh();
	},
	onCustomerDiscountClose:function(evt)
	{
		this.customerDiscountDialog.close();
	},
	onCustomerDiscountOK:function(evt)
	{
		this.customerDiscountDialog.close();
    this.customerDiscountRequester.saveCustomerDiscountValue();
    this.getView().getModel("c").refresh()
		// jQuery.sap.delayedCall(3000, this, function () {
		// 	this.router.navTo("launchpad");
		// });
	},







});
