jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.CustomerRequester");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("utils.Message");

view.abstract.AbstractController.extend("view.customer.CustomerEdit", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		
		var routeName = evt.getParameter("name");

		if ( routeName !== "customerEdit" ){
			return;
		}

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var id = evt.getParameters().arguments.id;
    this.openCustomerEdit(id);


//		model.collections.Customers.getById(id)
//		.then(
//
//			_.bind(function(result)
//			{
//				this.customer = result;
//				// this.refreshView(result, routeName)
//				this.refreshView(result);
//			}, this)
//		);

	},
	// refreshView : function(data, route)
	// {
	// 		var enable = {"editable": false};
	// 		var page = this.getView().byId("detailPage");
	// 		if(route && route.toUpperCase().indexOf("edit".toUpperCase())>=0)
	// 		{
	// 			var clone = _.cloneDeep(data.getModel().getData());
	// 			var clonedModel = new sap.ui.model.json.JSONModel(clone);
	// 			this.getView().setModel(clonedModel, "c");
	// 			enable.editable = true;
	// 			var toolbar = sap.ui.xmlfragment("view.fragment.editToolbar", this);
	// 			page.setFooter(toolbar);
	// 			//Maybe to parameterize
	// 			this.populateSelect();
	// 			//------------------------
	// 		}
	// 		else
	// 		{
	// 			this.getView().setModel(data.getModel(), "c");
	// 			enable.editable=false;
	// 			var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar", this);
	// 			page.setFooter(toolbar);
	// 		}
	// 		var enableModel = new sap.ui.model.json.JSONModel(enable);
	// 		this.getView().setModel(enableModel, "en");
	//
	// },
	refreshView : function(data) //Could it be abstracted?
	{
			var enable = {"editable": false};
			var page = this.getView().byId("editPage");

			var clone = _.cloneDeep(data.getModel().getData());
			if(this.getView().getModel("c"))
			{
				this.getView().getModel("c").setData(clone);
			}
			else
			{
				var clonedModel = new sap.ui.model.json.JSONModel(clone);
				this.getView().setModel(clonedModel, "c");
			}

				// var clonedModel = new sap.ui.model.json.JSONModel(clone);
				// this.getView().setModel(clonedModel, "c");



			enable.editable = true;
			// var toolbar = sap.ui.xmlfragment("view.fragment.editToolbar", this);
			// page.setFooter(toolbar);
			//Maybe to parameterize
			this.populateSelect();
			this.populateSegmentedCodeSelect();
				//-----------------------
			var enableModel = new sap.ui.model.json.JSONModel(enable);
			this.getView().setModel(enableModel, "en");

	},

	onSavePress :function()
	{
		// if(!this.validateCheck() )
		// {
		// 		sap.m.MessageToast.show("Client update failed");
		// 		return;
		// }
		// if(!this.onPhoneChanged())
		// {
		// 	sap.m.MessageToast.show("Client update failed: at least a Phone Number required");
		// 	return;
		// }
		var editedCustomer = this.getView().getModel("c").getData();
		// sap.m.MessageToast.show("Update still unrealized ");
		this.customer.update(editedCustomer);
		utils.Busy.show();
		this.customer.updateToSAP(true)
		.then(_.bind(function(res){
			utils.Busy.hide();
			//console.log(res);
			sap.m.MessageToast.show("Client updated with success");
			this.router.navTo("customerDetail", {id:this.customer.getId()})
			}, this),
			_.bind(function(err){

				utils.Busy.hide();
				var msg =utils.Message.getError(err);
				sap.m.MessageToast.show(msg);
				jQuery.sap.delayedCall(3000, this, _.bind(function(){this.router.navTo("customerDetail", {id:this.customer.getId()});}, this))
//				var error = JSON.parse(err.response.body);
//				var errorDetails = error.error.innererror.errordetails;
//				var msg = "Client update failed";
//				var detailsMsg = _.where(errorDetails, function(item){return (item.code.indexOf("IWBEP")>0)});
//
//				for(var i = 0; detailsMsg && detailsMsg.length && i < detailsMsg.length; i++)
//				{
//					msg += ", " +detailsMsg[i].message;
//				}
//				sap.m.MessageToast.show(msg);

			}, this))
		//
		// this.refreshView(this.customer);
	},

    navBackToMobileMaster: function(evt){
      this.router.myNavBack();
    },


	onResetPress : function()
	{
		this.getView().getModel("c").setData(_.cloneDeep(this.customer.getModel().getData()));
	},

	populateSelect :  function()
	{
		//HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
		//HP2 : Every collections contains an array of params where a select is needed
		// var pToSelArr = [
		// 	{"type":"registryTypes", "namespace":"rt"},
		// 	{"type":"billTypes", "namespace":"bt"},
		// 	{"type":"contactTypes", "namespace":"ct"},
		// 	{"type":"paymentConditions", "namespace": "pc"},
		// 	{"type": "places", "namespace":"p"},
    //         {"type": "customerTypes", "namespace":"customerType"}
		// 	];
		var pToSelArr = [
			{"type":"Land1", "namespace": "land"},
			{"type":"Bzirk", "namespace": "carrier"},
			{"type":"Zwels", "namespace":"billType"},//or PayMode
			// {"type":"contactTypes", "namespace":"ct"},ContactType missing
			{"type":"Zterm", "namespace": "payCond"}, //paymentCondition
			{"type":"Perfk", "namespace": "billFreq"},
			{"type":"Inco1", "namespace": "resa"},
			// {"type":"Kdgrp", "namespace":"clientType"}
			// {"type": "places", "namespace":"p"},LandName missing
      // {"type": "customerTypes", "namespace":"customerType"}, customerType missing
			];

		var workingUser = model.persistence.Storage.session.get("workingUser");
		this.orgData = {
										"Bukrs": workingUser.organizationData.results[0].society,
										"Vkorg": workingUser.organizationData.results[0].salesOrg,
										"Vtweg": workingUser.organizationData.results[0].distributionChannel,
										"Spart": workingUser.organizationData.results[0].division
									};
		_.map(pToSelArr, _.bind(function(item)
	{
		utils.Collections.getOdataSelect(item.type, this.orgData)
		.then(_.bind
			(function(result)
		{
			this.getView().setModel(result, item.namespace);

		}, this))
	}, this));


	},
	populateSegmentedCodeSelect: function(evt)
	{
		utils.Collections.getOdataSelect("Katr6",{})
		.then(_.bind
			(function(result)
		{
			this.getView().setModel(result, "segCode");

		}, this))
	},
	populateRegionSelect:function(evt)
	{
		var req = {"Land1":""};
		if(evt)
			req.Land1 = evt.getSource().getSelectedKey();
		else
		if(this.customer.registry.nation)
			req.Land1 = this.customer.registry.nation;
		else {
			return;
		}

		utils.Collections.getOdataSelect("Regio", req)
		.then(_.bind
			(function(result)
		{
			this.getView().setModel(result, "prov");

		}, this))

	},
	onNationChange:function(evt)
	{
		this.getView().byId("regionSelect").setValue("");
		this.populateRegionSelect(evt);
	},

  openCustomerEdit: function(id)
  {
  // model.collections.Customers.getById(id)
	// 	.then(
	//
	// 		_.bind(function(result)
	// 		{
	// 			this.customer = result;
	// 			// this.refreshView(result, routeName)
	// 			this.refreshView(result);
	// 		}, this)
	// 	);
		var reqData = this.getView().getModel("appStatus").getProperty("/customerRequester") ? this.getView().getModel("appStatus").getProperty("/customerRequester") : model.persistence.Storage.session.get("customerRequester");
		this.customerRequester = new model.CustomerRequester();
		this.customerRequester.setParameters(reqData);
		utils.Busy.show();
		this.customerRequester.loadCustomerDetail(id)
			.then(_.bind(function(result)
						{
							this.customer = result;
							this.populateRegionSelect();
							// this.refreshView(result, routeName)
							this.refreshView(result);
							utils.Busy.hide();
						}, this), function (){
							sap.m.MessageToast.show("Error loading Customers List");
							utils.Busy.hide();
						}

			);

  },
	onSelectionChange:function(evt)
	{
		var src = evt.getSource();
		var id = src.getId();
		var viewId = this.getView().getId();
		var value = src.getSelectedItem().getText();
		var clonedModel = this.getView().getModel("c");
		switch(id)
		{
			case viewId+"--comboResaId" :
				clonedModel.setProperty("/sales/resaDescr", value);
				break;
			// case viewId+"--comboBillTypeId":
			// 	clonedModel.setProperty("/sales/resaDescr", value);
			// 	break;
			// case viewId+"--comboBillFreqId":
			// 	clonedModel.setProperty("/sales/resaDescr", value);
			// 	break;
			case viewId+"--comboCarrierId":
				clonedModel.setProperty("/sales/carrierDescr", value);
				break;
			// case viewId+"--comboPayCondId":
			// 	clonedModel.setProperty("/sales/resaDescr", value);
			// 	break;
			default:
				//console.log("others");
				break;
		}
	},
	onPhoneChanged:function(evt)
	{

			var mobile = this.getView().byId("mobileEditInput");
			var phone = this.getView().byId("phoneEditInput");
			if(mobile.getValue() !== "" || phone.getValue()!== "")
			{
				mobile.setValueState("None");
				mobile.setValueStateText("");
				phone.setValueState("None");
				mobile.setValueStateText("");
				return true;
			}
			else
			{
				mobile.setValueState("Warning");
				mobile.setValueStateText(model.i18n.getText("WARNINGPHONETOOLTIP"));
				phone.setValueState("Warning");
				phone.setValueStateText(model.i18n.getText("WARNINGPHONETOOLTIP"));
				return false;
			}

	},

	onIBANChange :function(evt)
	{
		var ibanValue = evt.getSource().getValue();
		var customer = this.getView().getModel("c").getData();
		customer.setBankByIBAN(ibanValue);
		this.getView().getModel("c").setData(customer);
	}




});
