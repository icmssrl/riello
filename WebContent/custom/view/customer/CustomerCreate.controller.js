jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Customer");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.CustomerRequester");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("utils.Message");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("model.CustomerDiscountRequester");

view.abstract.AbstractMasterController.extend("view.customer.CustomerCreate", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    // 	if( !sap.ui.Device.system.tablet && !sap.ui.Device.system.phone ){
    //
    //
    //   if(!this.fragment)
    //   {
    //    this.fragment = new sap.ui.xmlfragment("view.fragment.RadioButton", this.getView().getController());
    //
    //   }
    //
    // }
    // else{
    //
    //
    //
    //  if(!this.fragment)
    //    {
    //     this.fragment = new sap.ui.xmlfragment("view.fragment.SegmentedButton", this.getView().getController());
    //
    //    }
    //   }
    // this.getView().byId("panelButton").addContent(this.fragment);
  },


  handleRouteMatched: function (evt) {


    var name = evt.getParameter("name");

    if (name !== "newCustomer") {
      return;
    }
    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);



    this.user = model.persistence.Storage.session.get("user");
    // this.userInfo =  model.persistence.Storage.session.get("workingUser").organizationData.results[0];
    // this.userInfo.userType = this.user.type;
    // this.userInfo.distrCh = this.userInfo.distributionChannel;
    this.userInfo = this.getUserInfo();
    this.userOrgData = model.persistence.Storage.session.get("workingUser"); //Not sure

    this.customer = new model.Customer();

    //---------------In Version 2 it should be modified--------------
    //	this.user must contain the maxValue(default) of agent
    //	In this.customer.initialize from this.user, get agentDefaultCondition

    this.customer.initialize(this.user, this.userOrgData);
    //----------------------------------------------------------------
    if (model.persistence.Storage.session.get("customerRequester") && this.getView().getModel("appStatus").getProperty("/fromCustomerDetail")) {
      this.getView().getModel("appStatus").setProperty("/fromCustomerDetail", false);
      this.customerRequester = model.persistence.Storage.session.get("customerRequester");
      this.customer.registry.agentCode = this.customerRequester.Cdage;
      this.customer.registry.territoryManager = this.customerRequester.Vkgrp;
    }

    this.customerModel = this.customer.getModel();
    this.getView().setModel(this.customerModel, "c");
    this.populateSelect();
    this.populateSegmentedCodeSelect();
    this.getView().byId("regAlrtBtn").setVisible(false);
    this.getView().byId("ctAlrtBtn").setVisible(false);
    this.getView().byId("bkAlrtBtn").setVisible(false);
    this.getView().byId("slsAlrtBtn").setVisible(false);

    this.setRegistryTypeDialog();
    this.uiModel.setProperty("/taxCodeState", "None");
    this.uiModel.setProperty("/vatNumState", "None");
    this.uiModel.setProperty("/enableRegion", false);
    
    
    //**
    /*
    carico gli sconti se e solo ho un agente selezionato
    */
    //**
    this.uiModel.setProperty("/discountPanelVisibility", false);
    
    //if(this.userOrgData.organizationData.results[0].division == "RI")
    var user = model.persistence.Storage.session.get("workingUser");
      //if(this.checkEnabledToTradePolicies())
    if(user.organizationData.results[0].canEditDiscount)
    {
    	this.uiModel.setProperty("/discountPanelVisibility", true);
    	if (this.userOrgData.userGeneralData.customer !== "" ) {
    	      this.loadCustomerDiscountConditions();  
    	}
    	
    	    
    	  this._discountModifyFlag = false;
    	    
    }
    

  },

  setRegistryTypeDialog: function () {
    if (!this.registryDialog) {
      this.registryDialog = sap.ui.xmlfragment(
        "view.dialog.setRegistryTypeDialog",
        this
      );
    }
    this.getView().addDependent(this.registryDialog);
    this.uiModel.setProperty("/regDialogCloseEn", false);
    this.registryDialog.open();
  },
  onSetRegistryOKPress: function (evt) {
    // this.checkRegTypeValues();

    var taxCodeItem = _.find(this._checkingValues, function (item) {
      return item.getId().indexOf("taxCode") > 0
    });
    this.checkInputField(taxCodeItem);
    var mailLabel = this.getView().byId("mailLabel");

    if (this.customer.registry.registryType === "C") {
      var vatNumItem = _.find(this._checkingValues, function (item) {
        return item.getId().indexOf("VATNum") > 0
      });
      vatNumItem = vatNumItem ? vatNumItem : sap.ui.getCore().byId("vatNumInput");
      this.checkInputField(vatNumItem);

      mailLabel.data("req", "true");
    } else {
      mailLabel.data("req", "changed");
      this.checkInputField(mailLabel);
      mailLabel.setValueState("None");
    }
    utils.Busy.show()
    this.customer.checkKeyData()
      .then(_.bind(function (result) {
          utils.Busy.hide();
          // var taxCodeInput = this.getView().byId("taxCode");
          // var vatNumInput =this.getView().byId("VATNum");
          // taxCodeInput.setEditable(false);
          // vatNumInput.setEditable(false);
          //sap.m.MessageToast.show(model.i18n.getText("CF_OK"));
          if (!this.customer.registry.nation) {
            this.customerModel.setData(this.customer);
            this.registryDialog.close();
          } else {
            var req = {
              "Land1": this.customer.registry.nation
            };
            utils.Collections.getOdataSelect("Regio", req)
              .then(_.bind(function (result) {
                this.uiModel.setProperty("/enableRegion", true);
                result.setSizeLimit(500);
                this.getView().setModel(result, "prov");
                this.customerModel.setData(this.customer);
                this.registryDialog.close();
              }, this))

          }


        }, this),
        _.bind(function (err) {
          utils.Busy.hide();
          sap.m.MessageToast.show(err);

        }, this))




  },
  onSetRegistryCancelPress: function (evt) {
    this.registryDialog.close();

    jQuery.sap.delayedCall(300, this, function () {
      this.onHomePress();
    });
  },
  onRegistryTypeModified: function () {
    if (!this.checkRegTypeValues()) {
      this.setRegistryTypeDialog();
    } else {
      var mailLabel = this.getView().byId("mailLabel");
      if (this.customer.registry.registryType === "C") {
        mailLabel.data("req", "true");
      } else {
        mailLabel.data("req", "changed");

        this.checkInputField(mailLabel);
        mailLabel.setValueState("None");
      }
    }

  },
  checkRegTypeValues: function () {
    this.customer.registry.VATNumber = this.customer.registry.VATNumber ? this.customer.registry.VATNumber.toUpperCase() : "";
    this.customer.registry.taxCode = this.customer.registry.taxCode ? this.customer.registry.taxCode.toUpperCase() : "";

    var res = !((this.customer.registry.registryType === "P" && _.isEmpty(this.customer.registry.taxCode)) || (this.customer.registry.registryType === "C" && (_.isEmpty(this.customer.registry.VATNumber) || _.isEmpty(this.customer.registry.taxCode))) || _.isEmpty(this.customer.registry.registryType));

    if (this.customer.registry.registryType === "P") {
      this.customer.registry.VATNumber = "";
    }

    this.uiModel.setProperty("/regDialogCloseEn", res);

    if (this.customer.registry.registryType === "C") {
      if (_.isEmpty(this.customer.registry.VATNumber)) {
        this.uiModel.setProperty("/vatNumState", "Error");
      } else {
        this.uiModel.setProperty("/vatNumState", "None");
      }
      if (_.isEmpty(this.customer.registry.taxCode)) {
        this.uiModel.setProperty("/taxCodeState", "Error");
      } else {
        this.uiModel.setProperty("/taxCodeState", "None");
      }
      //this.uiModel.setProperty("/taxCodeState", "None");

    }
    if (this.customer.registry.registryType === "P") {
      this.uiModel.setProperty("/vatNumState", "None");

      if (_.isEmpty(this.customer.registry.taxCode))
        this.uiModel.setProperty("/taxCodeState", "Error");

    }
    if (_.isEmpty(this.customer.registry.registryType)) {

      this.uiModel.setProperty("/taxCodeState", "Warning");
      this.uiModel.setProperty("/vatNumState", "Warning");

    }

    return res;
  },

  refreshView: function (data) {
    this.getView().setModel(data.getModel(), "c");
    // var masterCntrl = this.getView().getModel("appStatus").getProperty("/masterCntrl");
    // masterCntrl.refreshList();
  },



  // checkPanelStatus: function()
  // {
  // 	// var result = true;
  // 	// if(!this.validateCheck() || !this.checkRegTypeValues())
  // 	// {
  //
  // 			var failedControls = this.getFailedControls();
  // 			var result = (failedControls && failedControls.length>0);
  // 			_.forEach(failedControls, function(item)
  // 			{
  // 				var jqueryCntrl = $('#'+item.getId());
  // 				var parents = jqueryCntrl.parents();
  // 				var jqueryPanel = _.find(parents, function(obj){ return (obj.className === "sapMPanel");});
  // 				var ui5Panel = sap.ui.getCore().byId(jqueryPanel.id);
  // 				var hBar = ui5Panel.getHeaderToolbar();
  // 				var icon = _.find(hBar.getContent(), _.bind(function(value){
  // 				var props = value.mProperties;
  // 				return props.hasOwnProperty("icon");
  // 			}, this));
  // 				icon.setVisible(true);
  // 		})
  //
  // 	// }
  // 	return result;
  // },
  setErrorInPanel: function (panel, isError) {
    // var jqueryCntrl = $('#'+item.getId());
    // var parents = jqueryCntrl.parents();
    // var jqueryPanel = _.find(parents, function(obj){ return (obj.className === "sapMPanel");});
    // var ui5Panel = sap.ui.getCore().byId(jqueryPanel.id);
    var hBar = panel.getHeaderToolbar();
    var icon = _.find(hBar.getContent(), _.bind(function (value) {
      var props = value.mProperties;
      return props.hasOwnProperty("icon");
    }, this));
    icon.setVisible(isError);
    return icon;
  },
  getPanel: function (control) {
    var jqueryCntrl = $('#' + control.getId());
    var parents = jqueryCntrl.parents();
    var jqueryPanel = _.find(parents, function (obj) {
      return (obj.className === "sapMPanel");
    });
    var ui5Panel = sap.ui.getCore().byId(jqueryPanel.id);
    return ui5Panel;
  },

  //----------Using odata--------------------------------
  onSavePress: function () {
    // if(!this.validateCheck() || !this.checkRegTypeValues())
    // {
    // 		var failedControls = this.getFailedControls();
    // 		_.forEach(failedControls, function(item)
    // 	{
    // 		var jqueryCntrl = $('#'+item.getId());
    // 		var parents = jqueryCntrl.parents();
    // 		var jqueryPanel = _.find(parents, function(obj){ return (obj.className === "sapMPanel");});
    // 		var ui5Panel = sap.ui.getCore().byId(jqueryPanel.id);
    // 		var hBar = ui5Panel.getHeaderToolbar();
    // 		var icon = _.find(hBar.getContent(), _.bind(function(value){
    // 		var props = value.mProperties;
    // 		return props.hasOwnProperty("icon");
    // 	}, this));
    // 		icon.setVisible(true);
    // })

	 var user = model.persistence.Storage.session.get("workingUser");
	 
    if ( user.organizationData.results[0].canEditDiscount && this.customer.getCustomerDiscountValue() == 0 && !this._discountModifyFlag) {

      var that = this;

      sap.m.MessageBox.show(model.i18n.getText("customerDiscountValueEqualsZero"), {
        icon: sap.m.MessageBox.Icon.ALERT,
        title: model.i18n.getText("customerDiscountAlert"),
        actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
        onClose: _.bind(function (oAction) {

          if (oAction == "YES") {
            this._discountModifyFlag = true;
            this.onSavePress();
          };

        }, that)

      });

    }

    if (!this._discountModifyFlag && user.organizationData.results[0].canEditDiscount ) 
      return;


    if (!this.validateCheck() || !this.checkRegTypeValues()) {
      sap.m.MessageToast.show("Client creation failed");
      return;
    }
    if (!this.onPhoneChanged()) {
      sap.m.MessageToast.show(model.i18n.getText("WARNINGPHONETOOLTIP"));
      return;
    }

    //utils.Busy.show();
    this.getView().setBusy(true);
    this.customer.sendToSAP()
      .then(_.bind(function (res) {
        //utils.Busy.hide();
        this.getView().setBusy(false);
        //console.log(res);
        //sap.m.MessageToast.show("Client created with success with id:"+res.Kunnr);
        sap.m.MessageToast.show(model.i18n.getText("CUSTOMER_CREATE_SUCCESS") + res.Kunnr);
        if (model.persistence.Storage.session.get("society")) {
          var society = model.persistence.Storage.session.get("society");
          var division = model.persistence.Storage.session.get("division");
        }
        //			var that = this;
        //			sap.m.MessageBox.show(
        //	      model.i18n.getText("customerDiscountRequest"), {
        //	          icon: sap.m.MessageBox.Icon.QUESTION,
        //	          title: model.i18n.getText("additionalCustomerInfo"),
        //	          actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
        //	          onClose: _.bind(function(oAction) {
        //							if(oAction == "YES"){
        //								that.customerDiscountRequester = new model.CustomerDiscountRequester(that.customer);
        //								that.discountModel =that.customerDiscountRequester.getModel();
        //								that.getView().setModel(that.discountModel, "cd");
        //
        //
        //
        //
        //								var page = that.getView().byId("customerCreatePage");
        //							 // this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog", this);
        //								if(!that.discountDialog){
        //								that.discountDialog = sap.ui.xmlfragment("view.dialog.customerDiscountDialog", that);
        //								}
        //
        //
        //								page.addDependent(that.discountDialog);
        //								that.discountDialog.open();
        //								//-----------------------After----------------------------------------
        //								// that.customerDiscountRequester.loadMaxCustomerDiscountValue()
        //								// .then(_.bind(function(res){
        //								// 	that.discountModel =that.customerDiscountRequester.getModel();
        //								// 	that.getView().setModel(that.discountModel, "cd");
        //								//
        //								//
        //								// 	var page = that.getView().byId("customerCreatePage");
        //								//  // this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog", this);
        //								// 	if(!that.discountDialog){
        //								// 	that.discountDialog = sap.ui.xmlfragment("view.dialog.customerDiscountDialog", that);
        //								// 	}
        //								//
        //								//
        //								// 	page.addDependent(that.discountDialog);
        //								// 	that.discountDialog.open();
        //								//
        //								// }, that), _.bind(function(err){
        //								// 	sap.m.MessageToast.show(utils.Message.getError(err));
        //								// }, that))
        //								//------------------------------------------------------------------------------------
        //							}
        //							else {
        //								jQuery.sap.delayedCall(3000, that, function () {
        //									that.router.navTo("launchpad");
        //								});
        //							}
        //						}, that)
        //	      }
        //			);





        jQuery.sap.delayedCall(3000, this, function () {
          this.router.navTo("launchpad");
        });



      }, this), _.bind(function (err) {
        //utils.Busy.hide();
        this.getView().setBusy(false);
        //				var error = JSON.parse(err.response.body);
        //				var errorDetails = error.error.innererror.errordetails;
        //				var msg = "Client creation failed";
        //				var detailsMsg = _.where(errorDetails, function(item){return (item.code.indexOf("IWBEP")>0)});
        //				for(var i = 0; detailsMsg && detailsMsg.length && i < detailsMsg.length; i++)
        //				{
        //					msg += ", " +detailsMsg[i].message;
        //				}
        var msg = utils.Message.getError(err);
        sap.m.MessageToast.show(msg);

      }, this))

  },
  //----------------------------------------------------------------------
  // onSavePress : function()
  // {
  //
  // 	if(!this.validateCheck())
  // 	{
  // 			var failedControls = this.getFailedControls();
  // 			_.forEach(failedControls, function(item)
  // 		{
  // 			var jqueryCntrl = $('#'+item.getId());
  // 			var parents = jqueryCntrl.parents();
  // 			var jqueryPanel = _.find(parents, function(obj){ return (obj.className === "sapMPanel");});
  // 			var ui5Panel = sap.ui.getCore().byId(jqueryPanel.id);
  // 			var hBar = ui5Panel.getHeaderToolbar();
  // 			var icon = _.find(hBar.getContent(), _.bind(function(value){
  // 				var props = value.mProperties;
  // 				return props.hasOwnProperty("icon");
  // 			}, this));
  // 			icon.setVisible(true);
  // 		})
  //
  // 			sap.m.MessageToast.show("Client creation failed");
  // 			return;
  // 	}
  //
  // 	// adding directly costumer to collections costumers -> to substitute with create Entity and refresh collections
  // 	model.collections.Customers.addCustomers(this.customer);
  // 	sap.m.MessageToast.show("Client created with success");
  // 	this.router.navTo("launchpad");
  // },
  onResetPress: function () {
    this.customer = new model.Customer();
    this.getView().getModel("c").refresh();
  },

  populateSelect: function () {
    //HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
    //HP2 : Every collections contains an array of params where a select is needed
    // var pToSelArr = [
    // 	{"type":"registryTypes", "namespace":"rt"},
    // 	{"type":"billTypes", "namespace":"bt"},
    // 	{"type":"contactTypes", "namespace":"ct"},
    // 	{"type":"paymentConditions", "namespace": "pc"},
    // 	{"type": "places", "namespace":"p"},
    //         {"type": "customerTypes", "namespace":"customerType"}
    // 	];
    var pToSelArr = [
      {
        "type": "Bzirk",
        "namespace": "carrier"
      },
      {
        "type": "Zwels",
        "namespace": "billType"
      }, //or PayMode
      {
        "type": "Land1",
        "namespace": "land"
      },
			// {"type":"contactTypes", "namespace":"ct"},ContactType missing
      {
        "type": "Zterm",
        "namespace": "payCond"
      }, //paymentCondition
      {
        "type": "Perfk",
        "namespace": "billFreq"
      },
      {
        "type": "Inco1",
        "namespace": "resa"
      },
      {
        "type": "Kdgrp",
        "namespace": "clientType"
      },


			// {"type": "places", "namespace":"p"},LandName missing
			// {"type": "customerTypes", "namespace":"customerType"}, customerType missing
			];

    var workingUser = model.persistence.Storage.session.get("workingUser");
    this.orgData = {
      "Bukrs": workingUser.organizationData.results[0].society,
      "Vkorg": workingUser.organizationData.results[0].salesOrg,
      "Vtweg": workingUser.organizationData.results[0].distributionChannel,
      "Spart": workingUser.organizationData.results[0].division
    };
    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.getOdataSelect(item.type, this.orgData)
        .then(_.bind(function (result) {
          this.getView().setModel(result, item.namespace);

        }, this))
    }, this));


  },
  populateSegmentedCodeSelect: function (evt) {
    utils.Collections.getOdataSelect("Katr6", {})
      .then(_.bind(function (result) {
        this.getView().setModel(result, "segCode");

      }, this))
  },
  populateRegionSelect: function (evt) {
    this.uiModel.setProperty("/enableRegion", true);
    var req = {
      "Land1": ""
    };
    req.Land1 = evt.getSource().getSelectedKey();
    utils.Collections.getOdataSelect("Regio", req)
      .then(_.bind(function (result) {
        result.setSizeLimit(500);
        this.getView().setModel(result, "prov");
        this.getView().getModel("c").refresh();
      }, this))

  },
  onSelectionChange: function (evt) {
    var src = evt.getSource();
    var id = src.getId();
    var viewId = this.getView().getId();
    var value = src.getSelectedItem().getText();
    var clonedModel = this.getView().getModel("c");
    switch (id) {
    case viewId + "--comboResaId":
      clonedModel.setProperty("/sales/resaDescr", value);
      break;
      // case viewId+"--comboBillTypeId":
      // 	clonedModel.setProperty("/sales/resaDescr", value);
      // 	break;
      // case viewId+"--comboBillFreqId":
      // 	clonedModel.setProperty("/sales/resaDescr", value);
      // 	break;
    case viewId + "--comboCarrierId":
      clonedModel.setProperty("/sales/carrierDescr", value);
      break;
      // case viewId+"--comboPayCondId":
      // 	clonedModel.setProperty("/sales/resaDescr", value);
      // 	break;
    default:
      //console.log("others");
      break;
    }
  },

  //Agent Selection(if needed)
  handleAgentValueHelp: function (evt) {
    this.customerRequester = new model.CustomerRequester(this.userInfo);
    // var sInputValue = evt.getSource().getValue();
    //
    // this.inputId = evt.getSource().getId();

    // choose dialog type Based on user type
    var chooseDialog = function (result) {


      if (this.user.type == "TM") {
        var agentsModel = new sap.ui.model.json.JSONModel();
        agentsModel.setData({
          "results": result
        });
        this.getView().setModel(agentsModel, "ag");
        if (!this._agentValueHelpDialog) {
          this._agentValueHelpDialog = sap.ui.xmlfragment(
            "view.dialog.AgentValueHelpDialog",
            this);
        }

        this.getView().addDependent(this._agentValueHelpDialog);
        this._agentValueHelpDialog.open();
      } else if (this.user.type == "AM") {
        var tmsModel = new sap.ui.model.json.JSONModel();
        tmsModel.setData({
          "results": result
        });
        this.getView().setModel(tmsModel, "tm");
        if (!this._TMValueHelpDialog) {
          this._TMValueHelpDialog = sap.ui.xmlfragment(
            "view.dialog.TM_AgentValueHelpDialog",
            this
          );
        }
        this.getView().addDependent(this._TMValueHelpDialog);
        this._TMValueHelpDialog.open();
        sap.ui.getCore().byId("backBtn").setVisible(false);
      } else {
        //console.log("Error:It should be disabled");
        return;
      }


    }
    chooseDialog = _.bind(chooseDialog, this);

    this.customerRequester.loadCustomersList()
      .then(chooseDialog);



    // create a filter for the binding
    // this._agentValueHelpDialog.getBinding("items").filter([new Filter(
    // 	"Name",
    // 	sap.ui.model.FilterOperator.Contains, sInputValue
    // )]);

    // open value help dialog filtered by the input value



  },
  handleAgentSearch: function (oEvent) {


    var sValue = oEvent.getParameter("value") ? oEvent.getParameter("value") : oEvent.getParameter("newValue");
    var params = ["registry/agentName", "registry/agentCode"];


    var oBinding = oEvent.getSource().getBinding("items") ? oEvent.getSource().getBinding("items") : sap.ui.getCore().byId("agentList").getBinding("items");
    var filters = this.assignFilters(oBinding.oList[0], sValue, params);
    // var filters = [];
    // filters.push(new sap.ui.model.Filter("registry/agentName", sap.ui.model.FilterOperator.Contains, sValue));
    // filters.push(new sap.ui.model.Filter("registry/agentCode", sap.ui.model.FilterOperator.Contains, sValue));

    oBinding.filter(filters);
  },
  handleAgentDialogClose: function (oEvent) {

    var src = oEvent;
    if (oEvent.getParameters && oEvent.getParameter("selectedItem")) {

      src = oEvent.getParameter("selectedItem")
    }
    var selectedAgent = src.getBindingContext("ag").getObject();
    this.customer.registry.territoryManager = selectedAgent.registry.territoryManager;
    this.customer.registry.agentCode = selectedAgent.registry.agentCode;
    this.customer.registry.agentName = selectedAgent.registry.agentName;
    if (oEvent.getParameters && oEvent.getParameter("selectedItem")) {
      oEvent.getSource().getBinding("items").filter([]);
    } else {
      var list = sap.ui.getCore().byId("agentList");
      list.getBinding("items").filter([]);
    }

    //-----Version 2--------------------------
    //Now can be loaded the agent discount default Value//-------------------
    //this.customerDiscountRequester.loadInitialCustomerDiscountConditions() ...
    //---------------------------------------------------------------
    this.loadCustomerDiscountConditions();
    //	this.refreshView(this.customer);
  },
  onTMPress: function (evt) {
    var src = evt.getSource();
    var tmSel = src.getBindingContext("tm").getObject();
    var loadAgents = function (result) {
      var agentModel = new sap.ui.model.json.JSONModel();
      agentModel.setData({
        "results": result
      });
      this.getView().setModel(agentModel, "ag");
      var navCon = sap.ui.getCore().byId("TMAGNavCon");
      navCon.to("AGList");
      sap.ui.getCore().byId("backBtn").setVisible(true);
    }
    loadAgents = _.bind(loadAgents, this);
    this.customerRequester.setTerritoryManager(tmSel.registry.territoryManager);
    this.customerRequester.loadCustomersList()
      .then(loadAgents);
  },
  onAgentSelected: function (evt) {
    var src = evt.getSource();

    this.customerRequester.removeTerritoryManager();
    this._TMValueHelpDialog.close();
    var navCon = sap.ui.getCore().byId("TMAGNavCon");
    navCon.to("TMList");
    this.handleAgentDialogClose(src);
  },
  onBackToTMPress: function (evt) {
    this.customerRequester.removeTerritoryManager();
    // var loadTMs  =function(result)
    // {
    // 	var tmsModel = new sap.ui.model.json.JSONModel();
    // 	tmsModel.setData({"results":result});
    // 	this.getView().setModel(tmsModel, "tm");
    // 	var navCon = this.getView().byId("TMAGNavCon");
    // 	navCon.to("TMList");
    // }
    // loadTMs = _.bind(loadTMs, this);
    // this.customerRequester.loadCustomersList()
    // 	.then(loadTMs);
    var navCon = sap.ui.getCore().byId("TMAGNavCon");
    navCon.to("TMList");
    sap.ui.getCore().byId("backBtn").setVisible(false);

  },
  onTMAgentDialogClose: function (evt) {
    this.customerRequester.removeTerritoryManager();
    this._TMValueHelpDialog.close();
    var navCon = sap.ui.getCore().byId("TMAGNavCon");
    navCon.to("TMList");
  },

  // 	populateSelect :  function()
  // 	{
  // 		//HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
  // 		//HP2 : Every collections contains an array of params where a select is needed
  // 		var pToSelArr = [
  // 			{"type":"registryTypes", "namespace":"rt"},
  // 			{"type":"billTypes", "namespace":"bt"},
  // 			{"type":"contactTypes", "namespace":"ct"},
  // 			{"type":"paymentConditions", "namespace": "pc"},
  // 			{"type": "places", "namespace":"p"},
  //             {"type": "customerTypes", "namespace":"customerType"}
  // 			];
  //
  // 		_.map(pToSelArr, _.bind(function(item)
  // 	{
  // 		utils.Collections.getModel(item.type) //It could be abstracted internally with a method that get the respective odata from the type received
  // 		.then(_.bind
  // 			(function(result)
  // 		{
  // 			this.getView().setModel(result, item.namespace);
  //
  // 		}, this))
  // 	}, this));
  // }

  // onStartSessionPress : function()
  // {
  // 	model.Persistence.session.save("customerSession", true);
  // 	model.Persistence.session.save("currentCustomer", this.customer);
  // 	this.router.navTo("launchpad");
  // }
  onPhoneChanged: function (evt) {

    var mobile = this.getView().byId("mobileInput");
    var phone = this.getView().byId("phoneInput");
    if (mobile.getValue() !== "" || phone.getValue() !== "") {
      mobile.setValueState("None");
      mobile.setValueStateText("");
      phone.setValueState("None");
      mobile.setValueStateText("");
      return true;
    } else {
      mobile.setValueState("Warning");
      mobile.setValueStateText(model.i18n.getText("WARNINGPHONETOOLTIP"));
      phone.setValueState("Warning");
      phone.setValueStateText(model.i18n.getText("WARNINGPHONETOOLTIP"));
      return false;
    }

  },
  onIBANChange: function (evt) {
    var ibanValue = evt.getSource().getValue();
    this.customer.setBankByIBAN(ibanValue);
    this.customerModel.setData(this.customer);
  },
  onCustomerDiscountValueChange: function (evt) {

    this.discountModel.setProperty("/value", parseInt(this.discountModel.getProperty("/value")));
    this.discountModel.refresh();
  },
  onCustomerDiscountClose: function (evt) {
    this.discountDialog.close();
  },
  onCustomerDiscountOK: function (evt) {
    this.discountDialog.close();
    sap.m.MessageToast.show(model.i18n.getText("discountRequestSendSuccess"));
    jQuery.sap.delayedCall(3000, this, function () {
      this.router.navTo("launchpad");
    });
  },
  loadCustomerDiscountConditions: function (evt) {
    //
    this.customerDiscountRequester = new model.CustomerDiscountRequester(this.customer);

    //--------------Version 2------------------------------------------------
    // this function shouldn't  be here
    //because the discount default value should be already loaded if user AG(at login)
    //if user TM o AM use odata to load agentDiscount, eventually when selected the AG.
    var req = this.getOrgDataForSAP();
    req.Kunnr = this.customer.registry.agentCode;
    req.Prsdt = utils.ParseDate().formatDate(new Date(), "yyyy-MM-ddT00:00:00");
    req.Zzagente1 = "";
    req.Kschl = "ZSM3";
    req.Kappl = "V";
    
    this.customerDiscountRequester.loadInitialCustomerDiscountConditionsNew(req)
      .then(_.bind(function (res) {
        this.getView().getModel("c").refresh();
      }, this), _.bind(function (err) {
        sap.m.MessageToast.show("Error loading Customer Discount Details");
        //sap.m.MessageToast.show(utils.Message.getError(err));
      }, this))
      //-------------------------------------------------------------------------

  },
  onCustomerDiscountValueChange: function (evt) {
    var val = evt.getParameter("value");

    //	var discountVal = this.customer.getCustomerDiscountValue();
    
    if(parseInt(val)>this.customerDiscountRequester.maxCustomerDiscount)
    {
    	val = parseInt(this.customerDiscountRequester.maxCustomerDiscount);
    	sap.m.MessageBox.show(model.i18n.getText("discountInputError_maxValueOverflow"));
    }
    this.customer.setCustomerDiscountValue(parseInt(val));
    this._discountModifyFlag = true;
    this.customerModel.setData(this.customer);
  }




});