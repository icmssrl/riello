jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.CustomerRequester");

view.abstract.AbstractController.extend("view.customer.CustomerDetail", {

  onExit: function () {

  },

  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit
      .apply(this, arguments);

    this.prodhListModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.prodhListModel, "pl");


    this.destinationModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.destinationModel, "fdest");



  },

  handleRouteMatched: function (evt) {

    var routeName = evt.getParameter("name");

    if (routeName !== "customerDetail") {
      return;
    }
    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    var id = evt.getParameters().arguments.id;

    var enableModel = new sap.ui.model.json.JSONModel();
    enableModel.setData({
      "detail": true
    });
    this.getView().setModel(enableModel, "en");

    //**
    this.populateSelect();
    this.uiModel.setProperty("/newDestination", true);
    this.onInsertNewAddressReset();
    //**

    var reqData = this.getView().getModel("appStatus").getProperty("/customerRequester") ? this.getView().getModel("appStatus").getProperty("/customerRequester") : model.persistence.Storage.session.get("customerRequester");
    this.customerRequester = new model.CustomerRequester();
    this.customerRequester.setParameters(reqData);
    // model.collections.Customers.getById(id)
    // .then(
    //
    // _.bind(function(result)
    // {
    // this.customer = result;
    // // this.refreshView(result, routeName)
    // this.refreshView(result);
    // }, this)
    // );

    // var ref = this.refreshView(model.Current.getCustomer());
    
    this.user = model.persistence.Storage.session.get("workingUser");
    
    utils.Busy.show();
    this.customerRequester.loadCustomerDetail(id).then(
        _.bind(function (result) {
          utils.Busy.hide();
          this.customer = result;

          this.refreshView(result);
          this.loadDestination();
          //this.loadCustomerDiscountConditions()

        }, this), _.bind(function (error) {
          utils.Busy.hide();
          sap.m.MessageToast.show("Error loading Customer Detail");

          // this.refreshView(result, routeName)
          this.refreshView();
        }, this)
      )
      //.then(_.bind(this.loadDestination(),this));



    this.loadProdhList();

  },
  // refreshView : function(data, route)
  // {
  // var enable = {"editable": false};
  // var page = this.getView().byId("detailPage");
  // if(route && route.toUpperCase().indexOf("edit".toUpperCase())>=0)
  // {
  // var clone = _.cloneDeep(data.getModel().getData());
  // var clonedModel = new sap.ui.model.json.JSONModel(clone);
  // this.getView().setModel(clonedModel, "c");
  // enable.editable = true;
  // var toolbar = sap.ui.xmlfragment("view.fragment.editToolbar", this);
  // page.setFooter(toolbar);
  // //Maybe to parameterize
  // this.populateSelect();
  // //------------------------
  // }
  // else
  // {
  // this.getView().setModel(data.getModel(), "c");
  // enable.editable=false;
  // var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar", this);
  // page.setFooter(toolbar);
  // }
  // var enableModel = new sap.ui.model.json.JSONModel(enable);
  // this.getView().setModel(enableModel, "en");
  //
  // },
  refreshView: function (data) // maybe to abstract
    {
      if (!data) {

        this.getView().getModel("en").setProperty("/detail", false);
        // this.router.navTo("emptyCustomer");
        return;
      }
      // var page = this.getView().byId("detailPage");

      this.getView().setModel(data.getModel(), "c");
      this.getView().getModel("en").setProperty("/detail", true);
      this.getView().getModel("en").setProperty("/editable", false);
      // var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar",
      // this);
      // page.setFooter(toolbar);
      // var enableModel = new sap.ui.model.json.JSONModel(enable);
      // this.getView().setModel(enableModel, "en");

    },
  onStartSessionPress: function () {


      model.persistence.Storage.session.save("customerSession", true);
      model.persistence.Storage.session.save("currentCustomer", this.customer);
      // Maybe is better correcting save class
      model.Current.setCustomer(this.customer);
      // this.customer.setCustomerStatus(this.customer);
      this.router.navTo("launchpad");



  },
  onEditPress: function () {
    this.router.navTo("customerEdit", {
      id: this.customer.getId()
    });
    // this.refreshView(this.customer, "edit");
  },
	onUnlockPress :function()
	{
		// if(!this.validateCheck() )
		// {
		// 		sap.m.MessageToast.show("Client update failed");
		// 		return;
		// }
		// if(!this.onPhoneChanged())
		// {
		// 	sap.m.MessageToast.show("Client update failed: at least a Phone Number required");
		// 	return;
		// }
		var editedCustomer = this.getView().getModel("c").getData();
		// sap.m.MessageToast.show("Update still unrealized ");
		this.customer.update(editedCustomer);
		utils.Busy.show();
		this.customer.updateToSAP(true)
		.then(_.bind(function(res){
			utils.Busy.hide();
			//console.log(res);
			sap.m.MessageToast.show("Client updated with success");
			this.router.navTo("customerDetail", {id:this.customer.getId()})
			}, this),
			_.bind(function(err){

				utils.Busy.hide();
				var msg =utils.Message.getError(err);
				sap.m.MessageToast.show(msg);
				jQuery.sap.delayedCall(3000, this, _.bind(function(){this.router.navTo("customerDetail", {id:this.customer.getId()});}, this))
//				var error = JSON.parse(err.response.body);
//				var errorDetails = error.error.innererror.errordetails;
//				var msg = "Client update failed";
//				var detailsMsg = _.where(errorDetails, function(item){return (item.code.indexOf("IWBEP")>0)});
//
//				for(var i = 0; detailsMsg && detailsMsg.length && i < detailsMsg.length; i++)
//				{
//					msg += ", " +detailsMsg[i].message;
//				}
//				sap.m.MessageToast.show(msg);

			}, this))
		//
		// this.refreshView(this.customer);
	},

  navBackToMobileMaster: function (evt) {
    this.router.navTo("customersListMobile");
  },

  onCreatePress: function (evt) {
    this.getView().getModel("appStatus").setProperty("/fromCustomerDetail",
      true);
    this.router.navTo("newCustomer");
  },

  onLoadCondition: function (evt) {
    utils.Busy.show();
    var fSuccess = function (res) {
      // this.customer.setDiscountCondition(res);
      // this.customer.refresh();
      this.getView().getModel("c").refresh();
      utils.Busy.hide();
    };
    var fError = function (err) {
      sap.m.MessageToast.show(utils.Message.getError(err));
      utils.Busy.hide();
    };

    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);

    var req = {};
    var dataOrg = this.getOrgDataForSAP();
    var userInfo = this.getUserInfo();

    for (var prop in dataOrg) {
      req[prop] = dataOrg[prop];
    }
    req.Vkbur = userInfo.areaManager;
    req.Vkgrp = userInfo.territoryManager;
    req.Cdage = userInfo.agentCode;
    req.Kunnr = this.customer.registry.id;
    req.Prodh1 = this.customer.familyId;

    this.customer.loadDiscountConditions(req).then(fSuccess, fError);
  },

  handleProdhValueHelp: function (evt) {

    if (!this._prodhValueHelpDialog) {
      this._prodhValueHelpDialog = sap.ui.xmlfragment(
        "view.dialog.ProdhValueHelpDialog", this);
    }
    this.getView().addDependent(this._prodhValueHelpDialog);
    this._prodhValueHelpDialog.open();
  },

  handleProdhSearch: function (evt) {
    var sValue = evt.getParameter("value");
    var filters = []
    var idFilter = new sap.ui.model.Filter(
      "familyId",
      sap.ui.model.FilterOperator.Contains, sValue
    );
    filters.push(idFilter);
    var descrFilter = new sap.ui.model.Filter(
      "familyDescr",
      sap.ui.model.FilterOperator.Contains, sValue
    );
    filters.push(descrFilter);

    evt.getSource().getBinding("items").filter(new sap.ui.model.Filter({
      filters: filters,
      and: false
    }));

  },
  handleProdhDialogClose: function (evt) {
    var src = evt.getId();
    if (src == "confirm") {
      var item = evt.getParameter("selectedContexts")[0].getObject();
      this.customer.familyId = item.familyId;
      this.getView().getModel("c").refresh();
    }
    //this._prodhValueHelpDialog.close();
  },

  loadProdhList: function () {
    var defer = Q.defer();
    var orgData = this.getOrgDataForSAP();
    var req = {
    		"Vkorg": orgData.Vkorg,
    		"Vtweg":orgData.Vtweg
    			};

    var fSuccess = function (res) {

      var result = model.persistence.Serializer.prodH.fromSAPItems(res);
      this.prodhListModel.setData(result);
      this.prodhListModel.refresh();
      defer.resolve(result);
    };
    var fError = function (err) {

      sap.m.MessageToast.show(utils.Message.getError(err));
      defer.reject(err);
    };

    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);

    model.odata.chiamateOdata.loadProdHList(req, fSuccess, fError);
    return defer.promise;
  },

  loadDestination: function () {


    var infoForReq = this.getOrgDataForSAP();
    //var agentCode = this.getCustomerInfo().agentCode;
    infoForReq.KunnrAg = this.customer.registry.id;
    infoForReq.Cdage = this.customer.registry.agentCode;
    infoForReq.Parvw = "WE";
    delete infoForReq.Bukrs;

    var defer = Q.defer();
    var fSuccess = function (result) {
      this.dests = {
        "items": []
      };
      //destinations.items = this.customer.destinations;
      (this.dests).items = result;
      this.customer.setDestinations(result);
      //**
      this.customer.getModel().refresh();
      this.getView().getModel("c").refresh();
      defer.resolve(result);

    };
    var fError = function (err) {
      //console.log(err);
      defer.reject(err);
      sap.m.MessageToast.show("problems loading destinations");
    };

    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);
    var destModel = new model.Destination;
    destModel.loadDestinations(infoForReq).then(fSuccess, fError);

    return defer.promise;

  },


  onInsertNewAddressReset: function (evt) {
    var cleanDest = new model.Destination();
    this.destinationModel.setData(cleanDest);
    this.getView().getModel("fdest").refresh();
  },

  onInsertNewAddressCancel: function () {
    //this.onInsertNewAddressReset();
    this.insertNewAddressDialog.close();
  },

  onAddDestination: function () {
    if (!this.insertNewAddressDialog) {
      this.insertNewAddressDialog = sap.ui.xmlfragment("view.dialog.insertNewAddress", this);
      var page = this.getView().byId("detailPage");
      page.addDependent(this.insertNewAddressDialog);
    }
    this.onInsertNewAddressReset();
    this.insertNewAddressDialog.open();
  },
  onInsertNewAddressConfirm: function () {
  
    var fDestModel = this.getView().getModel("fdest").getData();
   

    var infoForReq = this.getOrgDataForSAP();
    infoForReq.KunnrAg = this.customer.registry.id;
    infoForReq.Cdage = this.customer.registry.agentCode;
    infoForReq.Parvw = "WE";
    delete infoForReq.Bukrs;
     for(var prop in fDestModel)
    {
      if(fDestModel[prop] && !_.isFunction(fDestModel[prop]))
      infoForReq[prop] = fDestModel[prop];
    }

    var fSuccess = function () {
      utils.Busy.hide();
      if (this.insertNewAddressDialog) {
        this.insertNewAddressDialog.close();
      }
      this.loadDestination();
    };
    var fError = function (err) {
      utils.Busy.hide();
      if (this.insertNewAddressDialog) {
        this.insertNewAddressDialog.close();
      }
    };
    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);

    model.Destination().addDestination(infoForReq).then(fSuccess,fError);



    //this.getView().getModel("o").refresh();
    //}
  },



  populateSelect: function () {
    var pToSelArr = [
      {
        "type": "Land1",
        "namespace": "land"
      }
    ];

    var workingUser = model.persistence.Storage.session.get("workingUser");
    this.orgData = {
      "Bukrs": workingUser.organizationData.results[0].society,
      "Vkorg": workingUser.organizationData.results[0].salesOrg,
      "Vtweg": workingUser.organizationData.results[0].distributionChannel,
      "Spart": workingUser.organizationData.results[0].division
    };
    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.getOdataSelect(item.type, this.orgData)
        .then(_.bind(function (result) {
          if (item.namespace === "codElabSpeciale" || item.namespace === "shippingType") {
            result.getData().results.unshift({});
          }
          this.getView().setModel(result, item.namespace);
        }, this));
    }, this));

  },

  populateRegionSelect: function (evt) {
    var req = {
      "Land1": ""
    };
    req.Land1 = evt.getSource().getSelectedKey();
    utils.Collections.getOdataSelect("Regio", req)
      .then(_.bind(function (result) {
        result.setSizeLimit(500);
        this.getView().setModel(result, "prov");
        this.uiModel.setProperty("/enableRegion", true);
      }, this));

  },
//--------Version 2.0 ------------------------------
  loadCustomerDiscountConditions:function(evt)
  {

    this.customerDiscountRequester = new model.CustomerDiscountRequester(this.customer);
    this.customerDiscountRequester.loadCustomerDiscountConditions()
    .then(_.bind(function(res){
      this.refreshView(this.customer);
    }, this), _.bind(function(err){
      sap.m.MessageToast.show("Error loading customer discounts Details");
      //sap.m.MessageToast.show(utils.Message.getError(err));
    }, this))


  },
//-----------------------------------------------------
  onCustomerDiscountEditPress:function(evt)
  {
	  this.customerDiscountRequester = new model.CustomerDiscountRequester(this.customer);
	  this.customerDiscountRequester.loadMaxCustomerDiscountValue(this.user)
	  .then(_.bind(function(res){
		  this.customerDiscountModel=this.customerDiscountRequester.getModel();
	      this.getView().setModel(this.customerDiscountModel, "cd");

	    if(!this.customerDiscountDialog)
	    {
	      this.customerDiscountDialog = sap.ui.xmlfragment("view.dialog.CustomerDiscountDialog", this);
	    }
	    var page = this.getView().byId("detailPage");

	    page.addDependent(this.customerDiscountDialog);
	    this.customerDiscountDialog.open();
		  
	  }, this))
      
  },

  onCustomerDiscountValueChange:function(evt)
	{
	  if(parseFloat(this.customerDiscountModel.getProperty("/value"))>parseFloat(this.customerDiscountModel.getProperty("/maxCustomerDiscount")))
	    {
		  this.customerDiscountModel.setProperty("/value", parseFloat(this.customerDiscountModel.getProperty("/maxCustomerDiscount")));
	    	
		  sap.m.MessageBox.show(model.i18n.getText("discountInputError_maxValueOverflow"));
	    }
		//this.customerDiscountModel.setProperty("/value", parseFloat(this.customerDiscountModel.getProperty("/value")));
		this.customerDiscountModel.refresh();
	},
	onCustomerDiscountClose:function(evt)
	{
		this.customerDiscountDialog.close();
	},
	onCustomerDiscountOK:function(evt)
	{
		this.customerDiscountDialog.close();
		var type = (model.persistence.Storage.session.get("user")).type
		var user = (model.persistence.Storage.session.get("workingUser"));
		//---------------------------------Version 2-------------------------------------
		//Discount must be update to a specific odata
		this.customerDiscountRequester.updateCustomerDiscount(user) 
		.then(_.bind(function(res){
			console.log(this.customer);
			sap.m.MessageToast.show(model.i18n.getText("discountRequestSendSuccess"));
			this.refreshView(this.customer);
			this.eventBus = sap.ui.getCore().getEventBus();
			this.eventBus.publish("discountChannel", "discountUpdated");
			}, this),
			_.bind(function(err)
			{
				sap.m.MessageToast.show(model.i18n.getText("errorDiscountUpdate"));
			}))
		//-------------------------------------------------------------------------------
		
//    this.customerDiscountRequester.saveCustomerDiscountValue(user, this.customer.discountStatus);
//    sap.m.MessageToast.show(model.i18n.getText("discountRequestSendSuccess"));
//    this.refreshView(this.customer);
		// jQuery.sap.delayedCall(3000, this, function () {
		// 	this.router.navTo("launchpad");
		// });
	},




  // onSavePress :function()
  // {
  // var editedCustomer = this.getView().getModel("c").getData();
  // this.customer.update(editedCustomer);
  // this.router.navTo("customerDetail", {id:this.customer.getId()});
  // // this.refreshView(this.customer);
  // },
  // onResetPress : function()
  // {
  // this.getView().getModel("c").setData(_.cloneDeep(this.customer.getModel().getData()));
  // },

  // populateSelect : function()
  // {
  // //HP1 : Maybe it's possible creating a mapping file on which every collection
  // has its params to select
  // //HP2 : Every collections contains an array of params where a select is
  // needed
  // var pToSelArr = [
  // {"type":"registryTypes", "namespace":"rt"},
  // {"type":"billTypes", "namespace":"bt"},
  // {"type":"contactTypes", "namespace":"ct"},
  // {"type":"paymentConditions", "namespace": "pc"},
  // {"type": "places", "namespace":"p"}
  // ];
  //
  // _.map(pToSelArr, _.bind(function(item)
  // {
  // utils.Collections.getModel(item.type)
  // .then(_.bind
  // (function(result)
  // {
  // this.getView().setModel(result, item.namespace);
  //
  // }, this))
  // }, this));
  //
  //
  // }

});
// >>>>>>> 0b06617131203541244a9c31da928befd31b5b5a

