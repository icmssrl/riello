jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.TransportDates");

view.abstract.AbstractController.extend("view.gestioneTrasportoDate.TransportDates", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);


    this.inputModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.inputModel, "in");
    //    this.valueStateModel.setProperty("valueState", "");

  },


  handleRouteMatched: function (evt) {

    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    var routeName = evt.getParameter("name");

    if (routeName !== "transportDates") {
      return;
    }
    this.dateModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.dateModel, "d");
    this.modelData = new model.TransportDates();
    this.uiModel.setProperty("/enableRegion", false);
    this.resetModel();
    this.populateSelect();

  },

  populateSelect: function () {
    var pToSelArr = [
      {
        "type": "Sdabw",
        "namespace": "codElabSpeciale"
      },
      {
        "type": "Inco1",
        "namespace": "incoterms"
      },
      {
        "type": "Vsart",
        "namespace": "shippingType"
      },
      {
        "type": "Land1",
        "namespace": "nation"
      }
    ];

    var workingUser = model.persistence.Storage.session.get("workingUser");
    this.orgData = {
      "Bukrs": workingUser.organizationData.results[0].society,
      "Vkorg": workingUser.organizationData.results[0].salesOrg,
      "Vtweg": workingUser.organizationData.results[0].distributionChannel,
      "Spart": workingUser.organizationData.results[0].division
    };
    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.getOdataSelect(item.type, this.orgData)
        .then(_.bind(function (result) {
          if (item.namespace === "codElabSpeciale" || item.namespace === "shippingType") {
            result.getData().results.unshift({});
          }
          this.getView().setModel(result, item.namespace);
        }, this));
    }, this));

  },

  populateRegionSelect: function (evt) {
	this.onRegionSelect(); //resetta la checkbox
    
    var req = {
      "Land1": ""
    };
    req.Land1 = evt.getSource().getSelectedKey();
    utils.Collections.getOdataSelect("Regio", req)
      .then(_.bind(function (result) {
        result.setSizeLimit(500);
        var combo = this.getView().byId("prov");
        combo.setSelectedItem(combo.getDefaultSelectedItem())
        this.getView().setModel(result, "prov");
        //		this.getView().getModel("c").refresh();
        this.uiModel.setProperty("/enableRegion", true);
      }, this));

  },


  onFindTransDate: function () {

    //sap.m.MessageBox.show("Under Construction");
    utils.Busy.show();
    var fSuccess = function (result) {
      utils.Busy.hide();
      this.dateModel.setData(result);
    };
    var fError = function (err) {
      utils.Busy.hide();
      sap.m.MessageToast.show(utils.Message.getError(err));
    };

    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);

    var req = {};

    for (var prop in this.orgData) {
      req[prop] = this.orgData[prop];
    }
    var input = this.inputModel.getData();
    for (var prop in input) {
      req[prop] = input[prop];
    }
    //console.log(req);
    this.modelData.getTransportDate(req).then(fSuccess, fError);

  },

  resetModel: function () {
    this.inputModel.setProperty("/incoterms", "");
    this.inputModel.setProperty("/shippingType", "");
    this.inputModel.setProperty("/codElabSpeciale", "");
    this.inputModel.setProperty("/nation", "");
    this.inputModel.setProperty("/prov", "");
    this.inputModel.setProperty("/provincia", "");
    this.inputModel.setProperty("/centroCitta", false);
    if (this.getView().byId("dichiarazioneCartCheckBox")) {
      this.getView().byId("dichiarazioneCartCheckBox").setSelected(false);
    }
  },

  checkSelectedCheckBox: function (evt) {
    var comboBox = this.getView().byId("prov");

    if (evt.getSource().getSelected()) {
      if (comboBox.getSelectedItem()) {
        this.inputModel.setProperty("/provincia", comboBox.getSelectedItem().getText());
      }
    }
    else {
        this.inputModel.setProperty("/provincia", "");
    }
  },
  
  onRegionSelect : function(evt)
  {
	  this.inputModel.getData().centroCitta = false;
	  this.inputModel.setProperty("/provincia", "");
	  this.inputModel.refresh();
  }



});