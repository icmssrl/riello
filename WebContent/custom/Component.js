jQuery.sap.declare("icms.Component");
jQuery.sap.require("icms.MyRouter");


sap.ui.core.UIComponent.extend("icms.Component", {
  metadata: {
    config: {
      resourceBundle: "custom/i18n/text.properties",

      //puntati dalla classe settings.core [custom/settins]
      //			settings: {
      //				lang: "IT",
      //				isMock:true,
      //				serverUrl:"sap.blabla:8000"
      //			},
      settings: {

				lang: "IT",

				isMock:false,


        // serverUrl:"https://xft.riellogroup.com/sap/opu/odata/sap/z_xf_srv/",
        // logoffUrl:"https://xft.riellogroup.com/sap/public/bc/icf/logoff",

        servers: [
          {
            serverUrl:"http://sap-nwg.sapit.riello.lan/sap/opu/odata/sap/z_xf_srv/",
            logoffUrl:"http://sap-nwg.sapit.riello.lan/sap/public/bc/icf/logoff",
            path:"http://sap-nwg.sapit.riello.lan/",
            id:"dev"
          },
          {
            serverUrl:"http://10.1.155.121:8000/sap/opu/odata/sap/z_xf_srv/",
            logoffUrl:"http://10.1.155.121:8000/sap/public/bc/icf/logoff",
            path:"http://10.1.155.121:8000/",
            id:"dev"
          },
          {
            serverUrl:"http://sap-ngt.sapit.riello.lan/sap/opu/odata/sap/z_xf_srv/",
            logoffUrl:"http://sap-ngt.sapit.riello.lan/sap/public/bc/icf/logoff",
            path:"http://sap-ngt.sapit.riello.lan/",
            id:"test"
          },
          {
            serverUrl:"http://10.1.155.161:8000/sap/opu/odata/sap/z_xf_srv/",
            logoffUrl:"http://10.1.155.161:8000/sap/public/bc/icf/logoff",
            path:"http://10.1.155.161:8000/",
            id:"test"
          },
          {
            serverUrl:"https://xft.riellogroup.com/sap/opu/odata/sap/z_xf_srv/",
    		logoffUrl:"https://xft.riellogroup.com/sap/public/bc/icf/logoff",
            path:"https://xft.riellogroup.com/",
            id:"test"
          },
          {
        	  serverUrl:"https://oro.riello.com/sap/opu/odata/sap/z_xf_srv/",
        	  logoffUrl:"https://oro.riello.com/sap/public/bc/icf/logoff",
              path:"https://oro.riello.com/",
              id:"prd"
          },
          {
        	  serverUrl:"https://oro.riello.com/sap/opu/odata/sap/z_xf_srv/",
        	  logoffUrl:"https://oro.riello.com/sap/public/bc/icf/logoff",
              path:"https://oro.riello.com/"
          },
          {
              serverUrl:"https://xfp.riellogroup.com/sap/opu/odata/sap/z_xf_srv/",
              logoffUrl:"https://xfp.riellogroup.com/sap/public/bc/icf/logoff",
              path:"https://xfp.riellogroup.com/",
              id:"prd"
            }

        ],

//
				serverUrl:"http://sap-nwg.sapit.riello.lan/sap/opu/odata/sap/z_xf_srv/",
        logoffUrl:"http://sap-nwg.sapit.riello.lan/sap/public/bc/icf/logoff",
        id:"dev",


//				 serverUrl:"http://sap-ngt.sapit.riello.lan/sap/opu/odata/sap/z_xf_srv/",
//         logoffUrl:"http://sap-ngt.sapit.riello.lan/sap/public/bc/icf/logoff",
//         id:"test",


//        Sviluppo

//
         //serverUrl:"http://10.1.155.121:8000/sap/opu/odata/sap/z_xf_srv/",
         //logoffUrl:"http://10.1.155.121:8000/sap/public/bc/icf/logoff"


        //Test
//       serverUrl:"http://10.1.155.161:8000/sap/opu/odata/sap/z_xf_srv/",
//       logoffUrl:"http://10.1.155.161:8000/sap/public/bc/icf/logoff"



        serverUrl:"proxy/sap/opu/odata/sap/z_xf_srv/",
         logoffUrl:"proxy/sap/public/bc/icf/logoff",
         id:"dev",



				//public link

				// serverUrl:"https://xft.riellogroup.com/sap/opu/odata/sap/z_xf_srv/",
				// logoffUrl:"https://xft.riellogroup.com/sap/public/bc/icf/logoff"

        	//Prod?


        	 //serverUrl:"https://oro.riello.com/sap/opu/odata/sap/z_xf_srv/",
       	  	//logoffUrl:"https://oro.riello.com/sap/public/bc/icf/logoff",


//          serverUrl:"https://xfp.riellogroup.com/sap/opu/odata/sap/z_xf_srv/",
//           logoffUrl:"https://xfp.riellogroup.com/sap/public/bc/icf/logoff"

			},


      //rootView: 'view.App'
    },
    includes: [ //importare css di custom e lib
			"css/custom.css",
			"libs/lodash.js",
			"libs/q.js",
      "libs/jquery.base64.js"
			],
    dependencies: {
      libs: [
				"sap.m",
				"sap.ui.layout"
				]
    },

    routing: {
      config: {
        viewType: "XML",
        viewPath: "view",
        clearTarget: false,
        // targetControl: "app",
        transition: "slide",
        // targetAggregation: "detailPages",
      },

      routes: [
        {
          name: "login",
          pattern: "",
          view: "Login",
          viewId: "loginId",
          targetAggregation: "pages",
          targetControl: "app"
				},
        {
          name: "soLaunchpad",
          pattern: "SOlaunchpad",
          view: "SOlaunchpad",
          viewId: "soLaunchpadId",
          targetAggregation: "pages",
          targetControl: "app"
				},
//		{
//	          name: "firstAccess",
//	          pattern: "FirstAccess",
//	          view: "FirstAccess",
//	          viewId: "firstaAccessId",
//	          targetAggregation: "pages",
//	          targetControl: "app"
//					},

//				{
//					name: "divisionLaunchpad",
//					pattern: "divisionLaunchpad/{society}",
//					view: "DivisionLaunchpad",
//					viewId: "divisionLaunchpadId",
//					targetAggregation:"pages",
//					targetControl:"app"
//				},

        {
          name: "launchpad",
          pattern: "home",
          view: "Launchpad",
          viewId: "launchpadId",
          targetAggregation: "pages",
          targetControl: "app"
				},
        {
          name: "changePassword",
          pattern: "changePassword",
          view: "user.ChangePassword",
          viewId: "changePasswordId",
          targetAggregation: "pages",
          targetControl: "app"
				},
        {
          name: "orderList",
          pattern: "orderList",
          view: "order.OrderList",
          viewId: "orderListId",
          targetAggregation: "pages",
          targetControl: "app"
				},
		{
	          name: "returnList",
	          pattern: "returnList",
	          view: "praticheAnomale.ReturnList",
	          viewId: "returnListId",
	          targetAggregation: "pages",
	          targetControl: "app"
					},
        {
          name: "newOrder",
          pattern: "home/order/new",
          view: "order.OrderCreate",
          viewId: "orderCreateId",
          targetAggregation: "pages",
          targetControl: "app"
				},

        ///*************************Master Mobile Views***************************////

          {
            name: "customersListMobile",
            pattern: "customersListMobile",
            view: "customer.CustomersList",
            viewId: "customersListId",
            targetAggregation:"pages",
            targetControl:"app"
          },
          {
            name: "productsListMobile",
            pattern: "productsListMobile",
            view: "product.ProductsList",
            viewId: "productsListId",
            targetAggregation:"pages",
            targetControl:"app"
          },
          {
            name: "readOnlyHierarchyMobile",
            pattern: "readOnlyHierarchyMobile",
            view: "product.ProductsList",
            viewId: "productsListId",
            targetAggregation:"pages",
            targetControl:"app"
          },
          {
            name: "historyCartsListMobile",
            pattern: "historyCartsListMobile",
            view: "HistoryCartsList",
            viewId: "historyCartsListId",
            targetAggregation:"pages",
            targetControl:"app"
          },
          {
            name: "shippment.shippmentsListMobile",
            pattern: "shippment.shippmentsListMobile",
            view: "shippment.ShippmentsList",
            viewId: "shippmentsListId",
            targetAggregation:"pages",
            targetControl:"app"
          },




          ///***********************************************************************////


        {
          name: "orderInfo",
          pattern: "orderInfo/{id}",
          view: "order.OrderInfo",
          viewId: "orderInfoId",
          targetAggregation: "pages",
          targetControl: "app"
				},
        {
          pattern: "split",
          name: "splitApp",
          view: "SplitApp",
          viewType: "JS",
          targetControl: "app",
          targetAggregation: "pages",
          subroutes: [
            {
              name: "customersList",
              pattern: "home/customers/list",
              view: "customer.CustomersList",
              viewId: "customersListId",
              targetAggregation: "masterPages",
              targetControl: "splitApp",
              subroutes: [
                {
                  name: "customerDetail",
                  pattern: "home/customers/detail/{id}",
                  view: "customer.CustomerDetail",
                  viewId: "customerDetailId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp"
								},
                {
                  name: "customerEdit",
                  pattern: "home/customers/edit/{id}",
                  view: "customer.CustomerEdit",
                  viewId: "customerEditId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp"
								},
                {
                  name: "emptyCustomer",
                  pattern: "home/customers/list/:empty:",
                  view: "Empty",
                  viewId: "emptyId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp"
								}

							]
					 	},

            {
              name: "productsList",
              pattern: "products",
              view: "product.ProductsList",
              viewId: "productsListId",
              targetAggregation: "masterPages",
              targetControl: "splitApp",

              subroutes: [
                {
                  name: "productDetail",
                  pattern: "products/detail/{id}",
                  view: "product.ProductDetail",
                  viewId: "productDetailId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp",

								},
                {
                  name: "productDetailMobile",
                  pattern: "products/detailMobile/{id}",
                  view: "product.ProductDetailMobile",
                  viewId: "productDetailId",
                  targetAggregation: "pages",
                  targetControl: "app",

								},

                {
                  name: "empty",
                  pattern: "products/empty",
                  view: "Empty",
                  viewId: "emptyId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp"
								}

							]
					 	},
            {
              name: "shippment.shippmentsList",
              pattern: "shippments",
              view: "shippment.ShippmentsList",
              viewId: "ShippmentsListId",
              targetAggregation: "masterPages",
              targetControl: "splitApp",

              subroutes: [
                {
                  name: "shippment.shippmentDetail",
                  pattern: "shippment/detail/{id}",
                  view: "shippment.ShippmentDetail",
                  viewId: "ShippmentDetailId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp",

								},

                {
                  name: "noDataSplitDetail",
                  pattern: "shippment/empty",
                  view: "NoDataSplitDetail",
                  viewId: "noDataSplitDetailId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp"
								}

							]
					 	},
            {
              name: "readOnlyHierarchy",
              pattern: "readOnlyHierarchy",
              view: "product.ProductsList",
              viewId: "productsListId",
              targetAggregation: "masterPages",
              targetControl: "splitApp",

              subroutes: [
                {
                  name: "productRODetail",
                  pattern: "productsRO/detail/{id}",
                  view: "product.ProductRODetail",
                  viewId: "productRODetailId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp",

								},
                //pagina mobile
                {
                  name: "productROMobileDetail",
                  pattern: "productsRO/detailMobile/{id}",
                  view: "product.ProductROMobileDetail",
                  viewId: "productRODetailId",
                  targetAggregation: "pages",
                  targetControl: "app",

								},
                //
                {
                  name: "emptyROHierarchy",
                  pattern: "roHierarchy/empty",
                  view: "Empty",
                  viewId: "emptyId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp"
								}

							]
					 	},

            {
              name: "historyCartsList",
              pattern: "historyCarts",
              view: "historyCart.HistoryCartsList",
              viewId: "historyCartsListId",
              targetAggregation: "masterPages",
              targetControl: "splitApp",

              subroutes: [
                {
                  name: "historyCartsDetail",
                  pattern: "historyCarts/detail/{id}",
                  // view: "HistoryCartsDetail",
                  view: "historyCart.CartInfo",
                  viewId: "HistoryCartsDetailId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp"

								},
                {
                  name: "emptyHistoryCarts",
                  pattern: "historyCarts/empty",
                  view: "Empty",
                  viewId: "emptyId",
                  targetAggregation: "detailPages",
                  targetControl: "splitApp"
								}

							]
					 	}

						// {
						// 	name: "customerEdit",
						// 	pattern: "customers/edit/{id}",
						// 	view: "CustomerDetail",
						// 	viewId: "customerDetailId",
						// 	targetAggregation:"detailPages",
						// 	targetControl:"splitApp"
						// }

					]
				},
        {
          name: "cartFullView",
          pattern: "cart/detail",
          view: "order.CartFull",
          viewId: "cartFullId",
          targetAggregation: "pages",
          targetControl: "app",
          transition: "flip"
				},
		{
              name: "resoDetail",
              pattern: "reso/detail/{id}",
              view: "praticheAnomale.ResoDetail",
              viewId: "resoDetailId",
              targetAggregation: "pages",
              targetControl: "app",

							},

        {
          name: "praticheAnomale.ResoConNC",
          pattern: "resoConNotaCredito/{type}/consegnaNr{shippmentId}",
//          view: "praticheAnomale.ResoConNC",
//          viewId: "resoConNCId",
          view: "praticheAnomale.Reso",
          viewId: "resoId",
          targetAggregation: "pages",
          targetControl: "app"

				},

        {
          name: "praticheAnomale.ResoSenzaNC",
          pattern: "resoSenzaNotaCredito/{type}/consegnaNr{shippmentId}",
//          view: "praticheAnomale.ResoSenzaNC",
//          viewId: "resoSenzaNCId",
          view: "praticheAnomale.Reso",
          viewId: "resoId",
          targetAggregation: "pages",
          targetControl: "app"

				},
		{
	          name: "praticheAnomale.MancanzaMerce",
	          pattern: "mancanzaMerce/{type}/consegnaNr{shippmentId}",
//			          view: "praticheAnomale.ResoSenzaNC",
//			          viewId: "resoSenzaNCId",
	          view: "praticheAnomale.Reso",
	          viewId: "resoId",
	          targetAggregation: "pages",
	          targetControl: "app"

					},
		{
	          name: "praticheAnomale.OrdPerSostituzione",
	          pattern: "ordPerSostituzione/{type}/consegnaNr{shippmentId}",
//						          view: "praticheAnomale.ResoSenzaNC",
//						          viewId: "resoSenzaNCId",
	          view: "praticheAnomale.Reso",
	          viewId: "resoId",
	          targetAggregation: "pages",
	          targetControl: "app"

					},

        {
          name: "praticheAnomale.RichiestaRicambi",
          pattern: "richiestaRicambi/{type}/consegnaNr{shippmentId}",
          view: "praticheAnomale.RichiestaRicambi",
          viewId: "richiestaRicambiId",
          targetAggregation: "pages",
          targetControl: "app"

				},
//        {
//          name: "praticheAnomale.MancanzaMerce",
//          pattern: "mancanzaMerce/consegnaNr{shippmentId}",
//          view: "praticheAnomale.MancanzaMerce",
//          viewId: "mancanzaMerceId",
//          targetAggregation: "pages",
//          targetControl: "app"
//
//				},
        {
          name: "newCustomer",
          pattern: "home/customer/new",
          view: "customer.CustomerCreate",
          viewId: "newCustomerId",
          targetAggregation: "pages",
          targetControl: "app"

				},
        {
          name: "fullCustomerDetail",
          pattern: "customer/detail/fullView",
          view: "customer.FullCustomerDetail",
          viewId: "fullCustomerDetailId",
          targetAggregation: "pages",
          targetControl: "app"

				},
        {
          name: "transportDates",
          pattern: "transportDates",
          view: "gestioneTrasportoDate.TransportDates",
          viewId: "transportDatesId",
          targetAggregation: "pages",
          targetControl: "app"

				},
        {
          name: "billsCommissions",
          pattern: "fattureProvvigioni",
          view: "fattureProvvigioni.fattureProvvigioni",
          viewId: "fattureProvvigioniId",
          targetAggregation: "pages",
          targetControl: "app"

				},

        //------------------Version 2.0------------------------
        {
          name: "customerDiscountApprovations",
          pattern: "customerDiscountApprovations",
          view: "customerDiscount.ApprovationsList",
          viewId: "customerDiscountApprovationId",
          targetAggregation: "pages",
          targetControl: "app"

				},
        {
          name: "fullCustomerDetail4Approvation",
          pattern: "customer/detailFullView/{id}",
          view: "customer.FullCustomerDetailExt",
          viewId: "fullCustomerDetailExtId",
          targetAggregation: "pages",
          targetControl: "app"

				},
        {
          name: "newOffer",
          pattern: "home/offer/new",
          view: "offer.OfferCreate",
          viewId: "offerCreateId",
          targetAggregation: "pages",
          targetControl: "app"
				},
        {
          name: "offersList",
          pattern: "offerlist",
          view: "offer.OffersList",
          viewId: "offersListId",
          targetAggregation: "pages",
          targetControl: "app"
				},
        {
          name: "offerApprovationsList",
          pattern: "offerApprovationslist",
          view: "offer.OfferApprovationsList",
          viewId: "offerApprovationsListId",
          targetAggregation: "pages",
          targetControl: "app"
				},
        {
          name: "offerInfo",
          pattern: "offerInfo/{id}",
          view: "offer.OfferInfo",
          viewId: "offerInfoId",
          targetAggregation: "pages",
          targetControl: "app"
        },
        {
          name: "offerCartFullView",
          pattern: "offer/cart/detail",
          view: "offer.OfferCartFull",
          viewId: "offerCartFullId",
          targetAggregation: "pages",
          targetControl: "app",
          transition: "flip"
				},
        //---------------------------------------------------------
		// {
    //          name: "discountList",
    //          pattern: "discountRequest",
    //          view: "DiscountList",
    //          viewId: "historyCartsListId",
    //          targetAggregation: "masterPages",
    //          targetControl: "splitApp",
    //
    //          subroutes: [
    //            {
    //              name: "discountDetail",
    //              pattern: "discountRequest/detail/{gamma}/:{discountType}:",
    //              // view: "HistoryCartsDetail",
    //              view: "DiscountDetail",
    //              viewId: "discountDetailId",
    //              targetAggregation: "detailPages",
    //              targetControl: "splitApp"
    //
		// 						},
		// 		{
    //              name: "discountCreation",
    //              pattern: "discountRequest/new",
    //              // view: "HistoryCartsDetail",
    //              view: "DiscountDetail",
    //              viewId: "discountDetailId",
    //              targetAggregation: "detailPages",
    //              targetControl: "splitApp"
    //
		// 						},
    //
    //            {
    //              name: "emptyDiscount",
    //              pattern: "discountRequest/empty",
    //              view: "Empty",
    //              viewId: "emptyId",
    //              targetAggregation: "detailPages",
    //              targetControl: "splitApp"
		// 						}]
		// }

	]}
  },

  /**
   * !!! The steps in here are sequence dependent !!!
   */
  init: function () {

    //il ns icms va cambiato a seconda del nome app e nome cliente
    jQuery.sap.registerModulePath("icms", "custom");
    jQuery.sap.registerModulePath("view", "custom/view");
    jQuery.sap.registerModulePath("model", "custom/model");
    jQuery.sap.registerModulePath("utils", "custom/utils");


    // 2. call overridden init (calls createContent)
    sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

    var oI18nModel = new sap.ui.model.resource.ResourceModel({
      bundleUrl: [this.getMetadata().getConfig().resourceBundle]
    });
    this.setModel(oI18nModel, "i18n");
    if (sessionStorage.getItem("language")) {
      sap.ui.getCore().getConfiguration().setLanguage(sessionStorage.getItem("language"));
    }
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    //sap.ui.getCore().setModel(oI18nModel, "i18n");

    // 3a. monkey patch the router
    jQuery.sap.require("sap.m.routing.RouteMatchedHandler");
    var router = this.getRouter();
    router.myNavBack = icms.MyRouter.myNavBack;
    router.myNavToWithoutHash = icms.MyRouter.myNavToWithoutHash;
    icms.MyRouter.router = router;

    this.routeHandler = new sap.m.routing.RouteMatchedHandler(router);
    router.initialize();


    // set device model
    var oDeviceModel = new sap.ui.model.json.JSONModel({
      isTouch: sap.ui.Device.support.touch,
      isNoTouch: !sap.ui.Device.support.touch,
      isPhone: sap.ui.Device.system.phone,
      isNoPhone: !sap.ui.Device.system.phone,
      listMode: (sap.ui.Device.system.phone) ? "None" : "SingleSelectMaster",
      listItemType: (sap.ui.Device.system.phone) ? "Active" : "Inactive"
    });
    oDeviceModel.setDefaultBindingMode("OneWay");
    this.setModel(oDeviceModel, "device");

    var appStatusModel = new sap.ui.model.json.JSONModel();
    appStatusModel.setProperty("/navBackVisible", false);
    this.setModel(appStatusModel, "appStatus");

  },




  destroy: function () {
    if (this.routeHandler) {
      this.routeHandler.destroy();
    }

    // call overridden destroy
    sap.ui.core.UIComponent.prototype.destroy.apply(this, arguments);
  },

createContent: function () {
	    // create root view
	    var oView = sap.ui.view({
	      id: "mainApp",
	      viewName: "view.App",
	      type: "JS",
	      viewData: {
	        component: this
	      }
	    });

	    return oView;
  }
});
